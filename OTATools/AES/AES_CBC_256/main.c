// AES_128_CBC_Test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

//#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <malloc.h>
#include <math.h>
//#include "aes_cbc.h"
#include "aes_soft.h"

#define CBC 1
#define CHUNK_SIZE_FLOAT   16.0
#define CHUNK_SIZE         16
//#define AES_DBUG
typedef struct binary_data_t
{
    uint32_t    size;
    uint8_t *   data;
}binary_data_t;

//#define AES_TEST_DATA_LEN       (1024 + 1 + 4UL)
//#define AES_TEST_PADDING_LEN    ((AES_TEST_DATA_LEN + 15) / 16 *16)

//const uint8_t aes_key[] = { 0xfd, 0xef, 0xe7, 0x82, 0x76, 0x61, 0x23, 0x3c, 0x00, 0x6b, 0x8a, 0x98, 0x47, 0x35, 0x89, 0x06 };
const uint8_t aes_key[] = { 0xfb, 0xcf, 0xe7, 0x82, 0x85, 0x64, 0x71, 0x1d, 0x01, 0x6e, 0x5f, 0x24, 0x17, 0x20, 0x43, 0x03,
                    0xf0, 0xfe, 0xc9, 0x42, 0x26, 0x55, 0x73, 0x1b, 0x00, 0x4a, 0x9f, 0x44, 0x66, 0x3f, 0x81, 0x08 };

size_t key_len = 32;

const uint8_t aes_iv[] = { 0xcb, 0xfe, 0x1a, 0x2e, 0xfd, 0xae, 0x4b, 0xbd, 0xdf, 0x3a, 0xf8, 0x98, 0x57, 0x20, 0x84, 0x07 };
uint8_t iv_len = 16;


//uint8_t aes_soft_in_data[AES_TEST_PADDING_LEN];

//uint8_t aes_soft_out_data[AES_TEST_PADDING_LEN];




binary_data_t* read_file(const char* filename)
{
    binary_data_t* binary_data = malloc(sizeof(binary_data_t));
    uint32_t file_length;

    FILE* f_in = fopen(filename, "rb");

    if (!f_in)
    {
        printf("Unable to open file %s\r\n", filename);
        return NULL;
    }
    // get file length
    fseek(f_in, 0, SEEK_END);
    file_length = ftell(f_in);
    fseek(f_in, 0, SEEK_SET);

	// if file original size is not multiple of 16 bytes
	uint32_t new_file_length = ceil(file_length / 16.0) * 16;
	
    //Allocate enough space to read the whole file
    binary_data->size = new_file_length;
    binary_data->data = (char*)malloc(binary_data->size);
	memset(binary_data->data, 0, binary_data->size);
    if (!(binary_data->data))
    {
        printf("Memory error!\r\n");
        fclose(f_in);
        return NULL;
    }

    fread(binary_data->data, file_length, 1, f_in);
    fclose(f_in);

	return binary_data;    
}


int write_file(const char* filename, char *binary_data, int data_size)
{
    FILE* f_out;
    f_out = fopen(filename, "wb");
    if (!f_out)
    {
        printf("Unable to open file to write\r\n");
        return 1;
    }
    fwrite(binary_data, data_size, 1, f_out);
    fclose(f_out);
    printf("Wrote to binary file %s\r\n", filename);
    return 0;
}


void dump_buffer(uint8_t * buffer, int buffer_size)
{
    int i;

    for (i = 0; i < buffer_size; ++i)
        printf("%02X", buffer[i]);
    printf("\r\n");
}

int main(int argc, char* argv[])
{
    
    int ret = 0;
    char bin_file_name[64];
    binary_data_t* p_bin;
    uint8_t *aes_out_data;
    uint8_t *aes_in_data;
    uint8_t *aes_dec_data;
    uint8_t* aes_enc_data;

    uint32_t aes_file_length;



    if (argc > 1)
    {
        strcpy(bin_file_name, argv[1]);
    }
    else
    {
        printf("Enter the binary file name: ");
        int ret = scanf("%s", bin_file_name);
		if (ret == EOF)
        {
            printf("Error in reading bin file!\r\n");
            return 1;
        }
	}

	if (strcmp(bin_file_name, "") != 0)
	{
		printf("bin file name is %s\r\n", bin_file_name);
		p_bin = read_file(bin_file_name);
		
		//dump_buffer(p_bin->data, p_bin->size);
		printf("File size is %d\r\n", p_bin->size);
		
		// write to a new binary file to test fread and fwrite
		//ret = write_file("bin_hex_c.bin", p_bin->data, p_bin->size);
		uint32_t chunk_count = ceil(p_bin->size / CHUNK_SIZE_FLOAT);

		//printf("Binary file has around %d blocks of 16-byte", chunk_count);
		aes_file_length = chunk_count * CHUNK_SIZE;

		aes_in_data = malloc(sizeof(uint8_t*) * CHUNK_SIZE);
		aes_enc_data = malloc(sizeof(uint8_t*) * aes_file_length);
		aes_out_data = malloc(sizeof(uint8_t*) * CHUNK_SIZE);
		aes_dec_data = malloc(sizeof(uint8_t*) * aes_file_length);

		memset(aes_in_data, 0, CHUNK_SIZE);
		//memcpy(aes_in_data, p_bin->data, p_bin->size);
		memset(aes_enc_data, 0, aes_file_length);

		struct AES_ctx ctx;
		//AES_init_ctx_iv(&ctx, aes_key, aes_iv);

		// do soft encryption for chunk bytes
		for (int j = 0; j < chunk_count; j++)
		{
			for (int i = 0; i < CHUNK_SIZE; i++)
			{
				aes_in_data[i] = p_bin->data[j * CHUNK_SIZE + i];
			}
			AES_init_ctx_iv(&ctx, aes_key, aes_iv);
			AES_CBC_encrypt_buffer(&ctx, aes_in_data, CHUNK_SIZE);
			
			for (int i = 0; i < CHUNK_SIZE; i++)
			{
				aes_enc_data[j * CHUNK_SIZE + i] = aes_in_data[i];
			}
		}

		

		//aes_out_data = (uint8_t*)malloc(sizeof(uint8_t) * p_bin->size);
		//AES_CBC_encrypt_buffer(aes_out_data, aes_in_data, aes_file_length, aes_key, aes_iv);

		ret |= write_file("puld_aes_encrypt.bin", aes_enc_data, aes_file_length);
		
		// Do a decryption for bhuck bytes 
		//AES_init_ctx_iv(&ctx, aes_key, aes_iv);

		for (int j = 0; j < chunk_count; j++)
		{
			for (int i = 0; i < CHUNK_SIZE; i++)
			{
				aes_out_data[i] = aes_enc_data[j * CHUNK_SIZE + i];
			}
			AES_init_ctx_iv(&ctx, aes_key, aes_iv);
			AES_CBC_decrypt_buffer(&ctx, aes_out_data, CHUNK_SIZE);
			for (int i = 0; i < CHUNK_SIZE; i++)
			{
				aes_dec_data[j * CHUNK_SIZE + i] = aes_out_data[i];
			}
		}

		
		//AES_CBC_decrypt_buffer(aes_decrypt_data, aes_out_data, aes_file_length, aes_key, aes_iv);

		ret |= write_file("puld_aes_decrypt.bin", aes_dec_data, aes_file_length);

		/* repeat enc/dec multiple times
		printf("Original data is:\r\n");
		for (int i = 0; i < 128; i++)
		{
			printf("%02X", aes_in_data[i]);
		}
		printf("\r\n========================\r\n");
		
		uint8_t aes_test_out_1[65];
		uint8_t in[64];
		memcpy(in, aes_in_data, 64);
		//AES_CBC_encrypt_buffer(aes_test_out_1, aes_in_data, 64, aes_key, aes_iv);
		struct AES_ctx ctx;
		AES_init_ctx_iv(&ctx, aes_key, aes_iv);
		AES_CBC_encrypt_buffer(&ctx, in, 64);
		printf("1st time Encrypt 1st 64 bytes of data:\r\n");
		for (int i = 0; i < 64; i++)
		{
			printf("%02X", in[i]);
		}
		printf("\r\n-------------------------\r\n");
		uint8_t aes_test_in[65];
		AES_init_ctx_iv(&ctx, aes_key, aes_iv);
		AES_CBC_decrypt_buffer(&ctx, in, 64);
		//AES_CBC_decrypt_buffer(aes_test_in, aes_test_out_1, 64, aes_key, aes_iv);
		printf("1st time Decrypt 1st 64 bytes of encrypted data:\r\n");
		for (int i = 0; i < 64; i++)
		{
			printf("%02X", in[i]);
		}
		
		printf("\r\n========================\r\n");
		
		uint8_t aes_test_out_2[64];
		//AES_CBC_encrypt_buffer(aes_test_out_2, aes_in_data, 64, aes_key, aes_iv);
		AES_init_ctx_iv(&ctx, aes_key, aes_iv);
		AES_CBC_encrypt_buffer(&ctx, in, 64);
		printf("2nd Time Encrypt 64 bytes of data:\r\n");
		for (int i = 0; i < 64; i++)
		{
			printf("%02X", in[i]);
		}
		printf("\r\n-------------------------\r\n");
		uint8_t aes_test_in_2[64];
		//AES_CBC_decrypt_buffer(aes_test_in_2, aes_test_out_2, 64, aes_key, aes_iv);
		AES_init_ctx_iv(&ctx, aes_key, aes_iv);
		AES_CBC_decrypt_buffer(&ctx, in, 64);
		printf("2nd time Decrypt the 1st 64 bytes of encrypted data:\r\n");
		for (int i = 0; i < 64; i++)
		{
			printf("%02X", in[i]);
		}

		printf("\r\n========================\r\n");

		uint8_t aes_test_out_3[64];
		//AES_CBC_encrypt_buffer(aes_test_out_3, aes_in_data, 64, aes_key, aes_iv);
		AES_init_ctx_iv(&ctx, aes_key, aes_iv);
		AES_CBC_encrypt_buffer(&ctx, in, 64);
		printf("3rd time Encrypt 1st 64 bytes of data:\r\n");
		for (int i = 0; i < 64; i++)
		{
			printf("%02X", in[i]);
		}
		printf("\r\n-------------------------\r\n");
		uint8_t aes_test_in_3[64];
		//AES_CBC_decrypt_buffer(aes_test_in_3, aes_test_out_3, 64, aes_key, aes_iv);
		AES_init_ctx_iv(&ctx, aes_key, aes_iv);
		AES_CBC_decrypt_buffer(&ctx, in, 64);
		printf("3rd time Decrypt the 1st 64 bytes of encrypted data:\r\n");
		for (int i = 0; i < 64; i++)
		{
			printf("%02X", in[i]);
		}
		*/

		free(p_bin);
		free(aes_in_data);
		free(aes_out_data);
		free(aes_dec_data);
		free(aes_enc_data);
	}
	else
	{
		printf("No binary file is provided!");
	}

    
    
    return ret;

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
