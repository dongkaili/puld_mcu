@echo off
color 0b
echo.
echo OTA Bin File Installer Command Line Options:
echo    arg[1] == Version... DEBUG or RELEASE
echo    arg[2] == port #... COM{arg[2]}
rem echo    arg[3] == bin file suffix... PULD#{arg[3]}.bin
echo    arg[3] == cipher flag... encipher or decipher
echo.

echo arg[1] = %1
echo arg[2] = %2
echo arg[3] = %3

if "%1%"=="" (
	set fw_version=debug
) else (
	set fw_version=%1%
)
echo fw_version = %fw_version%

rem raw_bin_name can be changed based on debug or release
set raw_bin_name="puld.bin"
set encrypt_bin_name="puld_aes_encrypt.bin"



if "%3%"=="encipher" (
	set cipher_flag=1
	.\binary_aes_cbc256.exe %raw_bin_name%
	set bin_name=%encrypt_bin_name%
) else (
	set cipher_flag=0
	set bin_name=%raw_bin_name%
)
echo cipher_flag = %cipher_flag%

rem set bin_path="C:\Bitbucket\puld-mcu\src\PULD\build\%fw_version%"

echo %bin_path%


.\gen_config_data.exe %bin_name% %cipher_flag%

C:\"Program Files"\7-Zip\7z.exe a -tzip kboot.kfpkg .\flash-list.json .\boot0.bin .\bootloader.bin .\config.bin

if "%cipher_flag%"=="1" (
	python .\ktool.py -b 1500000 kboot.kfpkg
	python .\ktool.py -a 0x80000 -b 1500000 .\\%encrypt_bin_name%

) else (
	python .\ktool.py -b 1500000 kboot.kfpkg
	python .\ktool.py -a 0x80000 -b 1500000 .\\%raw_bin_name%
)


rem if "%2%3%"=="" (
rem 	python .\ktool.py -b 2000000  kboot.kfpkg
rem 	python .\ktool.py -a 0x80000 -b 2000000 %bin_path%\PULD.bin
rem ) else if "%3%"=="" (
rem 	python .\ktool.py -b 2000000  kboot.kfpkg  -p COM%2
rem 	python .\ktool.py -a 0x80000 -b 2000000 %bin_path%\PULD.bin -p COM%2
rem ) else (
rem 	python .\ktool.py -b 2000000  kboot.kfpkg  -p COM%2
rem 	python .\ktool.py -a 0x80000 -b 2000000 %bin_path%\PULD#%3.bin -p COM%2
rem )
