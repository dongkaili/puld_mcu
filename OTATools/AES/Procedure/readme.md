# Steps of load raw binary file or encrypted binary file with OTA bootloader


1. unzip two *.zip files to this local folder, assume the COM 7 is to be used. 
2. copy the original app binary file, e.g., prosonic.bin to this folder
3. run the load_mcu_ota_fw.bat
	e.g., .\load_mcu_ota_fw.bat debug 7 encipher
	this command will encrypt the raw app binary file and load it. The bootloader will decipher it and run. 
	
	e.g., .\load_mcu_ota_fw.bat debug 7 decipher
	this command will directly load the raw app binary file. The bootloader will run it directly. 

4. In order to use OTA to load a new app firmware binary file, put the new binary file to this folder and run
>> .\ota_fw_bin.bat debug 7 decipher
	this command will load the unencrypted binary file into flash

>> .\ota_fw_bin.bat debug 7 encipher
	this command will do encryption for the binary file first, and then load the encrypted binary to the flash
	