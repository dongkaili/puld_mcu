@echo off
echo.
echo OTA Bin File Installer Command Line Options:
echo    arg[1] == Version... DEBUG or RELEASE
echo    arg[2] == port #... COM{arg[2]}
rem echo    arg[3] == bin file suffix... PULD#{arg[3]}.bin
echo    arg[3] == cipher flag... encipher or decipher
echo.

echo arg[1] = %1
echo arg[2] = %2
echo arg[3] = %3

if "%1%"=="" (
	set fw_version=debug
) else (
	set fw_version=%1%
)
echo fw_version = %fw_version%

rem raw_bin_name can be changed based on debug or release
set raw_bin_name="puld.bin"
set encrypt_bin_name="puld_aes_encrypt.bin"
rem set bin_path="C:\Bitbucket\puld-mcu\src\PULD\build\%fw_version%"
set bin_path="c:\src\ota_aes"


if "%3%"=="encipher" (
	set cipher_flag=1
	.\binary_aes_cbc256.exe %raw_bin_name%
	set bin_name=%encrypt_bin_name%
) else (
	set cipher_flag=0
	set bin_name=%raw_bin_name%
)
echo cipher_flag = %cipher_flag%


echo bin_path = %bin_path%
echo bin_name = %bin_name%




if "%cipher_flag%"=="1" (
	python .\send_ota_bin_v3.py -b %bin_path%\\%bin_name%  --mcu 0 --version %fw_version%
	python .\send_ota_bin_v3.py -b %bin_path%\\%bin_name%  --mcu 1 --version %fw_version%
	python .\send_ota_bin_v3.py -b %bin_path%\\%bin_name%  --mcu 2 --version %fw_version%
) else (
	python .\send_ota_bin_v3.py -b %bin_path%\\%bin_name% -c 0 --mcu 0 --version %fw_version%
	python .\send_ota_bin_v3.py -b %bin_path%\\%bin_name% -c 0 --mcu 1 --version %fw_version%
	python .\send_ota_bin_v3.py -b %bin_path%\\%bin_name% -c 0 --mcu 2 --version %fw_version%
)


rem if "%2%3%"=="" (
rem 	python .\ktool.py -b 2000000  kboot.kfpkg
rem 	python .\ktool.py -a 0x80000 -b 2000000 %bin_path%\PULD.bin
rem ) else if "%3%"=="" (
rem 	python .\ktool.py -b 2000000  kboot.kfpkg  -p COM%2
rem 	python .\ktool.py -a 0x80000 -b 2000000 %bin_path%\PULD.bin -p COM%2
rem ) else (
rem 	python .\ktool.py -b 2000000  kboot.kfpkg  -p COM%2
rem 	python .\ktool.py -a 0x80000 -b 2000000 %bin_path%\PULD#%3.bin -p COM%2
rem )
