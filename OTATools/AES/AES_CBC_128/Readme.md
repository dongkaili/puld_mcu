#How to generate executable file for Firmware Binary Encryption
1. Install C compiler, e.g., gcc from MinGW on Windows

2. Run gcc to generate executable file, e.g., binary_aes_cbc128.exe
>> gcc main.c aes_soft.c -o binary_aes_cbc128.exe

3. Put a raw binary file, e.g., prosonic_raw.bin under the same folder with above executable file

4. Run the executable file to generate encrypted binary file
>> binary_aes_cbc128.exe prosonic_raw.bin

5. The generated files will be like
prosonic_aes_encrypt.bin: encrypted binary using cbc_128. 

prosonic_aes_decrypt.bin: original binary file with paddings. The length of these two files are whole number times of 16 bytes. 

6. The other steps of using Gen_config.exe and ktool.kfpkg are based on the prosonic_aes_encrypt.bin file. 
