# -*- coding: utf-8 -*-
"""
This script updates the FRF version # in flash-list.json

@author: Scott Hurst
"""

# main.py
import sys, re

# Args #1: version string 
fname = "flash-list.json"
if __name__ == "__main__":
    for i, arg in enumerate(sys.argv):
        #print(f"Arguments count: {len(sys.argv)}")
        if i==1:
            repl_str = arg

            # https://www.codegrepper.com/code-examples/python/python+find+and+replace+string+in+file
            
            # Read in the file
            with open(fname, 'r') as file :
              filedata = file.read()

            # Replace the target string
            # https://stackoverflow.com/questions/35688126/replace-strings-in-a-file-using-regular-expressions
            # https://docs.python.org/2/library/re.html
            full_repl_str = 'V' + repl_str + '_FRF'
            filedata = re.sub(r'V(.*)_FRF', full_repl_str, filedata)
            
            # Write the file out again
            with open(fname, 'w') as file:
              file.write(filedata)

