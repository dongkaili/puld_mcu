@echo off
echo.

REM Command to run RFRF command to reset as factory version... No need to specify COM port if debug port is >250 and only a single PULD device connected 
python PULD_Config_Commands.py "FRF" # Connects to board and if it is already running F/W it will downgrade to FRF. Ensures subsequent install.
