# Code to show two plots of simulated streamed data. Data for each plot is processed (generated) 
# by separate threads, and passed back to the gui thread for plotting.
# This is an example of using movetothread, which is the correct way of using QThreads

# Michael Hogg, 2015

#import time, sys, re # re no used
import time, sys

for i, arg in enumerate(sys.argv):
    #print(f"Arguments count: {len(sys.argv)}")
    if i==1:
        if arg == "FRF":
            Install_FRF = True
        else:
            Install_FRF = False


from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QIntValidator
from PyQt5.Qt import QMutex

from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from PyQt5.QtWidgets import (
    QLabel,
    QLineEdit,
    QComboBox,
    QCheckBox,
    QPushButton,
)

# https://docs.python.org/3/library/multiprocessing.html
from multiprocessing.connection import Listener
import json

from multiprocessing import Queue
#import queue

# https://github.com/jupyter/qtconsole
from qtconsole.rich_jupyter_widget import RichJupyterWidget
from qtconsole.manager import QtKernelManager

from random import randint
from copy import copy

import serial
from serial.tools.list_ports import comports
from serial.tools import hexlify_codec

import numpy as np
import numpy.ma as ma

display_rate_sample_limit = 1
display_total_sample_limit = 10

enable_cycle_send = False
streaming_enabled = False
stripchart_enabled = False
plotDUTonly = False
process_is_done = True
#enable_send_rbv = False

dbfs_min_init = 0
dbfs_max_init = -120

Data_Stream_Mssg = ""
CMD_RSP_Mssg = ""

current_cycle_cmd = 'i2s_fft_raw' # either i2s_fft_raw or dir_raw, and can add more 

all_mcu_list = [] # a list of all MCU to send to 

all_cmd_list = [] # contain a list of all command to send

class SerialTransmitter(QtCore.QObject):
    
    def __init__(self, serial_instance, parent=None):
        super().__init__(parent)
        self.serial_instance = serial_instance
        
    def send_command_string(self, letter, command_type):
        """Write Micro Jason string to sys.stderr"""
        """
        HH(0x11), VV(Version Byte), TH(0x00: Raw, 0x01:Json), TL(0x00~0x04), Length(4 bytes, LSB first?), checksum, Reserve1, R2, R3, <data>
        
        """
        header    = 0x11
        #print(header.to_bytes(1, 'big'))
        version   = 0x02
        if command_type == 'JSON_Type':
            type_high = 0x01
            type_low  = 0x01
        elif command_type == 'LLC_Type':
            type_high = 0x02
            type_low  = 0x05
        elif command_type == 'Data_Type':
            type_high = 0x00
            type_low  = 0x06
        elif command_type == 'I2S_Raw_Type':
            type_high = 0x00
            type_low  = 0x07
        elif command_type == 'I2S_FFT_Type':
            type_high = 0x00
            type_low =  0x04
        elif command_type == 'RAW_FFT_Type':
            type_high = 0x00
            type_low =  0x08
        elif command_type == 'DIR_RAW_Type':
            type_high = 0x00
            type_low =  0x09
        elif command_type == None:
            if 'i2s_fft_raw' in letter:
                type_high = 0x00
                type_low =  0x08
            elif 'dir_raw' in letter:
                type_high = 0x00
                type_low =  0x09
        
        
            
        length_1  = 0x00
        length_2  = 0x00
        length_3  = 0x00
        length_4  = 0x00
        reserve_1 = 0x00
        reserve_2 = 0x00
        reserve_3 = 0x00
        
        
        if letter == '\x11':  # CTRL+Q
            cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"ENABLE"}')
        elif letter == '\x1A':  # CTRL+Z
            cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"DISABLE"}')
        elif letter == 'RBV' or letter == 'V':
            cmd_string = list('{"CMD": "RBV"}')
        elif letter == 'RBC' or letter == 'C':
            cmd_string = list('{"CMD": "RBC"}')
        elif letter == 'I':
            cmd_string = list('{"CMD": "RID", "MCU":0}')
        elif letter == 'F':
            cmd_string = list('{"CMD": "RFV", "MCU":0}')
        elif letter == 'p':
            cmd_string = list('{"CMD": "RPV"}')
        elif letter == 'v':
            cmd_string = list('version')
        else:
            cmd_string = list(letter)
            
        #print("Cmd: {}".format(cmd_string))
        
        length_1 = len(cmd_string) & 0xFF
        length_2 = (len(cmd_string) >> 8) & 0xFF
        length_3 = (len(cmd_string) >> 16) & 0xFF
        length_4 = (len(cmd_string) >> 24) & 0xFF
        
        calculated_checksum = 0
        check_sum = 0
        pay_load = []
        for i in range(0, len(cmd_string)):
            pay_load.append(ord(cmd_string[i]))
            calculated_checksum += pay_load[i]
        
        
        #calculated_checksum = header + version + type_high + type_low + length_1 + length_2 + length_3 + length_4 + reserve_1 + reserve_2 + reserve_3 + reserve_4
        
        calculated_checksum = calculated_checksum & 0xFF
        calculated_checksum = 0xFF - calculated_checksum
        
        check_sum = calculated_checksum
        #print('check_sum is ' + hex(check_sum))
        
        tx_data = []
        tx_data.append(header.to_bytes(1, 'big'))
        tx_data.append(version.to_bytes(1, 'big'))
        tx_data.append(type_high.to_bytes(1, 'big'))
        tx_data.append(type_low.to_bytes(1, 'big'))
        tx_data.append(length_1.to_bytes(1, 'big'))
        tx_data.append(length_2.to_bytes(1, 'big'))
        tx_data.append(length_3.to_bytes(1, 'big'))
        tx_data.append(length_4.to_bytes(1, 'big'))
        tx_data.append(check_sum.to_bytes(1, 'big'))
        tx_data.append(reserve_1.to_bytes(1, 'big'))
        tx_data.append(reserve_2.to_bytes(1, 'big'))
        tx_data.append(reserve_3.to_bytes(1, 'big'))

        
        for item in pay_load:
            tx_data.append(item.to_bytes(1, 'big'))        
               
        for i in range(0, len(tx_data)):
            #print(tx_data[i])
            self.serial_instance.write((tx_data[i]))



class DataReader(QtCore.QObject):

    newData  = QtCore.pyqtSignal(object)
    finished = QtCore.pyqtSignal()
    
    def __init__(self, serial_instance, parent=None):
        super().__init__(parent)
        self.serial = serial_instance
        self.recv_bytes_list = []
        
    def run(self):
        """Long Run task 1 to receive incoming data on serial port"""
        try:
            while True:
                # read all that is there or wait for one byte
                data = self.serial.read(self.serial.in_waiting)
                if data:
                    new_recv_bytes = list(data)
                    #self.recv_bytes_list.append(new_recv_bytes)
                    #self.recv_bytes_list = new_recv_bytes
                    self.newData.emit(new_recv_bytes)
                    #self.newData.emit(self.recv_bytes_list)    
                    
                    
        except serial.SerialException:
            print('serial has exception')
        

class ipcdatareader(QtCore.QObject):
    newdata  = QtCore.pyqtSignal(object)
    finished = QtCore.pyqtSignal()
    CMD_Sent = False

    def __init__(self, parent=None):
        super().__init__(parent)
        self.listener = Listener(('localhost', 6000), authkey=b'secret password')

    def forward_cmd(self, msg):
        """ Forward cmd to serial transmitter 
            Execute "%run LoadCmds.py" on system start and the send() function will be available 
            You can send any text with that function and the following code will determine if it is 
            a JSON command intended for the PULD device or for use by this app
        """
        main.serial_transmitter.send_command_string(msg, 'JSON_Type')

    def run(self):
        """long run task 1 to receive incoming data on ipc link"""
        running = True
        while running:
            try:        
                self.conn = self.listener.accept()
                #print('connection accepted from', self.listener.last_accepted)
                while True:
                    msg = self.conn.recv() # blocking but own thread so OK.... issue with multiple processes requiring access... need to timeout and close connection 
                    main.ui.IPCTestTxtLabel.setText(msg)
                    try:
                        # handle JSON command... could include one to close connection as well
                        # These are one-shot commands so the connection will be closed by the sender after sending
                        json_dict = json.loads(msg)
                        json_keys = json_dict.keys()
                        if "CMD" in json_keys: # is a valid remote command so forward to device
                            # set flag to be cleared by app when RSP is received and then return that back to sender
                            self.CMD_Sent = True    # may be able to do this with FIFO controls but need to distinguish between different processes that are getting access...
                                                    # Maybe have priority access to FIFO... Listener gets priority if it's the caller and
                                                    # .... make all commands come through Listener and update toplevel vars

                            # Send CMD to device
                            self.forward_cmd(msg)

                            # Wait for device response 
                            timer = 250 # 1s
                            while(timer>0):
                                if self.CMD_Sent==False: # RSP received so send back RSP mssg... may not need this if we just use FIFO controls but synching... how?
                                    json_string = main.MSG_FIFO.get(False) # 
                                    main.ui.cmdRspTxtLabel.setText(json_string)
                                    self.conn.send(json_string)
                                    break
                                time.sleep(0.001) # sleep for 1ms
                                timer = timer-1
                            if timer==0 : # we timed out so send error mssg
                                self.conn.send("No response received to " + msg)
                                print("Timeout!")
                                self.conn.close()
                                break
                        else: # otherwise treat like internal command (enable LOG, FILTER, TRANSFORM, etc.)
                                # Could have an internal command to enable all or filtered logging of data stream from device
                                # as well as commands to send text to the log file to capture conditions.
                            self.conn.send('Quick reply!')
                        time.sleep(0.1)
                        self.conn.close()
                        break
                    except:
                        # not a JSON command so report on status bar
                        # also these messages may be executed in batch so use just one connect5ion 
                        #print(">>>" + msg + "<<<")
                        if msg == 'close connection':
                            self.conn.close()
                            break
            except:
                print('ipc has exception')   
                self.conn.close()

    def close(self):
        self.conn.close()
        self.listener.close()
 

class DataSender(QtCore.QObject):

    newData  = QtCore.pyqtSignal(object)
    
    #def __init__(self,parent=None,sizey=100,rangey=[0,100],delay=1000):
    def __init__(self, serial_transmitter, command_list=None, delay=1000, parent=None):
        global all_cmd_list
        QtCore.QObject.__init__(self)
        self.parent = parent
        #self.sizey  = sizey
        #self.rangey = rangey
        self.serial_tx = serial_transmitter
        self.command_list = command_list
        all_cmd_list = command_list
        self.delay  = delay
        self.mutex  = QMutex()        
        #self.y      = [0 for i in range(sizey)]
        self.run    = True    
        self.cmd_index = 0
        
        
    def sendCommand(self):
        global enable_cycle_send, process_is_done, all_cmd_list, all_mcu_list
        while self.run:
            try:
                
                self.mutex.lock()        
                if enable_cycle_send == True and process_is_done == True:
                
                    #self.serial_tx.send_command_string(self.command_list[self.cmd_index], 'DIR_RAW_Type')  
                    self.serial_tx.send_command_string(all_cmd_list[self.cmd_index], None)  
                    
                    process_is_done = False
                    self.newData.emit(all_cmd_list[self.cmd_index])
                    self.cmd_index = self.cmd_index + 1
                    if (self.cmd_index >= len(all_cmd_list)):
                        self.cmd_index = 0
                self.mutex.unlock() 
                
                
                QtCore.QThread.msleep(self.delay)
                
            except: pass
        
# Console
        # Notes: 
        #       https://github.com/jupyter/qtconsole.git
        #       https://qtconsole.readthedocs.io/en/stable/
        #       https://qtconsole.readthedocs.io/en/stable/#embedding-the-qtconsole-in-a-qt-application

        #       https://stackoverflow.com/questions/59731016/non-blocking-ipython-qt-console-in-a-pyqt-application
        #       http://5.9.10.113/53804511/embedded-qtconsole-in-pyqt5-aplication-does-not-works-as-expected
        #       https://www.programcreek.com/python/example/114293/qtconsole.rich_jupyter_widget.RichJupyterWidget
        #       https://python.hotexamples.com/examples/qtconsole.rich_jupyter_widget/RichJupyterWidget/show/python-richjupyterwidget-show-method-examples.html
        #       https://python.hotexamples.com/examples/qtconsole.rich_jupyter_widget/RichJupyterWidget/kernel_client/python-richjupyterwidget-kernel_client-method-examples.html
        #       
        # Fun example
        #       %load https://matplotlib.org/stable/_downloads/3b9ac21ecf6a0b30550b0fb236dcec5a/custom_shaded_3d_surface.py
        #       https://matplotlib.org/stable/gallery/index.html

class ConsoleWidget(RichJupyterWidget):
    
    def __init__(self, customBanner=None, *args, **kwargs):
        """
            Start a kernel, connect to it, and create a RichJupyterWidget to use it
        """
    
        # Unexpected dependencies... Package adjustments needed to resolve
        # https://github.com/spyder-ide/spyder-notebook/issues/145
        # pip uninstall tornado; pip uninstall jupyter-client
        # pip install tornado==4.5.3; pip install jupyter-client==5.2.2
        # This will result in some "ERROR: pip's dependency resolver does not currently take into account..." messages but these don't seem to be an issue
        # We don't seem to have this dependency issue with the alternate method of using the RichJupyterWidget so we might want to revert.
    
        USE_KERNEL = 'python3'
        super(ConsoleWidget, self).__init__(*args, **kwargs)
        if customBanner is not None:
            self.banner = customBanner
        
        self.kernel_manager = kernel_manager = QtKernelManager(kernel_name=USE_KERNEL)
        kernel_manager.start_kernel()
        kernel_manager.kernel.gui = 'qt'
        
        self.kernel_client = kernel_client = self._kernel_manager.client()
        kernel_client.start_channels()
        return
    
    def push_vars(self, variableDict):
        """
        Given a dictionary containing name / value pairs, push those variables
        to the Jupyter console widget
        """
        self.kernel_manager.kernel.shell.push(variableDict)
    
    def clear(self):
        """
        Clears the terminal
        """
        self._control.clear()
        
        # self.kernel_manager
        
    def print_text(self, text):
        """
        Prints some plain text to the console
        """
        self._append_plain_text(text)
        
    def execute_command(self, command):
        """
        Execute a command in the frame of the console widget
        """
        self._execute(command, False)
        
class Ui_MainWindow(object):

    def gen_point_fades(self, num_pts): 
        # Called after the plotTotalCountEdit field is modified or on demand
        # Generates array of symbol transparency values to create aging trail effect
        n_points = num_pts
        num_max = 256
        num_min = 0
        seq_num = []
        seq_num = [int(num_min + i * (num_max-num_min)/num_pts)  for i in range(num_pts)] 
        for DUT in range(2):
            for mcu in range(3):
                col = self.mcu_colors[DUT][mcu] 
                r=QColor.red(col)
                g=QColor.green(col)
                b=QColor.blue(col)
                self.mcu_colors_arr[DUT][mcu] = [QColor(r, g, b, seq_num[i]) for i in range(num_pts)] # whatever color with decreasing transparency
                self.mcu_sizes_arr[DUT][mcu] = [0 + int(25*(seq_num[i]/256)) for i in range(num_pts)]

    def update_plotRateCount(self):
        global display_rate_sample_limit
        temp_str = self.plotRateCountEdit.text()
        if (not temp_str.isnumeric()):
            return()
        display_rate_sample_limit = int(temp_str)

    def update_plotSampleCount(self): # Called when number of sample points is modified... should be in passed in but is same as self.display_total_sample_count
        global display_total_sample_limit
        temp_str = self.plotSampleCountEdit.text()
        if (not temp_str.isnumeric()):
            return()
        num_pts=int(temp_str)
        display_total_sample_limit = num_pts
        self.gen_point_fades(num_pts) # regenerate symbol trails data

class ApplicationWindow(QtGui.QMainWindow):
    
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        
        self.port = None
        self.baudrate = 115200
        self.parity = 'N'
        self.rtscts = False
        self.xonxoff = False
        self.exclusive = True
        
        self.recv_bytes = []
        self.bytes_to_process = []
        self.i2s_sample_frequency = 317382
        
        self.data_length = 0
        self.data_type = 0
        
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setStyleSheet("QMainWindow {background: 'lightGray';}")
        
        try:
            self.port = self.ask_for_port()
        except KeyboardInterrupt:
            self.port = None
        
        
        try:
            self.serial_instance = serial.serial_for_url(
                self.port,
                self.baudrate,
                parity=self.parity,
                rtscts=self.rtscts,
                xonxoff=self.xonxoff,
                do_not_open=True,
                timeout= 8.0)

            if isinstance(self.serial_instance, serial.Serial):
                self.serial_instance.exclusive = self.exclusive

            self.serial_instance.open()
            if self.serial_instance.is_open == True:
                print('port {} is opened successfully!'.format(self.port))           
            else:
                print('Failed to open port {}!'.format(self.port))   
                return
                
            
        except serial.SerialException as e:
            sys.stderr.write('could not open port {!r}: {}\n'.format(self.port, e))
            
        
        self.serial_instance.flushInput()
        
        self.serial_transmitter = SerialTransmitter(self.serial_instance)
        
        """Long-running task in 5 steps."""
        # Step 2: Create a QThread object
        self.thread_read = QtCore.QThread()
        # Step 3: Create a worker object
        self.reader = DataReader(self.serial_instance)
        # Step 4: Move worker to the thread
        self.reader.moveToThread(self.thread_read)
        # Step 5: Connect signals and slots
        self.thread_read.started.connect(self.reader.run)
        self.reader.finished.connect(self.thread_read.quit)
        self.reader.finished.connect(self.reader.deleteLater)
        self.thread_read.finished.connect(self.thread_read.deleteLater)
        self.reader.newData.connect(self.processRecvData)
        # Step 6: Start the thread
        self.thread_read.start()

        
        # https://realpython.com/python-pyqt-qthread/
        """IPC Handler"""
        # create a qthread object
        self.thread_listen = QtCore.QThread()
        # create a worker object
        self.listener = ipcdatareader()
        # move worker to the thread
        self.listener.moveToThread(self.thread_listen)
        # connect signals and slots
        self.thread_listen.started.connect(self.listener.run)
        self.listener.finished.connect(self.thread_listen.quit)
        self.listener.finished.connect(self.listener.deleteLater)
        self.thread_listen.finished.connect(self.thread_listen.deleteLater)
        self.listener.newdata.connect(self.listener.run) # what should this be?
        # step 6: start the thread
        self.thread_listen.start()
        
        self.MSG_FIFO = Queue()
        #self.MSG_FIFO = queue.SimpleQueue()

# updateCharts
        """updateCharts Handler"""
        ## create a qthread object
        #self.thread_listen = QtCore.QThread()
        ## create a worker object
        #self.ChartUpdater = updateCharts()
        ## move worker to the thread
        #self.listener.moveToThread(self.thread_listen)
        ## connect signals and slots
        #self.thread_listen.started.connect(self.listener.run)
        #self.listener.finished.connect(self.thread_listen.quit)
        #self.listener.finished.connect(self.listener.deleteLater)
        #self.thread_listen.finished.connect(self.thread_listen.deleteLater)
        #self.listener.newdata.connect(self.listener.run) # what should this be?
        ## step 6: start the thread
        #self.thread_listen.start()

    def ask_for_port(self):
        """\
        Show a list of ports and ask the user for a choice. To make selection
        easier on systems with long device names, also allow the input of an
        index.
        """
        sys.stderr.write('\n--- Available ports:\n')
        ports = []
        for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
            port_num = int(port[3:])
            if (port_num <250):
                sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
                ports.append(port)
        time.sleep(0.05)
        if (len(ports)==1): # only one suitable port... could query USB device 
            port = ports[0]
            return port
        while True:
            port = input('--- Enter port index or full name: ')
            try:
                index = int(port) - 1
                if not 0 <= index < len(ports):
                    sys.stderr.write('--- Invalid index!\n')
                    continue
            except ValueError:
                pass
            else:
                port = ports[index]
            return port
       
    def processRecvData(self, var):
        received_bytes = copy(var)
        
        if len(received_bytes) > 0:
            if 0x11 != received_bytes[0]:
                self.recv_bytes = self.recv_bytes + received_bytes
            else:                
                self.recv_bytes = received_bytes
            
            #self.recv_bytes = received_bytes
            whole_string = ''.join([chr(elem) for elem in self.recv_bytes])
            #print(whole_string)
            
            if whole_string[-1] == '}' and whole_string.count('{') == whole_string.count('}'):
                #print(whole_string)
            
                start_index = whole_string.find('{')
                end_index = whole_string.find('}',start_index+1)
                start_count = whole_string.count('{', 0, end_index)
                while start_index != -1 and end_index != -1:
                    while start_count > 1:
                        end_index = whole_string.find('}', end_index+1)
                        start_count = start_count - 1
                    json_string = whole_string[start_index:end_index+1]
                    #print(json_string)  # debug
                    try:
                        json_dict = json.loads(json_string) 
                    except:
                        # start_index = whole_string.find('{', end_index+1) # skip current and find next
                        # end_index = whole_string.find('}', end_index+1)
                        # break # invalid json so exit
                        pass
                    json_keys = json_dict.keys()
                    if not all(key in json_keys for key in ["MCU","ANGLE","DBFS","STATE"]):
                        print(json_string)
                        self.MSG_FIFO.put(json_string, False)
                        if main.listener.CMD_Sent == True: # Command sent by remote process...
                            main.listener.CMD_Sent = False # ... so clear the flag and expect the listener to process the mssg
                        elif not self.MSG_FIFO.empty(): # local command (from GUI at present as this is built-in but wilGIO will be anotehr process so remote as well in future)
                                 # local command (from GUI at present as this is built-in but wilGIO will be anotehr process so remote as well in future)
                            self.CMD_RSP_Mssg = self.MSG_FIFO.get(False)
                                #self.ui.cmdRspTxtLabel.setText(self.CMD_RSP_Mssg) # proving FIFO operation
                                
                            
                        #break 
                    else:                    
                        # This is a MCU Data stream object
                        self.Data_Stream_Mssg = json_string
                        self.updateCharts(json_dict)
                    start_index = whole_string.find('{', end_index+1)
                    end_index = whole_string.find('}', end_index+1)
                    start_count = whole_string.count('{', start_index, end_index)


            
    def sendCMD(self, cmd, mcu=-1, state=""):
        has_mcu = 0
        if (mcu!=-1):
            mcu=int(mcu)
            if (mcu in range(4)):
                has_mcu = 1
                
        has_state = 0
        if (state!=""):
            if (state==1):
                state = "ENABLE"
                has_state = 1
            elif (state==0):
                state = "DISABLE"
                has_state = 1
        
        cmd_part1= '{{\"CMD\":\"{0}\"'.format(cmd)
        cmd_part2= ', \"MCU\":{0}'.format(str(mcu)) if has_mcu else ''
        cmd_part3= ', \"STATE\":\"{0}\"'.format(state) if has_state else ''
        cmd_part4='}'
        current_command = cmd_part1 + cmd_part2 + cmd_part3 + cmd_part4
        self.serial_transmitter.send_command_string(current_command, 'JSON_Type')
        #print(current_command )

    def setupMCUs(self):
        print("")
        print("Sending initialization commands. Wait for responses (if preinstalled F/W exists)...")

        self.serial_transmitter.send_command_string('{"CMD":"RST"}', 'JSON_Type')
        time.sleep(2.5)

        self.serial_transmitter.send_command_string('{"CMD":"PWR", "MCU":1, "STATE":"ENABLE"}', 'JSON_Type')
        time.sleep(1)
        self.serial_transmitter.send_command_string('{"CMD":"PWR", "MCU":2, "STATE":"ENABLE"}', 'JSON_Type')
        time.sleep(2)

        self.serial_transmitter.send_command_string('{"CMD":"RBLV","MCU":0}', 'JSON_Type')
        time.sleep(1)
        self.serial_transmitter.send_command_string('{"CMD":"RBLV","MCU":1}', 'JSON_Type')
        time.sleep(1)
        self.serial_transmitter.send_command_string('{"CMD":"RBLV","MCU":2}', 'JSON_Type')
        time.sleep(1)

        self.serial_transmitter.send_command_string('{"CMD":"RFRF","MCU":2}', 'JSON_Type')
        time.sleep(1)
        self.serial_transmitter.send_command_string('{"CMD":"RFRF","MCU":1}', 'JSON_Type')
        time.sleep(5)
        self.serial_transmitter.send_command_string('{"CMD":"RFRF","MCU":0}', 'JSON_Type')
        time.sleep(5)

        print("... done!")
        print("")
        print("")

    def checkMCUs(self):
        print("")
        print("Sending commands to verify installation. Wait for responses...")

        self.serial_transmitter.send_command_string('{"CMD":"RST"}', 'JSON_Type')
        time.sleep(3)

        self.serial_transmitter.send_command_string('{"CMD":"PWR", "MCU":1, "STATE":"ENABLE"}', 'JSON_Type')
        time.sleep(1)
        self.serial_transmitter.send_command_string('{"CMD":"PWR", "MCU":2, "STATE":"ENABLE"}', 'JSON_Type')
        time.sleep(2)

        self.serial_transmitter.send_command_string('{"CMD":"RBLV","MCU":0}', 'JSON_Type')
        time.sleep(1)
        self.serial_transmitter.send_command_string('{"CMD":"RBLV","MCU":1}', 'JSON_Type')
        time.sleep(1)
        self.serial_transmitter.send_command_string('{"CMD":"RBLV","MCU":2}', 'JSON_Type')
        time.sleep(1)

        print("... done!")
        print("")
        print("")

            
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    main = ApplicationWindow()
    main.resize(800, 10)
    if Install_FRF == True:
        main.setWindowTitle("Note if FRF and STAT is reported by all 3 MCU's (not required) then close this window.")
        main.setupMCUs()
    else:
        main.setWindowTitle("Confirm expected F/W is reported by all 3 MCU's then close this window.")
        main.checkMCUs()

    main.show()
    sys.exit(app.exec_())
