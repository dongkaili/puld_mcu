### flash OTA apps and data (boot0.bin, bootloader.bin, config.bin) to MCU:
`./ktool.py -b 2000000 kboot.kfpkg`

`./ktool.py -a 524288 -b 2000000 hello_world1.bin`

### create kboot.kfpkg:
`./genpkg.sh`

### build boot0.bin and bootloader.bin:
`cmake .. -DPROJ=boot0 -DNOSDK=yes -DPROJ_LDS=yes -DTOOLCHAIN=/Users/dli/work/riscv-toolchain/bin && make`

`cmake .. -DPROJ=bootloader -DNOSDK=yes -DPROJ_LDS=yes -DTOOLCHAIN=/Users/dli/work/riscv-toolchain/bin && make`

### create config.bin
`gcc gen_config_data.c`

`./a.out`

## procedure of using ktool.py for K210 bootloading in Windows environment
1. Download ktool.py from github, https://github.com/loboris/ktool

2. boot0.bin and bootloader.bin can be built from source files. For example, 
>> cd prosonic-mcu\src\boot0
>> mkdir build 
>> cd build
>> cmake -G "MinGW Makefiles" -DNOSDK=yes -DPROJ_LDS=yes ../../..
>> make

Please note, there might be some error message when building the boot0 and bootloader project. For example, the lines of code with "COMMAND powershell.exe" have to be commentted out temporarily. The autorevision generation code also needs commented temporarily, like this, 
#add_custom_target(
#	VCS_Info ALL
#	COMMAND bash ../../resources/autorevision.sh -C ../.. -t h >../../VCS_info.h
#)

3. Use the gen_config_data.c to generate config.bin for a binary image, e.g., Prosonic.bin
The Prosonic.bin is the binary image of the application program. 
3.1 In Windows, create a new empty c++ window console project, and copy the gen_config_data.c into the source folder
The content of the gen_config_data.c needs to be updated based on the binary file name. 
3.2 Build the project to generate the exe file, may have to remove some error by using "Disable Specific Warning"
at Properties->C/C++->Advanced 
3.3 In a windows power shell, run the executable file
>>.\Gen_Config.exe
and follow the steps to generate config.bin file

4. Load boot0.bin into flash address 0x0 at baud rate 2M bps and sha256Prefix=True
>> python .\ktool.py -a 0 -b 115200 .\boot0.bin

5. Load bootloader.bin into flash address 0x1000 (4096) at baud rate 2M bps and sha256Prefix=True
>> python .\ktool.py -a 4096 -b 115200 .\bootloader.bin

6. Load config.bin generated in 3.3 into flash address 0x4000 with sha256Prefix=False
>> python .\ktool.py -a 0x4000 -b 115200 .\config.bin --nosha

7. Load the same config.bin into flash address 0x5000 with sha256Prefix=False
>> python .\ktool.py -a 0x5000 -b 115200 .\config.bin --nosha

Please note the step 4 to 7 can be done alternatively in step 8

8. Select boot0.bin, bootloader.bin, config.bin and flash-list.json and right click to open zip program or 7-zip program. 
In 7-zip program, choose the archive format: zip, and rename the output file to be kboot.kfpkg, and click 'OK' button to generate
a kboot.kfpkg file. 
Alternatively, use a command to do this
>> C:'\Program Files'\7-Zip\7z.exe a -tzip kboot.kfpkg .\flash-list.json .\boot0.bin .\bootloader.bin .\config.bin
>> python .\ktool.py -b 2000000 kboot.kfpkg

9. Load the application image to 0x80000 
>> python .\ktool.py -a 0x80000 -b 2000000 Prosonic.bin