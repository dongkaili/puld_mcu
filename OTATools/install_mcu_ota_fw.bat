@echo off
echo.

REM Command to install as factory version... No need to specify COM port if debug port is >250 and only a single PULD device connected 
REM .\install_mcu_ota_fw.bat LEG 0.0.1_FRF\PULD_V0.0.1_FRF ENC REL COM#  <<< All new devices should be installed with V0.0.1_FRF
REM Omit REL and/or ENC to get other version

if "%1%2%3%4%5%"=="" (
	echo OTA Bin File Installer Command Line Options:
	echo args in any order
	echo    arg[1] == Legacy file... LEG followed by {sub-folder}\{base_version_string} without flags in string or filename extension
	echo    arg[2] == Version... REL=release or debug by default
	echo    arg[3] == port #... MUST start with [COM]
	echo    arg[4] == encryption flag... encoded [ENC] or raw binary by default
	echo    arg[5] == binfile suffix... MUST start with [V] as in V0.0.1_FRF to install PULD_V0.0.1_FRF_RELEASE_enc.bin for example
	echo    arg[6] == Legacy Bootloader... LBL... use kboot.kfpkg from legacy_bootloader folder
	echo.
	exit
)

echo arg[1] = %1
echo arg[2] = %2
echo arg[3] = %3
echo arg[4] = %4
echo arg[5] = %5
echo arg[6] = %6
echo.

REM Set defaults
set store=
set port=
set legacy_filename=
set cipher_flag=0
set fw_version=debug
set fw_version_str=_DEBUG
set fw_enc_str=
set binfile_suffix=
set legacy_bootloader=0


REM Call function with 2 args in order. If arg[1]==LEG then arg[2] must be the full filename 
call :SET_Parameters %1
call :SET_Parameters %2
call :SET_Parameters %3
call :SET_Parameters %4
call :SET_Parameters %5
call :SET_Parameters %6

set project=PULD


REM Latest version of FRF is V0.0.1_FRF
REM Initial ~20 units sold have V0.0.0_FRF installed but this does not handle multicase OTA the same as V0.0.1_FRF
REM When testing support for the initial units V0.0.0_FRF must be installed so change the following line as required
set FRF_VER_NUM=0.0.1
set factory_bin_path=..\src\PULD\Legacy_Firmware\%FRF_VER_NUM%_FRF
set factory_bin_fname=PULD_V%FRF_VER_NUM%_FRF_RELEASE_enc.bin


if "%store%"=="legacy" (
	set bin_path=..\src\PULD\Legacy_Firmware
	set bin_name=%legacy_filename%%fw_version_str%%fw_enc_str%.bin
	set fw_version=
	set binfile_suffix=
) else (
    set bin_path=..\src\PULD\build\%fw_version%
	set raw_bin_name=%project%%binfile_suffix%.bin
	set encrypt_bin_name=%project%%binfile_suffix%_enc.bin
)

if NOT "%store%"=="legacy" (
	if %cipher_flag%==1 (
		set bin_name=%encrypt_bin_name%
	) else (
		set bin_name=%raw_bin_name%
	)	
)


echo project = %project%
echo store = %store%
echo port = %port%
echo factory_bin_path = %factory_bin_path%
echo factory_bin_fname = %factory_bin_fname%
echo legacy_filename = %legacy_filename%
echo legacy_bootloader = %legacy_bootloader%
echo cipher_flag = %cipher_flag%
echo fw_version = %fw_version%
echo binfile_suffix = %binfile_suffix%
echo raw_bin_name = %raw_bin_name%
echo encrypt_bin_name = %encrypt_bin_name%
echo.

echo Path to binary: %bin_path%
echo Binary File: %bin_name%

set BL_path=.\AES\Procedure

.\AES\Procedure\gen_config_data.exe %bin_path%\%bin_name% %cipher_flag%

REM https://sevenzip.osdn.jp/chm/cmdline/syntax.htm
REM Set path to 7z in env

python update_flash-list.py %FRF_VER_NUM% # modifies flash-list.json to install the FRF identified by FRF_VER_NUM
python PULD_Config_Commands.py "FRF" # Connects to board and if it is already running F/W it will downgrade to FRF. Ensures subsequent install.

del kboot.kfpkg
if %legacy_bootloader%==0 (
	echo. NOTE: Installing new bootloader [supports encrypted binaries and RFRF command]
    7z.exe a -tzip kboot.kfpkg .\flash-list.json %BL_path%\boot0.bin %BL_path%\bootloader.bin .\config.bin %factory_bin_path%\%factory_bin_fname%
) else (
	echo. NOTE: Installing legacy bootloader [no support for encrypted binaries or RFRF command]
	7z.exe a -tzip kboot.kfpkg %BL_path%\legacy_bootloader\flash-list.json %BL_path%\boot0.bin %BL_path%\legacy_bootloader\bootloader.bin .\config.bin
)
python ktool.py -b 2000000  kboot.kfpkg  %port%
echo.
echo Programming %bin_name%
echo.
python ktool.py -a 0x80000 -b 2000000 %bin_path%\%bin_name% %port%

python PULD_Config_Commands.py # Connects to and displays installed F/W info

exit


:SET_Parameters
	set string=%1%
	if "%1%"=="REL" (
		set fw_version=release
		set fw_version_str=_RELEASE
	) else if "%1%"=="ENC" (
		set cipher_flag=1
		set fw_enc_str=_enc
	) else if "%string:~0,1%"=="V" (
		set binfile_suffix=_%string%
	) else if "%string:~0,3%"=="COM" ( 
		set port=-p %1%
	) else if "%1%"=="LEG" (
		set store=legacy
	) else if "%1%"=="LBL" (
		set legacy_bootloader=1
	) else if NOT "%1%"=="" (
		set legacy_filename=%1%
	)
