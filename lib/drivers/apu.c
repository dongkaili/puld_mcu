/* Copyright 2018 Canaan Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "syscalls.h"
#include "sysctl.h"
#include "apu.h"

#define BEAFORMING_BASE_ADDR    (0x50250200)
#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

//#define __dbgopt__ __attribute__((optimize(0)))
#define __dbgopt__

volatile apu_reg_t *const apu = (volatile apu_reg_t *)BEAFORMING_BASE_ADDR;

/*
APU Gain Register from RFM...
    Audio data gain factor. Used to adjust the energy of the audio signal output by the channel mixing and mixing adder. 11-bit fixed point number, low 10-bit is the fractional part. 
        Max: 111_1111_1111 = 7FF = 7FF/4FF = 1.999023438
        Min: 000_0000_0001 = 7FF = 001/4FF = 0.000976563
*/
void apu_set_audio_gain(uint16_t gain)
{
    apu_ch_cfg_t ch_cfg = apu->bf_ch_cfg_reg;

    ch_cfg.we_bf_target_dir = 0;
    ch_cfg.we_bf_sound_ch_en = 0;
    ch_cfg.we_data_src_mode = 0;
    ch_cfg.we_audio_gain = 1;
    ch_cfg.audio_gain = gain;
    apu->bf_ch_cfg_reg = ch_cfg;
}

uint16_t apu_get_audio_gain()
{
    apu_ch_cfg_t tmp = apu->bf_ch_cfg_reg;
    return tmp.audio_gain;
}


/*
Voice strength average value right shift factor.  When performing sound direction detect,
the average value of samples from different channels is required, this right shift factor
is used to perform division.
0x0: no right shift;               0x1: right shift by 1-bit;
 . . . . . .
0xF: right shift by 15-bit.
*/

/*set sampling shift*/
void apu_set_smpl_shift(uint8_t smpl_shift)
{
    apu_dwsz_cfg_t tmp = apu->bf_dwsz_cfg_reg;

    tmp.smpl_shift_bits = smpl_shift;
    apu->bf_dwsz_cfg_reg = tmp;
}

/*get sampling shift*/
uint8_t apu_get_smpl_shift(void)
{
    apu_dwsz_cfg_t tmp = apu->bf_dwsz_cfg_reg;

    return tmp.smpl_shift_bits;
}

/*
 * APU unit sound channel enable control bits.  Bit 'x' corresponds to enable bit for sound
 * channel 'x' (x = 0, 1, 2, . . ., 7).  APU sound channels are related with I2S host RX channels.
 * APU sound channel 0/1 correspond to the left/right channel of I2S RX0; APU channel 2/3 correspond
 * to left/right channels of I2S RX1; and things like that.  Software write '1' to enable a sound
 * channel and hardware automatically clear the bit after the sample buffers used for direction
 * searching is filled full.
 * 0x1: writing '1' to enable the corresponding APU sound channel.
 */
void apu_set_channel_enabled(uint8_t channel_bit)
{
    apu_ch_cfg_t ch_cfg;

    ch_cfg.we_audio_gain = 0;
    ch_cfg.we_bf_target_dir = 0;
    ch_cfg.we_bf_sound_ch_en = 1;
    ch_cfg.bf_sound_ch_en = channel_bit;
    apu->bf_ch_cfg_reg = ch_cfg;
}

/*
 * APU unit sound channel enable control bits.  Bit 'x' corresponds to enable bit for sound
 * channel 'x' (x = 0, 1, 2, . . ., 7).  APU sound channels are related with I2S host RX channels.
 * APU sound channel 0/1 correspond to the left/right channel of I2S RX0; APU channel 2/3 correspond
 * to left/right channels of I2S RX1; and things like that.  Software write '1' to enable a sound
 * channel and hardware automatically clear the bit after the sample buffers used for direction
 * searching is filled full.
 * 0x1: writing '1' to enable the corresponding APU sound channel.
 */
void apu_channel_enable(uint8_t channel_bit)
{
    apu_ch_cfg_t ch_cfg = apu->bf_ch_cfg_reg;

    ch_cfg.we_audio_gain = 0;
    ch_cfg.we_bf_target_dir = 0;
    ch_cfg.we_data_src_mode = 0;
    ch_cfg.we_bf_sound_ch_en = 1;
    ch_cfg.bf_sound_ch_en = channel_bit;
    apu->bf_ch_cfg_reg = ch_cfg;
}

/*
 *  audio data source configure parameter.  This parameter controls where the audio data source comes from.
 *  0x0: audio data directly sourcing from apu internal buffer;
 *  0x1: audio data sourcing from FFT result buffer.
 */
void apu_set_src_mode(uint8_t src_mode)
{
    apu_ch_cfg_t ch_cfg = apu->bf_ch_cfg_reg;

    ch_cfg.we_audio_gain = 0;
    ch_cfg.we_bf_target_dir = 0;
    ch_cfg.we_bf_sound_ch_en = 0;
    ch_cfg.we_data_src_mode = 1;
    ch_cfg.data_src_mode = src_mode;
    apu->bf_ch_cfg_reg = ch_cfg;
}

void apu_set_direction_delay(uint8_t dir_num, uint8_t *dir_bidx) // could set a global var to indicate special setup with 0x3f's in most places
{
  apu->bf_dir_bidx[dir_num][0] = (apu_dir_bidx_t)
  {
    .dir_rd_idx0 = dir_bidx[0],
    .dir_rd_idx1 = dir_bidx[1],
    .dir_rd_idx2 = dir_bidx[2],
    .dir_rd_idx3 = dir_bidx[3]
  };
  apu->bf_dir_bidx[dir_num][1] = (apu_dir_bidx_t)
  {
    .dir_rd_idx0 = dir_bidx[4],
    .dir_rd_idx1 = dir_bidx[5],
    .dir_rd_idx2 = dir_bidx[6],
    .dir_rd_idx3 = dir_bidx[7]
  };
}

/*
 * i2s_fs: I2S sampling frequency, radius:  , FOV: the angle of the scanning "cone", n_mic: the num of mics in UCA
 */

double to_radians(double angle)
{    
    return angle * M_PI / 180.0;
};


/***************************************************************************************************************  
 * Calculates the DIR delay to test for a signal arriving from local direction angles DOA_AZM & DOA_INC   
 * The two angles may be fractional but must adhere to the following rules:  
 *   1) -180 <= DOA_AZM <= 180 where DIR[0]  ==    0deg @mic0; +ve X 
 *                                  DIR[4]  ==  +90deg @mic2; +ve Y 
 *                                  DIR[8]  == ±180deg @mic4; -ve X 
 *                                  DIR[12] ==  -90deg @mic6; -ve Y 
 *   2)  -90 <= DOA_INC <=  90 where DIR[0]  ==   90deg pointing to device center 
 *                                  DIR[8]  ==    0deg is centered above array,  
 *                                  DIR[15] ==  -90deg pointing to source (away from device center) 
 *                                   
 * The formula calculates the overall fractional delay using the normalized delay from the center of the array to  
 * the center of the mic where normalized_delay = (radius / cm_tick) and cm_tick = SOUND_SPEED * 100 / i2s_fs. 
 *  
 * The delay is calculated in steps that will be explained here. 
 * #1: The angle from the center of the array to the mic is determined first as a value from 0-360deg 
 *     and then corrected to the ±180deg space by subtracting 180 
 * #2: The angle between the mic and DOA_AZM is determined by differencing the two values  
 *         ex#1: mic0 is in the direction of AZM=0deg so if DOA_AZM ==0 then diff ==0 
 *         ex#2: mic2 is in the direction of AZM=90deg so if DOA_AZM ==25 then diff ==65 
 * #3: Calculate the delay... 
 *     There are two parts to this formula which involve angles 
 *     a) The term (1 - cos(to_radians(angle_diff))) reveals that minimum delay occurs for cos(angle_diff) = 1  
 *        or at 0deg. This makes sense intuitively since if the angle between the mic and the DOA is zero it  
 *        must be that this mic is closest to the source. Likewise the maximum delay occurs for cos(angle_diff)  
 *        = -1,  which is at ±180deg to this mic. This occurs when the DOA is on the opposite side of the array 
 *        so the signal will pass all of the other mics before reaching this one. 
 *     b) The term sin(to_radians(DOA_INC)) is a correction for the incident angle, which is related to the  
 *        Field of View (FOV) of the device. We know that for a given set of delays the device will have optimial  
 *        performance within a specific range of angles.... which would be the intersection of two concentric  
 *        cones. For DOA_INC = 0 the signal approaches from directly above the array so it reaches all mics 
 *        at the same time. sin(0) == 0 so the correction in this case makes all delays zero. For DOA_INC = 90deg 
 *        the source wave is approach in a plane that is perpendicular to the face of the receiver, so the  
 *        distance travelled will be maximum. sin(90)==1 so this would maximize the delay. Intermediate angles 
 *        determine corrections within these limits. 
 ***************************************************************************************************************/  
double apu_calc_single_delay(uint8_t n_mic, int mic, double normalized_delay, double DOA_AZM, double DOA_INC) 
{ 
    double angle_diff, angle_to_mic; 
    double delay; 
    angle_to_mic = 360.0*(double)mic / n_mic - 180.0;                                          // step #1 
    angle_diff = angle_to_mic - DOA_AZM;                                                       // step #2 
    delay = normalized_delay * (1 - cos(to_radians(angle_diff))) * sin(to_radians(DOA_INC));   // step #3: 
    //printf("apu_calc_single_delay[mic=%d][DOA_AZM=%5.1f][DOA_INC=%5.1f]: delay = %f\n", mic, DOA_AZM, DOA_INC, delay); 
    return delay; 
} 

/******************************************************************************************************
 * Builds normal Delay Array for azimuth determination
 * FOV is  float val in range 0 < FOV < 180 although 0 isn't valid and 180 is out of operating range
 *******************************************************************************************************/
void apu_calc_delays_azimuth(uint32_t i2s_fs, float radius, uint8_t n_mic, float FOV, uint8_t delays[16][8])
{
    int mic, dir;
    double cm_tick = (double)SOUND_SPEED * 100 / (double)i2s_fs; /*distance per tick (cm)*/
    double normalized_delay =  ((double)radius / cm_tick);
    double min_delay;
    double frac_delays[8]; 
    
    //printf("BF[AZM]...\n");
    for (dir = 0; dir < DIRECTION_RES; dir++)
    {
        min_delay = 999.9;
        for (mic = 0; mic < n_mic; mic++)
        {
            double dir_frac = ((double)dir / DIRECTION_RES); // 0 <= dir_frac <= 1
            double DOA_AZM = 360.0*dir_frac - 180.0; 
            double DOA_INC = (double)FOV / 2;  // 90.0- 
            frac_delays[mic] = apu_calc_single_delay(n_mic, mic, normalized_delay, DOA_AZM, DOA_INC); 

            if (frac_delays[mic] < min_delay) // remember minimum for normalization if necessary
                min_delay = frac_delays[mic];
        }
        for (mic = 0; mic < n_mic; mic++)
        {
            delays[dir][mic] = (int)(frac_delays[mic] - min_delay + 0.5);
            //printf("AZM: dir=%d, mic=%d, frac_delay = %f, min_delay = %f, delay = %d\n", dir, mic, frac_delays[mic], min_delay, delays[dir][mic]);
        }
    }
}
    
/********************************************************************************
 * Calculated DIR from 0-15 as a double value (0 <= DIR < 16.0)
 * This is used in apu_calc_delays_incident
 *********************************************************************************/
double to_DIR(double angle)
{
    return (1 + (angle / 180.0)) * 8.0; // 0 <= DIR < 16.0
}

/********************************************************************************
 * Builds Delay Array for incident test as per dev plan 8.2.4
 * Delay Array may be used by HBF or SBF
 * Azimuth passed to function is angle determined by HBF output (±180°)
 * 
 * The table could be optimized since thare are regions that have useless delays 
 * (incident = 90 for ex) See comments below
 *********************************************************************************/
void apu_calc_delays_incident(uint32_t i2s_fs, float radius, uint8_t n_mic, float azimuth, uint8_t delays[16][8])
{
    int mic, dir;
    double cm_tick = (double)SOUND_SPEED * 100 / (double)i2s_fs; /*distance per tick (cm)*/
    double normalized_delay =  ((double)radius / cm_tick);
    double min_delay;
    double frac_delays[8];
        
    /*********************************************************************************************************************
     The outer loop could cycles through entries in a LUT so we can purposely assign angles to each DIRection  
     We could have the program search for the 1st interesting DIR from either end (FOV==0 to FOV==180)
     and set as limits the point when the table row would be different
     So from FOV==0... Search for increasing angles until 1st all non-zero row is hit
     From FOV==180 for decreasing angles until we get a change of value in the table and then remember the last entry
     Can determine that the table has changed bu summing the values or...?
     **********************************************************************************************************************/
    
    // These define the FOV limits for the outer loop. 
    // Float vars but search in whole degree increments is fine
    double  dir_lut_inc[16] = { 0 };      // make global in this module or static with global funcs to access
    dir_lut_inc[0] = -90; 
    dir_lut_inc[15] = 90; 
    for (int dir = 1; dir < DIRECTION_RES-1; dir++) // ends are set elsewhere
        dir_lut_inc[dir] = ((double)dir * (double)(dir_lut_inc[15] - dir_lut_inc[0]) / DIRECTION_RES) + dir_lut_inc[0];  // should carve up -90 to +90 into 16 pieces

    // build the table
    //printf("BF[INC]...\n");
    for(dir = 0 ; dir < DIRECTION_RES ; dir++)
    {           
        min_delay = 999.9;
        for (mic = 0; mic < n_mic; mic++)
        {
            frac_delays[mic] = apu_calc_single_delay(n_mic, mic, normalized_delay, azimuth, dir_lut_inc[dir]);
            if(frac_delays[mic] < min_delay) // remember minimum for normalization if necessary
                min_delay = frac_delays[mic];
        }
        for (mic = 0; mic < n_mic; mic++)
        {
            delays[dir][mic] = (int)(frac_delays[mic] - min_delay + 0.5);
            //printf("INC: dir=%d, mic=%d, frac_delay = %f, min_delay = %f, delay = %d\n", dir, mic, frac_delays[mic], min_delay, delays[dir][mic]);
        }
    }
}

void apu_get_delays(uint8_t DelayArray[16][8])
{
    
    for(int i = 0 ; i < 16 ; i++) {
        apu_dir_bidx_t bidx0 = apu->bf_dir_bidx[i][0];
        apu_dir_bidx_t bidx1 = apu->bf_dir_bidx[i][1];
            
        DelayArray[i][0] = bidx0.dir_rd_idx0;
        DelayArray[i][1] = bidx0.dir_rd_idx1;
        DelayArray[i][2] = bidx0.dir_rd_idx2;
        DelayArray[i][3] = bidx0.dir_rd_idx3;
        DelayArray[i][4] = bidx1.dir_rd_idx0;
        DelayArray[i][5] = bidx1.dir_rd_idx1;
        DelayArray[i][6] = bidx1.dir_rd_idx2;
        DelayArray[i][7] = bidx1.dir_rd_idx3;
    }
}

void apu_get_delay_str(uint8_t DelayArray[16][8], char DelayArrayStr[])
{
    strcat(DelayArrayStr, "[");  // start with the outer enclosing brace
    for(int i = 0 ; i < 16 ; i++) {
        char tempj[5];                              
        char tempi[3 * 8 - 1 + 2 + 1] = { '[' };    // full string is 8x3 (2 characters + comma) = 24 + braces [2] ... 1 extra comma as NULL
        
        for(int j = 0 ; j < 8 ; j++) 
        {
            sprintf(tempj, "%2d%1s", DelayArray[i][j], j < 7 ? "," : "");
            strcat(tempi, tempj);
        }
        strcat(tempi, i < 15 ? "]," : "]");
        strcat(DelayArrayStr, tempi);    
    }
    strcat(DelayArrayStr, "]");
}


void apu_set_delays(uint32_t i2s_fs, float radius, uint8_t n_mic, float FOV)
{
    // https://www.cs.swarthmore.edu/~newhall/unixhelp/C_arrays.html
    uint8_t Delays[16][8];
    apu_calc_delays_azimuth(i2s_fs, radius, n_mic, FOV, Delays);
    for (size_t i = 0; i < DIRECTION_RES; i++)
    {
        apu_set_direction_delay(i, Delays[i]);
    }
//    char DelayArrayStr[1024] = "";
//    apu_get_delay_str(Delays, DelayArrayStr);
//    printf("BF[AZM]: %s\n\n", DelayArrayStr);

}

void apu_set_delays_incident(uint32_t i2s_fs, float radius, uint8_t n_mic, float AZM)
{
    // https://www.cs.swarthmore.edu/~newhall/unixhelp/C_arrays.html
    uint8_t Delays[16][8];
    apu_calc_delays_incident(i2s_fs, radius, n_mic, AZM, Delays);
    for (size_t i = 0; i < DIRECTION_RES; i++)
    {
        apu_set_direction_delay(i, Delays[i]);
    }
//    char DelayArrayStr[1024] = "";
//    apu_get_delay_str(Delays, DelayArrayStr);
//    printf("BF[INC]: %s\n\n", DelayArrayStr);
}

/*
 Sound direction searching enable bit.  Software writes '1' to start sound direction searching function.
 When all the sound sample buffers are filled full, this bit is cleared by hardware (this sample buffers
 are used for direction detect only).
 0x1: enable direction searching.
 */
void apu_dir_enable(void)
{
    apu_ctl_t bf_en_tmp = apu->bf_ctl_reg;

    bf_en_tmp.we_bf_dir_search_en = 1;
    bf_en_tmp.bf_dir_search_en = 1;
    apu->bf_ctl_reg = bf_en_tmp;
}

void apu_dir_reset(void)
{
    apu_ctl_t bf_en_tmp = apu->bf_ctl_reg;

    bf_en_tmp.we_search_path_rst = 1;
    bf_en_tmp.search_path_reset = 1;
    apu->bf_ctl_reg = bf_en_tmp;
}

/*
 Valid voice sample stream generation enable bit.  After sound direction searching is done, software can
 configure this bit to generate a stream of voice samples for voice recognition.
 0x1: enable output of voice sample stream.
 0x0: stop the voice samlpe stream output.
 */
void apu_voc_enable(uint8_t enable_flag)
{
    apu_ctl_t bf_en_tmp = apu->bf_ctl_reg;

    bf_en_tmp.we_bf_stream_gen = 1;
    bf_en_tmp.bf_stream_gen_en = enable_flag;
    apu->bf_ctl_reg = bf_en_tmp;
}

void apu_voc_reset(void)
{
    apu_ctl_t bf_en_tmp = apu->bf_ctl_reg;

    bf_en_tmp.we_voice_gen_path_rst = 1;
    bf_en_tmp.voice_gen_path_reset = 1;
    apu->bf_ctl_reg = bf_en_tmp;
}

/*
Target direction select for valid voice output.  When the source voice direaction searching
is done, software can use this field to select one from 16 sound directions for the following
voice recognition
0x0: select sound direction 0;   0x1: select sound direction 1;
 . . . . . .
0xF: select sound direction 15.
*/
void apu_voc_set_direction(en_bf_dir_t direction)
{
    apu_ch_cfg_t ch_cfg = apu->bf_ch_cfg_reg;

    ch_cfg.we_bf_sound_ch_en = 0;
    ch_cfg.we_audio_gain = 0;
    ch_cfg.we_data_src_mode = 0;
    ch_cfg.we_bf_target_dir = 1;
    ch_cfg.bf_target_dir = direction;
    apu->bf_ch_cfg_reg = ch_cfg;

    apu_ctl_t bf_en_tmp = apu->bf_ctl_reg;

    bf_en_tmp.we_update_voice_dir = 1;
    bf_en_tmp.update_voice_dir = 1;
    apu->bf_ctl_reg = bf_en_tmp;
}

/*
 *I2S host beam-forming Filter FIR16 Coefficient Register
 */
void apu_dir_set_prev_fir(uint16_t *fir_coef)
{
    uint8_t i = 0;

    for (i = 0; i < 9; i++) {
        apu->bf_pre_fir0_coef[i] =
        (apu_fir_coef_t){
            .fir_tap0 = fir_coef[i * 2],
            .fir_tap1 = i == 8 ? 0 : fir_coef[i * 2 + 1]
        };
    }
}

void apu_dir_set_post_fir(uint16_t *fir_coef)
{
    uint8_t i = 0;

    for (i = 0; i < 9; i++) {
        apu->bf_post_fir0_coef[i] =
        (apu_fir_coef_t){
            .fir_tap0 = fir_coef[i * 2],
            .fir_tap1 = i == 8 ? 0 : fir_coef[i * 2 + 1]
        };
    }
}

void apu_voc_set_prev_fir(uint16_t *fir_coef)
{
    uint8_t i = 0;

    for (i = 0; i < 9; i++) {
        apu->bf_pre_fir1_coef[i] =
        (apu_fir_coef_t){
            .fir_tap0 = fir_coef[i * 2],
            .fir_tap1 = i == 8 ? 0 : fir_coef[i * 2 + 1]
        };
    }
}

void apu_voc_set_post_fir(uint16_t *fir_coef)
{
    uint8_t i = 0;

    for (i = 0; i < 9; i++) {
        apu->bf_post_fir1_coef[i] =
        (apu_fir_coef_t){
            .fir_tap0 = fir_coef[i * 2],
            .fir_tap1 = i == 8 ? 0 : fir_coef[i * 2 + 1]
        };
    }
}

void apu_set_fft_shift_factor(uint8_t enable_flag, uint16_t shift_factor)
{
    apu->bf_fft_cfg_reg =
    (apu_fft_cfg_t){
        .fft_enable = enable_flag,
        .fft_shift_factor = shift_factor
    };

    apu_ch_cfg_t ch_cfg = apu->bf_ch_cfg_reg;

    ch_cfg.we_data_src_mode = 1;
    ch_cfg.data_src_mode = enable_flag;
    apu->bf_ch_cfg_reg = ch_cfg;
}

void apu_dir_set_down_size(uint8_t dir_dwn_size)
{
    apu_dwsz_cfg_t tmp = apu->bf_dwsz_cfg_reg;

    tmp.dir_dwn_siz_rate = dir_dwn_size;
    apu->bf_dwsz_cfg_reg = tmp;
}

void apu_dir_set_interrupt_mask(uint8_t dir_int_mask)
{
    apu_int_mask_t tmp = apu->bf_int_mask_reg;

    tmp.dir_data_rdy_msk = dir_int_mask;
    apu->bf_int_mask_reg = tmp;
}

void apu_voc_set_down_size(uint8_t voc_dwn_size)
{
    apu_dwsz_cfg_t tmp = apu->bf_dwsz_cfg_reg;

    tmp.voc_dwn_siz_rate = voc_dwn_size;
    apu->bf_dwsz_cfg_reg = tmp;
}

void apu_voc_set_interrupt_mask(uint8_t voc_int_mask)
{
    apu_int_mask_t tmp = apu->bf_int_mask_reg;

    tmp.voc_buf_rdy_msk = voc_int_mask;
    apu->bf_int_mask_reg = tmp;
}

void apu_set_down_size(uint8_t dir_dwn_size, uint8_t voc_dwn_size)
{
    apu_dwsz_cfg_t tmp = apu->bf_dwsz_cfg_reg;

    tmp.dir_dwn_siz_rate = dir_dwn_size;
    tmp.voc_dwn_siz_rate = voc_dwn_size;
    apu->bf_dwsz_cfg_reg = tmp;
}

void apu_set_interrupt_mask(uint8_t dir_int_mask, uint8_t voc_int_mask)
{
    apu->bf_int_mask_reg =
    (apu_int_mask_t){
        .dir_data_rdy_msk = dir_int_mask,
        .voc_buf_rdy_msk = voc_int_mask
    };
}

void apu_dir_clear_int_state(void)
{
    apu->bf_int_stat_reg =
    (apu_int_stat_t){
        .dir_search_data_rdy = 1
    };
}

void apu_voc_clear_int_state(void)
{
    apu->bf_int_stat_reg =
    (apu_int_stat_t){
        .voc_buf_data_rdy = 1
    };
}

/* reset saturation_counter */
void apu_voc_reset_saturation_counter(void)
{
    apu->saturation_counter = 1<<31;
}

/*get saturation counter*/
/*high 16 bit is counter, low 16 bit is total.*/
uint16_t apu_voc_get_saturation_count(void)
{
    return apu_voc_get_saturation_counter()>>16 & 0X7FFF;
}

uint16_t apu_voc_get_saturation_sample_count(void)
{
    return apu_voc_get_saturation_counter() & 0XFFFF;
}

uint32_t apu_voc_get_saturation_counter(void)
{
    return apu->saturation_counter;
}

/*set saturation limit*/
void apu_voc_set_saturation_limit(uint16_t upper, uint16_t bottom)
{
    apu->saturation_limits = (uint32_t)bottom<<16 | upper;
}

/*get saturation limit*/
/*high 16 bit is counter, low 16 bit is total.*/
uint32_t apu_voc_get_saturation_limit(void)
{
    return apu->saturation_limits;
}

static void print_fir(const char *member_name, volatile apu_fir_coef_t *pfir)
{
    printf("  for(int i = 0; i < 9; i++){\n\r");
    for (int i = 0; i < 9; i++) {
        apu_fir_coef_t fir = pfir[i];

        printf("    apu->%s[%d] = (apu_fir_coef_t){\n\r", member_name, i);
        printf("      .fir_tap0 = 0x%x,\n\r", fir.fir_tap0);
        printf("      .fir_tap1 = 0x%x\n\r", fir.fir_tap1);
        printf("    };\n\r");
    }
    printf("  }\n\r");
}

void apu_print_setting(void)
{
    printf("void apu_setting(void) {\n\r");
    apu_ch_cfg_t bf_ch_cfg_reg = apu->bf_ch_cfg_reg;

    printf("  apu->bf_ch_cfg_reg = (apu_ch_cfg_t){\n\r");
    printf("    .we_audio_gain = 1, .we_bf_target_dir = 1, .we_bf_sound_ch_en = 1,\n\r");
    printf("    .audio_gain = 0x%x, .bf_target_dir = %d, .bf_sound_ch_en = %d, .data_src_mode = %d\n\r",
           bf_ch_cfg_reg.audio_gain, bf_ch_cfg_reg.bf_target_dir, bf_ch_cfg_reg.bf_sound_ch_en, bf_ch_cfg_reg.data_src_mode);
    printf("  };\n\r");

    apu_ctl_t bf_ctl_reg = apu->bf_ctl_reg;

    printf("  apu->bf_ctl_reg = (apu_ctl_t){\n\r");
    printf("    .we_bf_stream_gen = 1, .we_bf_dir_search_en = 1,\n\r");
    printf("    .bf_stream_gen_en = %d, .bf_dir_search_en = %d\n\r",
           bf_ctl_reg.bf_stream_gen_en, bf_ctl_reg.bf_dir_search_en);
    printf("  };\n\r");

    
    uint8_t DelayArray[16][8];
    char DelayArrayStr[1024] = "";
    apu_get_delays(DelayArray);
    apu_get_delay_str(DelayArray, DelayArrayStr);
    printf("DelayArrayStr = %s\n\n", DelayArrayStr);
    
    printf("  for(int i = 0; i < 16; i++){\n\r");
    for (int i = 0; i < 16; i++) {
        apu_dir_bidx_t bidx0 = apu->bf_dir_bidx[i][0];
        apu_dir_bidx_t bidx1 = apu->bf_dir_bidx[i][1];

        printf("    apu->bf_dir_bidx[%d][0] = (apu_dir_bidx_t){\n\r", i);
        printf("      .dir_rd_idx0 = 0x%x,\n\r", bidx0.dir_rd_idx0);
        printf("      .dir_rd_idx1 = 0x%x,\n\r", bidx0.dir_rd_idx1);
        printf("      .dir_rd_idx2 = 0x%x,\n\r", bidx0.dir_rd_idx2);
        printf("      .dir_rd_idx3 = 0x%x\n\r", bidx0.dir_rd_idx3);
        printf("    };\n\r");
        printf("    apu->bf_dir_bidx[%d][1] = (apu_dir_bidx_t){\n\r", i);
        printf("      .dir_rd_idx0 = 0x%x,\n\r", bidx1.dir_rd_idx0);
        printf("      .dir_rd_idx1 = 0x%x,\n\r", bidx1.dir_rd_idx1);
        printf("      .dir_rd_idx2 = 0x%x,\n\r", bidx1.dir_rd_idx2);
        printf("      .dir_rd_idx3 = 0x%x\n\r", bidx1.dir_rd_idx3);
        printf("    };\n\r");
    }
    printf("  }\n\r");

    print_fir("bf_pre_fir0_coef", apu->bf_pre_fir0_coef);
    print_fir("bf_post_fir0_coef", apu->bf_post_fir0_coef);
    print_fir("bf_pre_fir1_coef", apu->bf_pre_fir1_coef);
    print_fir("bf_post_fir1_coef", apu->bf_post_fir1_coef);


    apu_dwsz_cfg_t bf_dwsz_cfg_reg = apu->bf_dwsz_cfg_reg;

    printf("  apu->bf_dwsz_cfg_reg = (apu_dwsz_cfg_t){\n\r");
    printf("    .dir_dwn_siz_rate = %d, .voc_dwn_siz_rate = %d\n\r",
           bf_dwsz_cfg_reg.dir_dwn_siz_rate, bf_dwsz_cfg_reg.voc_dwn_siz_rate);
    printf("  };\n\r");

    apu_fft_cfg_t bf_fft_cfg_reg = apu->bf_fft_cfg_reg;

    printf("  apu->bf_fft_cfg_reg = (apu_fft_cfg_t){\n\r");
    printf("    .fft_enable = %d, .fft_shift_factor = 0x%x\n\r",
           bf_fft_cfg_reg.fft_enable, bf_fft_cfg_reg.fft_shift_factor);
    printf("  };\n\r");

    apu_int_mask_t bf_int_mask_reg = apu->bf_int_mask_reg;

    printf("  apu->bf_int_mask_reg = (apu_int_mask_t){\n\r");
    printf("    .dir_data_rdy_msk = %d, .voc_buf_rdy_msk = %d\n\r",
           bf_int_mask_reg.dir_data_rdy_msk, bf_int_mask_reg.voc_buf_rdy_msk);
    printf("  };\n\r");

    printf("}\n\r");
}

