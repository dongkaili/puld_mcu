Run filterDesigner in MatLab
CD to this folder
Load "PULD BP Filters.fda"
Open "Filter Manager" and select filter
Verify parameters and select "Design Filter"

Select Menu File | Export
	Export To --> Coefficient File (ASCII)
	Format --> Decimal
	Name file to match name in filterDesigner.

Open FCF file, select coefficients (17) and copy to clipboard.

Open "BF Coefficients.xlsx" 
Paste contents of clipbopard into "Coefficient (DEC)" column of table.
Properly formatted FIR coefficients are produced for use in apu_init.c 
