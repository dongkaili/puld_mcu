Setup Instructions:

Unzip kendryte-openocd-0.2.3-win32.zip to current location.
Move "./kendryte-openocd-0.2.3-win32/kendryte-openocd" to "./"
Delete empty folder "./kendryte-openocd-0.2.3-win32"
Follow instructions in "./kendryte-openocd/Readme.txt" and run "zadig-2.4.exe" to install J-Link drivers
  J-Link adaptor must be connected
Create Windows shortcut to PowerShell script "Run_OpenOCD.ps1" and add to start menu for convenience 
Test with J-Link and target connected... If successful then disconnect and proceed with VisualStudio/GDB setup
  

Manual troubleshooting steps follow...

https://forum.kendryte.com/topic/365/can-t-debug-hello_world-example-on-k210-with-openocd-resolved

1st PS window...
$ cd /opt/kendryte-openocd
$ ./bin/openocd -f ./tcl/kendryte.cfg

2nd PS Window from project "build" folder...
$ riscv64-unknown-elf-gdb hello_world --eval-command="target remote 127.0.0.1:3333"
<could have alias for gdb>

Examples...
break main.c:173 
break main.c:dir_logic:ade


If openocd is run while a J-Link is connected the MCU will freeze. Just shutting down the prog won't release it.

Conditions to permit program start:
	J-Link USB may be connected
	Board power OFF
	Plug in USB and it runs
	
Running OpenOCD freezes MCU. 
Stopping program doesn't release MCU from halt. 
Have to disconnect/reconnect MCU USB.

Reseting MCU while USB connected results in an unconditional halt.
Able to reset MCU without USB connected and it reboots as expected. RTS/DSR controls may be getting tied up?

NOTE: Teh previously describd fault conditions do not seem to occur with MVP PULD design so circuit design may have resolved these issues.