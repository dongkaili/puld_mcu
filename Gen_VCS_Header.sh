# Script that generates a number of useful defines that may be built into the design and read 
# so we can report run-time version to the host.
# Open Powershell and execute this Linux script from there to regenerate the VCS_info header file. 
# Requires Windows Subsystem for Linux (WSL)
# https://docs.microsoft.com/en-us/windows/wsl/install-win10
cd ./src/ProSonic
./resources/autorevision.sh -t h >VCS_info.h