# README #

This is the ISP flash utility that is downloaded as a precompiled binary by kflash during user app updates.
This was provided to Prosaris by Canaan under terms of an NDA

### What is this repository for? ###

* This code will be modified to allow two or more K210 MCU's in same system to update each other without the need for a host device connected by UART
* One device as the ISP master will execute this program while driving the BOOT (IO16) and RST controls and using the ISP_TX/ISP_RX pins on the slave device. 
* As far as the slave is concerned it is being programmed just like it normally would be by a host over a USB to UART interface.

This will enable OTA application and will be used as follows:
* MCU_A holds the other MCU's in reset and asserts BOOT (IO16) as well (dedicated controls to each) 
* MCU_A releass one of the slave devices and intiates the ISP prgramming sequence using this code as a wrapper but following the steps in the kflash utility. 
* When MCU_B & MCU_C are programmmed and running the new program (to be confirmed by version query) MCU_A will then raise a signal for MCU_B which indicates it is now to program MCU_A over ISP with it as the master and A as the slave.
* MCU_B then aserts MCU_A's BOOT and RST controls. This puts MCU_A into a reset state ready to wake up in ISP mode. B also asserts another control for MCU_A which, when released, indicates to A that B is sarisfied that A has been properly updated. As long as B holds this line up it is the system master.
* MCU_B releases A from reset and the ISP process begins to propgram MCU_A.
* When MCU_A wakes back up it should be running the new version of the software. B would confirm this with a version check and if this is corect it woudl release the extra control signal so A can resune as system master.


* If only one MCU will be in the system then a 2nd trivial MCU could be added to execute this function. 
** The 2nd MCU would need acces to te hflash nmenory wheret he primary MCU received the OTA update.  

