#include "uart.h"
#include "isp.h"
#include "slip.h"
#include "syscalls.h"
#include "plic.h"
#include "sysctl.h"
#include "flash.h"
#include "fpioa.h"

/* Define isp_cb in globle to reduce stack size */
isp_cb cb;

static uint32_t isp_calculate_checksum(uint8_t *message, uint32_t size)
{
    // crc32 checksum
    int j;
    uint32_t byte, crc, mask;

    crc = 0xFFFFFFFF;
    while (size != 0) {
        byte = *message;
        crc = crc ^ byte;
        for (j = 7; j >= 0; j--) {
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }

        message++;
        size--;

    }
    return ~crc;
}

void isp_send_info(const char *str)
{
    isp_response_t response = {
            .op_ret = FLASHMODE_DEBUG_MSG,
            .reason = 0
    };

    SLIP_send_frame_delimiter();
    SLIP_send_frame_data_buf(&response, sizeof(isp_response_t));
    SLIP_send_frame_data_buf(str, strlen(str));
    SLIP_send_frame_delimiter();
}

// void isp_change_uarths_baudrate(uint32_t val)
// {
//     uint32_t freq = 380000000;
//     //uint32_t freq = sysctl_clock_get_freq(SYSCTL_CLOCK_CPU);
//     uint16_t div = freq / val - 1;

//     /* Set UART registers */
//     uarths->div.div = div;
// }

void isp_change_uart_baudrate(uint32_t val)
{
    uart_configure(UART_NUM, val, 8, UART_STOP_1, UART_PARITY_NONE);
}

void handle_command(isp_cb *cb, isp_request_t *request, isp_response_t *resp)
{
    uint32_t flash_address;
    uint8_t *data_ptr;
    uint32_t flash_chip = 1; // in-chip spi flash

    switch (request->op) {
        case FLASHMODE_FLASH_INIT:
            /* Initialization the flash */
            flash_chip = request->address;
            if (flash_chip != 0 && flash_chip != 1) {
                resp->reason = FLASHMODE_RET_INVALID_COMMAND;
                break;
            }

            /* Flash can only be initialized once */
            if (!(cb->status & FLASHMODE_STATUS_FLASH_SET)) {
                if (FLASH_OK == flash_init(flash_chip)) {
                    cb->status |= FLASHMODE_STATUS_FLASH_SET;
                    resp->reason = FLASHMODE_RET_OK;
                } else {
                    resp->reason = FLASHMODE_RET_BAD_INITIALIZATION;
                }
            } else {
                resp->reason = FLASHMODE_RET_OK;
            }
            break;
        case FLASHMODE_FLASH_ERASE:
            if (!(cb->status & FLASHMODE_STATUS_FLASH_SET)) {
                resp->reason = FLASHMODE_RET_INVALID_COMMAND;
                break;
            }
            flash_chip_erase();

            while(flash_is_busy())
                ;
            resp->reason = FLASHMODE_RET_OK;

            break;
        case FLASHMODE_FLASH_WRITE:
//		if (!(cb->status & FLASHMODE_STATUS_IDLE)) {
//			// check current status
//			cb->status |= FLASHMODE_STATUS_IDLE;
//			resp->reason = FLASHMODE_RET_INVALID_COMMAND;
//			break;
//		}

            if (!(cb->status & FLASHMODE_STATUS_FLASH_SET)) {
                resp->reason = FLASHMODE_RET_INVALID_COMMAND;
                break;
            }

            // check crc32(address+data_len+data_buf)
            if (request->checksum != isp_calculate_checksum((uint8_t *) request + 8, request->data_len + 8)) {
                resp->reason = FLASHMODE_RET_BAD_DATA_CHECKSUM;
                break;
            }

            flash_address = request->address; // highest bytes is used as chip selection
            data_ptr = (uint8_t *) request->data_buf;

            uint32_t sector_addr, sector_offset;

            sector_addr = flash_address & (~(FLASH_SECTOR_SIZE * FLASH_DATAFRAME_SCALE - 1));
            sector_offset = flash_address & (FLASH_SECTOR_SIZE * FLASH_DATAFRAME_SCALE - 1);

            if (sector_offset != 0) {
                isp_send_info("Aligned error!\n\r");
                return;
            }

            /* erase sector and write */
            //flash_sector_erase(sector_addr);
            flash_64k_block_erase(sector_addr);
            while (flash_is_busy() == FLASH_BUSY)
                ;

            flash_write_data(flash_address, data_ptr, request->data_len);

            resp->reason = FLASHMODE_RET_OK;

            cb->status |= FLASHMODE_STATUS_IDLE;

            break;

        case FLASHMODE_UART_BAUDRATE_SET:
            if (request->data_len != 4) {
                resp->reason = FLASHMODE_RET_BAD_DATA_LEN;
                return;
            }
            isp_change_uart_baudrate(*(uint32_t *)request->data_buf);
            resp->reason = FLASHMODE_RET_OK;
            break;

        case FLASHMODE_REBOOT:
            if (!(cb->status & FLASHMODE_STATUS_IDLE)) {
                // check current status
                cb->status |= FLASHMODE_STATUS_IDLE;
                resp->reason = FLASHMODE_RET_INVALID_COMMAND;
                break;
            }
            isp_send_info("Reboot!!!");
            /* Hardware reset the SoC */
            sysctl_reset(SYSCTL_RESET_SOC);
            /* WARN: Can't use soft reset here! the memory and register is dirty */
            // jump to specified address
            //((void (*)(void)) 0x88000000)();

            break;
        case FLASHMODE_NOP:
            // ping-pong
            resp->reason = FLASHMODE_RET_OK;
            break;
    }
}

// int on_uarths_irq(void *ctx)
// {

//     isp_cb *cb = (isp_cb *) ctx;
//     uint8_t c = 0;
//     uint8_t *request = (uint8_t *) cb->reading_buf;

//     uarths->ie.rxwm = 0;

//     while (uarths->ip.rxwm)
//         c = uarths->rxdata.data;

//     // read next token
//     int16_t r = SLIP_recv_byte(c, (slip_state_t *) &cb->slip_state);

//     if (r >= 0) {
//         request[cb->recv_count++] = (uint8_t) r;

//         if (cb->recv_count > sizeof(isp_request_t)) {
//             /* shouldn't happen unless there are data errors */
// //            r = SLIP_FINISHED_FRAME;
//             r = SLIP_NO_FRAME;
//             cb->error = FLASHMODE_RET_BAD_DATA_LEN;
//         }
//     }

//     if (r == SLIP_FINISHED_FRAME) {
//         /* end of frame, set 'command'
//          *  to be processed by main thread
//          */

//         if (cb->reading_buf == cb->buf_a) {
//             cb->command_ptr = (isp_request_t *) cb->buf_a;
//             cb->reading_buf = cb->buf_b;
//         } else {
//             cb->command_ptr = (isp_request_t *) cb->buf_b;
//             cb->reading_buf = cb->buf_a;
//         }
//         cb->recv_count = 0;
//     }

//     uarths->ie.rxwm = 1;

//     return 0;

// }

int on_uart_recv(void *ctx)
{
    isp_cb *cb = (isp_cb *) ctx;
    uint8_t c = 0;
    uint8_t *request = (uint8_t *) cb->reading_buf;

    int count = uart_receive_data(UART_NUM, (char *)&c, 1);

    /* Ensure received 1 byte */
    if (count == 1) {
        // read next token
        int16_t r = SLIP_recv_byte(c, (slip_state_t *) &cb->slip_state);

        if (r >= 0) {
            request[cb->recv_count++] = (uint8_t) r;

            if (cb->recv_count > sizeof(isp_request_t)) {
                /* shouldn't happen unless there are data errors */
    //            r = SLIP_FINISHED_FRAME;
                r = SLIP_NO_FRAME;
                cb->error = FLASHMODE_RET_BAD_DATA_LEN;
            }
        }

        if (r == SLIP_FINISHED_FRAME) {
            /* end of frame, set 'command'
            *  to be processed by main thread
            */

            if (cb->reading_buf == cb->buf_a) {
                cb->command_ptr = (isp_request_t *) cb->buf_a;
                cb->reading_buf = cb->buf_b;
            } else {
                cb->command_ptr = (isp_request_t *) cb->buf_b;
                cb->reading_buf = cb->buf_a;
            }
            cb->recv_count = 0;
        }
    }

    return 0;
}

void io_mux_init(void)
{
    fpioa_set_function(4, FUNC_UART1_RX + UART_NUM * 2);
    fpioa_set_function(5, FUNC_UART1_TX + UART_NUM * 2);
}

void isp_main(void)
{
    io_mux_init();
    plic_init();
    sysctl_enable_irq();

    uart_init(UART_NUM);
    uart_configure(UART_NUM, 115200, 8, UART_STOP_1, UART_PARITY_NONE);

#ifdef DEBUG
    printf("\n\r[DEBUG] Enter Flash Mode\n\r");
#endif

    /*
     * No initialize callback to reduce size
     * Initialization data will put in bss so that .bss will very big
     * If we don't initializa it, it will only use data and become small
     */

    // isp_cb cb = {
    //         .status = FLASHMODE_STATUS_IDLE,
    //         .recv_count = 0,
    //         .slip_state = SLIP_NO_FRAME,
    //         .error = 0
    // };

    memset(&cb, 0, sizeof(cb));

    cb.reading_buf = cb.buf_a;

#ifdef DEBUG
    printf("[DEBUG] FlashMode: init uart interrupt\n\r");
#endif

    // uarths->ie.rxwm = 0; // close UART interrupt
    // uarths->ie.rxwm = 1; // open UART interrupt

    //	plic_init();

    //	plic_irq_disable(IRQN_UARTHS_INTERRUPT);
    //	plic_irq_deregister(IRQN_UARTHS_INTERRUPT);

    // // init UART interrupt
    // plic_set_priority(IRQN_UARTHS_INTERRUPT, 1);
    // plic_irq_enable(IRQN_UARTHS_INTERRUPT);
    // plic_irq_register(IRQN_UARTHS_INTERRUPT, on_uarths_irq, &cb);

    // /* Enable the Machine-External bit in MIE */
    // set_csr(mie, MIP_MEIP);
    // /* Enable interrupts in general. */
    // set_csr(mstatus, MSTATUS_MIE);


    uart_set_receive_trigger(UART_NUM, UART_RECEIVE_FIFO_1);
    uart_irq_register(UART_NUM, UART_RECEIVE, on_uart_recv, &cb, 2);

#ifdef DEBUG
    printf("[DEBUG] FlashMode: loop\n\r\r");
#endif

    while (1) {

        // wait for a command or an error
        while (cb.command_ptr == NULL && cb.error == 0);

        // handle command
        if (cb.command_ptr != NULL) {
            isp_response_t response = {
                    .op_ret = cb.command_ptr->op,
                    .reason = 0
            };

            handle_command(&cb, cb.command_ptr, &response);

            cb.command_ptr = NULL;

            SLIP_send((const void *) &response, 2);
        }

        // parse error
        if (cb.error != 0) {
            isp_response_t response = {
                    .op_ret = FLASHMODE_NOP,
                    .reason = cb.error
            };

            cb.error = 0;

            SLIP_send((const void *) &response, 2);
        }

    }
}
