#ifndef _FLAHSER_FLASHER_H
#define _FLAHSER_FLASHER_H

#include <stdint.h>
#include "slip.h"
#include "uart.h"

//#define FLASHER_DATAFRAME_SIZE 16

#define FLASH_SECTOR_SIZE         (4096)
#define FLASH_DATAFRAME_SCALE     (16)
#define USER_MEMORY_START_ADDRESS (0x80000000UL)
#define UARTHS_FIFO_SIZE          (0x4000 * FLASH_DATAFRAME_SCALE)
#define ISP_DATABUF_SIZE          (FLASH_SECTOR_SIZE * FLASH_DATAFRAME_SCALE)
#define UART_NUM                  (UART_DEVICE_3)

typedef enum {
    FLASHMODE_DEBUG_MSG = 0xD1,
    FLASHMODE_NOP = 0xD2,
    FLASHMODE_FLASH_ERASE = 0xD3,
    FLASHMODE_FLASH_WRITE = 0xD4, // write data to SRAM at specified address
    FLASHMODE_REBOOT = 0xD5, // invoke function at specified address
    FLASHMODE_UART_BAUDRATE_SET = 0xD6, // change uarths baudrate
    FLASHMODE_FLASH_INIT = 0xD7, // change uarths baudrate

} FLASHMODE_COMMAND;


typedef enum {
    FLASHMODE_RET_OK = 0xE0, // command finished
    FLASHMODE_RET_BAD_DATA_LEN = 0xE1, // unsupported or wrong data length
    FLASHMODE_RET_BAD_DATA_CHECKSUM = 0xE2, // wrong data checksum
    FLASHMODE_RET_INVALID_COMMAND = 0xE3, // invalid command
    FLASHMODE_RET_BAD_INITIALIZATION = 0xE4, // initialization fail
} FLASHMODE_ERROR_CODE;

typedef enum {
    FLASHMODE_STATUS_IDLE,
    FLASHMODE_STATUS_FLASH_SET = 1 << 1,
    FLASHMODE_STATUS_FLASH_WRITING = 1 << 2
} FLASHMODE_STATUS;

typedef struct __attribute__((packed)) {
    uint16_t op;
    uint16_t reserved;
    uint32_t checksum; // 下面的所有字段都要参与checksum的计算
    uint32_t address;
    uint32_t data_len;
    uint8_t data_buf[ISP_DATABUF_SIZE];
} isp_request_t;

typedef struct __attribute__((packed)) {
    uint8_t op_ret;
    uint8_t reason;
} isp_response_t;

typedef struct __attribute__((aligned(4))) {
    uint32_t status;
    uint8_t buf_a[UARTHS_FIFO_SIZE];
    uint8_t buf_b[UARTHS_FIFO_SIZE];
    volatile uint8_t *reading_buf;
    isp_request_t *command_ptr;
    uint32_t recv_count;
    uint32_t slip_state; // slip_state_t
    uint32_t error;
} isp_cb;

void isp_main(void);

#endif