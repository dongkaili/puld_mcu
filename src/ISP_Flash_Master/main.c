/*
 * isp_flash:
 *
 * A user-level bootstrap to dump data from UART to SPI Flash (w25qxx)
 * (plaint or encrypted codes)
 *
 * Following instructions should be done in host:
 * 1. Flash address calculation
 * 2. encrypt
 * 3. chunknize user program.
 *
 * data structure on BUFFER_START_ADDRESS:
 *  ----------------------------
 * | Flash Address |  Data(4k)  |
 *  ----------------------------
 *
 * by latyas.
 * */

#include <stdint.h>
#include "clint.h"
#include "isp.h"
#include "sysctl.h"

int main()
{
    unsigned long hart_id = read_csr(mhartid);
    //uint32_t cpu_freq = sysctl_clock_get_freq(SYSCTL_CLOCK_CPU);
    // uint32_t cpu_freq = 380000000;

    // // baudrate 115200
    // uint16_t div  = cpu_freq / 115200 - 1;

    // /* Set UART registers */
    // uarths->div.div      = div;
    // uarths->txctrl.txen  = 1;
    // uarths->rxctrl.rxen  = 1;
    // uarths->txctrl.txcnt = 0;
    // uarths->rxctrl.rxcnt = 0;
    // uarths->ip.txwm      = 1;
    // uarths->ip.rxwm      = 1;
    // uarths->ie.txwm      = 0;
    // uarths->ie.rxwm      = 1;

    /* Disable core 1 in ISP to prevent bugs. Reset SoC by sysctl to enable core 1. */
    if (hart_id == 1) {
        clint_ipi_clear(hart_id);
        clear_csr(mie, MIP_MSIP);
        do {
            asm volatile("wfi");
        } while (1);
    }

    isp_main();
    return 0;
}
