/* 
 * bootloader
 * ver. 1.0.1, 01/2020
 */

//#include "bsp.h"
#include "spi.h"
#include "sysctl.h"
//#include "fpioa.h"
//#include "gpiohs.h"
#include "sha256.h"
#include "encoding.h"



// exclusive aes flag
#define AES_CBC128_SOFT 0
#define AES_CBC128_HARD 0
#define AES_CBC256_HARD 1
//#define NO_AES          1

#if (AES_CBC128_HARD || AES_CBC256_HARD)
#include "aes.h"
#endif
#if (AES_CBC128_SOFT)
#include "aes_soft.h"
#endif

#define APP_START_SRAM_ADDRESS    0x80000000
#define DEFAULT_APP_ELF_OFFSET       0x00010000
#define ELF_SIZE_OFFSET             5

#define DEFAULT_APP_ELF_FLASH_ADDRESS  (SPI3_BASE_ADDR+DEFAULT_APP_ELF_OFFSET)
#define DEFAULT_APP_ELF_CODE_FLASH_ADDRESS         (DEFAULT_APP_ELF_FLASH_ADDRESS+ELF_SIZE_OFFSET)

// config sector infomation
// magic number
#define CONFIG_DATA_OFFSET              0x00004000
#define CONFIG_DATA_FLASH_ADDRESS       (SPI3_BASE_ADDR+CONFIG_DATA_OFFSET)
#define CONFIG_DATA_BACKUP_OFFSET              0x00005000
#define CONFIG_DATA_BACKUP_FLASH_ADDRESS       (SPI3_BASE_ADDR+CONFIG_DATA_BACKUP_OFFSET)
#define CONFIG_MAGIC_ID                 0x5DECA0A0
#define CONFIG_MAGIC_MASK               0xFFFFFFF0
#define CONFIG_CHECK_CRC                0x00000001
#define CONFIG_CHECK_SHA                0x00000002
#define CONFIG_CHECK_SIZE               0x00000004

typedef struct {
    uint32_t magic_id;
    uint32_t app_offset;
    uint32_t app_size;
    uint32_t app_crc;
    uint32_t cipher_flag;
    uint8_t  app_desc[16];
} bootloader_config_info_s;

// static bootloader_config_info_s bootloader_config_info = {
//     .app_offset = DEFAULT_APP_ELF_FLASH_ADDRESS,
//     .app_size = 0,
//     .app_crc = 0,
//     .app_desc = "app name",
// };

static bootloader_config_info_s frfw_info = {
        .magic_id = CONFIG_MAGIC_ID,
        .app_offset = DEFAULT_APP_ELF_OFFSET,
        .app_size = 0,
        .app_crc = 0,
        .cipher_flag = 1,
        .app_desc = "Factory Reset",
    };
    

#define DEBUG
#ifdef DEBUG
// K210 ROM functions
typedef int bootloader_rom_print_func(const char * restrict format, ... );
bootloader_rom_print_func *bootloader_rom_printf = (bootloader_rom_print_func*)0x88001418;     // fixed address in ROM
bootloader_rom_print_func *bootloader_rom_printk = (bootloader_rom_print_func*)0x880016b0;     // fixed address in ROM
#else
#define bootloader_rom_print_func(...)  ()
#endif

// static uint32_t print_enabled = 1;
// Printing messages if interactive mode is enabled
// K210 ROM code is used for printing
#define LOG(format, ...)                        \
    do                                          \
    {                                           \
        bootloader_rom_printk(format, ##__VA_ARGS__);  \
        usleep(200);                        \
    } while(0)


#define CHUNK_SIZE   16

#if (AES_CBC128_SOFT || AES_CBC128_HARD)
static uint8_t aes_key[] = { 0xfd, 0xef, 0xe7, 0x82, 0x76, 0x61, 0x23, 0x3c, 0x00, 0x6b, 0x8a, 0x98, 0x47, 0x35, 0x89, 0x06 };
static size_t key_len = 16;
#endif

#if (AES_CBC256_HARD)
static uint8_t aes_key[] = { 0xfb, 0xcf, 0xe7, 0x82, 0x85, 0x64, 0x71, 0x1d, 0x01, 0x6e, 0x5f, 0x24, 0x17, 0x20, 0x43, 0x03,
                    0xf0, 0xfe, 0xc9, 0x42, 0x26, 0x55, 0x73, 0x1b, 0x00, 0x4a, 0x9f, 0x44, 0x66, 0x3f, 0x81, 0x08 };
static size_t key_len = 32;
#endif

static uint8_t aes_iv[] = { 0xcb, 0xfe, 0x1a, 0x2e, 0xfd, 0xae, 0x4b, 0xbd, 0xdf, 0x3a, 0xf8, 0x98, 0x57, 0x20, 0x84, 0x07 };
static uint8_t iv_len = 16;

static uint8_t aes_soft_data[CHUNK_SIZE];
static uint8_t aes_hard_in_data[CHUNK_SIZE];
static uint8_t aes_hard_out_data[CHUNK_SIZE];
static uint8_t aes_hard_tmp_data[CHUNK_SIZE];

static uint32_t aes_soft_hard_unmatch_count = 0;

// Variables
//volatile sysctl_t *const sysctl = (volatile sysctl_t *)SYSCTL_BASE_ADDR;


// Variables
static uint32_t bootloader_core0_sync = 0;
static uint32_t bootloader_core1_sync = 0;


static uint8_t bootloader_app_encrypt_check(uint8_t* code_flash_ptr)
{
    uint8_t ret = 1;
    if ((code_flash_ptr[0] == 'P') &&
        (code_flash_ptr[1] == 'r') &&
        (code_flash_ptr[2] == 'o') &&
        (code_flash_ptr[3] == 's') &&
        (code_flash_ptr[4] == 'a') &&
        (code_flash_ptr[5] == 'r') &&
        (code_flash_ptr[6] == 'i') &&
        (code_flash_ptr[7] == 's'))
    {
        // for (int j = 0; j < 8; j++)
        // {
        //     LOG("%c", code_flash_ptr[j]);
        // }
        // LOG("\r\n");
        ret = 0;
    }
    return ret;
}

static bootloader_config_info_s bootloader_get_config_data(uint32_t address) {
    bootloader_config_info_s info = {
        .magic_id = 0,
        .app_offset = DEFAULT_APP_ELF_OFFSET,
        .app_size = 0,
        .app_crc = 0,
        .cipher_flag = 0,
        .app_desc = "app name",
    };

    const uint32_t *cfg_flash_ptr = (uint32_t *)(address);
    //uint32_t *cfg_flash_bptr = (uint32_t *)(address);
    uint32_t app_desc_word;
    info.magic_id = cfg_flash_ptr[0];
    LOG("\r\n* cfg_flash_ptr 0x%08X (info.magic_id:0x%08x)\n\r", cfg_flash_ptr, info.magic_id);
    //LOG("(info.app_offset:0x%08x)\n\r", info.app_offset);

    if ((info.magic_id & CONFIG_MAGIC_MASK) == CONFIG_MAGIC_ID) {
        info.app_offset = cfg_flash_ptr[1];
        info.app_size = cfg_flash_ptr[2];
        info.app_crc = cfg_flash_ptr[3];
        info.cipher_flag = cfg_flash_ptr[4];
        //cfg_flash_bptr = &(cfg_flash_ptr[5]);
        for(int i = 0; i < 4; i ++) {            
            for (int j = 0; j < 4; j++) {
                app_desc_word = cfg_flash_ptr[i+5];
                info.app_desc[i*4+j] = ( app_desc_word >> ((3-j)*8)) & 0xFF;            
            }
        }
        // for (int i = 0; i < 16; i++)
        // {
        //     LOG("[%d]0x%02x, ", i, info.app_desc[i]);
        // }
        // LOG("\n\r");
        
        
    } else {
        info.magic_id = (info.magic_id & 0x0);
    }
    LOG("app_desc:%s, info.app_size:%d\n\r", info.app_desc, info.app_size);
    LOG("(info.app_offset:0x%08x, info.cipher_flag:0x%08x)\n\r", info.app_offset, info.cipher_flag);
    return info;
}

static uint32_t bootloader_get_app_size(uint32_t app_addr) {
    uint8_t *flash_ptr = (uint8_t *)app_addr;
    uint32_t size = flash_ptr[1];
    size += flash_ptr[2] << 8;
    size += flash_ptr[3] << 16;
    size += flash_ptr[4] << 24;
    // bootloader_rom_printk("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
    return size;
}

/*
 * Check the application's SHA256 hash
 * The hash is 32 bytes long and written after the application's code
 * by Kflash or other application
 */
//--------------------------
static uint32_t bootloader_check_app_sha256(bootloader_config_info_s info)
{
    LOG("bootloader_check_app_sha256 app_size=%u\n\r", info.app_size);
    sha256_context_t context;
    uint8_t buffer[1024] = {0};
    // uint8_t app_hash[SHA256_HASH_LEN] = {0};
    uint8_t hash[SHA256_HASH_LEN] = {0};
    const uint8_t *app_flash_ptr = (uint8_t *)(info.app_offset + SPI3_BASE_ADDR);

    int size = info.app_size + 5;
    int sz;
    sha256_init(&context, size);
    uint32_t idx = 0;

    while (size > 0) {
        sz = (size >= 1024) ? 1024 : size;
        for (int n=0; n<sz; n++) {
            buffer[n] = app_flash_ptr[idx + n];
        }
        sha256_update(&context, buffer, sz);
        idx += sz;
        size -= sz;
    }

    sha256_final(&context, hash);

    // get the application's SHA256 hash
    uint32_t offset = info.app_size + 5;
    for (int n=0; n<SHA256_HASH_LEN; n++) {
        // LOG("HASH: cal hash[%d]:0x%02x, app_hash[%d]:0x%02x ", n, hash[n], n ,app_flash_ptr[offset + n]);
        if (hash[n] != app_flash_ptr[offset + n]) {
            LOG("SHA256 error, ");
            return 0;
        }
    }

    return 1;
}

/*
 * Check the current application CRC32 value
 */
//-------------------------
static uint32_t bootloader_check_app_crc32(bootloader_config_info_s info)
{
    uint32_t crc = 0xFFFFFFFF;
    uint32_t byte, mask;
    const uint8_t *app_flash_ptr = (uint8_t *)(info.app_offset + SPI3_BASE_ADDR);
    // Read from flash and update CRC32 value
    for (uint32_t n = 5; n < (info.app_size+5); n++) {
        byte = app_flash_ptr[n];
        crc = crc ^ byte;
        for (uint32_t j = 0; j < 8; j++) {
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
    }
    crc = ~crc;
    if (crc !=info.app_crc) {
        LOG("CRC32 error, ");
        return 0;
    }
    return 1;
}

/*
 * Check if the current application parameters
 * points to the valid application
 */
//-------------------------
static uint32_t bootloader_app_check(bootloader_config_info_s* info)
{
    LOG("bootloader_app_check (info.app_offset:0x%08x)\n\r", info->app_offset);
    const uint8_t *app_flash_ptr = (uint8_t *)(info->app_offset + SPI3_BASE_ADDR);
    uint8_t  key = app_flash_ptr[0];          // must be 0, SHA256 key NOT specified
    uint32_t sz = bootloader_get_app_size(info->app_offset + SPI3_BASE_ADDR);          // app size
    LOG("key = 0x%02x, sz=%u, \n\r", key, sz);

    //LOG("(info.magic_id:0x%08x)\n\r", info->magic_id);

    //LOG("%s,%s,%d\n\r", __FILE__,__FUNCTION__,__LINE__);
    if (key != 0) {
        LOG("%s,%s,%d\n\r", __FILE__,__FUNCTION__,__LINE__);
        return 0;
    }

    LOG("(info.magic_id:0x%08x)\n\r", info->magic_id);
    LOG("((info->magic_id & CONFIG_CHECK_SIZE):%d)\n\r", (info->magic_id & CONFIG_CHECK_SIZE));
    LOG("((info->app_size != sz):%d)\n\r", (info->app_size != sz));
    if ((info->magic_id & CONFIG_CHECK_SIZE) && (info->app_size != sz)) {
        LOG("%s,%s,%d\n\r", __FILE__,__FUNCTION__,__LINE__);
        return 0;
    }else
    if (info->app_size != sz) {
        LOG("app_size=%u, \n\r", sz);
        info->app_size = sz;
        info->magic_id = (info->magic_id | CONFIG_CHECK_SHA);
    }

    //LOG("(info.magic_id:0x%08x)\n\r", info->magic_id);
    LOG("((info->magic_id & CONFIG_CHECK_SHA):%d)\n\r", (info->magic_id & CONFIG_CHECK_SHA));
    if (info->magic_id & CONFIG_CHECK_SHA) {
        LOG("%s,%s,%d\n\r", __FILE__,__FUNCTION__,__LINE__);
        // SHA256 check was requested, check flash data
        return bootloader_check_app_sha256(*info);
    }
    LOG("((info->magic_id & CONFIG_CHECK_CRC):%d)\n\r", (info->magic_id & CONFIG_CHECK_CRC));
    if (info->magic_id & CONFIG_CHECK_CRC) {
        LOG("%s,%s,%d\n\r", __FILE__,__FUNCTION__,__LINE__);
        // CRC check was requested, check flash data
        return bootloader_check_app_crc32(*info);
    }
    LOG("info->cipher_flag: %d\n\r", info->cipher_flag);
    
    uint8_t * code_flash_ptr = (uint8_t *)(info->app_offset + SPI3_BASE_ADDR + ELF_SIZE_OFFSET);
    
    if ((info->cipher_flag) ^ (bootloader_app_encrypt_check(&code_flash_ptr[2])))
    {
        LOG("cipher flag NOT match with encryption check!\r\n");
        return 0;
    }   
    LOG("cipher flag matches with bin encryption check\r\n");
    
    return 1;
}



int main(void)
{
    uint32_t i = 0;
    //uint32_t freq = sysctl_pll_set_freq(SYSCTL_PLL0, 390000000);
    uint8_t *app_code_flash_ptr = (uint8_t *)DEFAULT_APP_ELF_CODE_FLASH_ADDRESS;
    uint8_t *app_sram_ptr = (uint8_t *)APP_START_SRAM_ADDRESS;

    if (0 != read_csr(mhartid)) {
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        bootloader_core0_sync = 1;

        while (bootloader_core1_sync == 0) {
            asm("nop");
        }
        usleep(1000);
        asm("fence");   // D-Cache; this may not be necessary, but it looks it doesn't hurt if it is executed
        asm("fence.i"); // I-Cache
        asm ("jr %0" : : "r"(APP_START_SRAM_ADDRESS));
        while (1) {
            asm("nop");
        }
    }

    bootloader_rom_printf(NULL);
    while (bootloader_core0_sync == 0) {
        asm("nop");
    }

    bootloader_config_info_s info = bootloader_get_config_data(CONFIG_DATA_FLASH_ADDRESS);
    if(0 == bootloader_app_check(&info)) {
        LOG("\n\r* application check failed!\n\r");
        LOG("* Load previous app\n\r");
        info = bootloader_get_config_data(CONFIG_DATA_BACKUP_FLASH_ADDRESS);
        if(0 == bootloader_app_check(&info)) {
            LOG("\n\r* backup application check failed!\n\r");
            LOG("* load factory reset firmware\n\r");
            // TODO: load the Factory reset FW
            info.app_size = bootloader_get_app_size(frfw_info.app_offset + SPI3_BASE_ADDR);
            info.cipher_flag = frfw_info.cipher_flag;
            info.magic_id = frfw_info.magic_id;
            info.app_offset = frfw_info.app_offset;
            for (uint8_t i = 0; i < 16; i++)
            {
                info.app_desc[i] = frfw_info.app_desc[i];
            }
            LOG("FR FW info: app_desc:%s, app_offset:0x%08X, app_size:%d, cipher_flag:0x%08X", info.app_desc, info.app_offset, info.app_size, info.cipher_flag);
            // while (1) {
            //     asm ("nop");
            // }
        }
    }
    
    LOG("* read backup config info:\r\n");
    bootloader_config_info_s backup_info = bootloader_get_config_data(CONFIG_DATA_BACKUP_FLASH_ADDRESS);
    
    LOG("\n\r* application check passed with app offset: 0x%08x and cipher flag: 0x%08x \n\r", info.app_offset, info.cipher_flag);
    app_code_flash_ptr = (uint8_t *)(info.app_offset + SPI3_BASE_ADDR + ELF_SIZE_OFFSET);
    
    // init_flash();

    // uint32_t size = bootloader_get_app_size(info.app_offset + SPI3_BASE_ADDR);
    // bootloader_rom_printk("APP size: %d\n\r", size);

    //usleep(6000000);
    
    uint32_t chunk_num = info.app_size / CHUNK_SIZE; 

#if (AES_CBC128_SOFT)
    struct AES_ctx ctx;
#endif

#if (AES_CBC128_HARD || AES_CBC256_HARD)    
    cbc_context_t cbc_context;
    cbc_context.input_key = aes_key;
    cbc_context.iv = aes_iv;
#endif

    if ((info.cipher_flag == 1) && (bootloader_app_encrypt_check(&app_code_flash_ptr[2]) == 1))
    {
        for (int j = 0; j < chunk_num; j++)
        {
            // get the encrypted data from flash
            for (int i = 0; i < CHUNK_SIZE; i++)
            {
#if (AES_CBC128_SOFT)
                aes_soft_data[i] = app_code_flash_ptr[i + j * CHUNK_SIZE];
#endif
#if (AES_CBC128_HARD || AES_CBC256_HARD)
                aes_hard_in_data[i] = app_code_flash_ptr[i + j * CHUNK_SIZE];
#endif
            }

// AES soft decrypt
#if (AES_CBC128_SOFT)
            AES_init_ctx_iv(&ctx, aes_key, aes_iv);
            AES_CBC_decrypt_buffer(&ctx, aes_soft_data, CHUNK_SIZE);
#endif

// AES hard decrypt
#if (AES_CBC128_HARD)
            aes_cbc128_hard_decrypt(&cbc_context, aes_hard_in_data, CHUNK_SIZE, aes_hard_out_data);
#endif

#if (AES_CBC256_HARD)
            aes_cbc256_hard_decrypt(&cbc_context, aes_hard_in_data, CHUNK_SIZE, aes_hard_out_data);
            if (j == 0)
            {
                if (bootloader_app_encrypt_check(&aes_hard_out_data[2]) == 0)
                {
                    LOG("Decrypting PULD fw...\r\n");
                }
            }
#endif

#if (AES_CBC128_HARD || AES_CBC256_HARD)
            // copy the hard decrypted data to RAM
            for (int i = 0; i < CHUNK_SIZE; i++)
            {
                app_sram_ptr[j * CHUNK_SIZE + i] = aes_hard_out_data[i]; // expect to use the hard decrypted
            }
#endif

#if (AES_CBC128_SOFT && AES_CBC128_HARD)
            if (aes_soft_hard_unmatch_count < 128)
            {
                for (int i = 0; i < CHUNK_SIZE; i++)
                {
                    if (aes_soft_data[i] != aes_hard_out_data[i])
                    {
                        aes_soft_hard_unmatch_count++;
                        LOG("Index: %d, Soft: %02x, Hard: %02x\r\n", j * CHUNK_SIZE + i, aes_soft_data[i], aes_hard_out_data[i]);
                    }
                }
            }

#endif
        }
    }
    else if (info.cipher_flag == 0 && bootloader_app_encrypt_check(&app_code_flash_ptr[2]) == 0)
    {
        for (i = 0; i < info.app_size; i++)
        {
            //*app_sram_ptr++ = *app_code_flash_ptr++;
            app_sram_ptr[i] = app_code_flash_ptr[i];
        }
    }
    

#if (AES_CBC128_SOFT && AES_CBC128_HARD)
    LOG("%d bytes of soft and hard decrypt results are NOT matched!\r\n", aes_soft_hard_unmatch_count);
#endif
    

    
    
    
    usleep(1000);
    // bootloader_rom_printk("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);

    // Disable XIP mode
    sysctl->peri.spi3_xip_en = 0;

    bootloader_core1_sync = 1;
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // bootloader_rom_printk("core0 boot0 jump to btloader\n\r");
    
    asm("fence");   // D-Cache; this may not be necessary, but it looks it doesn't hurt if it is executed
    asm("fence.i"); // I-Cache
    asm ("jr %0" : : "r"(APP_START_SRAM_ADDRESS));

    while (1) {
        asm("nop");
    }
    return 0;
}
