/* 
 * boot0
 * ver. 1.0.1, 01/2020
 */

#include "spi.h"
#include "sysctl.h"
#include "encoding.h"

#define BOOTLOADER_START_SRAM_ADDRESS    0x805e0000
#define BOOTLOADER_ELF_OFFSET       0x00001000
#define ELF_SIZE_OFFSET             5

#define BOOTLOADER_ELF_FLASH_ADDRESS  (SPI3_BASE_ADDR+BOOTLOADER_ELF_OFFSET)
#define BOOTLOADER_ELF_CODE_FLASH_ADDRESS         (BOOTLOADER_ELF_FLASH_ADDRESS+ELF_SIZE_OFFSET)


#ifdef DEBUG
// K210 ROM functions
typedef int rom_print_func(const char * restrict format, ... );
rom_print_func *rom_printf = (rom_print_func*)0x88001418;     // fixed address in ROM
rom_print_func *rom_printk = (rom_print_func*)0x880016b0;     // fixed address in ROM
#else
#define rom_print_func(...)  ()
#endif

// Variables
static uint32_t boot0_core0_sync = 0;
static uint32_t boot0_core1_sync = 0;

void boot0_init_flash() {
    volatile sysctl_t *const sysctl = (volatile sysctl_t *)SYSCTL_BASE_ADDR;
    volatile spi_t *spi_handle = (volatile spi_t *)SPI3_BASE_ADDR;

    // spi clock init (SPI3 clock source is PLL0/2, 390 MHz)
    sysctl->clk_sel0.spi3_clk_sel = 1;
    sysctl->clk_en_peri.spi3_clk_en = 1;
    sysctl->clk_th1.spi3_clk_threshold = 0;
    
    // spi3 init
    // sets spi clock to 43.333333 MHz
    spi_handle->baudr = 9;
    spi_handle->imr = 0x00;
    spi_handle->dmacr = 0x00;
    spi_handle->dmatdlr = 0x10;
    spi_handle->dmardlr = 0x00;
    spi_handle->ser = 0x00;
    spi_handle->ssienr = 0x00;
    spi_handle->ctrlr0 = (SPI_WORK_MODE_0 << 8) | (SPI_FF_QUAD << 22) | (7 << 0);
    spi_handle->spi_ctrlr0 = 0;
    spi_handle->endian = 0;

    // Enable XIP mode
    spi_handle->xip_ctrl = (0x01 << 29) | (0x02 << 26) | (0x01 << 23) | (0x01 << 22) | (0x04 << 13) |
                (0x01 << 12) | (0x02 << 9) | (0x06 << 4) | (0x01 << 2) | 0x02;
    spi_handle->xip_incr_inst = 0xEB;
    spi_handle->xip_mode_bits = 0x00;
    spi_handle->xip_ser = 0x01;
    spi_handle->ssienr = 0x01;
    sysctl->peri.spi3_xip_en = 1;
}

uint32_t boot0_get_bootloader_size() {
    uint8_t *flash_ptr = (uint8_t *)BOOTLOADER_ELF_FLASH_ADDRESS;
    uint32_t size = flash_ptr[1];
    size += flash_ptr[2] << 8;
    size += flash_ptr[3] << 16;
    size += flash_ptr[4] << 24;
    // rom_printk("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
    return size;
}

int main(void)
{
    uint32_t i = 0;
    uint8_t *bootloader_flash_ptr = (uint8_t *)BOOTLOADER_ELF_CODE_FLASH_ADDRESS;
    uint8_t *bootloader_sram_ptr = (uint8_t *)BOOTLOADER_START_SRAM_ADDRESS;

    if (0 != read_csr(mhartid)) {
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        // asm volatile("nop");
        boot0_core0_sync = 1;

        while (boot0_core1_sync == 0) {
            asm("nop");
        }

        asm("fence");   // D-Cache; this may not be necessary, but it looks it doesn't hurt if it is executed
        asm("fence.i"); // I-Cache
        asm ("jr %0" : : "r"(BOOTLOADER_START_SRAM_ADDRESS));
        while (1) {
            asm("nop");
        }
    }

    rom_printf(NULL);
    while (boot0_core0_sync == 0) {
        asm("nop");
    }

    boot0_init_flash();

    uint32_t size = boot0_get_bootloader_size();
    // rom_printk("bootloader size: %d\n\r", size);

    for (i = 0; i < size; i++) {
        *bootloader_sram_ptr++ = *bootloader_flash_ptr++;
    }
    // rom_printk("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);

    boot0_core1_sync = 1;
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // asm volatile("nop");
    // rom_printk("core0 boot0 jump to btloader\n\r");
    
    asm("fence");   // D-Cache; this may not be necessary, but it looks it doesn't hurt if it is executed
    asm("fence.i"); // I-Cache
    asm ("jr %0" : : "r"(BOOTLOADER_START_SRAM_ADDRESS));

    while (1) {
        asm("nop");
    }
    return 0;
}
