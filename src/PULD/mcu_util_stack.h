#ifndef __mcu_UTIL_STACK_H__
#define __mcu_UTIL_STACK_H__

typedef struct __mcu_util_stack_{
    uint32_t init_size;
    void *elems;
    uint16_t elem_size;
    uint32_t count;
    uint32_t size;
    void (*free_callback)(void *);
} _mcu_util_stack_s;

void mcu_util_stack_init(_mcu_util_stack_s *s, uint32_t init_size, uint16_t elem_size, void (*free_callback)(void *));
void mcu_util_stack_close(_mcu_util_stack_s *s);
void mcu_util_stack_push(_mcu_util_stack_s *s, void *elem_addr);
void mcu_util_stack_pop(_mcu_util_stack_s *s, void *elem_addr);
void mcu_util_stack_peek(_mcu_util_stack_s *s, void *elem_addr);
uint8_t mcu_util_stack_is_empty(_mcu_util_stack_s *s, void *elem_addr);

#endif

