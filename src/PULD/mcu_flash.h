#ifndef __mcu_FLASH_H__
#define __mcu_FLASH_H__

uint8_t mcu_flash_init(void*);
void mcu_flash_write(uint32_t address, uint8_t *data_buf, uint32_t length);
void mcu_flash_read(uint32_t address, uint8_t *data_buf, uint32_t length);
void mcu_flash_prepare_reset();
void mcu_flash_erase_app_area(uint32_t app_offset, uint32_t app_size);
#endif