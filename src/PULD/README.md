# autusonic
1. copy folder "autosonic" int to $SDK/src/ 

2. compile source code, under the $SDK/ folder
   ```
   mkdir build && cd build
   cmake -G "MinGW Makefiles" ../../..
   ```

3. flash bin to board
   ```
   python3 kflash.py -p /dev/ttyUSB1 -b 2000000 -B goE /home/kai/work/autosonic/sipeed/kendryte-standalone-sdk/build/autosonic.bin
   Using Windows kflash utility is best in Windows env
   
   ```

   

