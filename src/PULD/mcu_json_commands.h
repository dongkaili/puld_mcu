#ifndef _MCU_JSON_COMMANDS_
#define _MCU_JSON_COMMANDS_
#include "mcu_comm_protocol.h"
#include "mcu_adc.h"
#include "mjson.h"
#define SIZE     48
#define MCU_DATA_SIZE  512

typedef struct mcu_command_object {
    char command[SIZE];
    int  mcu;
    union {

        char state[SIZE*2];    // for slave 0

        // struct {
        //     char command[SIZE];
        //     char state[SIZE];
        // } mcu_command;     // command for slave 1/2

        struct
        {
            char    var_name[SIZE];
            double  var_value;
        } var_real;

        // struct {
        //     int block_number;
        //     int block_total_number;
        //     char mcu_data[MCU_DATA_SIZE];
        //     int check_sum;
        // } mcu_block_data;   // data for slave 1/2
        
        struct
        {
            char command[SIZE];
            int  var_value;
        } var_int;
        
        double var_value;

    } data;
} mcu_command_object_t;

typedef struct mcu_response_object {
    char response_command[SIZE];
    int  mcu;
    struct {
        char state[SIZE];
        double var_value;
    } data;
} mcu_response_object_t;

typedef struct mcu_stream_object {
    int  mcu;
    double angle;
    double angle_azm;
    double angle_inc;
    double angle_dut;
    double dbfs;
    double dbfs_azm;
    double dbfs_inc;
    double dbfs_dut;
    int  nmpa;
    char state[SIZE];
} mcu_stream_object_t;
void  mcu_json_command_proc(uint8_t index, mcu_comm_data_s* data);
void  mcu_init_command_handler(void);
void report_firmware_version();
void report_mcu_id();
void report_mcu_status();
void enable_host_reporting(bool enable_flag);
void slave_mcu_spi_ready_notification(uint8_t mcu_index);

//void modify_core_clock_frequency(uint8_t mcu_index, uint32_t core_clock_frequency);
void modify_core_clock_frequency_offset(uint8_t mcu_index, uint32_t core_clock_frequency);
    
void Set_LPM(bool status);
void remap_stdio_debug(void* mcu_idx, void* enable_flg);

void mcu_uart_restore_baud_rate(void);
#endif

