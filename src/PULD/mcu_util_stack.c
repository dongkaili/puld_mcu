#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "bsp.h"
#include "mcu_util_stack.h"
#include "mcu_ipc_sync.h"

void mcu_util_stack_init(_mcu_util_stack_s *s, uint32_t init_size, uint16_t elem_size, void (*free_callback)(void *)) {
    assert(elem_size > 0);
    s->init_size = init_size;
    s->elem_size = elem_size;
    s->count = 0;
    s->size = init_size;
    s->elems = malloc(init_size * elem_size);
    s->free_callback = free_callback;
    assert(s->elems != NULL);
}

void mcu_util_stack_close(_mcu_util_stack_s *s) {
    if (s->free_callback != NULL) {
        for (int i = 0; i < s->count; i++) {
            s->free_callback((char *)s->elems + i * s->elem_size);
        }
    }
    free(s->elems);
}

static void mcu_util_stack_renew(_mcu_util_stack_s *s) {
    s->size += s->init_size;
    void* p_elems = realloc(s->elems, s->size * s->elem_size);
    s->elems = p_elems;
}

void mcu_util_stack_push(_mcu_util_stack_s *s, void *elem_addr) {
    corelock_lock(&_init_lock);
    if (s->count == s->size) {
        mcu_util_stack_renew(s);
    }
    void *target = (char *)s->elems + s->count * s->elem_size;
    memcpy(target, elem_addr, s->elem_size);
    s->count++;
    corelock_unlock(&_init_lock);
}

void mcu_util_stack_pop(_mcu_util_stack_s *s, void *elem_addr) {
    corelock_lock(&_init_lock);
    assert(s->count > 0);
    s->count--;
    void *source = (char *)s->elems + s->count * s->elem_size;
    memcpy(elem_addr, source, s->elem_size);
    corelock_unlock(&_init_lock);
}

void mcu_util_stack_peek(_mcu_util_stack_s *s, void *elem_addr) {
    assert(s->count > 0);
    void *source = (char *)s->elems + (s->count - 1) * s->elem_size;
    memcpy(elem_addr, source, s->elem_size);
}

uint8_t mcu_util_stack_is_empty(_mcu_util_stack_s *s, void *elem_addr) {
    return (s->count) ? 1 : 0;
}
