#include <stdio.h>
#include "fpioa.h"
#include "sysctl.h"
#include "w25qxx.h"
#include "mcu_flash.h"
#include "mcu_common.h"

#define DATA_ADDRESS 0x110000

extern w25qxx_status_t w25qxx_64k_block_erase_dma(uint32_t addr);

uint8_t mcu_flash_init(void* p) {
    mcu_flash_prepare_reset();
    uint32_t spi_index = 3;
    uint8_t manuf_id, device_id;
    w25qxx_init_dma(spi_index, 0);
    w25qxx_enable_quad_mode_dma();
    w25qxx_read_id_dma(&manuf_id, &device_id);
    //printf("manuf_id:0x%02x, device_id:0x%02x\n\r", manuf_id, device_id);
    if ((manuf_id != 0xEF && manuf_id != 0xC8) || (device_id != 0x17 && device_id != 0x16))
    {
        printf("manuf_id:0x%02x, device_id:0x%02x\n\r", manuf_id, device_id);
        return 1;
    }
    return 0;
}

static uint8_t layout_A_enabled = 0;
static uint8_t layout_B_enabled = 0;
static uint8_t layout_config_enabled = 0;
static int prepareFlash(uint32_t address) {
    int rt = 0;
    int i = 0;
    if(LAYOUT_APP_B_END < address) {
        //unsupport
    } else if(LAYOUT_APP_A_END < address) {
        //#define LAYOUT_APP_B_SIZE              (2*1024*1024)
        if(layout_B_enabled == 0) {            
            mcu_flash_erase_app_area(LAYOUT_APP_B_OFFSET, LAYOUT_APP_B_SIZE);
            layout_B_enabled = 1;
        }
        rt = 1;
        //app B
    } else if(LAYOUT_RESERVE_END < address) {
        if(layout_A_enabled == 0) {
            mcu_flash_erase_app_area(LAYOUT_APP_A_OFFSET, LAYOUT_APP_A_SIZE);
            layout_A_enabled = 1;
        }
        rt = 1;
        //app A
    } else if(LAYOUT_CONFIG_END < address) {
        //reserve
    } else if(LAYOUT_BOOTLOADER_END < address) {
        if(layout_config_enabled == 0) {
            for(i = 0; i < LAYOUT_CONFIG_SIZE/(4*1024); i ++) {
                w25qxx_sector_erase_dma(LAYOUT_CONFIG_OFFSET + i*(4*1024));
                while (w25qxx_is_busy_dma() == W25QXX_BUSY)
                    asm volatile("nop");
            }
            layout_config_enabled = 1;
        }
        rt = 1;        //config
    } else if(LAYOUT_BOOT0_END < address) {
        //bootloader
    } else if(LAYOUT_BOOT0_OFFSET < address) {
        //boot0
    } else {
        //unsupport
    }
    return rt;
}

void mcu_flash_write(uint32_t address, uint8_t *data_buf, uint32_t length) {
    /*write data*/
    // printf("erase sector\n\r");
    if (prepareFlash(address) == 1)
        w25qxx_write_data_dma(address, data_buf, length);
    //printf("mcu_flash_write: %ld us\n\r", (stop - start));
}

void mcu_flash_read(uint32_t address, uint8_t *data_buf, uint32_t length) {
    //w25qxx_read_data_dma(address, data_buf, length, W25QXX_QUAD_FAST);//W25QXX_STANDARD);
    w25qxx_read_data_dma(address, data_buf, length, W25QXX_STANDARD);
}

void mcu_flash_prepare_reset() {
    layout_A_enabled = 0;
    layout_B_enabled = 0;
    layout_config_enabled = 0;
}

void mcu_flash_erase_app_area(uint32_t app_offset, uint32_t app_size)
{
    for (int i = 0; i < app_size / (64 * 1024); i++)
    {
        w25qxx_64k_block_erase_dma(app_offset + i * (64 * 1024));
        while (w25qxx_is_busy_dma() == W25QXX_BUSY)
            asm volatile("nop");
    }
}