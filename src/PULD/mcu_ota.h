#ifndef __MCU_OTA_H__
#define __MCU_OTA_H__

#if OTA_TEST
void fake_ota_data();
void fake_ota_command();
#endif
#if MASS_STRING_SEND_TEST
void send_test_string_from_local();
#endif
#endif