#ifndef __MCU_COMMON_DEFINE_H__
#define __MCU_COMMON_DEFINE_H__

#include <stdbool.h>
#include "mcu_wdt.h"
#include "uart.h"

#define FACTORY_RESET 0
#define FRF_FWV "V0.0.1"

#define START_WITH_HOST_REPORTING false

#define OTA_TEST 0
#define MASS_STRING_SEND_TEST 0

#define TEST_MULTI_UART  // Works with or without 
#define N_MULTI_UART 3   // Works for 1, 2, or 3
#define START_IN_TERMINAL_MODE false

#define HIGH_BAUD_RATE   0

#if HIGH_BAUD_RATE
    #define HOST_MCU_BAUD_RATE 115200*8
    #define MCU_MCU_BAUD_RATE 115200*20
#else
    #define HOST_MCU_BAUD_RATE 115200
    #define MCU_MCU_BAUD_RATE 115200
#endif
#define DEBUG_BAUD_RATE 115200

#define UART_OFFSET 0 // use this to test use of each UART with the master MCU: 0, 1 or 2

//#define USE_QUEUE_BUFFER
//#define USE_UART_DMA

#define BP_LOW_FREQ 26000
#define BP_HIGH_FREQ 42000

#define FOV_ANGLE 30               // Angle of the FOV cone centered on vector projected from center of each array and perpendicular to plane of array... 180° indicates in plane of array (previous default)
#define AZM_ANGLE 0                // Default azimuth angle when scan running HBF in incident mode

#define AGC_ADJUST_RATE 100

#define DISP_LIM  0.15
#define DISP_HYS  0.01

#define ROT_LIM   0.05
#define ROT_HYS   0.01

#define AZM_TO_INC_RATIO 4

#define MCU_COMM_MAX_DATA_SIZE 16*1024
//#define GET_FFT_BIN(CutoffFreq, SamplingFreq)                     
//    (uint16_t)(512 * CutoffFreq / SamplingFreq )

#define INCLUDE_WDT 1 // Determines whether the WDT devices are built into the design. WDT devices cannot be included during H/W debugging sessions. Target must be running non-WDT code.
#define ENABLE_MCU_PWR false
#define ENABLE_CHARGER true

#define INCLUDE_STDIO_REMAP true
// Set the following to false for any MCU which may need to be debugged by JTAG and install this as the active F/W with kflash
// This MCU will not wake with STDIO remapped but it can be remapped with CMD:DBG.
// Set to true means that STDIO function is remapped to JTAG header by default so startup behaviour can be captured.
// ***** IMPORTANT ***** If the ISP programming header (J3) has been bypassed with jumpers then it will NOT be possible to debug the board with JTAG.
#define ACTIVATE_STDIO_REMAP_MCU0 true
#define ACTIVATE_STDIO_REMAP_MCU1 true  
#define ACTIVATE_STDIO_REMAP_MCU2 true

extern uint16_t BP_LOW_N;
extern uint16_t BP_HIGH_N; 
  
#define ENABLELOG          0 // Enables general purpose LOG macro
#define INCLUDE_JSON_LOG   0 // Enables dump_hexinfo functions and related user menu commands to enable/disable in session with... 
#define ACTIVATE_JSON_LOG  0 // Initial state of Activate_JSON_Log variable (used by dump_hexinfo)
#define JSON_DEBUG         0 // Enables JSON specific LOG macro

#if ENABLELOG
#define LOG(format, ...)                        \
    do                                          \
    {                                           \
        if(mcu_id == index) {                   \
            mcu_stdio_printf(format, ## __VA_ARGS__); \
            usleep(200);                        \
        }                                       \
    } while(0)
#define LOG1(format, ...)                        \
    do                                          \
    {                                           \
            mcu_stdio_printf(format, ## __VA_ARGS__); \
            usleep(200);                        \
    } while(0)

#else
#define LOG(format, ...)                        \
    do                                          \
    {                                           \
    } while(0)
#define LOG1(format, ...)                        \
    do                                          \
    {                                           \
    } while(0)
#endif

#if INCLUDE_JSON_LOG
#define JSON_LOG(format, ...)                   \
    do                                          \
    {                                           \
        if(mcu_id == index) {                   \
            mcu_stdio_printf(format, ## __VA_ARGS__); \
            usleep(200);                        \
        }                                       \
    } while(0)
#else
#define JSON_LOG(format, ...)                        \
    do                                          \
    {                                           \
    } while(0)
#endif


#if JSON_DEBUG
#define JSON_debug_print(format, ...)           \
    do                                          \
    {                                           \
        if(mcu_id == index) {                   \
            mcu_stdio_printf(format, ## __VA_ARGS__); \
            usleep(200);                        \
        }                                       \
    } while(0)        
#define JSON_debug_print1(format, ...)           \
    do                                          \
    {                                           \
            mcu_stdio_printf(format, ## __VA_ARGS__); \
            usleep(200);                        \
    } while(0)        
#else
#define JSON_debug_print(format, ...)           \
    do                                          \
    {                                           \
                                                \
    } while(0)
#define JSON_debug_print1(format, ...)           \
    do                                          \
    {                                           \
                                                \
    } while(0)
#endif
// defines for APU debugging      
//#define DEBUG_APU_LINKED_LIST
//#define PAUSE_APU_CHART
	    
void Pause(char *str);	 
void ClrScr(void);
void moveTo(int row, int col);
void Cursor(int IO);

extern int mcu_stdio_printf(const char *fmt, ...);
void mcu_stdio_init(void* p);
uint8_t configure_apu_filters(uint32_t I2S_FS_target, uint8_t BP_Low_Freq_target, uint8_t BP_High_Freq_target);
void set_apu_bp_low_freq(uint32_t BP_Low_Freq_target);
void set_apu_bp_high_freq(uint32_t BP_High_Freq_target);
void set_apu_gain(float gain_r);
void set_FOV_Angle(float angle);
void set_AZM_Angle(float angle);

void set_firmware_version_string();
char* get_firmware_version_string();
void mcu_reboot();
const char* TF_string(bool flag);


const char* concat(char *str1, char *str2);
char as_printable_char(char c);
//const char * as_printable_string(char *str);

extern bool Terminal_Mode; // "Ctrl-T"
extern bool Enable_Host_Reporting;    // "H"
extern bool Enable_Barchart;  // "B"
extern bool Enable_Beamformer_Stats;
extern bool Enable_DIR_Buffer_Table; // "R" - mutually exclusive with "D"
extern bool Enable_DIR_Results_Table;  // "D" - mutually exclusive with "R"
extern bool Enable_I2S_Data_Capture; // "s" - Enabled along with with I, F|f or S|s
extern bool Enable_I2S_Hard_FFT; // "f"
extern bool Enable_I2S_Soft_FFT; // "s"
extern bool Enable_I2S_Data_Table; // "I"
extern bool Enable_I2S_Hard_FFT_Table; // "F"
extern bool Enable_I2S_Soft_FFT_Table; // "S"
extern bool Enable_i2s_receive_data_all;  // use i2s_receive_data_all() when enabled
extern bool Reset_i2s_rx_fifo_before_use; // flush stale data before use
extern bool Enable_i2s_rx_buff_padding; // Allows discard of 1st 96 samples and provides anotehr 32 on end for DAS beamformer use
extern bool Force_WDT0_Trip; // "r"
extern bool Force_WDT1_Trip; // "R"
extern bool WDT0_Enable; // "w"
extern bool WDT1_Enable; // "W"
//extern bool Wait_for_Letter_C;      // "C" to continue
extern bool ClrScr_Done;
extern bool angle_flipped;
extern float DIR_Sens;
extern bool Enable_BF_FSM_Debug;
extern bool BF_FSM_STATE_CHANGE;
extern bool Enable_LPM;
extern bool Activate_JSON_Log;
extern uint8_t  MPA_N;
extern bool MPA_N_changed;
extern void apu_set_channel_enabled(uint8_t channel_bit);
extern  int16_t Trial_ID, Trial_Num;  // Counters to group data for related test samples
extern float VRMS_MPA;   // RMS of MPA energy in signal picked up by mics
extern float DBFS;   // DB full scale of VRMS = 20*log10(VRMS_MPA/2^15)
extern float DBFS_CORR;     // Corrected DBFS = ((DBFS_MPA + DBFS_CORRECT_ADD) * DBFS_CORRECT_MULT) - 95.0; 
extern uint16_t I2S_Rx_Buff_Depth;  // Adjustment for depth of buffer used for direct I2S reads.... issue with samples<64 and on the other end as well.... 256 might be the right size  with +96 on front & +32 on end for DAS use
extern uint8_t Host_Update_Interval;
extern uint8_t Host_Update_Counter;
extern bool Enable_MCU1;
extern bool Enable_MCU2;
extern bool Enable_Charger;
extern bool Enable_VOC_FFT;

#if INCLUDE_STDIO_REMAP
extern bool Enable_Remap_STDIO;
#endif  
extern uint16_t APU_Gain_u;
extern float APU_Gain; // real number version
extern float NF_Adjust;
extern float Noise_Floor;
extern bool DBFS_UE_Adj;
extern bool Enable_AZM_INC_Data;
extern bool Angle_BF_INC_auto_EN;
extern bool Angle_BF_AZM_auto_EN;
extern bool Assign_INC_to_DUT;
extern bool Select_Angle_DUT1;
extern bool Select_DBFS_DUT1;
extern bool Debug_AZM_BF;
extern float IN_FOV_Angle_LVL;
extern float ON_TARGET_Angle_LVL;
extern float Angle_Hysteresis;


extern bool Enable_DUT_Test;
extern bool Enable_All_ANGLE;
extern bool Enable_All_DBFS;
extern bool Core_Loop_Time_Test;
extern uint64_t core0_us_start;
extern uint32_t core0_us_elapsed;
extern uint64_t core1_us_start;
extern uint32_t core1_us_elapsed;




//extern uint32_t I2S_FS_actual, I2S_FS_target;
extern uint32_t I2S_FS_actual;  // All freq in Hz
extern uint32_t BP_Low;
extern uint32_t BP_High;
extern float Angle_BF_AZM_static;
extern float Angle_BF_INC_static;
extern uint32_t System_Clock_Freq_Target;
extern uint32_t System_Clock_Freq_Actual;    

extern uint8_t mcu_id;
extern uint8_t mcu_uart_dev;

extern bool Enable_Stdio;

extern volatile uint32_t send_done_count;
extern bool AGC_Enable;
extern uint16_t AGC_Adjust_Rate;
extern uint8_t AZM_INC_BF_Duty_Cycle;

extern bool Force_IDLE;



#define DIR_DATA_TABLE_SAMPLE_SIZE 100
#define MPA_NMAX 64 // Number of points used in angle Moving Point Average (MPA)
#define MPA_NMAX_INITIAL 20 // Initial # points in MPA

#define HOST_UPDATE_INTERVAL 8

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288
#endif

typedef enum _GPIO_Index
{
    GPIO_0_MCU_ID0    = 0,
    GPIO_1_MCU_ID1    = 1,
    GPIO_2_MCU1_EN    = 2,
    GPIO_3_MCU2_EN    = 3,
    GPIO_4_ACPRn      = 4,
    GPIO_5_BAT_FAULTn = 5,
    GPIO_6_CHRGn      = 6,
    GPIO_7_CHARGE_EN  = 7
} GPIO_Index;         
                      
typedef enum _MCU_Ids 
{                     
  MCU0,               
  MCU1,               
  MCU2,
  MCU3
} MCU_IDs;            
typedef enum _GPIOHS_Index {
     // SPI_MASTER_VOLTAGE_CS_IO
    GPIOHS_0_TCK_TEST   =  0,
    GPIOHS_1_MCU1_SS    =  1,
    GPIOHS_2_MCU2_SS    =  2,
    GPIOHS_3_MCU1_RDY   =  3,
    GPIOHS_4_MCU2_RDY   =  4,
    GPIOHS_5            =  5,
    GPIOHS_6_VOLTAGE_SS =  6,
    GPIOHS_7_CURRENT_SS =  7,
    GPIOHS_8_MCU1_INT   =  8,
    GPIOHS_9_MCU2_INT   =  9,
    GPIOHS_10           = 10,
    GPIOHS_11           = 11,
    GPIOHS_12           = 12,
    GPIOHS_13           = 13,
    GPIOHS_14           = 14,
    GPIOHS_15           = 15,
    GPIOHS_16           = 16,
    GPIOHS_17           = 17,
    GPIOHS_18           = 18,
    GPIOHS_19           = 19,
    GPIOHS_20           = 20,
    GPIOHS_21           = 21,
    GPIOHS_22           = 22,
    GPIOHS_23           = 23,
    GPIOHS_24           = 24,
    GPIOHS_25           = 25,
    GPIOHS_26           = 26,
    GPIOHS_27           = 27,
    GPIOHS_28           = 28,
    GPIOHS_29           = 29,
    GPIOHS_30           = 30,
    GPIOHS_31           = 31    
} GPIOHS_Index; // These should be changed to reflect 

typedef enum _TEST_State
{                     
    TEST_DISABLE, // Test feature is completely disabled... no test data dump support available
    TEST_INIT,    // Initialize test # counters and system parameters
    TEST_RUN,     // Run configured test 
    TEST_WAIT,    // Test is paused waiting for instruction via JSON command
    TEST_FINISH   // Test is complete or should be ended
} TEST_State_t;


extern char bf_state_str_trim[30];
extern TEST_State_t TEST_State;


// Console colors
#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"


#endif

