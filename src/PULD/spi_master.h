/* Copyright 2018 Canaan Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SPI_MASTER__
#define __SPI_MASTER__

#include <stdint.h>
#include "spi.h"

// The INT_PIN is needed on PCB version OL1, not on OL2
#define SPI_MASTER_MCU1_INT_PIN      PIN_MCU_LINK10
#define SPI_MASTER_MCU1_INT_IO       GPIOHS_8_MCU1_INT
#define SPI_MASTER_MCU2_INT_PIN      PIN_MCU_LINK13
#define SPI_MASTER_MCU2_INT_IO       GPIOHS_9_MCU2_INT


#define SPI_MASTER_MCU1_RDY_PIN      PIN_MCU_LINK01
#define SPI_MASTER_MCU1_RDY_IO       GPIOHS_3_MCU1_RDY
#define SPI_MASTER_MCU2_RDY_PIN      PIN_MCU_LINK03
#define SPI_MASTER_MCU2_RDY_IO       GPIOHS_4_MCU2_RDY


#define SPI_MASTER_MCU1_CS_PIN       PIN_MCU_LINK00
#define SPI_MASTER_MCU1_CS_IO        GPIOHS_1_MCU1_SS
#define SPI_MASTER_MCU2_CS_PIN       PIN_MCU_LINK02
#define SPI_MASTER_MCU2_CS_IO        GPIOHS_2_MCU2_SS

#define SPI_MASTER_CLK_PIN      PIN_MCU_LINK11
#define SPI_MASTER_MOSI_PIN     PIN_MCU_LINK12
#define SPI_MASTER_MISO_PIN     PIN_MCU_LINK12

int spi_master_init(void);
int spi_master_transfer(uint8_t slave_index, uint8_t *data, uint32_t addr, uint32_t len, uint8_t mode);
void spi_master_init_slave_mcu1(void);
void spi_master_init_slave_mcu2(void);
uint32_t spi_master_get_data_address_slave_mcu1(void);
uint32_t spi_master_get_data_address_slave_mcu2(void);
#endif
