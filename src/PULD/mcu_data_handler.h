#ifndef __mcu_DATA_HANDLER_H__
#define __mcu_DATA_HANDLER_H__
#include "mcu_util_queue.h"

void mcu_init_data_handler(uint8_t index);
void mcu_comm_event_handler(uint8_t index, void *param);
_mcu_util_queue_s* mcu_data_handle_get_data_set(uint8_t index);

#endif
