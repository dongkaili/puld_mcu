#ifndef __MCU_COMMANDS_HANDLER_H__
#define __MCU_COMMANDS_HANDLER_H__
#define COMMAND_VERSION "version"
#define COMMAND_REBOOT "reboot"
#define COMMAND_OTA_INFO "ota_info"
#define COMMAND_OTA_FRAME "ota_frame"
#define COMMAND_OTA_FRAM_RECEIVED "ota_frame_received"
#define COMMAND_OTA_RECEIVE_FINISHED "ota_receive_finished"
#define COMMAND_ERROR "error"
#define COMMAND_READ_I2S_RAW "i2s_raw"
#define COMMAND_READ_I2S_SOFT_FFT "i2s_soft_fft"
#define COMMAND_READ_I2S_HARD_FFT "i2s_hard_fft"
#define COMMAND_READ_I2S_RAW_HARD_FFT "i2s_fft_raw"
#define COMMAND_READ_DIR_RAW "dir_raw"
#define COMMAND_READ_VOC_RAW "voc_raw"
#define COMMAND_READ_VOC_FFT "voc_fft"



void mcu_init_commands_handler();
uint8_t command_invoker(uint8_t* command_string);
uint8_t commands_receiver();

#endif