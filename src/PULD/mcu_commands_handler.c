#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "uart.h"
#include "mcu_util_cli.h"
#include "mcu_commands_handler.h"
#include "mcu_internal_fsm.h"
#include "mcu_ota_handler.h"
#include "mcu_comm_protocol.h"
#include "mcu_common.h"
#include "mcu_mic_array.h"

extern void mcu_reboot();
extern void send_string(uart_device_number_t index, char* str, _data_header_s head_data);

typedef struct _command_parameter_ {
    char command[MAX_COMMAND_NAME_LEN];
    char option[MAX_COMMAND_NAME_LEN];
    char value[MAX_COMMAND_NAME_LEN];
    char target[MAX_COMMAND_NAME_LEN];
} _command_parameter_s;

typedef void (*args_callback_t)(_command_parameter_s* p);

static void mcu_command_func_version(void *p);
static void mcu_command_func_reboot(void *p);
static void mcu_command_func_ota_info(void *p);
static void mcu_command_func_ota_frame(void *p);

#if !FACTORY_RESET
static void mcu_command_func_read_i2s_raw(void *p);
static void mcu_command_func_read_i2s_hard_fft(void *p);
static void mcu_command_func_read_i2s_soft_fft(void *p);
static void mcu_command_func_read_i2s_raw_hard_fft(void *p);
static void mcu_command_func_read_dir_raw(void *p);
static void mcu_command_func_read_voc_raw(void *p);
static void mcu_command_func_read_voc_fft(void *p);
#endif

typedef enum {
    INDEX_COMMAND_VERSION,
    INDEX_COMMAND_REBOOT,
    INDEX_COMMAND_OTA_INFO,
    INDEX_COMMAND_OTA_FRAME,
#if !FACTORY_RESET
    INDEX_COMMAND_I2S_RAW,
    INDEX_COMMAND_I2S_SOFT_FFT,
    INDEX_COMMAND_I2S_HARD_FFT,
    INDEX_COMMAND_I2S_RAW_HARD_FFT,
    INDEX_COMMAND_DIR_RAW,
    INDEX_COMMAND_VOC_RAW,
    INDEX_COMMAND_VOC_FFT
#endif
} MCU_COMMAND_INDEX_E;

static mcu_command_s MCU_COMMANDS_TABLE[] = {
    {INDEX_COMMAND_VERSION, COMMAND_VERSION,  mcu_command_func_version},
    {INDEX_COMMAND_REBOOT, COMMAND_REBOOT,  mcu_command_func_reboot},
    {INDEX_COMMAND_OTA_INFO, COMMAND_OTA_INFO,  mcu_command_func_ota_info},
    {INDEX_COMMAND_OTA_FRAME, COMMAND_OTA_FRAME,  mcu_command_func_ota_frame},
#if !FACTORY_RESET
    {INDEX_COMMAND_I2S_RAW, COMMAND_READ_I2S_RAW, mcu_command_func_read_i2s_raw},
    {INDEX_COMMAND_I2S_SOFT_FFT, COMMAND_READ_I2S_SOFT_FFT, mcu_command_func_read_i2s_soft_fft},
    {INDEX_COMMAND_I2S_HARD_FFT, COMMAND_READ_I2S_HARD_FFT, mcu_command_func_read_i2s_hard_fft},
    {INDEX_COMMAND_I2S_RAW_HARD_FFT, COMMAND_READ_I2S_RAW_HARD_FFT, mcu_command_func_read_i2s_raw_hard_fft},
    {INDEX_COMMAND_DIR_RAW, COMMAND_READ_DIR_RAW, mcu_command_func_read_dir_raw},
    {INDEX_COMMAND_VOC_RAW, COMMAND_READ_VOC_RAW, mcu_command_func_read_voc_raw},
    {INDEX_COMMAND_VOC_FFT, COMMAND_READ_VOC_FFT, mcu_command_func_read_voc_fft}
#endif    
    
};

static void parameter_proc(_command_parameter_s* parameter) {
    // void* parameter = NULL;
    uint32_t table_size = sizeof(MCU_COMMANDS_TABLE)/sizeof(mcu_command_s);
    mcu_ota_info_s* p_info = mcu_ota_info();
    for (uint16_t i = 0; i < table_size; i++) {
        char* p = strstr(parameter->command, MCU_COMMANDS_TABLE[i].command_name);
        if(p == parameter->command) {
            switch(i) {
                case INDEX_COMMAND_VERSION: {
                    if (0 == strcasecmp(parameter->target, "target")) {
                        p_info->target_mcu = atoi(parameter->value);
                    }
                    break;
                }
                case INDEX_COMMAND_REBOOT: {
                    break;
                }
                case INDEX_COMMAND_OTA_INFO: {
                    p_info->cur_config_info.cipher_flag = 0;
                    if (0 == strcasecmp(parameter->option, "crc")) {
                        p_info->crc = strtoul(parameter->value, NULL, 16);
                    } else if(0 == strcasecmp(parameter->option, "version")) {
                        memset(p_info->cur_config_info.app_desc, 0, 16);
                        sprintf(p_info->cur_config_info.app_desc, "%s", parameter->value);
                    } else if(0 == strcasecmp(parameter->option, "count")) {
                        p_info->frame_count = strtoul(parameter->value, NULL, 10);
                    } else if(0 == strcasecmp(parameter->option, "size")) {
                        p_info->bin_size = atol(parameter->value);
                    } else if (0 == strcasecmp(parameter->option, "target")) {
                        p_info->target_mcu = atoi(parameter->value);
                    } else if (0 == strcasecmp(parameter->option, "cipher")) {
                        p_info->cur_config_info.cipher_flag = atol(parameter->value);
                    }
                    break;
                }
                case INDEX_COMMAND_OTA_FRAME: {
                    if(0 == strcasecmp(parameter->option, "index")) {
                        p_info->cur_frame_info.index = atoi(parameter->value);
                    } else if(0 == strcasecmp(parameter->option, "size")) {
                        p_info->cur_frame_info.size = atol(parameter->value);
                    }
                    break;
                }
            }
        }
    }
    //printf("crc:0x%08x, index=%d\n\r", p_info->crc, p_info->cur_frame_info.index);
}

static void convert_parameters(char *str, const char *sep1, const char *sep2, char* target, char* start, char* end, char* channel) 
{
    // int options_count = sub_count(str, sep1);
    _command_parameter_s parameter;
    char *dup_str, *p, *p_left1, *p_left2, *result;
    dup_str = p = strdup(str);
    p_left1 = p_left2 = result = NULL;
    int i_count = 0;
    while((result = strtok_r( p, sep1, &p_left1))) {
        if(i_count == 0) {
            strcpy(parameter.command, result);
        }
        i_count ++;
        // printf("p_left1 is \"%s\"\n\r", p_left1);
        int i_inner_count = 0;
        while((result = strtok_r(result, sep2, &p_left2))) {
            if(i_count > 1) {
                i_inner_count ++;
                if(i_inner_count > 1) {
                    strcpy(parameter.value, result);
                    parameter_proc(&parameter);
                } else {
                    strcpy(parameter.option, result);
                }
                if ((strcmp(parameter.option, "target") == 0) && (target != NULL)) {
                    strcpy(target, parameter.value);
                }
                if ((strcmp(parameter.option, "start") == 0) && (start != NULL)) {
                    strcpy(start, parameter.value);
                }
                if ((strcmp(parameter.option, "end") == 0) && (end != NULL)) {
                    strcpy(end, parameter.value);
                }
                if ((strcmp(parameter.option, "channel") == 0) && (channel != NULL)) {
                    strcpy(channel, parameter.value);
                }
                
            }
            // printf( "inner p_left2 is \"%s\"\n\r", p_left2);
            result = NULL;
        }
        p = NULL;
    }
    free(dup_str);
    free(p);
    //printf( "str \"%s\"\n\r", str );
}

static void mcu_command_func_version(void *p) {
    char command[MAX_COMMAND_STRING_LEN] = {0};
    char target[MAX_COMMAND_STRING_LEN] = {0};
    
    strcpy(command, (char*)p);
    //printf("command::%s\n\r", command);
    convert_parameters(command, "-", " ", target, NULL, NULL, NULL);
    
    uint8_t target_mcu = atoi(target);
    printf("target_mcu:%d\n\r", target_mcu);
    #if (0) // for debug
    char strTemp[64];
    sprintf(strTemp, "target # is: %d", target_mcu);    
    send_string(MCU0, strTemp, head_data);
    #endif 
    
    
    if (target_mcu == mcu_id) {
        
        _data_header_s head_data = {
            .head = 0x11,
            .version = 0x02,
            .type = 0x0502,
        };
        char strTemp[128];
        //sprintf(strTemp, "fw version: 1.0.%d", mcu_id);
		mcu_ota_info_s* p_info = mcu_ota_info();
        memset(command, 0, MAX_COMMAND_STRING_LEN);
        sprintf(command, "version -v %s", p_info->cur_config_info.app_desc);
        //sprintf(strTemp, "version 1.0.%d", mcu_id);	
        send_string(mcu_uart_dev, strTemp, head_data);		
    } else {
        _data_header_s head_data = {
            .head = 0x11,
            .version = 0x02,
            .type = 0x0502,
        };
        //printf("pass command %s to mcu %d\r\n", command, target_mcu);
        send_string(target_mcu, command, head_data);
    }
    
}

static void mcu_command_func_reboot(void *p) {
    mcu_reboot();
}

static void mcu_command_func_ota_info(void *p) {
    char command[MAX_COMMAND_STRING_LEN] = {0};
    strcpy(command, (char*)p);
    //printf("command:%s\n\r", command);
    convert_parameters(command, "-", " ", NULL, NULL, NULL, NULL);
    _event_ota_s data;
    data.event_type = OTA_EVENT_GOT_OTA_INFO;
    data.data = NULL;
    mcu_ota_event_handler(&data);
}

static void mcu_command_func_ota_frame(void *p) {
    char command[MAX_COMMAND_STRING_LEN] = {0};
    strcpy(command, (char*)p);
    // printf("command:%s\n\r", command);
    convert_parameters(command, "-", " ", NULL, NULL, NULL, NULL);
    _event_ota_s data;
    data.event_type = OTA_EVENT_GOT_FRAME_INFO;
    data.data = NULL;
    mcu_ota_event_handler(&data);
}

#if !FACTORY_RESET
static void mcu_command_func_read_i2s_raw(void *p)
{
    char command[MAX_COMMAND_STRING_LEN] = {0};
    char target[MAX_COMMAND_STRING_LEN] = {0};
    char start[MAX_COMMAND_STRING_LEN] = {"1"};
    char end[MAX_COMMAND_STRING_LEN] = {"512"};
    char channel[MAX_COMMAND_STRING_LEN] = {"0xff"};
    strcpy(command, (char*)p);
    //printf("command:%s\n\r", command);
    convert_parameters(command, "-", " ", target, start, end, channel);
    
    uint8_t target_mcu = atoi(target);
    uint16_t start_sample = atoi(start);
    uint16_t end_sample = atoi(end);
    uint16_t channel_mask = (uint16_t)strtoul(channel, NULL, 16);
    //printf("target_mcu:%d, start_sample:%d, end_sample:%d, channel_mask:%x\n\r", target_mcu, start_sample, end_sample, channel_mask);
    
    if (target_mcu == mcu_id)
    {
        send_i2s_raw_data(target_mcu, start_sample, end_sample, channel_mask);    
    }
    else
    {
        _data_header_s head_data_i2s_raw = {
            .head = 0x11,
            .version = 0x02,
            .type = COMM_MESSAGE_FORMAT_TYPE_L_I2S,
        };
        head_data_i2s_raw.type = head_data_i2s_raw.type << 8;
        //pass command to target mcu
        send_string(target_mcu, command, head_data_i2s_raw);    
    }     

}

static void mcu_command_func_read_i2s_soft_fft(void *p)
{
    char command[MAX_COMMAND_STRING_LEN] = {0};
    char target[MAX_COMMAND_STRING_LEN] = {0};
    char start[MAX_COMMAND_STRING_LEN] = {"1"};
    char end[MAX_COMMAND_STRING_LEN] = {"256"};
    char channel[MAX_COMMAND_STRING_LEN] = {"0xff"};
    strcpy(command, (char*)p);
    //printf("command:%s\n\r", command);
    convert_parameters(command, "-", " ", target, start, end, channel);
    uint8_t target_mcu = atoi(target);
    uint16_t start_sample = atoi(start);
    uint16_t end_sample = atoi(end);
    uint16_t channel_mask = (uint16_t)strtol(channel, NULL, 16);
    //printf("target_mcu:%d, start_sample:%d, end_sample:%d\n\r", target_mcu, start_sample, end_sample);
    if (target_mcu == mcu_id)
    {
        send_i2s_fft_data(target_mcu, start_sample, end_sample, channel_mask);
    }
    else
    {
        _data_header_s head_data_i2s_fft = {
            .head = 0x11,
            .version = 0x02,
            .type = COMM_MESSAGE_FORMAT_TYPE_L_I2S_FFT,
        };
        head_data_i2s_fft.type = head_data_i2s_fft.type << 8;
        //pass command to target mcu
        send_string(target_mcu, command, head_data_i2s_fft); 
    }
    

}

static void mcu_command_func_read_i2s_hard_fft(void *p)
{
    char command[MAX_COMMAND_STRING_LEN] = {0};
    char target[MAX_COMMAND_STRING_LEN] = {0};
    char start[MAX_COMMAND_STRING_LEN] = {"1"};
    char end[MAX_COMMAND_STRING_LEN] = {"256"};
    char channel[MAX_COMMAND_STRING_LEN] = {"0xff"};
    strcpy(command, (char*)p);
    //printf("command:%s\n\r", command);
    convert_parameters(command, "-", " ", target, start, end, channel);
    uint8_t target_mcu = atoi(target);
    uint16_t start_sample = atoi(start);
    uint16_t end_sample = atoi(end);
    uint16_t channel_mask = (uint16_t)strtol(channel, NULL, 16);
    //printf("target_mcu:%d, start_sample:%d, end_sample:%d\n\r", target_mcu, start_sample, end_sample);
    if (target_mcu == mcu_id)
    {
        send_i2s_fft_data(target_mcu, start_sample, end_sample, channel_mask);
    }
    else
    {
        _data_header_s head_data_i2s_fft = {
            .head = 0x11,
            .version = 0x02,
            .type = COMM_MESSAGE_FORMAT_TYPE_L_I2S_FFT,
        };
        head_data_i2s_fft.type = head_data_i2s_fft.type << 8;
        //pass command to target mcu
        send_string(target_mcu, command, head_data_i2s_fft); 
    }   

}

static void mcu_command_func_read_i2s_raw_hard_fft(void *p)
{
    char command[MAX_COMMAND_STRING_LEN] = {0};
    char target[MAX_COMMAND_STRING_LEN] = {0};
    char start[MAX_COMMAND_STRING_LEN] = {0};
    char end[MAX_COMMAND_STRING_LEN] = {0};
    strcpy(command, (char*)p);
    //printf("command:%s\n\r", command);
    convert_parameters(command, "-", " ", target, start, end, NULL);
    uint8_t target_mcu = atoi(target);
    //uint16_t start_sample = atoi(start);
    //uint16_t end_sample = atoi(end);
    //printf("target_mcu:%d, start_sample:%d, end_sample:%d\n\r", target_mcu, start_sample, end_sample);
    if (target_mcu == mcu_id)
    {
        send_i2s_raw_and_fft_data(target_mcu);
    }
    else
    {
        _data_header_s head_data_raw_fft = {
            .head = 0x11,
            .version = 0x02,
            .type = COMM_MESSAGE_FORMAT_TYPE_L_I2S_RAW_FFT,
        };
        head_data_raw_fft.type = head_data_raw_fft.type << 8;
        //pass command to target mcu
        send_string(target_mcu, command, head_data_raw_fft); 
    }   

}


static void mcu_command_func_read_dir_raw(void *p)
{
    char command[MAX_COMMAND_STRING_LEN] = {0};
    char target[MAX_COMMAND_STRING_LEN] = {0};
    char start[MAX_COMMAND_STRING_LEN] = {"257"};
    char end[MAX_COMMAND_STRING_LEN] = {"512"};
    char channel[MAX_COMMAND_STRING_LEN] = {"0xffff"};
    strcpy(command, (char*)p);
    //printf("command:%s\n\r", command);
    convert_parameters(command, "-", " ", target, start, end, channel);
    
    uint8_t target_mcu = atoi(target);
    uint16_t start_sample = atoi(start);
    uint16_t end_sample = atoi(end);
    uint16_t channel_mask = (uint16_t)strtol(channel, NULL, 16);
    //printf("target_mcu:%d, start_sample:%d, end_sample:%d\n\r", target_mcu, start_sample, end_sample);
    
    if (target_mcu == mcu_id)
    {
        send_dir_raw_data(target_mcu, start_sample, end_sample, channel_mask);    
    }
    else
    {
        _data_header_s head_data_dir_raw = {
            .head = 0x11,
            .version = 0x02,
            .type = COMM_MESSAGE_FORMAT_TYPE_L_DIR,
        };
        head_data_dir_raw.type = head_data_dir_raw.type << 8;
        //pass command to target mcu
        send_string(target_mcu, command, head_data_dir_raw);    
    } 
}

#endif

static void mcu_command_func_read_voc_raw(void *p)
{
    char command[MAX_COMMAND_STRING_LEN] = {0};
    char target[MAX_COMMAND_STRING_LEN] = {0};
    char start[MAX_COMMAND_STRING_LEN] = {"1"};
    char end[MAX_COMMAND_STRING_LEN] = {"512"};
    //char channel[MAX_COMMAND_STRING_LEN] = {"0x01"};
    strcpy(command, (char*)p);
    //printf("command:%s\n\r", command);
    convert_parameters(command, "-", " ", target, start, end, NULL);
    
    uint8_t target_mcu = atoi(target);
    uint16_t start_sample = atoi(start);
    uint16_t end_sample = atoi(end);
    //uint16_t channel_mask = (uint16_t)strtol(channel, NULL, 16);
    //printf("target_mcu:%d, start_sample:%d, end_sample:%d\n\r", target_mcu, start_sample, end_sample);
    
    if (target_mcu == mcu_id)
    {
        send_voc_raw_data(target_mcu, start_sample, end_sample);    
    }
    else
    {
        _data_header_s head_data_voc_raw = {
            .head = 0x11,
            .version = 0x02,
            .type = COMM_MESSAGE_FORMAT_TYPE_L_VOC,
        };
        head_data_voc_raw.type = head_data_voc_raw.type << 8;
        //pass command to target mcu
        send_string(target_mcu, command, head_data_voc_raw);    
    } 
}

static void mcu_command_func_read_voc_fft(void *p)
{
    char command[MAX_COMMAND_STRING_LEN] = {0};
    char target[MAX_COMMAND_STRING_LEN] = {0};
    char start[MAX_COMMAND_STRING_LEN] = {"1"};
    char end[MAX_COMMAND_STRING_LEN] = {"256"};
    //char channel[MAX_COMMAND_STRING_LEN] = {"0x01"};
    strcpy(command, (char*)p);
    //printf("command:%s\n\r", command);
    convert_parameters(command, "-", " ", target, start, end, NULL);
    
    uint8_t target_mcu = atoi(target);
    uint16_t start_sample = atoi(start);
    uint16_t end_sample = atoi(end);
    //uint16_t channel_mask = (uint16_t)strtol(channel, NULL, 16);
    //printf("target_mcu:%d, start_sample:%d, end_sample:%d\n\r", target_mcu, start_sample, end_sample);
    
    if (target_mcu == mcu_id)
    {
        send_voc_fft_data(target_mcu, start_sample, end_sample);    
    }
    else
    {
        _data_header_s head_data_voc_fft = {
            .head = 0x11,
            .version = 0x02,
            .type = COMM_MESSAGE_FORMAT_TYPE_L_VOC_FFT,
        };
        head_data_voc_fft.type = head_data_voc_fft.type << 8;
        //pass command to target mcu
        send_string(target_mcu, command, head_data_voc_fft);    
    } 
}

_mcu_util_queue_s commands_queue;

uint8_t command_invoker(uint8_t* command_string) {
#if MASS_STRING_SEND_TEST
    printf("command_invoker:%s\n\r", command_string);
#endif
    uint32_t table_size = sizeof(MCU_COMMANDS_TABLE)/sizeof(mcu_command_s);
    return mcu_util_cli_invoker((char*)command_string, MCU_COMMANDS_TABLE, table_size, &commands_queue);
}

//can be called from any core
uint8_t commands_receiver() {
    return mcu_util_cli_receiver(&commands_queue);
}

static void mcu_release_commands_buffer(void *elem)
{
    uint8_t* p = (*(uint8_t **)elem);
    // LOG("free address: 0x%08lx  ]\n\r", (uint64_t)p);
	free(p);
}

void mcu_init_commands_handler() {
    mcu_util_queue_init(&commands_queue, 10, sizeof(mcu_command_data_s), mcu_release_commands_buffer);
}
