#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include "sysctl.h"
#include "mcu_comm_protocol.h"
#include "mcu_internal_fsm.h"
#include "mcu_data_handler.h"
#include "mcu_commands_handler.h"
#include "mcu_uart.h"
#include "mcu_device_management.h"
#include "mcu_json_commands.h"
#include "gpio.h"
#include "mcu_common_define.h"

extern uint8_t enable_mcu1_bf_data_read;
extern uint8_t enable_mcu2_bf_data_read;
extern uint64_t mcu1_bf_data_ready_count;
extern uint64_t mcu2_bf_data_ready_count;
extern uint64_t mcu1_bf_data_read_count;
extern uint64_t mcu2_bf_data_read_count;

bool Activate_JSON_Log = ACTIVATE_JSON_LOG;

// typedef struct {
//     uint8_t c;
//     COMM_PARSE_EVENT_E e;
// } s_event_data;

typedef struct {
    mcu_fsm_state_t state;
    mcu_comm_parse_data_t data_parsed_callback;
    // mcu_comm_event_t event_callback;
    uint8_t version;
    uint8_t format;
    uint8_t msg_type;
    uint32_t payload_length;
    uint32_t payload_count;
    uint8_t checksum;
    uint8_t current_sum;
} mcu_protocol_context_s;

static mcu_protocol_context_s sc[UART_DEVICE_MAX];


mcu_fsm_state_t mcu_comm_get_fsm_state(uint8_t index)
{
    return sc[index].state;
}

void mcu_comm_set_fsm_state(uint8_t index, mcu_fsm_state_t state)
{
    sc[index].state = state;
}

uint32_t mcu_comm_get_length(uint8_t index) {
    return sc[index].payload_length;
}

uint8_t mcu_comm_get_format(uint8_t index) {
    return sc[index].format;
}

uint8_t mcu_comm_get_type(uint8_t index) {
    return sc[index].msg_type;
}

mcu_fsm_state_t mcu_comm_get_state(uint8_t index) {
    return sc[index].state;
}

uint8_t checksum(uint8_t *buf, uint32_t len) { 
    uint8_t sum = 0;

    for(sum = 0; len != 0; len --) {
        sum += *(buf++);
    }
    return ~sum;
}

void init_parser(uint8_t index, mcu_comm_parse_data_t data_callback) {//}, mcu_comm_event_t event_callback) {
    //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    static int init_count = 0;
    sc[index].data_parsed_callback = data_callback;
    // sc.event_callback = event_callback;
    sc[index].state = S_COMM_INIT;
    mcu_init_data_handler(index);  
    init_count++;
    if (init_count == 1)
    {
        mcu_init_command_handler();
    }
    
}

static void send_event(uint8_t index, uint8_t type, uint8_t data) {
    //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    if(sc[index].data_parsed_callback != NULL) 
    {
        _event_comm_s e_data;
        e_data.event_type = type;
        e_data.data = (void *)data;
        sc[index].data_parsed_callback(index, &e_data);
    }

}

uint8_t mcu_comm_parse_data(uint8_t index, const uint8_t *buf, uint32_t len) {
    //LOG("%s,%s,%d\n\r",__FILE__,__FUNCTION__,__LINE__);
    if (index == 0) {
         // don't parse return messages from slaves... causing core fault in master
            dump_hexinfo(buf, len);
    }
    // LOG("sc.state: %d\n\r", sc.state);
    int32_t i = 0;
    uint32_t temp;
    while(i < len) 
    {
        // mcu_fsm_state_t pre_state = sc.state;
        uint8_t value = buf[i];
        i++;
        switch(sc[index].state) {
            case S_COMM_INIT:
            case S_COMM_IDLE: 
#if !FACTORY_RESET                
                enable_mcu1_bf_data_read = 1;
                enable_mcu2_bf_data_read = 1;
#endif
                if(value == COMM_MESSAGE_FORMAT_HEAD) 
                {
                    //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
#if !FACTORY_RESET                      
                    enable_mcu1_bf_data_read = 0;
                    enable_mcu2_bf_data_read = 0;
                    mcu1_bf_data_ready_count = 0L;
                    mcu2_bf_data_ready_count = 0L;
                    mcu1_bf_data_read_count = 0L;
                    mcu2_bf_data_read_count = 0L;
#endif                    
                    
                    sc[index].state = S_COMM_GOT_HEAD;
                    sc[index].payload_count = 0;
                    sc[index].current_sum = 0;

                    sc[index].format = 0;
                    sc[index].msg_type = 0;
                    sc[index].payload_length = 0;
                    sc[index].checksum = 0;
                    JSON_debug_print("received head %x\r\n", value);
                    // event_handler(&fsm, E_COMM_CHECK_HEAD);
                    send_event(index, E_COMM_CHECK_HEAD, value);
                } 
                else 
                {
                    //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
#if !FACTORY_RESET
                    enable_mcu1_bf_data_read = 1;
                    enable_mcu2_bf_data_read = 1;
#endif                
                    sc[index].state = S_COMM_IDLE;
                    // LOG("%s,%s,%d\n\r",__FILE__,__FUNCTION__,__LINE__);
                    send_event(index, E_COMM_ERROR, 0);
                }
                break;
            
            case S_COMM_GOT_HEAD: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                sc[index].state = S_COMM_GOT_VER;
                sc[index].version = value;
                JSON_debug_print("received version %x\r\n", sc[index].version);
                send_event(index, E_COMM_GET_VERSION, value);
    //             if(value == COMM_MESSAGE_FORMAT_VERSION) {
    // // LOG("%s,%s,%d\n\r",__FILE__,__FUNCTION__,__LINE__);
    //                 sc.state = S_COMM_GOT_VER;
    //                 sc.version = value;
    //             } else {
    //                 sc.state = S_COMM_IDLE;
    //                 send_event(E_COMM_ERROR, 0);
    //             }
                break;
            
            case S_COMM_GOT_VER: 
                //LOG("%s,%s,%d\n\r",__FILE__,__FUNCTION__,__LINE__);
                sc[index].state = S_COMM_GOT_TYPE_H;
                sc[index].format = value;
                JSON_debug_print("received data format %x\r\n", sc[index].format);
                send_event(index, E_COMM_GET_FORMAT, value);
                break;
            
            case S_COMM_GOT_TYPE_H: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                sc[index].state = S_COMM_GOT_TYPE_L;
                sc[index].msg_type = value;
                JSON_debug_print("received msg type %x\r\n", sc[index].msg_type);
                send_event(index, E_COMM_GET_TYPE, value);
                break;
            
            case S_COMM_GOT_TYPE_L: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                sc[index].state = S_COMM_GOT_LENGTH1;
                sc[index].payload_length |= value;
                
                break;
            
            case S_COMM_GOT_LENGTH1: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                sc[index].state = S_COMM_GOT_LENGTH2;
                temp = value;
                sc[index].payload_length += (temp << 8);
                break;
            
            case S_COMM_GOT_LENGTH2: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                sc[index].state = S_COMM_GOT_LENGTH3;
                temp = value;
                sc[index].payload_length += (temp << 16);
                break;
            
            case S_COMM_GOT_LENGTH3: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                sc[index].state = S_COMM_GOT_LENGTH4;
                temp = value;
                sc[index].payload_length += (temp << 24);
                JSON_debug_print("received length %x\r\n", sc[index].payload_length);
                // sc.payload_count = 0;
                if(MCU_COMM_MAX_DATA_SIZE < sc[index].payload_length) {
                    LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
#if !FACTORY_RESET
                    enable_mcu1_bf_data_read = 1;
                    enable_mcu2_bf_data_read = 1;
#endif                
                    sc[index].state = S_COMM_IDLE;
                    send_event(index, E_COMM_ERROR, 0);
                    break;
                }
                send_event(index, E_COMM_GET_LENGTH, sc[index].payload_length);
                break;
            
            case S_COMM_GOT_LENGTH4: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                sc[index].state = S_COMM_GOT_CHECKSUM;
                sc[index].checksum = value;
                JSON_debug_print("received checksum %x\r\n", sc[index].checksum);
                //send_event(E_COMM_GET_CHECKSUM, sc.checksum);
                break;
            
            case S_COMM_GOT_CHECKSUM: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                JSON_debug_print("state = S_COMM_GOT_CHECKSUM\r\n");
                sc[index].state = S_COMM_GOT_RESERVED;
                break;
            
            case S_COMM_GOT_RESERVED: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                JSON_debug_print("state = S_COMM_GOT_RESERVED\r\n");
                sc[index].state = S_COMM_GOT_SEQID_H;
                break;
            
            case S_COMM_GOT_SEQID_H: 
                JSON_debug_print("state = S_COMM_GOT_SEQID_H\r\n");
                sc[index].state = S_COMM_GOT_SEQID_L;
                break;
            
            case S_COMM_GOT_SEQID_L:                
            case S_COMM_GOT_PAYLOAD: 
                //LOG("%s,%s,%d\n\r",__FILE__,__FUNCTION__,__LINE__);
                // printf("payload_count:%d, payload_length:%d\n\r", sc.payload_count, sc.payload_length);
                sc[index].state = S_COMM_GOT_PAYLOAD;
                sc[index].payload_count++;
                sc[index].current_sum += value;  
                JSON_debug_print("payload value is 0x%x[%c] with payload_count %dth data of total %d\r\n", value, as_printable_char(value), sc[index].payload_count, sc[index].payload_length);          // SEH         
                //sc.fsm.state = S_COMM_GOT_RESERVED;
                send_event(index, E_COMM_GET_PAYLOAD, value);
                
                if(sc[index].payload_count == sc[index].payload_length) 
                {
                    //LOG("%s,%s,%d\n\r",__FILE__,__FUNCTION__,__LINE__);
#if !FACTORY_RESET
                    enable_mcu1_bf_data_read = 1;
                    enable_mcu2_bf_data_read = 1;
#endif                
                    sc[index].state = S_COMM_IDLE;
    // LOG("sc.current_sum:0x%02x,sc.checksum:0x%02x\n\r",sc.current_sum,sc.checksum);
                    uint8_t result = sc[index].current_sum += sc[index].checksum;                    
                    //LOG("result:0x%02x\n\r", result);
                    if(result == 0xff) 
                    {
                        //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                        JSON_debug_print("current sum is %x\r\n", sc[index].current_sum);
                        send_event(index, E_COMM_DATA_VALIDATED, 0);
                    } 
                    else 
                    {
                        LOG("%s,%s,%d\n\r",__FILE__,__FUNCTION__,__LINE__);
                        JSON_debug_print("error -- result:0x%02x\n\r", result);
                        send_event(index, E_COMM_ERROR, 0);
                    }
                } 
                else if (sc[index].payload_count > sc[index].payload_length) 
                {
                    LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
#if !FACTORY_RESET
                    enable_mcu1_bf_data_read = 1;
                    enable_mcu2_bf_data_read = 1;
#endif

                    sc[index].state = S_COMM_IDLE;
                    JSON_debug_print("payload_count > payload_length\r\n");
                    send_event(index, E_COMM_ERROR, 0);
                }
                break;
            
            default: 
                //LOG("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
                LOG("ERROR occerd while current state is : %d\n\r", sc[index].state);
#if !FACTORY_RESET                
                enable_mcu1_bf_data_read = 1;
                enable_mcu2_bf_data_read = 1;
#endif                
                sc[index].state = S_COMM_IDLE;
                JSON_debug_print("state = default\r\n");
                send_event(index, E_COMM_ERROR, 0);
                //error
                break;
            
        }
    }
    return 0;
}

void dump_hexinfo(const uint8_t *buf, int32_t len) {
#if INCLUDE_JSON_LOG
    if (Activate_JSON_Log)
    {
        char hexstr[1025] = "";
        if (len > (1025 / 5)) {
            len = 1025 / 5;
        }
        for (uint32_t i = 0; i < len; i++) {
    #if(0)
            sprintf((char *)&hexstr[(i * 5)], "0x%02x,", *(buf + i));
    #else
            char c = *(buf + i);
            //printf("\nisprint result for character %c: %d\n\r", c, isprint(c));
            sprintf((char *)&hexstr[(i * 8)], "0x%02x:%c\n\r", *(buf + i), as_printable_char(c));
#endif
        }
        JSON_LOG("%s,%s\n\r", __FUNCTION__, hexstr);
    }
#endif
    
}

