#ifndef __MCU_MIC_ARRAY_H__
#define __MCU_MIC_ARRAY_H__

#include "apu_init.h"
#include <complex.h>


typedef struct CH_Data_
{
  int8_t CH;   // retain original channel number after sort
  uint32_t dir_sum;
} CH_Data_s;

typedef  struct _all_mcu_bf_data_ { // the info in here isnt BF specific
    float angle[3];
    float dbfs[3];
} _all_mcu_bf_data_s;

typedef enum {
    S_BF_IDLE		,
    S_BF_HEARD  	,
    S_BF_SEEN	    ,
    S_BF_STABLE	    ,
    S_BF_IN_VIEW	,
    S_BF_ON_TARGET	,
} BF_STATE;

typedef struct  _all_bf_data_
{
    BF_STATE BF_STATE[3];    // Current state of each MCU BF FSM
    _all_mcu_bf_data_s azm;  // all azimuthal data
    _all_mcu_bf_data_s inc;  // all inclinational data
    _all_mcu_bf_data_s dut;  // for A/B testing
}  _all_bf_data_s;  


typedef struct DIR_Result_                           // record containing all of the components used to compute MPA of angle & energy from one test to the next
{                                                  
    uint8_t ID_Max;                                  // DIR with max strength in current scan
    double complex DIR_SUM_Vect_MAX;                 // Vector for DIR test with max result... use with angle rotation tests but linked version for actual angle and stability tests and better DBFS estimate
    //double complex NORMALIZED_DIR_SUM_Vect_MAX;    // May not be used
    uint8_t N_linked;                                // Number of linked items from current (or indexed) DIR test... may be used to compute weighted MPA... not a bad idea as this will filter out spurious changes in DIR
    double complex Linked_DIR_SUM_Vect;              // resultant from indexed DIR test over "linked" vectors 
    //double complex NORMALIZED_Linked_DIR_SUM_Vect; // resultant from indexed DIR test over "linked" vectors (normalized)... used to build direction vectors 
} DIR_Result_s;

typedef struct Unit_DIR_Vectors_
{
    float angle;     // seed angle used by this BF'r... FOV for AZM, Angle_AZM for INC, FOV? for grid... test against ne value to decice if the UDV's need to be regenerated
    double complex V[16];
} Unit_DIR_Vectors_s;

typedef struct UV_
{
    double complex V;
    double rotation;
    double dispersion;
} UV_s;

typedef struct beamformer_
{
    uint8_t id;                                 // index/id of beamformer for self-idenfication
    char name[16];                              // Name string identifier for self reporting
    Unit_DIR_Vectors_s UDV;                     // unit vectors for each anle used in DIR tests
    float UDV_angle;                            // seed angle used by this BF'r... FOV for AZM, Angle_AZM for INC, FOV? for grid
    uint8_t delays[16][8];                      // current delays for this BF's. May be reused if the angle hasn;t changed but in prCtice this will always change... stored for reference/debugging  
    uint8_t MPA_Index;                          // Current position of insertion point in DIR_Result_BUF ring buffer.
    uint8_t MPA_N_Current;                      // Starts at 1 first time in and increases to MPA_NMAX+1 at which point DIR_Result_BUFF_full==true.... needed for startup conditions only and is invalid after DIR_Result_BUFF is full 
    DIR_Result_s DIR_Result_BUFF[MPA_NMAX];     // MPA ring buffer of DIR results... max depth: may be dynamically reduced in size. Start with N=1 and increase based on STDEV within buffer? Try to keep N and SDEV low.
    DIR_Result_s MPA; 
    
    // statisic accumulators - Using these should improve overall performanmce as it will reduce the amount of time in stat processing loop
    uint16_t SUM_N_linked;                      // uint16_t because this can grow to 64*256 based on max depth and field size
    double complex SUM_Linked_DIR_SUM_Vect;     // quickest wqay to calculate angle and dbfs magnitude (use carg anmd cabs functions)
    double complex SUM_DIR_SUM_Vect_MAX;        // accumlator for DIR_SUM_Vect_MAX data (as vector) for dbfs MPA

    UV_s UVL, UVM;                              // convenient access to final vector detail
    
    // output angle & dbfs data
    float angle_local, angle, angle_dut;
    float dbfs, dbfs_dut;
    
    // Limit values for condition tests;
    float dispersion_LIM, dispersion_HYS;
    float rotation_LIM, rotation_HYS;
    
    bool rotation_LT_LIM_minus_hys;  
    bool rotation_GT_LIM_plus_hys;   
    bool dispersion_LT_LIM_minus_hys;
    bool dispersion_GT_LIM_plus_hys; 

    // BF FSM inputs calculated from statistics
    bool is_Stable;
    bool is_Rot_Pos;
    bool is_Rot_Neg;
} beamformer_s;


typedef enum {
    AZM,  // Identifies azimuthal beamformer in array index
    INC,  // Identifies inclination beamformer in array index
    N_BF
} BF_enum;

typedef struct _slave_mcu_data_s_
{
    uint8_t  slave_id;
    uint8_t  check_sum;
    uint8_t  padding_0;
    uint8_t  padding_1;
    //uint8_t slave_data[SLAVE_DATA_LENGTH-2];         // placeholder for testing
    BF_STATE BF_STATE;                                 // slave MCU BF FSM, (take 4 bytes)
    
    float    azm_angle;
    float    inc_angle;
    float    dut_angle;
    float    azm_dbfs;
    float    inc_dbfs;
    float    dut_dbfs;

} slave_mcu_data_s;

#define SLAVE_DATA_LENGTH    (sizeof(slave_mcu_data_s)/sizeof(uint8_t))
//extern uint16_t BP_LOW_N, BP_HIGH_N;

//extern uint16_t BP_LOW_N;   // I2S data bandpass filter cutoff frequencies
//extern uint16_t BP_HIGH_N;

typedef void (* mic_callback) (void *parameter);

uint8_t mic_array_init(mic_callback callback);
void mic_array_loop(void);
void send_i2s_raw_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample, uint16_t channel_mask);
void send_i2s_fft_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample, uint16_t channel_mask);
void send_i2s_raw_and_fft_data(uint8_t mcu);
void send_dir_raw_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample, uint16_t channel_mask);
void send_voc_raw_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample);
void send_voc_fft_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample);


void send_structure_block_data(uint8_t mcu, uint8_t format_type, void* piData, uint8_t max_channel, uint16_t channel_size,
                          uint8_t data_size, uint16_t start_sample, uint16_t end_sample, uint16_t channel_mask);
                     
void data_link_loop(void);
int core_ipi_callback_func(void * ctx);
#endif
