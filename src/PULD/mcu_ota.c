#include <stdio.h>
#include "mcu_ota.h"
#include "mcu_comm_protocol.h"
#include "mcu_uart.h"
#include "mcu_ipc_sync.h"
#include "mcu_common.h"

extern uint8_t checksum(uint8_t *buf, uint32_t len);
#if OTA_TEST
static void ota_test_data_send(uint8_t* data, size_t len, uint16_t type) {
    size_t size = sizeof(_data_header_s)/sizeof(uint8_t) + len*sizeof(uint8_t);
    corelock_lock(&_init_lock);
    uint8_t* pdata = malloc(size);
    memset(pdata, 0, size);

    _data_header_s head_data = {
        .head = 0x11,
        .version = 0x02,
        .type = type,//0x0006,
    };
    head_data.length = len;
    head_data.checksum = checksum(data, head_data.length);

    memcpy(pdata, &head_data, sizeof(_data_header_s)/sizeof(uint8_t));
    memcpy(pdata + sizeof(_data_header_s)/sizeof(uint8_t), data, len);
    ota_test_data_receive(pdata, size);
    // printf("pdata : 0x%08lx\n\r", (uint64_t)pdata);
    free(pdata);
    corelock_unlock(&_init_lock);
}


static unsigned int random_ota(int m) {
    return rand()%m;
}
unsigned int data_size = 160*1024;
unsigned int i = 0;

void fake_ota_command() {
    char str_size[10] = {0};
    unsigned char str[MAX_COMMAND_STRING_LEN] = {0};
    sprintf(str, "ota_info -crc 0x1234 -count 1 -size %d", data_size);
    size_t len = strlen(str);
    ota_test_data_send(str, len, 0x0502);
    memset(str, 0, MAX_COMMAND_STRING_LEN);
    sprintf(str, "ota_frame -index 1 -size %d", data_size/1024);
    len = strlen(str);
    ota_test_data_send(str, len, 0x0502);
}

void fake_ota_data() {
    // if(i > 160*1024) {
    if(i >= data_size) {
        // mcu_stdio_printf("finished\n\r");
        return;
    }
    unsigned int increment = 1024;
    i = i + increment;
    if(i > data_size) {
        increment = i - data_size;
        i = data_size;
    }
    unsigned char str[2048] = {0};
    size_t len = 0;
    for(unsigned int j = 0; j < increment; j ++) {
        str[j] = (33 + random_ota(93));
        len = j + 1;
    }
    str[len] = 0;
    mcu_stdio_printf("i:%d\n\r", i);
    if(len > 0)
        ota_test_data_send(str, len, 0x0600);
}

#endif
#if MASS_STRING_SEND_TEST
void send_test_string_from_local() {
    static int iloop = 1000;
    if(iloop <= 0) {
        return;
    }
    iloop --;
    char* test_string1 = "=1234567890=";
    char* test_string2= "AbcdefghijklmnopqrstuvwxyZ";
    ota_test_data_send(test_string1, strlen(test_string1), 0x0502);
    ota_test_data_send(test_string2, strlen(test_string2), 0x0502);
}
#endif
