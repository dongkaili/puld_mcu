/* Copyright 2018 Canaan Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 #include <stdio.h>
#include "mcu_pins.h"
#include "mcu_common_define.h"
#include "spi_master.h"
#include "fpioa.h"
#include "gpiohs.h"
#include "clint.h"

uint64_t mcu1_bf_data_ready_count = 0L;
uint64_t mcu2_bf_data_ready_count = 0L;
extern uint8_t enable_mcu1_bf_data_read;
extern uint8_t enable_mcu2_bf_data_read;

uint32_t slave1_data_addr;
uint32_t slave2_data_addr;
static uint32_t slave1_addr_bak, count=0;
static uint32_t slave2_addr_bak;

uint8_t bf_data_ready_mcu_mask = 0;

#define WAIT_TIMEOUT            0xFFFFFFF


static void spi_master_set_pin(uint8_t slave_index, uint8_t io_pin, gpio_pin_value_t pin_value);

#define SPI_MASTER_MCU1_CS_LOW()    spi_master_set_pin(MCU1, SPI_MASTER_MCU1_CS_IO, GPIO_PV_LOW)
#define SPI_MASTER_MCU1_CS_HIGH()   spi_master_set_pin(MCU1, SPI_MASTER_MCU1_CS_IO, GPIO_PV_HIGH)

#define SPI_MASTER_MCU1_INT_LOW()    spi_master_set_pin(MCU1, SPI_MASTER_MCU1_INT_IO, GPIO_PV_LOW)
#define SPI_MASTER_MCU1_INT_HIGH()   spi_master_set_pin(MCU1, SPI_MASTER_MCU1_INT_IO, GPIO_PV_HIGH)

#define SPI_MASTER_MCU2_CS_LOW()    spi_master_set_pin(MCU2, SPI_MASTER_MCU2_CS_IO, GPIO_PV_LOW)
#define SPI_MASTER_MCU2_CS_HIGH()   spi_master_set_pin(MCU2, SPI_MASTER_MCU2_CS_IO, GPIO_PV_HIGH)

#define SPI_MASTER_MCU2_INT_LOW()    spi_master_set_pin(MCU2, SPI_MASTER_MCU2_INT_IO, GPIO_PV_LOW)
#define SPI_MASTER_MCU2_INT_HIGH()   spi_master_set_pin(MCU2, SPI_MASTER_MCU2_INT_IO, GPIO_PV_HIGH)

static volatile uint8_t spi_slave_mcu1_ready;
static volatile uint8_t spi_slave_mcu2_ready;



uint8_t mcu1_bf_data_ready_flag = 0;
uint8_t mcu2_bf_data_ready_flag = 0;



static uint8_t spi_master_send_mcu1_cmd_flag = 0;
static uint8_t spi_master_send_mcu2_cmd_flag = 0;


static void spi_slave_mcu1_ready_irq(void);
static void spi_slave_mcu2_ready_irq(void);



static void spi_master_set_pin(uint8_t slave_index, uint8_t io_pin, gpio_pin_value_t pin_value)
{
    if (MCU1 == slave_index)
    {
        gpiohs_set_pin(io_pin, pin_value);
    }
    else if (MCU2 == slave_index)
    {
        gpiohs_set_pin(io_pin, pin_value);
    }
}

void spi_master_init_slave_mcu1(void)
{
    // set initial value related to MCU1
    SPI_MASTER_MCU1_CS_HIGH();
    gpiohs_set_drive_mode(SPI_MASTER_MCU1_CS_IO, GPIO_DM_OUTPUT);
    
    SPI_MASTER_MCU1_INT_HIGH();
    gpiohs_set_drive_mode(SPI_MASTER_MCU1_INT_IO, GPIO_DM_OUTPUT);
    
    gpiohs_set_drive_mode(SPI_MASTER_MCU1_RDY_IO, GPIO_DM_INPUT_PULL_UP);    
    gpiohs_set_pin_edge(SPI_MASTER_MCU1_RDY_IO, GPIO_PE_FALLING);
    gpiohs_set_irq(SPI_MASTER_MCU1_RDY_IO, 3, spi_slave_mcu1_ready_irq);

    spi_master_send_mcu1_cmd_flag = 0;
    mcu1_bf_data_ready_count = 0L;
    enable_mcu1_bf_data_read = 1;
    //printf("spi_master_init_slave_mcu1\r\n");
}

void spi_master_init_slave_mcu2(void)
{
    // set initial values related to MCU2
    SPI_MASTER_MCU2_CS_HIGH();
    gpiohs_set_drive_mode(SPI_MASTER_MCU2_CS_IO, GPIO_DM_OUTPUT);
        
    SPI_MASTER_MCU2_INT_HIGH();
    gpiohs_set_drive_mode(SPI_MASTER_MCU2_INT_IO, GPIO_DM_OUTPUT);
    
    gpiohs_set_drive_mode(SPI_MASTER_MCU2_RDY_IO, GPIO_DM_INPUT_PULL_UP);    
    gpiohs_set_pin_edge(SPI_MASTER_MCU2_RDY_IO, GPIO_PE_FALLING);
    gpiohs_set_irq(SPI_MASTER_MCU2_RDY_IO, 5, spi_slave_mcu2_ready_irq);
    
    spi_master_send_mcu2_cmd_flag = 0;
    mcu2_bf_data_ready_count = 0L;
    enable_mcu2_bf_data_read = 1;
    //printf("spi_master_init_slave_mcu2\r\n");
}

uint32_t spi_master_get_data_address_slave_mcu1(void)
{
    count = 0;
    do
    {
        slave1_data_addr = 0;
        slave1_addr_bak = 0;
        spi_master_transfer(MCU1, (uint8_t *)&slave1_data_addr, 8, 4, READ_CONFIG);
        spi_master_transfer(MCU1, (uint8_t *)&slave1_addr_bak, 8, 4, READ_CONFIG);
        count++;
    } while (slave1_data_addr != slave1_addr_bak);
    //printf("count: %d, slave1_data_addr: 0x%x\r\n", count, slave1_data_addr);
    return slave1_data_addr;
}

uint32_t spi_master_get_data_address_slave_mcu2(void)
{
    // read the address of the data on slave mcu 2
    count = 0;
    do
    {
        slave2_data_addr = 0;
        slave2_addr_bak = 0;
        spi_master_transfer(MCU2, (uint8_t *)&slave2_data_addr, 8, 4, READ_CONFIG);
        spi_master_transfer(MCU2, (uint8_t *)&slave2_addr_bak, 8, 4, READ_CONFIG);
        count++;
    } while (slave2_data_addr != slave2_addr_bak);
    //printf("count: %d, slave2_data_addr: 0x%x\r\n", count, slave2_data_addr);
    return slave2_data_addr;
}

int spi_master_init(void)
{
    fpioa_set_function(SPI_MASTER_CLK_PIN, FUNC_SPI0_SCLK);
    fpioa_set_function(SPI_MASTER_MOSI_PIN, FUNC_SPI0_D0);
    
    fpioa_set_function(SPI_MASTER_MCU1_CS_PIN, FUNC_GPIOHS0 + SPI_MASTER_MCU1_CS_IO);
    fpioa_set_function(SPI_MASTER_MCU1_RDY_PIN, FUNC_GPIOHS0 + SPI_MASTER_MCU1_RDY_IO);
    fpioa_set_function(SPI_MASTER_MCU1_INT_PIN, FUNC_GPIOHS0 + SPI_MASTER_MCU1_INT_IO);
    
    fpioa_set_function(SPI_MASTER_MCU2_CS_PIN, FUNC_GPIOHS0 + SPI_MASTER_MCU2_CS_IO);
    fpioa_set_function(SPI_MASTER_MCU2_RDY_PIN, FUNC_GPIOHS0 + SPI_MASTER_MCU2_RDY_IO);
    fpioa_set_function(SPI_MASTER_MCU2_INT_PIN, FUNC_GPIOHS0 + SPI_MASTER_MCU2_INT_IO);

    spi_init(SPI_DEVICE_0, SPI_WORK_MODE_0, SPI_FF_STANDARD, 8, 0);
    spi_set_clk_rate(0, 10000000);
    
    return 0;
}



static void spi_slave_mcu1_ready_irq(void)
{
    if (spi_master_send_mcu1_cmd_flag)
        spi_slave_mcu1_ready = 1;
    else
    {
        mcu1_bf_data_ready_flag = 1;
        //mcu1_bf_ready_irq_count++;
        //bf_data_ready_mcu_mask |= MCU1;
        //clint_ipi_send(0);
    }
        
    //printf("mcu1 data ready irq\r\n");
}

static void spi_slave_mcu2_ready_irq(void)
{
    if (spi_master_send_mcu2_cmd_flag)
        spi_slave_mcu2_ready = 1;
    else
    {
        mcu2_bf_data_ready_flag = 1;
        //mcu2_bf_ready_irq_count++;
        //bf_data_ready_mcu_mask |= MCU2;
        //clint_ipi_send(0);
    }
        
    //printf("mcu2 data ready irq\r\n");
}


static int spi_receive_data(uint8_t slave_index, uint8_t *data, uint32_t len)
{
    fpioa_set_function(SPI_MASTER_MISO_PIN, FUNC_SPI0_D1);
    if (MCU1 == slave_index)
    {
        SPI_MASTER_MCU1_CS_LOW();
        SPI_MASTER_MCU1_INT_LOW();
    }
    else if (MCU2 == slave_index)
    {
        SPI_MASTER_MCU2_CS_LOW();
        SPI_MASTER_MCU2_INT_LOW();
    }    
    
    spi_receive_data_standard(0, 0,  NULL, 0, (uint8_t *)data, len);
    if (MCU1 == slave_index)
    {
        SPI_MASTER_MCU1_CS_HIGH();
        SPI_MASTER_MCU1_INT_HIGH();
    }
    else if (MCU2 == slave_index)
    {
        SPI_MASTER_MCU2_CS_HIGH();
        SPI_MASTER_MCU2_INT_HIGH();
    }
    
    fpioa_set_function(SPI_MASTER_MOSI_PIN, FUNC_SPI0_D0);
    return 0;
}

static int spi_send_data(uint8_t slave_index, uint8_t *data, uint32_t len)
{
    if (MCU1 == slave_index)
    {
        SPI_MASTER_MCU1_CS_LOW();
        SPI_MASTER_MCU1_INT_LOW();
    }
    else if (MCU2 == slave_index)
    {
        SPI_MASTER_MCU2_CS_LOW();
        SPI_MASTER_MCU2_INT_LOW();
    }

    spi_send_data_standard(0, 0, NULL, 0, (const uint8_t *)data, len);

    if (MCU1 == slave_index)
    {
        SPI_MASTER_MCU1_CS_HIGH();
        SPI_MASTER_MCU1_INT_HIGH();
    }
    else if (MCU2 == slave_index)
    {
        SPI_MASTER_MCU2_CS_HIGH();
        SPI_MASTER_MCU2_INT_HIGH();
    }

    return 0;
}

static int spi_master_send_cmd(uint8_t slave_index, spi_slave_command_t *cmd)
{
    uint8_t data[8];

    for (uint32_t i = 0; i < WAIT_TIMEOUT; i++)
    {
        if (MCU1 == slave_index && gpiohs_get_pin(SPI_MASTER_MCU1_RDY_IO) == 1)
            break;
        if (MCU2 == slave_index && gpiohs_get_pin(SPI_MASTER_MCU2_RDY_IO) == 1)
            break;
    }
    if (MCU1 == slave_index && gpiohs_get_pin(SPI_MASTER_MCU1_RDY_IO) == 0)
        return -1;
    if (MCU2 == slave_index && gpiohs_get_pin(SPI_MASTER_MCU2_RDY_IO) == 0)
        return -1;
    

    data[0] = cmd->cmd;
    data[1] = cmd->addr;
    data[2] = cmd->addr >> 8;
    data[3] = cmd->addr >> 16;
    data[4] = cmd->addr >> 24;
    data[5] = cmd->len;
    data[6] = cmd->len >> 8;
    data[7] = 0;
    for (uint32_t i = 0; i < 7; i++)
        data[7] += data[i];
    if (MCU1 == slave_index)
        spi_slave_mcu1_ready = 0;
    else if (MCU2 == slave_index)
        spi_slave_mcu2_ready = 0;
    
    if (MCU1 == slave_index)
        spi_master_send_mcu1_cmd_flag = 1;
    else if (MCU2 == slave_index)
        spi_master_send_mcu2_cmd_flag = 1;
        
    spi_send_data(slave_index, data, 8);
    
    for (uint32_t i = 0; i < WAIT_TIMEOUT; i++)
    {
        if (MCU1 == slave_index && spi_slave_mcu1_ready != 0)
            break;
        if (MCU2 == slave_index && spi_slave_mcu2_ready != 0)
            break;
    }
    if (MCU1 == slave_index)
        spi_master_send_mcu1_cmd_flag = 0;
    else if (MCU2 == slave_index)
        spi_master_send_mcu2_cmd_flag = 0;
    
    if (MCU1 == slave_index && spi_slave_mcu1_ready)
        return 0;
    else if (MCU2 == slave_index && spi_slave_mcu2_ready)
        return 0;
    else
        return -2;
}

int spi_master_transfer(uint8_t slave_index, uint8_t *data, uint32_t addr, uint32_t len, uint8_t mode)
{
    spi_slave_command_t cmd;

    if (mode <= READ_DATA_BYTE)
    {
        if (len > 8)
            len = 8;
        cmd.len = len;
    }
    else if (mode <= READ_DATA_BLOCK)
    {
        if (len > 0x100000)
            len = 0x100000;
        //addr &= 0xFFFFFFF0;
        cmd.len = len >> 4;
    }
    else
        return -1;
    cmd.cmd = mode;
    cmd.addr = addr;
    if (spi_master_send_cmd(slave_index, &cmd) != 0)
        return -2;
    if (mode & 0x01)
        spi_receive_data(slave_index, data, len);
    else
        spi_send_data(slave_index, data, len);
    return 0;
}
