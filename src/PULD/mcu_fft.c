#include "mcu_common_define.h" 
#if !FACTORY_RESET 
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "dmac.h"
#include "fft.h"

#include "sysctl.h"
#include "fft_soft.h"
#include "apu_init.h"

#define FFT_N               APU_DIR_CHANNEL_SIZE
#define FFT_FORWARD_SHIFT   0x0U
#define FFT_BACKWARD_SHIFT  0x1ffU



typedef enum _complex_mode
{
	FFT_HARD        = 0,
	FFT_SOFT        = 1,
	FFT_COMPLEX_MAX,
} complex_mode_t;


int16_t real[FFT_N];
int16_t imag[FFT_N];
float hard_power[FFT_N];
float soft_power[FFT_N];
float hard_angle[FFT_N];
float soft_angle[FFT_N];
uint64_t fft_out_data[FFT_N / 2];
uint64_t buffer_input[FFT_N];
uint64_t buffer_output[FFT_N];
uint64_t cycle[FFT_COMPLEX_MAX][FFT_DIR_MAX];

uint16_t get_bit1_num(uint32_t data)
{
	uint16_t num;
	for (num = 0; data; num++)
		data &= data - 1;
	return num;
}

int calculate_fft(int16_t *p_value_in, int16_t * p_fft_power_hard, int16_t * p_fft_power_soft)
{

    int32_t i;
    //float tempf1[3];
    fft_data_t *output_data;
    fft_data_t *input_data;
    uint16_t bit1_num = get_bit1_num(FFT_FORWARD_SHIFT);
    complex_hard_t data_hard[FFT_N] = {0};
    complex data_soft[FFT_N] = {0};

    int8_t FFT_SCALE = 12; // sample value allows FFT on 40kHz tone from EM282-T emitter to run without overflow
    for (i = 0; i < FFT_N; i++)
    {
        if (Enable_I2S_Hard_FFT || Enable_VOC_FFT)
        {
            data_hard[i].real = (int16_t)(p_value_in[i]) >> FFT_SCALE;
            data_hard[i].imag = (int16_t)0 >> FFT_SCALE;
        }
        if (Enable_I2S_Soft_FFT)
        {
            data_soft[i].real = (int16_t)(p_value_in[i]);
            data_soft[i].imag = (int16_t)0;
        }
    }

    if (Enable_I2S_Hard_FFT || Enable_VOC_FFT)
    {
        for (int i = 0; i < FFT_N / 2; ++i)
        {
            input_data = (fft_data_t *)&buffer_input[i];
            input_data->R1 = data_hard[2 * i].real;
            input_data->I1 = data_hard[2 * i].imag;
            input_data->R2 = data_hard[2 * i + 1].real;
            input_data->I2 = data_hard[2 * i + 1].imag;
        }
        // http://kendryte-docs.s3-website.cn-north-1.amazonaws.com.cn/docs/freertos_programming_guide/en/FFT.html#overview
        // https://en.wikipedia.org/wiki/Cooley%E2%80%93Tukey_FFT_algorithm#The_radix-2_DIT_case
        // https://link.springer.com/article/10.1007/s11128-020-02776-5
        fft_complex_uint16_dma(DMAC_CHANNEL0, DMAC_CHANNEL1, FFT_FORWARD_SHIFT, FFT_DIR_FORWARD, buffer_input, FFT_N, buffer_output);
    }

    if (Enable_I2S_Soft_FFT)
    {
        fft_soft(data_soft, FFT_N);
    }

    if (Enable_I2S_Hard_FFT || Enable_VOC_FFT)
    {
        for (i = 0; i < FFT_N / 2; i++)
        {
            output_data = (fft_data_t *)&buffer_output[i];
            data_hard[2 * i].imag = (output_data->I1);
            data_hard[2 * i].real = (output_data->R1);
            data_hard[2 * i + 1].imag = (output_data->I2);
            data_hard[2 * i + 1].real = (output_data->R2);
        }
    }

    //	for (i = 0; i < FFT_N; i++)
    //	{
    //  		hard_power[i] = (float)sqrt(data_hard[i].real * data_hard[i].real + data_hard[i].imag * data_hard[i].imag) * 2 * (1 << FFT_SCALE );
    //  		soft_power[i] = (float)sqrt(data_soft[i].real * data_soft[i].real + data_soft[i].imag * data_soft[i].imag) * 2;
    //	}
    //
    //	*p_fft_power_hard = (float)(hard_power[0] / 2 / FFT_N * (1 << bit1_num));
    //  *p_fft_power_soft = (float)(soft_power[0] / 2 / FFT_N * (1 << bit1_num));
    //
    //	for (i = 1; i < FFT_N / 2; i++)
    //	{
    //  		p_fft_power_hard[i] = (float)(hard_power[i] / FFT_N * (1 << bit1_num));
    //  		p_fft_power_soft[i] = (float)(soft_power[i] / FFT_N * (1 << bit1_num));
    //	}

    if (Enable_I2S_Hard_FFT || Enable_VOC_FFT)
    {
        for (i = 0; i < FFT_N; i++)
        {
            hard_power[i] = (float)sqrt(data_hard[i].real * data_hard[i].real + data_hard[i].imag * data_hard[i].imag) * 1 * (1 << FFT_SCALE);
        }

        *p_fft_power_hard = (float)(hard_power[0] / 2 / FFT_N * (1 << bit1_num));

        for (i = 1; i < FFT_N / 2; i++)
        {
            p_fft_power_hard[i] = (float)(hard_power[i] / FFT_N * (1 << bit1_num));
        }
    }

    if (Enable_I2S_Soft_FFT)
    {
        for (i = 0; i < FFT_N; i++)
        {
            soft_power[i] = (float)sqrt(data_soft[i].real * data_soft[i].real + data_soft[i].imag * data_soft[i].imag) * 1;
        }

        *p_fft_power_soft = (float)(soft_power[0] / 2 / FFT_N * (1 << bit1_num));

        for (i = 1; i < FFT_N / 2; i++)
        {
            p_fft_power_soft[i] = (float)(soft_power[i] / FFT_N * (1 << bit1_num));
        }
    }
    return 0;
}



#endif