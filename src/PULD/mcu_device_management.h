#ifndef __mcu_DEVICE_MANAGEMENT_H__
#define __mcu_DEVICE_MANAGEMENT_H__

#define SYSTEM_CLOCK_FREQ_INIT_MHZ        (  800) // in MHz
#define SYSTEM_CLOCK_FREQ_OFFSET_MHZ      (   20) // SCFO = Separation between core clock frequencies for MCU's 0(-SCFO), 1(0) & 2(+SCFO) in MHz
                                                  // Actual == 780MHz,  806MHz, 819MHz
#define SYSTEM_CLOCK_FREQ_MIN_MHZ            (40) // min freq... read char on UART0 interface to trigger restart (95mA @ 20MHz, 34mA @ 30MHz, 36mA @ 40MHz) 1500/36 = 41hrs standby +15mA if periph clocks ON 
#define SYSTEM_CLOCK_FREQ_MAX_MHZ          (1200) // highest stable frequency

void mcu_device_bsp_init(void);

void mcu_devices_core0_init(void);
void mcu_devices_core1_init(void);
void mcu_devices_core0_loop(void);
void mcu_devices_core1_loop(void);
#endif