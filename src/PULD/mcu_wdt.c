#include <stdio.h>
#include "mcu_wdt.h"
#include "gpio.h"
#include "mcu_common_define.h"

#define WDT_TIME_OUT_MS      3000
#define WDT_RESET_PERIOD_US  1000000


typedef struct _mcu_wdt_context_ {
    wdt_device_number_t device_number;
    wdt_callback callback;
    uint8_t timeout;
} _mcu_wdt_context_s;

static _mcu_wdt_context_s s_context[WDT_DEVICE_MAX] = {

    {
        .device_number = WDT_DEVICE_0,
        .timeout = 0,
        .callback = NULL,
    },
    {
        .device_number = WDT_DEVICE_1,
        .timeout = 0,
        .callback = NULL,
    }

};

void mcu_wdt_init(wdt_device_number_t device_num, wdt_callback callback) {
    s_context[device_num].device_number = device_num;
    if (callback != NULL)
    {
        s_context[device_num].callback = callback;
    }
    
    uint32_t wdt_start_time = wdt_init(device_num, WDT_TIME_OUT_MS, s_context[device_num].callback, (void*)&(s_context[device_num].device_number));
    wdt_response_mode(device_num, WDT_CR_RMOD_RESET);
    mcu_stdio_printf("WDT%d start time is %d ms\n\r", device_num, wdt_start_time);
    mcu_stdio_printf("wdt%d is off\n\r", device_num);  
}


void mcu_wdt_loop(wdt_device_number_t device_num) 
{
    static uint64_t t_us_before[WDT_DEVICE_MAX] = { 0, 0 };
    static uint64_t t_us_cur[WDT_DEVICE_MAX] = { 0, 0 };
    t_us_cur[device_num] = sysctl_get_time_us();
    uint64_t t_us_count = t_us_cur[device_num] - t_us_before[device_num];
    if(t_us_count > WDT_RESET_PERIOD_US) {
        s_context[device_num].timeout++;
        t_us_before[device_num] = sysctl_get_time_us();
        // printf("timeout:%d\n\r", s_content[device_num].timeout);
        wdt_feed(device_num);
    }
}

void mcu_wdt_enable(wdt_device_number_t id, uint8_t enable_flag)
{
    if (enable_flag)
    {
        wdt_start(id);
        //mcu_stdio_printf("wdt%d is on\n\r", id);  
    }
    else
    {
        wdt_stop(id);
        mcu_wdt_init(id, NULL);
        //mcu_stdio_printf("wdt%d is off\n\r", id);
    }
    
}


    