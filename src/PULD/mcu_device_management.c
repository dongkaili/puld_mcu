#include <stdio.h>
#include "bsp.h"
#include "sysctl.h"
#include "mcu_hw_module.h"
#include "mcu_common_define.h"
#include "mcu_comm_protocol.h"
#include "mcu_device_management.h"
#include "mcu_board_module.h"
#include "uart.h"
#include "gpio.h"
#include "fpioa.h"
#include "mcu_pins.h"
#include "mjson.h"
#include "mcu_data_handler.h"
#include "apu_init.h"
#include "math.h"
#include "mcu_mic_array.h"
#include "mcu_ipc_sync.h"
#include "mcu_ota.h"
#include "mcu_commands_handler.h"
#include "mcu_common_define.h"
#include "mcu_ota_handler.h"
#include "mcu_common.h"
#include "mcu_json_commands.h"

bool Enable_Stdio = true; // SEH: This may not be needed now with STDIO remap to JTAG port. Seems OK to enable by default for direct kflash installs.
uint32_t System_Clock_Freq_Target; 

#define QUEUETEST 0
#if QUEUETEST
_mcu_util_queue_s t_data_queue_set;
typedef struct {
    uint8_t *p_data;
    size_t size;
} test_data_s;

static void test_free_callback(void *elem)
{
    uint8_t* p = (uint8_t *)elem;
    printf("test_free_callback free address: 0x%08lx  ]\n\r", (uint64_t)p);
	free(p);
}
#endif

#define GET_MODULE_AND_INIT_BLOCK(module_id, device_id, init_parameter)\
    const _mcu_hw_module_t *module = NULL;\
    mcu_hw_get_module(#module_id, &module);\
    _mcu_hw_module_methods_t *method = (_mcu_hw_module_methods_t *)(((_mcu_##module_id##_module_t *)module)->common.methods);\
    method->open(module, #device_id, (_mcu_hw_device_t **)& device_id##_device);\
    device_id##_device->init(init_parameter);\


#define GET_MODULE_INSTANCE_AND_INIT_BLOCK(module_id, device_type, device_num, init_parameter)\
    char device_id_str[10]; \
    sprintf((char *)device_id_str, "%s%d", #device_type, device_num); \
    const _mcu_hw_module_t *module = NULL;\
    mcu_hw_get_module(#module_id, &module);\
    _mcu_hw_module_methods_t *method = (_mcu_hw_module_methods_t *)(((_mcu_##module_id##_module_t *)module)->common.methods);\
    method->open(module, device_id_str, (_mcu_hw_device_t **)& device_type##_device[device_num]); \
    device_type##_device[device_num]->init(device_num, init_parameter); \
    /*printf("device_id_str = %s:%s\n\r", device_id_str, device_type##_device[device_num]->common.id);\*/
    
#define PLL2_OUTPUT_FREQ 45158400UL
_mcu_util_queue_s pointers_queue_set;

static _mcu_uart_device_t *uart_device[UART_DEVICE_MAX];
#if !FACTORY_RESET
static _mcu_mic_device_t *mic_device;
static _mcu_fpga_device_t *fpga_device;
#endif
static _mcu_flash_device_t *flash_device;
static _mcu_wdt_device_t *wdt_device[WDT_DEVICE_MAX];
static _mcu_stdio_device_t *stdio_device;
uint32_t System_Clock_Freq_Actual;    
bool Restart_MCU0 = false; // flag set by RFRF command when MCU==3 (multicast) which allows return to main loop to process mssgs received from slaves before restarting system

extern bool block_LPM;  // flag that is cleared after min delay (5 sec) after to allow LPM command through. Needed to handle spurious LPM commands when app restarts
#define BLOCK_LPM_DELAY 5
static uint64_t t_block_LPM_us_start = 0;


void mcu_device_bsp_init(void) {

    // Set MCU ID input pin functions
    fpioa_set_function(PIN_MCU_ID0, FUNC_GPIO0);
    fpioa_set_function(PIN_MCU_ID1, FUNC_GPIO1);
    gpio_init();
   
    // Set MCU ID input pins
    gpio_set_drive_mode(GPIO_0_MCU_ID0, GPIO_DM_INPUT);
    gpio_set_drive_mode(GPIO_1_MCU_ID1, GPIO_DM_INPUT);

    // read pin status for MCU ID
    uint8_t id_0 = gpio_get_pin(GPIO_0_MCU_ID0);
    uint8_t id_1 = gpio_get_pin(GPIO_1_MCU_ID1);

    mcu_id = (id_1 << 1) + id_0;
#ifdef UART_OFFSET
    mcu_uart_dev = (mcu_id + UART_OFFSET) % 3;
#else
    mcu_uart_dev = mcu_id;
#endif  
    
    // Set MCU core clock frequencies with offsets
    System_Clock_Freq_Target = (SYSTEM_CLOCK_FREQ_INIT_MHZ + (SYSTEM_CLOCK_FREQ_OFFSET_MHZ * (int8_t)(mcu_id - 1))) * 1e6;
    System_Clock_Freq_Actual = sysctl_pll_set_freq(SYSCTL_PLL0, System_Clock_Freq_Target);
    sysctl_pll_set_freq(SYSCTL_PLL1, 400000000UL);
    dmac_init();
    plic_init();
    sysctl_enable_irq();

    mcu_hw_regist(mcu_get_board_module());
}

#if defined USE_QUEUE_BUFFER
static void uart_rx_callback(uint8_t index, const uint8_t *buf, int32_t len) {
    mcu_comm_parse_data(index, buf, len);
}
#else
static void uart_rx_callback(uint8_t index, const uint8_t *buf, int32_t len, int32_t shift_len) 
{

    if(shift_len > 0) 
    {
        const uint8_t * left_buf = buf + len - shift_len - RING_BUFFER_LEN;
        mcu_comm_parse_data(index, buf, len - shift_len);
        mcu_comm_parse_data(index, left_buf, shift_len);
    } 
    else 
    {
        mcu_comm_parse_data(index, buf, len);
    }

}
#endif

static void device_uart_init(uart_device_number_t uart_dev_number ) 
{
    init_parser(uart_dev_number, mcu_comm_event_handler);   
    mcu_init_commands_handler();
#ifdef DEBUG_ENABLE
    json_enable_debug(3, stdout);
#endif    
	mcu_init_ota_handler((write_t)(flash_device->write), (read_t)(flash_device->read), (reset_t)(flash_device->reset));
    GET_MODULE_INSTANCE_AND_INIT_BLOCK(board, uart, uart_dev_number, uart_rx_callback)
}


volatile uint8_t flag_sending = 0;
void send_string(uart_device_number_t index, char* str, _data_header_s head_data) 
{
    if(flag_sending == 1)
        return;
    flag_sending = 1;
    uint64_t header_size = sizeof(_data_header_s) / sizeof(uint8_t);
    uint64_t size = header_size + strlen(str)*sizeof(uint8_t);
    // printf("sizeof(_data_header_s):%ld\n\r", sizeof(_data_header_s));
    uint8_t* pdata = malloc(size);
    // printf("pdata address: 0x%08lx\n\r", (uint64_t)pdata);
    memset(pdata, 0, size);

    head_data.length = strlen(str);
    head_data.checksum = checksum((uint8_t *)str, head_data.length);

    memcpy(pdata, &head_data, header_size);
    memcpy(pdata + header_size, str, strlen(str));
    // printf("head_data.pdata:%s\n\r", head_data->pdata);

    //LOG("send data str:%s\n\r", str);
#if FACTORY_RESET
        usleep(1000);
#endif    
    int r = 0;
    r = uart_device[index]->send(index, pdata, size);
    if(r < 0) {
        //LOG("data, try send again\n\r");
        // asm volatile("nop");
        r = uart_device[index]->send(index, pdata, size);
        //LOG("send r:%d\n\r", r);
    }
    free(pdata);
    flag_sending = 0;
}


volatile uint8_t flag_raw_sending = 0;
void send_raw_bytes(uart_device_number_t index, uint8_t* bytes, uint32_t byte_size, _data_header_s head_data) 
{
    if(flag_raw_sending == 1)
        return;
    flag_raw_sending = 1;
    uint64_t header_size = sizeof(_data_header_s) / sizeof(uint8_t);
    uint64_t size = header_size + byte_size;
    // printf("sizeof(_data_header_s):%ld\n\r", sizeof(_data_header_s));
    uint8_t* pdata = malloc(size * sizeof(uint8_t));
    // printf("pdata address: 0x%08lx\n\r", (uint64_t)pdata);
    memset(pdata, 0, size);

    head_data.length = byte_size;
    head_data.checksum = checksum(bytes, head_data.length);

    memcpy(pdata, (void*)&head_data, header_size);
    memcpy(pdata + header_size, (void*)bytes, byte_size);
    // printf("head_data.pdata:%s\n\r", head_data->pdata);

    //LOG("send data str:%s\n\r", str);
    int r = 0;
    r = uart_device[index]->send(index, pdata, size);
    if(r < 0) {
        //LOG("data, try send again\n\r");
        // asm volatile("nop");
        r = uart_device[index]->send(index, pdata, size);
        //LOG("send r:%d\n\r", r);
    }
    free(pdata);
    flag_raw_sending = 0;
}

static void send_ota_event(uint8_t type, void * data) {
    _event_ota_s e_data;
    e_data.event_type = type;
    e_data.data = data;
    mcu_ota_event_handler(&e_data);
}

void data_handle_loop(uint8_t index) {
    commands_receiver();
    // corelock_lock(&_init_lock); // SEH: not needed? TBD confirm
    _mcu_util_queue_s* pdata_set = mcu_data_handle_get_data_set(index);
    while(0 == mcu_util_queue_is_empty(pdata_set)) {
        mcu_comm_data_s tdata;
        mcu_util_queue_deque(pdata_set, &tdata);
        switch(tdata.format) {
            case COMM_MESSAGE_FORMAT_TYPE_H_RAW: {
                if(COMM_MESSAGE_FORMAT_TYPE_L_OTA_BIN == tdata.msg_type) {
                    //Enable_Stdio = false;
                    send_ota_event(OTA_EVENT_FRAME_PAYLOAD, (void *)&tdata);
                }
                else if ( (COMM_MESSAGE_FORMAT_TYPE_L_I2S == tdata.msg_type) || 
                          (COMM_MESSAGE_FORMAT_TYPE_L_I2S_FFT == tdata.msg_type) ||
                          (COMM_MESSAGE_FORMAT_TYPE_L_I2S_RAW_FFT == tdata.msg_type) || 
                          (COMM_MESSAGE_FORMAT_TYPE_L_DIR == tdata.msg_type) ||
                          (COMM_MESSAGE_FORMAT_TYPE_L_VOC == tdata.msg_type) ||
                          (COMM_MESSAGE_FORMAT_TYPE_L_VOC_FFT == tdata.msg_type))
                {
                    uint8_t * data = malloc((tdata.size+12) * sizeof(uint8_t));
                    //printf("tdata.size is %d\r\n", tdata.size);
                    memset((void*)data, 0, tdata.size+12);
                    memcpy((void *)data, tdata.p_data, tdata.size);
                    if (index == mcu_uart_dev) {
                        command_invoker(data);
                    }
                    else if ((mcu_id == MCU0) && (index != mcu_id))
                    {
                        _data_header_s head_data = {
		                    .head = 0x11,
		                    .version = 0x02,
		                    .type = tdata.msg_type,
	                    };
	                    head_data.type = head_data.type << 8;
                        send_raw_bytes(UART_DEVICE_1, (uint8_t *)data, tdata.size, head_data);
                        usleep(10000);
                        //printf("MCU0 forward I2S Raw from %d to host\r\n", index);
                    }  
                    free(data);
                }
                
                break;
            }
            case COMM_MESSAGE_FORMAT_TYPE_H_JSON: {
                //Enable_Stdio = false;
                mcu_json_command_proc(index, &tdata);
                break;
            }
            case COMM_MESSAGE_FORMAT_TYPE_H_STRING: {
                if(COMM_MESSAGE_FORMAT_TYPE_L_COMMAND == tdata.msg_type
                    && tdata.size <= MAX_COMMAND_STRING_LEN) {
                    //Enable_Stdio = false;
                    uint8_t command[MAX_COMMAND_STRING_LEN] = {0};
                    memcpy((void *)command, tdata.p_data, tdata.size);
                    if (index == mcu_uart_dev) {
                        command_invoker(command);
                    }
                    else if ((mcu_id == MCU0) && (index != mcu_id))
                    {
                        _data_header_s head_data = {
		                    .head = 0x11,
		                    .version = 0x02,
		                    .type = 0x0502,
	                    };
                        send_string(UART_DEVICE_1, (char *)command, head_data);
                        usleep(10000);
                        //printf("cmd: %s, idx: %d\r\n", command, index);
                        
                        //mcu0_handle_ota_response_from_slave((char*)command, index);
                        
                        
                    }
                    
                        
                }
                break;
            }
            default:
                break;
        }
        // printf("format:%d, type: %d, tdata.size:%d\n\r", tdata.format, tdata.msg_type, tdata.size);
        // size_t heap_size = get_free_heap_size();
        // printf("heap_size:%ld\n\r", heap_size);
        mcu_util_queue_enque(&pointers_queue_set, &tdata.p_data);
    }
    // corelock_unlock(&_init_lock); // SEH: not needed? TBD confirm
}

#if !FACTORY_RESET
static void mic_array_callback(void *parameter) {
    //bool mcu_1_report = true;
    /*
	bool mcu_1_report = false;
    static uint64_t mic_callback_time = 0;
    if (sysctl_get_time_us() > (mic_callback_time + 50000))
    {
        //mcu_1_report = true;
        mic_callback_time = sysctl_get_time_us();
    }
    */
	
    // sleep(5);
    // uint16_t dir_max_strength =  *((uint16_t*)parameter);
    //_all_mcu_bf_data_s mic_data =  *((_all_mcu_bf_data_s*)parameter);
    _all_bf_data_s bf_data = *((_all_bf_data_s *)parameter);
        
        
    char str[1024] = "";

//  #if !defined(DEBUG_APU) && !defined(DEBUG_APU_BARCHART) && !defined(DEBUG_APU_LINKED_LIST) && !defined(PAUSE_APU_CHART)
    if((Enable_Host_Reporting) && ((Host_Update_Counter == Host_Update_Interval) || BF_FSM_STATE_CHANGE))
    {
        char AZM_INC_angle_str[64] = "";
        if ((Enable_AZM_INC_Data || Enable_All_ANGLE) && mcu_id != MCU0)
        {
            sprintf(AZM_INC_angle_str, "\"ANGLE_TMP_AZM\":%.1f, \"ANGLE_TMP_INC\":%.1f, ", 
                (float)bf_data.azm.angle[mcu_id], (float)bf_data.inc.angle[mcu_id]);
        }
        
        char AZM_INC_dbfs_str[64] = "";
        if ((Enable_AZM_INC_Data || Enable_All_DBFS) && mcu_id != MCU0)
        {
            sprintf(AZM_INC_dbfs_str, "\"DBFS_TMP_AZM\":%.1f, \"DBFS_TMP_INC\":%.1f, ", 
                (float)bf_data.azm.dbfs[mcu_id], (float)bf_data.inc.dbfs[mcu_id]);
        }
        
        char DUT_str[128] = ""; 
        if (Enable_DUT_Test) // Optional A/B Test signals
            sprintf(DUT_str, "\"ANGLE_DUT\":%.1f, \"DBFS_DUT\":%.1f, ", (float)bf_data.dut.angle[mcu_id], (float)bf_data.dut.dbfs[mcu_id]);
        
        char all_mcu_angle_str[256] = ""; 
        if (mcu_id == MCU0 && Enable_All_ANGLE) 
        {
            char all_angle_azm_str[60] = "";
            sprintf(all_angle_azm_str, "\"ANGLE_AZM\":[%.1f, %.1f, %.1f]", bf_data.azm.angle[0], bf_data.azm.angle[1], bf_data.azm.angle[2]);
            
            char all_angle_inc_str[60] = "";
            sprintf(all_angle_inc_str, ",\"ANGLE_INC\":[%.1f, %.1f, %.1f]", bf_data.inc.angle[0], bf_data.inc.angle[1], bf_data.inc.angle[2]);

            sprintf(all_mcu_angle_str, "%s%s, ", all_angle_azm_str, all_angle_inc_str);
        }

        char all_mcu_dbfs_str[256] = ""; 
        if (mcu_id == MCU0 && Enable_All_DBFS) 
        {
            char all_dbfs_azm_str[60] = "";
            sprintf(all_dbfs_azm_str, ", \"DBFS_AZM\":[%.1f, %.1f, %.1f]", bf_data.azm.dbfs[0], bf_data.azm.dbfs[1], bf_data.azm.dbfs[2]);
            
            char all_dbfs_inc_str[60] = "";
            sprintf(all_dbfs_inc_str, ", \"DBFS_INC\":[%.1f, %.1f, %.1f]", bf_data.inc.dbfs[0], bf_data.inc.dbfs[1], bf_data.inc.dbfs[2]);

            sprintf(all_mcu_dbfs_str, "%s%s, ", all_dbfs_azm_str, all_dbfs_inc_str);
        }

        sprintf(str, "{\"MCU\":%d, \"ANGLE\":%.1f, \"DBFS\":%.1f, %s%s%s%s%s\"NMPA\":%u, \"STATE\":\"%s\"}", mcu_id, (float)bf_data.azm.angle[mcu_id], (float)bf_data.azm.dbfs[mcu_id], AZM_INC_angle_str, AZM_INC_dbfs_str, DUT_str, all_mcu_angle_str, all_mcu_dbfs_str, (uint8_t)MPA_N, bf_state_str_trim);
        _data_header_s head_data = {
            .head = 0x11,
            .version = 0x02,
            .type = 0x0101,
        };
        send_string(mcu_uart_dev, str, head_data); // SEH: Hardcoded as mcu_id
    }
    else if((Enable_Host_Reporting) && ((Host_Update_Counter != Host_Update_Interval) && BF_FSM_STATE_CHANGE)) // report separately and on every event if host is enabled
    {
        sprintf(str, "{\"STATE\":\"%s\"}", bf_state_str_trim);
        _data_header_s head_data = {
            .head = 0x11,
            .version = 0x02,
            .type = 0x0101,
        };
        send_string(mcu_uart_dev, str, head_data);
    }
  BF_FSM_STATE_CHANGE = false;  // must be unconditional in here so it still clears if host is disabled... must be done in here after used 

//#endif
}

static void device_mic_init(void) {
    GET_MODULE_AND_INIT_BLOCK(board, mic, mic_array_callback)
}
#endif

#if !FACTORY_RESET
static void device_fpga_init(void) {
    GET_MODULE_AND_INIT_BLOCK(board, fpga, NULL)
}

#endif

static void device_flash_init(void) {
    GET_MODULE_AND_INIT_BLOCK(board, flash, NULL)
}

static int wdt_irq(void *ctx) {
    wdt_device_number_t device_num = *(wdt_device_number_t*)ctx;
    static int s_wdt_irq_cnt[2] = { 0, 0 };
    LOG1("%s\n", __func__) ;
    s_wdt_irq_cnt[device_num]++;
    if (s_wdt_irq_cnt[device_num] < 3) {
        wdt_clear_interrupt(device_num);
        LOG1("irq_cnt:%d\n\r", s_wdt_irq_cnt[device_num]);
    }
    else
        while (1) ;
    return 0;
}


static void device_wdt_init(wdt_device_number_t wdt_dev_number) {
       GET_MODULE_INSTANCE_AND_INIT_BLOCK(board, wdt, wdt_dev_number, wdt_irq)
}

        
static void device_stdio_init(void) {
    GET_MODULE_AND_INIT_BLOCK(board, stdio, NULL)
}

void mcu_devices_core0_init(void) {
    device_flash_init();
#ifdef TEST_MULTI_UART
    for (uint8_t i = 0; i < N_MULTI_UART; i++)
        if ((mcu_id == 0) || (i == mcu_id))
            device_uart_init(i);
        else
            sysctl_clock_disable(SYSCTL_CLOCK_UART1 + i); // disable unused UART devices
#else
    for(uint8_t i = 0 ; i < N_MULTI_UART ; i++)
        if(i == mcu_id)
            device_uart_init(i);
        else
            sysctl_clock_disable(SYSCTL_CLOCK_UART1 + i);  // disable unused UART devices
#endif
    device_stdio_init();
	mcu_util_queue_init(&(pointers_queue_set), 10, sizeof(uint8_t*), NULL);
#if !FACTORY_RESET
    device_fpga_init();
#endif
#if INCLUDE_WDT
    device_wdt_init(WDT_DEVICE_0);
#endif
#if OTA_TEST
    fake_ota_command();
#endif
    
    t_block_LPM_us_start = sysctl_get_time_us(); // starts timer to determine when it's OK to enable LPM
}
    

void mcu_devices_core1_init(void) {
#if !FACTORY_RESET
    device_mic_init(); 
#endif
#if QUEUETEST
    mcu_util_queue_init(&(t_data_queue_set), 10, sizeof(test_data_s), test_free_callback);
#endif
#if INCLUDE_WDT
    device_wdt_init(WDT_DEVICE_1);
#endif
    report_firmware_version(mcu_id);
    report_mcu_id(mcu_id);
    //report_mcu_status(mcu_id); // will not work in this location... must be in main()
#if !FACTORY_RESET
    usleep(10000);   // needed, otherwise lost the next JSON response string
    if (mcu_id == MCU1 || mcu_id == MCU2)
    {
        slave_mcu_spi_ready_notification(mcu_id);
    }
#endif
}

static void release_queue_buffer() {
    //must run on same core with protocol parser
    corelock_lock(&_init_lock);
    while(0 < mcu_util_queue_get_size(&pointers_queue_set)) {
        uint8_t* address = NULL;
        mcu_util_queue_deque(&pointers_queue_set, &address);
        // printf("address : 0x%08lx\n\r", (uint64_t)address);
        free(address);
    }
    corelock_unlock(&_init_lock);
}




void mcu_devices_core0_loop(void) {
    
    if (Core_Loop_Time_Test && !Terminal_Mode)
    {
        core0_us_elapsed = (uint32_t)(sysctl_get_time_us() - core0_us_start);
        core0_us_start = sysctl_get_time_us();
        printf("Core0 Loop Time = %d\n\r", core0_us_elapsed);
    }
    
    mcu_ota_eval_fsm_state();
    if (mcu_ota_get_start_flag() == false)  
    {
        stdio_device->loop();
    }
#ifdef TEST_MULTI_UART
    // run uarts... 3 on master and just one on each slave
    for(uint8_t i = 0 ; i < N_MULTI_UART ; i++) {
        if((mcu_uart_dev == 0) || (i == mcu_uart_dev))
        {
            uart_device[i]->loop(i);
        }
    }
#else
    uart_device[mcu_id]->loop(mcu_id);
#endif
#ifdef TEST_MULTI_UART
    // run data handlers... 3 on master and just one on each slave
    for(uint8_t i = 0 ; i < N_MULTI_UART ; i++)
        if((mcu_uart_dev == 0) || (i == mcu_uart_dev))
    {
        data_handle_loop(i);
    }
#else
    data_handle_loop(mcu_id);
#endif
#if !FACTORY_RESET
    fpga_device->loop();
	data_link_loop();
#endif
    
#if OTA_TEST
    fake_ota_data();
#endif
    release_queue_buffer();
#if INCLUDE_WDT
    wdt_device[WDT_DEVICE_0]->loop(WDT_DEVICE_0);
    if (Force_WDT0_Trip)
    {
        printf("Restarting with WDT%d trip...\n\r", WDT_DEVICE_0);
        while (1) ;
    }
# endif
    mcu_uart_restore_baud_rate();

    // OK to enable LPM?
    uint64_t t_block_LPM_us_elapsed = sysctl_get_time_us() - t_block_LPM_us_start;
    //printf("t_block_LPM_elapsed (sec)  = %f.1\n", (float)t_block_LPM_us_elapsed/1000000);
    if (t_block_LPM_us_elapsed  > (BLOCK_LPM_DELAY * 1000000))
    {
        block_LPM = false;    
    }

}

#if QUEUETEST
static uint32_t testidx = 0;
static char testdata[1024] = "";
#endif

void mcu_devices_core1_loop(void) {
    if (Core_Loop_Time_Test && !Terminal_Mode)
    {
        core1_us_elapsed = (uint32_t)(sysctl_get_time_us() - core1_us_start);
        core1_us_start = sysctl_get_time_us();
        printf("Core1 Loop Time = %d\n\r", core1_us_elapsed);
    }

    if (mcu_ota_get_start_flag() == false) 
    {
#if !FACTORY_RESET
    	mic_device->loop();
#endif
    }

    
    if (Restart_MCU0 == true) // flag set by RFRF command... allows mssgs sent by slaves to be forwarded before system is restarted
        sysctl_reset(SYSCTL_RESET_SOC);
    
#if MASS_STRING_SEND_TEST
    send_test_string_from_local();
#endif
  
#if QUEUETEST
    testidx ++;
    memset(testdata, 0, 1024);
    sprintf(testdata, "%d", testidx);
    size_t len = strlen(testdata);
    //malloc buffer
    test_data_s tdata = {
        .size = len,
        .p_data = malloc(len*sizeof(char))
    };
    memcpy(tdata.p_data, testdata, len*sizeof(char));
    mcu_util_queue_enque(&(t_data_queue_set), &(tdata));


    if(mcu_util_queue_get_size(&(t_data_queue_set)) % 15 == 1) {
        while(1 != mcu_util_queue_is_empty(&(t_data_queue_set))) {
            test_data_s tdata;
            mcu_util_queue_deque(&(t_data_queue_set), &tdata);
            uint8_t str1[1024] = {0};
            strncpy(str1, tdata.p_data, tdata.size);
            LOG("==queue value: %s\n\r", str1);
            LOG("==[0]:%02x,len: %ld\n\r",tdata.p_data[0], tdata.size);
            free(tdata.p_data);
        }
    }
#endif
#if INCLUDE_WDT
    
    wdt_device[WDT_DEVICE_1]->loop(WDT_DEVICE_1);
    if (Force_WDT1_Trip)
    {
        printf("Restarting with WDT%d trip...\n\r", WDT_DEVICE_1);
        while (1);
    }
#endif
}

void mcu_reboot() { 
    mcu_stdio_printf("\n\rInitiating reboot...\n\r"); 
    usleep(50000); // need non-zero delay and works with 10ms but use 50ms to be safe
    sysctl_reset(SYSCTL_RESET_SOC);    
}
