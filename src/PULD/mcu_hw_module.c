#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "mcu_common_define.h"
#include "mcu_hw_module.h"

uint8_t module_count = 0;

_mcu_hw_module_t **module_table = NULL;

uint8_t mcu_id = 0xff;
uint8_t mcu_uart_dev = 0xff;

void free_module_set(void *elem)
{
	free(*(_mcu_hw_module_t **)elem);
}

int mcu_hw_get_module(const char *id, const _mcu_hw_module_t **module) {
    assert(module_table != NULL);
    // uint8_t count = sizeof(module_tag_table)/sizeof(uint32_t);
    // printf("%s,%s,%d\n\r", __FILE__,__FUNCTION__,__LINE__);
    for(int i = 0; i < module_count; i ++) {
        _mcu_hw_module_t *item = module_table[i];
        if(0 == strcmp(item->id, id)) {
            *module = item;
            break;
        }
    }
    return 0;
}

void mcu_hw_regist(_mcu_hw_module_t *module) {
    module_count ++;
    _mcu_hw_module_t ** p_table = (_mcu_hw_module_t **)realloc(module_table, module_count*sizeof(_mcu_hw_module_t*));
    module_table = p_table;
    module_table[module_count - 1] = module;
}
