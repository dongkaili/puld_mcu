#include "mcu_common_define.h" 
#if !FACTORY_RESET 
#include "apu_init.h"
#include <stddef.h>
#include <stdio.h>
#include <printf.h>
#include "apu.h"
#include "mcu_pins.h"
#include "mcu_mic_array.h"

uint64_t dir_logic_count;
uint64_t voc_logic_count;
uint32_t I2S_FS_actual;
// Bandpass Filter Settings... Currently set with selection of enumerated settings in apu_fir_coefficients_options[] array
// Currently only the LP value is used and that value must be compared against all options in apu_fir_coefficients_options[] array to select proper index to set i2s_fs_sel_current
// FUTURE: Actual vlues supplied by host (in kHz) for I2S_FS, LP & HP vars will be used to calculate coefficients for H/W FIR filter stages and for use with FFT results.
uint32_t BP_Low = BP_LOW_FREQ;
uint32_t BP_High = BP_HIGH_FREQ;
i2s_fs_options_t i2s_fs_sel_current = I2S_FS;
i2s_fs_options_t i2s_fs_sel_saved = I2S_FS;
float Angle_BF_AZM_static = FOV_ANGLE;
float Angle_BF_INC_static = AZM_ANGLE;
TEST_State_t TEST_State = TEST_DISABLE;

extern BF_enum active_BF; 

//#if APU_FFT_ENABLE
// uint32_t APU_DIR_FFT_BUFFER[APU_DIR_CHANNEL_MAX]
// 				[APU_DIR_CHANNEL_SIZE]
// 	__attribute__((aligned(128)));
uint32_t APU_VOC_FFT_BUFFER[APU_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));
//int16_t APU_VOC_FFT_BUFFER[APU_VOC_CHANNEL_SIZE]
//    __attribute__((aligned(128)));
//#endif
//#if(1)
int16_t APU_DIR_BUFFER[APU_DIR_CHANNEL_MAX][APU_DIR_CHANNEL_SIZE]
	__attribute__((aligned(128)));
int16_t APU_VOC_BUFFER[APU_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));
//#endif




int int_apu(void *ctx)
{
	apu_int_stat_t rdy_reg = apu->bf_int_stat_reg;

	if (rdy_reg.dir_search_data_rdy) {
		apu_dir_clear_int_state();

#if (0)//APU_FFT_ENABLE
		static int ch;

		ch = (ch + 1) % 16;
		for (uint32_t i = 0; i < 512; i++) { //
			uint32_t data = apu->sobuf_dma_rdata;

			APU_DIR_FFT_BUFFER[ch][i] = data;
		}
		if (ch == 0) { //
			dir_logic_count++;
		}
#else
		for (uint32_t ch = 0; ch < APU_DIR_CHANNEL_MAX; ch++) {
			for (uint32_t i = 0; i < APU_DIR_CHANNEL_SIZE/2; i++) { //
				uint32_t data = apu->sobuf_dma_rdata;
        
                    APU_DIR_BUFFER[ch][i * 2 + 0] =
                        data & 0xffff;
                    APU_DIR_BUFFER[ch][i * 2 + 1] =
                        (data >> 16) & 0xffff;
                }
            }
            dir_logic_count++;
#endif

	} else if (rdy_reg.voc_buf_data_rdy) {
		apu_voc_clear_int_state();

//#if APU_FFT_ENABLE
        if (Enable_VOC_FFT & 0x00)
        {
            // this code is used when the FFT in apu is enabled, and 
            // supposed to read frequency domain data of VOC
            for (uint32_t i = 0; i < APU_VOC_CHANNEL_SIZE; i++)
            { //
                uint32_t data = apu->vobuf_dma_rdata;
                APU_VOC_FFT_BUFFER[i] = data;
            }
        }
        else
        {
            // read time domain data of VOC
            for (uint32_t i = 0; i < APU_VOC_CHANNEL_SIZE / 2; i++)
            { //
                uint32_t data = apu->vobuf_dma_rdata;
                if (active_BF == INC)
                {
                    APU_VOC_BUFFER[i * 2 + 0] = data & 0xffff;
                    APU_VOC_BUFFER[i * 2 + 1] = (data >> 16) & 0xffff;
                }
                
            }
        }
//#else
		
//#endif

		voc_logic_count++;
	} else { //
		printk("[waring]: unknown %s interrupt cause.\n\r", __func__);
	}
	return 0;
}

#if APU_DMA_ENABLE
int int_apu_dir_dma(void *ctx)
{
	uint64_t chx_intstatus =
		dmac->channel[APU_DIR_DMA_CHANNEL].intstatus;
	if (chx_intstatus & 0x02) {
		dmac_chanel_interrupt_clear(APU_DIR_DMA_CHANNEL);

#if APU_FFT_ENABLE
		static int ch;

		ch = (ch + 1) % 16;
		dmac->channel[APU_DIR_DMA_CHANNEL].dar =
			(uint64_t)APU_DIR_FFT_BUFFER[ch];
#else
		dmac->channel[APU_DIR_DMA_CHANNEL].dar =
			(uint64_t)APU_DIR_BUFFER;
#endif

		dmac->chen = 0x0101 << APU_DIR_DMA_CHANNEL;

#if APU_FFT_ENABLE
		if (ch == 0) { //
			dir_logic_count++;
		}
#else
		dir_logic_count++;
#endif

	} else {
		printk("[warning] unknown dma interrupt. %lx %lx\n\r",
		       dmac->intstatus, dmac->com_intstatus);
		printk("dir intstatus: %lx\n\r", chx_intstatus);

		dmac_chanel_interrupt_clear(APU_DIR_DMA_CHANNEL);
	}
	return 0;
}


int int_apu_voc_dma(void *ctx)
{
	uint64_t chx_intstatus =
		dmac->channel[APU_VOC_DMA_CHANNEL].intstatus;

	if (chx_intstatus & 0x02) {
		dmac_chanel_interrupt_clear(APU_VOC_DMA_CHANNEL);

#if APU_FFT_ENABLE
		dmac->channel[APU_VOC_DMA_CHANNEL].dar =
			(uint64_t)APU_VOC_FFT_BUFFER;
#else
		dmac->channel[APU_VOC_DMA_CHANNEL].dar =
			(uint64_t)APU_VOC_BUFFER;
#endif

		dmac->chen = 0x0101 << APU_VOC_DMA_CHANNEL;


		voc_logic_count++;

	} else {
		printk("[warning] unknown dma interrupt. %lx %lx\n\r",
		       dmac->intstatus, dmac->com_intstatus);
		printk("voc intstatus: %lx\n\r", chx_intstatus);

		dmac_chanel_interrupt_clear(APU_VOC_DMA_CHANNEL);
	}
	return 0;
}
#endif

void init_fpioa(void)
{
	printk("init fpioa.\n\r");
	fpioa_init();
  
  // I2S Interface
  fpioa_set_function(PIN_MCU_I2S0_IN_D0, FUNC_I2S0_IN_D0);
  fpioa_set_function(PIN_MCU_I2S0_IN_D1, FUNC_I2S0_IN_D1);
  fpioa_set_function(PIN_MCU_I2S0_IN_D2, FUNC_I2S0_IN_D2);
  fpioa_set_function(PIN_MCU_I2S0_IN_D3, FUNC_I2S0_IN_D3);
  fpioa_set_function(PIN_MCU_I2S0_LRCLK, FUNC_I2S0_WS);
  fpioa_set_function(PIN_MCU_I2S0_SCLK , FUNC_I2S0_SCLK);
}

uint32_t set_I2S_FS(uint32_t I2S_FS_freq)
{
  sysctl_pll_set_freq(SYSCTL_PLL2, I2S_FS_freq * 32 * 64);       // 64x BCLK... easy number to divide down with clock circuits
	uint32_t freq = i2s_set_sample_rate(I2S_DEVICE_0, I2S_FS_freq) / 32;
return freq;
}  


////////////////////////////////////////////////////////////////////////////////////////////////
// APU FIR Filter Coefficient Definitions... Setup in 2D array with I2S_FS as index
// 1st few are presently unused
////////////////////////////////////////////////////////////////////////////////////////////////
uint16_t fir_BP18_42_256_t[] = {
	0xf3a1, 0x0c15, 0xfa8f, 0xe507, 0xd706, 0xe177, 
	0x06f8, 0x329e, 0x4602, 0x329e, 0x06f8, 0xe177, 
	0xd706, 0xe507, 0xfa8f, 0x0c15, 0xf3a1  
}; // Bandpass 18kHz-42kHz; Matlab: Fs=256kHz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple

    // Use this for production units and for testing when phone is not used... used when #ifndef EXPANDED_BP_RANGE
uint16_t fir_BP26_42_253906_t[] = {
	0x1d36, 0x246f, 0x290d, 0xbf83, 0xb2aa, 0xbffd,
	0xe8c5, 0x31c5, 0x54d1, 0x31c5, 0xe8c5, 0xbffd,
	0xb2aa, 0xbf83, 0x290d, 0x246f, 0x1d36
}; // Bandpass 26kHz-42kHz; Matlab: Fs=253906Hz, FS1=16, FP1=26,FP2=42,FS2=52; BP,equiripple

uint16_t fir_BP18_60_253906_t[] = {
	0x0545, 0xe8c2, 0xf534, 0xffe3, 0xeb82, 0xca62, 
	0xe300, 0x3a90, 0x6e0a, 0x3a90, 0xe300, 0xca62,
	0xeb82, 0xffe3, 0xf534, 0xe8c2, 0x0545
}; // Bandpass 18kHz-60kHz; Matlab: Fs=253906Hz, FS1=8, FP1=18,FP2=60,FS2=70; BP,equiripple

////////////////////////////////////////////////////////////////////////////////////////////////
// Enumerated I2S_FS FIR Filter options... 
////////////////////////////////////////////////////////////////////////////////////////////////
uint16_t fir_unity[] = { // no filtering
	0x8000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
};

uint16_t fir_BP18_42_253906[] = {
	0xf347, 0x0b9f, 0xfb55, 0xe5bf, 0xd6d0, 0xe075, 
	0x0623, 0x32ad, 0x4684, 0x32ad, 0x0623, 0xe075,
	0xd6d0, 0xe5bf, 0xfb55, 0x0b9f, 0xf347
}; // Bandpass 18kHz-42kHz; Matlab: Fs=253906Hz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple

uint16_t fir_BP18_42_285644[] = {
	0xfc1a, 0x06c7, 0xf70d, 0xeef2, 0xee34, 0xf726, 				
	0x07ea, 0x1880, 0x1f6c, 0x1880, 0x07ea, 0xf726, 				
	0xee34, 0xeef2, 0xf70d, 0x06c7, 0xfc1a				
}; // Bandpass 18kHz-42kHz; Matlab: Fs=285644Hz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple

/*******************/
/* I2S_FS = 317382 */
/*******************/
uint16_t fir_BP15_42_317382[] = { 0xfa1a, 0x04ce, 0xf2ad, 0xee3e, 0xf37a, 0xff02, 0x0d82, 0x19e3, 0x1ecd, 0x19e3, 0x0d82, 0xff02, 0xf37a, 0xee3e, 0xf2ad, 0x04ce, 0xfa1a };  // Bandpass 15kHz-42kHz; Matlab: Fs=317382Hz, FS1=5, FP1=15,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP16_42_317382[] = { 0xfb44, 0x049d, 0xf1fa, 0xee38, 0xf33b, 0xfe3a, 0x0c8e, 0x1908, 0x1e05, 0x1908, 0x0c8e, 0xfe3a, 0xf33b, 0xee38, 0xf1fa, 0x049d, 0xfb44 };  // Bandpass 16kHz-42kHz; Matlab: Fs=317382Hz, FS1=6, FP1=16,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP17_42_317382[] = { 0xfc6a, 0x045e, 0xf145, 0xee4c, 0xf2ef, 0xfd5f, 0x0b8e, 0x1821, 0x1d2a, 0x1821, 0x0b8e, 0xfd5f, 0xf2ef, 0xee4c, 0xf145, 0x045e, 0xfc6a };  // Bandpass 17kHz-42kHz; Matlab: Fs=317382Hz, FS1=7, FP1=17,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP18_42_317382[] = { 0xfd99, 0x0408, 0xf08b, 0xee80, 0xf28d, 0xfc72, 0x0a87, 0x172b, 0x1c41, 0x172b, 0x0a87, 0xfc72, 0xf28d, 0xee80, 0xf08b, 0x0408, 0xfd99 };  // Bandpass 18kHz-42kHz; Matlab: Fs=317382Hz, FS1=8, FP1=18,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP19_42_317382[] = { 0xfed5, 0x0396, 0xefd8, 0xeedf, 0xf207, 0xfb76, 0x097d, 0x1624, 0x1b3a, 0x1624, 0x097d, 0xfb76, 0xf207, 0xeedf, 0xefd8, 0x0396, 0xfed5 };  // Bandpass 19kHz-42kHz; Matlab: Fs=317382Hz, FS1=9, FP1=19,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP20_42_317382[] = { 0x0033, 0x02ef, 0xef30, 0xef6e, 0xf149, 0xfa78, 0x086f, 0x150b, 0x1a1d, 0x150b, 0x086f, 0xfa78, 0xf149, 0xef6e, 0xef30, 0x02ef, 0x0033 };  // Bandpass 20kHz-42kHz; Matlab: Fs=317382Hz, FS1=10, FP1=20,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP21_42_317382[] = { 0x0465, 0xfa55, 0xf8a8, 0xeae8, 0xef1c, 0xfc9b, 0x06b1, 0x12d5, 0x1a4d, 0x12d5, 0x06b1, 0xfc9b, 0xef1c, 0xeae8, 0xf8a8, 0xfa55, 0x0465 };  // Bandpass 21kHz-42kHz; Matlab: Fs=317382Hz, FS1=11, FP1=21,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP22_42_317382[] = { 0x0955, 0xf91d, 0xf392, 0xef3d, 0xf096, 0xf944, 0x06a7, 0x12d2, 0x17b9, 0x12d2, 0x06a7, 0xf944, 0xf096, 0xef3d, 0xf392, 0xf91d, 0x0955 };  // Bandpass 22kHz-42kHz; Matlab: Fs=317382Hz, FS1=12, FP1=22,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP23_42_317382[] = { 0x0a82, 0xfa95, 0xf434, 0xeeff, 0xefc4, 0xf850, 0x05e3, 0x124b, 0x1745, 0x124b, 0x05e3, 0xf850, 0xefc4, 0xeeff, 0xf434, 0xfa95, 0x0a82 };  // Bandpass 23kHz-42kHz; Matlab: Fs=317382Hz, FS1=13, FP1=23,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP24_42_317382[] = { 0x0b5f, 0xfbf6, 0xf514, 0xef0e, 0xef11, 0xf74a, 0x0503, 0x11c8, 0x16f3, 0x11c8, 0x0503, 0xf74a, 0xef11, 0xef0e, 0xf514, 0xfbf6, 0x0b5f };  // Bandpass 24kHz-42kHz; Matlab: Fs=317382Hz, FS1=14, FP1=24,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP25_42_317382[] = { 0x0bee, 0xfc98, 0xf5bb, 0xef92, 0xef40, 0xf701, 0x043f, 0x10a8, 0x15b2, 0x10a8, 0x043f, 0xf701, 0xef40, 0xef92, 0xf5bb, 0xfc98, 0x0bee };  // Bandpass 25kHz-42kHz; Matlab: Fs=317382Hz, FS1=15, FP1=25,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP26_42_317382[] = { 0x0c64, 0xfd29, 0xf65d, 0xf022, 0xef85, 0xf6cd, 0x0386, 0x0f86, 0x146a, 0x0f86, 0x0386, 0xf6cd, 0xef85, 0xf022, 0xf65d, 0xfd29, 0x0c64 };  // Bandpass 26kHz-42kHz; Matlab: Fs=317382Hz, FS1=16, FP1=26,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP27_42_317382[] = { 0x0cc8, 0xfdb3, 0xf6fe, 0xf0b5, 0xefd3, 0xf6a4, 0x02d5, 0x0e6a, 0x1325, 0x0e6a, 0x02d5, 0xf6a4, 0xefd3, 0xf0b5, 0xf6fe, 0xfdb3, 0x0cc8 };  // Bandpass 27kHz-42kHz; Matlab: Fs=317382Hz, FS1=17, FP1=27,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP28_42_317382[] = { 0x0d21, 0xfe38, 0xf79b, 0xf14b, 0xf026, 0xf683, 0x022b, 0x0d54, 0x11e6, 0x0d54, 0x022b, 0xf683, 0xf026, 0xf14b, 0xf79b, 0xfe38, 0x0d21 };  // Bandpass 28kHz-42kHz; Matlab: Fs=317382Hz, FS1=18, FP1=28,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP29_42_317382[] = { 0x0d71, 0xfeb9, 0xf836, 0xf1e0, 0xf07d, 0xf666, 0x0186, 0x0c42, 0x10ac, 0x0c42, 0x0186, 0xf666, 0xf07d, 0xf1e0, 0xf836, 0xfeb9, 0x0d71 };  // Bandpass 29kHz-42kHz; Matlab: Fs=317382Hz, FS1=19, FP1=29,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP30_42_317382[] = { 0x0dbd, 0xff37, 0xf8cf, 0xf275, 0xf0d4, 0xf64b, 0x00e5, 0x0b34, 0x0f75, 0x0b34, 0x00e5, 0xf64b, 0xf0d4, 0xf275, 0xf8cf, 0xff37, 0x0dbd };  // Bandpass 30kHz-42kHz; Matlab: Fs=317382Hz, FS1=20, FP1=30,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP31_42_317382[] = { 0x0e09, 0xffb4, 0xf967, 0xf308, 0xf12a, 0xf631, 0x0046, 0x0a29, 0x0e42, 0x0a29, 0x0046, 0xf631, 0xf12a, 0xf308, 0xf967, 0xffb4, 0x0e09 };  // Bandpass 31kHz-42kHz; Matlab: Fs=317382Hz, FS1=21, FP1=31,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP32_42_317382[] = { 0x0e5f, 0x0037, 0xfa07, 0xf3a2, 0xf186, 0xf61f, 0xffb2, 0x092c, 0x0d1d, 0x092c, 0xffb2, 0xf61f, 0xf186, 0xf3a2, 0xfa07, 0x0037, 0x0e5f };  // Bandpass 32kHz-42kHz; Matlab: Fs=317382Hz, FS1=22, FP1=32,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP33_42_317382[] = { 0x0ec4, 0x00c8, 0xfab6, 0xf44c, 0xf1f5, 0xf621, 0xff37, 0x084a, 0x0c12, 0x084a, 0xff37, 0xf621, 0xf1f5, 0xf44c, 0xfab6, 0x00c8, 0x0ec4 };  // Bandpass 33kHz-42kHz; Matlab: Fs=317382Hz, FS1=23, FP1=33,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP34_42_317382[] = { 0x0f3c, 0x0169, 0xfb77, 0xf50c, 0xf27a, 0xf63d, 0xfed8, 0x0788, 0x0b28, 0x0788, 0xfed8, 0xf63d, 0xf27a, 0xf50c, 0xfb77, 0x0169, 0x0f3c };  // Bandpass 34kHz-42kHz; Matlab: Fs=317382Hz, FS1=24, FP1=34,FP2=42,FS2=52; BP, equiripple
uint16_t fir_BP35_42_317382[] = { 0x0fc6, 0x021c, 0xfc51, 0xf5e5, 0xf31e, 0xf67b, 0xfe9e, 0x06ed, 0x0a68, 0x06ed, 0xfe9e, 0xf67b, 0xf31e, 0xf5e5, 0xfc51, 0x021c, 0x0fc6 };  // Bandpass 35kHz-42kHz; Matlab: Fs=317382Hz, FS1=25, FP1=35,FP2=42,FS2=52; BP, equiripple
/*******************/
/* I2S_FS = 317382 */
/*******************/

uint16_t fir_BP18_42_349121[] = {
	0xfd7a, 0xff96, 0xec0b, 0xf0c7, 0xf73d, 0x0027, 				
	0x0bcf, 0x15d0, 0x19b8, 0x15d0, 0x0bcf, 0x0027, 				
	0xf73d, 0xf0c7, 0xec0b, 0xff96, 0xfd7a										
}; // Bandpass 18kHz-42kHz; Matlab: Fs=349121Hz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple
	
uint16_t fir_BP18_42_380859[] = { 
    0xfa8d, 0xfbfd, 0xeadd, 0xf2f7, 0xfc26, 0x02ed, 
	0x0be8, 0x1484, 0x17f9, 0x1484, 0x0be8, 0x02ed, 				
	0xfc26, 0xf2f7, 0xeadd, 0xfbfd, 0xfa8d											
}; // Bandpass 18kHz-42kHz; Matlab: Fs=380859Hz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple

uint16_t fir_BP18_42_399902[] = {
	0xf6d8, 0xfb28, 0xed09, 0xf25c, 0xfe4d, 0x0598, 				
	0x0ba8, 0x1353, 0x1745, 0x1353, 0x0ba8, 0x0598, 				
	0xfe4d, 0xf25c, 0xed09, 0xfb28, 0xf6d8														
}; // Bandpass 18kHz-42kHz; Matlab: Fs=399902Hz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple
	
uint16_t fir_BP18_42_406250[] = {
	0xf54f, 0xfadd, 0xee9d, 0xf21c, 0xfdf9, 0x06a1, 				
	0x0c88, 0x12d7, 0x161b, 0x12d7, 0x0c88, 0x06a1, 				
	0xfdf9, 0xf21c, 0xee9d, 0xfadd, 0xf54f																	
}; // Bandpass 18kHz-42kHz; Matlab: Fs=406250Hz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
apu_fir_coefficients_t apu_fir_coefficients_options[] = // entries should match 
    {
        {   8190,  0, 42, fir_unity }, 
        { 253906, 18, 42, fir_BP18_42_253906 }, 
        { 285644, 18, 42, fir_BP18_42_285644 },
        { 317382, 15, 42, fir_BP15_42_317382 },
        { 317382, 16, 42, fir_BP16_42_317382 },
        { 317382, 17, 42, fir_BP17_42_317382 },
        { 317382, 18, 42, fir_BP18_42_317382 },
        { 317382, 19, 42, fir_BP19_42_317382 },
        { 317382, 20, 42, fir_BP20_42_317382 },
        { 317382, 21, 42, fir_BP21_42_317382 },
        { 317382, 22, 42, fir_BP22_42_317382 },
        { 317382, 23, 42, fir_BP23_42_317382 },
        { 317382, 24, 42, fir_BP24_42_317382 },
        { 317382, 25, 42, fir_BP25_42_317382 },
        { 317382, 26, 42, fir_BP26_42_317382 },
        { 317382, 27, 42, fir_BP27_42_317382 },
        { 317382, 28, 42, fir_BP28_42_317382 },
        { 317382, 29, 42, fir_BP29_42_317382 },
        { 317382, 30, 42, fir_BP30_42_317382 },
        { 317382, 31, 42, fir_BP31_42_317382 },
        { 317382, 32, 42, fir_BP32_42_317382 },
        { 317382, 33, 42, fir_BP33_42_317382 },
        { 317382, 34, 42, fir_BP34_42_317382 },
        { 317382, 35, 42, fir_BP35_42_317382 },
        { 349121, 18, 42, fir_BP18_42_349121 },
        { 380859, 18, 42, fir_BP18_42_380859 },
        { 399902, 18, 42, fir_BP18_42_399902 },
        { 406250, 18, 42, fir_BP18_42_406250 }
    };


// update all things affected by change in sampling frequency:
// + BF delays
// + FIR filter coefficients
// + BP frequency bin limit for FFT
// Discrete frequency options for now. 

int update_all_i2s_fs(i2s_fs_options_t i2s_fs_sel) 
{
    i2s_fs_options_t i2s_fs_sel_old = i2s_fs_sel_current;   // save old value
    I2S_FS_actual = set_I2S_FS(apu_fir_coefficients_options[i2s_fs_sel].i2s_fs);
    //mcu_stdio_printf("i2s_fs_sel = %d, apu_fir_coefficients_options[i2s_fs_sel].i2s_fs = %d, I2S_FS_actual = %d\n\r", i2s_fs_sel, apu_fir_coefficients_options[i2s_fs_sel].i2s_fs, I2S_FS_actual); 
    //mcu_stdio_printf("bp_low = %d, bp_high = %d\n\r", apu_fir_coefficients_options[i2s_fs_sel].bp_low, apu_fir_coefficients_options[i2s_fs_sel].bp_high); 
    if (I2S_FS_actual != apu_fir_coefficients_options[i2s_fs_sel].i2s_fs)
    {
        printf("ERROR: I2S_FS not set... [Expected: %d]-[Actual: %d]\n\r", apu_fir_coefficients_options[i2s_fs_sel].i2s_fs, I2S_FS_actual);
        i2s_fs_sel_current = i2s_fs_sel_old;   // restore previous value
        return(-1);   // quit update
    }
    i2s_fs_sel_current = i2s_fs_sel; // OK to update

//    if (Debug_incident)
//		apu_set_delays_incident(I2S_FS_actual, UCA8_RADIUS_CM,UCA_N, AZM_Angle);
//    else
//		apu_set_delays(I2S_FS_actual, UCA8_RADIUS_CM, UCA_N, FOV_Angle);
        
    BP_Low = apu_fir_coefficients_options[i2s_fs_sel_current].bp_low * 1000;
    BP_High = apu_fir_coefficients_options[i2s_fs_sel_current].bp_high * 1000;
    BP_LOW_N  = (uint16_t)(APU_DIR_CHANNEL_SIZE * BP_Low / I2S_FS_actual /2);
    BP_HIGH_N = (uint16_t)(APU_DIR_CHANNEL_SIZE * BP_High / I2S_FS_actual /2);
    apu_dir_set_prev_fir(apu_fir_coefficients_options[i2s_fs_sel_current].apu_coefficients);
    apu_dir_set_post_fir(apu_fir_coefficients_options[i2s_fs_sel_current].apu_coefficients);
    //apu_dir_set_prev_fir(fir_unity);
    //apu_dir_set_post_fir(fir_unity);
    
    apu_voc_set_prev_fir(fir_unity); 
    apu_voc_set_post_fir(fir_unity);
    apu_dir_reset();
    usleep(1000);
    apu_dir_enable();
    return (0);
};

/*********************************************************************************
* This function will be used along with the apu_fir_coefficients_options[]
* array to determine the appropriate i2s_fs_sel enum value for the given I2S_FS, 
* BP_Low_Freq & BP_High_Freq values. When we have the ability to calculate the APU 
* FIR filter coefficients dynamically then we can eliminate related selection code.
* The simplest approach at this point is to ignore all I2S_FS values other than the 
* default, which is 317382Hz. 
* 
* A future version of this function will compute all APU filter parameters using
* the actual values of I2S_FS, BP_Low & BP_Val frequencies and other things like
* the stop frequencies. For now all of these a fixed options. The hgh BP filter 
* may be included with limited oiptions for test but the main I2S_FS is just too 
* complicated to deal with for now. All ae included in the function header.
*********************************************************************************/
uint8_t configure_apu_filters(uint32_t I2S_FS_target, uint8_t BP_Low_Freq_target, uint8_t BP_High_Freq_target)
{
    // We have a large number of combinations of BP_Low_Freq_target, BP_High_Freq_target & I2S_FS_target 
    // to work with but only a limited number of options as per apu_fir_coefficients_options[] above.
    // The target values are supplied by the host but I2S_FS provides the least # of options so we we will 
    // only use the default value at this point (317382Hz) 
    // With I2S_FS fitting into a 6 char string and assuming the BP target values will be <2 chars each, then 
    // we can determine the right index by sprintfing the target values into a string[13] array (with NULL)  
    // and strcmping these against similarly constructed strings from the above array 
    // indexed over the range I2S_FS_LPM > index < I2S_FS_MAX. The indices at either end are not used here
    // nor are the indexed entries with I2S_FS<>317382. These will be skipped.

    char target_str[13];
    memset(target_str, '\0', sizeof target_str);
    sprintf(target_str, "%6d%2d%2d", 317382, BP_Low_Freq_target, BP_High_Freq_target);    // Build the target compare string
	//mcu_stdio_printf("target_str = %s\n\r", target_str);

    char lookup_str[13];
    memset(lookup_str, '\0', sizeof lookup_str);

    for (uint8_t i2s_fs_bp_sel = I2S_FS_LPM; i2s_fs_bp_sel < I2S_FS_MAX; i2s_fs_bp_sel++)
    {        
        uint32_t i2s_fs_cmp = apu_fir_coefficients_options[i2s_fs_bp_sel].i2s_fs;
        uint8_t BP_Low_Freq_cmp = apu_fir_coefficients_options[i2s_fs_bp_sel].bp_low;
        uint8_t BP_High_Freq_cmp = apu_fir_coefficients_options[i2s_fs_bp_sel].bp_high;
        sprintf(lookup_str, "%6d%2d%2d", i2s_fs_cmp, BP_Low_Freq_cmp, BP_High_Freq_cmp);    // Build the target compare string

        if (0 == strcmp(target_str, lookup_str))
        {
            //mcu_stdio_printf("String Found: target_str = %s\n\r", target_str);
            update_all_i2s_fs(i2s_fs_bp_sel); // Got a hit so update settings
        }
    }
    
    return 0;
}


void set_apu_bp_low_freq(uint32_t BP_Low_Freq_target)
{
    uint8_t bp_freq = BP_Low_Freq_target / 1000;
    if (bp_freq < 15)
        bp_freq = 15;
    else if (bp_freq > 35)
        bp_freq = 35;
    configure_apu_filters(I2S_FS_actual, bp_freq, (uint8_t)(BP_High / 1000));
}    

void set_apu_bp_high_freq(uint32_t BP_High_Freq_target)
{
    uint8_t bp_freq = BP_High_Freq_target / 1000;
    if (bp_freq < 42) // Unused for now nut may be modified if additional coef sets are defined with different high BP freq's
        bp_freq = 42;
    else if (bp_freq > 42)
        bp_freq = 42;
    configure_apu_filters(I2S_FS_actual, (uint8_t)(BP_Low / 1000), (uint8_t)(BP_High_Freq_target / 1000));
}    

void set_apu_gain(float gain_r)
{
    if (gain_r < (float)0x0001 / 0x0400)  
        gain_r = (float)0x0001 / 0x0400;  
    if (gain_r > (float)0x07FF / 0x0400)
        gain_r = (float)0x07FF / 0x0400;
    APU_Gain_u = (uint16_t)(gain_r * 0x0400);
    apu_set_audio_gain(APU_Gain_u);
    //mcu_stdio_printf("APU_Gain = 0x%x\n\r", APU_Gain_u);
    APU_Gain = (float)(apu_get_audio_gain()) / 0x0400;
}    
    
void set_FOV_Angle(float angle)
{
    // FOV_Angle==0 is probably best to not have as an option since each DIR  gets exact same result regardless of where source is positioned
    // Could have the F/W try angles that result in the minimum angle that provides real delay discrimination.
    // By default this might be the angle that results in a rounding up to 1 from 0.6+ on at least one DIR test 
    // Could even have the code perform a min(SSQ_ERR) calc
    Angle_BF_AZM_static = angle;
    if (Angle_BF_AZM_static < 0)  
        Angle_BF_AZM_static = 0;  
    if (Angle_BF_AZM_static > 180)  
        Angle_BF_AZM_static= 180;  
    //mcu_stdio_printf("\nFOV_Angle = %f\n\r", FOV_Angle);
	apu_set_delays(I2S_FS_actual, UCA8_RADIUS_CM, UCA_N, Angle_BF_AZM_static);
    
}    

void set_AZM_Angle(float angle)
{
    // FOV_Angle==0 is probably best to not have as an option since each DIR  gets exact same result regardless of where source is positioned
    // Could have the F/W try angles that result in the minimum angle that provides real delay discrimination.
    // By default this might be the angle that results in a rounding up to 1 from 0.6+ on at least one DIR test 
    // Could even have the code perform a min(SSQ_ERR) calc
    Angle_BF_INC_static = angle;
    if (Angle_BF_INC_static < -180)  
        Angle_BF_INC_static = -180;  
    if (Angle_BF_INC_static > 180)  
        Angle_BF_INC_static= 180;  
    //mcu_stdio_printf("\AZM_Anglee = %f\n\r", AZM_Angle);
	apu_set_delays_incident(I2S_FS_actual, UCA8_RADIUS_CM, UCA_N, Angle_BF_INC_static);
}    



void init_i2s(void)
{
	printk("init i2s.\n\r");

	/* I2s init */
    i2s_init(I2S_DEVICE_0, I2S_RECEIVER, 0x3);

  // evaluate diffeent values for trigger_level input other than TRIGGER_LEVEL_4
  i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_0,
            RESOLUTION_16_BIT, SCLK_CYCLES_16,
            TRIGGER_LEVEL_4, STANDARD_MODE);
  i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_1,
            RESOLUTION_16_BIT, SCLK_CYCLES_16,
            TRIGGER_LEVEL_4, STANDARD_MODE);
  i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_2,
            RESOLUTION_16_BIT, SCLK_CYCLES_16,
            TRIGGER_LEVEL_4, STANDARD_MODE);
  i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_3,
            RESOLUTION_16_BIT, SCLK_CYCLES_16,
            TRIGGER_LEVEL_4, STANDARD_MODE);

  i2s_set_dma_divide_16(I2S_DEVICE_0, 1);
  // i2s_fs is set with call to update_all_i2s_fs() in init_bf()
}


void init_bf(void)
{
	printk("init bf.\n\r");

    // Should have a global variable of type i2s_fs_options_that indicates the current frequency we're working with
    // Could also set BP limits but that means more entries in tabel and new colum(s) for BP frequencies
    // Will have to stanbdardize on roll-off frequencies... 10kHz seems OK
	
    update_all_i2s_fs(i2s_fs_sel_current);

	apu_set_smpl_shift(APU_SMPL_SHIFT);
	//apu_voc_set_saturation_limit(APU_SATURATION_VPOS_DEBUG, APU_SATURATION_VNEG_DEBUG);
    apu_voc_set_saturation_limit(0xFFFF, 0xFFFF);
	APU_Gain_u = APU_AUDIO_GAIN;
    
    set_apu_gain((float)APU_Gain_u / 0x0400); 
	
	apu_voc_set_direction(0);
	apu_set_channel_enabled(0xff);
	apu_set_down_size(0, 0);

//#if APU_FFT_ENABLE
    // enable the FFT in APU seems not working
//    apu_set_fft_shift_factor(1, 0xa0); // 0xaa
//#else
	apu_set_fft_shift_factor(0, 0);
//#endif

	apu_set_interrupt_mask(APU_DMA_ENABLE, APU_DMA_ENABLE);
#if APU_DIR_ENABLE
	apu_dir_enable();
#endif
#if APU_VOC_ENABLE == 1
	apu_voc_enable(1);
#else
	apu_voc_enable(0);
#endif
}

#if APU_DMA_ENABLE
void init_dma(void)
{
	printk("%s\n\r", __func__);
	// dmac enable dmac and interrupt
	union dmac_cfg_u dmac_cfg;

	dmac_cfg.data = readq(&dmac->cfg);
	dmac_cfg.cfg.dmac_en = 1;
	dmac_cfg.cfg.int_en = 1;
	writeq(dmac_cfg.data, &dmac->cfg);

	sysctl_dma_select(SYSCTL_DMA_CHANNEL_0 + APU_DIR_DMA_CHANNEL,
			  SYSCTL_DMA_SELECT_I2S0_BF_DIR_REQ);
	sysctl_dma_select(SYSCTL_DMA_CHANNEL_0 + APU_VOC_DMA_CHANNEL,
			  SYSCTL_DMA_SELECT_I2S0_BF_VOICE_REQ);
}
#endif

void init_dma_ch(int ch, volatile uint32_t *src_reg, void *buffer,
		 size_t size_of_byte)
{
	printk("%s %d\n\r", __func__, ch);

	dmac->channel[ch].sar = (uint64_t)src_reg;
	dmac->channel[ch].dar = (uint64_t)buffer;
	dmac->channel[ch].block_ts = (size_of_byte / 4) - 1;
	dmac->channel[ch].ctl =
		(((uint64_t)1 << 47) | ((uint64_t)15 << 48)
		 | ((uint64_t)1 << 38) | ((uint64_t)15 << 39)
		 | ((uint64_t)3 << 18) | ((uint64_t)3 << 14)
		 | ((uint64_t)2 << 11) | ((uint64_t)2 << 8) | ((uint64_t)0 << 6)
		 | ((uint64_t)1 << 4) | ((uint64_t)1 << 2) | ((uint64_t)1));
	/*
	 * dmac->channel[ch].ctl = ((  wburst_len_en  ) |
	 *                        (    wburst_len   ) |
	 *                        (  rburst_len_en  ) |
	 *                        (    rburst_len   ) |
	 *                        (one transaction:d) |
	 *                        (one transaction:s) |
	 *                        (    dst width    ) |
	 *                        (    src width   ) |
	 *                        (    dinc,0 inc  )|
	 *                        (  sinc:1,no inc ));
	 */

	dmac->channel[ch].cfg = (((uint64_t)1 << 49) | ((uint64_t)ch << 44)
				 | ((uint64_t)ch << 39) | ((uint64_t)2 << 32));
	/*
	 * dmac->channel[ch].cfg = ((     prior       ) |
	 *                         (      dst_per    ) |
	 *                         (     src_per     )  |
	 *           (    peri to mem  ));
	 *  01: Reload
	 */

	dmac->channel[ch].intstatus_en = 0x2; // 0xFFFFFFFF;
	dmac->channel[ch].intclear = 0xFFFFFFFF;

	dmac->chen = 0x0101 << ch;
}


void init_interrupt(void)
{
	//plic_init();
	// bf
	plic_set_priority(IRQN_I2S0_INTERRUPT, 4);
	plic_irq_enable(IRQN_I2S0_INTERRUPT);
	plic_irq_register(IRQN_I2S0_INTERRUPT, int_apu, NULL);

#if APU_DMA_ENABLE
	// dma
	plic_set_priority(IRQN_DMA0_INTERRUPT + APU_DIR_DMA_CHANNEL, 4);
	plic_irq_register(IRQN_DMA0_INTERRUPT + APU_DIR_DMA_CHANNEL,
			  int_apu_dir_dma, NULL);
	plic_irq_enable(IRQN_DMA0_INTERRUPT + APU_DIR_DMA_CHANNEL);
	// dma
	plic_set_priority(IRQN_DMA0_INTERRUPT + APU_VOC_DMA_CHANNEL, 4);
	plic_irq_register(IRQN_DMA0_INTERRUPT + APU_VOC_DMA_CHANNEL,
			  int_apu_voc_dma, NULL);
	plic_irq_enable(IRQN_DMA0_INTERRUPT + APU_VOC_DMA_CHANNEL);
#endif
}

void init_all(void)
{
	init_fpioa();
	init_interrupt();
	init_i2s();
	init_bf();
	if (APU_DMA_ENABLE) {
		#if APU_DMA_ENABLE
		init_dma();
		#endif
//#if APU_FFT_ENABLE
		// init_dma_ch(APU_DIR_DMA_CHANNEL,
		// 	    &apu->sobuf_dma_rdata,
		// 	    APU_DIR_FFT_BUFFER[0], 512 * 4);
		// init_dma_ch(APU_VOC_DMA_CHANNEL,
		// 	    &apu->vobuf_dma_rdata, APU_VOC_FFT_BUFFER,
		// 	    512 * 4);
//#else
		// init_dma_ch(APU_DIR_DMA_CHANNEL,
		// 	    &apu->sobuf_dma_rdata, APU_DIR_BUFFER,
		// 	    512 * 16 * 2);
		// init_dma_ch(APU_VOC_DMA_CHANNEL,
		// 	    &apu->vobuf_dma_rdata, APU_VOC_BUFFER,
		// 	    512 * 2);
//#endif
	}
	
  // apu_print_setting();
}
#endif