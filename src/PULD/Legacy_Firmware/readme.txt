This is a repository of all legacy F/W that is or may be installed on the REDspace Firestore server.
To install any of these as the primary binary copy the entirefolder to the ./src/PULD folder
in your repo and use the install_mcu_ota_fw.bat batch file with the LEG argument. 
This will force the script to use the filename as specified on the command line. 

A file would be installed as follows from the .\OTATools folder
	install_mcu_ota_fw.bat COM12 PULD_V0.1.2_DEBUG_enc ENC
	NOTE: If the binary is encrypted the ENC command line arg nust be included.


Dec 17, 2020 (file dates match server)
	PULD_0.1.#.bin are identical except for version string
	PULD_0.1.3.bin << Deployed
	PULD#0.0.# different from each other by version string but by a lot from DULD versions.


0.0.0_FRF/
	*** Built from cc16a1 2021-07-27... supports high-speed multicast OTA as well as Low Power Mode (LPM) and aligned with app
	PULD_V0.0.0_FRF.bin           <<< DECrypted, RELease version
	PULD_V0.0.0_FRF_enc.bin       <<< ENCrypted, REL

0.1.3/
	*** DO NOT USE ****
	PULD_V0.1.3_2020-12-18.bin - the broken F/W that was installed on PP devices on Dec 31. CVS tag wasn't updated.
	*******************

0.1.4/
	*** Based on V0.1.3 but with improvements to DBFS calc
	PULD_V0.1.4.2021.01.22#f19deb66.bin  << Includes DBFS formula corrections. Works well but has vulnerability
	PULD_V0.1.4.2021.01.25#3b95e0c3.bin  << Deployed: Includes UART & ota_data_handler fixes that prevents core fault during OTA
NOTE: This F/W WILL NOT work with evices running bootloader v2.0.1 and later
	
0.1.5/
	*** Installers, OTA Support & Beamformer Improvements
	PULD_V0.1.5_2021-02-04_164b9e77_DEBUG.bin 
	PULD_V0.1.5_2021-02-04_164b9e77_RELEASE.bin
	
0.1.6/	
	*** Code Cleanup 
	PULD_V0.1.6_2021-02-10_5ab3514c_DEBUG.bin
	PULD_V0.1.6_2021-02-10_5ab3514c_DEBUG_enc.bin
	PULD_V0.1.6_2021-02-10_5ab3514c_RELEASE.bin
	PULD_V0.1.6_2021-02-10_5ab3514c_RELEASE_enc.bin
	
0.2.0/
	*** Legacy bootloader support
	PULD_V0.2.0_2021-02-18_c5edc08_DEBUG
    PULD_V0.2.0_2021-02-18_c5edc08_DEBUG_enc
    PULD_V0.2.0_2021-02-18_c5edc08_RELEASE
    PULD_V0.2.0_2021-02-18_c5edc08_RELEASE_enc