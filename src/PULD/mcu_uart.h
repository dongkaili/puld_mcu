#ifndef __mcu_UART_H__
#define __mcu_UART_H__

#include "uart.h"
#include "mcu_common_define.h"

#define RING_BUFFER_LEN 14400


#if defined USE_QUEUE_BUFFER
typedef void (*recv_callback_func_t)(uint8_t index, const uint8_t *buf, int32_t len);
#else
typedef void (*recv_callback_func_t)(uint8_t index, const uint8_t *buf, int32_t len, int32_t shift_len);
#endif
//void received_callback_register(callback_func_t callback);


void mcu_uart_init(uint8_t index, recv_callback_func_t callback);

int8_t mcu_uart_send_data(uint8_t index, const uint8_t *buffer, size_t buf_len);

void mcu_uart_loop(uint8_t index);
uint32_t mcu_uart_get_actual_set_baud_rate(uint8_t uart_index);
uint32_t mcu_uart_get_nominal_baud_rate(uint8_t uart_index);
void mcu_uart_set_nominal_baud_rate(uint8_t uart_index, uint32_t baud_rate_sp);

void mcu_uart_change_baud_rate(uint8_t uart_index, uint32_t baud_rate_sp);

#ifdef OTA_TEST
void ota_test_data_receive(uint8_t* v_buf, size_t buf_len);
#endif

#endif
