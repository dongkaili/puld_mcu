#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "mcu_internal_fsm.h"

void mcu_fsm_register(mcu_fsm_s* p_fsm, mcu_fsm_table_s* p_table, uint32_t count, mcu_fsm_state_t first_state) {
    p_fsm->table = p_table;
    p_fsm->size = count;
    p_fsm->state = first_state;
}

void mcu_fsm_event_handler(uint8_t index, mcu_fsm_s* p_fsm, mcu_fsm_event_t event) {
    mcu_fsm_state_t next_state = p_fsm->state;
    mcu_fsm_handle_func_t trans_handle = NULL;
    mcu_fsm_condition_func_t condication = NULL;
    void *data = NULL;
    for(uint32_t i = 0; i < p_fsm->size; i ++) {
        if(event == p_fsm->table[i].event 
            && p_fsm->state == p_fsm->table[i].cur_state) {
                condication = p_fsm->table[i].condition;
                data = p_fsm->table[i].data;
                next_state = p_fsm->table[i].next_state;
                trans_handle = p_fsm->table[i].handle_func;
                break;
            }
    }
    if((NULL == condication) ||
        (NULL != condication && 0 == condication(data))) {
        p_fsm->state = next_state;
        if(NULL != trans_handle)
            trans_handle(index, p_fsm);
        
    }
}
