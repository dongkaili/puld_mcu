#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "bsp.h"
#include "sysctl.h"
#include "uart.h"
#include "mcu_ota_handler.h"
#include "mcu_internal_fsm.h"
#include "mcu_comm_protocol.h"
#include "mcu_commands_handler.h"
#include "mcu_common.h"
#include "sha256.h"

#define OTA_STATE_TIMEOUT   10000000

uint8_t slave_response_count = 0;
int mcu0_receive_rsp_count = 0;
int mcu0_receive_multicast_cmd = 0;

typedef enum {
    STATE_NORMAL,
    STATE_OTA_WAIT_FRAME,
    STATE_OTA_FRAME_PROCESSING,
    STATE_OTA_UPDATE_CONFIG,
    //STATE_OTA_MAX,
} MCU_WORKING_STATE;

static const char* MCU_WORKING_STATE_str[] =  {
    "STATE_NORMAL",
    "STATE_OTA_WAIT_FRAME",
    "STATE_OTA_FRAME_PROCESSING",
    "STATE_OTA_UPDATE_CONFIG",
};

typedef struct {
    mcu_fsm_s fsm;
    mcu_comm_data_s* event_data;
    mcu_fsm_state_t state;
    write_t flash_write;
    read_t flash_read;
    reset_t flash_reset;
    mcu_ota_info_s info;
    uint32_t data_count;
	uint8_t can_ota;
    bool    ota_start_flag;
} mcu_ota_handle_context_s;

static mcu_ota_handle_context_s sc = {
    .fsm.state = STATE_NORMAL,
    .state = STATE_NORMAL,
    .data_count = 0,
    .ota_start_flag = false,
};

static _data_header_s head_data_LLC = {
    .head = 0x11,
    .version = 0x02,
    .type = 0x0502,
};

static _data_header_s head_data_JSON = {
    .head = 0x11,
    .version = 0x02,
    .type = 0x0101,
};

static _data_header_s head_data_BIN = {
    .head = 0x11,
    .version = 0x02,
    .type = 0x0600,
};

static uint64_t current_time;
static uint64_t state_enter_time;

extern void mcu_reboot();
extern void send_string(uart_device_number_t index, char* str, _data_header_s head_data);

extern void send_raw_bytes(uart_device_number_t index, uint8_t* bytes, uint32_t byte_size, _data_header_s head_data);

// static uint8_t app_encrypt_check(uint8_t* code_flash_ptr);

// static uint8_t app_encrypt_check(uint8_t* code_flash_ptr)
// {
//     uint8_t ret = 1;
//     if ((code_flash_ptr[0] == 'P') &&
//         (code_flash_ptr[1] == 'r') &&
//         (code_flash_ptr[2] == 'o') &&
//         (code_flash_ptr[3] == 's') &&
//         (code_flash_ptr[4] == 'a') &&
//         (code_flash_ptr[5] == 'r') &&
//         (code_flash_ptr[6] == 'i') &&
//         (code_flash_ptr[7] == 's'))
//     {        
//         ret = 0;
//     }
//     return ret;
// }

static void swap_data32(uint32_t address) {
    uint8_t* pdata = (uint8_t*)address;
    uint8_t buf4[4];
    buf4[0] = pdata[0];
    buf4[1] = pdata[1];
    buf4[2] = pdata[2];
    buf4[3] = pdata[3];
    pdata[0] = buf4[3];
    pdata[1] = buf4[2];
    pdata[2] = buf4[1];
    pdata[3] = buf4[0];
}

static uint32_t get_app_size(uint32_t app_addr) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    //uint32_t app_size = 0;
    uint8_t data[5] = {1};
    sc.flash_read(app_addr, data, 5);
    uint32_t size = data[1];
    size += data[2] << 8;
    size += data[3] << 16;
    size += data[4] << 24;
    //printf("get_app_size: %u\n\r", size);
    //printf("flag: %d\n\r", data[0]);
    return size;
}

static void ota_write_app_head_to_flash(uint32_t appsize) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    uint32_t ota_app_launch_address = (DEFAULT_OTA_APP_LAUNCH_ADDRESS == sc.info.cur_config_info.app_address) ? DEFAULT_OTA_APP_B_LAUNCH_ADDRESS : DEFAULT_OTA_APP_LAUNCH_ADDRESS;
    // printf("ota_write_app_head_to_flash app_address: 0x%08x\n\r", DEFAULT_OTA_APP_B_LAUNCH_ADDRESS);
    // printf("ota_write_app_head_to_flash app_size=%u\n\r", appsize);
    uint8_t aes = 0;
    uint8_t* test_buf = (uint8_t*)(&aes);
    sc.flash_write(ota_app_launch_address, test_buf, 1);
    test_buf = (uint8_t*)(&appsize);
    sc.flash_write(ota_app_launch_address + 1, test_buf, 4);
}

static void ota_write_app_SHA_to_flash(uint32_t appsize, uint8_t* hash) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    uint32_t ota_app_launch_address = (DEFAULT_OTA_APP_LAUNCH_ADDRESS == sc.info.cur_config_info.app_address) ? DEFAULT_OTA_APP_B_LAUNCH_ADDRESS : DEFAULT_OTA_APP_LAUNCH_ADDRESS;
    ota_app_launch_address += (5 + appsize);
    sc.flash_write(ota_app_launch_address, hash, SHA256_HASH_LEN);
}

static void ota_write_app_to_flash(uint8_t* p_data, uint32_t offset, uint32_t size) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    uint32_t ota_app_launch_address = (DEFAULT_OTA_APP_LAUNCH_ADDRESS == sc.info.cur_config_info.app_address) ? DEFAULT_OTA_APP_B_LAUNCH_ADDRESS : DEFAULT_OTA_APP_LAUNCH_ADDRESS;
    ota_app_launch_address = (ota_app_launch_address + 5 + offset);
    sc.flash_write(ota_app_launch_address, p_data, size);
    //uint64_t stop = sysctl_get_time_us();
}

static void gen_hash(uint8_t* p_hash, uint32_t app_addr, uint32_t app_size) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    sha256_context_t context;
    uint8_t buffer[1024] = {0};
    int size = app_size + 5;
    int sz = 0;
    sha256_init(&context, size);
    uint32_t idx = 0;

    while (size > 0) {
        sz = (size >= 1024) ? 1024 : size;
        sc.flash_read(app_addr + idx, buffer, sz);
        sha256_update(&context, buffer, sz);
        idx += sz;
        size -= sz;
    }

    sha256_final(&context, p_hash);
}

static uint32_t check_app_sha256(bootloader_config_info_s info)
{
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    //printf("check_app_sha256 app_size=%u\n\r", info.app_size);
    uint8_t hash[SHA256_HASH_LEN] = {0};
    uint8_t app_hash[SHA256_HASH_LEN] = {0};

    gen_hash(hash, info.app_address, info.app_size);

    // get the application's SHA256 hash
    uint32_t offset = info.app_size + 5;
    sc.flash_read(info.app_address + offset, app_hash, SHA256_HASH_LEN);
    int rt = memcmp((void*)hash, (void*)app_hash, SHA256_HASH_LEN);
    //printf("check_app_sha256 rt=%d\n\r", rt);

    if(rt == 0)
        return 1;
    return 0;
}

static int mcu_check_app() {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    int rt = 0;
    bootloader_config_info_s* pdata = &(sc.info.cur_config_info);
    uint8_t* test_buf = (uint8_t*)(pdata);
    memset(pdata, 0, sizeof(bootloader_config_info_s));
    sc.flash_read(CONFIG_DATA_OFFSET, test_buf, sizeof(sc.info.cur_config_info));

    swap_data32((uint32_t)(&pdata->magic_id));
    swap_data32((uint32_t)(&pdata->app_address));
    swap_data32((uint32_t)(&pdata->app_size));
    swap_data32((uint32_t)(&pdata->app_crc));
    //printf("config data id: 0x%08x\n\r", pdata->magic_id);
    if ((pdata->magic_id & CONFIG_MAGIC_MASK) == CONFIG_MAGIC_ID) {
        // check_app_SHA256();
        //printf("flash read: %02x, %02x, %02x\n\r", test_buf[0], test_buf[1], test_buf[2]);
        //printf("config read app_desc:%s, info.app_size:%u\n\r", pdata->app_desc, pdata->app_size);
        //printf("read app app_address: 0x%08x\n\r", pdata->app_address);
        //printf("config read data crc: %u\n\r", pdata->app_crc);
        uint32_t appsize = get_app_size(pdata->app_address);
        //printf("appsize: %u\n\r", appsize);
        if(sc.info.cur_config_info.app_size != appsize) {
            sc.info.cur_config_info.app_size = appsize;
        }
        rt = check_app_sha256(sc.info.cur_config_info);
    }
    return rt;
}

void mcu_copy_factory_reset_config_to_config_sector() {
    uint32_t bin_size = get_app_size(FACTORY_RESET_APP_LAUNCH_ADDRESS);

    bootloader_config_info_s data = {
        .magic_id = CONFIG_MAGIC_ID,
        .app_address = FACTORY_RESET_APP_LAUNCH_ADDRESS,
        .app_size = bin_size,
        .app_crc = 0,
        .cipher_flag = 1,
    };
    swap_data32((uint32_t)(&data.magic_id));
    swap_data32((uint32_t)(&data.app_address));
    swap_data32((uint32_t)(&data.app_size));
    swap_data32((uint32_t)(&data.app_crc));
    swap_data32((uint32_t)(&data.cipher_flag));
    memset(data.app_desc, 0, 16);
    strcpy((char*)(data.app_desc), "FRFW App");
    uint8_t* test_buf = (uint8_t*)(&data);
    sc.flash_write(CONFIG_DATA_OFFSET, test_buf, sizeof(data));
    sc.flash_write(CONFIG_DATA_BACKUP_OFFSET, test_buf, sizeof(data));

}

static void mcu_write_data_to_config_sector() {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    // before update 0x4000 to contain the latest fw info, save it to 0x5000 as a backup
    bootloader_config_info_s* pdata = (bootloader_config_info_s*)malloc(sizeof(bootloader_config_info_s));
    uint8_t* temp_buf = (uint8_t*)(pdata);
    //memset(pdata, 0, sizeof(bootloader_config_info_s));
    sc.flash_read(CONFIG_DATA_OFFSET, temp_buf, sizeof(bootloader_config_info_s));
    sc.flash_write(CONFIG_DATA_BACKUP_OFFSET, temp_buf, sizeof(bootloader_config_info_s));    
    
    uint32_t ota_app_launch_address = (DEFAULT_OTA_APP_LAUNCH_ADDRESS == sc.info.cur_config_info.app_address) ? DEFAULT_OTA_APP_B_LAUNCH_ADDRESS : DEFAULT_OTA_APP_LAUNCH_ADDRESS;
    //printf("write app app_address: 0x%08x\n\r", ota_app_launch_address);
    uint32_t bin_size = get_app_size(ota_app_launch_address);
    
    //uint8_t code_flash_first_line[16];
    //sc.flash_read(ota_app_launch_address + SPI3_BASE_ADDR + ELF_SIZE_OFFSET, code_flash_first_line, 16);
    //uint8_t encryption = app_encrypt_check(&code_flash_first_line[2]);    
    // if (encryption != sc.info.cur_config_info.cipher_flag)
    // {
    //     sc.info.cur_config_info.cipher_flag = encryption;
    // }
    
    bootloader_config_info_s data = {
        .magic_id = CONFIG_MAGIC_ID,
        .app_address = ota_app_launch_address,
        .app_size = bin_size,
        .app_crc = sc.info.crc,
        .cipher_flag = sc.info.cur_config_info.cipher_flag,
    };
    swap_data32((uint32_t)(&data.magic_id));
    swap_data32((uint32_t)(&data.app_address));
    swap_data32((uint32_t)(&data.app_size));
    swap_data32((uint32_t)(&data.app_crc));
    swap_data32((uint32_t)(&data.cipher_flag));
    memset(data.app_desc, 0, 16);
    strcpy((char*)(data.app_desc), sc.info.cur_config_info.app_desc);
    //printf("write app data.app_desc: %s\n\r", data.app_desc);
    uint8_t* test_buf = (uint8_t*)(&data);

    sc.flash_write(CONFIG_DATA_OFFSET, test_buf, sizeof(data));
    free(pdata);
}

mcu_ota_info_s* mcu_ota_info() {
    return &(sc.info);
}

static void mcu_ota_handle_start(uint8_t index, void *p) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    
    sc.ota_start_flag = true;
    slave_response_count = 0;
    mcu_stdio_printf("MCU[%d] F/W Upgrade Started... \n\r", sc.info.target_mcu);

    mcu_wdt_enable(WDT_DEVICE_0, false);
    mcu_wdt_enable(WDT_DEVICE_1, false);

	char strTemp[128];
	//char response[16];
	
	if (sc.info.target_mcu == mcu_id)
	{
		if(sc.can_ota != 1) {
        	_event_ota_s data;
        	data.event_type = OTA_EVENT_ERROR;
    		//data.data = malloc(sizeof(__FUNCTION__) + sizeof(__LINE__) + 2);
			data.data = "Can OTA failed";			
        	mcu_ota_event_handler(&data);
        	return;
    	}
		ota_write_app_head_to_flash(sc.info.bin_size);  // comment it when testing comm only
	    
	    //sprintf(response, "%s_%d", COMMAND_OTA_INFO, mcu_id);
	    //send_string(mcu_uart_dev, response, head_data_LLC);
	    send_string(mcu_uart_dev, COMMAND_OTA_INFO, head_data_LLC);
	    
	    sc.data_count = 0;
	}
    else if (mcu_id == MCU0)
    {
        if (sc.info.target_mcu != MCU3)
        {
            // pass command to MCU1 or MCU2
            sprintf(strTemp, "ota_info -crc %d -version %s -count %d -size %d -target %d -cipher %d", sc.info.crc,
                    sc.info.cur_config_info.app_desc, sc.info.frame_count, sc.info.bin_size,
                    sc.info.target_mcu, sc.info.cur_config_info.cipher_flag);
            send_string(sc.info.target_mcu, strTemp, head_data_LLC);
            sc.data_count = 0;
        }
        else
        {
            
            // MCU0 response the command
            if (sc.can_ota != 1)
            {
                _event_ota_s data;
                data.event_type = OTA_EVENT_ERROR;
                //data.data = malloc(sizeof(__FUNCTION__) + sizeof(__LINE__) + 2);
                data.data = "Can OTA failed";
                mcu_ota_event_handler(&data);
                return;
            }
            ota_write_app_head_to_flash(sc.info.bin_size); // comment it when testing comm only
            //sprintf(response, "%s_%d", COMMAND_OTA_INFO, mcu_id);
            //send_string(mcu_uart_dev, response, head_data_LLC);
            send_string((uart_device_number_t)MCU0, COMMAND_OTA_INFO, head_data_LLC);
            sc.data_count = 0;
            
            // pass command to MCU1 and MCU2
            sprintf(strTemp, "ota_info -crc %d -version %s -count %d -size %d -target %d -cipher %d", sc.info.crc,
                    sc.info.cur_config_info.app_desc, sc.info.frame_count, sc.info.bin_size,
                    MCU1, sc.info.cur_config_info.cipher_flag);
            send_string((uart_device_number_t)MCU1, strTemp, head_data_LLC);

            sprintf(strTemp, "ota_info -crc %d -version %s -count %d -size %d -target %d -cipher %d", sc.info.crc,
                    sc.info.cur_config_info.app_desc, sc.info.frame_count, sc.info.bin_size,
                    MCU2, sc.info.cur_config_info.cipher_flag);
            send_string((uart_device_number_t)MCU2, strTemp, head_data_LLC);
            
        }
    }
}

static void mcu_ota_handle_send_frame_req(uint8_t index, void *p) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    
    char strTemp[128];
    //char response[24];
	if (sc.info.target_mcu == mcu_id)
	{
	    //sprintf(response, "%s_%d", COMMAND_OTA_FRAME, mcu_id);
	    //send_string(mcu_uart_dev, response, head_data_LLC);	
        send_string(mcu_uart_dev, COMMAND_OTA_FRAME, head_data_LLC);	
	}
	else if (mcu_id == MCU0)
	{
	    if (sc.info.target_mcu != MCU3)
	    {
	        // pass the command to MCU1 or MCU2
		    sprintf(strTemp, "ota_frame -index %d -size %d -target %d", sc.info.cur_frame_info.index, sc.info.cur_frame_info.size, sc.info.target_mcu);
	        send_string(sc.info.target_mcu, strTemp, head_data_LLC);
	    }
	    else
	    {
	        // MCU0 response the command
	        //sprintf(response, "%s_%d", COMMAND_OTA_FRAME, mcu_id);
	        //send_string(mcu_uart_dev, response, head_data_LLC);	
	        send_string((uart_device_number_t)MCU0, COMMAND_OTA_FRAME, head_data_LLC);
	        
	        // pass the command to MCU1 and MCU2
	        sprintf(strTemp, "ota_frame -index %d -size %d -target %d", sc.info.cur_frame_info.index, sc.info.cur_frame_info.size, MCU1);
    	    send_string((uart_device_number_t)MCU1, strTemp, head_data_LLC);
	        
	        sprintf(strTemp, "ota_frame -index %d -size %d -target %d", sc.info.cur_frame_info.index, sc.info.cur_frame_info.size, MCU2);
    	    send_string((uart_device_number_t)MCU2, strTemp, head_data_LLC);
	        
	        
	    }
        
	}
	
    
}

static void mcu_ota_handle_error(uint8_t index, void *p) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    char command[MAX_COMMAND_STRING_LEN] = { 0 };
    
    if (sc.info.target_mcu == mcu_id)
	{
		sc.flash_reset();  // comment this when testing comm only
		send_string(mcu_uart_dev, COMMAND_ERROR, head_data_LLC);
    	sprintf(command, "{\"RSP\":\"OTA\", \"MCU\":%d, \"STATE\":\"Fault: %s\"}", mcu_id, (char *)p);
    	send_string(mcu_uart_dev, command, head_data_JSON);
	}	
    sc.ota_start_flag = false;
}

static void mcu_ota_handle_payload(uint8_t index, void *p) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 

    if (sc.info.target_mcu == mcu_id)
    {
        //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
        uint32_t progress_percentage = 0;

		ota_write_app_to_flash((uint8_t *)(sc.event_data->p_data), sc.data_count, sc.event_data->size);
        //usleep(30000); // assume write to flash block takes 20ms, comment this line and uncomment above when needed
		sc.data_count = sc.data_count + sc.event_data->size;
		char command[MAX_COMMAND_STRING_LEN] = {0};
		
        progress_percentage = 100 * sc.data_count / sc.info.bin_size;        
        
        sprintf(command, "%s -i %d -t %d -c %d -s %d", COMMAND_OTA_FRAM_RECEIVED, sc.info.cur_frame_info.index, sc.info.target_mcu, sc.data_count, sc.info.bin_size);
        send_string(mcu_uart_dev, command, head_data_LLC);
		//send_string(mcu_uart_dev, COMMAND_OTA_FRAM_RECEIVED, head_data);
        usleep(5000);

        sprintf(command, "{\"RSP\":\"OTA\", \"MCU\":%d, \"STATE\":%d}", mcu_id, progress_percentage);
        send_string(mcu_uart_dev, command, head_data_JSON);		
        //printf("sc.data_count: %d, sc.info.bin_size = %d\n\r", sc.data_count, sc.info.bin_size);
        if (sc.data_count == sc.info.bin_size) 
        {
            //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
            uint32_t ota_app_launch_address = (DEFAULT_OTA_APP_LAUNCH_ADDRESS == sc.info.cur_config_info.app_address) ? DEFAULT_OTA_APP_B_LAUNCH_ADDRESS : DEFAULT_OTA_APP_LAUNCH_ADDRESS;
        	uint8_t hash[SHA256_HASH_LEN] = {0};
        	gen_hash(hash, ota_app_launch_address, sc.info.bin_size);
        	ota_write_app_SHA_to_flash(sc.info.bin_size, hash);

            //usleep(10000); // 50ms delay to give a bit gap between the two send_string
            //char response[32];
            //sprintf(response, "%s_%d", COMMAND_OTA_RECEIVE_FINISHED, mcu_id);
            //send_string(mcu_uart_dev, response, head_data_LLC);
            send_string(mcu_uart_dev, COMMAND_OTA_RECEIVE_FINISHED, head_data_LLC);
            // MCU0 single or MCU 1/2 directly moves to next state
            _event_ota_s data;
            data.event_type = OTA_EVENT_FRAME_FINISHED;
            data.data = NULL;
            mcu_ota_event_handler(&data);
        } 
        else if (sc.data_count > sc.info.bin_size) 
        {
            //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
            _event_ota_s data;
        	data.event_type = OTA_EVENT_ERROR;
            data.data = NULL;
            //sprintf(data.data, "%s,%d\n\r", __FUNCTION__, __LINE__);
        	mcu_ota_event_handler(&data);
		}
        
    }
    else if ((mcu_id == MCU0) && (sc.info.target_mcu != mcu_id))
    {

        if (sc.info.target_mcu != MCU3)
        {

            //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__);
            // MCU0 pass the binary data to target MCU
            send_raw_bytes(sc.info.target_mcu, (uint8_t *)(sc.event_data->p_data), sc.event_data->size, head_data_BIN);

            sc.data_count = sc.data_count + sc.event_data->size;
            //printf("sc.data_count: %d, sc.info.bin_size = %d\n\r", sc.data_count, sc.info.bin_size);
            if (sc.data_count == sc.info.bin_size)
            {
                //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__);
                // in 1-1 mode, after passing bin data to one of the other 2 MCUs, MCU0 needs to move to next state by itself
                _event_ota_s data;
                data.event_type = OTA_EVENT_FRAME_FINISHED;
                data.data = NULL;
                mcu_ota_event_handler(&data);
            }
            else if (sc.data_count > sc.info.bin_size)
            {
                //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__);
                _event_ota_s data;
                data.event_type = OTA_EVENT_ERROR;
                data.data = NULL;
                mcu_ota_event_handler(&data);
            }
        }
        else if (sc.info.target_mcu == MCU3)
        {
            mcu0_receive_multicast_cmd = 1;
            // MCU0 handle the command
            uint32_t progress_percentage = 0;

		    ota_write_app_to_flash((uint8_t *)(sc.event_data->p_data), sc.data_count, sc.event_data->size);
            //usleep(30000); // assume write to flash block takes 20ms, comment this line and uncomment above when needed
    		sc.data_count = sc.data_count + sc.event_data->size;
    		char command[MAX_COMMAND_STRING_LEN] = {0};
    		
            progress_percentage = 100 * sc.data_count / sc.info.bin_size;        
            
            sprintf(command, "%s -i %d -t %d -c %d -s %d", COMMAND_OTA_FRAM_RECEIVED, sc.info.cur_frame_info.index, MCU0, sc.data_count, sc.info.bin_size);
            send_string((uart_device_number_t)MCU0, command, head_data_LLC);
    		//send_string(mcu_uart_dev, COMMAND_OTA_FRAM_RECEIVED, head_data);
            usleep(5000);
    
            sprintf(command, "{\"RSP\":\"OTA\", \"MCU\":%d, \"STATE\":%d}", mcu_id, progress_percentage);
            send_string((uart_device_number_t)MCU0, command, head_data_JSON);

            if (sc.data_count == sc.info.bin_size)
            {
                //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__);
                uint32_t ota_app_launch_address = (DEFAULT_OTA_APP_LAUNCH_ADDRESS == sc.info.cur_config_info.app_address) ? DEFAULT_OTA_APP_B_LAUNCH_ADDRESS : DEFAULT_OTA_APP_LAUNCH_ADDRESS;
                uint8_t hash[SHA256_HASH_LEN] = {0};
                gen_hash(hash, ota_app_launch_address, sc.info.bin_size);
                ota_write_app_SHA_to_flash(sc.info.bin_size, hash);

                //char response[32];
                //sprintf(response, "%s_%d", COMMAND_OTA_RECEIVE_FINISHED, mcu_id);
                //send_string(MCU0, response, head_data_LLC);
                send_string((uart_device_number_t)MCU0, COMMAND_OTA_RECEIVE_FINISHED, head_data_LLC);
                // MCU 0 finished all frame, but not going to next state, will wait for response from mcu 1/2
                
            }
            else if (sc.data_count > sc.info.bin_size)
            {
                //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__);
                _event_ota_s data;
                data.event_type = OTA_EVENT_ERROR;
                data.data = NULL;
                //sprintf(data.data, "%s,%d\n\r", __FUNCTION__, __LINE__);
                mcu_ota_event_handler(&data);
            }
            
            // pass data to MCU1 and MCU2
            send_raw_bytes((uart_device_number_t)MCU1, (uint8_t *)(sc.event_data->p_data), sc.event_data->size, head_data_BIN);
            //usleep(10000);
            send_raw_bytes((uart_device_number_t)MCU2, (uint8_t *)(sc.event_data->p_data), sc.event_data->size, head_data_BIN);
            
        }
    }
    
}

static void mcu_ota_handle_config(uint8_t index, void *p) {
    //mcu_stdio_printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
   
    if ((sc.info.target_mcu == mcu_id) || (mcu_id == MCU0 && sc.info.target_mcu == MCU3))
    {
        mcu_write_data_to_config_sector();   // comment this when testing comm only
    }
        
    _event_ota_s data;
    data.event_type = OTA_EVENT_CONFIG_UPDATED;
    data.data = NULL;
    mcu_ota_event_handler(&data);
}

static void mcu_ota_handle_reboot(uint8_t index, void *p) {
    //mcu_stdio_printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    sc.ota_start_flag = false;
    char command[MAX_COMMAND_STRING_LEN];
    memset(command, '\0', sizeof command);

    mcu_stdio_printf("MCU[%d] F/W Upgrade Complete...\n\r", sc.info.target_mcu);
    if (sc.info.target_mcu == mcu_id || (mcu_id == MCU0 && sc.info.target_mcu == MCU3))
    {
        sprintf(command, "{\"RSP\":\"OTA\", \"MCU\":%d, \"STATE\":\"COMPLETE\"}", mcu_id);
        send_string(mcu_uart_dev, command, head_data_JSON);
        // if (mcu_id != MCU0)
        // {
        //     mcu_reboot();
        // }
            
    }
    
}

static mcu_fsm_table_s mcu_ota_handle_table[] = {
 /* data  condition  current_state              event                     next_state                  handler */
    {NULL,  NULL,  STATE_NORMAL,                OTA_EVENT_GOT_OTA_INFO,   STATE_OTA_WAIT_FRAME,       mcu_ota_handle_start          },
    {NULL,  NULL,  STATE_NORMAL,                OTA_EVENT_ERROR,          STATE_NORMAL,               mcu_ota_handle_error          },
    {NULL,  NULL,  STATE_OTA_WAIT_FRAME,        OTA_EVENT_GOT_FRAME_INFO, STATE_OTA_FRAME_PROCESSING, mcu_ota_handle_send_frame_req },
    {NULL,  NULL,  STATE_OTA_WAIT_FRAME,        OTA_EVENT_ERROR,          STATE_NORMAL,               mcu_ota_handle_error          },
    {NULL,  NULL,  STATE_OTA_FRAME_PROCESSING,  OTA_EVENT_ERROR,          STATE_NORMAL,               mcu_ota_handle_error          },
    {NULL,  NULL,  STATE_OTA_FRAME_PROCESSING,  OTA_EVENT_FRAME_PAYLOAD,  STATE_OTA_WAIT_FRAME,       mcu_ota_handle_payload        },
    {NULL,  NULL,  STATE_OTA_FRAME_PROCESSING,  OTA_EVENT_FRAME_NEXT,     STATE_OTA_WAIT_FRAME,       mcu_ota_handle_send_frame_req },
    {NULL,  NULL,  STATE_OTA_WAIT_FRAME,        OTA_EVENT_FRAME_FINISHED, STATE_OTA_UPDATE_CONFIG,    mcu_ota_handle_config         },
    {NULL,  NULL,  STATE_OTA_UPDATE_CONFIG,     OTA_EVENT_CONFIG_UPDATED, STATE_NORMAL,               mcu_ota_handle_reboot         },
    {NULL,  NULL,  STATE_OTA_UPDATE_CONFIG,     OTA_EVENT_ERROR,          STATE_NORMAL,               mcu_ota_handle_error          },
};

void mcu_init_ota_handler(write_t write, read_t read, reset_t reset) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    sc.flash_write = write;
    sc.flash_read = read;
    sc.flash_reset = reset;
    int count = sizeof(mcu_ota_handle_table)/sizeof(mcu_fsm_table_s);
    mcu_fsm_register(&(sc.fsm), mcu_ota_handle_table, count, STATE_NORMAL);
    sc.can_ota = mcu_check_app();
}

void mcu_ota_event_handler(void *param) {
    //printf("%s, %s, %d\n\r", __FILE__, __FUNCTION__, __LINE__); 
    uint8_t type = (uint8_t)((_event_ota_s*)param)->event_type;
    char * data = (char *)((_event_ota_s*)param)->data;
    //printf("mcu_ota_event_handler: type: %d, state:%d\n\r", type, sc.fsm.state);
    switch(sc.fsm.state) {
        case STATE_NORMAL: {
            if(type != OTA_EVENT_GOT_OTA_INFO)
                type = OTA_EVENT_ERROR;
            break;
        }
        case STATE_OTA_WAIT_FRAME: {
            if(type != OTA_EVENT_GOT_FRAME_INFO
                && type != OTA_EVENT_FRAME_FINISHED)
                type = OTA_EVENT_ERROR;
            break;
        }
        case STATE_OTA_FRAME_PROCESSING: {
            if(type != OTA_EVENT_FRAME_PAYLOAD
                && type != OTA_EVENT_FRAME_NEXT)
                type = OTA_EVENT_ERROR;
            break;
        }
        case STATE_OTA_UPDATE_CONFIG: {
            if(type != OTA_EVENT_CONFIG_UPDATED)
                type = OTA_EVENT_ERROR;
            break;
        }
        default: {
            type = OTA_EVENT_ERROR;
        }
    }
    
    if(type == OTA_EVENT_ERROR) {
        if (data == NULL) {
	        data = (char*)malloc(sizeof(char)*32);
//	        if (sc.fsm.state >= STATE_NORMAL && sc.fsm.state < STATE_OTA_MAX) {
//                strcpy(data, MCU_WORKING_STATE_str[(uint8_t)sc.fsm.state]);
//            }
//            else {
//                strcpy(data, "Other Errors");
//            }
	        strcpy(data, MCU_WORKING_STATE_str[(uint8_t)sc.fsm.state]);
            mcu_ota_handle_error(mcu_id, (void*)data);
            free(data);
        }
        else {
            mcu_ota_handle_error(mcu_id, (void*)data);
        }
        return;
    }
    sc.event_data = (mcu_comm_data_s*)((_event_ota_s*)param)->data;
    mcu_fsm_event_handler(MCU0, &(sc.fsm), type);
}

bool mcu_ota_get_start_flag(void) {
    return sc.ota_start_flag;
}


// force the MCU to reset if stuck into one OTA state other than normal too long
void mcu_ota_eval_fsm_state(void) {
    
    char command[MAX_COMMAND_STRING_LEN] = { 0 };

    if (sc.ota_start_flag == false)
    {
        state_enter_time = sysctl_get_time_us();
    }
    else if (sc.state != sc.fsm.state)
    {
        state_enter_time = sysctl_get_time_us();
        sc.state = sc.fsm.state;
    }

    current_time = sysctl_get_time_us();
    
    int32_t elapsed_time = current_time - state_enter_time;
    if ((sc.ota_start_flag) && (elapsed_time > OTA_STATE_TIMEOUT))
    {
	    //sc.fsm.state = STATE_NORMAL;
	    //sc.state = OTA_EVENT_ERROR;
	    // sc.data_count = 0;
	    // sc.ota_start_flag = false;
        mcu_stdio_printf("MCU[%d] F/W Upgrade Timeout @ %s\n\r", sc.info.target_mcu, MCU_WORKING_STATE_str[(uint8_t)sc.fsm.state]);

        sprintf(command, "{\"RSP\":\"OTA\", \"MCU\":%d, \"STATE\":\"Fault: %s @ %s\"}", mcu_id, "OTA FSM Timeout", MCU_WORKING_STATE_str[(uint8_t)sc.fsm.state]);
        send_string(mcu_uart_dev, command, head_data_JSON);

	    usleep(10000);
	    sysctl_reset(SYSCTL_RESET_SOC); 
    }
}

void read_bootloader_version_string(uint8_t * version)
{
    uint8_t bootloader_first_line[32];
    //printf("Read flash addr 0x1000\r\n");
    sc.flash_read(BOOTLOADER_LAUNCH_ADDRESS + SPI3_BASE_ADDR + ELF_SIZE_OFFSET, bootloader_first_line, 32);
    
    for (int i = 0; i < 10; i++)
    {
        version[i] = bootloader_first_line[i+15];
    }
    
}

void mcu0_handle_ota_response_from_slave(char * command, uint8_t index)
{
    
    if ((strcmp(command, COMMAND_OTA_RECEIVE_FINISHED) == 0) && index == MCU1)
    {
        slave_response_count++;
    }
    
    if ((strcmp(command, COMMAND_OTA_RECEIVE_FINISHED) == 0) && index == MCU2)
    {
        slave_response_count++;
    }
    
    
    if (slave_response_count == 2)
    {
        slave_response_count = 0;
        _event_ota_s data;
        data.event_type = OTA_EVENT_FRAME_FINISHED;
        data.data = NULL;
        mcu_ota_event_handler(&data);
    }

}