#ifndef __mcu_BOARD_MODULE_H__
#define __mcu_BOARD_MODULE_H__
#include "mcu_uart.h"
#include "mcu_hw_module.h"
#include "mcu_mic_array.h"
#include "mcu_stdio.h"
#include "mcu_flash.h"
#include "mcu_wdt.h"
#include "mcu_fpga.h"

typedef struct _mcu_board_module_ {
    _mcu_hw_module_t common;
} _mcu_board_module_t;

typedef struct _mcu_uart_device_ {
    _mcu_hw_device_t common;
    void (*init)(uint8_t uart_dev_index, recv_callback_func_t callback);
    int8_t (*send)(uint8_t uart_dev_index, const uint8_t *buffer, size_t buf_len);
    void (*loop)(uint8_t uart_dev_index);
} _mcu_uart_device_t;

typedef struct _mcu_mic_device_ {
    _mcu_hw_device_t common;
    uint8_t (*init)(mic_callback callback);
    void (*loop)(void);
} _mcu_mic_device_t;

typedef struct _mcu_flash_device_ {
    _mcu_hw_device_t common;
    uint8_t (*init)(void*);
    void (*write)(uint32_t address, uint8_t *data_buf, uint32_t length);
    void (*read)(uint32_t address, uint8_t *data_buf, uint32_t length);
    void (*reset)(void);
} _mcu_flash_device_t;

typedef struct _mcu_wdt_device_ {
    _mcu_hw_device_t common;
    void(*init)(wdt_device_number_t device_num, wdt_callback callback);
    void(*loop)(wdt_device_number_t device_num);
    // uint32_t (*init)(wdt_device_number_t id, uint64_t time_out_ms, plic_irq_callback_t on_irq, void *ctx);
} _mcu_wdt_device_t;

typedef struct _mcu_stdio_device_ {
    _mcu_hw_device_t common;
    void (*init)(void*);
    void (*loop)(void);
} _mcu_stdio_device_t;

typedef struct _mcu_fpga_device_ {
    _mcu_hw_device_t common;
    void (*init)(void*);
    void (*loop)(void);
} _mcu_fpga_device_t;

_mcu_hw_module_t* mcu_get_board_module();

#endif
