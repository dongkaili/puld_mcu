#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "mcu_board_module.h"

#define ID_MODULE_BOARD "board"

#define BOARD_MODULE_TAG 0xFFFF0001

#define UART_0_DEVICE_TAG   0x00FF0001
#define UART_1_DEVICE_TAG   0x00FF0002
#define UART_2_DEVICE_TAG   0x00FF0003
#define MIC_DEVICE_TAG      0x00FF0004
#define FLASH_DEVICE_TAG    0x00FF0005
#define WDT0_DEVICE_TAG     0x00FF0006
#define WDT1_DEVICE_TAG     0x00FF0007
#define STDIO_DEVICE_TAG    0x00FF0008
#define FPGA_DEVICE_TAG     0x00FF0009

#define UART_0_DEVICE_ID    "uart0"
#define UART_1_DEVICE_ID    "uart1"
#define UART_2_DEVICE_ID    "uart2"
#define MIC_DEVICE_ID "mic"
#define FLASH_DEVICE_ID "flash"
#define WDT0_DEVICE_ID "wdt0"
#define WDT1_DEVICE_ID "wdt1"
#define STDIO_DEVICE_ID "stdio"
#define FPGA_DEVICE_ID "fpga"


/* 
static int mcu_uart_device_close(_mcu_hw_device_t* device){
    _mcu_uart_device_t *dev = (_mcu_uart_device_t *)device;
    free(dev);
    return 0;
}
 */
#define DEVICE_CLOSE_FUNCTION(dev_type) \
    static int mcu_##dev_type##_device_close(_mcu_hw_device_t* device){ \
        _mcu_##dev_type##_device_t *dev = (_mcu_##dev_type##_device_t *)device; \
    free(dev); \
    return 0; \
}

/* 
        _mcu_uart_device_t *dev = malloc(sizeof(_mcu_uart_device_t));
        memset(dev,0, sizeof(_mcu_uart_device_t));
        dev->common.tag = UART_DEVICE_TAG;
        dev->common.id = UART_DEVICE_ID;
        dev->common.module = module;
        dev->common.close = mcu_uart_device_close;
 */
#define DEVICE_INIT_BLOCK(dev_type, const_name) \
        _mcu_##dev_type##_device_t *dev = malloc(sizeof(_mcu_##dev_type##_device_t)); \
        memset(dev,0, sizeof(_mcu_##dev_type##_device_t)); \
        dev->common.tag = const_name##_DEVICE_TAG; \
        dev->common.id = const_name##_DEVICE_ID; \
        dev->common.module = module; \
        dev->common.close = mcu_##dev_type##_device_close;

DEVICE_CLOSE_FUNCTION(uart)
#if !FACTORY_RESET
    DEVICE_CLOSE_FUNCTION(mic)
    DEVICE_CLOSE_FUNCTION(fpga)
#endif
DEVICE_CLOSE_FUNCTION(flash)
DEVICE_CLOSE_FUNCTION(wdt)
DEVICE_CLOSE_FUNCTION(stdio)
int mcu_board_module_open(const _mcu_hw_module_t* module, const char* id,
              _mcu_hw_device_t** device);

_mcu_hw_module_methods_t board_method = {
        .open =  mcu_board_module_open
};

_mcu_board_module_t BOARD_HAL_MODULE_INFO_SYM = {
        .common =  {
                .tag =  BOARD_MODULE_TAG,
                .id =  ID_MODULE_BOARD,
                .methods =  &board_method
        }
};

int mcu_board_module_open(const _mcu_hw_module_t* module, const char* id,
              _mcu_hw_device_t** device) {
    assert(id);
    if(0 == strcmp(id, UART_0_DEVICE_ID)) {
        DEVICE_INIT_BLOCK(uart, UART_0)
        dev->init = mcu_uart_init;
        dev->loop = mcu_uart_loop;
        dev->send = mcu_uart_send_data;
	    *device = (_mcu_hw_device_t*)dev;
    } else if (0 == strcmp(id, UART_1_DEVICE_ID)) {
        DEVICE_INIT_BLOCK(uart, UART_1) 
        dev->init = mcu_uart_init;
        dev->loop = mcu_uart_loop;
        dev->send = mcu_uart_send_data;
	    *device = (_mcu_hw_device_t*)dev;
    } else if (0 == strcmp(id, UART_2_DEVICE_ID)) {
        DEVICE_INIT_BLOCK(uart, UART_2) 
        dev->init = mcu_uart_init;
        dev->loop = mcu_uart_loop;
        dev->send = mcu_uart_send_data;
	    *device = (_mcu_hw_device_t*)dev;
#if !FACTORY_RESET
    } else if(0 == strcmp(id, MIC_DEVICE_ID)) {
        DEVICE_INIT_BLOCK(mic, MIC)
        dev->init = mic_array_init;
        dev->loop = mic_array_loop;
	    *device = (_mcu_hw_device_t*)dev;
#endif
    } else if(0 == strcmp(id, FLASH_DEVICE_ID)) {
        DEVICE_INIT_BLOCK(flash, FLASH)
        dev->init = mcu_flash_init;
        dev->write = mcu_flash_write;
        dev->read = mcu_flash_read;
        dev->reset = mcu_flash_prepare_reset;
	    *device = (_mcu_hw_device_t*)dev;
    } else if(0 == strcmp(id, WDT0_DEVICE_ID)) {
        DEVICE_INIT_BLOCK(wdt, WDT0)
        dev->init = mcu_wdt_init;
        dev->loop = mcu_wdt_loop;
	    *device = (_mcu_hw_device_t*)dev;
    } else if(0 == strcmp(id, WDT1_DEVICE_ID)) {
        DEVICE_INIT_BLOCK(wdt, WDT1)
        dev->init = mcu_wdt_init;
        dev->loop = mcu_wdt_loop;
	    *device = (_mcu_hw_device_t*)dev;
#if !FACTORY_RESET
    } else if(0 == strcmp(id, FPGA_DEVICE_ID)) {
        DEVICE_INIT_BLOCK(fpga, FPGA)
        dev->init = mcu_fpga_init;
        dev->loop = mcu_fpga_loop;
	    *device = (_mcu_hw_device_t*)dev;
#endif
    } else if(0 == strcmp(id, STDIO_DEVICE_ID)) {
        DEVICE_INIT_BLOCK(stdio, STDIO)
        dev->init = mcu_stdio_init;
        dev->loop = mcu_stdio_loop;
	    *device = (_mcu_hw_device_t*)dev;
    } else {
        printf("can't find: %s\n\r", id);
    }
	return 0;
}

_mcu_hw_module_t* mcu_get_board_module() {
    return &BOARD_HAL_MODULE_INFO_SYM.common;
}

