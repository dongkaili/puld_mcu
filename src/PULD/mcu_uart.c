#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <unistd.h>
#include "mcu_pins.h"
#include "fpioa.h"
#include "uart.h"
#include "sysctl.h"
#include "mcu_uart.h"
#include "mcu_util_queue.h"
#include "mcu_comm_protocol.h"
#include "mcu_ipc_sync.h"

#if defined USE_UART_DMA
#define RECV_DMA_LENTH  1
#endif

typedef struct {
    uint8_t *p_data;
    uint32_t size;
} mcu_uart_data_s;


typedef struct _mcu_uart_context_ {
    recv_callback_func_t callback;
    volatile uint32_t received_flag;
    volatile uint32_t send_flag;
    uint8_t *recv_buf;
    volatile int64_t recv_count;

    volatile int32_t cur_data_buf_len;
    uint8_t *cur_data_buf;

    uint8_t recv_ring_buf[RING_BUFFER_LEN];
    uint8_t index;
#if defined USE_QUEUE_BUFFER
    _mcu_util_queue_s data_queue_set;
#endif
} _mcu_uart_context_s;

static _mcu_uart_context_s s_context[UART_DEVICE_MAX];


#if defined USE_UART_DMA
    static uint32_t ring_buf_index[UART_DEVICE_MAX] = { 0, 0, 0 };
#endif

const int pin_mcu_ser_tx[3][3] = {
  { PIN_MCU_SER_TX, PIN_MCU_SER_TX01, PIN_MCU_SER_TX02 }, // MCU0
  {            -1 , PIN_MCU_SER_TX10,               -1 }, // MCU1
  {            -1 ,               -1, PIN_MCU_SER_TX20 }  // MCU2
};
const int pin_mcu_ser_rx[3][3] = {
  { PIN_MCU_SER_RX, PIN_MCU_SER_RX01, PIN_MCU_SER_RX02 }, // MCU0
  {             -1, PIN_MCU_SER_RX10,               -1 }, // MCU1
  {             -1,               -1, PIN_MCU_SER_RX20 }  // MCU2
};

static uint32_t uart_nominal_baud_rate[UART_DEVICE_MAX] = {HOST_MCU_BAUD_RATE, MCU_MCU_BAUD_RATE, MCU_MCU_BAUD_RATE};

static void mcu_uart_io_init(uart_device_number_t device_num)
{
#ifdef UART_OFFSET
    uart_device_number_t device_num_new = ((device_num + UART_OFFSET) % 3);
#else
    uart_device_number_t device_num_new = device_num;
#endif
    fpioa_set_function(pin_mcu_ser_rx[mcu_id][device_num], FUNC_UART1_RX + device_num_new  * 2);    // uart dev step = 2
    fpioa_set_function(pin_mcu_ser_tx[mcu_id][device_num], FUNC_UART1_TX + device_num_new * 2);     // mcu step = 3
}

volatile uint32_t send_done_count;

static int uart_send_done(void *ctx)
{
    uint8_t index = *(uint8_t *)ctx;
    send_done_count += 1;
    s_context[index].send_flag = 1;
    return 0;
}

#if defined USE_QUEUE_BUFFER
static void mcu_release_uart_data_buffer(void *elem)
{
    uint8_t* p = (*(uint8_t **)elem);
    //printf("free address: 0x%08lx  ]\n\r", (uint64_t)p);
	free(p);
}
#endif

static int uart_recv_done(void *ctx)
{
    uint8_t index = *(uint8_t *)ctx;
    // return 0;
#if defined USE_QUEUE_BUFFER
    uint8_t v_buf[10] = {0};
    int ret =  uart_receive_data(index, v_buf, 8);
    //printf("%s, ret : %d\r\n", __FUNCTION__, ret);
    //malloc buffer
    mcu_uart_data_s tdata = {
        .size = ret,
        .p_data = malloc(ret*sizeof(uint8_t))
    };
    memcpy(tdata.p_data, v_buf, ret*sizeof(uint8_t));
    mcu_util_queue_enque(&(s_context[index].data_queue_set), &(tdata));
    s_context[index].recv_count += ret;
    //printf("USE_QUEUE_BUFFER\r\n");
#else
    s_context[index].received_flag = 0;
    //uint8_t v_buf[10] = {0};
    //int ret =  uart_receive_data(index, v_buf, 8);
    uint8_t v_buf[64] = {0};
    uint8_t v_char = 0;
    int ret = 0;
    while(uart_receive_data(index, (char *)&v_char, 1)) {
        v_buf[ret++] = v_char;
    }
    
    // printf("ret : %d\r\n", ret);
    // printf("%s#\r\n", v_buf);
    size_t shift_len  = 0; // SEH: array?
    uint8_t *v_dest = s_context[index].recv_buf + ret;
    if(v_dest >= s_context[index].recv_ring_buf + RING_BUFFER_LEN) {
        shift_len = (v_dest - s_context[index].recv_ring_buf - RING_BUFFER_LEN);
        v_dest = s_context[index].recv_ring_buf + shift_len;
        if((v_dest - s_context[index].cur_data_buf) >= 0) {
            printf("\r\nWARNING: UART #%d ring buffer full #1\r\n", index);   // no recovery from this... reset device?
            return -1;
        }
    }
//    else if((v_dest - s_context[index].cur_data_buf) <= 0) {
//        printf("\r\nWARNING: UART #%d ring buffer full #2\r\n", index);
//        return -2;
//    }

    //LOG("data_receive buf_len: %d\r\n", ret);
    //LOG("data_receive shift_len: %ld\r\n", shift_len);
    //LOG("Here 1\n\r"); // LOG's here all fail 
    memcpy(s_context[index].recv_buf, v_buf, ret - shift_len);
    if(shift_len > 0)
        memcpy(s_context[index].recv_ring_buf, v_buf + (ret - shift_len), shift_len);
    s_context[index].recv_buf = v_dest;
    s_context[index].recv_count += ret;
    s_context[index].received_flag = 1;
    // LOG("USE_UART_else\r\n"); // breaks JSON cmd handler
#endif    
    {
        //LOG("count: %ld\r\n", s_context[index].recv_count);  // seems to be fine if only this enabled within this block but if this isn't enabled it breaks!
        //mcu_stdio_printf("%s,%s,%d\r\n", __FILE__,__FUNCTION__,__LINE__); // breaks!
        //mcu_stdio_printf("[    ]"); // breaks
        //mcu_stdio_printf("[   ]"); // works for 1 @ HEN:0:ENA and 1 @ HEN:0:DIS but then JSON cmd hander breaks
    }
    return 0;
}



void mcu_uart_init(uint8_t index, recv_callback_func_t callback) {
    s_context[index].callback = callback;
    s_context[index].recv_count = 0;
    s_context[index].received_flag = 0;
    s_context[index].send_flag = 1;
    s_context[index].cur_data_buf = s_context[index].recv_ring_buf;
    s_context[index].recv_buf = s_context[index].recv_ring_buf;
    s_context[index].cur_data_buf_len = 0;
    s_context[index].index = index;
#if defined USE_QUEUE_BUFFER
    mcu_util_queue_init(&(s_context[index].data_queue_set), 10, sizeof(mcu_uart_data_s), mcu_release_uart_data_buffer);
#endif
    mcu_uart_io_init(index);
    
    uart_init(index);
    
    uart_configure(index, index == MCU0 ? HOST_MCU_BAUD_RATE : MCU_MCU_BAUD_RATE, UART_BITWIDTH_8BIT, UART_STOP_1, UART_PARITY_NONE);
    
    uart_set_work_mode(index, UART_NORMAL);

#if !defined USE_UART_DMA
    
    uart_irq_register(index, UART_SEND, uart_send_done, (void*)&(s_context[index].index), 1);
#endif

    
    uart_irq_register(index, UART_RECEIVE, uart_recv_done, (void*)&(s_context[index].index), 2);
}

int8_t mcu_uart_send_data(uint8_t index, const uint8_t *buffer, size_t buf_len) 
{
#if 0
    uart_send_data(index, buffer, buf_len);
#else
    if(1 == s_context[index].send_flag) {
        s_context[index].send_flag = 0;
#if defined USE_UART_DMA
        dmac_channel_number_t dmac_channel = index;
        uart_send_data_dma_irq(index, dmac_channel, buffer, buf_len, uart_send_done, &(s_context[index].index), 2);
#else
        uart_send_data(index, (char *)buffer, buf_len);
#endif
        return 0;
    }
    return - 1;
#endif
}

void mcu_uart_loop(uint8_t index) {
#if defined USE_QUEUE_BUFFER
    if (0 == mcu_util_queue_is_empty(&(s_context[index].data_queue_set))) {
        mcu_uart_data_s tdata;
        mcu_util_queue_deque(&(s_context[index].data_queue_set), &tdata);
        s_context[index].callback(index, tdata.p_data, tdata.size);

        // uint8_t str1[1024] = {0};
        // strncpy(str1, tdata.p_data, tdata.size);
        // printf("queue value: %s\r\n", str1);
        printf("[0]:%02x,len: %ud, count: %ld\r\n", tdata.p_data[0], tdata.size, s_context[index].recv_count);
        free(tdata.p_data);
    }
#else
    if(s_context[index].recv_buf == s_context[index].cur_data_buf) {
        // printf("empty buffer\r\n");
        return;
    }
    // corelock_lock(&_init_lock); // SEH: TBD confirm unneeded TBD
    //LOG("count: %ld\r\n", s_context[index].recv_count);
    int32_t shift_len = 0;
    const uint8_t *buf = s_context[index].cur_data_buf;
    s_context[index].cur_data_buf_len = (int32_t)(s_context[index].recv_buf - s_context[index].cur_data_buf);
    //LOG("s_context.cur_data_buf_len: %d\r\n", s_context[index].cur_data_buf_len);
    if(s_context[index].cur_data_buf_len < 0) {
        shift_len = (s_context[index].recv_buf - s_context[index].recv_ring_buf);
        s_context[index].cur_data_buf_len = RING_BUFFER_LEN + shift_len + s_context[index].recv_ring_buf - s_context[index].cur_data_buf;
    }
    const uint16_t len = s_context[index].cur_data_buf_len;
    s_context[index].cur_data_buf = s_context[index].recv_buf;
    s_context[index].cur_data_buf_len = 0;
    //LOG("[0]:%02x,len: %d, shift_len: %d, count: %ld\r\n", buf[0], len, shift_len, s_context[index].recv_count);
    s_context[index].callback(index, buf, len, shift_len);
    // corelock_unlock(&_init_lock); // SEH: COnfirm uneeded TBD
#endif
}


//
uint32_t mcu_uart_get_actual_set_baud_rate(uint8_t uart_index)
{
    uint32_t baud_rate = 0;
    uint32_t freq = sysctl_clock_get_freq(SYSCTL_CLOCK_APB0);
    uint32_t divisor = freq / uart_nominal_baud_rate[uart_index];    
    baud_rate = freq / divisor;
    //printf("baudrate_sp: %d, baud_rate: %d, divisor: %d, freq: %d ", baud_rate_sp, baud_rate, divisor, freq);
    return baud_rate;
}

uint32_t mcu_uart_get_nominal_baud_rate(uint8_t uart_index)
{
    return uart_nominal_baud_rate[uart_index];
}

void mcu_uart_set_nominal_baud_rate(uint8_t uart_index, uint32_t baud_rate_sp)
{
    uart_nominal_baud_rate[uart_index] = baud_rate_sp;
}


// change the baud rate for UART, uart_index = 0,1,2
void mcu_uart_change_baud_rate(uint8_t uart_index, uint32_t baud_rate_sp)
{
    uart_configure(uart_index, baud_rate_sp, 8, UART_STOP_1, UART_PARITY_NONE);
}

#ifdef OTA_TEST
void ota_test_data_receive(uint8_t* v_buf, size_t buf_len) {
    s_context[mcu_uart_dev].received_flag = 0;
    size_t ret =  buf_len;
    // printf("buf_len:%ld\r\n", buf_len);
    size_t shift_len  = 0;
    uint8_t *v_dest = s_context[mcu_uart_dev].recv_buf + ret;
    if (v_dest >= s_context[mcu_uart_dev].recv_ring_buf + RING_BUFFER_LEN) {
        shift_len = (v_dest - s_context[mcu_uart_dev].recv_ring_buf - RING_BUFFER_LEN);
        v_dest = s_context[mcu_uart_dev].recv_ring_buf + shift_len;
		if ((v_dest - s_context[mcu_uart_dev].cur_data_buf) > 0) {
    		printf("\r\nWARNING: UART #%d ring buffer full #3\r\n", mcu_uart_dev);
            return;
        }
    }
    else if((v_dest - s_context[mcu_uart_dev].cur_data_buf) < 0) {
        printf("\r\nWARNING: UART #%d ring buffer full #4\r\n", mcu_uart_dev);
        return;
    }

    // if(v_dest >= s_context[mcu_id].recv_ring_buf + RING_BUFFER_LEN) {
    //     shift_len = (v_dest - s_context[mcu_id].recv_ring_buf - RING_BUFFER_LEN);
    //     v_dest = s_context[mcu_id].recv_ring_buf + shift_len;
    // }
    LOG1("ota_test_data_receive buf_len: %ld\r\n", buf_len);
    LOG1("ota_test_data_receive shift_len: %ld\r\n", shift_len);
    memcpy(s_context[mcu_uart_dev].recv_buf, v_buf, ret - shift_len);
    if(shift_len > 0)
        memcpy(s_context[mcu_uart_dev].recv_ring_buf, v_buf + (ret - shift_len), shift_len);
    s_context[mcu_uart_dev].recv_buf = v_dest;
    s_context[mcu_uart_dev].recv_count += ret;
    s_context[mcu_uart_dev].received_flag = 1;
}
#endif
