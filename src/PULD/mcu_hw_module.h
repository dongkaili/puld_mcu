#ifndef __mcu_HW_MODULE_H__
#define __mcu_HW_MODULE_H__

struct _mcu_hw_module_t;
struct _mcu_hw_module_methods_t;
struct _mcu_hw_device_t;

typedef struct _mcu_hw_module_t {
    uint32_t tag;
    const char *id;
    struct _mcu_hw_module_methods_t* methods;
    // uint8_t fpioa_count;
    // _mcu_hw_fpioa_parameter_s *p_set;
    // uint8_t (*init)(void *param);
    // void (*close)()
} _mcu_hw_module_t;

typedef struct _mcu_hw_device_t {
    uint32_t tag;
    const char *id;
    const _mcu_hw_module_t* module;
    int (*close)(struct _mcu_hw_device_t* device);
} _mcu_hw_device_t;

typedef struct _mcu_hw_module_methods_t {
    /** Open a specific device */
    int (*open)(const _mcu_hw_module_t* module, const char* id,
            _mcu_hw_device_t** device);
} _mcu_hw_module_methods_t;

int mcu_hw_get_module(const char *id, const _mcu_hw_module_t **module);

void mcu_hw_regist(_mcu_hw_module_t *module);

#endif