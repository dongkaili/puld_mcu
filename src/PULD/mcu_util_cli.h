#ifndef __mcu_UTIL_CLI_H__
#define __mcu_UTIL_CLI_H__

#include "mcu_common.h"
#include "mcu_util_queue.h"

typedef void (*mcu_command_entry_func_t)(void *param);

typedef struct {
    uint16_t command_index;
    char*  command_name;
    mcu_command_entry_func_t command_callback;
} mcu_command_s;

typedef struct {
    mcu_command_entry_func_t callback;
    char  command[MAX_COMMAND_STRING_LEN];
} mcu_command_data_s;

uint8_t mcu_util_cli_invoker(char* command_string, mcu_command_s* table, uint32_t table_size, _mcu_util_queue_s* commands_stack);
uint8_t mcu_util_cli_receiver(_mcu_util_queue_s* commands_stack);

#endif
