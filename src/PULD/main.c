#include <stdio.h>
#include "wdt.h"
#include "bsp.h"
#include "mcu_internal_fsm.h"
#include "mcu_uart.h"
#include "mcu_comm_protocol.h"
#include "mcu_stdio.h"
#include "mcu_flash.h"
#include "mcu_mic_array.h"
#include "sysctl.h"
#include "mcu_common_define.h"
#include "mcu_device_management.h"
#include "mcu_json_commands.h"
#include "mcu_util_queue.h"
#include "gpio.h"
#include "gpiohs.h"
#include "fpioa.h"
#include "timer.h"
#include "mcu_pins.h"
#include "mcu_ipc_sync.h"
#include "spi_master.h"
#include "spi_slave.h"
#include "clint.h"



// an array to contain slave data address info
uint8_t slave_cfg[32];
// data struct on slave MCUs
slave_mcu_data_s slave_data_tmp;

extern uint8_t mcu1_bf_data_ready_flag;
extern uint8_t mcu2_bf_data_ready_flag;

corelock_t _init_lock;



int core1_function(void *ctx)
{
    corelock_lock(&_init_lock);
    uint64_t core = current_coreid();	
    mcu_stdio_printf("\n\rCore #1 [%ld] handles beamformer post-processing.\n\r\n\r", core);
    mcu_devices_core1_init();
    report_mcu_status(mcu_id); // unable to put this in mcu_device_management() with similar functions... report_mcu_id() & report_firmware_version()
    corelock_unlock(&_init_lock);

    while(1) {
        if (Enable_LPM == 1)
        {
            uint8_t v_char = 0;
            if (uart_receive_data(mcu_id, (char *)&v_char, 1)) {
                mcu_reboot();
            }
        }
        else
            mcu_devices_core1_loop();
    }
}


int main()	
{
    corelock_lock(&_init_lock);
    mcu_device_bsp_init();

#if INCLUDE_STDIO_REMAP
    switch (mcu_id)
    { // able to set default state for each MCU for targetted debugging
        case 0: Enable_Remap_STDIO = ACTIVATE_STDIO_REMAP_MCU0; break;
        case 1: Enable_Remap_STDIO = ACTIVATE_STDIO_REMAP_MCU1; break;
        case 2: Enable_Remap_STDIO = ACTIVATE_STDIO_REMAP_MCU2; break;
        default: Enable_Remap_STDIO = false;
    }
    // Configure TCK pin as input so it can be checked. 
    //If it goes high then JTAG is starting so we have to release from STDIO debug.
    fpioa_set_function(PIN_MCU_JTAG_TCK, FUNC_GPIOHS0); 
    gpiohs_set_drive_mode(GPIOHS_0_TCK_TEST, GPIO_DM_INPUT_PULL_DOWN);
#endif
    mcu_stdio_init(NULL);  // bring up debug interfaces 1st
    
    printf("\n\rStarting ");
#ifdef DEBUG
    printf("DEBUG");
#endif
#ifdef NDEBUG
    printf("RELEASE");
#endif    
    printf(" Version\n\r");
    
    set_firmware_version_string(); // one-time build of F/W version string
    printf("F/W Version: %s\n\r", get_firmware_version_string());

    printf("Last restart was due to %s reset\n\r", get_last_reset_reason());
    
    fpioa_set_function(PIN_MCU1_EN, FUNC_GPIO2);
    fpioa_set_function(PIN_MCU2_EN, FUNC_GPIO3);

    fpioa_set_function(PIN_MCU_ACPRn,      FUNC_GPIO4);
    fpioa_set_function(PIN_MCU_BAT_FAULTn, FUNC_GPIO5);
    fpioa_set_function(PIN_MCU_CHRGn,      FUNC_GPIO6);
    fpioa_set_function(PIN_MCU_CHARGE_EN,  FUNC_GPIO7);
    
	gpio_init();
	
    // Set GPIO mode for MCU enable pins
    gpio_set_drive_mode(GPIO_2_MCU1_EN, GPIO_DM_OUTPUT);    
    gpio_set_drive_mode(GPIO_3_MCU2_EN, GPIO_DM_OUTPUT);

    // Set charger pins
    gpio_set_drive_mode(GPIO_4_ACPRn,      GPIO_DM_INPUT);
    gpio_set_drive_mode(GPIO_5_BAT_FAULTn, GPIO_DM_INPUT);
    gpio_set_drive_mode(GPIO_6_CHRGn,      GPIO_DM_INPUT);
    gpio_set_drive_mode(GPIO_7_CHARGE_EN,  GPIO_DM_OUTPUT);    

    // Set initial GPIO levels for charge_en
    gpio_set_pin(GPIO_7_CHARGE_EN, (gpio_pin_value_t)Enable_Charger);
    
    // and slave enables... Must be initialized or we will get a restart later when trying to enable... Must be in odd state on start without driving.
    gpio_set_pin(GPIO_2_MCU1_EN, GPIO_PV_LOW); // ensure slaves restart with master... maybe make this definable
    gpio_set_pin(GPIO_3_MCU2_EN, GPIO_PV_LOW);
    msleep(10);
    gpio_set_pin(GPIO_2_MCU1_EN, (gpio_pin_value_t)Enable_MCU1);
    gpio_set_pin(GPIO_3_MCU2_EN, (gpio_pin_value_t)Enable_MCU2);

    printf("\r\nMCU[%1d] uses UART[%1d]\r\n", mcu_id, mcu_uart_dev);
#ifdef USE_UART_DMA
    printf("UART transmitters are in DMA Mode\r\n");
#elif defined USE_QUEUE_BUFFER
    printf("UART transmitters are in Queue Buffer Mode\r\n");
#else    
    printf("UART transmitters are in Normal Mode\r\n");
#endif        
    uint64_t core = current_coreid();	
    printf("\n\rCore #0 [%ld] handles mic array processing.\n\r\n\r", core);
    // register_core1(core1_function, NULL);
    // Cores have to be initialized sequentially due to conflict with the wdt init macro's.
    // It doesn't matter where thery are intialiazed so do both here
    
    mcu_devices_core0_init();
#if !FACTORY_RESET    
    // initialize SPI master/slave based on MCU number
    if (mcu_id == MCU0)
    {
        //uint8_t slave_data_length = SLAVE_DATA_LENGTH;
        printf("spi master init\r\n");
        spi_master_init();
        //printf("slave data length: %d\r\n", slave_data_length);
    }
    else if (mcu_id == MCU1 || mcu_id == MCU2)
    {
        slave_data_tmp.slave_id = mcu_id;        
        
	    //printf("slave_data_tmp addr: 0x%x, slave_data addr: 0x%x\r\n", (uint32_t)&slave_data_tmp, (uint32_t)&slave_data_tmp.slave_data);
        printf("\r\nSPI slave init\r\n");
        *(uint32_t *)(&slave_cfg[8]) = (uint32_t)&slave_data_tmp;
        printf("slave_cfg array: ");
        for (int i = 0; i < 32; i++)
        {
            printf("0x%x,", slave_cfg[i]);
        }
        printf("\r\n");
        spi_slave_init(slave_cfg, 32, mcu_id);
    }
#endif    
    // if (MCU0 == mcu_id)
    // {
    //     // init inter processor interrupt (ipi) on core 0 of MCU0
    //     if (clint_ipi_init() != 0)
    //     {
    //         printf("Failed to init IPI on core 0\r\n");
    //     }
    //     else if (clint_ipi_enable() != 0)
    //     {
    //         printf("Failed to enable IPI on core 0\r\n"); // enable ipi on core 0
    //     }
    //     clint_ipi_register(core_ipi_callback_func, NULL);
    // }

    register_core1(core1_function, NULL);
    
    
    
    corelock_unlock(&_init_lock);
    
    while(1) {
        mcu_devices_core0_loop();
    }
}
