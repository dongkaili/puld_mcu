#include <stdio.h>
#include <stdlib.h>
#include "mcu_pins.h"
#include "mcu_adc.h"
#include "fpioa.h"
#include "spi.h"
#include "sysctl.h"
#include "gpiohs.h"
#include "mcu_common_define.h"

typedef enum {
  ADC_VOLTAGE,
  ADC_CURRENT,
} ADC_TYPES;


#define SPI_MASTER_VOLTAGE_CS_IO       GPIOHS_6_VOLTAGE_SS
#define SPI_MASTER_CURRENT_CS_IO       GPIOHS_7_CURRENT_SS

#define SPI_MASTER_VOLTAGE_CS_LOW()    gpiohs_set_pin(SPI_MASTER_VOLTAGE_CS_IO, GPIO_PV_LOW)
#define SPI_MASTER_VOLTAGE_CS_HIGH()   gpiohs_set_pin(SPI_MASTER_VOLTAGE_CS_IO, GPIO_PV_HIGH)

#define SPI_MASTER_CURRENT_CS_LOW()    gpiohs_set_pin(SPI_MASTER_CURRENT_CS_IO, GPIO_PV_LOW)
#define SPI_MASTER_CURRENT_CS_HIGH()   gpiohs_set_pin(SPI_MASTER_CURRENT_CS_IO, GPIO_PV_HIGH)


static void mcu_adc_read_voltage(uint8_t * voltage);
static void mcu_adc_read_current(uint8_t * current);
static float get_ADC(ADC_TYPES which, float full_scale_value);




void mcu_adc_init( void )
{
    fpioa_set_function(PIN_MCU_VOLTAGE_SS, FUNC_GPIOHS0 + SPI_MASTER_VOLTAGE_CS_IO);  // voltage_ss
    fpioa_set_function(PIN_MCU_CURRENT_SS, FUNC_GPIOHS0 + SPI_MASTER_CURRENT_CS_IO);  // current_ss    
    
    fpioa_set_function(PIN_MCU_ADC_CLK, FUNC_SPI1_SCLK);  // ADC_CLK 
    fpioa_set_function(PIN_MCU_ADC_MISO, FUNC_SPI1_D1);   // ADC_MISO

    spi_init(SPI_DEVICE_1, SPI_WORK_MODE_3, SPI_FF_STANDARD, 16, 0);
    spi_set_clk_rate(SPI_DEVICE_1, 5000000);

    SPI_MASTER_VOLTAGE_CS_HIGH();
    gpiohs_set_drive_mode(SPI_MASTER_VOLTAGE_CS_IO, GPIO_DM_OUTPUT);

    SPI_MASTER_CURRENT_CS_HIGH();
    gpiohs_set_drive_mode(SPI_MASTER_CURRENT_CS_IO, GPIO_DM_OUTPUT);

}

static void mcu_adc_read_voltage(uint8_t * voltage)
{
    mcu_adc_init();
    SPI_MASTER_VOLTAGE_CS_LOW();
    spi_receive_data_standard(SPI_DEVICE_1, 0, NULL, 0, voltage, 2);
    SPI_MASTER_VOLTAGE_CS_HIGH();
}
static void mcu_adc_read_current(uint8_t * current)
{
    mcu_adc_init();
    SPI_MASTER_CURRENT_CS_LOW();
    spi_receive_data_standard(SPI_DEVICE_1, 0, NULL, 0, current, 2);
    SPI_MASTER_CURRENT_CS_HIGH();
}


static float get_ADC(ADC_TYPES which, float full_scale_value)
{
    float result;
    uint8_t rec_data[2] = {0};
    if (which == ADC_VOLTAGE)
        mcu_adc_read_voltage(rec_data);
    else
        mcu_adc_read_current(rec_data);
    
    uint32_t data = 0;
    data = rec_data[1];
    data = (data << 8) + rec_data[0];
    result = (float)(data >> 1) / 0x1000 * full_scale_value; // 16th bit is discarded... see fig 4 of DS
    return result;
}

float get_battery_voltage(float scale_val)
{
    float temp, fValue;
    fValue = 0;
    for (int i = 0; i < 50; i++) 
    {
        temp = get_ADC(ADC_VOLTAGE, scale_val);
        //printf("Voltage = %5.1f\n\r", temp);
        fValue += temp;
    }
    fValue /= 50;
    return fValue;
    }

float get_battery_current(float scale_val)
{
    float temp, fValue;
    fValue = 0;
    for (int i = 0; i < 100; i++) 
    {
        temp = get_ADC(ADC_CURRENT, scale_val);
        //printf("Current = %5.1f\r\n", temp);
        fValue += temp;
    }
    fValue /= 100;
    return fValue;
}