# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 12:01:57 2020

@author: Bruce Wu
"""
import numpy as np

import matplotlib.pyplot as plt

#from scipy.fft import fft

# number of sample points
N = 512

# sample spacing
T = 1.0 / 512.0

x = np.linspace(0.0, N*T, N)

samplingFrequency   = 253906

samplingInterval    = 1 / samplingFrequency

# Begin time period of the signals

beginTime           = 0
endTime             = 10

# Time points

time        = np.arange(beginTime, endTime, samplingInterval)
 

mic_0 = []
mic_1 = []
mic_2 = []
mic_3 = []
mic_4 = []
mic_5 = []
mic_6 = []
mic_7 = []


# Open the file with read only permit
f = open('i2s_data_log.txt')
# use readline() to read the first line 
line = f.readline()
# use the read line to read further.
# If the file is not empty keep reading one line
# at a time, till the file is empty
while line:
    # in python 2+
    # print line
    # in python 3 print is a builtin function, so
    if (line.startswith('3')):
        a = line.replace('\t', ' ')
        b = a.split()
        mic_0.append(int(b[2]))
        mic_1.append(int(b[3]))
        mic_2.append(int(b[4]))
        mic_3.append(int(b[5]))
        mic_4.append(int(b[6]))
        mic_5.append(int(b[7]))
        mic_6.append(int(b[8]))
        mic_7.append(int(b[9]))
        
        #print(b[2:])
    # use realine() to read next line
    line = f.readline()

    
f.close()


#print(mic_0)
    
#x_fft = np.linspace(0.0, 1.0/(2.0*T), N//2)
mic_0_fft = np.fft.fft(mic_0) / len(mic_0)
mic_1_fft = np.fft.fft(mic_1) / len(mic_1)
mic_2_fft = np.fft.fft(mic_2) / len(mic_2)
mic_3_fft = np.fft.fft(mic_3) / len(mic_3)
mic_4_fft = np.fft.fft(mic_4) / len(mic_4)
mic_5_fft = np.fft.fft(mic_5) / len(mic_5)
mic_6_fft = np.fft.fft(mic_6) / len(mic_6)
mic_7_fft = np.fft.fft(mic_7) / len(mic_7)  

fft_mic_0 = mic_0_fft[range(int(len(mic_0)/2))]
fft_mic_1 = mic_1_fft[range(int(len(mic_1)/2))]
fft_mic_2 = mic_2_fft[range(int(len(mic_2)/2))]
fft_mic_3 = mic_3_fft[range(int(len(mic_3)/2))]
fft_mic_4 = mic_4_fft[range(int(len(mic_4)/2))]
fft_mic_5 = mic_5_fft[range(int(len(mic_5)/2))]
fft_mic_6 = mic_6_fft[range(int(len(mic_6)/2))]
fft_mic_7 = mic_7_fft[range(int(len(mic_7)/2))]

tpCount     = len(mic_0)

values      = np.arange(int(tpCount/2))

timePeriod  = tpCount/samplingFrequency

frequencies = values/timePeriod
 
print('Plot FFT results for mic 0 to 7')

figure, axis = plt.subplots(2, 1)
plt.subplots_adjust(hspace=1)
axis[0].set_title('Raw I2S data')
axis[0].plot(x, mic_0)
axis[0].plot(x, mic_1)
axis[0].plot(x, mic_2)
axis[0].plot(x, mic_3)
axis[0].plot(x, mic_4)
axis[0].plot(x, mic_5)
axis[0].plot(x, mic_6)
axis[0].plot(x, mic_7)

axis[1].set_title('FFT result for Raw I2S data')
axis[1].plot(frequencies, np.abs(fft_mic_0))
axis[1].plot(frequencies, np.abs(fft_mic_1))
axis[1].plot(frequencies, np.abs(fft_mic_2))
axis[1].plot(frequencies, np.abs(fft_mic_3))
axis[1].plot(frequencies, np.abs(fft_mic_4))
axis[1].plot(frequencies, np.abs(fft_mic_5))
axis[1].plot(frequencies, np.abs(fft_mic_6))
axis[1].plot(frequencies, np.abs(fft_mic_7))

plt.show()

#plt.plot(x_fft, 2.0/N * np.abs(mic_0_fft[0:N//2]))
#plt.plot(x_fft, 2.0/N * np.abs(mic_1_fft[0:N//2]))
#plt.plot(x_fft, 2.0/N * np.abs(mic_2_fft[0:N//2]))
#plt.plot(x_fft, 2.0/N * np.abs(mic_3_fft[0:N//2]))
#plt.plot(x_fft, 2.0/N * np.abs(mic_4_fft[0:N//2]))
#plt.plot(x_fft, 2.0/N * np.abs(mic_5_fft[0:N//2]))
#plt.plot(x_fft, 2.0/N * np.abs(mic_6_fft[0:N//2]))
#plt.plot(x_fft, 2.0/N * np.abs(mic_7_fft[0:N//2]))