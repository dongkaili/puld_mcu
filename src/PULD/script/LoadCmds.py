from multiprocessing.connection import Client
import time

# https://stackoverflow.com/questions/20309456/call-a-function-from-another-file
# https://docs.python.org/3/library/multiprocessing.html
# https://www.programcreek.com/python/example/55597/multiprocessing.connection.Client

def send(mssg):
    err_count = 0
    while True:
        try:
            conn = Client(('localhost', 6000), authkey=b'secret password')
            try:
                conn.send(mssg)
                if conn.poll(.5):
                    rsp = conn.recv()
                else:
                    rsp = '##NO_RSP##'
            except:
                return '##NO_RSP##'
            conn.close()
            return rsp
        except:
            err_count += 1
            if (err_count==5):
                print("Connection attempt failed 5 times")
                return '##CONN_ERR##'