# Code to show two plots of simulated streamed data. Data for each plot is processed (generated) 
# by separate threads, and passed back to the gui thread for plotting.
# This is an example of using movetothread, which is the correct way of using QThreads

# Michael Hogg, 2015

import time, sys, math, json

from PyQt5.QtCore import Qt
from numpy.lib.type_check import imag
from pyqtgraph.Qt import QtGui, QtCore
from PyQt5.Qt import QMutex
import pyqtgraph as pg
from PyQt5.QtWidgets import (
    QApplication,
    QLabel,
    QLineEdit,
    QComboBox,
    QCheckBox,
    QMainWindow,
    QPushButton,
    QRadioButton,
    QVBoxLayout,
    QGridLayout,
    QWidget,
)
from random import randint
from copy import copy
#from pyqtgraph.metaarray.MetaArray import axis

import serial
from serial.tools.list_ports import comports
from serial.tools import hexlify_codec

import numpy as np
from scipy.fft import rfft, rfftfreq







class SerialTransmitter(QtCore.QObject):
    
    def __init__(self, serial_instance, parent=None):
        super().__init__(parent)
        self.serial_instance = serial_instance
        
    def send_command_string(self, letter, command_type):
        """Write Micro Jason string to sys.stderr"""
        """
        HH(0x11), VV(Version Byte), TH(0x00: Raw, 0x01:Json), TL(0x00~0x04), Length(4 bytes, LSB first?), checksum, Reserve1, R2, R3, <data>
        
        """
        header    = 0x11
        #print(header.to_bytes(1, 'big'))
        version   = 0x02
        if command_type == 'JSON_Type':
            type_high = 0x01
            type_low  = 0x01
        elif command_type == 'LLC_Type':
            type_high = 0x02
            type_low  = 0x05
        elif command_type == 'Data_Type':
            type_high = 0x00
            type_low  = 0x06
        elif command_type == 'I2S_Raw_Type':
            type_high = 0x00
            type_low  = 0x07
        elif command_type == 'I2S_FFT_Type':
            type_high = 0x00
            type_low =  0x04
        elif command_type == 'RAW_FFT_Type':
            type_high = 0x00
            type_low =  0x08
        elif command_type == 'DIR_RAW_Type':
            type_high = 0x00
            type_low =  0x09
        elif command_type == 'VOC_RAW_Type':
            type_high = 0x00
            type_low =  0x10        
        elif command_type == None:
            if 'i2s_raw' in letter:
                type_high = 0x00
                type_low  = 0x07
            elif 'i2s_soft_fft' in letter or 'i2s_hard_fft' in letter:
                type_high = 0x00
                type_low  = 0x04
            elif 'i2s_fft_raw' in letter:
                type_high = 0x00
                type_low =  0x08
            elif 'dir_raw' in letter:
                type_high = 0x00
                type_low =  0x09
            elif 'voc_raw' in letter:
                type_high = 0x00
                type_low =  0x10
            elif '{' in letter and '}' in letter:
                type_high = 0x01
                type_low  = 0x01
        
            
        length_1  = 0x00
        length_2  = 0x00
        length_3  = 0x00
        length_4  = 0x00
        reserve_1 = 0x00
        reserve_2 = 0x00
        reserve_3 = 0x00
        
        
        if letter == '\x11':  # CTRL+Q
            cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"ENABLE"}')
        elif letter == '\x1A':  # CTRL+Z
            cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"DISABLE"}')
        elif letter == 'RBV' or letter == 'V':
            cmd_string = list('{"CMD": "RBV"}')
        elif letter == 'RBC' or letter == 'C':
            cmd_string = list('{"CMD": "RBC"}')
        elif letter == 'I':
            cmd_string = list('{"CMD": "RID", "MCU":0}')
        elif letter == 'F':
            cmd_string = list('{"CMD": "RFV", "MCU":0}')
        elif letter == 'p':
            cmd_string = list('{"CMD": "RPV"}')
        elif letter == 'v':
            cmd_string = list('version')
        else:
            cmd_string = list(letter)
            
        #print("Cmd: {}".format(cmd_string))
        
        length_1 = len(cmd_string) & 0xFF
        length_2 = (len(cmd_string) >> 8) & 0xFF
        length_3 = (len(cmd_string) >> 16) & 0xFF
        length_4 = (len(cmd_string) >> 24) & 0xFF
        
        calculated_checksum = 0
        check_sum = 0
        pay_load = []
        for i in range(0, len(cmd_string)):
            pay_load.append(ord(cmd_string[i]))
            calculated_checksum += pay_load[i]
        
        
        #calculated_checksum = header + version + type_high + type_low + length_1 + length_2 + length_3 + length_4 + reserve_1 + reserve_2 + reserve_3 + reserve_4
        
        calculated_checksum = calculated_checksum & 0xFF
        calculated_checksum = 0xFF - calculated_checksum
        
        check_sum = calculated_checksum
        #print('check_sum is ' + hex(check_sum))
        
        tx_data = []
        tx_data.append(header.to_bytes(1, 'big'))
        tx_data.append(version.to_bytes(1, 'big'))
        tx_data.append(type_high.to_bytes(1, 'big'))
        tx_data.append(type_low.to_bytes(1, 'big'))
        tx_data.append(length_1.to_bytes(1, 'big'))
        tx_data.append(length_2.to_bytes(1, 'big'))
        tx_data.append(length_3.to_bytes(1, 'big'))
        tx_data.append(length_4.to_bytes(1, 'big'))
        tx_data.append(check_sum.to_bytes(1, 'big'))
        tx_data.append(reserve_1.to_bytes(1, 'big'))
        tx_data.append(reserve_2.to_bytes(1, 'big'))
        tx_data.append(reserve_3.to_bytes(1, 'big'))

        
        #for i in range(0, len(pay_load)):
        for item in pay_load:
            tx_data.append(item.to_bytes(1, 'big'))        
               
        for i in range(0, len(tx_data)):
            #print(tx_data[i])
            self.serial_instance.write((tx_data[i]))



class DataReader(QtCore.QObject):
    newData  = QtCore.pyqtSignal(object)
    finished = QtCore.pyqtSignal()
    
    def __init__(self, serial_instance, parent=None):
        super().__init__(parent)
        self.serial = serial_instance
        
    def run(self):
        """Long Run task 1 to receive incoming data on serial port"""
        try:
            while True:
                # read all that is there or wait for one byte
                data = self.serial.read(self.serial.in_waiting)
                #print(data)
                #data = self.serial.read()
                if data:
                    new_recv_bytes = list(data)
                    self.newData.emit(new_recv_bytes)    
        except serial.SerialException:
            print('serial has exception')
        
        
 

class DataSender(QtCore.QObject):

    newData  = QtCore.pyqtSignal(object)
    finished = QtCore.pyqtSignal()
    
    #def __init__(self,parent=None,sizey=100,rangey=[0,100],delay=1000):
    def __init__(self, serial_transmitter,  delay=1000, parent=None):
        QtCore.QObject.__init__(self)
        self.parent = parent
        self.serial_tx = serial_transmitter
        self.delay  = delay
        self.mutex  = QMutex()        
        self.run    = True    
        self.cmd_index = 0
        self.recv_is_done = True
        self.start_time = time.time()
        self.enable_cycle_send = False # initially disabled
        self.all_cmd_list = []
        
    def sendCommand(self):
        while self.run:
            try:
                
                self.mutex.lock()        
                if self.enable_cycle_send == True and self.recv_is_done == True:
                    print('all send command: {}'.format(self.all_cmd_list))
                    #self.serial_tx.send_command_string(self.command_list[self.cmd_index], 'DIR_RAW_Type')  
                    self.start_time = time.time()
                    self.serial_tx.send_command_string(self.all_cmd_list[self.cmd_index], None)  
                    
                    self.recv_is_done = False
                    self.newData.emit(self.all_cmd_list[self.cmd_index])
                    self.cmd_index = self.cmd_index + 1
                    if (self.cmd_index >= len(self.all_cmd_list)):
                        self.cmd_index = 0
                self.mutex.unlock() 
                
                
                QtCore.QThread.msleep(self.delay)
                
            except serial.SerialException: 
                print('exception occurs in send thread')
        
        
class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        #MainWindow.resize(1600, 960)
        MainWindow.setMinimumSize(QtCore.QSize(1800, 1000))
        
        main_layout = pg.LayoutWidget()
        pg.setConfigOption('background', 'k')
        pg.setConfigOption('foreground', 'w')
        
        self.symbols = ['o', 't', '+', 'h', 'star', 'p', 'd', 'x', 't1', 't2', 't3', 'o', 't', 'p', 'h', 'star']
        
        self.pw1 = pg.PlotWidget(title="I2S Raw")
        self.pw1_plot_item = self.pw1.getPlotItem()
        self.pw1_plot_item.addLegend()
        self.pw1_plot_item.enableAutoRange('x', True)
        self.pw1_plot_item.enableAutoRange('y', True)
        
        self.curve_i2s_raw = [None] * 8
        for i in range(8):
            self.curve_i2s_raw[i] = self.pw1.plot(pen=(i, 8), name='raw {}'.format(i), symbol=self.symbols[i])
            #self.curve_i2s_raw[i] = self.pw1.plot(pen=(i, 8), name='raw {}'.format(i+1))
        
        
        
        self.pw1_plot_item.showGrid(True, True, 0.5)
            
        self.pw1.setLabel('left', 'Power', units='')
        self.pw1.setLabel('bottom', 'Time', units='Sample')
        main_layout.addWidget(self.pw1, row=0, col=5, rowspan=1, colspan=5)
        
        
        self.pw2 = pg.PlotWidget(title="FFT Power")
        self.pw2_plot_item = self.pw2.getPlotItem()
        self.pw2_plot_item.addLegend()
        self.pw2_plot_item.showGrid(True, True)
        self.curve_fft_power = [None] * 16      
        
        for i in range(len(self.curve_fft_power)):
            #self.curve_fft_power[i] = self.pw2.plot(pen=(i, 8), name='fft {}'.format(i), symbol=self.symbols[i])
            self.curve_fft_power[i] = self.pw2.plot(pen=(i, 8), name='fft {}'.format(i+1))
                
        self.pw2.setLabel('left', 'Power', units='')
        self.pw2.setLabel('bottom', 'Frequency', units='Hz')  
        main_layout.addWidget(self.pw2, row=1, col=5, rowspan=1, colspan=5)
        
        
        
        self.pw3 = pg.PlotWidget(title="DIR Raw")
        self.pw3_plot_item = self.pw3.getPlotItem()
        self.pw3_plot_item.addLegend()
        self.pw3_plot_item.showGrid(True, True)
        self.curve_dir_raw = [None] * 16
        #for i in range(16):
            #self.curve_dir_raw[i] = self.pw3.plot(pen=(i, 16), name='dir {}'.format(i), symbol=self.symbols[i], symbolPen=(i,16))
            #self.curve_dir_raw[i] = self.pw3.plot(pen=(i, 16), name='dir {}'.format(i))
        
        self.curve_dir_raw[0] = self.pw3.plot(pen=(1, 16), name='dir {}'.format(0))
        self.curve_dir_raw[1] = self.pw3.plot(pen=(2, 16), name='dir {}'.format(1))
        self.curve_dir_raw[2] = self.pw3.plot(pen=(3, 16), name='dir {}'.format(2))
        self.curve_dir_raw[3] = self.pw3.plot(pen=(4, 16), name='dir {}'.format(3))
        self.curve_dir_raw[4] = self.pw3.plot(pen=(5, 16), name='dir {}'.format(4))
        self.curve_dir_raw[5] = self.pw3.plot(pen=(6, 16), name='dir {}'.format(5), symbol='+')
        self.curve_dir_raw[6] = self.pw3.plot(pen=(7, 16), name='dir {}'.format(6), symbol='o')
        self.curve_dir_raw[7] = self.pw3.plot(pen=(8, 16), name='dir {}'.format(7), symbol='t')
        self.curve_dir_raw[8] = self.pw3.plot(pen=(9, 16), name='dir {}'.format(8))
        self.curve_dir_raw[9] = self.pw3.plot(pen=(10, 16), name='dir {}'.format(9))
        self.curve_dir_raw[10] = self.pw3.plot(pen=(11, 16), name='dir {}'.format(10))
        self.curve_dir_raw[11] = self.pw3.plot(pen=(12, 16), name='dir {}'.format(11))
        self.curve_dir_raw[12] = self.pw3.plot(pen=(13, 16), name='dir {}'.format(12))
        self.curve_dir_raw[13] = self.pw3.plot(pen=(14, 16), name='dir {}'.format(13))
        self.curve_dir_raw[14] = self.pw3.plot(pen=(15, 16), name='dir {}'.format(14))
        self.curve_dir_raw[15] = self.pw3.plot(pen=(16, 16), name='dir {}'.format(15))

        
        
        
        self.pw3.setLabel('left', 'Amplitude', units='')
        self.pw3.setLabel('bottom', 'Time', units='Sample')
        main_layout.addWidget(self.pw3, row=0, col=0, rowspan=1, colspan=5)
        
        
        
        self.pw4 = pg.PlotWidget(title="FFT Power (Soft)")
        self.pw4_plot_item = self.pw4.getPlotItem()
        '''
        self.pw4_plot_item.setAspectLocked()
        self.pw4_plot_item.addLegend()
        #self.pw4_plot_item.showGrid(True, True)
        self.pw4_plot_item.addLine(x=0, pen=0.3)
        self.pw4_plot_item.addLine(y=0, pen=0.3)
        
        
        radius_range = 120
        
        for r in np.arange(0.0, radius_range, 5.0):
            circle = pg.QtGui.QGraphicsEllipseItem(-r, -r, r * 2, r * 2)
            circle.setPen(pg.mkPen(0.3))
            self.pw4_plot_item.addItem(circle)
        
        degree_symbol = u"\u00b0"
        deg0Text = pg.TextItem('0' + degree_symbol)
        self.pw4_plot_item.addItem(deg0Text)
        deg0Text.setPos(0, radius_range*1.05) 
        
        deg45Text = pg.TextItem('45' + degree_symbol)
        self.pw4_plot_item.addItem(deg45Text)
        deg45Text.setPos(int(radius_range*0.71), int(radius_range*0.71))
        
        deg90Text = pg.TextItem('90' + degree_symbol)
        self.pw4_plot_item.addItem(deg90Text)
        deg90Text.setPos(radius_range, 0)
        
        deg135Text = pg.TextItem('135' + degree_symbol)
        self.pw4_plot_item.addItem(deg135Text)
        deg135Text.setPos(int(radius_range*0.75), int(radius_range*-0.75))
        
        deg180Text = pg.TextItem('-/+180' + degree_symbol)
        self.pw4_plot_item.addItem(deg180Text)
        deg180Text.setPos(0, -radius_range)
        
        degn45Text = pg.TextItem('-45' + degree_symbol)
        self.pw4_plot_item.addItem(degn45Text)
        degn45Text.setPos(int(radius_range*-0.75), int(radius_range*0.75))
        
        degn90Text = pg.TextItem('-90' + degree_symbol)
        self.pw4_plot_item.addItem(degn90Text)
        degn90Text.setPos(int(radius_range*-1.1), 0)
        
        degn135Text = pg.TextItem('-135' + degree_symbol)
        self.pw4_plot_item.addItem(degn135Text)
        degn135Text.setPos(int(radius_range*-0.75), int(radius_range*-0.75))
        
                
        dLine_1 = pg.InfiniteLine(angle=45, movable=False, pen=0.3)
        dLine_2 = pg.InfiniteLine(angle=135, movable=False, pen=0.3)
        
        self.pw4_plot_item.addItem(dLine_1)
        self.pw4_plot_item.addItem(dLine_2) 
        '''
        
        self.curve_fft_phase = [None] * 16              
        
        for i in range(len(self.curve_fft_phase)):
            #self.curve_fft_phase[i] = self.pw4.plot(pen=(i, 16), name='fft {}'.format(i), symbol=self.symbols[i])
            self.curve_fft_phase[i] = self.pw4.plot(pen=(i, 16), name='FFT phase {}'.format(i+1), symbol=self.symbols[i], symbolPen=(i,16))
                
        #self.pw4.setLabel('left', 'Power', units='')
        #self.pw4.setLabel('bottom', 'Frequency', units='Hz')  
        main_layout.addWidget(self.pw4, row=1, col=0, rowspan=1, colspan=5)
        
        
        
        #self.clicksLabel = QLabel()
        #self.clicksLabel.setText('')
        #self.clicksLabel.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                
        #main_layout.addWidget(self.clicksLabel, row=1, col=0)
        
        #self.sendBtn = QPushButton()
        #self.sendBtn.setText('Start Reading Battery')
        #main_layout.addWidget(self.sendBtn, row=2, col=0)
        
        
        #self.secondClickLabel = QLabel()
        #self.secondClickLabel.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        #main_layout.addWidget(self.secondClickLabel, row=1, col=5)
        
        #self.secondSendBtn = QPushButton()
        #self.secondSendBtn.setText('Read F/W Version')
        #main_layout.addWidget(self.secondSendBtn, row=2, col=0)
        
        
        #self.thirdLabel = QLabel()
        #self.thirdLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        #main_layout.addWidget(self.thirdLabel, row=2, col=1)
        
        #newfont = QtGui.QFont("Times", 10, QtGui.QFont.Bold)
        
        self.cmdTextLabel = QLabel()
        #self.cmdTextLabel.setFont(newfont)
        self.cmdTextLabel.setText('Command: ')
        self.cmdTextLabel.setMaximumWidth(130)
        self.cmdTextLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.cmdTextLabel, row=3, col=0)
        
        #self.cmdlineEdit = QLineEdit()
        #self.cmdlineEdit.setText('i2s_raw')
        #self.cmdlineEdit.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        #self.cmdlineEdit.setMaximumWidth(400)
        
        #main_layout.addWidget(self.cmdlineEdit, row=3, col=1, rowspan=1, colspan=1)
        
        self.cmdComboBox = QComboBox()
        self.cmdComboBox.setEditable(True)
        
        
        self.cmdComboBox.addItem('i2s_raw -start 100 -end 512')
        self.cmdComboBox.addItem('i2s_soft_fft')
        self.cmdComboBox.addItem('i2s_hard_fft')
        
        self.cmdComboBox.addItem('i2s_fft_raw')
        
        self.cmdComboBox.addItem('i2s_raw -target 1')
        self.cmdComboBox.addItem('i2s_soft_fft -target 1')
        self.cmdComboBox.addItem('i2s_hard_fft -target 1')
        
        self.cmdComboBox.addItem('i2s_fft_raw -target 1')
        
        self.cmdComboBox.addItem('i2s_raw -target 2')
        self.cmdComboBox.addItem('i2s_soft_fft -target 2')
        self.cmdComboBox.addItem('i2s_hard_fft -target 2')
        
        self.cmdComboBox.addItem('i2s_fft_raw -target 2')
        
        self.cmdComboBox.addItem('dir_raw')
        self.cmdComboBox.addItem('dir_raw -target 1')
        self.cmdComboBox.addItem('dir_raw -target 2')
        
        self.cmdComboBox.addItem('voc_raw')
        self.cmdComboBox.addItem('voc_raw -target 1')
        self.cmdComboBox.addItem('voc_raw -target 2')
        
        
        self.cmdComboBox.addItem('{"CMD":"RST"}')
        self.cmdComboBox.addItem('{"CMD":"PWR", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"PWR", "MCU":2, "STATE":"ENABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"RBV"}')        
        self.cmdComboBox.addItem('{"CMD":"RBC"}')
        self.cmdComboBox.addItem('{"CMD":"RFV"}')
        
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":2, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":2, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":2, "STATE":"DISABLE"}')
        
        
        
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":0, "STATE":"ENABLE"}')
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":0, "STATE":"DISABLE"}')
        
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":1, "STATE":"ENABLE"}')
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":1, "STATE":"DISABLE"}')
        
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":2, "STATE":"ENABLE"}')
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":2, "STATE":"DISABLE"}')
        
        
        
        #self.cmdComboBox.setMinimumWidth(400)
        main_layout.addWidget(self.cmdComboBox, row=3, col=1, rowspan=1, colspan=1)
        
        self.cmdSendBtn = QPushButton()
        self.cmdSendBtn.setText('Send Command')
        #self.cmdSendBtn.setMaximumWidth(150)
        
        main_layout.addWidget(self.cmdSendBtn, row=3, col=2)
        
        self.cmdStartBtn = QPushButton()
        self.cmdStartBtn.setText('Start Reading')
        #self.cmdStartBtn.setMaximumWidth(150)
        main_layout.addWidget(self.cmdStartBtn, row=3, col=3)
        
        self.i2sRawRadioBtn = QRadioButton('I2S Raw')
        self.i2sRawRadioBtn.setChecked(True)
        self.i2sRawRadioBtn.command_name = 'i2s_raw'
        #main_layout.addWidget(self.i2sRawRadioBtn, row=3, col=4)
        
        self.i2sRawFFTRadioBtn = QRadioButton('I2S Raw+FFT')
        self.i2sRawFFTRadioBtn.setChecked(False)
        self.i2sRawFFTRadioBtn.command_name = 'i2s_fft_raw'
        #main_layout.addWidget(self.i2sRawFFTRadioBtn, row=3, col=5)
        
        self.dirRadioButton = QRadioButton('DIR Raw')
        #self.radioButton.setChecked(False)
        self.dirRadioButton.command_name = 'dir_raw'
        #main_layout.addWidget(self.dirRadioButton, row=3, col=6)
        
        
        self.mcu0ChkBox = QCheckBox()
        self.mcu0ChkBox.setText('MCU0')
        self.mcu0ChkBox.setChecked(True)
        main_layout.addWidget(self.mcu0ChkBox, row=3, col=7)
        
        
        self.mcu1ChkBox = QCheckBox()
        self.mcu1ChkBox.setText('MCU1')
        main_layout.addWidget(self.mcu1ChkBox, row=3, col=8)
        
        
        self.mcu2ChkBox = QCheckBox()
        self.mcu2ChkBox.setText('MCU2')
        main_layout.addWidget(self.mcu2ChkBox, row=3, col=9)
        
        
        #self.indexTextLabel = QLabel()
        #self.indexTextLabel.setText('Max FFT Index: ')
        #self.indexTextLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        #main_layout.addWidget(self.indexTextLabel, row=4, col=5)
        
        #self.indexLabel = QLabel()
        #self.indexLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        #main_layout.addWidget(self.indexLabel, row=4, col=6)
        
        
        self.cmdIndexLabel = QLabel()
        self.cmdIndexLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        main_layout.addWidget(self.cmdIndexLabel, row=4, col=7)
        
        
        self.rspTxtLabel = QLabel()
        self.rspTxtLabel.setText('Command Response: ')
        self.rspTxtLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        #self.rspTxtLabel.setMaximumWidth(140)
        main_layout.addWidget(self.rspTxtLabel, row=4, col=0)
        
        self.rspLabel = QLabel()
        self.rspLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        main_layout.addWidget(self.rspLabel, row=4, col=1, rowspan=1, colspan=6)
        
        MainWindow.setCentralWidget(main_layout)
        
        
        
        
        
class ApplicationWindow(QtGui.QMainWindow):

    def __init__(self):
        
        QtGui.QMainWindow.__init__(self)
        
        
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setStyleSheet("QMainWindow {background: 'lightGray';}")
        
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        #self.ui.secondSendBtn.clicked.connect(self.secondSendButtonClicks)
        self.ui.cmdSendBtn.clicked.connect(self.commandSendButtonClicks)
        self.ui.cmdStartBtn.clicked.connect(self.readingStartButtonClicks)
        
        self.ui.i2sRawRadioBtn.toggled.connect(self.commandRadioBtnClicks)
        self.ui.i2sRawFFTRadioBtn.toggled.connect(self.commandRadioBtnClicks)
        self.ui.dirRadioButton.toggled.connect(self.commandRadioBtnClicks)
        
        #self.ui.mcu0ChkBox.toggled.connect(self.mcu0CheckBoxClicks)
        #self.ui.mcu1ChkBox.toggled.connect(self.mcu1CheckBoxClicks)
        #self.ui.mcu2ChkBox.toggled.connect(self.mcu2CheckBoxClicks)
        
        
        #finish = QAction("Quit", self)
        #finish.triggered.connect(self.closeEvent)
        self.port = None
        self.baudrate = 115200
        self.parity = 'N'
        self.rtscts = False
        self.xonxoff = False
        self.exclusive = True
        
        
        self.recv_bytes = []
        self.i2s_sample_frequency = 317382
        
        self.i2s_all_raw_bytes = []
        self.i2s_all_raw_value = []
        self.i2s_chan_raw_value = [0]*8
        
        self.active_chan_number = 0
        self.sample_start = 0
        self.sample_end = 0
        self.channel_mask = 0
        
        self.i2s_all_fft_bytes = []
        self.i2s_all_fft_value = []
        self.i2s_chan_fft_value = [0]*8
        
        #self.i2s_fft_max_value = [0]*8
        
        
        
        self.dir_all_raw_bytes = []
        self.dir_all_raw_value = []
        self.dir_chan_raw_value = [0]*16
        
        # soft Beamforming direction array
        self.soft_dir_chan_value = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
        
        
         
         
        self.soft_dir_sum = [0]*16
        
        self.voc_raw_bytes = []
        self.voc_raw_value = []
        
        
        
        self.data_length = 0
        self.data_type = 0
        
        self.fft_x = []
        for k in range(256):
            self.fft_x.append(k * float(self.i2s_sample_frequency) / 512)
                                
                                
        
        try:
            self.port = self.ask_for_port()
        except KeyboardInterrupt:
            self.port = None
        
        
        try:
            self.serial_instance = serial.serial_for_url(
                self.port,
                self.baudrate,
                parity=self.parity,
                rtscts=self.rtscts,
                xonxoff=self.xonxoff,
                do_not_open=True,
                timeout= 8.0)

            #if not hasattr(self.serial_instance, 'cancel_read'):
                # enable timeout for alive flag polling if cancel_read is not available
            #    self.serial_instance.timeout = 1
            
            if isinstance(self.serial_instance, serial.Serial):
                self.serial_instance.exclusive = self.exclusive

            self.serial_instance.open()
            if self.serial_instance.is_open == True:
                print('port {} is opened successfully!'.format(self.port))           
            else:
                print('Failed to open port {}!'.format(self.port))   
                return
                
            
        except serial.SerialException as e:
            sys.stderr.write('could not open port {!r}: {}\n'.format(self.port, e))
            
        
        self.serial_instance.flushInput()
        
        self.serial_transmitter = SerialTransmitter(self.serial_instance)
        
        
        # first task of cycle reading 
        #self.x1 = range(0,100)
        self.thread_send = QtCore.QThread()
        self.sender   = DataSender(self.serial_transmitter, 200)
        self.sender.moveToThread(self.thread_send)
        self.thread_send.started.connect(self.sender.sendCommand)
        self.sender.finished.connect(self.thread_send.quit)
        self.sender.finished.connect(self.sender.deleteLater)
        self.thread_send.finished.connect(self.thread_send.deleteLater)
        self.sender.newData.connect(self.processSendNewData)
        self.thread_send.start()
        
        
        
        """Long-running task in 5 steps."""
        # Step 2: Create a QThread object
        self.thread_read = QtCore.QThread()
        # Step 3: Create a worker object
        self.reader = DataReader(self.serial_instance)
        # Step 4: Move worker to the thread
        self.reader.moveToThread(self.thread_read)
        # Step 5: Connect signals and slots
        self.thread_read.started.connect(self.reader.run)
        self.reader.finished.connect(self.thread_read.quit)
        self.reader.finished.connect(self.reader.deleteLater)
        self.thread_read.finished.connect(self.thread_read.deleteLater)
        self.reader.newData.connect(self.processNewData)
        # Step 6: Start the thread
        self.thread_read.start()
        
           
           
    
        

        # Final resets
        #self.longRunningBtn.setEnabled(False)
        #self.thread_read.finished.connect(
        #    lambda: self.longRunningBtn.setEnabled(True)
        #)
        #self.thread.finished.connect(
        #    lambda: self.stepLabel.setText("Long-Running Step: 0")
        #)
       
       
    def ask_for_port(self):
        """\
        Show a list of ports and ask the user for a choice. To make selection
        easier on systems with long device names, also allow the input of an
        index.
        """
        sys.stderr.write('\n--- Available ports:\n')
        ports = []
        for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
            sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
            ports.append(port)
            if 'USB Serial Port' in desc:
                return port
        
        while True:
            port = input('--- Enter port index or full name: ')
            try:
                index = int(port) - 1
                if not 0 <= index < len(ports):
                    sys.stderr.write('--- Invalid index!\n')
                    continue
            except ValueError:
                pass
            else:
                port = ports[index]
            return port

       
         
            
    
        
      
    def processSendNewData(self, var):
        self.ui.cmdIndexLabel.setText(str(var))
       
        
    def processNewData(self, var):
        """handler for new data received by serial"""
        
        received_bytes = copy(var)
        self.recv_bytes = self.recv_bytes + received_bytes
        #print(self.recv_bytes) # debug
            
        if 0x11 == self.recv_bytes[0]:
            #print('first byte found!')
            if len(self.recv_bytes) >= 12:
                self.data_length = self.recv_bytes[4] + (self.recv_bytes[5] << 8) + \
                    (self.recv_bytes[6] << 16) + (self.recv_bytes[7] << 24)
                
                
                if len(self.recv_bytes) == self.data_length + 12:
                    self.data_type = (self.recv_bytes[3] << 8) + self.recv_bytes[2]
                    #print('data length is {}'.format(self.data_length))
                    #print('data type is {}'.format(self.data_type))
                    
                    
                    # handle i2s raw data
                    if self.data_type == 0x0700:
                        self.i2s_all_raw_value = []
                        self.i2s_all_raw_bytes = self.recv_bytes[12:]
                        self.recv_bytes = []
                        #print(len(self.i2s_all_raw_bytes))
                        i2s_value = 0
                        for i in range(0, len(self.i2s_all_raw_bytes)//2):
                            i2s_value = self.i2s_all_raw_bytes[2*i] + (self.i2s_all_raw_bytes[2*i+1] << 8)                            
                            if (i2s_value & 0x8000):
                                self.i2s_all_raw_value.append(i2s_value - 65536)
                            else:
                                self.i2s_all_raw_value.append(i2s_value)
                        
                        #print('length of all received data: {}'.format(len(self.i2s_all_raw_value)))
                        i2s_channel_data_length = int(len(self.i2s_all_raw_value) / self.active_chan_number)
                        #print('channel length: {}'.format(i2s_channel_data_length))
                        if i2s_channel_data_length == 0:
                            self.sender.recv_is_done = True
                            print('received data length is 0!')
                            return
                        
                        
                        j = 0
                        for i in range(8):
                            if ((self.channel_mask >> i) & 0x01) != 0:
                                
                                #print('{}th bit, {}'.format(i, j))
                                self.i2s_chan_raw_value[i] = self.i2s_all_raw_value[j * i2s_channel_data_length : j * i2s_channel_data_length + i2s_channel_data_length]
                                #print('elapsed time of reading raw data with size {0} is {1}'.format(len(self.i2s_all_raw_value), (time.time() - self.sender.start_time)))
        
                                self.ui.curve_i2s_raw[i].setData(range(self.sample_start, self.sample_start+len(self.i2s_chan_raw_value[i])), self.i2s_chan_raw_value[i])
                            
                                j = j + 1
                            else:
                                self.i2s_chan_raw_value[i] = []
                                self.ui.curve_i2s_raw[i].setData([], [])
                            self.ui.pw1_plot_item.setXRange(self.sample_start, self.sample_end + 1)  
                            
                        print('elapsed time of reading raw format i2s data with size {0} is {1}'.format(len(self.i2s_all_raw_value), (time.time() - self.sender.start_time)))
        
                    
                    
                        
                    
                    
                    # handle FFT data only
                    if self.data_type == 0x0400:
                        self.i2s_all_fft_value = []
                        self.i2s_all_fft_bytes = self.recv_bytes[12:]
                        self.recv_bytes = []
                        
                        i2s_fft_value = 0
                        for i in range(0, len(self.i2s_all_fft_bytes)//2):
                            i2s_fft_value = self.i2s_all_fft_bytes[2*i] + (self.i2s_all_fft_bytes[2*i+1] << 8)                            
                            if (i2s_fft_value & 0x8000):
                                self.i2s_all_fft_value.append(i2s_fft_value - 65536)
                            else:
                                self.i2s_all_fft_value.append(i2s_fft_value)
                        
                        #print(len(self.i2s_all_fft_value))                   
                        #print(self.active_chan_number)
                        #print(self.channel_mask)
                        
                        
                        fft_channel_data_length = int(len(self.i2s_all_fft_value) / self.active_chan_number)
                        #print('start:{}, end:{}'.format(self.sample_start, self.sample_end))
                        x_f = []
                        for k in range(256):
                            x_f.append(k * float(self.i2s_sample_frequency) / 512)
                        
                        j = 0
                        for i in range(8):
                            if ((self.channel_mask >> i) & 0x01) != 0:
                                self.i2s_chan_fft_value[i] = self.i2s_all_fft_value[j*fft_channel_data_length : j*fft_channel_data_length + fft_channel_data_length]
                                self.ui.curve_fft_power[i].setData(x_f[self.sample_start-1:self.sample_end], self.i2s_chan_fft_value[i])
                                j = j + 1
                            else:
                                self.i2s_chan_fft_value[i] = []
                                self.ui.curve_fft_power[i].setData([], self.i2s_chan_fft_value[i])
                            
                            
                            self.ui.pw2_plot_item.setXRange(x_f[self.sample_start-1], x_f[self.sample_end-1]+1)
                            
                            
                    
                    # handle i2s raw and fft received from MCU                      
                    if self.data_type == 0x0800:
                        self.i2s_all_raw_value = []
                        self.i2s_all_raw_bytes = self.recv_bytes[12:8204]
                        self.i2s_all_fft_value = []
                        self.i2s_all_fft_bytes = self.recv_bytes[8204:]
                        self.recv_bytes = []
                        
                        i2s_value = 0
                        for i in range(0, len(self.i2s_all_raw_bytes)//2):
                            i2s_value = self.i2s_all_raw_bytes[2*i] + (self.i2s_all_raw_bytes[2*i+1] << 8)                            
                            if (i2s_value & 0x8000):
                                self.i2s_all_raw_value.append(i2s_value - 65536)
                            else:
                                self.i2s_all_raw_value.append(i2s_value)
                                                                                
                        # extract certain length of i2s raw data for each channel
                        i2s_raw_cut_sample_start = 0   
                        i2s_raw_cut_sample_end = 512
                        for i in range(8):
                            self.i2s_chan_raw_value[i] = self.i2s_all_raw_value[i*512 + i2s_raw_cut_sample_start : i*512 + 512 + i2s_raw_cut_sample_end]
                            chan_abs_sum = 0
                            chan_sum = 0
                            #print(len(self.i2s_chan_raw_value[i]))
                            for j in range(len(self.i2s_chan_raw_value[i])):
                                #print(abs(self.i2s_chan_raw_value[i][j]))
                                chan_abs_sum = chan_abs_sum + abs(self.i2s_chan_raw_value[i][j])
                                chan_sum = chan_sum + self.i2s_chan_raw_value[i][j]
                            
                            self.ui.curve_i2s_raw[i].setData(range(0, len(self.i2s_chan_raw_value[i])), self.i2s_chan_raw_value[i])
                        
                        #print(len(self.i2s_chan_raw_value[0]))
                        
                        
                        # read the FFT part 
                        i2s_fft_value = 0
                        for i in range(0, len(self.i2s_all_fft_bytes)//2):
                            i2s_fft_value = self.i2s_all_fft_bytes[2*i] + (self.i2s_all_fft_bytes[2*i+1] << 8)                            
                            if (i2s_fft_value & 0x8000):
                                self.i2s_all_fft_value.append(i2s_fft_value - 65536)
                            else:
                                self.i2s_all_fft_value.append(i2s_fft_value)
                                
                        for i in range(8):
                            self.i2s_chan_fft_value[i] = self.i2s_all_fft_value[i*256 : i*256 + 256]
                            
                            #self.i2s_fft_max_value[i] = max(self.i2s_chan_fft_value[i])
                            
                            #print(len(self.i2s_chan_fft_value[i]))
                            x_f = []
                            for j in range(len(self.i2s_chan_fft_value[i])):
                                x_f.append(j * float(self.i2s_sample_frequency) / 512)
                            
                            self.ui.curve_fft_power[i].setData(x_f, self.i2s_chan_fft_value[i])
                            
                        
                        
                        
                    # handle directly read DIR buffer from MCU    
                    if self.data_type == 0x0900:
                        
                        self.dir_all_raw_bytes = self.recv_bytes[12:]
                        self.recv_bytes = []
                        
                        self.dir_all_raw_value = []
                        dir_value = 0
                        for i in range(0, len(self.dir_all_raw_bytes)//2):
                            dir_value = self.dir_all_raw_bytes[2*i] + (self.dir_all_raw_bytes[2*i+1] << 8)                            
                            if (dir_value & 0x8000):
                                self.dir_all_raw_value.append(dir_value - 65536)
                            else:
                                self.dir_all_raw_value.append(dir_value)
                        
                        dir_data_length = int(len(self.dir_all_raw_value) / self.active_chan_number)
                        
                        j = 0
                        for i in range(16):
                            if ((self.channel_mask >> i) & 0x01) != 0:
                                self.dir_chan_raw_value[i] = self.dir_all_raw_value[j*dir_data_length : j*dir_data_length + dir_data_length]
                                self.ui.curve_dir_raw[i].setData(range(self.sample_start-1, self.sample_end), self.dir_chan_raw_value[i])
                                j = j + 1
                            else:
                                self.dir_chan_raw_value[i] = []
                                self.ui.curve_dir_raw[i].setData([], self.dir_chan_raw_value[i])
                            self.ui.pw4_plot_item.setXRange(self.sample_start, self.sample_end + 1)  
                        
                        
                    if self.data_type == 0x0101:
                        init_string = ''.join([chr(elem) for elem in self.recv_bytes[12:]])
                        self.recv_bytes = []
                        if 'RSP' in init_string:
                            self.ui.rspLabel.setText(init_string) # debug
                        elif 'I2S' in init_string and 'FFT' not in init_string:
                            print('Total length of json string is {}'.format(len(init_string))) #debug
                            json_dict = json.loads(init_string)
                            #print(json_dict)
                            print('---- elapsed time of reading i2s data in json array of size {0} is {1}'.format(len(json_dict['DATA']), (time.time() - self.sender.start_time)))
                            #self.ui.rspLabel.setText(str(json_dict['DATA']))
                            #print(len(json_dict['DATA']))
                            for i in range(len(json_dict['DATA'])):
                                #print(json_dict['DATA'][i])
                                if len(json_dict['DATA'][i]) > 0:
                                    self.ui.curve_i2s_raw[i].setData(range(self.sample_start, self.sample_start+len(json_dict['DATA'][i])), json_dict['DATA'][i])
                                else:
                                    self.ui.curve_i2s_raw[i].setData([], json_dict['DATA'][i])
                            #json_data = np.array([int(i) for i in json_dict['DATA']])
                            #print(json_data)
                            #self.ui.curve_i2s_raw[0].setData(range(0, len(json_dict['DATA'])), json_dict['DATA'])
                        elif 'I2S_FFT' in init_string:
                            
                            
                            print('Total length of json string is {}'.format(len(init_string))) #debug
                            json_dict = json.loads(init_string)
                            print('---- elapsed time of reading i2s_FFT data in json array of size {0} is {1}'.format(len(json_dict['DATA']), (time.time() - self.sender.start_time)))
                            for i in range(len(json_dict['DATA'])):
                                #print(json_dict['DATA'][i])
                                if len(json_dict['DATA'][i]) > 0:
                                    self.ui.curve_fft_power[i].setData(self.fft_x, json_dict['DATA'][i])
                                else:
                                    self.ui.curve_fft_power[i].setData([], json_dict['DATA'][i])
                            
                    if self.data_type == 0x1000:
                        self.voc_raw_value = []
                        self.voc_raw_bytes = self.recv_bytes[12:]
                        self.recv_bytes = []
                        print(len(self.voc_raw_bytes))
                        voc_value = 0
                        for i in range(0, len(self.voc_raw_bytes)//2):
                            voc_value = self.voc_raw_bytes[2*i] + (self.voc_raw_bytes[2*i+1] << 8)                            
                            if (voc_value & 0x8000):
                                self.voc_raw_value.append(voc_value - 65536)
                            else:
                                self.voc_raw_value.append(voc_value)
                        
                        
                        self.ui.curve_i2s_raw[0].setData(range(0, len(self.voc_raw_value)), self.voc_raw_value)
                        
                        
                            
        
        
                    self.sender.recv_is_done = True
        else:
            self.recv_bytes = []
            self.sender.recv_is_done = True        
                
                                         
    def commandRadioBtnClicks(self):
        radioButton = self.sender()
        
        if radioButton.isChecked():
            print("Comamnd is %s" % (radioButton.command_name))    
            if 'i2s' in radioButton.command_name:
                self.ui.pw3_plot_item.setTitle('Soft DIR Buffer')
            elif 'dir_raw' in radioButton.command_name:
                self.ui.pw3_plot_item.setTitle('Hard DIR Buffer')
    
    
    def findLLCommandFieldValue(self, llc_command, field_name):
        """handler of extract information about llc """
        # set default value of a field
        field_value = 0
        if field_name == '-start':
            if 'dir_raw' in llc_command:
                field_value = 257
            else:
                field_value = 1
        elif field_name == '-end':
            if 'i2s_raw' in llc_command or 'dir_raw' in llc_command:
                field_value = 512
            elif 'i2s_soft' in llc_command or 'i2s_hard' in llc_command:
                field_value = 256
        elif field_name == '-channel':
            if 'i2s_raw' in llc_command or 'i2s_soft' in llc_command or 'i2s_hard' in llc_command:
                field_value = 0xFF
            elif 'dir_raw' in llc_command:
                field_value = 0xFFFF
        
        field_string = ''        
        x = llc_command.split(" ")        
        if llc_command.find(field_name) != -1:
            index_field = x.index(field_name)            
            for i in range(index_field+1, len(x)):            
                if x[i] != '':
                    field_string = x[i]
                    break
            #print('start value: {}'.format(start_string))
        # convert from str to int value for the field value
        if field_string != '':
            if field_string.find('x') != -1:
                field_value = int(field_string, base=16)
            else:
                field_value = int(field_string)
        return field_value
        
        
    def commandSendButtonClicks(self):
        """handler of clicking the one-time send button"""
        #self.recv_bytes = []
        current_command = str(self.ui.cmdComboBox.currentText())
        #print(current_command)
        
        if current_command != "":
            self.sample_start = self.findLLCommandFieldValue(current_command, '-start')
            self.sample_end = self.findLLCommandFieldValue(current_command, '-end')
            self.channel_mask = self.findLLCommandFieldValue(current_command, '-channel')
            self.active_chan_number = bin(self.channel_mask).count("1")
            
        self.sender.start_time = time.time()
        if self.sender.recv_is_done == True:    
            #print(self.ui.cmdlineEdit.text())
            if 'i2s_raw' in current_command:                
                self.serial_transmitter.send_command_string(current_command, 'I2S_Raw_Type')
            elif 'i2s_soft' in current_command or 'i2s_hard' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'I2S_FFT_Type')
            elif 'i2s_fft_raw' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'RAW_FFT_Type')
            elif 'dir_raw' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'DIR_RAW_Type')
            elif 'voc_raw' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'VOC_RAW_Type')
            
            elif 'CMD' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'JSON_Type')
        else:
            print('wait for receiving is done!')
    
    
    def readingStartButtonClicks(self):
        """handler for clicking the Start Sending (cycle reading) Button"""
        
        self.sender.enable_cycle_send = not self.sender.enable_cycle_send
        
        if self.sender.enable_cycle_send == True:
            self.ui.cmdStartBtn.setText('Stop Reading')
            
            current_cycle_cmd = str(self.ui.cmdComboBox.currentText())
            if current_cycle_cmd != "":
                self.sample_start = self.findLLCommandFieldValue(current_cycle_cmd, '-start')
                self.sample_end = self.findLLCommandFieldValue(current_cycle_cmd, '-end')
                self.channel_mask = self.findLLCommandFieldValue(current_cycle_cmd, '-channel')
                self.active_chan_number = bin(self.channel_mask).count("1")
            
            self.sender.all_cmd_list = []
            
            if self.ui.mcu0ChkBox.isChecked() == True:
                current_cmd = current_cycle_cmd 
                self.sender.all_cmd_list.append(current_cmd)
            if self.ui.mcu1ChkBox.isChecked() == True:
                current_cmd = current_cycle_cmd + ' -target 1'
                self.sender.all_cmd_list.append(current_cmd)
            if self.ui.mcu2ChkBox.isChecked() == True:
                current_cmd = current_cycle_cmd + ' -target 2'
                self.sender.all_cmd_list.append(current_cmd)
                
            
                
            
            print('Start to periodically send commands {}'.format(self.sender.all_cmd_list))
            
            self.ui.i2sRawFFTRadioBtn.setEnabled(False)
            self.ui.dirRadioButton.setEnabled(False)
            self.ui.mcu0ChkBox.setEnabled(False)
            self.ui.mcu1ChkBox.setEnabled(False)
            self.ui.mcu2ChkBox.setEnabled(False)
        else:
            self.ui.cmdStartBtn.setText('Start Reading')
            self.ui.i2sRawFFTRadioBtn.setEnabled(True)
            self.ui.dirRadioButton.setEnabled(True)
            self.ui.mcu0ChkBox.setEnabled(True)
            self.ui.mcu1ChkBox.setEnabled(True)
            self.ui.mcu2ChkBox.setEnabled(True)
            print('Stop periodically sending command {}'.format(self.sender.all_cmd_list))
            
            
    
    
            
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    main = ApplicationWindow()
    main.resize(1200, 700)
    main.setWindowTitle('I2S and DIR Plotting')
    main.show()
    sys.exit(app.exec_())
