# Code to show two plots of simulated streamed data. Data for each plot is processed (generated) 
# by separate threads, and passed back to the gui thread for plotting.
# This is an example of using movetothread, which is the correct way of using QThreads

# Michael Hogg, 2015

#import time, sys, re # re no used
import time, sys, copy
from multiprocessing.connection import Client, Listener
import PULD_Server as ps

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QIntValidator
from PyQt5.Qt import QMutex

from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
from PyQt5.QtWidgets import (
    QLabel,
    QLineEdit,
    QComboBox,
    QCheckBox,
    QPushButton,
)


## https://docs.python.org/3/library/multiprocessing.html
#from multiprocessing.connection import Listener
#import json

#from multiprocessing import Queue
##import queue

## https://github.com/jupyter/qtconsole
from qtconsole.rich_jupyter_widget import RichJupyterWidget
from qtconsole.manager import QtKernelManager

#from random import randint
#from copy import copy


import numpy as np
#import numpy.ma as ma

display_rate_sample_limit = 1
display_total_sample_limit = 10

streaming_enabled = False
stripchart_enabled = False

dbfs_min_init = 0
dbfs_max_init = -120

A_B_Testing = True

#Data_Stream_Mssg = ""
#CMD_RSP_Mssg = ""






#class DataReader(QtCore.QObject):

#    newData  = QtCore.pyqtSignal(object)
#    finished = QtCore.pyqtSignal()
    
#    def __init__(self, serial_instance, parent=None):
#        super().__init__(parent)
#        self.serial = serial_instance
#        self.recv_bytes_list = []
        
#    def run(self):
#        """Long Run task 1 to receive incoming data on serial port"""
#        try:
#            while True:
#                # read all that is there or wait for one byte
#                data = self.serial.read(self.serial.in_waiting)
#                if data:
#                    new_recv_bytes = list(data)
#                    #self.recv_bytes_list.append(new_recv_bytes)
#                    #self.recv_bytes_list = new_recv_bytes
#                    self.newData.emit(new_recv_bytes)
#                    #self.newData.emit(self.recv_bytes_list)    
                    
                    
#        except serial.SerialException:
#            print('serial has exception')
        

class ipcDataReader(QtCore.QObject):
    newdata = QtCore.pyqtSignal(object)
    finished = QtCore.pyqtSignal()
    CMD_Sent = False
    
    def __init__(self, parent=None):
        super().__init__(parent)
        
    
    def run(self):
        """long run task 1 to receive incoming data on ipc link"""
        running = True
        while True:
            #conn = self.listener.accept()
            try:
                conn = Client(('localhost', 6001), authkey=b'secret password')
                while conn.poll(.01):
                    mssg = conn.recv()
                    #print(mssg)
                    self.newdata.emit(mssg)
                               
                #QtCore.QThread.msleep(10)
            except:
                print('Exception happens')
                pass
            finally:
                conn.close()
            

        
# Console
        # Notes: 
        #       https://github.com/jupyter/qtconsole.git
        #       https://qtconsole.readthedocs.io/en/stable/
        #       https://qtconsole.readthedocs.io/en/stable/#embedding-the-qtconsole-in-a-qt-application

        #       https://stackoverflow.com/questions/59731016/non-blocking-ipython-qt-console-in-a-pyqt-application
        #       http://5.9.10.113/53804511/embedded-qtconsole-in-pyqt5-aplication-does-not-works-as-expected
        #       https://www.programcreek.com/python/example/114293/qtconsole.rich_jupyter_widget.RichJupyterWidget
        #       https://python.hotexamples.com/examples/qtconsole.rich_jupyter_widget/RichJupyterWidget/show/python-richjupyterwidget-show-method-examples.html
        #       https://python.hotexamples.com/examples/qtconsole.rich_jupyter_widget/RichJupyterWidget/kernel_client/python-richjupyterwidget-kernel_client-method-examples.html
        #       
        # Fun example
        #       %load https://matplotlib.org/stable/_downloads/3b9ac21ecf6a0b30550b0fb236dcec5a/custom_shaded_3d_surface.py
        #       https://matplotlib.org/stable/gallery/index.html

class ConsoleWidget(RichJupyterWidget):
    
    def __init__(self, customBanner=None, *args, **kwargs):
        """
            Start a kernel, connect to it, and create a RichJupyterWidget to use it
        """
    
        # Unexpected dependencies... Package adjustments needed to resolve
        # https://github.com/spyder-ide/spyder-notebook/issues/145
        # pip uninstall tornado; pip uninstall jupyter-client
        # pip install tornado==4.5.3; pip install jupyter-client==5.2.2
        # This will result in some "ERROR: pip's dependency resolver does not currently take into account..." messages but these don't seem to be an issue
        # We don't seem to have this dependency issue with the alternate method of using the RichJupyterWidget so we might want to revert.
    
        USE_KERNEL = 'python3'
        super(ConsoleWidget, self).__init__(*args, **kwargs)
        if customBanner is not None:
            self.banner = customBanner
        
        self.kernel_manager = kernel_manager = QtKernelManager(kernel_name=USE_KERNEL)
        kernel_manager.start_kernel()
        kernel_manager.kernel.gui = 'qt'
        
        self.kernel_client = kernel_client = self._kernel_manager.client()
        kernel_client.start_channels()
        return
    
    def push_vars(self, variableDict):
        """
        Given a dictionary containing name / value pairs, push those variables
        to the Jupyter console widget
        """
        self.kernel_manager.kernel.shell.push(variableDict)
    
    def clear(self):
        """
        Clears the terminal
        """
        self._control.clear()
        
        # self.kernel_manager
        
    def print_text(self, text):
        """
        Prints some plain text to the console
        """
        self._append_plain_text(text)
        
    def execute_command(self, command):
        """
        Execute a command in the frame of the console widget
        """
        self._execute(command, False)
        
        
        
        
class Ui_MainWindow(object):

    def gen_point_fades(self, num_pts): 
        # Called after the plotTotalCountEdit field is modified or on demand
        # Generates array of symbol transparency values to create aging trail effect
        n_points = num_pts
        num_max = 256
        num_min = 0
        seq_num = []
        seq_num = [int(num_min + i * (num_max-num_min)/num_pts)  for i in range(num_pts)] 
        for DUT in range(2):
            for mcu in range(3):
                col = self.mcu_colors[DUT][mcu] 
                r=QColor.red(col)
                g=QColor.green(col)
                b=QColor.blue(col)
                self.mcu_colors_arr[DUT][mcu] = [QColor(r, g, b, seq_num[i]) for i in range(num_pts)] # whatever color with decreasing transparency
                self.mcu_sizes_arr[DUT][mcu] = [0 + int(25*(seq_num[i]/256)) for i in range(num_pts)]

    def update_plotRateCount(self):
        global display_rate_sample_limit
        temp_str = self.plotRateCountEdit.text()
        if (not temp_str.isnumeric()):
            return()
        display_rate_sample_limit = int(temp_str)

    def update_plotTotalCount(self): # Called when number of sample points is modified... should be in passed in but is same as self.display_total_sample_count
        global display_total_sample_limit
        temp_str = self.plotTotalCountEdit.text()
        if (not temp_str.isnumeric()):
            return()
        num_pts=int(temp_str)
        display_total_sample_limit = num_pts
        self.gen_point_fades(num_pts) # regenerate symbol trails data

    def setupUi(self, MainWindow):

        # Index into each position array
        w_row=0
        w_col=1
        w_rowspan=2
        w_colspan=3

        # Widgit position and size   [row, col, rowspan, colspan]
        p_pw_polar                 = [  0,   0,       3,       5]
        p_pw_angle                 = [  0,   5,       1,       5]
        p_pw_dbfs                  = [  1,   5,       1,       5]
        p_console                  = [  2,   5,       1,       5]
        p_cmdLabel                 = [  3,   0]
        p_cmdComboBox              = [  3,   1,       1,       1]
        p_cmdSendBtn               = [  3,   2]                 
        p_resetDBFSButton          = [  3,   3]               
        p_MinMaxDBFSLabel          = [  3,   4]               
        p_mcuChkBox                = [  3,   5]               
        p_cmdStreamingBtn          = [  3,   8]               
        p_cmdStripchartBtn         = [  3,   9]               
        p_cmdRspLabel              = [  4,   0]
        p_cmdRspTxtLabel           = [  4,   1,       1,       4] 
        p_cmdClearPlotBtn          = [  4,   9]               
        p_plotRateCountTxtLabel    = [  4,   5]               
        p_plotRateCountEdit        = [  4,   6]               
        p_plotTotalCountTxtLabel   = [  4,   7] 
        p_plotTotalCountEdit       = [  4,   8] 
        p_streamLabel              = [  5,   0]
        p_streamTxtLabel           = [  5,   1,       1,       4] 
        p_IPCTestLabel             = [  5,   5]
        p_IPCTestTxtLabel          = [  5,   6,       1,       4] 

        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.setMinimumSize(QtCore.QSize(1800, 1000))
        
        global display_total_sample_limit
        global display_rate_sample_limit

        main_layout = pg.LayoutWidget()
        pg.setConfigOption('background', 'k')
        pg.setConfigOption('foreground', 'w')
        
        self.symbols = ['o', 't', 'p', 'h', 'star', '+', 'd', 'x']

        self.mcu_colors = [[QColor(Qt.red), QColor(Qt.green), QColor(Qt.blue)], [QColor(Qt.magenta), QColor(Qt.yellow), QColor(Qt.cyan)]]
        self.mcu_symbols = [['t3', 't1', 't'],['o', 's', '+']]
        
        self.mcu_colors_arr= [[[], [], []],[[], [], []]]
        self.mcu_sizes_arr= [[[], [], []],[[], [], []]]


        # Angle stripchart
        self.pw_angle = pg.PlotWidget(title="Angle")
        self.pw_angle_plot_item = self.pw_angle.getPlotItem()
        self.pw_angle_plot_item.addLegend()
        self.pw_angle_plot_item.showGrid(True, True)
        self.pw_angle_plot_item.vb.setYRange(-190, 190)
        self.curve_angle = [[[], [], []],[[], [], []]]
        for DUT in range(2 if A_B_Testing else 1):
            for mcu in range(3):
                self.curve_angle[DUT][mcu] = self.pw_angle.plot(pen=self.mcu_colors[DUT][mcu], symbol=self.mcu_symbols[DUT][mcu], name=('MCU' if DUT==0 else 'DUT') +'{0}'.format(mcu))
        self.pw_angle.setLabel('left', 'Angle', units='deg')
        self.pw_angle.setLabel('bottom', 'Sample', units='count')
        main_layout.addWidget(self.pw_angle, row=p_pw_angle[w_row], col=p_pw_angle[w_col], rowspan=p_pw_angle[w_rowspan], colspan=p_pw_angle[w_colspan])
        
        
        # DBFS stripchart
        self.pw_dbfs = pg.PlotWidget(title="DBFS")
        self.pw_dbfs_plot_item = self.pw_dbfs.getPlotItem()
        self.pw_dbfs_plot_item.addLegend()
        self.pw_dbfs_plot_item.showGrid(True, True)
        #self.pw_dbfs_plot_item.vb.setYRange(dbfs_min_init, dbfs_max_init)
        self.curve_dbfs =  [[[], [], []],[[], [], []]]
        for DUT in range(2 if A_B_Testing else 1):
            for mcu in range(3):
                self.curve_dbfs[DUT][mcu] = self.pw_dbfs.plot(pen=self.mcu_colors[DUT][mcu], symbol=self.mcu_symbols[DUT][mcu], name=('MCU' if DUT==0 else 'DUT') +'{0}'.format(mcu))
        self.pw_dbfs.setLabel('left', 'DBFS', units='')
        self.pw_dbfs.setLabel('bottom', 'Sample', units='count')
        main_layout.addWidget(self.pw_dbfs, row=p_pw_dbfs[w_row], col=p_pw_dbfs[w_col], rowspan=p_pw_dbfs[w_rowspan], colspan=p_pw_dbfs[w_colspan])


        
        
        self.console = ConsoleWidget()        
        main_layout.addWidget(self.console, row=p_console[w_row], col=p_console[w_col], rowspan=p_console[w_rowspan], colspan=p_console[w_colspan])
        
        #Polar Plot
        self.pw_polar = pg.PlotWidget(title="Polar Plot")
        self.pw_polar_plot_item = self.pw_polar.getPlotItem()
        self.pw_polar_plot_item.setAspectLocked()
        self.pw_polar_plot_item.addLegend()

        # X & Y Axis
        self.pw_polar_plot_item.addLine(x=0, pen=0.3)
        self.pw_polar_plot_item.addLine(y=0, pen=0.3)
        
        radius_range = 100 # 0 - 100% so normalize data
        for r in np.arange(0, radius_range, radius_range/20):
            circle = pg.QtGui.QGraphicsEllipseItem(-r, -r, r * 2, r * 2)
            circle.setPen(pg.mkPen(0.3))
            self.pw_polar_plot_item.addItem(circle)
        
        degree_symbol = u"\u00b0"
        deg0Text = pg.TextItem('0' + degree_symbol)
        self.pw_polar_plot_item.addItem(deg0Text)
        deg0Text.setPos(0, radius_range*1.05) 
        
        deg45Text = pg.TextItem('45' + degree_symbol)
        self.pw_polar_plot_item.addItem(deg45Text)
        deg45Text.setPos(int(radius_range*0.71), int(radius_range*0.71))
        
        deg90Text = pg.TextItem('90' + degree_symbol)
        self.pw_polar_plot_item.addItem(deg90Text)
        deg90Text.setPos(radius_range, 0)
        
        deg135Text = pg.TextItem('135' + degree_symbol)
        self.pw_polar_plot_item.addItem(deg135Text)
        deg135Text.setPos(int(radius_range*0.75), int(radius_range*-0.75))
        
        deg180Text = pg.TextItem('-/+180' + degree_symbol)
        self.pw_polar_plot_item.addItem(deg180Text)
        deg180Text.setPos(0, -radius_range)
        
        degn45Text = pg.TextItem('-45' + degree_symbol)
        self.pw_polar_plot_item.addItem(degn45Text)
        degn45Text.setPos(int(radius_range*-0.75), int(radius_range*0.75))
        
        degn90Text = pg.TextItem('-90' + degree_symbol)
        self.pw_polar_plot_item.addItem(degn90Text)
        degn90Text.setPos(int(radius_range*-1.1), 0)
        
        degn135Text = pg.TextItem('-135' + degree_symbol)
        self.pw_polar_plot_item.addItem(degn135Text)
        degn135Text.setPos(int(radius_range*-0.75), int(radius_range*-0.75))
        
                
        dLine_1 = pg.InfiniteLine(angle=45, movable=False, pen=0.3)
        dLine_2 = pg.InfiniteLine(angle=135, movable=False, pen=0.3)
        
        self.pw_polar_plot_item.addItem(dLine_1)
        self.pw_polar_plot_item.addItem(dLine_2) 
        
        self.curve_polar_angle = [[[], [], []], [[], [], []]]

        for DUT in range(2 if A_B_Testing else 1):
            for mcu in range(3):
                #self.curve_polar_angle[DUT][mcu] = self.pw_polar.plot(pen=self.mcu_color[(DUT*3)+mcu] , symbol=mcu_symbol[(DUT*3)+mcu], name=('MCU' if DUT==0 else 'DUT') +'{0}'.format(mcu))
                self.curve_polar_angle[DUT][mcu] = pg.ScatterPlotItem(pen=self.mcu_colors[DUT][mcu], symbol=self.mcu_symbols[DUT][mcu], brush=pg.mkBrush(self.mcu_colors[DUT][mcu]), name=('MCU' if DUT==0 else 'DUT') +'{0}'.format(mcu))
                self.pw_polar.addItem(self.curve_polar_angle[DUT][mcu])

        main_layout.addWidget(self.pw_polar, row=p_pw_polar[w_row], col=p_pw_polar[w_col], rowspan=p_pw_polar[w_rowspan], colspan=p_pw_polar[w_colspan])
        
        #newfont = QtGui.QFont("Times", 10, QtGui.QFont.Bold)
        
        self.CmdLabel = QLabel()
        #self.cmdTextLabel.setFont(newfont)
        self.CmdLabel.setText('Command:')
        self.CmdLabel.setMaximumWidth(120)
        self.CmdLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.CmdLabel, row=p_cmdLabel[w_row], col=p_cmdLabel[w_col])
        
        self.cmdComboBox = QComboBox()
        self.cmdComboBox.setEditable(True)

        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"FOV_Angle", "VAL":22.5}')
        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"FOV_Angle", "VAL":45}')
        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"FOV_Angle", "VAL":90}')
        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"FOV_Angle", "VAL":135}')
        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"FOV_Angle", "VAL":180}')
        self.cmdComboBox.addItem('{"CMD":"GET", "VAR":"FOV_Angle"}')

        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"Autofocus_Adjust_Rate", "VAL":100}')
        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"Autofocus_Adjust_Rate", "VAL":0}')
        self.cmdComboBox.addItem('{"CMD":"GET", "VAR":"Autofocus_Adjust_Rate"}')

        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"AGC_Adjust_Rate", "VAL":100}')
        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"AGC_Adjust_Rate", "VAL":0}')
        self.cmdComboBox.addItem('{"CMD":"GET", "VAR":"AGC_Adjust_Rate"}')

        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"APU_Gain", "VAL":0.025}')
        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"APU_Gain", "VAL":1.0}')
        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"APU_Gain", "VAL":2.0}')
        self.cmdComboBox.addItem('{"CMD":"GET", "VAR":"APU_Gain"}')

        self.cmdComboBox.addItem('{"CMDENV":"SET","VAR":"gen_polarplot_data", "VAL":1}')
        self.cmdComboBox.addItem('{"CMDENV":"SET","VAR":"gen_polarplot_data", "VAL":0}')
        self.cmdComboBox.addItem('{"CMDENV":"SET","VAR":"gen_stripchart_data", "VAL":1}')
        self.cmdComboBox.addItem('{"CMDENV":"SET","VAR":"gen_stripchart_data", "VAL":0}')
        
        self.cmdComboBox.addItem('{"CMD":"SET", "VAR":"Enable_DUT_Test", "VAL":1}')
        
        self.cmdComboBox.addItem('{"CMD":"PWR", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"PWR", "MCU":2, "STATE":"ENABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"RST"}')
        
        self.cmdComboBox.addItem('{"CMD":"RBC"}')
        self.cmdComboBox.addItem('{"CMD":"RBV"}')
        
        self.cmdComboBox.addItem('{"CMD":"RFV"}')
        
        self.cmdComboBox.addItem('{"CMD":"HEN", "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"HEN", "STATE":"DISABLE"}')

        
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":2, "STATE":"DISABLE"}')
        
        main_layout.addWidget(self.cmdComboBox, row=p_cmdComboBox[w_row], col=p_cmdComboBox[w_col], rowspan=p_cmdComboBox[w_rowspan], colspan=p_cmdComboBox[w_colspan])
        
        self.cmdSendBtn = QPushButton()
        self.cmdSendBtn.setText('Send Command')
        main_layout.addWidget(self.cmdSendBtn, row=p_cmdSendBtn[w_row], col=p_cmdSendBtn[w_col])
        
        self.resetDBFSButton = QPushButton()
        self.resetDBFSButton.setText('Reset DBFS Limits')
        main_layout.addWidget(self.resetDBFSButton, row=p_resetDBFSButton[w_row], col=p_resetDBFSButton[w_col])
        
        self.MinMaxDBFSLabel = QLabel()        
        main_layout.addWidget(self.MinMaxDBFSLabel, row=p_MinMaxDBFSLabel[w_row], col=p_MinMaxDBFSLabel[w_col])
        
        self.cmdStreamingBtn = QPushButton()
        self.cmdStreamingBtn.setText('Start Streaming')
        main_layout.addWidget(self.cmdStreamingBtn, row=p_cmdStreamingBtn[w_row], col=p_cmdStreamingBtn[w_col])
        
        self.cmdStripchartBtn = QPushButton()
        self.cmdStripchartBtn .setText('Enable Stripcharts')
        main_layout.addWidget(self.cmdStripchartBtn, row=p_cmdStripchartBtn[w_row], col=p_cmdStripchartBtn[w_col])
        
        self.cmdClearPlotBtn = QPushButton()
        self.cmdClearPlotBtn.setText('Reset Capture Buffer')
        main_layout.addWidget(self.cmdClearPlotBtn, row=p_cmdClearPlotBtn[w_row], col=p_cmdClearPlotBtn[w_col])
        
        # MCU enable checkboxes...
        self.mcuChkBox = [0,0,0]
        for mcu in range(3):
            self.mcuChkBox[mcu] = QCheckBox()
            self.mcuChkBox[mcu].setText('MCU{0}'.format(mcu))
            self.mcuChkBox[mcu].setChecked(mcu==0)
            main_layout.addWidget(self.mcuChkBox[mcu], row=p_mcuChkBox[w_row], col=p_mcuChkBox[w_col]+mcu)
        
        # JSON Command Responses
        self.CmdRspLabel = QLabel()
        self.CmdRspLabel.setText('Response:')
        self.CmdRspLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.CmdRspLabel, row=p_cmdRspLabel[w_row], col=p_cmdRspLabel[w_col])
        
        self.cmdRspTxtLabel = QLabel()
        self.cmdRspTxtLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.cmdRspTxtLabel.setFont(QtGui.QFont('Times', 9))
        self.cmdRspTxtLabel.setWordWrap(True)
        main_layout.addWidget(self.cmdRspTxtLabel, row=p_cmdRspTxtLabel[w_row], col=p_cmdRspTxtLabel[w_col], rowspan=p_cmdRspTxtLabel[w_rowspan], colspan=p_cmdRspTxtLabel[w_colspan])
        
        # JSON Data Stream
        self.streamLabel = QLabel()
        self.streamLabel.setText('Data Stream:')
        self.streamLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.streamLabel, row=p_streamLabel[w_row], col=p_streamLabel[w_col])
        
        self.streamTxtLabel = QLabel()
        self.streamTxtLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.streamTxtLabel.setFont(QtGui.QFont('Arial', 9))
        self.streamTxtLabel.setWordWrap(True)
        main_layout.addWidget(self.streamTxtLabel, row=p_streamTxtLabel[w_row], col=p_streamTxtLabel[w_col], rowspan=p_streamTxtLabel[w_rowspan], colspan=p_streamTxtLabel[w_colspan])
        
        # IPC Test
        self.IPCTestLabel = QLabel()
        self.IPCTestLabel.setText('IPC Test:')
        self.IPCTestLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.IPCTestLabel, row=p_IPCTestLabel[w_row], col=p_IPCTestLabel[w_col])
        
        self.IPCTestTxtLabel = QLabel()
        self.IPCTestTxtLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.IPCTestTxtLabel.setFont(QtGui.QFont('Arial', 10))
        main_layout.addWidget(self.IPCTestTxtLabel, row=p_IPCTestTxtLabel[w_row], col=p_IPCTestTxtLabel[w_col], rowspan=p_IPCTestTxtLabel[w_rowspan], colspan=p_IPCTestTxtLabel[w_colspan])

        # Plot update rate
        self.plotRateCountTxtLabel = QLabel()
        self.plotRateCountTxtLabel.setText('Plot Update Interval:')
        self.plotRateCountTxtLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.plotRateCountTxtLabel, row=p_plotRateCountTxtLabel[w_row], col=p_plotRateCountTxtLabel[w_col])
        
        self.plotRateCountEdit = QLineEdit()
        self.plotRateCountEdit.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.plotRateCountEdit.setMaximumWidth(120)
        self.plotRateCountEdit.setValidator(QIntValidator(1, 50))
        self.plotRateCountEdit.editingFinished.connect(self.update_plotRateCount)
        main_layout.addWidget(self.plotRateCountEdit, row=p_plotRateCountEdit[w_row], col=p_plotRateCountEdit[w_col])
        
        # Total sample count entry
        self.plotTotalCountTxtLabel = QLabel()
        self.plotTotalCountTxtLabel.setText('# Samples:')
        self.plotTotalCountTxtLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.plotTotalCountTxtLabel, row=p_plotTotalCountTxtLabel[w_row], col=p_plotTotalCountTxtLabel[w_col])
        
        self.plotTotalCountEdit = QLineEdit()
        self.plotTotalCountEdit.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.plotTotalCountEdit.setMaximumWidth(120)
        self.plotTotalCountEdit.setValidator(QIntValidator(1, 50))
        self.plotTotalCountEdit.editingFinished.connect(self.update_plotTotalCount)
        main_layout.addWidget(self.plotTotalCountEdit, row=p_plotTotalCountEdit[w_row], col=p_plotTotalCountEdit[w_col])
        
        MainWindow.setCentralWidget(main_layout)
        
        
        
class ApplicationWindow(QtGui.QMainWindow):
    
    def __init__(self):
        global display_total_sample_limit
        global display_rate_sample_limit
        global display_rate_sample_counter
        QtGui.QMainWindow.__init__(self)
        

        
#        self.mcu_list = []
#        self.curve_angle_arr = [[],[]]
#        self.curve_dbfs_arr = [[],[]]
#        self.angle_list = [[],[]]
#        self.dbfs_list = [[],[]]
        self.mcu_angle_list = [[[], [], []],[[], [], []]]
        self.mcu_dbfs_list = [[[], [], []],[[], [], []]]

        self.dbfs_min = dbfs_min_init
        self.dbfs_max = dbfs_max_init


        self.mcu_polar_x_list = [ [[],[],[]], [[],[],[]] ]
        self.mcu_polar_y_list = [ [[],[],[]], [[],[],[]] ]
        
        self.recv_new_xy_count = 0
#        display_rate_sample_counter = 0


        
#        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
#        self.setStyleSheet("QMainWindow {background: 'lightGray';}")
        
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.plotRateCountEdit.setText(str(display_rate_sample_limit))
        self.ui.plotTotalCountEdit.setText(str(display_total_sample_limit))
        self.ui.MinMaxDBFSLabel.setText("MIN:" + str(self.dbfs_min) + " : " + "MAX:" + str(self.dbfs_max))

        self.ui.cmdSendBtn.clicked.connect(self.cmdSendBtnClicked)
        self.ui.cmdStreamingBtn.clicked.connect(self.cmdStreamingBtnClicked)
        self.ui.cmdStripchartBtn.clicked.connect(self.cmdStripchartBtnClicked)

        self.ui.cmdClearPlotBtn.clicked.connect(self.cmdClearPlotBtnClicked)
        self.ui.resetDBFSButton.clicked.connect(self.resetDBFSButtonClicked)

        for mcu in range(3):
            self.ui.mcuChkBox[mcu].toggled.connect(self.mcuSelectEnableBtnClicked)

        # initial generation of the fade arrays
        self.ui.gen_point_fades(display_total_sample_limit) 

        
        
#        """Long-running task in 5 steps."""
#        # Step 2: Create a QThread object
#        self.thread_read = QtCore.QThread()
#        # Step 3: Create a worker object
#        self.reader = DataReader(self.serial_instance)
#        # Step 4: Move worker to the thread
#        self.reader.moveToThread(self.thread_read)
#        # Step 5: Connect signals and slots
#        self.thread_read.started.connect(self.reader.run)
#        self.reader.finished.connect(self.thread_read.quit)
#        self.reader.finished.connect(self.reader.deleteLater)
#        self.thread_read.finished.connect(self.thread_read.deleteLater)
#        self.reader.newData.connect(self.processRecvData)
#        # Step 6: Start the thread
#        self.thread_read.start()

        
#        # https://realpython.com/python-pyqt-qthread/
#        """IPC Handler"""
        # create a qthread object
        self.thread_listen = QtCore.QThread()
        # create a worker object
        self.listener = ipcDataReader()
        # move worker to the thread
        self.listener.moveToThread(self.thread_listen)
        # connect signals and slots
        self.thread_listen.started.connect(self.listener.run)
        self.listener.finished.connect(self.thread_listen.quit)
        self.listener.finished.connect(self.listener.deleteLater)
        self.thread_listen.finished.connect(self.thread_listen.deleteLater)
        self.listener.newdata.connect(self.processRecvData) # what should this be?
        # step 6: start the thread
        self.thread_listen.start()
        
#        self.MSG_FIFO = Queue()
#        #self.MSG_FIFO = queue.SimpleQueue()

## updateCharts
#        """updateCharts Handler"""
#        ## create a qthread object
#        #self.thread_listen = QtCore.QThread()
#        ## create a worker object
#        #self.ChartUpdater = updateCharts()
#        ## move worker to the thread
#        #self.listener.moveToThread(self.thread_listen)
#        ## connect signals and slots
#        #self.thread_listen.started.connect(self.listener.run)
#        #self.listener.finished.connect(self.thread_listen.quit)
#        #self.listener.finished.connect(self.listener.deleteLater)
#        #self.thread_listen.finished.connect(self.thread_listen.deleteLater)
#        #self.listener.newdata.connect(self.listener.run) # what should this be?
#        ## step 6: start the thread
#        #self.thread_listen.start()


       
#    def updateDBFSlimits(self, dbfs):
#        global display_total_sample_limit

#        if (dbfs < self.dbfs_min):
#            self.dbfs_min = dbfs
#        if (dbfs > self.dbfs_max):
#            self.dbfs_max = dbfs
#        self.ui.MinMaxDBFSLabel.setText("MIN:" + str(self.dbfs_min) + " : " + "MAX:" + str(self.dbfs_max))
#        self.ui.pw_dbfs_plot_item.vb.setYRange(self.dbfs_min, self.dbfs_max)

    def processRecvData(self, var):
        #received_str = copy(var)
        global stripchart_enabled
        header, msg = var
        #print(var)
        if 'Polar x' in header:
            self.mcu_polar_x_list = msg
            self.recv_new_xy_count = self.recv_new_xy_count + 1
        elif 'Polar y' in header:
            self.mcu_polar_y_list = msg
            self.recv_new_xy_count = self.recv_new_xy_count + 1
        elif 'Angles' in header:
            self.mcu_angle_list = msg
        elif 'DBFS' in header:
            self.mcu_dbfs_list = msg
            
        
        #self.ui.IPCTestTxtLabel.setText(str(msg[0]))
        #self.ui.IPCTestTxtLabel.setText(str(self.mcu_polar_x_list))
        #print(self.mcu_polar_x_list)
        #print(self.mcu_polar_y_list)
              
              
        if (stripchart_enabled):
            for DUT in range(2 if A_B_Testing else 1):
                for mcu in range(3):
                    self.ui.curve_angle[DUT][mcu].setData(self.mcu_angle_list[DUT][mcu])
                    self.ui.curve_dbfs[DUT][mcu].setData(self.mcu_dbfs_list[DUT][mcu])      
                self.update_strip_chart_Axis()
              
        if self.recv_new_xy_count == 2:      
            for DUT in range(2 if A_B_Testing else 1):
                for mcu in range(3):                
                #self.ui.streamTxtLabel.setText(str(var))
                  
                    if len(self.mcu_polar_x_list[DUT][mcu]) != len(self.mcu_polar_y_list[DUT][mcu]):
                        print('DUT:{}, mcu:{} size mismatch!'.format(DUT, mcu))
                    self.ui.curve_polar_angle[DUT][mcu].setData(self.mcu_polar_x_list[DUT][mcu], self.mcu_polar_y_list[DUT][mcu])
                    # Assign symbol transparency values and colors based on data set and position to create aging trail effect
                    pw_polar_plot_item = self.ui.pw_polar.getPlotItem()
                    plot_data = pw_polar_plot_item.listDataItems()
                    num_pts = plot_data[DUT*3+mcu].data.size
                    if num_pts > 0:
                        plot_data[DUT*3+mcu].setBrush(self.ui.mcu_colors_arr[DUT][mcu][-num_pts:])
                        plot_data[DUT*3+mcu].setPen(self.ui.mcu_colors_arr[DUT][mcu][-num_pts:])
                        plot_data[DUT*3+mcu].setSize(self.ui.mcu_sizes_arr[DUT][mcu][-num_pts:])
                    
            
            self.recv_new_xy_count = 0
            
            
                
                
            
            
            
        



#    def updateCharts(self, json_dict): # pass JSON object?
#        """ Function running in own thread to maintain data for charts """
#        global streaming_enabled
#        global display_total_sample_limit, display_rate_sample_limit, display_rate_sample_counter

#        self.ui.streamTxtLabel.setText(self.Data_Stream_Mssg)

#        # The current JSON mssg should be tagged with a flag to indicate it's type or just parse teeh result into a DICT item with that identified 
#        # Each message object can be then be pushed into a FIFO and popped by another thread that is handling the following steps
#        # The other thread processes data while the FIFO is not empty
#        # FIFO entries could include datetime stamp as well as enum status [PULD_RSP, PULD_DTA, PULD_ERR] where PULD_DTA is 
#        # an angle/dbfs stream DATA record and PULD_ERR is an invalid/malformed record 
#        # Could have resettable counters for various statistics including the number of responses for each type of command (at least an ERR counter)
        
#        display_rate_sample_counter += 1 # regulates the rate at which the chart is updated
#        self.mcu = json_dict["MCU"]
#        self.mcu_list.append(self.mcu) # MCU's are also referenced for each data point so they can be split up when plotted
#        self.mcu_list = self.mcu_list[-display_total_sample_limit:] # Truncate to display_total_sample_count points
#        self.mcu_arr = np.array(self.mcu_list) # matching numpy array for masking
#        json_keys = json_dict.keys()
#        Found_DUT_Data = True # May not need this
#        for DUT in range(2 if A_B_Testing else 1):
#            if ((DUT==1) & (not "ANGLE_DUT" in json_keys)): # not streaming DUT data so exit inner loop and get next JSON mssg
#                Found_DUT_Data = False
#                break 
#            self.angle = json_dict["ANGLE_DUT"] if DUT==1 else json_dict["ANGLE"]
#            self.dbfs = json_dict["DBFS_DUT"] if DUT==1 else json_dict["DBFS"]
#            self.updateDBFSlimits(self.dbfs)

#            if (self.dbfs_max == self.dbfs_min):
#                signal_strength = 100
#            else:
#                signal_strength = (self.dbfs - self.dbfs_min)/(self.dbfs_max - self.dbfs_min) * 100

#            # Add new Angle & DBFS values to list
#            self.angle_list[DUT].append(self.angle)
#            self.dbfs_list[DUT].append(self.dbfs)
#            # Truncate to max display_total_sample_count points
#            self.angle_list[DUT] = self.angle_list[DUT][-display_total_sample_limit:]
#            self.dbfs_list[DUT] = self.dbfs_list[DUT][-display_total_sample_limit:]

#            # Calculate X & Y coords of current polar plot point...
#            polar_x = signal_strength * np.sin(self.angle / 180.0 * np.pi)
#            polar_y = signal_strength * np.cos(self.angle / 180.0 * np.pi)
#            # Add to lists...
#            self.polar_x_list[DUT].append(polar_x)
#            self.polar_y_list[DUT].append(polar_y)
#            # Truncate to display_total_sample_count points
#            self.polar_x_list[DUT] = self.polar_x_list[DUT][-display_total_sample_limit:]
#            self.polar_y_list[DUT] = self.polar_y_list[DUT][-display_total_sample_limit:]

#            if (display_rate_sample_counter % display_rate_sample_limit == 0):
#                display_rate_sample_counter = 0
#                if (stripchart_enabled):
#                    self.curve_angle_arr[DUT] = np.array(self.angle_list[DUT])
#                    self.curve_dbfs_arr[DUT] = np.array(self.dbfs_list[DUT])
#                    list_len = len(self.mcu_list)
#                    # Option to make next steps quicker by indexing with mcu_arr... 
#                    #    PUSH onto end of current [DUT][mcu] array
#                    #    POP from head of mcu array pointed by previous head (maybe do the mcu_arr updated after)
#                    #    ISSUE: mcu_####_list's are only updated when we are in this loop so we at least have to do all PUSH & POP steps
#                    for mcu in range(3):
#                        self.mcu_match = np.array(self.mcu_arr==mcu) # boolean array indicating MCU# match positions
#                        self.mcu_match = self.mcu_match[0:len(self.curve_angle_arr[DUT])] # adjustment while DUT array is added in and gets sized
#                        self.mcu_angle_list[DUT][mcu] = self.curve_angle_arr[DUT][self.mcu_match]
#                        self.mcu_dbfs_list[DUT][mcu] = self.curve_dbfs_arr[DUT][self.mcu_match]

#                        # Update stripchart data sets (conditional when rate counter is fired)
#                        self.ui.curve_angle[DUT][mcu].setData(self.mcu_angle_list[DUT][mcu])
#                        self.ui.curve_dbfs[DUT][mcu].setData(self.mcu_dbfs_list[DUT][mcu])
    
#                self.polar_x_arr[DUT] = np.array(self.polar_x_list[DUT])
#                self.polar_y_arr[DUT] = np.array(self.polar_y_list[DUT])
#                for mcu in range(3):
#                    self.mcu_match = np.array(self.mcu_arr==mcu) # boolean array indicating MCU# match positions
#                    self.mcu_match = self.mcu_match[0:len(self.polar_x_arr[DUT])] # adjustment while DUT array is added in and gets sized
#                    self.mcu_polar_x_list[DUT][mcu] = self.polar_x_arr[DUT][self.mcu_match] # extract X data for this MCU
#                    self.mcu_polar_y_list[DUT][mcu] = self.polar_y_arr[DUT][self.mcu_match] # and Y
#                    self.ui.curve_polar_angle[DUT][mcu].setData(self.mcu_polar_x_list[DUT][mcu], self.mcu_polar_y_list[DUT][mcu]) # update trace data

#                    # Assign symbol transparency values and colors based on data set and position to create aging trail effect
#                    pw_polar_plot_item = self.ui.pw_polar.getPlotItem()
#                    plot_data = pw_polar_plot_item.listDataItems()
#                    num_pts = plot_data[DUT*3+mcu].data.size
#                    if num_pts>0:
#                        plot_data[DUT*3+mcu].setBrush(self.ui.mcu_colors_arr[DUT][mcu][-num_pts:])
#                        plot_data[DUT*3+mcu].setPen(self.ui.mcu_colors_arr[DUT][mcu][-num_pts:])

#                        plot_data[DUT*3+mcu].setSize(self.ui.mcu_sizes_arr[DUT][mcu][-num_pts:])
#                        # https://pyqtgraph.readthedocs.io/en/latest/graphicsItems/plotitem.html

#                self.update_strip_chart_Axis()

         
                
    def cmdClearPlotBtnClicked(self):
        """Handle the click of Clear button """
        self.mcu_list = []
        self.angle_list = [[], []]
        self.dbfs_list = [[], []]

        self.polar_x_list = [[],[]]
        self.polar_y_list = [[],[]]
        
        self.mcu_polar_x_list = [ [[],[],[]], [[],[],[]] ]
        self.mcu_polar_y_list = [ [[],[],[]], [[],[],[]] ]
        self.mcu_angle_list = [[[], [], []],[[], [], []]]
        self.mcu_dbfs_list = [[[], [], []],[[], [], []]]
        
        for DUT in range(2):
            for mcu in range(3):
                self.ui.curve_polar_angle[DUT][mcu].setData(self.mcu_polar_x_list[DUT][mcu], self.mcu_polar_y_list[DUT][mcu])
                self.ui.curve_angle[DUT][mcu].setData(self.mcu_angle_list[DUT][mcu])
                self.ui.curve_dbfs[DUT][mcu].setData(self.mcu_dbfs_list[DUT][mcu])
        
    def mcuSelectEnableBtnClicked(self):
        """Handle the click of 3 MCU radio buttons """
        global streaming_enabled
        EnableButton = self.sender()        
        isChecked = EnableButton.isChecked()

        mcu_num = EnableButton.text()
        mcu_num = int(mcu_num[-1])
        # if streaming is enabled then send HEN with appropriate state
        if streaming_enabled == True:
            self.sendCMD("HEN", mcu_num, isChecked)

    def resetDBFSButtonClicked(self):
        self.dbfs_min = dbfs_min_init
        self.dbfs_max = dbfs_max_init
        self.ui.MinMaxDBFSLabel.setText("MIN:" + str(self.dbfs_min) + " : " + "MAX:" + str(self.dbfs_max)) # handled by function?
        
        
    def cmdSendBtnClicked(self):
        """handler of clicking the one-time send button"""
        self.recv_bytes = []
        current_command = str(self.ui.cmdComboBox.currentText())
        if current_command != "":
            resp = self.sendCmdStr(current_command)
            self.ui.cmdRspTxtLabel.setText(str(resp))
            
            
    def sendCmdStr(self, cmd):
        """send command string directly """
        conn = Client(('localhost', 6000), authkey=b'secret password')
        conn.send(cmd)
        if conn.poll(.75):
            rsp = conn.recv()
        else:
            rsp = '##NO_RSP##'
        conn.close()
        return rsp
        
            
    def sendCMD(self, cmd, mcu=-1, state=""):
        has_mcu = 0
        if (mcu!=-1):
            mcu=int(mcu)
            if (mcu in range(4)):
                has_mcu = 1
                
        has_state = 0
        if (state!=""):
            if (state==1):
                state = "ENABLE"
                has_state = 1
            elif (state==0):
                state = "DISABLE"
                has_state = 1
        
        cmd_part1= '{{\"CMD\":\"{0}\"'.format(cmd)
        cmd_part2= ', \"MCU\":{0}'.format(str(mcu)) if has_mcu else ''
        cmd_part3= ', \"STATE\":\"{0}\"'.format(state) if has_state else ''
        cmd_part4='}'
        current_command = cmd_part1 + cmd_part2 + cmd_part3 + cmd_part4
        rsp = self.sendCmdStr(current_command)
        return rsp


    def update_strip_chart_Axis(self):
        global display_total_sample_limit
        # update chart axis with latest sample count limits
        # https://stackoverflow.com/questions/41078849/pyqtgraph-how-to-set-intervals-of-axis
        for widget in [self.ui.pw_angle, self.ui.pw_dbfs]:
            temp_plot_item = widget.getPlotItem()
            ax = temp_plot_item.getAxis('bottom')
            dx = [(value, str(value)) for value in list(range(0, display_total_sample_limit+1, int(round((display_total_sample_limit+15)/30,0))))]
            ax.setTicks([dx, []])


    def cmdStreamingBtnClicked(self):
        global streaming_enabled
        current_command = None
        if streaming_enabled == True:
            streaming_enabled = False
            self.ui.cmdStreamingBtn.setText('Start Streaming')
            rsp = self.sendCMD("HEN", 3, False)
            self.ui.cmdRspTxtLabel.setText(str(rsp))
            
            
        else:
            streaming_enabled = True
            self.ui.cmdStreamingBtn.setText('Stop Streaming')
            for mcu in range(3):
                if (self.ui.mcuChkBox[mcu].isChecked() == True):
                    rsp = self.sendCMD("HEN", mcu, True)

    def cmdStripchartBtnClicked(self):
        global stripchart_enabled
        current_command = None
        if stripchart_enabled == True:
            stripchart_enabled = False
            cmd = '{"CMDENV":"SET","VAR":"gen_stripchart_data", "VAL":0}'
            self.sendCmdStr(cmd)
            self.ui.cmdStripchartBtn.setText('Enable Stripcharts')
        elif stripchart_enabled == False:
            stripchart_enabled = True
            cmd = '{"CMDENV":"SET","VAR":"gen_stripchart_data", "VAL":1}'
            self.sendCmdStr(cmd)
            self.ui.cmdStripchartBtn.setText('Disable Stripcharts')


    def setupMCUs(self):
       #self.sendCMD("RST")
       time.sleep(.5)
       self.sendCMD("PWR", 1, True)  # Enable slave #1
       time.sleep(.5)
       self.sendCMD("PWR", 2, True)  # Enable slave #2
       time.sleep(.5)
       self.sendCMD("HEN", 3, False) # Disable streaming
            
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    #PS = ps.PULD_Server()
    main = ApplicationWindow()
    main.resize(1200, 700)
    main.setWindowTitle('PULD Streaming Data Monitor')
    main.setupMCUs()
    main.show()
    sys.exit(app.exec_())
