# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 12:01:57 2020

This script is to display I2S data, Soft and Hard FFT data into figures. 

@author: Bruce Wu / Scott Hurst
"""

# main.py
import sys

# Args #1: filename
desc = ""
fname = "MCU_Data.log" # default
title = ""
if __name__ == "__main__":
#    print(f"Arguments count: {len(sys.argv)}")
    for i, arg in enumerate(sys.argv):
#        print(f"Argument {i:>6}: {arg}")
        if i == 1:
            fname = arg
            if fname == "FILE":
                fname = ""
#            else:
 #               print(f"Data File Name: {fname}")
        if i == 2:
            desc = arg
#            print(f"Description: {desc}") # deprecated

#print(f"Arguments count: {len(sys.argv)}")

#import tkinter as tk
#from tkinter import filedialog
#if fname=="":
#    root = tk.Tk()
#    root.withdraw()
#    file_path = filedialog.askopenfilename()

import numpy as np
import matplotlib.pyplot as plt
import mplcursors as mplcursor
# https://mplcursors.readthedocs.io/en/stable/examples/

import ctypes  # An included library with Python install.
def Mbox(title, text, style):
    return ctypes.windll.user32.MessageBoxW(0, text, title, style)
result=Mbox('Charting Help', 'Module mplcursors Installed\n\nLeft-Click:\tAdd Tag\nRight-CLick:\tRemove Tag\nMiddle-CLick:\tDrag Tag\n\n   h: Toggle viewing of data point labels\n   e: Toggle cursors ON/OFF ', 1)


import os
if result==2: # cancel
    sys.exit(0)


from matplotlib.widgets import Slider, Button, RadioButtons
# https://matplotlib.org/api/widgets_api.html
# https://www.programcreek.com/python/example/108372/matplotlib.widgets.SpanSelector
# https://matplotlib.org/3.3.1/gallery/widgets/span_selector.html
# https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.pyplot.subplots.html

cwd = os.getcwd()
print("Current Working Directory =", cwd)

#from scipy.fft import fft

# number of sample points
#N = 256

# sample spacing
#T = 1.0 / 256.0

#x = np.linspace(0.0, N*T, N)

I2S_FS   = 253906

# Begin time period of the signals

beginTime           = 0
endTime             = 10

# Time points

#time        = np.arange(beginTime, endTime, samplingInterval)
 

mic_i2s = [[[], [], [], [], [], [], [], []]]
mic_i2s_i_max = [[]]
mic_i2s_max = []
#mic_i2s = np.array([[[], [], [], [], [], [], [], []]])
#mic_i2s_i_max = np.array([[]])
#mic_i2s_max = np.array([])


mic_soft_fft = []
mic_hard_fft = []

# to hold the calculated FFT using numpy
mic_calc_fft = [[], [], [], [], [], [], [], []]

calc_fft = [[], [], [], [], [], [], [], []]



DESCRIPTION = ''
DATE = ''
TIME = ''
SOURCE = ''
I2S_FS = ''
SIGNAL_PATH = ''
TRIAL_DONE = 0

found_i2s = 0
found_soft_fft = 0
found_hard_fft = 0

i2s_row_number = 0
sft_row_number = 0
hft_row_number = 0

max_i2s_val = 0;

#id_str = input('Enter the ID number of data: ')
id_str = "1" # just working with one run trials
trial_id=int(id_str)
#print(id_str)
#id_num = int(id_str)

#os.system("pause")
NORMALIZE_I2S_DATA = 0
# Open the file with read only permit
f = open(fname)
# use readline() to read the first line 
line = f.readline()
# use the read line to read further.
# If the file is not empty keep reading one line
# at a time, till the file is empty
while line:
    # in python 2+
    # print line
    # in python 3 print is a builtin function, so
    if (line.find('DESCRIPTION:') != -1):
        colon_index = line.index(':')
        DESCRIPTION= line[colon_index+1:].strip()

    if (line.find('DATE:') != -1):
        colon_index = line.index(':')
        DATE = line[colon_index+1:].strip()
        
    if (line.find('TIME:') != -1):
        colon_index = line.index(':')
        TIME = line[colon_index+1:].strip()

    if (line.find('SOURCE:') != -1):
        colon_index = line.index(':')
        SOURCE= line[colon_index+1:].strip()

    if (line.find('SIGNAL PATH:') != -1):
        colon_index = line.index(':')
        SIGNAL_PATH= line[colon_index+1:].strip()

    if (line.find('I2S BUFFER SIZE:') != -1):
        colon_index = line.index(':')
        I2S_BUFFER_SIZE= int(line[colon_index+1:].strip())

    if (line.find('I2S PLOT SIZE:') != -1):
        colon_index = line.index(':')
        I2S_PLOT_SIZE= int(line[colon_index+1:].strip())

    if (line.find('NORMALIZE I2S DATA:') != -1):
        colon_index = line.index(':')
        NORMALIZE_I2S_DATA = int(line[colon_index+1:].strip())

    if (line.find('I2S_FS') != -1):
        colon_index = line.index(':')
        I2S_FS = float(line[colon_index+1:])

    if (line.find('##TRIAL DONE##') != -1):
        TRIAL_DONE = 1
        
    
    if (line.find('I2S[') != -1):
        found_i2s = 1
        found_hard_fft = 0
        found_soft_fft = 0
        i2s_row_number = 0
        sft_row_number = 0
        hft_row_number = 0
        TRIAL_DONE     = 0

    if (line.find('SFT ') != -1) or (line.find('HFT ') != -1):
        found_i2s = 0
        found_hard_fft = 0
        found_soft_fft = 0
        i2s_row_number = 0
        sft_row_number = 0
        hft_row_number = 0
        TRIAL_DONE     = 0
    
    if (line.find('SFT[') != -1):
        found_i2s = 0
        found_hard_fft = 0
        found_soft_fft = 1
        i2s_row_number = 0
        sft_row_number = 0
        hft_row_number = 0
        TRIAL_DONE     = 0
        
    if (line.find('HFT[') != -1):
        found_i2s = 0
        found_hard_fft = 1
        found_soft_fft = 0
        i2s_row_number = 0
        sft_row_number = 0
        hft_row_number = 0
        TRIAL_DONE     = 0
        
    if (found_i2s == 1):
        if ((line.startswith('ID')) == 0) & (TRIAL_DONE == 0):
            i2s_row_number = i2s_row_number + 1   
            a = line.replace('\t', ' ')
            b = a.split() 
            for i in range(0, 8):
                b[i+2] = int(b[i+2])
            if int(b[1])==0: # start of new data set
                trial_id = int(b[0])
#                mic_i2s_i_max = np.append(mic_i2s_i_max, [np.zeros(8)])
#                mic_i2s_i_max[trial_id] = np.append(mic_i2s_i_max[trial_id], [np.zeros(8)]) # array to hold largest value in each data set
                mic_i2s_i_max[trial_id] = np.append(mic_i2s_i_max[trial_id], [np.zeros(8)]) # array to hold largest value in each data set
                mic_i2s_max = np.append(mic_i2s_max, 0.)
            for i in range(0, 8):
                mic_i2s[trial_id][i] = np.append(mic_i2s[trial_id][i], b[i+2])
                if (abs(float(b[i+2])) > mic_i2s_i_max[trial_id][i]):
                    mic_i2s_i_max[trial_id][i] = abs(float(b[i+2]))
                if mic_i2s_i_max[trial_id][i] > mic_i2s_max[trial_id]:
                    mic_i2s_max[trial_id] = mic_i2s_i_max[trial_id][i]
            if (i2s_row_number > 512):
                i2s_row_number = 0
                hft_row_number = 0
                sft_row_number = 0
                found_i2s = 0
                found_hard_fft = 0
                found_soft_fft = 0
        
    if (found_hard_fft == 1):
        if ((line.startswith('ID')) == 0) & (TRIAL_DONE == 0):
            hft_row_number = hft_row_number + 1   
            a = line.replace('\t', ' ')
            b = a.split()        
            if int(b[1])==0: # start of new data set
                trial_id = int(b[0])
                mic_hard_fft.append([[], [], [], [], [], [], [], []])
            for i in range(0, 8):
                mic_hard_fft[trial_id][i].append(float(b[i+2]))
            if (hft_row_number > 256):
                i2s_row_number = 0
                hft_row_number = 0
                sft_row_number = 0
                found_i2s = 0
                found_hard_fft = 0
                found_soft_fft = 0
        
    if (found_soft_fft == 1):
        if ((line.startswith('ID')) == 0) & (TRIAL_DONE == 0):
            sft_row_number = sft_row_number + 1   
            a = line.replace('\t', ' ')
            b = a.split()        
            if int(b[1])==0: # start of new data set
                trial_id = int(b[0])
                mic_soft_fft.append([[], [], [], [], [], [], [], []])
            for i in range(0, 8):
                mic_soft_fft[trial_id][i].append(float(b[i+2]))
            if (sft_row_number > 256):
                i2s_row_number = 0
                hft_row_number = 0
                sft_row_number = 0
                found_i2s = 0
                found_hard_fft = 0
                found_soft_fft = 0
    
    line = f.readline()

    
f.close()

NORMALIZE = 1

samplingInterval    = 1 / I2S_FS

if (len(mic_i2s[0][0]) > 0):
    for i in range(0, 8):
        mic_calc_fft[i] = np.fft.fft(mic_i2s[0][i]) / len(mic_i2s[0][i])
        calc_fft[i] = mic_calc_fft[i][range(int(len(mic_i2s[0][i])/2))]
    
    
#print(fft_mic_0)

#print(mic_soft_fft[0])
    
#x_fft = np.linspace(0.0, 1.0/(2.0*T), N//2)


#tpCount     = len(mic_soft_fft[0])
tpCount     = len(mic_i2s[0][0])/2

print(tpCount)

i2s_values      = np.arange(int(tpCount*2))
fft_values      = np.arange(int(tpCount))

timePeriod  = 1/I2S_FS

frequencies = fft_values*(I2S_FS/tpCount/2)/1000
time_us = i2s_values * timePeriod *1E6
 
print('Plot FFT results for mic 0 to 7')

legend = ['MK0','MK1', 'MK2', 'MK3', 'MK4', 'MK5', 'MK6', 'MK7']
linestyles = ['solid', 'dotted', 'dashed', 'dashdot', 'solid', 'dotted', 'dashed', 'dashdot']

# Function used by all plot loops to place chart text
def setup_common_plot(which_data):
    plt.legend()
    suptitle = which_data
    if len(mic_i2s)>1:
        suptitle = suptitle + f' Trial #{trial_id}'
    if SOURCE != '':
        suptitle = suptitle + ', SRC: ' + SOURCE
    if I2S_FS != '':
        suptitle = suptitle + ', FS: ' + '{:.1f}'.format(I2S_FS/1000) + 'kHz'
    if SIGNAL_PATH != '':
        suptitle = suptitle + '\n@ ' + SIGNAL_PATH
    if DESCRIPTION != '':
        title = DESCRIPTION
    if NORMALIZE_I2S_DATA != 0:
        title = DESCRIPTION + ' (normalized data)'
    if fname != '':
        plt.figtext(0.95, 0.01, fname, horizontalalignment='right', fontsize=6) 
    if DATE != '':
        plt.figtext(0.05, 0.01, DATE + ' @ ' + TIME, horizontalalignment='left', fontsize=6) 
    plt.suptitle(suptitle, fontsize=10)
    plt.title(title, fontsize=6)
 
    
fig_i2s = []
for trial_id in range(0, len(mic_i2s)):
    if (len(mic_i2s[trial_id]) > 0):
        fig_i2s.append(plt.figure(f"I2S:{trial_id}"))
        for i in range(0, 8):
# If required scale data by (mic_i2s_max[trial_id]/mic_i2s_i_max[trial_id][i])
            scale = 1.0
            if (NORMALIZE_I2S_DATA ==1):
                scale =  float(mic_i2s_max[trial_id])/float(mic_i2s_i_max[trial_id][i])
#           plt.plot(time_us[:I2S_PLOT_SIZE], mic_i2s[trial_id][i][:I2S_PLOT_SIZE]*scale, linestyle=linestyles[i], label=legend[i])
            plt.plot(i2s_values[:I2S_PLOT_SIZE], mic_i2s[trial_id][i][:I2S_PLOT_SIZE]*scale, linestyle=linestyles[i], label=legend[i])
        setup_common_plot('I2S Data')
        plt.ylabel('I2S (dBFS)')
#        plt.xlabel('Time (µS)')
        plt.xlabel('Sample #')
    x_ticks = np.arange(0, int(I2S_FS/1000/2), 10)
    x_labels = list(map(str, x_ticks.astype(int)))

fig_hfft = []
for trial_id in range(0, len(mic_hard_fft)):
    if (len(mic_hard_fft[trial_id]) > 0):
        fig_hfft.append(plt.figure(f"HFFT:{trial_id}"))
        for i in range(0, 8):
            plt.plot(frequencies, mic_hard_fft[trial_id][i], linestyle=linestyles[i], label=legend[i])
        setup_common_plot('I2S Data H/W FFT')
        plt.ylabel('I2S H/W FFT')
        plt.xlabel('Frequency (kHz)')

fig_sfft = []
for trial_id in range(0, len(mic_soft_fft)):
    if (len(mic_soft_fft[trial_id]) > 0):
        fig_sfft.append(plt.figure(f"SFFT:{trial_id}"))
        for i in range(0, 8):
            plt.plot(frequencies, mic_soft_fft[trial_id][i], linestyle=linestyles[i], label=legend[i])
        setup_common_plot('I2S Data S/W FFT')
        plt.ylabel('I2S S/W FFT')
        plt.xlabel('Frequency (kHz)')

    #hover=True, highlight=True, 
mplcursor.cursor(bindings={"toggle_visible": "h", "toggle_enabled": "e"}, multiple=True)
plt.show()



