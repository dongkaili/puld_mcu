# Data server for Prosaris PULD device
# Uses separate threads for send and receive operation and provides an IPC interface 
# for a separate connection to administer tests or device probing

import time, sys

DataParser_in_thread = True

from PyQt5.QtCore import QObject, pyqtSignal, QThread
from PyQt5.QtWidgets import QApplication

# https://docs.python.org/3/library/multiprocessing.html
from multiprocessing.connection import Listener
import json

# https://www.cloudcity.io/blog/2019/02/27/things-i-wish-they-told-me-about-multiprocessing-in-python/

from multiprocessing import Queue
from multiprocessing.pool import ThreadPool
# https://stackoverflow.com/questions/53094812/how-to-assure-the-multiprocessing-queue-is-empty

from copy import copy

import serial
from serial.tools.list_ports import comports
from serial.tools import hexlify_codec
from datetime import datetime

import numpy as np
import numpy.ma as ma

from inspect import currentframe
def get_linenumber(): 
    cf = currentframe() 
    return cf.f_back.f_lineno 
# print(get_linenumber(), " : ") 


from enum import Enum, auto
class FIFOS(Enum):
    IPC_CMD_FIFO      = auto()
    IPC_CMD_RSP_FIFO  = auto()
    ALT_CMD_RSP_FIFO  = auto()
    ALL_CMD_RSP_FIFO  = auto()
    BF_MSSG_FIFO      = auto()

class MSSG_FIFOS(object): # container to make it easierto pass references
    def __init__(self):
        max_size = 5 # 3 seems to be min value to avoid unintended overflow so go with 5 for now... can set higher

        self.IPC_CMD_FIFO = Queue(max_size) # CMD's in DICT format sent to the device from the IPC channel are dropped in here and pulled out by 
        
        # Device JSON message responses are split up by processRecvData() depending on the source of the command
        self.IPC_CMD_RSP_FIFO = Queue(max_size) # CMD RSP's recongnized as being to message from the IPC channel are put in here... use to verify command execution during testing
        self.ALT_CMD_RSP_FIFO = Queue(max_size) # CMD RSP's not from the IPC channel are put in here... tracks responses from internal devie management tasks 
        self.ALL_CMD_RSP_FIFO = Queue(max_size) # All CMD RSP's are inserted here... large bucket for every CMD RSP

        self.BF_MSSG_FIFO = Queue(max_size) # All messages from BeamFormer output as a complex object (maybe as JSON DICT) with additional detail

    def put(self, which, what):
        # then handle the new IPC source item
        try:
            if which ==  FIFOS.IPC_CMD_FIFO    : 
                self.IPC_CMD_FIFO.put(what, False) 
            if which ==  FIFOS.IPC_CMD_RSP_FIFO: 
                self.IPC_CMD_RSP_FIFO.put(what, False) 
            if which ==  FIFOS.ALT_CMD_RSP_FIFO: 
                self.ALT_CMD_RSP_FIFO.put(what, False) 
            if which ==  FIFOS.ALL_CMD_RSP_FIFO: 
                self.ALL_CMD_RSP_FIFO.put(what, False) 
            if which ==  FIFOS.BF_MSSG_FIFO    :
                self.BF_MSSG_FIFO.put(what, False) 
        except: # just iognore overflow
            None

    def get_queue_sizes(self, called_from):
        #return
        return ("{0} ... qsizes  [{1}:{2} | {3}:{4} | {5}:{6} | {7}:{8} | {9}:{10}]".format(called_from, 
        self.IPC_CMD_FIFO.qsize(),      self.IPC_CMD_FIFO.empty(), 
        self.IPC_CMD_RSP_FIFO.qsize(),  self.IPC_CMD_RSP_FIFO.empty(), 
        self.ALT_CMD_RSP_FIFO.qsize(),  self.ALT_CMD_RSP_FIFO.empty(), 
        self.ALL_CMD_RSP_FIFO.qsize(),  self.ALL_CMD_RSP_FIFO.empty(), 
        self.BF_MSSG_FIFO.qsize(),       self.BF_MSSG_FIFO.empty()
        ))


class PULD_Server(object):
    def __init__(self):

        global gen_polarplot_data, gen_stripchart_data

        self.port = None
        self.baudrate = 115200
        self.parity = 'N'
        self.rtscts = False
        self.xonxoff = False
        self.exclusive = True

        self.recv_bytes = []
        
        self.FIFOS = MSSG_FIFOS()
        self.IPC_CMD_Sent = False # flag to track CMD's that were sent to more than one MCU so mssgs for all can be aggregated
        self.IPC_CMD = "" # Holder for last IPC command that was sent so it can be tested against other... Should only be loaded for multicast mssgs

        enable_cycle_send = False
        process_is_done = True

        all_cmd_list = [] # contain a list of all commands to send
        
        gen_polarplot_data = True
        gen_stripchart_data = False # should be disabled by default... should be true for polar data as well.

        try:
            self.port = self.ask_for_port()
        except KeyboardInterrupt:
            self.port = None
        
        try:
            self.serial_instance = serial.serial_for_url(
                self.port,
                self.baudrate,
                parity=self.parity,
                rtscts=self.rtscts,
                xonxoff=self.xonxoff,
                do_not_open=True,
                timeout= 8.0)

            if isinstance(self.serial_instance, serial.Serial):
                self.serial_instance.exclusive = self.exclusive

            self.serial_instance.open()
            if self.serial_instance.is_open == True:
                print('port {} is opened successfully!'.format(self.port))           
            else:
                print('Failed to open port {}!'.format(self.port))   
                return
                
            
        except serial.SerialException as e:
            sys.stderr.write('could not open port {!r}: {}\n'.format(self.port, e))
            
        
        self.serial_instance.flushInput()
        
        self.serial_transmitter = self.SerialTransmitter(self.serial_instance)
        
        """Long-running task in 5 steps."""
        # Step 2: Create a QThread object
        self.thread_read = QThread()
        # Step 3: Create a worker object
        self.reader = self.DataReader(self.serial_instance)
        # Step 4: Move worker to the thread
        self.reader.moveToThread(self.thread_read)
        # Step 5: Connect signals and slots
        self.thread_read.started.connect(self.reader.run)
        self.reader.finished.connect(self.thread_read.quit)
        self.reader.finished.connect(self.reader.deleteLater)
        self.thread_read.finished.connect(self.thread_read.deleteLater)
        self.reader.newData.connect(self.processRecvData)
        # Step 6: Start the thread
        self.thread_read.start()

        # https://realpython.com/python-pyqt-qthread/
        """IPC CMD Handler"""
        # create a qthread object
        self.thread_listen = QThread()
        # create a worker object
        self.listener = self.ipcdatareader(self.serial_transmitter, self.FIFOS) # initalize and pass handles for functions it needs
        # move worker to the thread
        self.listener.moveToThread(self.thread_listen)
        # connect signals and slots
        self.thread_listen.started.connect(self.listener.run)
        self.listener.finished.connect(self.thread_listen.quit)
        self.listener.finished.connect(self.listener.deleteLater)
        self.thread_listen.finished.connect(self.thread_listen.deleteLater)
        self.listener.newdata.connect(self.listener.run) # what should this be?
        # step 6: start the thread
        self.thread_listen.start()

        """IPC Data Stream Handler"""
        # create a qthread object
        self.thread_DataParse = QThread()
        # create a worker object
        self.DataParser = self.ipcDataParser(self.FIFOS) # initalize and pass handles for functions it needs
        # move worker to the thread
        self.DataParser.moveToThread(self.thread_DataParse )
        # connect signals and slots
        self.thread_DataParse.started.connect(self.DataParser.run)
        self.DataParser.finished.connect(self.thread_DataParse.quit)
        self.DataParser.finished.connect(self.DataParser.deleteLater)
        self.thread_DataParse.finished.connect(self.thread_DataParse.deleteLater)
        self.DataParser.newdata.connect(self.DataParser.run) # what should this be?
        # step 6: start the thread
        if (DataParser_in_thread):
            self.thread_DataParse.start()

        
    class SerialTransmitter(QObject):

        def __init__(self, serial_instance, parent=None):
            super().__init__(parent)
            self.serial_instance = serial_instance
        
        def send_command_string(self, letter, command_type):
            """ Write Micro Json string to sys.stderr"""
            
            """
            HH(0x11), VV(Version Byte), TH(0x00: Raw, 0x01:Json), TL(0x00~0x04), Length(4 bytes, LSB first?), checksum, Reserve1, R2, R3, <data>
        
            """
            header    = 0x11
            #print(header.to_bytes(1, 'big'))
            version   = 0x02
            if command_type == 'JSON_Type':
                type_high = 0x01
                type_low  = 0x01
            elif command_type == 'LLC_Type':
                type_high = 0x02
                type_low  = 0x05
            elif command_type == 'Data_Type':
                type_high = 0x00
                type_low  = 0x06
            elif command_type == 'I2S_Raw_Type':
                type_high = 0x00
                type_low  = 0x07
            elif command_type == 'I2S_FFT_Type':
                type_high = 0x00
                type_low =  0x04
            elif command_type == 'RAW_FFT_Type':
                type_high = 0x00
                type_low =  0x08
            elif command_type == 'DIR_RAW_Type':
                type_high = 0x00
                type_low =  0x09
            elif command_type == None:
                if 'i2s_fft_raw' in letter:
                    type_high = 0x00
                    type_low =  0x08
                elif 'dir_raw' in letter:
                    type_high = 0x00
                    type_low =  0x09
            
            length_1  = 0x00
            length_2  = 0x00
            length_3  = 0x00
            length_4  = 0x00
            reserve_1 = 0x00
            reserve_2 = 0x00
            reserve_3 = 0x00
        
            # Prob need to change this to only accept full text strings 
            if letter == '\x11':  # CTRL+Q
                cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"ENABLE"}')
            elif letter == '\x1A':  # CTRL+Z
                cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"DISABLE"}')
            elif letter == 'RBV' or letter == 'V':
                cmd_string = list('{"CMD": "RBV"}')
            elif letter == 'RBC' or letter == 'C':
                cmd_string = list('{"CMD": "RBC"}')
            elif letter == 'I':
                cmd_string = list('{"CMD": "RID", "MCU":0}')
            elif letter == 'F':
                cmd_string = list('{"CMD": "RFV", "MCU":0}')
            elif letter == 'p':
                cmd_string = list('{"CMD": "RPV"}')
            elif letter == 'v':
                cmd_string = list('version')
            else:
                cmd_string = list(letter)
            
            #print("Cmd: {}".format(cmd_string))
        
            length_1 = len(cmd_string) & 0xFF
            length_2 = (len(cmd_string) >> 8) & 0xFF
            length_3 = (len(cmd_string) >> 16) & 0xFF
            length_4 = (len(cmd_string) >> 24) & 0xFF
        
            calculated_checksum = 0
            check_sum = 0
            pay_load = []
            for i in range(0, len(cmd_string)):
                pay_load.append(ord(cmd_string[i]))
                calculated_checksum += pay_load[i]
        
            calculated_checksum = calculated_checksum & 0xFF
            calculated_checksum = 0xFF - calculated_checksum
        
            check_sum = calculated_checksum
            #print('check_sum is ' + hex(check_sum))
        
            tx_data = []
            tx_data.append(header.to_bytes(1, 'big'))
            tx_data.append(version.to_bytes(1, 'big'))
            tx_data.append(type_high.to_bytes(1, 'big'))
            tx_data.append(type_low.to_bytes(1, 'big'))
            tx_data.append(length_1.to_bytes(1, 'big'))
            tx_data.append(length_2.to_bytes(1, 'big'))
            tx_data.append(length_3.to_bytes(1, 'big'))
            tx_data.append(length_4.to_bytes(1, 'big'))
            tx_data.append(check_sum.to_bytes(1, 'big'))
            tx_data.append(reserve_1.to_bytes(1, 'big'))
            tx_data.append(reserve_2.to_bytes(1, 'big'))
            tx_data.append(reserve_3.to_bytes(1, 'big'))

        
            for item in pay_load:
                tx_data.append(item.to_bytes(1, 'big'))        
               
            for i in range(0, len(tx_data)):
                self.serial_instance.write((tx_data[i]))

    class DataReader(QObject):

        newData  = pyqtSignal(object)
        finished = pyqtSignal()
    
        def __init__(self, serial_instance, parent=None):
            super().__init__(parent)
            self.serial = serial_instance
            self.recv_bytes_list = []
        
        def run(self):
            """Long Run task 1 to receive incoming data on serial port"""
            try:
                while True:
                    # read all that is there or wait for one byte
                    data = self.serial.read(self.serial.in_waiting)
                    if data:
                        new_recv_bytes = list(data)
                        self.newData.emit(new_recv_bytes)
                    
                    
            except serial.SerialException:
                print('serial has exception')
        

    class ipcdatareader(QObject):
        newdata  = pyqtSignal(object)
        finished = pyqtSignal()
        CMD_Sent = False

        def __init__(self, serial_tx, fifos, port=6000, timeout=500, parent=None):
            super().__init__(parent)
            self.listener = Listener(('localhost', port), authkey=b'secret password')

            self.serial_transmitter = serial_tx
            self.FIFOS = fifos
            self.timeout = timeout
            self.rsp_counter = 0

        def forward_cmd(self, mssg):
            """ Forward cmd to serial transmitter 
                Execute "%run LoadCmds.py" on system start and the send() function will be available 
                You can send any text with that function and the following code will determine if it is 
                a JSON command intended for the PULD device or for use by this app
            """
            # https://stackoverflow.com/questions/10791588/getting-container-parent-object-from-within-python
            #import pdb; pdb.set_trace()
            self.serial_transmitter.send_command_string(mssg, 'JSON_Type') # This shoudl be safe to run in main process but might be better to push/pull IPC_CMD from IPC_CMD_FIFO rather than call directly... not using FIFOa at present!
        
        def run(self):
            global gen_polarplot_data, gen_stripchart_data

            """long run task 1 to receive incoming data on ipc link"""
            running = True

            def send(header, mssg):
                print("IPC_Send" + header + mssg)
                send_mssg = header, mssg
                self.conn.send(send_mssg)

            while running:
                try:        
                    self.conn = self.listener.accept()
                    #print('connection accepted from', self.listener.last_accepted)
                    while True:
                        # TODO: try: except: here ?
                        mssg = self.conn.recv() # blocking but own thread so OK
                        #print("Here 0.1")
                        try:
                            # handle JSON command... could include one to close connection as well
                            # These are one-shot commands so the connection will be closed by the sender after sending
                            #print("Here 0.2")
                            ipc_cmd_dict = json.loads(mssg)
                            ipc_cmd_keys = ipc_cmd_dict.keys()
                            if "CMD" in ipc_cmd_keys: # is a valid remote command so forward to device
                                ## set flag to be cleared by app when RSP is received and then return that back to sender
                                #try:
                                #    # Send CMD to device
                                #    self.FIFOS.put(FIFOS.IPC_CMD_FIFO, ipc_cmd_dict) # push DICT version so it's more readily used 
                                #except:
                                #    # no room in IPC_CMD_FIFO so RSP receiver is backed up... therefore no point in waiting around for response
                                #    self.conn.close() 
                                #    break

                                # room available in IPC_CMD_FIFO for new CMD so send it and process response
                                self.forward_cmd(mssg)
                                time.sleep(0.15) # a little delay to give device time to respond 0.05 will cause MISMATCH faults, 0.75 wil cause conn error faults

                                rsp = [] # clear response container

                                # safely wait for initial response (with timeout)
                                timer = self.timeout # configurable wait for ack... default=1s
                                if True:
                                    while (timer>0):
                                        try:
                                            next_rsp = self.FIFOS.IPC_CMD_RSP_FIFO.get(False)
                                            break # kick out when rsp is received
                                        except:
                                            #time.sleep(0.001) # sleep for 1ms
                                            timer = timer-1
                                else:                                
                                    next_rsp = '' # clear to start
                                    while (timer>0):
                                        try:
                                            while True: # read to flush and when we're empty we'll kick out either with a new value or nothng
                                                prev_rsp = next_rsp
                                                next_rsp = self.FIFOS.IPC_CMD_RSP_FIFO.get(False)
                                                self.rsp_counter += 1
                                                if prev_rsp != next_rsp and prev_rsp != '':
                                                    print(get_linenumber(), ':', self.rsp_counter, " : ", 'Flushing:', prev_rsp) 
                                            # exit here if I get an exception after getting semething otherwise stay in loop until timer expires... 
                                            # should have last item in FIFO at that point
                                            # We don't want to fully flush... just until we get a matching CMD & MCU pattern (exact if MCU!=3)
                                            # art 0.5s delays we eventually get flushing errors... likely due to MCU3 responses gettign in before we get back to the next read
                                        except:
                                            #time.sleep(0.001) # sleep for 1ms
                                            timer = timer-1
                                            if next_rsp != '' :
                                                break

                                #print(get_linenumber(), " : ", ipc_cmd_dict, next_rsp) 
                                if timer == 0:
                                    self.conn.close() # quiting loop so must close
                                    break

                                rsp.append(next_rsp) # insert rsp message into full rsp container

                                # if multicast cmd then we may get up to 2 more responses
                                if (not "MCU" in ipc_cmd_dict.keys()) or ipc_cmd_dict["MCU"]==3: 
                                    for i in [2, 1]:

                                        # safely wait for 2nd response (with timeout)
                                        timer = self.timeout # configurable wait for ack... default=1s
                                        while (timer>0):
                                            try:
                                                next_rsp = self.FIFOS.IPC_CMD_RSP_FIFO.get(False)
                                                break # kick out when rsp is received
                                            except:
                                                #time.sleep(0.001) # sleep for 1ms
                                                timer = timer-1

                                        if timer == 0:
                                            break # breaking inner loop bpot not outer so don't close conn

                                        rsp.append(next_rsp) # insert resp message into full rsp container
                                
                                # when we get here we're done for this IPC CMD
                                
                                # error checking to catch mismatch... could be caught and corrected earlier 
                                error = False
                                for mssg in rsp:
                                    cmd_rsp_match = ipc_cmd_dict["CMD"] == mssg["RSP"]
                                    mcu_match = (not "MCU" in ipc_cmd_dict.keys()) or ipc_cmd_dict["MCU"]==3 or ipc_cmd_dict["MCU"] == mssg["MCU"]
                                    if not (cmd_rsp_match and mcu_match) :
                                        error = True
                                if error:
                                    print("Mismatch error!", get_linenumber(), " : ", ipc_cmd_dict, rsp)

                                    # flush RSP FIFO
                                    while not self.FIFOS.IPC_CMD_RSP_FIFO.empty():
                                        try:
                                            next_rsp = self.FIFOS.IPC_CMD_RSP_FIFO.get(False)
                                            rsp.append(next_rsp) # send back aggregate of all RSP's that are being rejected 
                                        except:
                                            time.sleep(0.1) # a little delay to wait for next RSP if available

                                    # Enother approach...
                                    # Read from IPC_RSP FIFO until empty and retain the last 3 RSP's that match CMD
                                    # non-matches get sent to ALT FIFO
                                    # I think I tried to set that up but it needs rethinking
                                 
                                self.conn.send(rsp) 
                                self.conn.close()

                            elif "CMDENV" in ipc_cmd_keys: # is a valid remote command so forward to device
                                # LoadCmds.send('{"CMDENV":"SET","VAR":"gen_stripchart_data", "VAL":1}')
                                # LoadCmds.send('{"CMDENV":"SET","VAR":"gen_polarplot_data", "VAL":1}')
                                try:
                                    if (ipc_cmd_dict["CMDENV"] == "SET"):
                                        if (ipc_cmd_dict["VAR"] == "gen_polarplot_data"):
                                            gen_polarplot_data = True if ipc_cmd_dict["VAL"] == 1 else False
                                            send_mssg = json.dumps(ipc_cmd_dict) + ", State =" + str(gen_polarplot_data)
                                        elif (ipc_cmd_dict["VAR"] == "gen_stripchart_data"):
                                            #gen_stripchart_data = ipc_cmd_dict["VAL"]
                                            gen_stripchart_data = True if ipc_cmd_dict["VAL"] == 1 else False
                                            send_mssg = json.dumps(ipc_cmd_dict) + ", State =" + str(gen_stripchart_data)
                                        mssg="Success: "+ send_mssg
                                        self.conn.send(mssg)
                                    elif (ipc_cmd_dict["CMDENV"] == "GET"):
                                        if (ipc_cmd_dict["VAR"] == "gen_polarplot_data"):
                                            send_mssg = json.dumps(ipc_cmd_dict) + ", State =" + str(gen_polarplot_data)
                                        elif (ipc_cmd_dict["VAR"] == "gen_stripchart_data"):
                                            send_mssg = json.dumps(ipc_cmd_dict) + ", State =" + str(gen_stripchart_data)
                                        mssg="Success: "+ send_mssg
                                        self.conn.send(mssg)
                                except:
                                    print('Malformed Test Environment Command:' +  ipc_cmd_dict)
                                    send('Malformed Test Environment Command:',  ipc_cmd_dict)

                            else: # otherwise treat like internal command (enable LOG, FILTER, TRANSFORM, etc.)
                                    # Could have an internal command to enable all or filtered logging of data stream from device
                                    # as well as commands to send text to the log file to capture conditions.
                                send('Unrecognized Command:',  ipc_cmd_dict)
                            time.sleep(0.1)
                            self.conn.close()
                            break
                        except:
                            # not a JSON command so report on status bar.. will generate a lot of noise so NO
                            # also these messages may be executed in batch so use just one connect5ion 
                            if mssg == 'close connection':
                                self.conn.close()
                                break
                except:
                    print('IPC CMD handler has exception')   
                    self.conn.close()

        def close(self):
            self.conn.close()
            self.listener.close()
 
    def ask_for_port(self):
        """\
        Show a list of ports and ask the user for a choice. To make selection
        easier on systems with long device names, also allow the input of an
        index.
        """
        sys.stderr.write('\n--- Available ports:\n')
        ports = []
        for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
            port_num = int(port[3:])
            if (port_num <250): # By convention #250+ is for debug ports
                sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
                ports.append(port)
                if 'USB Serial Port' in desc:
                    return port
                
        time.sleep(0.05)
        if (len(ports)==1): # only one suitable port... could query USB device 
            port = ports[0]
            return port
        while True:
            port = input('--- Enter port index or full name: ')
            try:
                index = int(port) - 1
                if not 0 <= index < len(ports):
                    sys.stderr.write('--- Invalid index!\n')
                    continue
            except ValueError:
                pass
            else:
                port = ports[index]
            return port
       
    def processRecvData(self, var):
        received_bytes = copy(var)
        
        if len(received_bytes) > 0:
            if 0x11 != received_bytes[0]:
                self.recv_bytes = self.recv_bytes + received_bytes
            else:                
                self.recv_bytes = received_bytes
            
            #self.recv_bytes = received_bytes
            whole_string = ''.join([chr(elem) for elem in self.recv_bytes])
            #print(whole_string)
            
            if whole_string[-1] == '}' and whole_string.count('{') == whole_string.count('}'):
                #print(whole_string)
            
                start_index = whole_string.find('{')
                end_index = whole_string.find('}',start_index+1)
                start_count = whole_string.count('{', 0, end_index)
                while start_index != -1 and end_index != -1:
                    while start_count > 1:
                        end_index = whole_string.find('}', end_index+1)
                        start_count = start_count - 1
                    json_string = whole_string[start_index:end_index+1]
                    try:
                        rsp_dict = json.loads(json_string) 
                    except:
                        # start_index = whole_string.find('{', end_index+1) # skip current and find next
                        # end_index = whole_string.find('}', end_index+1)
                        # break # invalid json so exit
                        pass

                    rsp_keys = rsp_dict.keys()

                    if not all(key in rsp_keys for key in ["MCU","ANGLE","DBFS","STATE"]):
                        # This is a CMD response (either IPC or  ALT... sort that out later)
                        self.FIFOS.put(FIFOS.IPC_CMD_RSP_FIFO, rsp_dict) # send the MC rsp that was in progress
                        
                        # NOTE: Next FIFO will be useful when IPC_CMD_RSP_FIFO support is available when IPC & NON-IPC streams will be split 
                        #self.FIFOS.put(FIFOS.ALL_CMD_RSP_FIFO, rsp_dict) # This will hold all CMD RSP messagess we can compare to what is reported in IPC channel
                    elif all(key in rsp_keys for key in ["MCU","ANGLE","DBFS","STATE"]):                    
                        # This is an MCU Data stream object
                        self.FIFOS.put(FIFOS.BF_MSSG_FIFO, rsp_dict)
                    else:
                        print("Malformed message: {0}".format(json_string))

                    #print(self.FIFOS.get_queue_sizes(get_linenumber()), " : ")
                    start_index = whole_string.find('{', end_index+1)
                    end_index = whole_string.find('}', end_index+1)
                    start_count = whole_string.count('{', start_index, end_index)


    class ipcDataParser(QObject):
        newdata  = pyqtSignal(object)
        finished = pyqtSignal()

        def __init__(self, fifos, port=6001, timeout=1000, parent=None):
            super().__init__(parent)
            self.DataStreamer = Listener(('localhost', port), authkey=b'secret password')
            self.FIFOS = fifos
            self.timeout = timeout
            self.display_rate_sample_limit  = 1
            self.display_rate_sample_counter = 0
            self.display_total_sample_limit = 10

            self.A_B_Testing = True

            #self.gen_polarplot_data = False
            #self.gen_stripchart_data = False # should be disabled by default... should be true for polar data as well.

            # Parsed data details ready for charting when populated
            self.mcu = 0
            self.mcu_arr = []
            self.mcu_list = []
            self.curve_angle_arr = [[],[]]
            self.curve_dbfs_arr = [[],[]]
            self.angle_list = [[],[]]
            self.dbfs_list = [[],[]]
            self.mcu_angle_list = [[[], [], []],[[], [], []]]
            self.mcu_dbfs_list = [[[], [], []],[[], [], []]]

            dbfs_min_init = 0
            dbfs_max_init = -120
        
            self.dbfs_min = dbfs_min_init
            self.dbfs_max = dbfs_max_init

            self.polar_x_list = [[],[]]
            self.polar_y_list = [[],[]]
            self.polar_x_arr = [[],[]]
            self.polar_y_arr = [[],[]]
            self.mcu_polar_x_list = [ [[],[],[]], [[],[],[]] ]
            self.mcu_polar_y_list = [ [[],[],[]], [[],[],[]] ]
            
            self.pw_dbfs_plot_item = "" # may be monitoe by label elements in GUI to self-update

            #print("ipcDataParser.init")

        def updateDBFSlimits(self, dbfs):
            global display_total_sample_limit

            if (dbfs < self.dbfs_min):
                self.dbfs_min = dbfs
            if (dbfs > self.dbfs_max):
                self.dbfs_max = dbfs
            # Convenient text for GUI label item
            self.MinMaxDBFSLabel = ("MIN:" + str(self.dbfs_min) + " : " + "MAX:" + str(self.dbfs_max))

        def run(self):
            #print("ipcDataParser.run #1")
            #print(self.DataStreamer)

            # Each PULD mssg message object is pushed into an appropriate FIFO and popped off where needed
            # In this case we are working with self.FIFOS.BF_MSSG_FIFO
            # FIFO entries could include datetime stamp as well as enum status [PULD_RSP, PULD_DTA, PULD_ERR] where PULD_DTA is  
            # Could have resettable counters for various statistics including the number of responses for each type of command (at least an ERR counter)
            global gen_polarplot_data, gen_stripchart_data

            """long run task 1 to receive incoming data on ipc link"""
            running = True
            while running:
                try:        
                    self.conn = self.DataStreamer.accept()
                    #print('connection accepted from', self.DataStreamer.last_accepted)

                    json_dict = ""
                    while not self.FIFOS.BF_MSSG_FIFO.empty(): # if non-empty then we have a propery deconstructed beamformer message already in DICT format
                        try:
                            json_dict = self.FIFOS.BF_MSSG_FIFO.get(False) # should already be in DICT format and this will empty IPC CMD fifo as well
                        except: # something wrong with FIFO access so close conn and quit inner loop... shouldnt; get in here
                            print("ipcDataParser.run FIFO error")
                            self.conn.close()
                            break
                        try:
                            json_string = json.dumps(json_dict) # ending full string for now but could be broken down data or DICT
                            json_keys = json_dict.keys()

                        except: #either a conversion error or lost connection
                            break

                        # Continue processing
                        self.display_rate_sample_counter += 1 # regulates the rate at which the chart is updated
                        self.mcu = json_dict["MCU"]
                        #self.mcu_list.append(self.mcu) # MCU's are also referenced for each data point so they can be split up when plotted
                        self.mcu_list.append(self.mcu) # MCU's are also referenced for each data point so they can be split up when plotted
                        self.mcu_list = self.mcu_list[-self.display_total_sample_limit:] # Truncate to display_total_sample_count points
                        self.mcu_arr = np.array(self.mcu_list) # matching numpy array for masking
                        json_keys = json_dict.keys()
                        Found_DUT_Data = True # May not need this
                        for DUT in range(2 if self.A_B_Testing else 1):
                            if ((DUT==1) & (not "ANGLE_DUT" in json_keys)): # not streaming DUT data so exit inner loop and get next JSON mssg
                                Found_DUT_Data = False
                                break 
                            self.angle = json_dict["ANGLE_DUT"] if DUT==1 else json_dict["ANGLE"]
                            self.dbfs = json_dict["DBFS_DUT"] if DUT==1 else json_dict["DBFS"]
                            self.updateDBFSlimits(self.dbfs)

                            if (self.dbfs_max == self.dbfs_min):
                                signal_strength = 100
                            else:
                                signal_strength = (self.dbfs - self.dbfs_min)/(self.dbfs_max - self.dbfs_min) * 100

                            # Add new Angle & DBFS values to list
                            self.angle_list[DUT].append(self.angle)
                            self.dbfs_list[DUT].append(self.dbfs)
                            # Truncate to max display_total_sample_count points
                            self.angle_list[DUT] = self.angle_list[DUT][-self.display_total_sample_limit:]
                            self.dbfs_list[DUT] = self.dbfs_list[DUT][-self.display_total_sample_limit:]

                            # Calculate X & Y coords of current polar plot point...
                            polar_x = signal_strength * np.sin(self.angle / 180.0 * np.pi)
                            polar_y = signal_strength * np.cos(self.angle / 180.0 * np.pi)
                            # Add to lists...
                            self.polar_x_list[DUT].append(polar_x)
                            self.polar_y_list[DUT].append(polar_y)
                            # Truncate to display_total_sample_count points
                            self.polar_x_list[DUT] = self.polar_x_list[DUT][-self.display_total_sample_limit:]
                            self.polar_y_list[DUT] = self.polar_y_list[DUT][-self.display_total_sample_limit:]

                            if (self.display_rate_sample_counter % self.display_rate_sample_limit == 0):
                                self.display_rate_sample_counter = 0
                                if (gen_stripchart_data):
                                    self.curve_angle_arr[DUT] = np.array(self.angle_list[DUT])
                                    self.curve_dbfs_arr[DUT] = np.array(self.dbfs_list[DUT])
                                    list_len = len(self.mcu_list)
                                    # Option to make next steps quicker by indexing with mcu_arr... 
                                    #    PUSH onto end of current [DUT][mcu] array
                                    #    POP from head of mcu array pointed by previous head (maybe do the mcu_arr updated after)
                                    #    ISSUE: mcu_####_list's are only updated when we are in this loop so we at least have to do all PUSH & POP steps
                                    for mcu in range(3):
                                        self.mcu_match = np.array(self.mcu_arr==mcu) # boolean array indicating MCU# match positions
                                        self.mcu_match = self.mcu_match[0:len(self.curve_angle_arr[DUT])] # adjustment while DUT array is added in and gets sized
                                        self.mcu_angle_list[DUT][mcu] = self.curve_angle_arr[DUT][self.mcu_match]
                                        self.mcu_dbfs_list[DUT][mcu] = self.curve_dbfs_arr[DUT][self.mcu_match]

                                if (gen_polarplot_data):
                                    self.polar_x_arr[DUT] = np.array(self.polar_x_list[DUT])
                                    self.polar_y_arr[DUT] = np.array(self.polar_y_list[DUT])
                                    for mcu in range(3):
                                        self.mcu_match = np.array(self.mcu_arr==mcu) # boolean array indicating MCU# match positions
                                        self.mcu_match = self.mcu_match[0:len(self.polar_x_arr[DUT])] # adjustment while DUT array is added in and gets sized
                                        self.mcu_polar_x_list[DUT][mcu] = self.polar_x_arr[DUT][self.mcu_match] # extract X data for this MCU
                                        self.mcu_polar_y_list[DUT][mcu] = self.polar_y_arr[DUT][self.mcu_match] # and Y

                                # Data is all ready at this point and can be assigned to chart
                                # elements with additional pointwise formattign if needed 

                                #send_mssg = "MCU list: ", self.mcu_arr
                                def send(header, mssg):
                                    send_mssg = header, mssg
                                    self.conn.send(send_mssg)

                                if (gen_stripchart_data):
                                    send("MCU Angles: ", self.mcu_angle_list)
                                    send("MCU DBFS: ", self.mcu_dbfs_list)
                                elif (gen_polarplot_data):
                                    send("MCU Polar x: ", self.mcu_polar_x_list)
                                    send("MCU Polar y: ", self.mcu_polar_y_list)
                                else:
                                    send("Angles: ", self.angle_list)

                                if not DataParser_in_thread:
                                    break
                    if not DataParser_in_thread:
                        break

                except:
                    print('IPC Data Parser has exception')   
                    self.conn.close()
                    if not DataParser_in_thread:
                        break

        def close(self):
            self.conn.close()
            self.DataParser.close()

    def sendCMD(self, cmd, mcu=-1, state=""):
        has_mcu = 0
        if (mcu!=-1):
            mcu=int(mcu)
            if (mcu in range(4)):
                has_mcu = 1
                
        has_state = 0
        if (state!=""):
            if (state==1):
                state = "ENABLE"
                has_state = 1
            elif (state==0):
                state = "DISABLE"
                has_state = 1
        
        cmd_part1= '{{\"CMD\":\"{0}\"'.format(cmd)
        cmd_part2= ', \"MCU\":{0}'.format(str(mcu)) if has_mcu else ''
        cmd_part3= ', \"STATE\":\"{0}\"'.format(state) if has_state else ''
        cmd_part4='}'
        current_command = cmd_part1 + cmd_part2 + cmd_part3 + cmd_part4
        self.serial_transmitter.send_command_string(current_command, 'JSON_Type')

    def setupMCUs(self):
        self.sendCMD("RST")
        time.sleep(1.5)
        self.sendCMD("PWR", 1, True)  # Enable slave #1
        self.sendCMD("PWR", 2, True)  # Enable slave #2
        self.sendCMD("HEN", 3, False) # Disable streaming

if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = PULD_Server()
    sys.exit(app.exec_())
