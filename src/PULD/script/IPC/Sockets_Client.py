from multiprocessing.connection import Client
import time

# Client 1
conn = Client(('localhost', 6000), authkey=b'secret password')
conn.send('foo')
time.sleep(1)
conn.send('close connection')
conn.close()
time.sleep(1)

time.sleep(1)

# Client 3 - Random #'s
conn = Client(('localhost', 6000), authkey=b'secret password')
for i in range(250):
    temp_str = "Hello after i = " +str(i)
    conn.send(temp_str)
    time.sleep(0.01)
conn.send('close connection')
conn.close()
time.sleep(1)

# Client 2
conn = Client(('localhost', 6000), authkey=b'secret password')
conn.send('foo2')
time.sleep(1)
conn.send('close connection')
conn.close()

