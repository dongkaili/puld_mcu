from multiprocessing.connection import Client
import time
import json
import random

# https://stackoverflow.com/questions/20309456/call-a-function-from-another-file
# https://docs.python.org/3/library/multiprocessing.html
# https://www.programcreek.com/python/example/55597/multiprocessing.connection.Client
def send(mssg):
    err_count = 0
    while True:
        try:
            conn = Client(('localhost', 6000), authkey=b'secret password')
            try:
                conn.send(mssg)
                if conn.poll(.5):
                    rsp = conn.recv()
                else:
                    rsp = '##NO_RSP##'
            except:
                return '##NO_RSP##'
            conn.close()
            return rsp
        except:
            err_count += 1
            if (err_count==2):
                print("Connection attempt failed {} times".format(err_count))
                return '##CONN_ERR##'
                    

mcu0_cmds = [
"CHEN",
"RCS", 
"RBV", 
"RBC"
]

mcu_cmds_state = [
#"PWR", 
#"RID", 
#"RFV", 
#"RST", 
#"RFRF", 
#"RBLV", 
#"BAUD", 
#"RBR", 
"WDT", 
"HEN"
]
    
mcu_cmds_immediate = [
#"PWR", 
"RID", 
"RFV", 
#"RST", 
#"RFRF", 
"RBLV", 
#"BAUD", 
"RBR", 
#"WDT", 
#"HEN"
]
    

cmd_states = [
"ENABLE",
"DISABLE"
]
    
mcu_vars = [
"double_var_1",
"double_var_2",
"uint32_var_1",
"uint32_var_2",
"uint32_arr_1",
"double_arr_1",
"BP_Low",
"BP_High",
"NF_Adjust",
"DIR_Sens",
"APU_Gain",
"FOV_Angle",
"DBFS_UE_Adj",
"MPA_N",
"Host_Update_Interva",
"Enable_DUT_Test",
"Uart_Check_Interval", 
]

#mcu_list = range(3,4) 
mcu_list = range(4) 
sleep_delay = 0.25
 
i=1  
no_rsp_count=0  
 
Errors_Only = True

print()
print("Starting random command test.")


while True:  
    CMD = '{{"CMD":"{0}", "MCU":{1}, "STATE":"{2}"}}'.format(random.choice(mcu_cmds_state), random.choice(mcu_list), random.choice(cmd_states))  
    rsp=send(CMD)  
    no_rsp_count = no_rsp_count + (1 if rsp=='##NO_RSP##' else 0)   
    if not Errors_Only:
        print("i= {0}:{1} ".format(i*2, no_rsp_count), CMD, " : " , rsp)  
    else:
        error = False
        if rsp!='##NO_RSP##' and rsp!= '##CONN_ERR##':   
            for mssg in rsp:
                try:
                    if mssg["RSP"] != json.loads(CMD)["CMD"]: # prob should check more fields
                        error = True
                except:
                    print('TypeError: string indices must be integers', mssg)
        else:
            error = True

        if error:
            print("MISMATCH ERROR: i= {0}: ".format(i*2), CMD, " : " , rsp)  
            time.sleep(1)


    time.sleep(sleep_delay)  
  
    CMD = '{{"CMD":"{0}", "MCU":{1}}}'.format(random.choice(mcu_cmds_immediate), random.choice(mcu_list))   
    rsp=send(CMD)  
    no_rsp_count = no_rsp_count + (1 if rsp=='##NO_RSP##' else 0)   
    if not Errors_Only:
        print("n= {0}:{1} ".format(i*2+1, no_rsp_count), CMD, " : " , rsp)  
    else:
        error = False
        if rsp!='##NO_RSP##' and rsp!= '##CONN_ERR##':   
            for mssg in rsp:
                try:
                    if mssg["RSP"] !=json.loads(CMD)["CMD"]: # prob should check more fields
                        error = True
                except:
                    print('TypeError: string indices must be integers', mssg)
        else:
            error = True

        if error:
            print("MISMATCH ERROR: n= {0}: ".format(i*2+1), CMD, " : " , rsp)  
            time.sleep(1)
            #quit
    
        mod_count_val = 10
        if i % mod_count_val == 0: # modulo 50 but 2 mssgs per loop so 1100 mssgs
            print('Passed: {0} mssgs ... '.format(i*2))
                
    i+=1  
  
    time.sleep(sleep_delay) 