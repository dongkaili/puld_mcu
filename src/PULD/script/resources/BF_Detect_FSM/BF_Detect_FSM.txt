Inputs:
_7_Min_Sig_Strength_Detected
_6_stable_angle
_5_Flipped_Angle_Condition
_4_wait_angle_check_timeout
_3_wait_angle_check_OK
_2_check_for_angle_flip
_1_flip_detect_check_OK
_0_Min_N_Angle_Flips_OK

Outputs:
_2_check_for_angle_flip

States:
IDLE
IN_FOV
HUNTING
FLIP_SIDE
FOUND
