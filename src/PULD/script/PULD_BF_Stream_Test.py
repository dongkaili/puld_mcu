from multiprocessing.connection import Client
import time
import json
import random

# https://stackoverflow.com/questions/20309456/call-a-function-from-another-file
# https://docs.python.org/3/library/multiprocessing.html
# https://www.programcreek.com/python/example/55597/multiprocessing.connection.Client

while True:
    try: # exception at next steps means server is unavailable so go try again (timeout = 1s)
        conn = Client(('localhost', 6001), authkey=b'secret password')
        try: # we have a connection so exception at next step should close it
            while conn.poll(.01):
                mssg = conn.recv()
                print(mssg)
            conn.close()
        except:
            conn.close()
    except:
        None