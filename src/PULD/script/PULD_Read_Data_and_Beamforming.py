# Code to show two plots of simulated streamed data. Data for each plot is processed (generated) 
# by separate threads, and passed back to the gui thread for plotting.
# This is an example of using movetothread, which is the correct way of using QThreads

# Michael Hogg, 2015

import time, sys, math, json, timeit

from statistics import mode, mean, median

from PyQt5.QtCore import Qt
from numpy.lib.function_base import median
from numpy.lib.type_check import imag
from pyqtgraph.Qt import QtGui, QtCore
from PyQt5.Qt import QMutex
import pyqtgraph as pg
from PyQt5.QtWidgets import (
    QApplication,
    QLabel,
    QLineEdit,
    QComboBox,
    QCheckBox,
    QMainWindow,
    QPushButton,
    QRadioButton,
    QVBoxLayout,
    QGridLayout,
    QWidget,
    QFileDialog
)
from random import randint
from copy import copy

import serial
from serial.tools.list_ports import comports
from serial.tools import hexlify_codec

import numpy as np
from scipy.fft import rfft, rfftfreq



class SerialTransmitter(QtCore.QObject):
    
    def __init__(self, serial_instance, parent=None):
        super().__init__(parent)
        self.serial_instance = serial_instance
        
    def send_command_string(self, letter, command_type):
        """Write Micro Jason string to sys.stderr"""
        """
        HH(0x11), VV(Version Byte), TH(0x00: Raw, 0x01:Json), TL(0x00~0x04), Length(4 bytes, LSB first?), checksum, Reserve1, R2, R3, <data>
        
        """
        header    = 0x11
        #print(header.to_bytes(1, 'big'))
        version   = 0x02
        
        if command_type == None:
            if 'i2s_raw' in letter:
                type_high = 0x00
                type_low  = 0x07
            elif 'i2s_soft_fft' in letter or 'i2s_hard_fft' in letter:
                type_high = 0x00
                type_low  = 0x04
            elif 'i2s_fft_raw' in letter:
                type_high = 0x00
                type_low =  0x08
            elif 'dir_raw' in letter:
                type_high = 0x00
                type_low =  0x09
            elif 'voc_raw' in letter:
                type_high = 0x00
                type_low =  0x10
            elif 'voc_fft' in letter:
                type_high = 0x00
                type_low =  0x12
            elif '{' in letter and '}' in letter:
                type_high = 0x01
                type_low  = 0x01
        
            
        length_1  = 0x00
        length_2  = 0x00
        length_3  = 0x00
        length_4  = 0x00
        reserve_1 = 0x00
        reserve_2 = 0x00
        reserve_3 = 0x00
        
                
        cmd_string = list(letter)
        #print("Cmd: {}".format(cmd_string))
        
        length_1 = len(cmd_string) & 0xFF
        length_2 = (len(cmd_string) >> 8) & 0xFF
        length_3 = (len(cmd_string) >> 16) & 0xFF
        length_4 = (len(cmd_string) >> 24) & 0xFF
        
        calculated_checksum = 0
        check_sum = 0
        pay_load = []
        for i in range(0, len(cmd_string)):
            pay_load.append(ord(cmd_string[i]))
            calculated_checksum += pay_load[i]
        
        
        #calculated_checksum = header + version + type_high + type_low + length_1 + length_2 + length_3 + length_4 + reserve_1 + reserve_2 + reserve_3 + reserve_4
        
        calculated_checksum = calculated_checksum & 0xFF
        calculated_checksum = 0xFF - calculated_checksum
        
        check_sum = calculated_checksum
        #print('check_sum is ' + hex(check_sum))
        
        tx_data = []
        tx_data.append(header.to_bytes(1, 'big'))
        tx_data.append(version.to_bytes(1, 'big'))
        tx_data.append(type_high.to_bytes(1, 'big'))
        tx_data.append(type_low.to_bytes(1, 'big'))
        tx_data.append(length_1.to_bytes(1, 'big'))
        tx_data.append(length_2.to_bytes(1, 'big'))
        tx_data.append(length_3.to_bytes(1, 'big'))
        tx_data.append(length_4.to_bytes(1, 'big'))
        tx_data.append(check_sum.to_bytes(1, 'big'))
        tx_data.append(reserve_1.to_bytes(1, 'big'))
        tx_data.append(reserve_2.to_bytes(1, 'big'))
        tx_data.append(reserve_3.to_bytes(1, 'big'))

        
        #for i in range(0, len(pay_load)):
        for item in pay_load:
            tx_data.append(item.to_bytes(1, 'big'))        
               
        for i in range(0, len(tx_data)):
            #print(tx_data[i])
            self.serial_instance.write((tx_data[i]))



class DataReader(QtCore.QObject):
    newData  = QtCore.pyqtSignal(object)
    finished = QtCore.pyqtSignal()
    
    def __init__(self, serial_instance, parent=None):
        super().__init__(parent)
        self.serial = serial_instance
        self.recv_bytes_list = []
        
    def run(self):
        """Long Run task 1 to receive incoming data on serial port"""
        try:
            while True:
                # read all that is there or wait for one byte
                data = self.serial.read(self.serial.in_waiting)
                #print(type(data))
                #data = self.serial.read()
                if data:
                    new_recv_bytes = list(data)
                    #self.recv_bytes_list.append(new_recv_bytes)
                    self.newData.emit(new_recv_bytes)    
        except serial.SerialException:
            print('serial has exception')
        
        
 

class DataSender(QtCore.QObject):

    newData  = QtCore.pyqtSignal(object)
    finished = QtCore.pyqtSignal()
    
    #def __init__(self,parent=None,sizey=100,rangey=[0,100],delay=1000):
    def __init__(self, serial_transmitter,  delay=1000, parent=None):
        QtCore.QObject.__init__(self)
        self.parent = parent
        self.serial_tx = serial_transmitter
        self.delay  = delay
        self.mutex  = QMutex()        
        self.run    = True    
        self.cmd_index = 0
        self.recv_is_done = True
        self.start_time = time.time()
        self.enable_cycle_send = False # initially disabled
        self.all_cmd_list = []
        
    def sendCommand(self):
        while self.run:
            try:
                
                self.mutex.lock()        
                if self.enable_cycle_send == True and self.recv_is_done == True:
                    #print('send command: {}'.format(self.all_cmd_list[self.cmd_index]))
                    #self.serial_tx.send_command_string(self.command_list[self.cmd_index], 'DIR_RAW_Type')  
                    self.start_time = time.time()
                    self.serial_tx.send_command_string(self.all_cmd_list[self.cmd_index], None)  
                    
                    self.recv_is_done = False
                    self.newData.emit(self.all_cmd_list[self.cmd_index])
                    self.cmd_index = self.cmd_index + 1
                    if (self.cmd_index >= len(self.all_cmd_list)):
                        self.cmd_index = 0
                self.mutex.unlock() 
                
                
                QtCore.QThread.msleep(self.delay)
                
            except serial.SerialException: 
                print('exception occurs in send thread')
        
        
class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        #MainWindow.resize(1600, 960)
        MainWindow.setMinimumSize(QtCore.QSize(1800, 1000))
        
        main_layout = pg.LayoutWidget()
        pg.setConfigOption('background', 'k')
        pg.setConfigOption('foreground', 'w')
        
        #self.symbols = ['o', 't', '+', 't1', 'star', 't2', 'd', 'x', 't1', 't2', 't3', 'o', 't', 'p', 'h', 'star']
        self.symbols = ['o', 't', '+', 'h', 'star', 'p', 'd', 'x', 't1', 't2', 't3', 'o', 't', 'p', 'h', 'star']
        
        self.pw1 = pg.PlotWidget(title="I2S Raw")
        self.pw1_plot_item = self.pw1.getPlotItem()
        self.pw1_plot_item.addLegend()
        self.pw1_plot_item.enableAutoRange('x', True)
        self.pw1_plot_item.enableAutoRange('y', True)
        #self.pw1_plot_item.setXRange(100, 124)
        
        self.curve_i2s_raw = [None] * 8
        for i in range(8):
            self.curve_i2s_raw[i] = self.pw1.plot(pen=(i, 8), name='raw {}'.format(i), symbol=self.symbols[i])
            #self.curve_i2s_raw[i] = self.pw1.plot(pen=(i, 8), name='raw {}'.format(i+1))
        
        
        
        self.pw1_plot_item.showGrid(True, True, 0.5)
            
        self.pw1.setLabel('left', 'Power', units='')
        self.pw1.setLabel('bottom', 'Time', units='Sample')
        main_layout.addWidget(self.pw1, row=0, col=5, rowspan=1, colspan=5)
        
        
        self.pw2 = pg.PlotWidget(title="FFT Power")
        self.pw2_plot_item = self.pw2.getPlotItem()
        self.pw2_plot_item.addLegend()
        self.pw2_plot_item.showGrid(True, True)
        self.curve_fft_power = [None] * 16      
        
        for i in range(len(self.curve_fft_power)):
            #self.curve_fft_power[i] = self.pw2.plot(pen=(i, 8), name='fft {}'.format(i), symbol=self.symbols[i])
            self.curve_fft_power[i] = self.pw2.plot(pen=(i, 8), name='fft {}'.format(i+1), symbol=self.symbols[i])
                
        self.pw2.setLabel('left', 'Power', units='')
        self.pw2.setLabel('bottom', 'Frequency', units='Hz')  
        main_layout.addWidget(self.pw2, row=1, col=5, rowspan=1, colspan=5)
        
        
        
        self.pw3 = pg.PlotWidget(title="DIR Raw")
        self.pw3_plot_item = self.pw3.getPlotItem()
        self.pw3_plot_item.addLegend()
        self.pw3_plot_item.showGrid(True, True)
        self.curve_dir_raw = [None] * 17
        #for i in range(16):
            #self.curve_dir_raw[i] = self.pw3.plot(pen=(i, 16), name='dir {}'.format(i), symbol=self.symbols[i], symbolPen=(i,16))
            #self.curve_dir_raw[i] = self.pw3.plot(pen=(i, 16), name='dir {}'.format(i))
        
        self.curve_dir_raw[0] = self.pw3.plot(pen=(1, 16), name='dir {}'.format(0))
        self.curve_dir_raw[1] = self.pw3.plot(pen=(2, 16), name='dir {}'.format(1))
        self.curve_dir_raw[2] = self.pw3.plot(pen=(3, 16), name='dir {}'.format(2))
        self.curve_dir_raw[3] = self.pw3.plot(pen=(4, 16), name='dir {}'.format(3))
        self.curve_dir_raw[4] = self.pw3.plot(pen=(5, 16), name='dir {}'.format(4))
        self.curve_dir_raw[5] = self.pw3.plot(pen=(6, 16), name='dir {}'.format(5), symbol='+')
        self.curve_dir_raw[6] = self.pw3.plot(pen=(7, 16), name='dir {}'.format(6), symbol='o')
        self.curve_dir_raw[7] = self.pw3.plot(pen=(8, 16), name='dir {}'.format(7), symbol='t')
        self.curve_dir_raw[8] = self.pw3.plot(pen=(9, 16), name='dir {}'.format(8))
        self.curve_dir_raw[9] = self.pw3.plot(pen=(10, 16), name='dir {}'.format(9))
        self.curve_dir_raw[10] = self.pw3.plot(pen=(11, 16), name='dir {}'.format(10))
        self.curve_dir_raw[11] = self.pw3.plot(pen=(12, 16), name='dir {}'.format(11))
        self.curve_dir_raw[12] = self.pw3.plot(pen=(13, 16), name='dir {}'.format(12))
        self.curve_dir_raw[13] = self.pw3.plot(pen=(14, 16), name='dir {}'.format(13))
        self.curve_dir_raw[14] = self.pw3.plot(pen=(15, 16), name='dir {}'.format(14))
        self.curve_dir_raw[15] = self.pw3.plot(pen=(16, 16), name='dir {}'.format(15))
        self.curve_dir_raw[16] = self.pw3.plot(pen=(16, 16), name='dir {}'.format(16))
        

        
        
        
        self.pw3.setLabel('left', 'Amplitude', units='')
        self.pw3.setLabel('bottom', 'Time', units='Sample')
        main_layout.addWidget(self.pw3, row=0, col=0, rowspan=1, colspan=5)
        
        
        
        self.pw4 = pg.PlotWidget(title="FFT Phase (Polar View)")
        self.pw4_plot_item = self.pw4.getPlotItem()
        self.pw4_plot_item.setAspectLocked()
        self.pw4_plot_item.addLegend()
        #self.pw4_plot_item.showGrid(True, True)
        self.pw4_plot_item.addLine(x=0, pen=0.3)
        self.pw4_plot_item.addLine(y=0, pen=0.3)
        
        
        radius_range = 120
        
        for r in np.arange(0.0, radius_range, 5.0):
            circle = pg.QtGui.QGraphicsEllipseItem(-r, -r, r * 2, r * 2)
            circle.setPen(pg.mkPen(0.3))
            self.pw4_plot_item.addItem(circle)
        
        degree_symbol = u"\u00b0"
        deg0Text = pg.TextItem('0' + degree_symbol)
        self.pw4_plot_item.addItem(deg0Text)
        deg0Text.setPos(0, radius_range*1.05) 
        
        deg45Text = pg.TextItem('45' + degree_symbol)
        self.pw4_plot_item.addItem(deg45Text)
        deg45Text.setPos(int(radius_range*0.71), int(radius_range*0.71))
        
        deg90Text = pg.TextItem('90' + degree_symbol)
        self.pw4_plot_item.addItem(deg90Text)
        deg90Text.setPos(radius_range, 0)
        
        deg135Text = pg.TextItem('135' + degree_symbol)
        self.pw4_plot_item.addItem(deg135Text)
        deg135Text.setPos(int(radius_range*0.75), int(radius_range*-0.75))
        
        deg180Text = pg.TextItem('-/+180' + degree_symbol)
        self.pw4_plot_item.addItem(deg180Text)
        deg180Text.setPos(0, -radius_range)
        
        degn45Text = pg.TextItem('-45' + degree_symbol)
        self.pw4_plot_item.addItem(degn45Text)
        degn45Text.setPos(int(radius_range*-0.75), int(radius_range*0.75))
        
        degn90Text = pg.TextItem('-90' + degree_symbol)
        self.pw4_plot_item.addItem(degn90Text)
        degn90Text.setPos(int(radius_range*-1.1), 0)
        
        degn135Text = pg.TextItem('-135' + degree_symbol)
        self.pw4_plot_item.addItem(degn135Text)
        degn135Text.setPos(int(radius_range*-0.75), int(radius_range*-0.75))
        
                
        dLine_1 = pg.InfiniteLine(angle=45, movable=False, pen=0.3)
        dLine_2 = pg.InfiniteLine(angle=135, movable=False, pen=0.3)
        
        self.pw4_plot_item.addItem(dLine_1)
        self.pw4_plot_item.addItem(dLine_2) 
        
        self.curve_fft_phase = [None] * 16              
        
        for i in range(len(self.curve_fft_phase)):
            #self.curve_fft_phase[i] = self.pw4.plot(pen=(i, 16), name='fft {}'.format(i), symbol=self.symbols[i])
            self.curve_fft_phase[i] = self.pw4.plot(pen=(i, 16), name='FFT phase {}'.format(i+1), symbol=self.symbols[i], symbolPen=(i,16))
                
        #self.pw4.setLabel('left', 'Power', units='')
        #self.pw4.setLabel('bottom', 'Frequency', units='Hz')  
        main_layout.addWidget(self.pw4, row=1, col=0, rowspan=1, colspan=5)
        
        
        
        #self.clicksLabel = QLabel()
        #self.clicksLabel.setText('')
        #self.clicksLabel.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                
        #main_layout.addWidget(self.clicksLabel, row=1, col=0)
        
        #self.sendBtn = QPushButton()
        #self.sendBtn.setText('Start Reading Battery')
        #main_layout.addWidget(self.sendBtn, row=2, col=0)
        
        
        #self.secondClickLabel = QLabel()
        #self.secondClickLabel.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        #main_layout.addWidget(self.secondClickLabel, row=1, col=5)
        
        #self.secondSendBtn = QPushButton()
        #self.secondSendBtn.setText('Read F/W Version')
        #main_layout.addWidget(self.secondSendBtn, row=2, col=0)
        
        
        #self.thirdLabel = QLabel()
        #self.thirdLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        #main_layout.addWidget(self.thirdLabel, row=2, col=1)
        
        #newfont = QtGui.QFont("Times", 10, QtGui.QFont.Bold)
        
        self.cmdTextLabel = QLabel()
        #self.cmdTextLabel.setFont(newfont)
        self.cmdTextLabel.setText('Command: ')
        self.cmdTextLabel.setMaximumWidth(130)
        self.cmdTextLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.cmdTextLabel, row=3, col=0)
        
        #self.cmdlineEdit = QLineEdit()
        #self.cmdlineEdit.setText('i2s_raw')
        #self.cmdlineEdit.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        #self.cmdlineEdit.setMaximumWidth(400)
        
        #main_layout.addWidget(self.cmdlineEdit, row=3, col=1, rowspan=1, colspan=1)
        
        self.cmdComboBox = QComboBox()
        self.cmdComboBox.setEditable(True)
        
        
        self.cmdComboBox.addItem('i2s_raw')
        self.cmdComboBox.addItem('i2s_raw -target 1')
        self.cmdComboBox.addItem('i2s_raw -target 2')
        self.cmdComboBox.addItem('--------------------------------------')
        
        self.cmdComboBox.addItem('i2s_soft_fft')
        self.cmdComboBox.addItem('i2s_soft_fft -target 1')
        self.cmdComboBox.addItem('i2s_soft_fft -target 2')
        self.cmdComboBox.addItem('--------------------------------------')
        
        self.cmdComboBox.addItem('i2s_hard_fft')
        self.cmdComboBox.addItem('i2s_hard_fft -target 1')
        self.cmdComboBox.addItem('i2s_hard_fft -target 2')
        self.cmdComboBox.addItem('--------------------------------------')
        
        self.cmdComboBox.addItem('i2s_fft_raw')
        self.cmdComboBox.addItem('i2s_fft_raw -target 1')
        self.cmdComboBox.addItem('i2s_fft_raw -target 2')
        self.cmdComboBox.addItem('--------------------------------------')
        
        self.cmdComboBox.addItem('dir_raw')
        self.cmdComboBox.addItem('dir_raw -target 1')
        self.cmdComboBox.addItem('dir_raw -target 2')
        self.cmdComboBox.addItem('--------------------------------------')
        
        self.cmdComboBox.addItem('voc_raw')
        self.cmdComboBox.addItem('voc_raw -target 1')
        self.cmdComboBox.addItem('voc_raw -target 2')
        self.cmdComboBox.addItem('--------------------------------------')
        
        self.cmdComboBox.addItem('voc_fft')
        self.cmdComboBox.addItem('voc_fft -target 1')
        self.cmdComboBox.addItem('voc_fft -target 2')
        self.cmdComboBox.addItem('--------------------------------------')
        
        
        self.cmdComboBox.addItem('{"CMD":"RST"}')
        self.cmdComboBox.addItem('{"CMD":"PWR", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"PWR", "MCU":2, "STATE":"ENABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"RID"}')
        self.cmdComboBox.addItem('{"CMD":"RBV"}')        
        self.cmdComboBox.addItem('{"CMD":"RBC"}')
        self.cmdComboBox.addItem('{"CMD":"RFV"}')
        
        self.cmdComboBox.addItem('--------------------------------------')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":2, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('--------------------------------------')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":2, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('--------------------------------------')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":2, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('--------------------------------------')
        self.cmdComboBox.addItem('{"CMD":"SET", "MCU":0, "VAR":"Enable_VOC_FFT", "VAL":1}')
        self.cmdComboBox.addItem('{"CMD":"SET", "MCU":0, "VAR":"Enable_VOC_FFT", "VAL":0}')
        
        self.cmdComboBox.addItem('{"CMD":"SET", "MCU":1, "VAR":"Enable_VOC_FFT", "VAL":1}')
        self.cmdComboBox.addItem('{"CMD":"SET", "MCU":1, "VAR":"Enable_VOC_FFT", "VAL":0}')
        
        self.cmdComboBox.addItem('{"CMD":"SET", "MCU":2, "VAR":"Enable_VOC_FFT", "VAL":1}')
        self.cmdComboBox.addItem('{"CMD":"SET", "MCU":2, "VAR":"Enable_VOC_FFT", "VAL":0}')
        
        
        
        
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":0, "STATE":"ENABLE"}')
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":0, "STATE":"DISABLE"}')
        
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":1, "STATE":"ENABLE"}')
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":1, "STATE":"DISABLE"}')
        
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":2, "STATE":"ENABLE"}')
        #self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":2, "STATE":"DISABLE"}')
        
        
        
        #self.cmdComboBox.setMinimumWidth(400)
        main_layout.addWidget(self.cmdComboBox, row=3, col=1, rowspan=1, colspan=1)
        
        self.cmdSendBtn = QPushButton()
        self.cmdSendBtn.setText('Send Command')
        #self.cmdSendBtn.setMaximumWidth(150)
        
        main_layout.addWidget(self.cmdSendBtn, row=3, col=2)
        
        self.cmdStartBtn = QPushButton()
        self.cmdStartBtn.setText('Start Reading+Logging')
        #self.cmdStartBtn.setMaximumWidth(150)
        main_layout.addWidget(self.cmdStartBtn, row=3, col=3)
        
		# used to record the current offset (delay), no longer in use
        self.recordOffsetBtn = QPushButton()
        self.recordOffsetBtn.setText('Record Offset')
        main_layout.addWidget(self.recordOffsetBtn, row=3, col=4)
        
        # self.i2sRawRadioBtn = QRadioButton('I2S Raw')
        # self.i2sRawRadioBtn.setChecked(True)
        # self.i2sRawRadioBtn.command_name = 'i2s_raw'
        # #main_layout.addWidget(self.i2sRawRadioBtn, row=3, col=4)
        
        # self.i2sRawFFTRadioBtn = QRadioButton('I2S Raw+FFT')
        # self.i2sRawFFTRadioBtn.setChecked(False)
        # self.i2sRawFFTRadioBtn.command_name = 'i2s_fft_raw'
        # #main_layout.addWidget(self.i2sRawFFTRadioBtn, row=3, col=5)
        
        #self.dirRadioButton = QRadioButton('DIR Raw')
        #self.radioButton.setChecked(False)
        #self.dirRadioButton.command_name = 'dir_raw'
        #main_layout.addWidget(self.dirRadioButton, row=3, col=6)
        
        
        self.chanMaskLabel = QLabel()
        self.chanMaskLabel.setText('Plot Channel Mask:')
        self.chanMaskLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)                
        main_layout.addWidget(self.chanMaskLabel, row=3, col=5, rowspan=1, colspan=1)
        
        
        self.chanMasklineEdit = QLineEdit()
        self.chanMasklineEdit.setText('0xFF')
        self.chanMasklineEdit.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        self.chanMasklineEdit.setMaximumWidth(200)
        
        main_layout.addWidget(self.chanMasklineEdit, row=3, col=6, rowspan=1, colspan=1)
        
        
        self.mcu0ChkBox = QCheckBox()
        self.mcu0ChkBox.setText('MCU0')
        self.mcu0ChkBox.setChecked(True)
        main_layout.addWidget(self.mcu0ChkBox, row=3, col=7)
        
        
        self.mcu1ChkBox = QCheckBox()
        self.mcu1ChkBox.setText('MCU1')
        main_layout.addWidget(self.mcu1ChkBox, row=3, col=8)
        
        
        self.mcu2ChkBox = QCheckBox()
        self.mcu2ChkBox.setText('MCU2')
        main_layout.addWidget(self.mcu2ChkBox, row=3, col=9)
        
        
        # self.indexTextLabel = QLabel()
        # self.indexTextLabel.setText('Max FFT Index: ')
        # self.indexTextLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        # #main_layout.addWidget(self.indexTextLabel, row=4, col=5)
        
        self.dirTextLabel = QLabel()
        self.dirTextLabel.setText('Direction: ')
        self.dirTextLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.dirTextLabel, row=4, col=5)
        
        self.dirComboBox = QComboBox()
        self.dirComboBox.setEditable(True)
        for i in range(17):
            self.dirComboBox.addItem('direction {}'.format(i))
        main_layout.addWidget(self.dirComboBox, row=4, col=6)
        
        
        
        self.indexLabel = QLabel()
        self.indexLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        main_layout.addWidget(self.indexLabel, row=4, col=8)
        
        
        self.cmdIndexLabel = QLabel()
        self.cmdIndexLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        main_layout.addWidget(self.cmdIndexLabel, row=4, col=9)
        
        
        self.rspTxtLabel = QLabel()
        self.rspTxtLabel.setText('Command Response: ')
        self.rspTxtLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        #self.rspTxtLabel.setMaximumWidth(140)
        main_layout.addWidget(self.rspTxtLabel, row=4, col=0)
        
        self.rspLabel = QLabel()
        self.rspLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        main_layout.addWidget(self.rspLabel, row=4, col=1, rowspan=1, colspan=3)
        
        MainWindow.setCentralWidget(main_layout)
        
        
        
        
        
class ApplicationWindow(QtGui.QMainWindow):
    
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        
        
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setStyleSheet("QMainWindow {background: 'lightGray';}")
        
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        #self.ui.secondSendBtn.clicked.connect(self.secondSendButtonClicks)
        self.ui.cmdSendBtn.clicked.connect(self.commandSendButtonClicks)
        self.ui.cmdStartBtn.clicked.connect(self.readingStartButtonClicks)
        
        self.ui.recordOffsetBtn.clicked.connect(self.recordOffsetButtonClicks)
        
        #self.ui.i2sRawRadioBtn.toggled.connect(self.commandRadioBtnClicks)
        #self.ui.i2sRawFFTRadioBtn.toggled.connect(self.commandRadioBtnClicks)
        #self.ui.dirRadioButton.toggled.connect(self.commandRadioBtnClicks)
        
        #self.ui.mcu0ChkBox.toggled.connect(self.mcu0CheckBoxClicks)
        #self.ui.mcu1ChkBox.toggled.connect(self.mcu1CheckBoxClicks)
        #self.ui.mcu2ChkBox.toggled.connect(self.mcu2CheckBoxClicks)
        
        
        #finish = QAction("Quit", self)
        #finish.triggered.connect(self.closeEvent)
        self.port = None
        self.baudrate = 115200
        self.parity = 'N'
        self.rtscts = False
        self.xonxoff = False
        self.exclusive = True
        
        
        self.recv_bytes = []  # list of received bytes
        
        
        self.i2s_sample_frequency = 317382
        
        self.i2s_all_raw_bytes = []  # list of the whole i2s raw data
        
        
        self.i2s_all_raw_value = []
        
        self.i2s_chan_raw_value = [[],[],[],[],[],[],[],[]]
        
        
        self.active_chan_number = 0
        self.sample_start = 0
        self.sample_end = 0
        self.channel_mask = 0
		
        self.i2s_chan_abs_sum_value = [0]*8
        self.i2s_chan_sum_value = [0]*8
        self.i2s_chan_avg_value = [0]*8
        
        
        self.i2s_all_fft_bytes = []
        self.i2s_all_fft_value = []
        self.i2s_chan_fft_value = [0]*8
        
        self.i2s_fft_max_value = [0]*8
        
        # calculated FFT power and phase, could be used for i2s or dir data
        self.calc_fft_frequency = [0] * 8
        self.calc_fft_magnitude = [0] * 8
        self.calc_fft_phase = [0] * 8
        self.calc_fft_raw = [None] * 8
        self.calc_fft_phase_angle = [0] * 8
        self.calc_fft_magnitude_max = [0] * 8
        
        self.voc_raw_value = []
        
        
        
        self.polar_x = [0] * 8
        self.polar_y = [0] * 8
        
        self.polar_x_value_list = [[],[],[],[],[],[],[],[]]
        self.polar_y_value_list = [[],[],[],[],[],[],[],[]]
        
        self.display_rate_sample_count = 1
        self.display_total_sample_count = 1
        
        self.dir_all_raw_bytes = []
        self.dir_all_raw_value = []
        self.dir_chan_raw_value = [0]*16
        
        self.mic_ref = 0
        self.mic_under_test = 4
        
        self.plot_channel_mask = 0xff
        # soft Beamforming direction array
        self.soft_dir_chan_value = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
        self.das_cf_value = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
        
        #self.offsets = [[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8,[0]*8 ]
        self.offsets = [[0] * 8 for i in range(16)]
        '''
        self.offsets = [ 
                        [0,1,3,4,10,8,4,1],                        
                        [0,0,1,3,10,6,4,1],                         
                        [1,3,0,3,0,12,8,6],                       
                        [2,0,0,2,5,8,8,5],                                                 
                        [3,3,0,0,11,11,2,10],                         
                        [5,4,0,0,8,8,6,7],
                        
                        [5,4,1,0,1,4,7,8],
                         
                         [8,5,2,0,0,3,6,8],
                         [8,7,4,1,0,1,4,7],
                         [8,8,5,2,0,0,3,6],
                         [7,8,7,4,1,0,1,4],
                         [6,8,8,5,2,0,0,3],
                         [4,7,8,7,4,1,0,1],
                         [3,6,8,7,5,2,0,0],
                         [2,5,7,8,7,4,1,0],
                         [0,3,6,8,7,5,2,0] ]
        '''                 
        self.offsets_hardware = [ 
        
                        
                        [0, 1, 4, 7, 8, 7, 4, 1],
                        [0, 0, 2, 5, 8, 8, 5, 2],
                        [1, 0, 1, 4, 7, 8, 7, 4],
                        [2, 0, 0, 2, 5, 8, 8, 5],
                        [4, 1, 0, 1, 4, 7, 8, 7],
                        [5, 2, 0, 0, 2, 5, 8, 8],
                        [7, 4, 1, 0, 1, 4, 7, 8],
                        [8, 5, 2, 0, 0, 2, 5, 8],
                        [8, 7, 4, 1, 0, 1, 4, 7],
                        [8, 8, 5, 2, 0, 0, 2, 5],
                        [7, 8, 7, 4, 1, 0, 1, 4],
                        [5, 8, 8, 5, 2, 0, 0, 2],                        
                        [4, 7, 8, 7, 4, 1, 0, 1],
                        [2, 5, 8, 8, 5, 2, 0, 0],
                        [1, 4, 7, 8, 7, 4, 1, 0],
                        [0, 2, 5, 8, 8, 5, 2, 0],
                        [0, 0, 0, 0, 0, 0, 0, 0]
                        
                        
                        ]
         
        self.offsets_new = [
                        [0, 1, 4, 7, 8, 7, 4, 1],
                        [0, 0, 3, 6, 8, 8, 6, 3],
                        [1, 0, 1, 4, 7, 8, 7, 4],
                        [3, 0, 0, 3, 6, 8, 8, 6],
                        [4, 1, 0, 1, 4, 7, 8, 7],
                        [6, 3, 0, 0, 3, 6, 8, 8],
                        [7, 4, 1, 0, 1, 4, 7, 8],
                        [8, 6, 3, 0, 0, 3, 6, 8],
                        [8, 7, 4, 1, 0, 1, 4, 7],
                        [8, 8, 6, 3, 0, 0, 3, 6],
                        [7, 8, 7, 4, 1, 0, 1, 4],
                        [6, 8, 8, 6, 3, 0, 0, 3],
                        [4, 7, 8, 7, 4, 1, 0, 1],
                        [3, 6, 8, 8, 6, 3, 0, 0],
                        [1, 4, 7, 8, 7, 4, 1, 0],
                        [0, 3, 6, 8, 8, 6, 3, 0]
        
                        ] 
        
         
        self.offsets_predefined = [ 
                        # offset based on raw i2s data
                        #[0, 3, 6, 1, 3, 3, 1, 0],   # dir 0, good
                        [0, 1, 5, 7, 1, 0, 0, 6],   
                        
                        #[0, 0, 3, 8, 0, 2, 2, 6],   # dir 1, good
                        [0, 0, 4, 6, 1, 1, 0, 6],
                        
                        #[0, 0, 3, 6, 2, 3, 2, 6],   # dir 2, good
                        [1, 0, 2, 4, 1, 1, 2, 0],
                        
                        #[1, 0, 0, 3, 7, 2, 2, 1],   # dir 3, good
                        [2, 0, 1, 3, 7, 1, 2, 1],
                        
                        #[2, 0, 0, 3, 7, 2, 4, 3],   # dir 4, good
                        [4, 1, 0, 1, 4, 7, 1, 1],
                        
                        #[2, 0, 0, 0, 3, 7, 2, 1],   # dir 5, good
                        [4, 1, 0, 0, 3, 5, 1, 1],
                        
                        #[4, 2, 1, 0, 4, 6, 2, 7],   # dir 6, good  
                        [5, 2, 1, 0, 2, 5, 1, 2],
                        
                        #[6, 5, 2, 0, 0, 2, 3, 3],   # dir 7, good
                        [5, 3, 2, 0, 0, 1, 6, 0],
                        
                        #[5, 5, 3, 5, 0, 3, 1, 5],    # dir 8, good
                        [5, 4, 3, 0, 0, 1, 5, 0],
                        
                        #[7, 6, 6, 0, 0, 0, 3, 8],    # dir 9, good
                        [5, 4, 3, 1, 0, 0, 4, 6],
                        
                        #[8, 4, 7, 2, 4, 0, 0, 3],    # dir 10, good
                        [3, 5, 5, 2, 1, 0, 3, 5],
                        
                        #[7, 2, 7, 3, 5, 0, 0, 1],    # dir 11, good
                        [2, 4, 5, 3, 2, 0, 1, 2],
                        #[3, 4, 5, 4, 2, 0, 0, 3],
                        
                        #[6, 2, 4, 5, 5, 1, 0, 0],    # dir 12, good
                        [0, 2, 4, 4, 2, 0, 0, 1],
                        
                        #[3, 4, 3, 6, 4, 1, 0, 0],    # dir 13, good
                        [6, 0, 4, 4, 4, 1, 0, 0],
                        
                        #[3, 7, 3, 6, 7, 4, 2, 0],    # dir 14, good
                        [5, 7, 3, 4, 4, 2, 1, 0],
                        
                        #[2, 6, 2, 4, 2, 5, 3, 0],    # dir 15, good
                        #[2, 5, 1, 4, 7, 4, 2, 0],
                        [4, 7, 2, 4, 5, 3, 2, 0],
                        
                        [0, 0, 0, 0, 0, 0, 0, 0]  ]  # dir 16 is the direction for on-target 
        
        # for i in range(16):
        #     for j in range(8):
        #         self.offsets_predefined[i][j] = self.offsets_hardware[i][j]
        
        self.apu_set_delay(self.i2s_sample_frequency, 0.437, 180, 8, 0)
        
        self.offsets = self.apu_calc_delays_azimuth(self.i2s_sample_frequency, 0.437, 8, 180)
        
        self.record_offset_flag = False
        
        self.current_direction = 0
        
        self.offsets_calculated = [ [0]*8 ] * 17
         
        self.soft_dir_sum = [0]*17
        
        self.do_beamforming = False
        
        self.data_length = 0
        self.data_type = 0
        
        self.y_display_max = 0.0
        
        self.log_file_name = None
        self.log_file_in_writing = False
        self.log_file = None
        self.scan_count = 0
        
        try:
            self.port = self.ask_for_port()
        except KeyboardInterrupt:
            self.port = None
        
        
        try:
            self.serial_instance = serial.serial_for_url(
                self.port,
                self.baudrate,
                parity=self.parity,
                rtscts=self.rtscts,
                xonxoff=self.xonxoff,
                do_not_open=True,
                timeout= 8.0)

            #if not hasattr(self.serial_instance, 'cancel_read'):
                # enable timeout for alive flag polling if cancel_read is not available
            #    self.serial_instance.timeout = 1
            
            if isinstance(self.serial_instance, serial.Serial):
                self.serial_instance.exclusive = self.exclusive

            self.serial_instance.open()
            if self.serial_instance.is_open == True:
                print('port {} is opened successfully!'.format(self.port))           
            else:
                print('Failed to open port {}!'.format(self.port))   
                return
                
            
        except serial.SerialException as e:
            sys.stderr.write('could not open port {!r}: {}\n'.format(self.port, e))
            
        
        self.serial_instance.flushInput()
        
        self.serial_transmitter = SerialTransmitter(self.serial_instance)
        
        
        # first task of cycle reading 
        #self.x1 = range(0,100)
        self.thread_send = QtCore.QThread()
        self.sender   = DataSender(self.serial_transmitter, 200)
        self.sender.moveToThread(self.thread_send)
        self.thread_send.started.connect(self.sender.sendCommand)
        self.sender.finished.connect(self.thread_send.quit)
        self.sender.finished.connect(self.sender.deleteLater)
        self.thread_send.finished.connect(self.thread_send.deleteLater)
        self.sender.newData.connect(self.processSendNewData)
        self.thread_send.start()
        
        
        
        """Long-running task in 5 steps."""
        # Step 2: Create a QThread object
        self.thread_read = QtCore.QThread()
        # Step 3: Create a worker object
        self.reader = DataReader(self.serial_instance)
        # Step 4: Move worker to the thread
        self.reader.moveToThread(self.thread_read)
        # Step 5: Connect signals and slots
        self.thread_read.started.connect(self.reader.run)
        self.reader.finished.connect(self.thread_read.quit)
        self.reader.finished.connect(self.reader.deleteLater)
        self.thread_read.finished.connect(self.thread_read.deleteLater)
        self.reader.newData.connect(self.processNewData)
        # Step 6: Start the thread
        self.thread_read.start()
        
           
           
    
        

        # Final resets
        #self.longRunningBtn.setEnabled(False)
        #self.thread_read.finished.connect(
        #    lambda: self.longRunningBtn.setEnabled(True)
        #)
        #self.thread.finished.connect(
        #    lambda: self.stepLabel.setText("Long-Running Step: 0")
        #)
       
       
    def ask_for_port(self):
        """\
        Show a list of ports and ask the user for a choice. To make selection
        easier on systems with long device names, also allow the input of an
        index.
        """
        sys.stderr.write('\n--- Available ports:\n')
        ports = []
        for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
            sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
            ports.append(port)
            if 'USB Serial Port' in desc:
                return port
        
        while True:
            port = input('--- Enter port index or full name: ')
            try:
                index = int(port) - 1
                if not 0 <= index < len(ports):
                    sys.stderr.write('--- Invalid index!\n')
                    continue
            except ValueError:
                pass
            else:
                port = ports[index]
            return port
       
       
    def apu_calc_single_delay(self, normalized_delay, FOV, n_mic, mic, dir_frac):
        angle = 360.0*(float(mic) / n_mic - dir_frac) * np.pi / 180.0
        delay = (int)(normalized_delay * (1 - np.cos(angle)) * np.sin(FOV / 2 * np.pi / 180.0) + 0.5)
        return delay
    
         
    def apu_calc_delays_azimuth(self, i2s_fs, radius, n_mic, FOV):
        delay = [[0]*8]*16
        cm_tick = 340.0 * 100 / i2s_fs
        normalized_delay =  radius / cm_tick
        print('New Version of offsets for azimuth = 180 deg')
        for dir in range(16):
            for mic in range(n_mic):
                delay[dir][mic] = self.apu_calc_single_delay(normalized_delay, FOV, n_mic, mic, float(dir) / 16.0)
            print(delay[dir])
        return delay    
            
            
    def apu_set_delay(self, i2s_fs, radius, FOV, mic_num_a_circle, center):
        seta = [0]*8
        delay = [0]*8
        
        cm_tick = 340.0 * 100 / i2s_fs
        for i in range(mic_num_a_circle):
            seta[i] = 360.0 * i / mic_num_a_circle
            hudu_jiao = seta[i] * np.pi / 180.0
            delay[i] = radius * (1 - np.cos(hudu_jiao)) * np.sin((FOV / 2 * np.pi / 180.0)) / cm_tick
            
        if center == 1:
            delay[mic_num_a_circle] = radius / cm_tick
            
        for i in range(mic_num_a_circle + center):
            self.offsets[0][i] = int(delay[i] + 0.5)
        
        #print('delay array is {}'.format(delay))

        for j in range(1, 16):
            for i in range(mic_num_a_circle):
                seta[i] = seta[i] - 360.0 / 16
                hudu_jiao = seta[i] * np.pi / 180.0
                delay[i] = radius * (1 - np.cos(hudu_jiao)) * np.sin(FOV / 2 * np.pi / 180.0) / cm_tick
            
            min = 2 * radius
            for i in range(mic_num_a_circle):
                if(delay[i] < min):
                    min = delay[i]
            
            if min != 0:
                for i in range(mic_num_a_circle + center):
                    delay[i] = delay[i] - min
            
            for i in range(mic_num_a_circle + center):
                self.offsets[j][i] = int(delay[i] + 0.5)
        
        print('Old version of hard offsets:')
        for j in range(16):
            print(self.offsets[j])
            
            
    
    def processSendNewData(self, var):
        send_cmd = str(var)
        self.ui.cmdIndexLabel.setText(send_cmd)
        # if 'voc' in send_cmd:
        #     self.ui.pw1_plot_item.setTitle('VOC Raw')
        
       
       
    def convert_bytes_to_int16_value(self, bytes_list):
        value_list = []
        temp_value = None
        for i in range(0, len(bytes_list)//2):
            temp_value = bytes_list[2*i] + (bytes_list[2*i+1] << 8)                        
            if (temp_value & 0x8000):
                value_list.append(temp_value - 65536)
            else:
                value_list.append(temp_value)
        return value_list
       
    def convert_bytes_to_uint32_value(self, bytes_list):
        value_list = []
        temp_value = None
        for i in range(0, len(bytes_list)//4):
            temp_value = bytes_list[4*i] + (bytes_list[4*i+1] << 8) + (bytes_list[4*i+2] << 16) + (bytes_list[4*i+3] << 24)                        
            value_list.append(temp_value)
        return value_list
        
        
        
    def processI2sRawData(self):
        self.i2s_all_raw_value = []
        self.i2s_all_raw_bytes = self.recv_bytes[12:]
        self.recv_bytes = []
        #print(len(self.i2s_all_raw_bytes))
        self.i2s_all_raw_value = self.convert_bytes_to_int16_value(self.i2s_all_raw_bytes)
        
        #print('length of all received data: {}'.format(len(self.i2s_all_raw_value)))
        i2s_channel_data_length = int(len(self.i2s_all_raw_value) / self.active_chan_number)
        #print('channel length: {}'.format(i2s_channel_data_length))
        if i2s_channel_data_length == 0:
            self.sender.recv_is_done = True
            print('received data length is 0!')
            return
        
        self.plot_channel_mask = int(self.ui.chanMasklineEdit.text(), base=16)
            # plot channel mask should has less and equal number of bit 1 than the channel mask
        self.plot_channel_mask = self.plot_channel_mask & self.channel_mask
            
        j = 0
        for i in range(8):
            if ((self.plot_channel_mask >> i) & 0x01) != 0:
                
                #print('{}th bit, {}'.format(i, j))
                self.i2s_chan_raw_value[i] = self.i2s_all_raw_value[j * i2s_channel_data_length : j * i2s_channel_data_length + i2s_channel_data_length]
                #print('elapsed time of reading raw data with size {0} is {1}'.format(len(self.i2s_all_raw_value), (time.time() - self.sender.start_time)))
                self.ui.curve_i2s_raw[i].setData(range(self.sample_start, self.sample_start+len(self.i2s_chan_raw_value[i])), self.i2s_chan_raw_value[i])
            
                j = j + 1
            else:
                self.i2s_chan_raw_value[i] = []
                self.ui.curve_i2s_raw[i].setData([], [])
            self.ui.pw1_plot_item.setXRange(self.sample_start, self.sample_end + 1)  
            
        print('elapsed time of reading raw format i2s data with size {0} is {1:.6f} seconds'.format(len(self.i2s_all_raw_value), (time.time() - self.sender.start_time)))
        
        
        
    def processI2sRawDataWithBeamforming(self):
        self.i2s_all_raw_value = []
        self.i2s_all_raw_bytes = self.recv_bytes[12:]
        #self.i2s_raw_all_array = self.recv_byte_array[12:]
        self.recv_bytes = []
        #self.recv_byte_array = np.array([], int)
        
        #print(self.i2s_raw_all_array) # debug
        
        #print(len(self.i2s_raw_all_array))
        #print('Receive I2S Raw Data!')
        self.i2s_all_raw_value = self.convert_bytes_to_int16_value(self.i2s_all_raw_bytes)
        
        #print('length of all received data: {}'.format(len(self.i2s_raw_all_value)))
        i2s_channel_data_length = int(len(self.i2s_all_raw_value) / self.active_chan_number)
        #print('channel length: {}'.format(i2s_channel_data_length))  # debug
        #print('elapsed time of reading raw data with size {0} is {1}'.format(len(self.i2s_all_raw_value), (time.time() - self.sender.start_time)))
        
        i2s_raw_cut_sample_start = 0
        i2s_raw_cut_sample_end = 512
        i2s_raw_sample_count = i2s_raw_cut_sample_end - i2s_raw_cut_sample_start
        fft_raw = [[],[],[],[],[],[],[],[]]
        #i2s_fft_raw = np.array([[],[],[],[],[],[],[],[]], np.int16)
        threshold = [0,0,0,0,0,0,0,0]
        fft_threshold = np.zeros((8,), np.int16)
        
        #print('channel mask is {}'.format(self.channel_mask))
        #print(self.i2s_all_raw_value)
        #print('the size of whole i2s data: %d' % len(self.i2s_all_raw_value))
        
        """slice the whole received raw data into channel data"""
        j = 0
        for i in range(8):
            if ((self.channel_mask >> i) & 0x01) != 0:
                #self.i2s_chan_raw_value[i] = self.i2s_all_raw_value[i*512 + i2s_raw_cut_sample_start : i*512 + i2s_raw_cut_sample_end]
                self.i2s_chan_raw_value[i] = self.i2s_all_raw_value[j * i2s_channel_data_length : j * i2s_channel_data_length + i2s_channel_data_length]
                #print(self.i2s_chan_raw_value[i]) #debug
                #self.ui.curve_i2s_raw[i].setData(range(self.sample_start, self.sample_start+len(self.i2s_chan_raw_value[i])), self.i2s_chan_raw_value[i])
                        
                j = j + 1
            else:
                self.i2s_chan_raw_value[i] = []
                #self.ui.curve_i2s_raw[i].setData([], [])
                                
            #self.i2s_raw_chan_value[i] = np.append(self.i2s_raw_chan_value[i], self.i2s_raw_all_value[i*512 + i2s_raw_cut_sample_start : i*512 + i2s_raw_cut_sample_end] )
            # print('{0} channel data is of size {1}'.format(i, len(self.i2s_chan_raw_value[i]))) # debug
        
        """normalize raw i2s data by dividing the maximum magnitude"""
        for i in range(8):
            #i2s_abs = [abs(item) for item in self.i2s_chan_raw_value[i]]
            i2s_abs = np.abs(self.i2s_chan_raw_value[i])
            #i2s_abs = np.abs(self.i2s_raw_chan_value[i])
            #print(i2s_abs)
            i2s_abs_max = max(i2s_abs)
            i2s_abs_max = max(i2s_abs_max, 1e-10)  # avoid all 0 case
            # print(i2s_abs_max)
            self.i2s_chan_raw_value[i] = [float(item) / i2s_abs_max for item in self.i2s_chan_raw_value[i]]
        
        
        """Calculate FFT power and phase """
        for i in range(8):
            # doing a soft FFT calculation
            self.calc_fft_raw[i] = rfft(self.i2s_chan_raw_value[i])
            self.calc_fft_magnitude[i] = np.abs(self.calc_fft_raw[i])
            self.calc_fft_frequency[i] = rfftfreq(i2s_raw_sample_count, 1 / self.i2s_sample_frequency)
            fft_raw[i] = copy(self.calc_fft_raw[i])
            #print(fft_raw[i][0:5])
            #fft_abs = [abs(item) for item in self.i2s_chan_raw_value[i]]
            fft_abs = np.abs(self.calc_fft_raw[i])
            #print(max(fft_abs))
            threshold[i] = max(fft_abs) - 2 #max(fft_abs) / 10
            for j in range(len(self.calc_fft_raw[i])):
                if abs(self.calc_fft_raw[i][j]) < threshold[i]:
                    fft_raw[i][j] = 0
            
            #print(fft_raw[i])
            self.calc_fft_phase[i] = []
            for j in range(len(self.calc_fft_raw[i])):
                self.calc_fft_phase[i].append(math.atan2(np.imag(fft_raw[i][j]), np.real(fft_raw[i][j])) * 180 / math.pi)
            
            self.calc_fft_phase_angle[i] = max(self.calc_fft_phase[i], key=abs)
            self.calc_fft_magnitude_max[i] = max(self.calc_fft_magnitude[i])
            
            self.polar_x[i] = self.calc_fft_magnitude_max[i] * np.sin(self.calc_fft_phase_angle[i] / 180.0 * np.pi)
            self.polar_y[i] = self.calc_fft_magnitude_max[i] * np.cos(self.calc_fft_phase_angle[i] / 180.0 * np.pi)
    
    
            self.polar_x_value_list[i].append(self.polar_x[i])
            self.polar_y_value_list[i].append(self.polar_y[i])
            
            #print(self.calc_fft_raw[i])
            #chan_abs_sum = 0
            #for j in range(len(self.i2s_chan_raw_value[i])):
                #print(abs(self.i2s_chan_raw_value[i][j]))
                #chan_abs_sum = chan_abs_sum + abs(self.i2s_chan_raw_value[i][j])
                #chan_sum = chan_sum + self.i2s_chan_raw_value[i][j]
            
            
            #self.i2s_chan_abs_sum_value[i] = chan_abs_sum
            #print(len(self.i2s_chan_raw_value[i]))
            #if i == self.mic_ref or i == self.mic_under_test:
            
        """Plot raw i2s data and its FFT power and phase """    
        for i in range(8):
            # plot mask is only for masking plot
            self.plot_channel_mask = int(self.ui.chanMasklineEdit.text(), base=16)
            # plot channel mask should has less and equal number of bit 1 than the channel mask
            self.plot_channel_mask = self.plot_channel_mask & self.channel_mask
            
            if (self.plot_channel_mask >> i) & 0x01:
                self.ui.curve_i2s_raw[i].setData(range(0, len(self.i2s_chan_raw_value[i])), self.i2s_chan_raw_value[i])
                self.ui.curve_fft_power[i].setData(self.calc_fft_frequency[i], self.calc_fft_magnitude[i])
                
                #self.ui.curve_fft_phase[i].setData(self.calc_fft_frequency[i], self.calc_fft_phase[i])
                if len(self.polar_x_value_list[i]) % self.display_rate_sample_count == 0:
                    self.ui.curve_fft_phase[i].setData(self.polar_x_value_list[i], self.polar_y_value_list[i])
                    
            else:
                self.ui.curve_i2s_raw[i].setData([], [])
                self.ui.curve_fft_power[i].setData([], [])
                self.ui.curve_fft_phase[i].setData([], [])
            
            if len(self.polar_x_value_list[i]) == self.display_total_sample_count:
                self.polar_x_value_list[i] = self.polar_x_value_list[i][self.display_rate_sample_count:]
                self.polar_y_value_list[i] = self.polar_y_value_list[i][self.display_rate_sample_count:]
                
                #print('{} phase is {}'.format(i, self.calc_fft_phase_angle[i]))
        
        """calculate the offsets """
        # dynamically calculate the optimal offset
        range_start = [0]*8
        range_end = [0]*8
        search_start = 100
        search_range = int(self.i2s_sample_frequency / 40000)
        i2s_raw_segment = [[],[],[],[],[],[],[],[]]
        for i in range(8):
            i2s_raw_segment[i] = self.i2s_chan_raw_value[i][search_start:search_start+search_range]
        
        # debug to decide on-target condition
            relative_start = i2s_raw_segment[i].index(max(i2s_raw_segment[i]))
            range_start[i] = relative_start + search_start
            #print(range_start[i])
            #range_end[0] = i2s_raw_segment[0].index(max(i2s_raw_segment[0][range_start[0]+1:range_start[0]+10]))
            #print(range_end[0])
        for i in range(8):
            i2s_raw_segment[i] = self.i2s_chan_raw_value[i][range_start[i]+1:range_start[i]+1+search_range]
            
            relative_end = i2s_raw_segment[i].index(max(i2s_raw_segment[i]))
            range_end[i] = relative_end + range_start[i] + 1
            #print(range_start[i])
            #print(range_end[i])
        
        #print(range_start)
        #print(range_end)
        #print(np.std(range_start))
        #print(np.std(range_end))
    
        # pair_number = 0
        # for i in range(0, 8):
        #     for j in range(i+1, 8):
        #         if range_start[i] == range_start[j]:
        #             pair_number = pair_number + 1
                    
        # print('pair number: {}'.format(pair_number))
        #print('range start {}'.format(range_start))
        offset = [0]*8
        for j in range(16):
            
            
            for i in range(8):
                #offset[i] = range_start[i] - search_start
                #self.offsets_calculated[j][i] = offset[i] - offset[int(j/2)]
                self.offsets_calculated[j][i] = range_start[i] - range_start[int(j/2)]
                #offset[i] = offset[i] - offset[int(j/2)]
                if self.offsets_calculated[j][i] < 0:
                    self.offsets_calculated[j][i] = self.offsets_calculated[j][i] + search_range
            
            #print('offset value for direction {0} is {1}'.format(j, self.offsets_calculated[j]))
        
        for i in range(8):
            self.offsets_calculated[16][i] = range_start[i] - search_start
        
        """ update the offset matrix using the calculated delay value """
        # record the calculated offset and use it in Beamforming immediately
        if self.record_offset_flag == True:
            print('current direction is %s' % self.current_direction)
            for i in range(8):
                self.offsets_predefined[self.current_direction][i] = self.offsets_calculated[self.current_direction][i]
            
            self.record_offset_flag = False
            
            print('direction: {}: offsets: {}'.format(self.current_direction, self.offsets_predefined[self.current_direction]))
            #print(self.offsets_calculated[self.current_direction])
        
        #print(mean(self.offsets_predefined[16]))
        #print(mode(self.offsets_predefined[16]))
        #print(round(median(self.offsets_predefined[16]), 0))
        
        
        # for i in range(8):
        #     #self.offsets_predefined[16][i] = int(round(median(self.offsets_predefined[16]), 0))
        #     if self.offsets_predefined[16][i] != mode(self.offsets_predefined[16]):
        #         self.offsets_predefined[16][i] = int(round(mean(self.offsets_predefined[16]),0))
                
        """Do a soft beamforming using Delay-Multiply-and-Sum Method """
        mic_delayed_value = [0] * 8
        # correct factor value 
        cf_value = [0] * 17
        # direction 17 is the direction vertical to the mic array
        for i in range(17):
            self.soft_dir_chan_value[i] = []
            #self.das_cf_value[i] = []
            for j in range(512):
                for k in range(8):
                    if i != 17:
                        if (j + self.offsets_predefined[i][k]) < 512:
                            mic_delayed_value[k] = self.i2s_chan_raw_value[k][j + self.offsets_predefined[i][k]] 
                        else:
                            mic_delayed_value[k] = 1e-10
                    else:
                        if (j + self.offsets_calculated[i][k]) < 512:
                            mic_delayed_value[k] = self.i2s_chan_raw_value[k][j + self.offsets_calculated[i][k]]
                        else:
                            mic_delayed_value[k] = 1e-10
                
                mic_sum_square = sum(mic_delayed_value) * sum(mic_delayed_value)
                mic_square_sum = sum([a*b for a,b in zip(mic_delayed_value, mic_delayed_value)])
                cf_value[i] = mic_sum_square / (8 * mic_square_sum)
                #print('cf value: {}'.format(cf_value[i]))
                #cf_value[i] = 1
                
                if i != 16:
                    self.soft_dir_chan_value[i].append(sum(mic_delayed_value) * cf_value[i] )
                elif i == 16:
                    self.soft_dir_chan_value[i].append(1/8*sum(mic_delayed_value) * cf_value[i])
                
                
                
            if i in range(17):    
                self.ui.curve_dir_raw[i].setData(range(0, len(self.soft_dir_chan_value[i])), self.soft_dir_chan_value[i])
        
        
        # calculate delay-and-sum (DAS)
        for i in range(17):        
            #norm = np.linalg.norm(self.soft_dir_chan_value[i])
            #self.soft_dir_chan_value[i] = self.soft_dir_chan_value[i] / norm
            self.soft_dir_sum[i] = 0
            for j in range(150, 512):
                self.soft_dir_sum[i] = self.soft_dir_sum[i] + self.soft_dir_chan_value[i][j] ** 2
            
        
        max_dir_index = self.soft_dir_sum.index(max(self.soft_dir_sum))
        #print('soft DIR delay_sum: {}, max index {}'.format(self.soft_dir_sum, max_dir_index))
        print('Direction with Max Delay-and-Sum is {}'.format(max_dir_index))
        self.ui.indexLabel.setText('Direction: ' + str(max_dir_index))
        #max_sum_index = self.i2s_chan_abs_sum_value.index(max(self.i2s_chan_abs_sum_value))
        
        #print("elapsed time from start reading to the end of Beamforming is %s seconds ---" % (time.time() - self.sender.start_time))
        
        
        
        #for i in range(8):
        #    print(self.i2s_chan_raw_value[0].index(max(self.i2s_chan_raw_value[0][100:115])))
    
    def processJSONData(self, json_dict):
        """handler for processing received data in JSON format"""
        json_keys = json_dict.keys()
        
        if json_dict["TYPE"] == 'DIR_RAW':
            
            for i in range(16):
                if (len(json_dict['DATA'][i]) > 0):
                    x_raw = range(self.sample_start, self.sample_start+len(json_dict['DATA'][i]))
                else:
                    x_raw = []
                y_dir_raw = json_dict['DATA'][i]
                self.ui.curve_dir_raw[i].setData(x_raw, y_dir_raw)
                # plot the new 
                #self.ui.curve_dir_raw[i].setData(x_raw, json_dict['DATA'][i])
            print('elapsed time of reading Raw DIR data with size {0} x {1} is {2:.6f} seconds'.format(len(json_dict['DATA']), len(json_dict['DATA'][i]), (time.time() - self.sender.start_time)))
        
        elif json_dict["TYPE"] == 'VOC_RAW':
            #print(json_dict["DATA"])
            for i in range(8):
                self.ui.curve_i2s_raw[i].setData([],[])
            self.voc_raw_value = json_dict["DATA"]
            self.ui.curve_i2s_raw[0].setData(range(0, len(self.voc_raw_value)), self.voc_raw_value)
            print('elapsed time of reading raw VOC data with size {0} is {1:.6f} seconds'.format(len(self.voc_raw_value), (time.time() - self.sender.start_time)))
        
        elif json_dict["TYPE"] == 'VOC_FFT':
            self.voc_fft_value = json_dict["DATA"]
                           
            x_f = []
            y_power = []
            y_power_sum = 0
            #y_power_max = 0.0
            log_line_index = 1
            self.scan_count = self.scan_count + 1
            for k in range(len(self.voc_fft_value)):
                x_f.append(k * float(self.i2s_sample_frequency) / 512)
                y_power.append(float(self.voc_fft_value[k]))
                if self.log_file_in_writing:
                    self.log_file.write(str(int(self.scan_count)) + ', ' + str(log_line_index) + ', ' + str("{:-12.5f}".format(x_f[k])) + ', ' + str(y_power[k]) + '\n')
                self.y_display_max = max(self.y_display_max, y_power[k])
                y_power_sum = y_power_sum + float(self.voc_fft_value[k])
                log_line_index = log_line_index + 1
                
            # clear old all drawing curves
            for i in range(16):
                self.ui.curve_fft_power[i].setData([], [])
            # plot the new 
            self.ui.curve_fft_power[2].setData(x_f, y_power)
            self.ui.pw2_plot_item.setXRange(x_f[self.sample_start], x_f[self.sample_end-1]) 
            self.ui.pw2_plot_item.setYRange(-1, self.y_display_max)
            print('elapsed time of reading VOC FFT data with size {0} is {1:.6f} seconds with max power {2} and power sum {3}'.format(len(self.voc_fft_value), (time.time() - self.sender.start_time), max(y_power), y_power_sum))
        
        elif json_dict["TYPE"] == 'I2S_RAW':
            for i in range(8):
                self.ui.curve_i2s_raw[i].setData([],[])
            #print(json_dict['DATA'])
            for i in range(len(json_dict['DATA'])):
                #print(json_dict['DATA'][i])
                if len(json_dict['DATA'][i]) > 0:
                    self.ui.curve_i2s_raw[i].setData(range(self.sample_start, self.sample_start+len(json_dict['DATA'][i])), json_dict['DATA'][i])
            print('elapsed time of reading data with size {0} x {1} is {2:.6f} seconds'.format(len(json_dict['DATA']), len(json_dict['DATA'][i]), (time.time() - self.sender.start_time)))
        
        elif json_dict["TYPE"] == 'I2S_FFT':
            for i in range(16):
                self.ui.curve_fft_power[i].setData([], [])
            x_f = []
            for k in range(256):
                x_f.append(k * float(self.i2s_sample_frequency) / 512)
            
            for i in range(len(json_dict['DATA'])):
                #print(json_dict['DATA'][i])
                
                if len(json_dict['DATA'][i]) > 0:
                    self.ui.curve_fft_power[i].setData(x_f[self.sample_start-1:self.sample_end], json_dict['DATA'][i])
            print('elapsed time of reading I2S_FFT data with size {0} x {1} is {2:.6f} seconds'.format(len(json_dict['DATA']), len(json_dict['DATA'][i]), (time.time() - self.sender.start_time)))
        
        
    def processNewData(self, var):
        """handler for new data received by serial"""
        
        received_bytes = copy(var)
        
        if (len(received_bytes) > 0):
            self.recv_bytes = self.recv_bytes + received_bytes
        
            #print(self.recv_bytes) # debug
            # handle JSON command
            whole_string = ''.join([chr(elem) for elem in self.recv_bytes])
            #if whole_string[-1] == '}' and whole_string.count('{') == whole_string.count('}'):
            if whole_string[-1] == '}':
                start_index = whole_string.find('{')
                end_index = whole_string.find('}',start_index+1)
                #start_count = whole_string.count('{', 0, end_index)
                while start_index != -1 and end_index != -1:
                    #while start_count > 1:
                        #end_index = whole_string.find('}', end_index+1)
                        #start_count = start_count - 1
                    json_string = whole_string[start_index:end_index+1]
                    #print(json_string)
                    try:
                        json_dict = json.loads(json_string) 
                        json_keys = json_dict.keys()
                        #if not all(key in json_keys for key in ["MCU","ANGLE","DBFS","STATE"]):
                        #    self.ui.rspLabel.setText(json_string)
                        if all(key in json_keys for key in ["MCU", "TYPE", "DATA"]):
                            #print(json_dict['DATA']) # debug to show all json
                            self.processJSONData(json_dict)
                            #self.ui.rspLabel.setText('Received data in JSON format')
                        else:
                            self.ui.rspLabel.setText(json_string)
                        start_index = whole_string.find('{', end_index+1)
                        end_index = whole_string.find('}', end_index+1)  
                        #start_count = whole_string.count('{', start_index, end_index)
                    #except:
                    except json.JSONDecodeError as err:
                        print(err)
                        start_index = whole_string.find('{', end_index+1) # skip current and find next
                        end_index = whole_string.find('}', end_index+1)
                        break # invalid json so exit
                    
                self.recv_bytes = []
                self.sender.recv_is_done = True 
            
            elif 0x11 == self.recv_bytes[0]:
                #print('first byte found!')
                if len(self.recv_bytes) >= 12:
                    self.data_length = self.recv_bytes[4] + (self.recv_bytes[5] << 8) + \
                        (self.recv_bytes[6] << 16) + (self.recv_bytes[7] << 24)
                    
                    
                    if len(self.recv_bytes) == self.data_length + 12:
                        self.data_type = (self.recv_bytes[3] << 8) + self.recv_bytes[2]
                        #print('data length is {}'.format(self.data_length))
                        #print('data type is {}'.format(self.data_type))
                        
                        
                        if self.data_length == 0:
                            
                            self.recv_bytes = []
                            self.sender.recv_is_done = True
                            print('received data length is 0!')
                            return
                                
                        
                        # handle i2s raw data
                        if self.data_type == 0x0700:
                            if self.do_beamforming == True:
                                self.processI2sRawDataWithBeamforming()
                            else:
                                self.processI2sRawData()
                            self.recv_bytes = []
                            self.sender.recv_is_done = True
                        
                        
                        # handle FFT data only
                        if self.data_type == 0x0400:
                            self.i2s_all_fft_value = []
                            self.i2s_all_fft_bytes = self.recv_bytes[12:]
                            self.recv_bytes = []
                            
                            # i2s_fft_value = 0
                            # for i in range(0, len(self.i2s_all_fft_bytes)//2):
                            #     i2s_fft_value = self.i2s_all_fft_bytes[2*i] + (self.i2s_all_fft_bytes[2*i+1] << 8)                            
                            #     if (i2s_fft_value & 0x8000):
                            #         self.i2s_all_fft_value.append(i2s_fft_value - 65536)
                            #     else:
                            #         self.i2s_all_fft_value.append(i2s_fft_value)
                            self.i2s_all_fft_value = self.convert_bytes_to_int16_value(self.i2s_all_fft_bytes)
                            #print(len(self.i2s_all_fft_value))                   
                            #print(self.active_chan_number)
                            #print(self.channel_mask)
                            
                            
                            fft_channel_data_length = int(len(self.i2s_all_fft_value) / self.active_chan_number)
                            #print('start:{}, end:{}'.format(self.sample_start, self.sample_end))
                            x_f = []
                            for k in range(256):
                                x_f.append(k * float(self.i2s_sample_frequency) / 512)
                            
                            j = 0
                            for i in range(8):
                                if ((self.channel_mask >> i) & 0x01) != 0:
                                    self.i2s_chan_fft_value[i] = self.i2s_all_fft_value[j*fft_channel_data_length : j*fft_channel_data_length + fft_channel_data_length]
                                    self.ui.curve_fft_power[i].setData(x_f[self.sample_start-1:self.sample_end], self.i2s_chan_fft_value[i])
                                    j = j + 1
                                else:
                                    self.i2s_chan_fft_value[i] = []
                                    self.ui.curve_fft_power[i].setData([], self.i2s_chan_fft_value[i])
                                
                                
                                self.ui.pw2_plot_item.setXRange(x_f[self.sample_start-1], x_f[self.sample_end-1]+1)
                                
                                
                        
                        # handle i2s raw and fft received from MCU                      
                        if self.data_type == 0x0800:
                            self.i2s_all_raw_value = []
                            self.i2s_all_raw_bytes = self.recv_bytes[12:8204]
                            self.i2s_all_fft_value = []
                            self.i2s_all_fft_bytes = self.recv_bytes[8204:]
                            self.recv_bytes = []
                            
                            self.i2s_all_raw_value = self.convert_bytes_to_int16_value(self.i2s_all_raw_bytes)
                            # extract certain length of i2s raw data for each channel
                            i2s_raw_cut_sample_start = 0   
                            i2s_raw_cut_sample_end = 512
                            for i in range(8):
                                self.i2s_chan_raw_value[i] = self.i2s_all_raw_value[i*512 + i2s_raw_cut_sample_start : i*512 + 512 + i2s_raw_cut_sample_end]
                                chan_abs_sum = 0
                                chan_sum = 0
                                #print(len(self.i2s_chan_raw_value[i]))
                                for j in range(len(self.i2s_chan_raw_value[i])):
                                    #print(abs(self.i2s_chan_raw_value[i][j]))
                                    chan_abs_sum = chan_abs_sum + abs(self.i2s_chan_raw_value[i][j])
                                    chan_sum = chan_sum + self.i2s_chan_raw_value[i][j]
                                
                                
                                #self.i2s_chan_abs_sum_value[i] = chan_abs_sum
                                #self.i2s_chan_sum_value[i] = chan_sum
                                #self.i2s_chan_avg_value[i] = chan_abs_sum / len(self.i2s_chan_raw_value[i])
                                #print('i2s_chan raw abs sum: {}'.format(self.i2s_chan_sum_value[i]))
                                #if i == self.mic_ref or i == self.mic_under_test:
                                self.ui.curve_i2s_raw[i].setData(range(0, len(self.i2s_chan_raw_value[i])), self.i2s_chan_raw_value[i])
                            
                            #print(len(self.i2s_chan_raw_value[0]))
                            
                            # calculate soft direction buffer using delay-and-sum BF 
                            mic_delayed_value = [0] * 8
                            for i in range(16):
                                self.soft_dir_chan_value[i] = []
                                for j in range(512):
                                    for k in range(8):
                                        if (j + self.offsets_hardware[i][k]) < 512:
                                            mic_delayed_value[k] = self.i2s_chan_raw_value[k][j + self.offsets_hardware[i][k]] 
                                        else:
                                            mic_delayed_value[k] = 0
                                    self.soft_dir_chan_value[i].append(sum(mic_delayed_value))
                            
                            for i in range(16):        
                                #norm = np.linalg.norm(self.soft_dir_chan_value[i])
                                #self.soft_dir_chan_value[i] = self.soft_dir_chan_value[i] / norm
                                self.soft_dir_sum[i] = 0
                                
                                for j in range(256):
                                    self.soft_dir_sum[i] = self.soft_dir_sum[i] + self.soft_dir_chan_value[i][256+j] ** 2
                                if i in range(0,16):    
                                    self.ui.curve_dir_raw[i].setData(range(0, len(self.soft_dir_chan_value[i])), self.soft_dir_chan_value[i])
                            
                            max_dir_index = self.soft_dir_sum.index(max(self.soft_dir_sum))
                            #print('soft DIR delay_sum: {}, max index {}'.format(self.soft_dir_sum, max_dir_index))
                            
                            #max_sum_index = self.i2s_chan_abs_sum_value.index(max(self.i2s_chan_abs_sum_value))
                            #print('i2s chan raw sum max index: {}'.format(max_sum_index))
                            #print('i2s chan raw mean value: {}'.format(self.i2s_chan_avg_value))
                            #print('standard deviation of raw sum value: {}'.format(np.std(self.i2s_chan_abs_sum_value)))
                            
                        
                            # i2s_fft_value = 0
                            # for i in range(0, len(self.i2s_all_fft_bytes)//2):
                            #     i2s_fft_value = self.i2s_all_fft_bytes[2*i] + (self.i2s_all_fft_bytes[2*i+1] << 8)                            
                            #     if (i2s_fft_value & 0x8000):
                            #         self.i2s_all_fft_value.append(i2s_fft_value - 65536)
                            #     else:
                            #         self.i2s_all_fft_value.append(i2s_fft_value)
                            self.i2s_all_fft_value = self.convert_bytes_to_int16_value(self.i2s_all_fft_bytes)                       
                                    
                            for i in range(8):
                                self.i2s_chan_fft_value[i] = self.i2s_all_fft_value[i*256 : i*256 + 256]
                                
                                #self.i2s_fft_max_value[i] = max(self.i2s_chan_fft_value[i])
                                
                                #print(len(self.i2s_chan_fft_value[i]))
                                x_f = []
                                for j in range(len(self.i2s_chan_fft_value[i])):
                                    x_f.append(j * float(self.i2s_sample_frequency) / 512)
                                
                                self.ui.curve_fft_power[i].setData(x_f, self.i2s_chan_fft_value[i])
                                
                            self.max_index = self.i2s_all_fft_value.index(max(self.i2s_all_fft_value)) // 256
                            self.ui.indexLabel.setText(str(self.max_index))
                            #print('Channel index with max power is {}'.format(self.max_index) )
                            
                            #print('max value array is {} with sum {}'.format(self.i2s_fft_max_value, sum(self.i2s_fft_max_value)))
                            #print('standard deviation of max value: {}'.format(np.std(self.i2s_fft_max_value)))
                            
                            max_difference = max(self.i2s_fft_max_value) - min(self.i2s_fft_max_value)
                            #print('Max difference of max values: {}'.format(max_difference))
                            
                            #print('1st mic fft value: {}'.format(self.i2s_fft_max_value[0]))
                            
                        # handle directly read DIR buffer from MCU    
                        if self.data_type == 0x0900:
                            
                            self.dir_all_raw_bytes = self.recv_bytes[12:]
                            self.recv_bytes = []
                            
                            self.dir_all_raw_value = []
                            
                            self.dir_all_raw_value = self.convert_bytes_to_int16_value(self.dir_all_raw_bytes)
                            
                            
                            dir_data_length = int(len(self.dir_all_raw_value) / self.active_chan_number)
                            
                            # use plot channel mask to display specific channels plot
                            self.plot_channel_mask = int(self.ui.chanMasklineEdit.text(), base=16)
                            # plot channel mask should has less and equal number of bit 1 than the channel mask
                            self.plot_channel_mask = self.plot_channel_mask & self.channel_mask
                            j = 0
                            for i in range(16):
                                if ((self.plot_channel_mask >> i) & 0x01) != 0:
                                    self.dir_chan_raw_value[i] = self.dir_all_raw_value[j*dir_data_length : j*dir_data_length + dir_data_length]
                                    self.ui.curve_dir_raw[i].setData(range(self.sample_start-1, self.sample_end), self.dir_chan_raw_value[i])
                                    j = j + 1
                                else:
                                    self.dir_chan_raw_value[i] = []
                                    self.ui.curve_dir_raw[i].setData([], self.dir_chan_raw_value[i])
                                self.ui.pw3_plot_item.setXRange(self.sample_start, self.sample_end + 1) 
                            self.sender.recv_is_done = True
                            
                        """handle the read of raw VOC data"""        
                        if self.data_type == 0x1000:
                            self.voc_raw_value = []
                            self.voc_raw_bytes = self.recv_bytes[12:]
                            self.recv_bytes = []
                            #print(len(self.voc_raw_bytes))
                            
                            self.voc_raw_value = self.convert_bytes_to_int16_value(self.voc_raw_bytes)
                            
                            for curve in self.ui.curve_i2s_raw:
                                curve.setData([],[])
                            self.ui.curve_i2s_raw[0].setData(range(0, len(self.voc_raw_value)), self.voc_raw_value)
                            
                            print('elapsed time of reading raw VOC data with size {0} is {1:.6f} seconds'.format(len(self.voc_raw_value), (time.time() - self.sender.start_time)))
                            self.sender.recv_is_done = True
                        # """handle the JSON command """
                        # if self.data_type == 0x0101:
                        #     init_string = ''.join([chr(elem) for elem in self.recv_bytes[12:]])
                        #     self.recv_bytes = []
                        #     #print(init_string)
                        #     if 'RSP' in init_string:
                        #         self.ui.rspLabel.setText(init_string) # debug
                        #     else:
                        #         json_dict = json.loads(init_string)
                        #         #print(json_dict)
                        #         print('---- elapsed time of reading json array with size {0} is {1}'.format(len(json_dict['DATA']), (time.time() - self.sender.start_time)))
                        #         #self.ui.rspLabel.setText(str(json_dict['DATA']))
                        #         #print(len(json_dict['DATA']))
                        #         for i in range(len(json_dict['DATA'])):
                        #             #print(json_dict['DATA'][i])
                        #             if len(json_dict['DATA'][i]) > 0:
                        #                 self.ui.curve_i2s_raw[i].setData(range(self.sample_start, self.sample_start+len(json_dict['DATA'][i])), json_dict['DATA'][i])
                        #             else:
                        #                 self.ui.curve_i2s_raw[i].setData([], json_dict['DATA'][i])
                              
                        """handle the reading of VOC FFT data"""    
                        if self.data_type == 0x1200:
                            self.voc_fft_value = []
                            self.voc_fft_bytes = self.recv_bytes[12:]
                            self.recv_bytes = []
                            
                            
                            self.voc_fft_value = self.convert_bytes_to_int16_value(self.voc_fft_bytes)
                            # print((self.voc_fft_value))                   
                            # print(self.active_chan_number)
                            # print(self.channel_mask)
                            # print(self.sample_start)
                            # print(self.sample_end)
                            
                            
                            fft_channel_data_length = int(len(self.voc_fft_value))
                            #print('voc fft data size is {}'.format(fft_channel_data_length))
                            #print('start:{}, end:{}'.format(self.sample_start, self.sample_end))
                            
                            x_f = []
                            y_power = []
                            for k in range(fft_channel_data_length):
                                x_f.append(k * float(self.i2s_sample_frequency) / 512)
                                y_power.append(float(self.voc_fft_value[k]))
                            
                            # clear old all drawing curves
                            for i in range(8):
                                self.ui.curve_fft_power[i].setData([], [])
                            # plot the new 
                            self.ui.curve_fft_power[2].setData(x_f, y_power)
                            print('elapsed time of reading VOC FFT data with size {0} is {1:.6f} seconds with max power {2}'.format(fft_channel_data_length, (time.time() - self.sender.start_time), max(y_power)))
                            
                            
                        self.sender.recv_is_done = True
            else:
                self.recv_bytes = []
                self.sender.recv_is_done = True        
                
                                         
    def commandRadioBtnClicks(self):
        radioButton = self.sender()
        
        if radioButton.isChecked():
            print("Comamnd is %s" % (radioButton.command_name))    
            if 'i2s' in radioButton.command_name:
                self.ui.pw3_plot_item.setTitle('Soft DIR Buffer')
            elif 'dir_raw' in radioButton.command_name:
                self.ui.pw3_plot_item.setTitle('Hard DIR Buffer')
            #self.sender.all_cmd_list.append(str(radioButton.command_name))
            #current_cycle_cmd = str(radioButton.command_name)
            #print('Command list is {}'.format(self.sender.all_cmd_list))
    
    def findLLCommandFieldValue(self, llc_command, field_name):
        """handler of extract information about llc """
        # set default value of a field
        field_value = 0
        if field_name == '-start':
            if 'dir_raw' in llc_command:
                field_value = 257
            else:
                field_value = 1
        elif field_name == '-end':
            if 'i2s_raw' in llc_command or 'dir_raw' in llc_command or 'voc_raw'in llc_command :
                field_value = 512
            elif 'i2s_soft' in llc_command or 'i2s_hard' in llc_command or 'voc_fft' in llc_command :
                field_value = 256
        elif field_name == '-channel':
            if 'i2s_raw' in llc_command or 'i2s_soft' in llc_command or 'i2s_hard' in llc_command:
                field_value = 0xFF
            elif 'dir_raw' in llc_command:
                field_value = 0xFFFF
            else:
                field_value = 0x01
        
        field_string = ''        
        x = llc_command.split(" ")        
        if llc_command.find(field_name) != -1:
            index_field = x.index(field_name)            
            for i in range(index_field+1, len(x)):            
                if x[i] != '':
                    field_string = x[i]
                    break
            #print('start value: {}'.format(start_string))
        # convert from str to int value for the field value
        if field_string != '':
            if field_string.find('x') != -1:
                field_value = int(field_string, base=16)
            else:
                field_value = int(field_string)
        return field_value
        
        
    def updatePlotTitle(self, current_command):
        """Update the titles for associated plot based on the command name"""
        if 'i2s_raw' in current_command:     
            self.ui.pw1_plot_item.setTitle('I2S Raw')
        elif 'i2s_soft_fft' in current_command or 'i2s_hard_fft' in current_command:
            self.ui.pw2_plot_item.setTitle('I2S FFT')
        elif 'i2s_fft_raw' in current_command:
            self.ui.pw1_plot_item.setTitle('I2S Raw')
            self.ui.pw2_plot_item.setTitle('I2S FFT')
        elif 'dir_raw' in current_command:
            self.ui.pw3_plot_item.setTitle('DIR Raw')
        elif 'voc_raw' in current_command:
            self.ui.pw1_plot_item.setTitle('VOC Raw')
        elif 'voc_fft' in current_command:
            self.ui.pw2_plot_item.setTitle('VOC FFT')
        
        
    def commandSendButtonClicks(self):
        """handler of clicking the one-time send button"""
        #self.recv_bytes = []
        current_command = str(self.ui.cmdComboBox.currentText())
        #print(current_command)
        
        if current_command != "":
            self.sample_start = self.findLLCommandFieldValue(current_command, '-start')
            self.sample_end = self.findLLCommandFieldValue(current_command, '-end')
            self.channel_mask = self.findLLCommandFieldValue(current_command, '-channel')
            self.active_chan_number = bin(self.channel_mask).count("1")
            
        self.sender.start_time = time.time()    
        if self.sender.recv_is_done == True:    
            #print(self.ui.cmdlineEdit.text())
            self.updatePlotTitle(current_command)
            self.serial_transmitter.send_command_string(current_command, None)
        else:
            print('wait for receiving is done!')
    
    
    def readingStartButtonClicks(self):
        """handler for clicking the Start Sending (cycle reading) Button"""
        
        if self.sender.enable_cycle_send == False:
        
            self.log_file_name = QFileDialog.getSaveFileName(self, 'Save file', 'c:\\',"Log files (*.csv)")[0]
            
            if self.log_file_name != '':
                self.log_file = open(self.log_file_name, 'w')
                self.log_file_in_writing = True
                self.scan_count = 0
                self.log_file.write('Scan, Index, Frequency, FFT_Power\n')
            else:
                print('Logging is NOT activated')
            #print(self.log_file_name)
        
            self.ui.cmdStartBtn.setText('Stop Reading')
            
            current_cycle_cmd = str(self.ui.cmdComboBox.currentText())
            if current_cycle_cmd != "":
                self.sample_start = self.findLLCommandFieldValue(current_cycle_cmd, '-start')
                self.sample_end = self.findLLCommandFieldValue(current_cycle_cmd, '-end')
                self.channel_mask = self.findLLCommandFieldValue(current_cycle_cmd, '-channel')
                self.active_chan_number = bin(self.channel_mask).count("1")
            
            self.sender.all_cmd_list = []
            current_command = None
            if self.ui.mcu0ChkBox.isChecked() == True:
                current_command = current_cycle_cmd 
                self.sender.all_cmd_list.append(current_command)
            if self.ui.mcu1ChkBox.isChecked() == True:
                current_command = current_cycle_cmd + ' -target 1'
                self.sender.all_cmd_list.append(current_command)
            if self.ui.mcu2ChkBox.isChecked() == True:
                current_command = current_cycle_cmd + ' -target 2'
                self.sender.all_cmd_list.append(current_command)
                
            self.updatePlotTitle(current_command)   
            
            print('Start to periodically send commands {}'.format(self.sender.all_cmd_list))
            
            #self.ui.i2sRawFFTRadioBtn.setEnabled(False)
            #self.ui.dirRadioButton.setEnabled(False)
            self.ui.mcu0ChkBox.setEnabled(False)
            self.ui.mcu1ChkBox.setEnabled(False)
            self.ui.mcu2ChkBox.setEnabled(False)
        else:
            self.ui.cmdStartBtn.setText('Start Reading+Logging')
            #self.ui.i2sRawFFTRadioBtn.setEnabled(True)
            #self.ui.dirRadioButton.setEnabled(True)
            self.ui.mcu0ChkBox.setEnabled(True)
            self.ui.mcu1ChkBox.setEnabled(True)
            self.ui.mcu2ChkBox.setEnabled(True)
            print('Stop periodically sending command {}'.format(self.sender.all_cmd_list))
            if self.log_file_in_writing == True:
                self.log_file_name = ''
                self.log_file.close()
                self.log_file_in_writing = False
        # turn on the cycle reading at the last step        
        self.sender.enable_cycle_send = not self.sender.enable_cycle_send    
            
    def recordOffsetButtonClicks(self):
        direction_str = self.ui.dirComboBox.currentText()
        direction = direction_str.split()[1]
        self.current_direction = int(direction)
        self.record_offset_flag = True
    
            
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    main = ApplicationWindow()
    main.resize(1200, 700)
    main.setWindowTitle('PULD Data Reading Beamforming and Plotting')
    main.show()
    sys.exit(app.exec_())
