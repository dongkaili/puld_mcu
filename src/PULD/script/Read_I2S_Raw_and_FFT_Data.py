# Code to show two plots of simulated streamed data. Data for each plot is processed (generated) 
# by separate threads, and passed back to the gui thread for plotting.
# This is an example of using movetothread, which is the correct way of using QThreads

# Michael Hogg, 2015

import time, sys

from PyQt5.QtCore import Qt
from pyqtgraph.Qt import QtGui, QtCore
from PyQt5.Qt import QMutex
import pyqtgraph as pg
from PyQt5.QtWidgets import (
    QApplication,
    QLabel,
    QLineEdit,
    QComboBox,
    QCheckBox,
    QMainWindow,
    QPushButton,
    QRadioButton,
    QVBoxLayout,
    QGridLayout,
    QWidget,
)
from random import randint
from copy import copy

import serial
from serial.tools.list_ports import comports
from serial.tools import hexlify_codec

import numpy as np

#recv_bytes_list = []
enable_cycle_send = False
receive_is_done = True
#enable_send_rbv = False

current_cycle_cmd = 'i2s_fft_raw' # either i2s_fft_raw or dir_raw, and can add more 

all_mcu_list = [] # a list of all MCU to send to 

all_cmd_list = [] # contain a list of all command to send


class SerialTransmitter(QtCore.QObject):
    
    def __init__(self, serial_instance, parent=None):
        super().__init__(parent)
        self.serial_instance = serial_instance
        
    def send_command_string(self, letter, command_type):
        """Write Micro Jason string to sys.stderr"""
        """
        HH(0x11), VV(Version Byte), TH(0x00: Raw, 0x01:Json), TL(0x00~0x04), Length(4 bytes, LSB first?), checksum, Reserve1, R2, R3, <data>
        
        """
        header    = 0x11
        #print(header.to_bytes(1, 'big'))
        version   = 0x02
        if command_type == 'JSON_Type':
            type_high = 0x01
            type_low  = 0x01
        elif command_type == 'LLC_Type':
            type_high = 0x02
            type_low  = 0x05
        elif command_type == 'Data_Type':
            type_high = 0x00
            type_low  = 0x06
        elif command_type == 'I2S_Raw_Type':
            type_high = 0x00
            type_low  = 0x07
        elif command_type == 'I2S_FFT_Type':
            type_high = 0x00
            type_low =  0x04
        elif command_type == 'RAW_FFT_Type':
            type_high = 0x00
            type_low =  0x08
        elif command_type == 'DIR_RAW_Type':
            type_high = 0x00
            type_low =  0x09
        elif command_type == None:
            if 'i2s_fft_raw' in letter:
                type_high = 0x00
                type_low =  0x08
            elif 'dir_raw' in letter:
                type_high = 0x00
                type_low =  0x09
        
        
            
        length_1  = 0x00
        length_2  = 0x00
        length_3  = 0x00
        length_4  = 0x00
        reserve_1 = 0x00
        reserve_2 = 0x00
        reserve_3 = 0x00
        
        
        if letter == '\x11':  # CTRL+Q
            cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"ENABLE"}')
        elif letter == '\x1A':  # CTRL+Z
            cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"DISABLE"}')
        elif letter == 'RBV' or letter == 'V':
            cmd_string = list('{"CMD": "RBV"}')
        elif letter == 'RBC' or letter == 'C':
            cmd_string = list('{"CMD": "RBC"}')
        elif letter == 'I':
            cmd_string = list('{"CMD": "RID", "MCU":0}')
        elif letter == 'F':
            cmd_string = list('{"CMD": "RFV", "MCU":0}')
        elif letter == 'p':
            cmd_string = list('{"CMD": "RPV"}')
        elif letter == 'v':
            cmd_string = list('version')
        else:
            cmd_string = list(letter)
            
        #print("Cmd: {}".format(cmd_string))
        
        length_1 = len(cmd_string) & 0xFF
        length_2 = (len(cmd_string) >> 8) & 0xFF
        length_3 = (len(cmd_string) >> 16) & 0xFF
        length_4 = (len(cmd_string) >> 24) & 0xFF
        
        calculated_checksum = 0
        check_sum = 0
        pay_load = []
        for i in range(0, len(cmd_string)):
            pay_load.append(ord(cmd_string[i]))
            calculated_checksum += pay_load[i]
        
        
        #calculated_checksum = header + version + type_high + type_low + length_1 + length_2 + length_3 + length_4 + reserve_1 + reserve_2 + reserve_3 + reserve_4
        
        calculated_checksum = calculated_checksum & 0xFF
        calculated_checksum = 0xFF - calculated_checksum
        
        check_sum = calculated_checksum
        #print('check_sum is ' + hex(check_sum))
        
        tx_data = []
        tx_data.append(header.to_bytes(1, 'big'))
        tx_data.append(version.to_bytes(1, 'big'))
        tx_data.append(type_high.to_bytes(1, 'big'))
        tx_data.append(type_low.to_bytes(1, 'big'))
        tx_data.append(length_1.to_bytes(1, 'big'))
        tx_data.append(length_2.to_bytes(1, 'big'))
        tx_data.append(length_3.to_bytes(1, 'big'))
        tx_data.append(length_4.to_bytes(1, 'big'))
        tx_data.append(check_sum.to_bytes(1, 'big'))
        tx_data.append(reserve_1.to_bytes(1, 'big'))
        tx_data.append(reserve_2.to_bytes(1, 'big'))
        tx_data.append(reserve_3.to_bytes(1, 'big'))

        
        #for i in range(0, len(pay_load)):
        for item in pay_load:
            tx_data.append(item.to_bytes(1, 'big'))        
               
        for i in range(0, len(tx_data)):
            #print(tx_data[i])
            self.serial_instance.write((tx_data[i]))



class DataReader(QtCore.QObject):
    newData  = QtCore.pyqtSignal(object)
    finished = QtCore.pyqtSignal()
    
    def __init__(self, serial_instance, parent=None):
        super().__init__(parent)
        self.serial = serial_instance
        self.recv_bytes_list = []
        
    def run(self):
        """Long Run task 1 to receive incoming data on serial port"""
        #global recv_bytes_list
        try:
            while True:
                # read all that is there or wait for one byte
                data = self.serial.read(self.serial.in_waiting)
                #print(data)
                #data = self.serial.read()
                if data:
                    new_recv_bytes = list(data)
                    self.recv_bytes_list.append(new_recv_bytes)
                    self.newData.emit(self.recv_bytes_list)    
        except serial.SerialException:
            print('serial has exception')
        
        
 

class DataSender(QtCore.QObject):

    newData  = QtCore.pyqtSignal(object)
    
    #def __init__(self,parent=None,sizey=100,rangey=[0,100],delay=1000):
    def __init__(self, serial_transmitter, command_list=None, delay=1000, parent=None):
        global all_cmd_list
        QtCore.QObject.__init__(self)
        self.parent = parent
        #self.sizey  = sizey
        #self.rangey = rangey
        self.serial_tx = serial_transmitter
        self.command_list = command_list
        all_cmd_list = command_list
        self.delay  = delay
        self.mutex  = QMutex()        
        #self.y      = [0 for i in range(sizey)]
        self.run    = True    
        self.cmd_index = 0
        
        
    def sendCommand(self):
        global enable_cycle_send, receive_is_done, all_cmd_list, all_mcu_list
        while self.run:
            try:
                
                self.mutex.lock()        
                if enable_cycle_send == True and receive_is_done == True:
                
                    #self.serial_tx.send_command_string(self.command_list[self.cmd_index], 'DIR_RAW_Type')  
                    self.serial_tx.send_command_string(all_cmd_list[self.cmd_index], None)  
                    
                    receive_is_done = False
                    self.newData.emit(all_cmd_list[self.cmd_index])
                    self.cmd_index = self.cmd_index + 1
                    if (self.cmd_index >= len(all_cmd_list)):
                        self.cmd_index = 0
                self.mutex.unlock() 
                
                
                QtCore.QThread.msleep(self.delay)
                
            except: pass
        
        
class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        #MainWindow.resize(1600, 960)
        MainWindow.setMinimumSize(QtCore.QSize(1800, 1000))
        
        main_layout = pg.LayoutWidget()
        pg.setConfigOption('background', 'k')
        pg.setConfigOption('foreground', 'w')
        
        self.symbols = ['o', 't', 'p', 'h', 'star', '+', 'd', 'x']
        
        self.pw1 = pg.PlotWidget(title="I2S Raw")
        self.pw1_plot_item = self.pw1.getPlotItem()
        self.pw1_plot_item.addLegend()
        self.curve_i2s_raw = [None] * 8
        for i in range(8):
            #self.curve_i2s_raw[i] = self.pw1.plot(pen=(i, 8), name='raw {}'.format(i), symbol=self.symbols[i])
            self.curve_i2s_raw[i] = self.pw1.plot(pen=(i, 8), name='raw {}'.format(i))
        
        
        
        self.pw1_plot_item = self.pw1.getPlotItem()
        self.pw1_plot_item.addLegend()
            
        self.pw1.setLabel('left', 'Power', units='')
        self.pw1.setLabel('bottom', 'Time', units='Sample')
        main_layout.addWidget(self.pw1, row=0, col=0, rowspan=1, colspan=5)
        
        
        self.pw2 = pg.PlotWidget(title="I2S FFT")
        self.pw2_plot_item = self.pw2.getPlotItem()
        self.pw2_plot_item.addLegend()
        self.curve_i2s_fft = [None] * 8               
        
        for i in range(8):
            #self.curve_i2s_fft[i] = self.pw2.plot(pen=(i, 8), name='fft {}'.format(i), symbol=self.symbols[i])
            self.curve_i2s_fft[i] = self.pw2.plot(pen=(i, 8), name='fft {}'.format(i))
                
        self.pw2.setLabel('left', 'Power', units='')
        self.pw2.setLabel('bottom', 'Frequency', units='Hz')  
        main_layout.addWidget(self.pw2, row=0, col=5, rowspan=1, colspan=5)
        
        
        
        self.pw3 = pg.PlotWidget(title="DIR Raw")
        self.pw3_plot_item = self.pw3.getPlotItem()
        self.pw3_plot_item.addLegend()
        self.curve_dir_raw = [None] * 16
        for i in range(16):
            #self.curve_dir_raw[i] = self.pw3.plot(pen=(i, 8), name='raw {}'.format(i), symbol=self.symbols[i])
            self.curve_dir_raw[i] = self.pw3.plot(pen=(i, 16), name='dir {}'.format(i))
        
        self.pw3.setLabel('left', 'Amplitude', units='')
        self.pw3.setLabel('bottom', 'Time', units='Sample')
        main_layout.addWidget(self.pw3, row=1, col=0, rowspan=1, colspan=5)
        
        
        
        self.pw4 = pg.PlotWidget(title="DIR FFT")
        self.pw4_plot_item = self.pw4.getPlotItem()
        self.pw4_plot_item.addLegend()
        self.curve_dir_fft = [None] * 16              
        
        for i in range(16):
            #self.curve_dir_fft[i] = self.pw4.plot(pen=(i, 16), name='fft {}'.format(i), symbol=self.symbols[i])
            self.curve_dir_fft[i] = self.pw4.plot(pen=(i, 16), name='dir fft {}'.format(i))
                
        self.pw4.setLabel('left', 'Power', units='')
        self.pw4.setLabel('bottom', 'Frequency', units='Hz')  
        main_layout.addWidget(self.pw4, row=1, col=5, rowspan=1, colspan=5)
        
        
        
        #self.clicksLabel = QLabel()
        #self.clicksLabel.setText('')
        #self.clicksLabel.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                
        #main_layout.addWidget(self.clicksLabel, row=1, col=0)
        
        #self.sendBtn = QPushButton()
        #self.sendBtn.setText('Start Reading Battery')
        #main_layout.addWidget(self.sendBtn, row=2, col=0)
        
        
        #self.secondClickLabel = QLabel()
        #self.secondClickLabel.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        #main_layout.addWidget(self.secondClickLabel, row=1, col=5)
        
        #self.secondSendBtn = QPushButton()
        #self.secondSendBtn.setText('Read F/W Version')
        #main_layout.addWidget(self.secondSendBtn, row=2, col=0)
        
        
        #self.thirdLabel = QLabel()
        #self.thirdLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        #main_layout.addWidget(self.thirdLabel, row=2, col=1)
        
        #newfont = QtGui.QFont("Times", 10, QtGui.QFont.Bold)
        
        self.cmdTextLabel = QLabel()
        #self.cmdTextLabel.setFont(newfont)
        self.cmdTextLabel.setText('Command: ')
        self.cmdTextLabel.setMaximumWidth(130)
        self.cmdTextLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        main_layout.addWidget(self.cmdTextLabel, row=3, col=0)
        
        #self.cmdlineEdit = QLineEdit()
        #self.cmdlineEdit.setText('i2s_raw')
        #self.cmdlineEdit.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        #self.cmdlineEdit.setMaximumWidth(400)
        
        #main_layout.addWidget(self.cmdlineEdit, row=3, col=1, rowspan=1, colspan=1)
        
        self.cmdComboBox = QComboBox()
        
        self.cmdComboBox.addItem('i2s_raw')
        self.cmdComboBox.addItem('i2s_soft_fft')
        self.cmdComboBox.addItem('i2s_hard_fft')
        
        self.cmdComboBox.addItem('i2s_fft_raw')
        
        self.cmdComboBox.addItem('i2s_raw -target 1')
        self.cmdComboBox.addItem('i2s_soft_fft -target 1')
        self.cmdComboBox.addItem('i2s_hard_fft -target 1')
        
        self.cmdComboBox.addItem('i2s_fft_raw -target 1')
        
        self.cmdComboBox.addItem('i2s_raw -target 2')
        self.cmdComboBox.addItem('i2s_soft_fft -target 2')
        self.cmdComboBox.addItem('i2s_hard_fft -target 2')
        
        self.cmdComboBox.addItem('i2s_fft_raw -target 2')
        
        self.cmdComboBox.addItem('dir_raw')
        self.cmdComboBox.addItem('dir_raw -target 1')
        self.cmdComboBox.addItem('dir_raw -target 2')
        
        
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IDC", "MCU":2, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"ISF", "MCU":2, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"IHF", "MCU":2, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"RBC"}')
        self.cmdComboBox.addItem('{"CMD":"RBV"}')
        self.cmdComboBox.addItem('{"CMD":"PWR", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"PWR", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"RST"}')
        self.cmdComboBox.addItem('{"CMD":"RFV"}')
        
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":0, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":0, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":1, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":1, "STATE":"DISABLE"}')
        
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":2, "STATE":"ENABLE"}')
        self.cmdComboBox.addItem('{"CMD":"HEN", "MCU":2, "STATE":"DISABLE"}')
        
        
        
        #self.cmdComboBox.setMinimumWidth(400)
        main_layout.addWidget(self.cmdComboBox, row=3, col=1, rowspan=1, colspan=1)
        
        self.cmdSendBtn = QPushButton()
        self.cmdSendBtn.setText('Send Command')
        #self.cmdSendBtn.setMaximumWidth(150)
        
        main_layout.addWidget(self.cmdSendBtn, row=3, col=2)
        
        self.cmdStartBtn = QPushButton()
        self.cmdStartBtn.setText('Start Reading')
        #self.cmdStartBtn.setMaximumWidth(150)
        main_layout.addWidget(self.cmdStartBtn, row=3, col=3)
        
        
        self.i2sRadioButton = QRadioButton('I2S Raw+FFT')
        self.i2sRadioButton.setChecked(True)
        self.i2sRadioButton.command_name = 'i2s_fft_raw'
        main_layout.addWidget(self.i2sRadioButton, row=3, col=5)
        
        self.dirRadioButton = QRadioButton('DIR Raw')
        #self.radioButton.setChecked(False)
        self.dirRadioButton.command_name = 'dir_raw'
        main_layout.addWidget(self.dirRadioButton, row=3, col=6)
        
        
        self.mcu0ChkBox = QCheckBox()
        self.mcu0ChkBox.setText('MCU0')
        #self.mcu0ChkBox.setChecked(True)
        main_layout.addWidget(self.mcu0ChkBox, row=3, col=7)
        
        
        self.mcu1ChkBox = QCheckBox()
        self.mcu1ChkBox.setText('MCU1')
        main_layout.addWidget(self.mcu1ChkBox, row=3, col=8)
        
        
        self.mcu2ChkBox = QCheckBox()
        self.mcu2ChkBox.setText('MCU2')
        main_layout.addWidget(self.mcu2ChkBox, row=3, col=9)
        
        
        self.indexTextLabel = QLabel()
        self.indexTextLabel.setText('Max FFT Index: ')
        self.indexTextLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        main_layout.addWidget(self.indexTextLabel, row=4, col=5)
        
        self.indexLabel = QLabel()
        self.indexLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        main_layout.addWidget(self.indexLabel, row=4, col=6)
        
        
        self.cmdIndexLabel = QLabel()
        self.cmdIndexLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        main_layout.addWidget(self.cmdIndexLabel, row=4, col=7)
        
        
        self.rspTxtLabel = QLabel()
        self.rspTxtLabel.setText('Command Response: ')
        self.rspTxtLabel.setAlignment(Qt.AlignRight | Qt.AlignVCenter)
        #self.rspTxtLabel.setMaximumWidth(140)
        main_layout.addWidget(self.rspTxtLabel, row=4, col=0)
        
        self.rspLabel = QLabel()
        #self.rspLabel.setText('Command Response: ')
        self.rspLabel.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        main_layout.addWidget(self.rspLabel, row=4, col=1, rowspan=1, colspan=6)
        
        MainWindow.setCentralWidget(main_layout)
        
        
        
        
        
class ApplicationWindow(QtGui.QMainWindow):
    
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        
        
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setStyleSheet("QMainWindow {background: 'lightGray';}")
        
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        #self.ui.secondSendBtn.clicked.connect(self.secondSendButtonClicks)
        self.ui.cmdSendBtn.clicked.connect(self.commandSendButtonClicks)
        self.ui.cmdStartBtn.clicked.connect(self.readingStartButtonClicks)
        self.ui.i2sRadioButton.toggled.connect(self.commandRadioBtnClicks)
        self.ui.dirRadioButton.toggled.connect(self.commandRadioBtnClicks)
        
        self.ui.mcu0ChkBox.toggled.connect(self.mcu0CheckBoxClicks)
        self.ui.mcu1ChkBox.toggled.connect(self.mcu1CheckBoxClicks)
        self.ui.mcu2ChkBox.toggled.connect(self.mcu2CheckBoxClicks)
        
        
        #finish = QAction("Quit", self)
        #finish.triggered.connect(self.closeEvent)
        self.port = None
        self.baudrate = 115200
        self.parity = 'N'
        self.rtscts = False
        self.xonxoff = False
        self.exclusive = True
        
        
        self.recv_bytes = []
        self.i2s_sample_frequency = 317382
        
        self.i2s_all_raw_bytes = []
        self.i2s_all_raw_value = []
        self.i2s_chan_raw_value = [None]*8
        
        
        self.i2s_all_fft_bytes = []
        self.i2s_all_fft_value = []
        self.i2s_chan_fft_value = [None]*8
        
        self.i2s_fft_max_value = [0]*8
        
        
        self.dir_all_raw_bytes = []
        self.dir_all_raw_value = []
        self.dir_chan_raw_value = [None]*16
        
        
        
        
        self.json_missing_counts = 0
        self.cmd_response = []
        self.cmd_response_list = []
        
        self.rx_state = 'Idle'
        self.data_length = 0
        self.data_type = 0
        
        # todo: add more command to read all MCU's FFT
        #self.command_list =  ['i2s_fft_raw', 'i2s_fft_raw -target 1', 'i2s_fft_raw -target 2'] # ['RBV', 'RBC'] #
        self.command_list =  ['i2s_fft_raw']
        #self.command_list =  ['dir_raw']
        
        self.batt_voltage_data = []
        self.batt_current_data = []
        
        try:
            self.port = self.ask_for_port()
        except KeyboardInterrupt:
            self.port = None
        
        
        try:
            self.serial_instance = serial.serial_for_url(
                self.port,
                self.baudrate,
                parity=self.parity,
                rtscts=self.rtscts,
                xonxoff=self.xonxoff,
                do_not_open=True,
                timeout= 8.0)

            #if not hasattr(self.serial_instance, 'cancel_read'):
                # enable timeout for alive flag polling if cancel_read is not available
            #    self.serial_instance.timeout = 1
            
            if isinstance(self.serial_instance, serial.Serial):
                self.serial_instance.exclusive = self.exclusive

            self.serial_instance.open()
            if self.serial_instance.is_open == True:
                print('port {} is opened successfully!'.format(self.port))           
            else:
                print('Failed to open port {}!'.format(self.port))   
                return
                
            
        except serial.SerialException as e:
            sys.stderr.write('could not open port {!r}: {}\n'.format(self.port, e))
            
        
        self.serial_instance.flushInput()
        
        self.serial_transmitter = SerialTransmitter(self.serial_instance)
        
        
        # first task of cycle reading 
        #self.x1 = range(0,100)
        self.thread_send = QtCore.QThread()
        self.dgen1   = DataSender(self.serial_transmitter, self.command_list, 200)
        self.dgen1.moveToThread(self.thread_send)
        self.thread_send.started.connect(self.dgen1.sendCommand)
        self.dgen1.newData.connect(self.processSendNewData)
        self.thread_send.start()
        
        
        
        """Long-running task in 5 steps."""
        # Step 2: Create a QThread object
        self.thread_read = QtCore.QThread()
        # Step 3: Create a worker object
        self.reader = DataReader(self.serial_instance)
        # Step 4: Move worker to the thread
        self.reader.moveToThread(self.thread_read)
        # Step 5: Connect signals and slots
        self.thread_read.started.connect(self.reader.run)
        self.reader.finished.connect(self.thread_read.quit)
        self.reader.finished.connect(self.reader.deleteLater)
        self.thread_read.finished.connect(self.thread_read.deleteLater)
        self.reader.newData.connect(self.processNewData)
        # Step 6: Start the thread
        self.thread_read.start()
        
           
           
    
        

        # Final resets
        #self.longRunningBtn.setEnabled(False)
        #self.thread_read.finished.connect(
        #    lambda: self.longRunningBtn.setEnabled(True)
        #)
        #self.thread.finished.connect(
        #    lambda: self.stepLabel.setText("Long-Running Step: 0")
        #)
       
       
    def ask_for_port(self):
        """\
        Show a list of ports and ask the user for a choice. To make selection
        easier on systems with long device names, also allow the input of an
        index.
        """
        sys.stderr.write('\n--- Available ports:\n')
        ports = []
        for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
            sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
            ports.append(port)
        while True:
            port = input('--- Enter port index or full name: ')
            try:
                index = int(port) - 1
                if not 0 <= index < len(ports):
                    sys.stderr.write('--- Invalid index!\n')
                    continue
            except ValueError:
                pass
            else:
                port = ports[index]
            return port
       
       
    '''   
    def search_for_command(self):
        if self.json_missing_counts > 0:
            if len(self.recv_bytes) == self.json_missing_counts:
                self.cmd_response = self.cmd_response + self.recv_bytes                                
                self.cmd_response_list.append(self.cmd_response)
                #self.ota_proc_event.set()
                self.json_missing_counts = 0
                self.cmd_response = []
                self.recv_bytes = []
                
            elif len(self.recv_bytes) < self.json_missing_counts:    # not enough
                self.cmd_response = self.cmd_response + self.recv_bytes
                self.json_missing_counts = self.json_missing_counts - len(self.recv_bytes)
                self.recv_bytes = []
                
            elif len(self.recv_bytes) > self.json_missing_counts:    # more than one command needs
                self.cmd_response = self.cmd_response + self.recv_bytes[0:self.json_missing_counts]
                self.cmd_response_list.append(self.cmd_response)
                #self.ota_proc_event.set()
                self.cmd_response = []
                self.recv_bytes = self.recv_bytes[self.json_missing_counts:]
                self.json_missing_counts = 0
        
        while 0x11 in self.recv_bytes:
            index = self.recv_bytes.index(0x11)
            # get the length of bytes from 0x11 to the end of the array
            size = len(self.recv_bytes[index:])
            # the payload length of json command is in this array
            if size > 4:
                # get the actual length of json command 
                length = self.recv_bytes[index+4] + 12
                if size > length:
                    # a json command is in this current array
                    self.cmd_response = self.recv_bytes[index : index+length]
                    self.cmd_response_list.append(self.cmd_response)
                    #self.ota_proc_event.set()
                    self.cmd_response = []
                    self.recv_bytes = self.recv_bytes[index+length:]
                elif size == length:
                    self.cmd_response = self.recv_bytes[index:]
                    self.cmd_response_list.append(self.cmd_response)
                    #self.ota_proc_event.set()
                    self.cmd_response = []
                    self.recv_bytes = []
                elif size < length:
                    self.cmd_response = self.recv_bytes[index:]
                    self.recv_bytes = []
                    self.json_missing_counts = length - size
            else:
                break
    '''        
            
    def findJsonValueField(self, init_string):
        """retrieve the value field from the json string"""
        index_value = init_string.find('VALUE')
        if (index_value == -1):
            index_value = init_string.find('VAL')
        if (index_value == -1):
            index_value = init_string.find('STATE')
        
        sec_string = ''
        if (index_value != -1):
            sec_string = init_string[index_value+7:]
            bad_chars = [':', '}', '\\', '\"']
            for i in bad_chars:
                sec_string = sec_string.replace(i, '')
        else:
            sec_string = init_string
        return sec_string
        
    def findJsonResponseNameField(self, init_string):
        """search for the RSP name"""
        sec_string = ''
        index_1 = init_string.find('RSP')
        index_2 = init_string.find(',')
        if (index_1 != -1 and index_2 != -1):
            sec_string = init_string[index_1+6:index_2]
            bad_chars = [':', '}', '\\', '\"']
            for i in bad_chars:
                sec_string = sec_string.replace(i, '')
        return sec_string
        
        
        
    def processSendNewData(self, var):
        self.ui.cmdIndexLabel.setText(str(var))
        
        
    def processNewData(self, var):
        """handler for new data received by serial"""
        global receive_is_done
        if len(var) > 0:
            received_bytes = var.pop(0)
            self.recv_bytes = self.recv_bytes + received_bytes
            #print(self.recv_bytes) # debug
            if self.rx_state == 'Idle' and 0x11 in self.recv_bytes:
                #print('first byte found!')
                if len(self.recv_bytes) >= 8:
                    self.data_length = self.recv_bytes[4] + (self.recv_bytes[5] << 8) + \
                        (self.recv_bytes[6] << 16) + (self.recv_bytes[7] << 24)
                    self.data_type = (self.recv_bytes[3] << 8) + self.recv_bytes[2]
                    
                    if len(self.recv_bytes) == self.data_length + 12:
                        
                        #print('data length is {}'.format(self.data_length))
                        #print('data type is {}'.format(self.data_type))
                        
                        
                        # handle raw data
                        if self.data_type == 0x0700:
                            self.i2s_all_raw_value = []
                            self.i2s_all_raw_bytes = self.recv_bytes[12:]
                            self.rx_state = 'Idle'
                            self.recv_bytes = []
                            #print(self.i2s_all_raw_bytes)
                            #print(len(self.i2s_all_raw_bytes))
                            #print('Receive I2S Raw Data!')
                            i2s_value = 0
                            for i in range(0, len(self.i2s_all_raw_bytes)//2):
                                i2s_value = self.i2s_all_raw_bytes[2*i] + (self.i2s_all_raw_bytes[2*i+1] << 8)                            
                                if (i2s_value & 0x8000):
                                    self.i2s_all_raw_value.append(i2s_value - 65536)
                                else:
                                    self.i2s_all_raw_value.append(i2s_value)
                            
                            #print(self.i2s_all_raw_value)
                            
                            for i in range(8):
                                self.i2s_chan_raw_value[i] = self.i2s_all_raw_value[i*512 : i*512 + 512]
                                #print(len(self.i2s_chan_raw_value[i]))
                                self.ui.curve_i2s_raw[i].setData(range(0, len(self.i2s_chan_raw_value[i])), self.i2s_chan_raw_value[i])
                        
                        # handle FFT data
                        if self.data_type == 0x0400:
                            self.i2s_all_fft_value = []
                            self.i2s_all_fft_bytes = self.recv_bytes[12:]
                            self.rx_state = 'Idle'
                            self.recv_bytes = []
                            #print(self.i2s_all_fft_bytes)
                            #print(len(self.i2s_all_raw_bytes))
                            #print('Receive I2S FFT Data!')
                            i2s_fft_value = 0
                            for i in range(0, len(self.i2s_all_fft_bytes)//2):
                                i2s_fft_value = self.i2s_all_fft_bytes[2*i] + (self.i2s_all_fft_bytes[2*i+1] << 8)                            
                                if (i2s_fft_value & 0x8000):
                                    self.i2s_all_fft_value.append(i2s_fft_value - 65536)
                                else:
                                    self.i2s_all_fft_value.append(i2s_fft_value)
                            
                            #print(self.i2s_all_fft_value)
                            
                            
                            
                            
                            
                            for i in range(8):
                                self.i2s_chan_fft_value[i] = self.i2s_all_fft_value[i*256 : i*256 + 256]
                                
                                self.i2s_fft_max_value[i] = max(self.i2s_chan_fft_value[i])
                                
                                #print(len(self.i2s_chan_fft_value[i]))
                                x_f = []
                                for j in range(len(self.i2s_chan_fft_value[i])):
                                    x_f.append(j * float(self.i2s_sample_frequency) / 512)
                                #x_f = list(range(0, len(self.i2s_chan_fft_value[i]))) * self.i2s_sample_frequency / 256 / 1000
                                #print(x_f)
                                self.ui.curve_i2s_fft[i].setData(x_f, self.i2s_chan_fft_value[i])
                            
                            self.max_index = self.i2s_all_fft_value.index(max(self.i2s_all_fft_value)) // 256
                            
                            self.ui.indexLabel.setText(str(self.max_index))
                            print('Channel index with max power is {}'.format(self.max_index) )
                            
                            print('max value array is {}'.format(self.i2s_fft_max_value))
                            print('standard deviation of max value: {}'.format(np.std(self.i2s_fft_max_value)))
                            
                            max_difference = max(self.i2s_fft_max_value) - min(self.i2s_fft_max_value)
                            print('Max difference of max values: {}'.format(max_difference))
                            #self.ui.curve_i2s_raw_1.setData(range(0, 100), [0, 99])
                            #self.ui.pw1.curve_i2s_raw_2.setData(range(0, len(self.i2s_all_raw_value[512:1023])), self.i2s_all_raw_value[512:1023])
                                              
                                              
                        if self.data_type == 0x0800:
                            self.i2s_all_raw_value = []
                            self.i2s_all_raw_bytes = self.recv_bytes[12:8204]
                            self.i2s_all_fft_value = []
                            self.i2s_all_fft_bytes = self.recv_bytes[8204:]
                            self.rx_state = 'Idle'
                            self.recv_bytes = []
                            
                            i2s_value = 0
                            for i in range(0, len(self.i2s_all_raw_bytes)//2):
                                i2s_value = self.i2s_all_raw_bytes[2*i] + (self.i2s_all_raw_bytes[2*i+1] << 8)                            
                                if (i2s_value & 0x8000):
                                    self.i2s_all_raw_value.append(i2s_value - 65536)
                                else:
                                    self.i2s_all_raw_value.append(i2s_value)
                                                                                   
                            for i in range(8):
                                self.i2s_chan_raw_value[i] = self.i2s_all_raw_value[i*512 : i*512 + 512]
                                #print(len(self.i2s_chan_raw_value[i]))
                                self.ui.curve_i2s_raw[i].setData(range(0, len(self.i2s_chan_raw_value[i])), self.i2s_chan_raw_value[i])
                        
                            i2s_fft_value = 0
                            for i in range(0, len(self.i2s_all_fft_bytes)//2):
                                i2s_fft_value = self.i2s_all_fft_bytes[2*i] + (self.i2s_all_fft_bytes[2*i+1] << 8)                            
                                if (i2s_fft_value & 0x8000):
                                    self.i2s_all_fft_value.append(i2s_fft_value - 65536)
                                else:
                                    self.i2s_all_fft_value.append(i2s_fft_value)
                                    
                            for i in range(8):
                                self.i2s_chan_fft_value[i] = self.i2s_all_fft_value[i*256 : i*256 + 256]
                                
                                self.i2s_fft_max_value[i] = max(self.i2s_chan_fft_value[i])
                                
                                #print(len(self.i2s_chan_fft_value[i]))
                                x_f = []
                                for j in range(len(self.i2s_chan_fft_value[i])):
                                    x_f.append(j * float(self.i2s_sample_frequency) / 512)
                                
                                self.ui.curve_i2s_fft[i].setData(x_f, self.i2s_chan_fft_value[i])
                                
                            self.max_index = self.i2s_all_fft_value.index(max(self.i2s_all_fft_value)) // 256
                            self.ui.indexLabel.setText(str(self.max_index))
                            print('Channel index with max power is {}'.format(self.max_index) )
                            
                            print('max value array is {}'.format(self.i2s_fft_max_value))
                            print('standard deviation of max value: {}'.format(np.std(self.i2s_fft_max_value)))
                            
                            max_difference = max(self.i2s_fft_max_value) - min(self.i2s_fft_max_value)
                            print('Max difference of max values: {}'.format(max_difference))
                            
                            
                        if self.data_type == 0x0900:
                            
                            self.dir_all_raw_bytes = self.recv_bytes[12:]
                            self.rx_state = 'Idle'
                            self.recv_bytes = []
                            
                            self.dir_all_raw_value = []
                            dir_value = 0
                            for i in range(0, len(self.dir_all_raw_bytes)//2):
                                dir_value = self.dir_all_raw_bytes[2*i] + (self.dir_all_raw_bytes[2*i+1] << 8)                            
                                if (dir_value & 0x8000):
                                    self.dir_all_raw_value.append(dir_value - 65536)
                                else:
                                    self.dir_all_raw_value.append(dir_value)
                            
                            #print(self.i2s_all_raw_value)
                            
                            for i in range(16):
                                self.dir_chan_raw_value[i] = self.dir_all_raw_value[i*256 : i*256 + 256]
                                self.ui.curve_dir_raw[i].setData(range(0, len(self.dir_chan_raw_value[i])), self.dir_chan_raw_value[i])
                        
                            
                            
                        if self.data_type == 0x0101:
                            init_string = ''.join([chr(elem) for elem in self.recv_bytes[12:]])
                            self.rx_state = 'Idle'
                            self.recv_bytes = []
                            #print(init_string)
                            name_string = self.findJsonResponseNameField(init_string)
                            value_string = self.findJsonValueField(init_string)
                            
                            # update ui to display the new data
                            '''
                            if name_string == 'RBC':
                                self.ui.secondClickLabel.setText('{}: {}'.format(name_string, value_string))                    
                                self.batt_current_data.append(float(value_string))
                                #self.ui.curve_batt_current.setData(range(0, len(self.batt_current_data)), self.batt_current_data)
                            elif name_string == 'RBV':
                                self.ui.clicksLabel.setText('{}: {}'.format(name_string, value_string))
                                self.batt_voltage_data.append(float(value_string))
                                #self.ui.curve_i2s_raw_1.setData(range(0, len(self.batt_voltage_data)), self.batt_voltage_data)
                            '''
                            if name_string == 'RFV':
                                #self.ui.thirdLabel.setText('{}: {}'.format(name_string, value_string))
                                self.ui.rspLabel.setText('{}: {}'.format(name_string, value_string))
                            else:
                                #self.ui.rspLabel.setText('{}: {}'.format(name_string, value_string))
                                self.ui.rspLabel.setText(init_string)
                        receive_is_done = True
            else:
                self.recv_bytes = []
                        
                
                                         
    def commandRadioBtnClicks(self):
        global current_cycle_cmd
        radioButton = self.sender()
        
        if radioButton.isChecked():
            print("Comamnd is %s" % (radioButton.command_name))    
            #self.command_list = []
            #all_cmd_list = []
            #all_cmd_list.append(str(radioButton.command_name))
            current_cycle_cmd = str(radioButton.command_name)
            #print('Command list is {}'.format(all_cmd_list))
    
   
    '''
    def secondSendButtonClicks(self):
        """handler of clicking the second send button"""
        #self.ui.clicksLabel.setText('click send button')        
        self.serial_transmitter.send_command_string('F', 'JSON_Type')    
    '''    
        
    def mcu0CheckBoxClicks(self, state):
        global all_mcu_list
        chkBox = self.sender()
        #print(str(chkBox.isChecked()))
        if (chkBox.isChecked()):
            all_mcu_list.append('0')
        else:
            all_mcu_list.remove('0')
        #print(all_mcu_list)
            
            
    def mcu1CheckBoxClicks(self, state):
        global all_mcu_list
        chkBox = self.sender()
        #print(str(chkBox.isChecked()))
        if (chkBox.isChecked()):
            all_mcu_list.append('1')
        else:
            all_mcu_list.remove('1')
        #print(all_mcu_list)
        
        
    def mcu2CheckBoxClicks(self, state):
        chkBox = self.sender()
        #print(str(chkBox.isChecked()))
        if (chkBox.isChecked()):
            all_mcu_list.append('2')
        else:
            all_mcu_list.remove('2')
        #print(all_mcu_list)
        
        
    def commandSendButtonClicks(self):
        """handler of clicking the one-time send button"""
        self.recv_bytes = []
        current_command = str(self.ui.cmdComboBox.currentText())
        if current_command != "":
            #print(self.ui.cmdlineEdit.text())
            if 'i2s_raw' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'I2S_Raw_Type')
            elif 'i2s_soft' in current_command or 'i2s_hard' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'I2S_FFT_Type')
            elif 'i2s_fft_raw' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'RAW_FFT_Type')
            elif 'dir_raw' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'DIR_RAW_Type')
            elif 'CMD' in current_command:
                self.serial_transmitter.send_command_string(current_command, 'JSON_Type')
            
    
    
    def readingStartButtonClicks(self):
        """handler for clicking the Start Sending (cycle reading) Button"""
        global enable_cycle_send, current_cycle_cmd, all_mcu_list, all_cmd_list
        enable_cycle_send = not enable_cycle_send
        if enable_cycle_send == True:
            self.ui.cmdStartBtn.setText('Stop Reading')
            #print(current_cycle_cmd)
            #print(all_mcu_list)
            
            all_cmd_list = []
            
            if '0' in all_mcu_list:
                current_cmd = current_cycle_cmd 
                all_cmd_list.append(current_cmd)
            if '1' in all_mcu_list:
                current_cmd = current_cycle_cmd + ' -target 1'
                all_cmd_list.append(current_cmd)
            if '2' in all_mcu_list:
                current_cmd = current_cycle_cmd + ' -target 2'
                all_cmd_list.append(current_cmd)
                
            
                
            
            print('Ready to send commands {}'.format(all_cmd_list))
            
            self.ui.i2sRadioButton.setEnabled(False)
            self.ui.dirRadioButton.setEnabled(False)
            self.ui.mcu0ChkBox.setEnabled(False)
            self.ui.mcu1ChkBox.setEnabled(False)
            self.ui.mcu2ChkBox.setEnabled(False)
        else:
            self.ui.cmdStartBtn.setText('Start Reading')
            self.ui.i2sRadioButton.setEnabled(True)
            self.ui.dirRadioButton.setEnabled(True)
            self.ui.mcu0ChkBox.setEnabled(True)
            self.ui.mcu1ChkBox.setEnabled(True)
            self.ui.mcu2ChkBox.setEnabled(True)
            
            
    
    
            
if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    main = ApplicationWindow()
    main.resize(1200, 700)
    main.setWindowTitle('Plot Battery Voltage and Current')
    main.show()
    sys.exit(app.exec_())
