#!/usr/bin/env python
#
# Very simple serial terminal
#
# This file is part of pySerial. https://github.com/pyserial/pyserial
# (C)2002-2017 Chris Liechti <cliechti@gmx.net>
#
# SPDX-License-Identifier:    BSD-3-Clause


""" Usage of this terminal program for PULD
python miniterm.py 
choose the COM port to connect with

Ctrl+S followed by a single character to send pre-defined command, can be expanded in send_command_string()

Ctrl+S followed by V: {"CMD": "RBV"}
Ctrl+S followed by C: {"CMD": "RBC"}
Ctrl+S followed by I: {"CMD": "RID"}
Ctrl+S followed by F: {"CMD": "RFV"}
Ctrl+S followed by v: version


Ctrl+A is to enter the command mode
In the command mode, linux-like command and JSON formatted command are both acceptable. 
e.g., version -target 1
{"CMD": "RBV"}

Use Ctrl+] in the menu mode to exit the program

"""



from __future__ import absolute_import

import codecs
import os
import sys
import threading
import re

import time

import serial
from serial.tools.list_ports import comports
from serial.tools import hexlify_codec

import random

import os
cwd = os.path.dirname(os.path.realpath(__file__))
os.chdir(cwd)

import ctypes  # An included library with Python install.
def Mbox(title, text, style):
    return ctypes.windll.user32.MessageBoxW(0, text, title, style)
#result=Mbox('cwd = ', cwd, 1)

# pylint: disable=wrong-import-order,wrong-import-position

codecs.register(lambda c: hexlify_codec.getregentry() if c == 'hexlify' else None)

try:
    raw_input
except NameError:
    # pylint: disable=redefined-builtin,invalid-name
    raw_input = input   # in python3 it's "raw"
    unichr = chr


def key_description(character):
    """generate a readable description for a key"""
    ascii_code = ord(character)
    if ascii_code < 32:
        return 'Ctrl+{:c}'.format(ord('@') + ascii_code)
    else:
        return repr(character)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class ConsoleBase(object):
    """OS abstraction for console (input/output codec, no echo)"""

    def __init__(self):
        if sys.version_info >= (3, 0):
            self.byte_output = sys.stdout.buffer
        else:
            self.byte_output = sys.stdout
        self.output = sys.stdout

    def setup(self):
        """Set console to read single characters, no echo"""

    def cleanup(self):
        """Restore default console settings"""

    def getkey(self):
        """Read a single key from the console"""
        return None

    def write_bytes(self, byte_string):
        """Write bytes (already encoded)"""
        self.byte_output.write(byte_string)
        self.byte_output.flush()

    def write(self, text):
        """Write string"""
        self.output.write(text)
        self.output.flush()

    def cancel(self):
        """Cancel getkey operation"""

    #  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -
    # context manager:
    # switch terminal temporary to normal mode (e.g. to get user input)

    def __enter__(self):
        self.cleanup()
        return self

    def __exit__(self, *args, **kwargs):
        raw_input()
        self.setup()

if os.name == 'nt':  # noqa
    import msvcrt
    import ctypes

    class Out(object):
        """file-like wrapper that uses os.write"""

        def __init__(self, fd):
            self.fd = fd

        def flush(self):
            pass

        def write(self, s):
            os.write(self.fd, s)

    class Console(ConsoleBase):
        def __init__(self):
            super(Console, self).__init__()
            self._saved_ocp = ctypes.windll.kernel32.GetConsoleOutputCP()
            self._saved_icp = ctypes.windll.kernel32.GetConsoleCP()
            ctypes.windll.kernel32.SetConsoleOutputCP(65001)
            ctypes.windll.kernel32.SetConsoleCP(65001)
            self.output = codecs.getwriter('UTF-8')(Out(sys.stdout.fileno()), 'replace')
            # the change of the code page is not propagated to Python, manually fix it
            sys.stderr = codecs.getwriter('UTF-8')(Out(sys.stderr.fileno()), 'replace')
            sys.stdout = self.output
            self.output.encoding = 'UTF-8'  # needed for input

        def __del__(self):
            ctypes.windll.kernel32.SetConsoleOutputCP(self._saved_ocp)
            ctypes.windll.kernel32.SetConsoleCP(self._saved_icp)

        def getkey(self):
            while True:
                z = msvcrt.getwch()
                if z == unichr(13):
                    return unichr(10)
                elif z in (unichr(0), unichr(0x0e)):    # functions keys, ignore
                    msvcrt.getwch()
                else:
                    return z

        def cancel(self):
            # CancelIo, CancelSynchronousIo do not seem to work when using
            # getwch, so instead, send a key to the window with the console
            hwnd = ctypes.windll.kernel32.GetConsoleWindow()
            ctypes.windll.user32.PostMessageA(hwnd, 0x100, 0x0d, 0)

elif os.name == 'posix':
    import atexit
    import termios
    import fcntl

    class Console(ConsoleBase):
        def __init__(self):
            super(Console, self).__init__()
            self.fd = sys.stdin.fileno()
            self.old = termios.tcgetattr(self.fd)
            atexit.register(self.cleanup)
            if sys.version_info < (3, 0):
                self.enc_stdin = codecs.getreader(sys.stdin.encoding)(sys.stdin)
            else:
                self.enc_stdin = sys.stdin

        def setup(self):
            new = termios.tcgetattr(self.fd)
            new[3] = new[3] & ~termios.ICANON & ~termios.ECHO & ~termios.ISIG
            new[6][termios.VMIN] = 1
            new[6][termios.VTIME] = 0
            termios.tcsetattr(self.fd, termios.TCSANOW, new)

        def getkey(self):
            c = self.enc_stdin.read(1)
            if c == unichr(0x7f):
                c = unichr(8)    # map the BS key (which yields DEL) to backspace
            return c

        def cancel(self):
            fcntl.ioctl(self.fd, termios.TIOCSTI, b'\0')

        def cleanup(self):
            termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.old)

else:
    raise NotImplementedError(
        'Sorry no implementation for your platform ({}) available.'.format(sys.platform))

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

class Transform(object):
    """do-nothing: forward all data unchanged"""
    def rx(self, text):
        """text received from serial port"""
        return text

    def tx(self, text):
        """text to be sent to serial port"""
        return text

    def echo(self, text):
        """text to be sent but displayed on console"""
        return text


class CRLF(Transform):
    """ENTER sends CR+LF"""

    def tx(self, text):
        return text.replace('\n', '\r\n')


class CR(Transform):
    """ENTER sends CR"""

    def rx(self, text):
        return text.replace('\r', '\n')

    def tx(self, text):
        return text.replace('\n', '\r')


class LF(Transform):
    """ENTER sends LF"""


class NoTerminal(Transform):
    """remove typical terminal control codes from input"""

    REPLACEMENT_MAP = dict((x, 0x2400 + x) for x in range(32) if unichr(x) not in '\r\n\b\t')
    REPLACEMENT_MAP.update(
        {
            0x7F: 0x2421,  # DEL
            0x9B: 0x2425,  # CSI
        })

    def rx(self, text):
        return text.translate(self.REPLACEMENT_MAP)

    echo = rx


class NoControls(NoTerminal):
    """Remove all control codes, incl. CR+LF"""

    REPLACEMENT_MAP = dict((x, 0x2400 + x) for x in range(32))
    REPLACEMENT_MAP.update(
        {
            0x20: 0x2423,  # visual space
            0x7F: 0x2421,  # DEL
            0x9B: 0x2425,  # CSI
        })


class Printable(Transform):
    """Show decimal code for all non-ASCII characters and replace most control codes"""

    def rx(self, text):
        r = []
        for c in text:
            if ' ' <= c < '\x7f' or c in '\r\n\b\t':
                r.append(c)
            elif c < ' ':
                r.append(unichr(0x2400 + ord(c)))
            else:
                r.extend(unichr(0x2080 + ord(d) - 48) for d in '{:d}'.format(ord(c)))
                r.append(' ')
        return ''.join(r)

    echo = rx


class Colorize(Transform):
    """Apply different colors for received and echo"""

    def __init__(self):
        # XXX make it configurable, use colorama?
        self.input_color = '\x1b[37m'
        self.echo_color = '\x1b[31m'

    def rx(self, text):
        return self.input_color + text

    def echo(self, text):
        return self.echo_color + text


class DebugIO(Transform):
    """Print what is sent and received"""

    def rx(self, text):
        sys.stderr.write(' [RX:{!r}] '.format(text))
        sys.stderr.flush()
        return text

    def tx(self, text):
        sys.stderr.write(' [TX:{!r}] '.format(text))
        sys.stderr.flush()
        return text


# other ideas:
# - add date/time for each newline
# - insert newline after: a) timeout b) packet end character

EOL_TRANSFORMATIONS = {
    'crlf': CRLF,
    'cr': CR,
    'lf': LF,
}

TRANSFORMATIONS = {
    'direct': Transform,    # no transformation
    'default': NoTerminal,
    'nocontrol': NoControls,
    'printable': Printable,
    'colorize': Colorize,
    'debug': DebugIO,
}

def search_for_port(com_port):
    """\
    Show a list of ports and ask the user for a choice. To make selection
    easier on systems with long device names, also allow the input of an
    index.
    """
    sys.stderr.write('\n--- Available ports:\n')
    ports = []
    for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
        sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
        ports.append(port)
    
    index = 0
    while index < n:
        #port = raw_input('--- Enter port index or full name: ')
        if (ports[index] == com_port):
            return ports[index]
        index = index + 1
        
    print('Request port does NOT match any port we have, please select one...')    
    while True:
        port = raw_input('--- Enter port index or full name: ')
        try:
            index = int(port) - 1
            if not 0 <= index < len(ports):
                sys.stderr.write('--- Invalid index!\n')
                continue            
        except ValueError:
            pass
        else:
            port = ports[index]
        return port
        
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
def ask_for_port():
    """\
    Show a list of ports and ask the user for a choice. To make selection
    easier on systems with long device names, also allow the input of an
    index.
    """
    sys.stderr.write('\n--- Available ports:\n')
    ports = []
    for n, (port, desc, hwid) in enumerate(sorted(comports()), 1):
        sys.stderr.write('--- {:2}: {:20} {!r}\n'.format(n, port, desc))
        ports.append(port)
    while True:
        port = raw_input('--- Enter port index or full name: ')
        try:
            index = int(port) - 1
            if not 0 <= index < len(ports):
                sys.stderr.write('--- Invalid index!\n')
                continue
        except ValueError:
            pass
        else:
            port = ports[index]
        return port

class Miniterm(object):
    """\
    Terminal application. Copy data from serial port to console and vice versa.
    Handle special keys from the console to show menu etc.
    """

    def __init__(self, serial_instance, echo=False, eol='crlf', filters=(), bin_file = "PULD.bin", mcu=0, cipher=0, version='PULD bin'):
        self.console = Console()
        self.serial = serial_instance
        self.echo = echo
        self.raw = False
        self.input_encoding = 'UTF-8'
        self.output_encoding = 'UTF-8'
        self.eol = eol
        self.filters = filters
        self.update_transformations()
        self.exit_character = unichr(0x1d)  # GS/CTRL+]
        self.menu_character = unichr(0x13)  # Menu: CTRL+T, changed to CTRL+S by bwu
        self.cmd_character  = unichr(0x01)  # CTRL+A as start of a command 
        self.alive = None
        self._reader_alive = None
        self.receiver_thread = None
        self.rx_decoder = None
        self.tx_decoder = None
        
        self.menu_state = 'Menu_Mode'
        self.raw_command = []
        
        self.ota_state = 'Idle'
        
        #self.op_mode = 'CMD_Mode'
        
        self.recv_bytes = []
        self.recv_bytes_list = []
        self.recv_string_length = 0
        self.file_frame_count = 0
        self.file_bin_size = 0
        self.file_frame_size = 2048 * 5
        self.file_chunk_size = []
        
        self.file_frame_index = 1   # initial index = 1, increase from 1 to count
        self.file_frame_count = 0
        self.file_frame = []
        self.ota_start_time = time.time()
        self.ota_end_time   = time.time()
        
        self.ota_start_flag = False
        
        self.target_mcu = mcu
        
        self.json_missing_counts = 0
        self.ota_command = []
        self.ota_command_list = []
        self.ota_proc_event = threading.Event()
        self._processor_alive = None
        self.enter_ota_state = False
        self.ota_state_loop = 0
        self.OTA_STATE_LOOP_COUNT = 100
        self.response_mcu = 0   # use to count how many mcu response the OTA command 
        self.response_complete = 0
        if self.target_mcu == 3:
            self.ota_mode = 1       # 1: one-to-3 mode
        else:
            self.ota_mode = 0       # 0: one-to-one mode
        
        self.cipher_flag = cipher
        self.bin_version = version
        
        # do the OTA host, get the binary file frame countsWant to bootload
        with open(bin_file, 'rb') as f:
            chunk = f.read(self.file_frame_size)
            while chunk:
                
                self.file_frame.append((chunk))
                self.file_frame_count += 1
                self.file_chunk_size.append(len(chunk))
                self.file_bin_size += len(chunk)
                #print('binary file\'s frame {} has size {}'.format(self.file_frame_count, self.file_chunk_size[self.file_frame_count-1]))
                    
                chunk = f.read(self.file_frame_size)
            f.close()
            print('Binary file size {} bytes = {} frames x {} bytes + the last frame of {} bytes'.format(self.file_bin_size, self.file_frame_count-1, \
            self.file_frame_size, self.file_chunk_size[-1]))
            #print(len(self.file_frame[0]))
            #print(self.file_frame[0][1])
        
    def set_target_mcu(self, x):
        self.target_mcu = x

    def _start_processor(self):
        """Start processor thread"""
        self._processor_alive = True
        self.processor_thread = threading.Thread(target=self.process_recv_bytes, name='prox', args=(self.recv_bytes_list,))
        self.processor_thread.daemon = True
        self.processor_thread.start()
        
    def _stop_processor(self):
        """Stop processor thread"""
        self._processor_alive = False
        self.processor_thread.join()
        

    def _start_reader(self):
        """Start reader thread"""
        self._reader_alive = True
        # start serial->console thread
        self.receiver_thread = threading.Thread(target=self.reader, name='rx')
        self.receiver_thread.daemon = True
        self.receiver_thread.start()

    def _stop_reader(self):
        """Stop reader thread only, wait for clean exit of thread"""
        self._reader_alive = False
        if hasattr(self.serial, 'cancel_read'):
            self.serial.cancel_read()
        self.receiver_thread.join()

    def start(self):
        """start worker threads"""
        self.alive = True
        self._start_reader()
        self._start_processor()
        # enter console->serial loop
        self.transmitter_thread = threading.Thread(target=self.writer, name='tx')
        self.transmitter_thread.daemon = True
        self.transmitter_thread.start()
        self.console.setup()

    def stop(self):
        """set flag to stop worker threads"""
        self.alive = False

    def join(self, transmit_only=False):
        """wait for worker threads to terminate"""
        self.transmitter_thread.join()
        if not transmit_only:
            if hasattr(self.serial, 'cancel_read'):
                self.serial.cancel_read()
            self.receiver_thread.join()
            self.processor_thread.join()

    def close(self):
        self.serial.close()

    def update_transformations(self):
        """take list of transformation classes and instantiate them for rx and tx"""
        transformations = [EOL_TRANSFORMATIONS[self.eol]] + [TRANSFORMATIONS[f]
                                                             for f in self.filters]
        self.tx_transformations = [t() for t in transformations]
        self.rx_transformations = list(reversed(self.tx_transformations))

    def set_rx_encoding(self, encoding, errors='replace'):
        """set encoding for received data"""
        self.input_encoding = encoding
        self.rx_decoder = codecs.getincrementaldecoder(encoding)(errors)

    def set_tx_encoding(self, encoding, errors='replace'):
        """set encoding for transmitted data"""
        self.output_encoding = encoding
        self.tx_encoder = codecs.getincrementalencoder(encoding)(errors)

    def dump_port_settings(self):
        """Write current settings to sys.stderr"""
        sys.stderr.write("\n--- Settings: {p.name}  {p.baudrate},{p.bytesize},{p.parity},{p.stopbits}\n".format(
            p=self.serial))
        sys.stderr.write('--- RTS: {:8}  DTR: {:8}  BREAK: {:8}\n'.format(
            ('active' if self.serial.rts else 'inactive'),
            ('active' if self.serial.dtr else 'inactive'),
            ('active' if self.serial.break_condition else 'inactive')))
        try:
            sys.stderr.write('--- CTS: {:8}  DSR: {:8}  RI: {:8}  CD: {:8}\n'.format(
                ('active' if self.serial.cts else 'inactive'),
                ('active' if self.serial.dsr else 'inactive'),
                ('active' if self.serial.ri else 'inactive'),
                ('active' if self.serial.cd else 'inactive')))
        except serial.SerialException:
            # on RFC 2217 ports, it can happen if no modem state notification was
            # yet received. ignore this error.
            pass
        sys.stderr.write('--- software flow control: {}\n'.format('active' if self.serial.xonxoff else 'inactive'))
        sys.stderr.write('--- hardware flow control: {}\n'.format('active' if self.serial.rtscts else 'inactive'))
        sys.stderr.write('--- serial input encoding: {}\n'.format(self.input_encoding))
        sys.stderr.write('--- serial output encoding: {}\n'.format(self.output_encoding))
        sys.stderr.write('--- EOL: {}\n'.format(self.eol.upper()))
        sys.stderr.write('--- filters: {}\n'.format(' '.join(self.filters)))


    def start_ota(self):

        self.set_target_mcu(self.target_mcu)
        #ota_confirm = input('\r\nWant to bootload MCU? [Y=Yes|N=No]:')
        #if ota_confirm == 'Y' or ota_confirm == 'y':
        #    x = input('=== Which MCU (0|1|2) to update firmware: ')
        #    if x == '0' or x == '1' or x == '2':
        #        self.set_target_mcu(int(x))
        #    else:
        #        print('The default MCU 0 is chosen for bootloading...')
        #        self.set_target_mcu(0)
        #    print('--- Ctrl+S followed by \'t\' to start OTA ---')
        #else:
        #    print('Did not start OTA, Ctrl+S to enter Menu Mode')
                    
    
            
            
            
            


    def send_command_string(self, letter, command_type):
        """Write Micro Jason string to sys.stderr"""
        """
        HH(0x11), VV(Version Byte), TH(0x00: Raw, 0x01:Json), TL(0x00~0x04), Length(4 bytes, LSB first?), checksum, Reserve1, R2, R3, <data>
        
        """
        header    = 0x11
        #print(header.to_bytes(1, 'big'))
        version   = 0x02
        if command_type == 'JSON_Type':
            type_high = 0x01
            type_low  = 0x01
        elif command_type == 'LLC_Type':
            type_high = 0x02
            type_low  = 0x05
        elif command_type == 'Data_Type':
            type_high = 0x00
            type_low  = 0x06
            
        length_1  = 0
        length_2  = 0x00
        length_3  = 0x00
        length_4  = 0x00
        reserve_1 = 0x00
        reserve_2 = 0x00
        reserve_3 = 0x00
        
        """
        if letter == '\x11':  # CTRL+Q
            cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"ENABLE"}')
        elif letter == '\x1A':  # CTRL+Z
            cmd_string = list('{"CMD":"HEN","MCU":1,"STATE":"DISABLE"}')
        elif letter == 'V':
            cmd_string = list('{"CMD": "RBV"}')
        elif letter == 'C':
            cmd_string = list('{"CMD": "RBC"}')
        elif letter == 'I':
            cmd_string = list('{"CMD": "RID", "MCU":2}')
        elif letter == 'F':
            cmd_string = list('{"CMD": "RFV", "MCU":1}')
        elif letter == 'p':
            cmd_string = list('{"CMD": "RPV"}')
        elif letter == 'v':
            cmd_string = list('version')
        """
        if letter == 'f':
            if (self.ota_state == 'Received_OTA_Info'):
                self.file_frame_index = 1
                str_temp = 'ota_frame -index {} -size {} -target {}'.format(self.file_frame_index, \
                self.file_chunk_size[self.file_frame_index-1], self.target_mcu)
                #print(str_temp)
                cmd_string = list(str_temp)
            elif self.ota_state == 'Received_Frame_Info':
                frame_data = list((self.file_frame[self.file_frame_index-1]))
                
                #print(self.file_frame[self.file_frame_index-1])
                
                print('Send {} / {} frame data with frame size {} bytes\r\n'.format(self.file_frame_index, \
                self.file_frame_count, self.file_chunk_size[self.file_frame_index-1]), end="\r", flush=True)
                cmd_string = []
                for item in frame_data:
                    cmd_string.append(chr(item))
                #print(cmd_string)
            elif self.ota_state == 'Received_Frame_Data':
                str_temp = 'ota_frame -index {} -size {} -target {}'.format(self.file_frame_index, \
                self.file_chunk_size[self.file_frame_index-1], self.target_mcu)
                #print('frame index {}'.format(self.file_frame_index))
                #print(str_temp)
                cmd_string = list(str_temp)
            
        elif letter == 't':  # OTA start from here

            self.menu_state = 'OTA_Mode'
            self.ota_start_time = time.time()
            self.ota_start_flag = True
            if self.cipher_flag == 0:
                str_temp = 'ota_info -crc 0 -version {} -count {} -size {} -target {}'.format(self.bin_version, \
                #str_temp = 'ota_info -crc 0 -version {} -count {} -size {} -target {} -cipher {}'.format(self.bin_version, \
                self.file_frame_count, self.file_bin_size, self.target_mcu)
            else:
                str_temp = 'ota_info -crc 0 -version {} -count {} -size {} -target {} -cipher {}'.format(self.bin_version, \
                self.file_frame_count, self.file_bin_size, self.target_mcu, self.cipher_flag)
            
            cmd_string = list(str_temp)
            print(str_temp)
        
        else:
            return
        
        length_1 = len(cmd_string) & 0xFF
        length_2 = (len(cmd_string) >> 8) & 0xFF
        length_3 = (len(cmd_string) >> 16) & 0xFF
        length_4 = (len(cmd_string) >> 24) & 0xFF
        
        calculated_checksum = 0
        check_sum = 0
        pay_load = []
        for i in range(0, len(cmd_string)):
            pay_load.append(ord(cmd_string[i]))
            calculated_checksum += pay_load[i]
        
        
        #calculated_checksum = header + version + type_high + type_low + length_1 + length_2 + length_3 + length_4 + reserve_1 + reserve_2 + reserve_3 + reserve_4
        
        calculated_checksum = calculated_checksum & 0xFF
        calculated_checksum = 0xFF - calculated_checksum
        
        check_sum = calculated_checksum
        #print('check_sum is ' + hex(check_sum))
        
        tx_data = []
        tx_data.append(header.to_bytes(1, 'big'))
        tx_data.append(version.to_bytes(1, 'big'))
        tx_data.append(type_high.to_bytes(1, 'big'))
        tx_data.append(type_low.to_bytes(1, 'big'))
        tx_data.append(length_1.to_bytes(1, 'big'))
        tx_data.append(length_2.to_bytes(1, 'big'))
        tx_data.append(length_3.to_bytes(1, 'big'))
        tx_data.append(length_4.to_bytes(1, 'big'))
        tx_data.append(check_sum.to_bytes(1, 'big'))
        tx_data.append(reserve_1.to_bytes(1, 'big'))
        tx_data.append(reserve_2.to_bytes(1, 'big'))
        tx_data.append(reserve_3.to_bytes(1, 'big'))

        
        #for i in range(0, len(pay_load)):
        for item in pay_load:
            tx_data.append(item.to_bytes(1, 'big'))        
        
        #if command_type == 'Data_Type':
            #print(tx_data)
        
        for i in range(0, len(tx_data)):
            self.serial.write((tx_data[i]))
            #time.sleep(0.000001)
            
        #self.send_random_json_cmd()

        

    def send_raw_command_string(self, str, command_type):
        """Write Micro Jason string to sys.stderr"""
        """
        HH(0x11), VV(Version Byte), TH(0x00: Raw, 0x01:Json), TL(0x00~0x04), Length(4 bytes, LSB first?), checksum, Reserve1, R2, R3, <data>
        
        """
        header    = 0x11
        #print(header.to_bytes(1, 'big'))
        version   = 0x02
        if command_type == 'JSON_Type':
            type_high = 0x01
            type_low  = 0x01
        elif command_type == 'LLC_Type':
            type_high = 0x02
            type_low  = 0x05
            
        length_1  = 0
        length_2  = 0x00
        length_3  = 0x00
        length_4  = 0x00
        reserve_1 = 0x00
        reserve_2 = 0x00
        reserve_3 = 0x00
            
        cmd_string = str       
        
        length_1 = len(cmd_string) & 0xFF
        length_2 = (len(cmd_string) >> 8) & 0xFF
        length_3 = (len(cmd_string) >> 16) & 0xFF
        length_4 = (len(cmd_string) >> 24) & 0xFF
        
        calculated_checksum = 0
        check_sum = 0
        pay_load = []
        for i in range(0, len(cmd_string)):
            pay_load.append(ord(cmd_string[i]))
            calculated_checksum += pay_load[i]
        
        
        #calculated_checksum = header + version + type_high + type_low + length_1 + length_2 + length_3 + length_4 + reserve_1 + reserve_2 + reserve_3 + reserve_4
        
        calculated_checksum = calculated_checksum & 0xFF
        calculated_checksum = 0xFF - calculated_checksum
        
        check_sum = calculated_checksum
        #print('check_sum is ' + hex(check_sum))
        
        tx_data = []
        tx_data.append(header.to_bytes(1, 'big'))
        tx_data.append(version.to_bytes(1, 'big'))
        tx_data.append(type_high.to_bytes(1, 'big'))
        tx_data.append(type_low.to_bytes(1, 'big'))
        tx_data.append(length_1.to_bytes(1, 'big'))
        tx_data.append(length_2.to_bytes(1, 'big'))
        tx_data.append(length_3.to_bytes(1, 'big'))
        tx_data.append(length_4.to_bytes(1, 'big'))
        tx_data.append(check_sum.to_bytes(1, 'big'))
        tx_data.append(reserve_1.to_bytes(1, 'big'))
        tx_data.append(reserve_2.to_bytes(1, 'big'))
        tx_data.append(reserve_3.to_bytes(1, 'big'))

        
        for i in range(0, len(pay_load)):
            tx_data.append(pay_load[i].to_bytes(1, 'big'))
        
        
        for i in range(0, len(tx_data)):
            self.serial.write((tx_data[i]))
            
    def send_random_json_cmd(self):
        N = random.randint(0,3)
        if N == 0:
            cmd_string = '{"CMD": "RBV"}'
        elif N == 1:
            cmd_string = '{"CMD": "RBC"}'
        elif N == 2:
            cmd_string = '{"CMD": "RID", "MCU":2}'
        elif N == 3:
            cmd_string = '{"CMD": "RFV", "MCU":1}'
        
        print("Sending command:",cmd_string)
        cmd_string_list = list(cmd_string)
        self.send_raw_command_string(cmd_string_list, 'JSON_Type')


            
    def search_for_command(self):
        if self.json_missing_counts > 0:
            if len(self.recv_bytes) == self.json_missing_counts:
                self.ota_command = self.ota_command + self.recv_bytes                                
                self.ota_command_list.append(self.ota_command)
                #self.ota_proc_event.set()
                self.json_missing_counts = 0
                self.ota_command = []
                self.recv_bytes = []
                
            elif len(self.recv_bytes) < self.json_missing_counts:    # not enough
                self.ota_command = self.ota_command + self.recv_bytes
                self.json_missing_counts = self.json_missing_counts - len(self.recv_bytes)
                self.recv_bytes = []
                
            elif len(self.recv_bytes) > self.json_missing_counts:    # more than one command needs
                self.ota_command = self.ota_command + self.recv_bytes[0:self.json_missing_counts]
                self.ota_command_list.append(self.ota_command)
                #self.ota_proc_event.set()
                self.ota_command = []
                self.recv_bytes = self.recv_bytes[self.json_missing_counts:]
                self.json_missing_counts = 0
        
        while 0x11 in self.recv_bytes:
            index = self.recv_bytes.index(0x11)
            # get the length of bytes from 0x11 to the end of the array
            size = len(self.recv_bytes[index:])
            # the payload length of json command is in this array
            if size > 4:
                # get the actual length of json command 
                length = self.recv_bytes[index+4] + 12
                if size > length:
                    # a json command is in this current array
                    self.ota_command = self.recv_bytes[index : index+length]
                    self.ota_command_list.append(self.ota_command)
                    #self.ota_proc_event.set()
                    self.ota_command = []
                    self.recv_bytes = self.recv_bytes[index+length:]
                elif size == length:
                    self.ota_command = self.recv_bytes[index:]
                    self.ota_command_list.append(self.ota_command)
                    #self.ota_proc_event.set()
                    self.ota_command = []
                    self.recv_bytes = []
                elif size < length:
                    self.ota_command = self.recv_bytes[index:]
                    self.recv_bytes = []
                    self.json_missing_counts = length - size
            else:
                break
            
            
    def process_recv_bytes(self, var):
        try:        
            while self.alive and self._processor_alive:
                #if self.ota_proc_event.is_set():
                    
                if len(var) > 0:
                    self.recv_bytes = self.recv_bytes + var.pop(0) # get oldest bytes that received
                    self.search_for_command()    # reorganize to make json commands
                    
                    
                if len(self.ota_command_list) > 0:
                    ota_command = self.ota_command_list.pop(0)
                    #print('string in queue {}'.format(ota_command))
                    json_str = ''.join([chr(elem) for elem in ota_command])
                    #print(json_str[12:])
                    self.ota_fsm_handler(json_str[12:])
                else:
                    self.ota_fsm_handler('')
        except:
            self.alive = False
            raise
            
            
    def ota_fsm_handler(self, text):
        if self.ota_state == 'Idle':
            if 'ota_info' in text:
                self.response_mcu = self.response_mcu + 1
                if (self.ota_mode == 0) or (self.ota_mode == 1 and self.response_mcu == 3):
                    self.ota_state = 'Received_OTA_Info'
                    self.enter_ota_state = True
                    self.ota_state_loop = self.OTA_STATE_LOOP_COUNT
                    self.response_mcu = 0
            
            
        elif self.ota_state == 'Received_OTA_Info':
            
            self.ota_state_loop = self.ota_state_loop - 1
            if 'ota_frame' in text:
                self.response_mcu = self.response_mcu + 1
                if (self.ota_mode == 0) or (self.ota_mode == 1 and self.response_mcu == 3):
                    self.ota_state = 'Received_Frame_Info'
                    self.ota_state_loop = self.OTA_STATE_LOOP_COUNT
                    self.enter_ota_state = True
                    self.response_mcu = 0
                
            if self.enter_ota_state and self.ota_state_loop == 0:
                #print(self.ota_state)
                # send the first frame info
                print('Send first frame info')
                self.enter_ota_state = False
                self.ota_state_loop = self.OTA_STATE_LOOP_COUNT
                self.send_command_string('f', 'LLC_Type')
                
            
        
        elif self.ota_state == 'Received_Frame_Info':
            self.ota_state_loop = self.ota_state_loop - 1
            if 'ota_frame_received' in text:
                self.response_mcu = self.response_mcu + 1
                if (self.ota_mode == 0) or (self.ota_mode == 1 and self.response_mcu == 3):
                    self.ota_state = 'Received_Frame_Data'
                    self.enter_ota_state = True
                    self.ota_state_loop = self.OTA_STATE_LOOP_COUNT
                    self.response_mcu = 0
                
            if self.enter_ota_state and self.ota_state_loop == 0:
                #print(self.ota_state)
                # send frame data
                #print('Send {} frame data'.format(self.file_frame_index))
                self.enter_ota_state = False
                self.ota_state_loop = self.OTA_STATE_LOOP_COUNT
                self.send_command_string('f', 'Data_Type')
                
            
        
        elif self.ota_state == 'Received_Frame_Data':
            self.ota_state_loop = self.ota_state_loop - 1
        
            if 'ota_frame' in text:
                self.response_mcu = self.response_mcu + 1
                if (self.ota_mode == 0) or (self.ota_mode == 1 and self.response_mcu == 3):
                    self.ota_state = 'Received_Frame_Info'
                    self.enter_ota_state = True
                    self.ota_state_loop = self.OTA_STATE_LOOP_COUNT
                    self.response_mcu = 0
                
            if self.enter_ota_state and self.ota_state_loop == 0:
                #print(self.ota_state)                
                self.file_frame_index += 1
                self.enter_ota_state = False
                if self.file_frame_index <= self.file_frame_count:
                    #print('Send {} frame info'.format(self.file_frame_index))
                    self.ota_state_loop = self.OTA_STATE_LOOP_COUNT
                    self.send_command_string('f', 'LLC_Type')  # send next frame info
                
            
        
        
            
    
        '''
        #text = text[12:]
        if 'ota_info' in text and self.ota_state == 'Idle':
            self.ota_state = 'Received_OTA_Info'
            print(self.ota_state)
            # send the first frame info
            self.send_command_string('f', 'LLC_Type')
            return
        elif ('ota_frame' in text) and self.ota_state == 'Received_OTA_Info':
            self.ota_state = 'Received_Frame_Info'
            print(self.ota_state)
            self.send_command_string('f', 'Data_Type')
            return
        elif 'ota_frame_received' in text and self.ota_state == 'Received_Frame_Info':
            self.ota_state = 'Received_Frame_Data'
            print(self.ota_state)
            self.file_frame_index += 1
            if self.file_frame_index <= self.file_frame_count:
                #self.ota_state = 'Received_Frame_Data'
                #time.sleep(1e-3)
                #print(self.ota_state)
                self.send_command_string('f', 'LLC_Type')  # send next frame info
            #else:
                # should not come here, error state
            #    self.ota_state = 'OTA_Error'
            #    self.ota_start_flag = False                      
            #    self.menu_state = 'Menu_Mode'
            #    self.file_frame_index = 1
                
            # ask for next OTA 
            #time.sleep(1e-2)
            #print('Enter Ctrl+S and \'a\' to show the OTA option...')
                #time.sleep(1e-3)
            return
        elif 'ota_frame' in text and self.ota_state == 'Received_Frame_Data':
            self.ota_state = 'Received_Frame_Info'  
            print(self.ota_state)
            self.send_command_string('f', 'Data_Type')  # send next frame data
            return
        '''
#        if (text != ""):
#            print("text ", text)
        if 'STATE' in text:
            index_state = text.index('STATE')
            state_info = text[index_state+7:]
            state_info = re.sub('[" }]', '', state_info)
            if 'Fault' in state_info:                
                print(state_info)
            elif any(char.isdigit() for char in state_info):
                percentage_str = re.sub('[^0-9]', '', state_info)
                print('Done: {} %'.format(percentage_str))
            else:
                print('State: {}'.format(state_info))
                if state_info == 'COMPLETE':
                    self.response_complete = self.response_complete + 1
        #elif 'ota_receive_finished' in text and self.ota_state == 'Received_Frame_Data':
        #if 'COMPLETE' in text:
            
        if (self.ota_mode == 0 and self.response_complete == 1) or (self.ota_mode == 1 and self.response_complete == 3):
        
            self.ota_state = 'Idle'
            self.ota_start_flag = False
            time.sleep(1.1)
            #print("\n")
            #print(self.ota_state)
            self.ota_end_time = time.time()            
            print('OTA finished with period {:06.3f} seconds!'.format(self.ota_end_time - self.ota_start_time))            
            #self.recv_string = []            
            #self.menu_state = 'Menu_Mode'
            #self.file_frame_index = 1
            #self.ota_start_flag = False
            
            self.serial.close()
            self.stop() # just exit                     
                       
            #sys.exit() # suppose to close the console


            # ask for next OTA
            #time.sleep(1)
            #print('Enter Ctrl+S and \'a\' to show the OTA option...')
            
            
            
    def reader(self):
        """loop and copy serial->console"""
        try:
            while self.alive and self._reader_alive:
                # read all that is there or wait for one byte
                data = self.serial.read(self.serial.in_waiting)
                #data = self.serial.read()
                if data:
                    
                    if self.raw:
                        self.console.write_bytes(data)
                    elif self.ota_start_flag:
                        new_recv_bytes = list(data)  
                        #print(new_recv_bytes) #debug
                        
                        #str_3 = ''.join([chr(elem) for elem in new_recv_bytes])
                        #print(str_3) #debug to display readable received bytes
                        
                        # add received bytes into a list
                        self.recv_bytes_list.append(new_recv_bytes)
                        
                        
                        
                        # add new receeived bytes to the end of remaining processed bytes
                        #self.recv_bytes = self.recv_bytes + new_recv_bytes
                        #print('{}, {}'.format(self.recv_bytes, self.json_missing_counts))  # debug
                        
                        
                            
                            
                        #if len(self.ota_command_list) > 0:
                        #    print(self.ota_command_list) # debug
                        #    self.ota_proc_event.set()
                        
                        '''
                        try:
                            head_index = self.recv_bytes.index(0x11)
                            
                            if head_index == 0:
                                if (len(self.recv_bytes) >= 12):
                                    self.recv_string_length = self.recv_bytes[4] + 12
                                    if (len(self.recv_bytes) > self.recv_string_length):
                                        current_str = self.recv_bytes[0 : self.recv_string_length]
                                        #str_1 = ''.join([chr(elem) for elem in current_str])
                                        #print(str_1) #debug
                                        #self.ota_fsm_handler(str_1)
                                        
                                        self.ota_command_list.append(current_str)
                                        self.ota_proc_event.set()
                                        
                                        self.recv_bytes = self.recv_bytes[self.recv_string_length :]
                                    elif (len(self.recv_bytes) == self.recv_string_length):
                                        current_str = self.recv_bytes[0 : self.recv_string_length]
                                        
                                        self.ota_command_list.append(current_str)
                                        self.ota_proc_event.set()
                                        
                                        #str_1 = ''.join([chr(elem) for elem in current_str])
                                        #print(str_1) # debug
                                        #self.ota_fsm_handler(str_1)
                                        self.recv_bytes = []
                            else:
                                print('---0x11 in middle---')
                                
                            
                        except ValueError: # did not find 0x11
                            print('===did not find 0x11===')
                        
                        '''
                        
                        
                    elif self.ota_start_flag == False:
                        text = self.rx_decoder.decode(data)
                        for transformation in self.rx_transformations:
                            text = transformation.rx(text)
                        self.console.write(text)
                        
                        
                        #text = self.rx_decoder.decode(data)
                        #for transformation in self.rx_transformations:
                        #    text = transformation.rx(text)
                        #self.console.write(text)
                        
                        
        except serial.SerialException:
            self.alive = False
            self.console.cancel()
            raise       # XXX handle instead of re-raise?

    def writer(self):
        """\
        Loop and copy console->serial until self.exit_character character is
        found. When self.menu_character is found, interpret the next key
        locally.
        """
        menu_active = False
        try:
            while self.alive:
                try:
                    c = self.console.getkey()
                except KeyboardInterrupt:
                    c = '\x03'
                if not self.alive:
                    break
                if self.menu_state == 'Menu_Mode':
                    if menu_active:
                        self.handle_menu_key(c)
                        menu_active = False
                    elif c == self.menu_character:
                        menu_active = True      # next char will be for menu
                    elif c == self.exit_character:
                        self.stop()             # exit app
                        break
                    elif c == self.cmd_character:  # Ctrl+A to enter command mode
                        self.raw_command = []   # clear raw command list
                        self.menu_state = 'Cmd_Mode'
                        print("\r\nEnter Command Mode")
                    else:
                        #~ if self.raw:
                        text = c
                        for transformation in self.tx_transformations:
                            text = transformation.tx(text)
                        self.serial.write(self.tx_encoder.encode(text))
                        if self.echo:
                            echo_text = c
                            for transformation in self.tx_transformations:
                                echo_text = transformation.echo(echo_text)
                            self.console.write(echo_text)
                elif self.menu_state == 'Cmd_Mode':        
                    if c == '\n':
                        # process the command string if it is not empty
                        #print(self.raw_command)
                        if '{' in self.raw_command:
                            self.send_raw_command_string(self.raw_command, 'JSON_Type')
                        else:
                            self.send_raw_command_string(self.raw_command, 'LLC_Type')
                        self.raw_command = []   # clear raw command after each process the raw command
                    elif c == '\x02':    # CTRL+B
                        self.menu_state = 'Menu_Mode'
                        print("\r\nEnter Menu Mode")
                    elif c == '\x08':    # Backspace
                        if (len(self.raw_command) > 0):
                            self.raw_command.pop(len(self.raw_command)-1)
                    else:
                        self.raw_command.append(c)
                        
                    echo_text = c
                    for transformation in self.tx_transformations:
                        echo_text = transformation.echo(echo_text)
                    self.console.write(echo_text)
                
        except:
            self.alive = False
            raise

    def handle_menu_key(self, c):
        """Implement a simple menu / settings"""
        if c == self.menu_character or c == self.exit_character:
            # Menu/exit character again -> send itself
            self.serial.write(self.tx_encoder.encode(c))
            if self.echo:
                self.console.write(c)
        elif c == '\x15':                       # CTRL+U -> upload file
            self.upload_file()
        elif c in '\x08hH?':                    # CTRL+H, h, H, ? -> Show help
            sys.stderr.write(self.get_help_text())
        elif c == '\x12':                       # CTRL+R -> Toggle RTS
            self.serial.rts = not self.serial.rts
            sys.stderr.write('--- RTS {} ---\n'.format('active' if self.serial.rts else 'inactive'))
        elif c == '\x04':                       # CTRL+D -> Toggle DTR
            self.serial.dtr = not self.serial.dtr
            sys.stderr.write('--- DTR {} ---\n'.format('active' if self.serial.dtr else 'inactive'))
        elif c == '\x02':                       # CTRL+B -> toggle BREAK condition
            self.serial.break_condition = not self.serial.break_condition
            sys.stderr.write('--- BREAK {} ---\n'.format('active' if self.serial.break_condition else 'inactive'))
        elif c == '\x05':                       # CTRL+E -> toggle local echo
            self.echo = not self.echo
            sys.stderr.write('--- local echo {} ---\n'.format('active' if self.echo else 'inactive'))
        elif c == '\x06':                       # CTRL+F -> edit filters
            self.change_filter()
        elif c == '\x0c':                       # CTRL+L -> EOL mode
            modes = list(EOL_TRANSFORMATIONS)   # keys
            eol = modes.index(self.eol) + 1
            if eol >= len(modes):
                eol = 0
            self.eol = modes[eol]
            sys.stderr.write('--- EOL: {} ---\n'.format(self.eol.upper()))
            self.update_transformations()
        elif c == '\x01':                       # CTRL+A -> set encoding
            self.change_encoding()
        elif c == '\x09':                       # CTRL+I -> info
            self.dump_port_settings()
        #~ elif c == '\x01':                       # CTRL+A -> cycle escape mode
        #~ elif c == '\x0c':                       # CTRL+L -> cycle linefeed mode
        elif c == '\x1A':                       # CTRL+Z -> test microJason
            self.send_command_string(c, 'JSON_Type')
        elif c == '\x11':
            self.send_command_string(c, 'JSON_Type')
        elif c == 'V':
            self.send_command_string(c, 'JSON_Type')
        elif c == 'v':
            self.send_command_string(c, 'LLC_Type') 
        elif c == 'C':
            self.send_command_string(c, 'JSON_Type')
        
        elif c == 'C':
            self.send_command_string(c, 'JSON_Type')
        elif c == 'I':
            self.send_command_string(c, 'JSON_Type')
        elif c == 'F':
            self.send_command_string(c, 'JSON_Type')
        elif c == 't':
            self.send_command_string(c, 'LLC_Type')
        elif c == 'p':
            self.send_command_string(c, 'JSON_Type') # get probe variable value
        elif c == 'a':
            self.start_ota()
        elif c in 'P':                         # P -> change port
            self.change_port()
        elif c in 'sS':                         # S -> suspend / open port temporarily
            self.suspend_port()
        elif c in 'bB':                         # B -> change baudrate
            self.change_baudrate()
        elif c == '8':                          # 8 -> change to 8 bits
            self.serial.bytesize = serial.EIGHTBITS
            self.dump_port_settings()
        elif c == '7':                          # 7 -> change to 8 bits
            self.serial.bytesize = serial.SEVENBITS
            self.dump_port_settings()
        elif c in 'eE':                         # E -> change to even parity
            self.serial.parity = serial.PARITY_EVEN
            self.dump_port_settings()
        elif c in 'oO':                         # O -> change to odd parity
            self.serial.parity = serial.PARITY_ODD
            self.dump_port_settings()
        elif c in 'mM':                         # M -> change to mark parity
            self.serial.parity = serial.PARITY_MARK
            self.dump_port_settings()
        elif c in 'sS':                         # S -> change to space parity
            self.serial.parity = serial.PARITY_SPACE
            self.dump_port_settings()
        elif c in 'nN':                         # N -> change to no parity
            self.serial.parity = serial.PARITY_NONE
            self.dump_port_settings()
        elif c == '1':                          # 1 -> change to 1 stop bits
            self.serial.stopbits = serial.STOPBITS_ONE
            self.dump_port_settings()
        elif c == '2':                          # 2 -> change to 2 stop bits
            self.serial.stopbits = serial.STOPBITS_TWO
            self.dump_port_settings()
        elif c == '3':                          # 3 -> change to 1.5 stop bits
            self.serial.stopbits = serial.STOPBITS_ONE_POINT_FIVE
            self.dump_port_settings()
        elif c in 'xX':                         # X -> change software flow control
            self.serial.xonxoff = (c == 'X')
            self.dump_port_settings()
        elif c in 'rR':                         # R -> change hardware flow control
            self.serial.rtscts = (c == 'R')
            self.dump_port_settings()
        else:
            sys.stderr.write('--- unknown menu character {} --\n'.format(key_description(c)))

    def upload_file(self):
        """Ask user for filenname and send its contents"""
        sys.stderr.write('\n--- File to upload: ')
        sys.stderr.flush()
        with self.console:
            filename = sys.stdin.readline().rstrip('\r\n')
            if filename:
                try:
                    with open(filename, 'rb') as f:
                        sys.stderr.write('--- Sending file {} ---\n'.format(filename))
                        while True:
                            block = f.read(1024)
                            if not block:
                                break
                            self.serial.write(block)
                            # Wait for output buffer to drain.
                            self.serial.flush()
                            sys.stderr.write('.')   # Progress indicator.
                    sys.stderr.write('\n--- File {} sent ---\n'.format(filename))
                except IOError as e:
                    sys.stderr.write('--- ERROR opening file {}: {} ---\n'.format(filename, e))

    def change_filter(self):
        """change the i/o transformations"""
        sys.stderr.write('\n--- Available Filters:\n')
        sys.stderr.write('\n'.join(
            '---   {:<10} = {.__doc__}'.format(k, v)
            for k, v in sorted(TRANSFORMATIONS.items())))
        sys.stderr.write('\n--- Enter new filter name(s) [{}]: '.format(' '.join(self.filters)))
        with self.console:
            new_filters = sys.stdin.readline().lower().split()
        if new_filters:
            for f in new_filters:
                if f not in TRANSFORMATIONS:
                    sys.stderr.write('--- unknown filter: {!r}\n'.format(f))
                    break
            else:
                self.filters = new_filters
                self.update_transformations()
        sys.stderr.write('--- filters: {}\n'.format(' '.join(self.filters)))

    def change_encoding(self):
        """change encoding on the serial port"""
        sys.stderr.write('\n--- Enter new encoding name [{}]: '.format(self.input_encoding))
        with self.console:
            new_encoding = sys.stdin.readline().strip()
        if new_encoding:
            try:
                codecs.lookup(new_encoding)
            except LookupError:
                sys.stderr.write('--- invalid encoding name: {}\n'.format(new_encoding))
            else:
                self.set_rx_encoding(new_encoding)
                self.set_tx_encoding(new_encoding)
        sys.stderr.write('--- serial input encoding: {}\n'.format(self.input_encoding))
        sys.stderr.write('--- serial output encoding: {}\n'.format(self.output_encoding))

    def change_baudrate(self):
        """change the baudrate"""
        sys.stderr.write('\n--- Baudrate: ')
        sys.stderr.flush()
        with self.console:
            backup = self.serial.baudrate
            try:
                self.serial.baudrate = int(sys.stdin.readline().strip())
            except ValueError as e:
                sys.stderr.write('--- ERROR setting baudrate: {} ---\n'.format(e))
                self.serial.baudrate = backup
            else:
                self.dump_port_settings()

    def change_port(self):
        """Have a conversation with the user to change the serial port"""
        with self.console:
            try:
                port = ask_for_port()
            except KeyboardInterrupt:
                port = None
        if port and port != self.serial.port:
            # reader thread needs to be shut down
            self._stop_reader()
            self._stop_processor()
            # save settings
            settings = self.serial.getSettingsDict()
            try:
                new_serial = serial.serial_for_url(port, do_not_open=True)
                # restore settings and open
                new_serial.applySettingsDict(settings)
                new_serial.rts = self.serial.rts
                new_serial.dtr = self.serial.dtr
                new_serial.open()
                new_serial.break_condition = self.serial.break_condition
            except Exception as e:
                sys.stderr.write('--- ERROR opening new port: {} ---\n'.format(e))
                new_serial.close()
            else:
                self.serial.close()
                self.serial = new_serial
                sys.stderr.write('--- Port changed to: {} ---\n'.format(self.serial.port))
            # and restart the reader thread
            self._start_reader()
            self._start_processor()

    def suspend_port(self):
        """\
        open port temporarily, allow reconnect, exit and port change to get
        out of the loop
        """
        # reader thread needs to be shut down
        self._stop_reader()
        self.serial.close()
        sys.stderr.write('\n--- Port closed: {} ---\n'.format(self.serial.port))
        self._stop_processor()
        do_change_port = False
        while not self.serial.is_open:
            sys.stderr.write('--- Quit: {exit} | p: port change | any other key to reconnect ---\n'.format(
                exit=key_description(self.exit_character)))
            k = self.console.getkey()
            if k == self.exit_character:
                self.stop()             # exit app
                break
            elif k in 'pP':
                do_change_port = True
                break
            try:
                self.serial.open()
            except Exception as e:
                sys.stderr.write('--- ERROR opening port: {} ---\n'.format(e))
        if do_change_port:
            self.change_port()
        else:
            # and restart the reader thread
            self._start_reader()
            sys.stderr.write('--- Port opened: {} ---\n'.format(self.serial.port))
            self._start_processor()

    def get_help_text(self):
        """return the help text"""
        # help text, starts with blank line!
        return """
--- pySerial ({version}) - miniterm - help
---
--- {exit:8} Exit program
--- {menu:8} Menu escape key, followed by:
--- Menu keys:
---    {menu:7} Send the menu character itself to remote
---    {exit:7} Send the exit character itself to remote
---    {info:7} Show info
---    {upload:7} Upload file (prompt will be shown)
---    {repr:7} encoding
---    {filter:7} edit filters
--- Toggles:
---    {rts:7} RTS   {dtr:7} DTR   {brk:7} BREAK
---    {echo:7} echo  {eol:7} EOL
---
--- Port settings ({menu} followed by the following):
---    p          change port
---    7 8        set data bits
---    N E O S M  change parity (None, Even, Odd, Space, Mark)
---    1 2 3      set stop bits (1, 2, 1.5)
---    b          change baud rate
---    x X        disable/enable software flow control
---    r R        disable/enable hardware flow control
""".format(version=getattr(serial, 'VERSION', 'unknown version'),
           exit=key_description(self.exit_character),
           menu=key_description(self.menu_character),
           rts=key_description('\x12'),
           dtr=key_description('\x04'),
           brk=key_description('\x02'),
           echo=key_description('\x05'),
           info=key_description('\x09'),
           upload=key_description('\x15'),
           repr=key_description('\x01'),
           filter=key_description('\x06'),
           eol=key_description('\x0c'))


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# default args can be used to override when calling main() from an other script
# e.g to create a miniterm-my-device.py
def main(default_port=None, default_baudrate=115200, default_rts=None, default_dtr=None):
    """Command line tool, entry point"""

    import argparse

    parser = argparse.ArgumentParser(
        description='Miniterm - A simple terminal program for the serial port.')

    parser.add_argument(
        'port',
        nargs='?',
        help='serial port name ("-" to show port list)',
        default=default_port)

    parser.add_argument(
        'baudrate',
        nargs='?',
        type=int,
        help='set baud rate, default: %(default)s',
        default=default_baudrate)

    group = parser.add_argument_group('port settings')

    group.add_argument(
        '--parity',
        choices=['N', 'E', 'O', 'S', 'M'],
        type=lambda c: c.upper(),
        help='set parity, one of {N E O S M}, default: N',
        default='N')

    group.add_argument(
        '--rtscts',
        action='store_true',
        help='enable RTS/CTS flow control (default off)',
        default=False)

    group.add_argument(
        '--xonxoff',
        action='store_true',
        help='enable software flow control (default off)',
        default=False)

    group.add_argument(
        '--rts',
        type=int,
        help='set initial RTS line state (possible values: 0, 1)',
        default=default_rts)

    group.add_argument(
        '--dtr',
        type=int,
        help='set initial DTR line state (possible values: 0, 1)',
        default=default_dtr)

    group.add_argument(
        '--non-exclusive',
        dest='exclusive',
        action='store_false',
        help='disable locking for native ports',
        default=True)

    group.add_argument(
        '--ask',
        action='store_true',
        help='ask again for port when open fails',
        default=False)

    group = parser.add_argument_group('data handling')

    group.add_argument(
        '-e', '--echo',
        action='store_true',
        help='enable local echo (default off)',
        default=False)

    group.add_argument(
        '--encoding',
        dest='serial_port_encoding',
        metavar='CODEC',
        help='set the encoding for the serial port (e.g. hexlify, Latin1, UTF-8), default: %(default)s',
        default='UTF-8')

    group.add_argument(
        '-f', '--filter',
        action='append',
        metavar='NAME',
        help='add text transformation',
        default=[])

    group.add_argument(
        '--eol',
        choices=['CR', 'LF', 'CRLF'],
        type=lambda c: c.upper(),
        help='end of line mode',
        default='CRLF')

    group.add_argument(
        '--raw',
        action='store_true',
        help='Do no apply any encodings/transformations',
        default=False)

    group = parser.add_argument_group('hotkeys')

    group.add_argument(
        '--exit-char',
        type=int,
        metavar='NUM',
        help='Unicode of special character that is used to exit the application, default: %(default)s',
        default=0x1d)  # GS/CTRL+]

    group.add_argument(
        '--menu-char',
        type=int,
        metavar='NUM',
        help='Unicode code of special character that is used to control miniterm (menu), default: %(default)s',
        default=0x13)  # Menu: CTRL+T, changed to CTRL+S by bwu

    group = parser.add_argument_group('diagnostics')

    group.add_argument(
        '-q', '--quiet',
        action='store_true',
        help='suppress non-error messages',
        default=False)

    group.add_argument(
        '--develop',
        action='store_true',
        help='show Python traceback on error',
        default=False)

    group = parser.add_argument_group('flash_config')

    group.add_argument(
        '-b', '--bin_file',
        help='specifiy K210 F/W binary file',
        default='PULD.bin')

    group.add_argument(
        '-m', '--mcu',
        type=int,
        help='specifiy MCU to program (possible values: 0, 1 & 2)',
        default=0)
    group.add_argument(
        '-c', '--cipher',
        type=int,
        help='specifiy bin file is encrypted or not (possible values: 0, 1)',
        default=0)
        
    group.add_argument(
        '-v', '--version',        
        help='specifiy new bin file version',
        default='V0.0.0')
        
    

    args = parser.parse_args()

    print('Programming MCU={} on port={} with bin_file = {} with version {} and cipher = {}'.format(args.mcu, args.port, args.bin_file, args.version, args.cipher))

    if args.menu_char == args.exit_char:
        parser.error('--exit-char can not be the same as --menu-char')

    if args.filter:
        if 'help' in args.filter:
            sys.stderr.write('Available filters:\n')
            sys.stderr.write('\n'.join(
                '{:<10} = {.__doc__}'.format(k, v)
                for k, v in sorted(TRANSFORMATIONS.items())))
            sys.stderr.write('\n')
            sys.exit(1)
        filters = args.filter
    else:
        filters = ['default']

    while True:
        # no port given on command line -> ask user now
        if args.port is None or args.port == '-':
            try:
                args.port = ask_for_port()
            except KeyboardInterrupt:
                sys.stderr.write('\n')
                parser.error('user aborted and port is not given')
            else:
                if not args.port:
                    parser.error('port is not given')
        else:
            args.port = search_for_port(args.port)
  
        try:
            serial_instance = serial.serial_for_url(
                args.port,
                args.baudrate,
                parity=args.parity,
                rtscts=args.rtscts,
                xonxoff=args.xonxoff,
                do_not_open=True)

            if not hasattr(serial_instance, 'cancel_read'):
                # enable timeout for alive flag polling if cancel_read is not available
                serial_instance.timeout = 1

            if args.dtr is not None:
                if not args.quiet:
                    sys.stderr.write('--- forcing DTR {}\n'.format('active' if args.dtr else 'inactive'))
                serial_instance.dtr = args.dtr
            if args.rts is not None:
                if not args.quiet:
                    sys.stderr.write('--- forcing RTS {}\n'.format('active' if args.rts else 'inactive'))
                serial_instance.rts = args.rts

            if isinstance(serial_instance, serial.Serial):
                serial_instance.exclusive = args.exclusive

            serial_instance.open()
        except serial.SerialException as e:
            sys.stderr.write('could not open port {!r}: {}\n'.format(args.port, e))
            if args.develop:
                raise
            if not args.ask:
                sys.exit(1)
            else:
                args.port = '-'
        else:
            break

    miniterm = Miniterm(
        serial_instance,
        echo=args.echo,
        eol=args.eol.lower(),
        filters=filters,
        bin_file=args.bin_file,
        mcu=args.mcu, 
        cipher=args.cipher,
        version=args.version)
    miniterm.exit_character = unichr(args.exit_char)
    miniterm.menu_character = unichr(args.menu_char)
    miniterm.raw = args.raw
    miniterm.set_rx_encoding(args.serial_port_encoding)
    miniterm.set_tx_encoding(args.serial_port_encoding)

    if not args.quiet:
        sys.stderr.write('--- Miniterm on {p.name}  {p.baudrate},{p.bytesize},{p.parity},{p.stopbits} ---\n'.format(
            p=miniterm.serial))
        sys.stderr.write('--- Quit: {} | Menu: {} | Help: {} followed by {} ---\n'.format(
            key_description(miniterm.exit_character),
            key_description(miniterm.menu_character),
            key_description(miniterm.menu_character),
            key_description('\x08')))
        #print('Enter Ctrl+S and \'a\' to show the OTA option...')
    #sys.stderr.write('Enter Ctrl+S and \'t\' to start OTA programming...\r\n')

    miniterm.start()
    time.sleep(1.5)
    miniterm.send_command_string('t', 'LLC_Type')
    try:
        miniterm.join(True)
    except KeyboardInterrupt:
        pass
    if not args.quiet:
        sys.stderr.write('\n--- exit ---\n')
    miniterm.join()
    miniterm.close()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
if __name__ == '__main__':
    main()

