//#pragma once
#ifndef _MCU_ADC_
#define _MCU_ADC_
#include <stdlib.h>



void mcu_adc_init( void );

float get_battery_voltage(float scale_val);
float get_battery_current(float scale_val);

#endif
