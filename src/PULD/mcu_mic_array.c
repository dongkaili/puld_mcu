#include "mcu_common_define.h"
#include "mcu_comm_protocol.h"
#if !FACTORY_RESET
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>
#include <ctype.h>

#include "sleep.h"
#include "sysctl.h"
#include "fpioa.h"
#include "apu_init.h"
#include "apu.h"
#include "mcu_mic_array.h"
#include "i2s.h"
#include "mcu_fft.h"
#include "mcu_stdio.h"
#include "spi_master.h"
#include "spi_slave.h"

//#define __dbgopt__ __attribute__((optimize(0)))
#define __dbgopt__

#define STABLE_ANGLE_DEFAULT 0.15               
#define __dbgopt__
//#define __dbgopt__ __attribute__((optimize(0)))
#define DISCARDED_DIR_BUFF_SAMPLES 150

#define IN_FOV_ANGLE_LVL     40
#define ON_TARGET_ANGLE_LVL  20
#define ANGLE_HYSTERESIS     5  // Determines band in which the IN_FOV and ON_TARGET states are to remain stable. Range is {*_Angle_LVL} ±{Angle_Hysteresis}/2


#define AZM_INC_BF_DUTY_CYCLE 2                 // duty cycle for azimuthal & incidental beamformer execution... min==2 means alternating, 3 means 2@AZM + 1@INC

#define MIN_MIC_DB        -95.0
#define NF_ADJUST           0.0
#define DBFS_UE_ADJ_ADD     8.0
#define DBFS_UE_ADJ_MULT    1.6
#define MINUS_INF        -999.9


static char* BF_STATE_str[] = { 
    "IDLE     ",    
    "HUNTING  ",  
    "HUNTING  ", 
    "HUNTING  ",
    "IN_VIEW  ",   
    "ON_TARGET",
};


#ifdef DEBUG
static const char* BF_STATE_col[] = { 
    KNRM,
    KYEL,  
    KBLU, 
    KMAG,
    KGRN,   
    KRED,
};
#endif


#define DBFS_UE_ADJ 1
#define IN_FOV_TEST 0

#define BLOCK_DATA_JSON_FORMAT  1

extern void send_raw_bytes(uart_device_number_t index, uint8_t* bytes, uint32_t byte_size, _data_header_s head_data);
extern void send_string(uart_device_number_t index, char* str, _data_header_s head_data);
extern void swap_bytes_in_double_words(void* addr, uint32_t len);

float Noise_Floor = MIN_MIC_DB + NF_ADJUST;   // initially set to lowest sensitivity of mics + any necessary adjustment 
bool DBFS_UE_Adj = DBFS_UE_ADJ; // host modifiable flag to enable/disable the adjustment to DBFS to make it align with the UE System detector
bool Enable_DUT_Test = false;
bool Enable_All_ANGLE = false;
bool Enable_All_DBFS = false;
bool Enable_AZM_INC_Data = true;
bool Angle_BF_INC_auto_EN = true;
bool Angle_BF_AZM_auto_EN = true;
bool Debug_AZM_BF = true;
bool Assign_INC_to_DUT = true;
bool Select_Angle_DUT1 = true; // Select from either of 2 internal version of angle for comparisson
bool Select_DBFS_DUT1 = false;  // Select from either of 2 internal version of dbfs for comparisson : false currently uses SUM_DIR_SUM_Vect_MAX version for DBFS rather than linked... seems better
bool Force_IDLE = false;

float NF_Adjust  = NF_ADJUST;   
    
uint16_t BP_LOW_N  = 0;
uint16_t BP_HIGH_N = (APU_DIR_CHANNEL_SIZE / 2) - 1;  // 1/2 # of bins as points in FFT 

uint16_t I2S_Rx_Buff_Depth = APU_DIR_CHANNEL_SIZE; // max size of direct read I2S data that may be used for analysis (+ 128 for garbage trimming)
   
bool BF_FSM_STATE_CHANGE = false;

bool _8_Min_Detect  = false;
bool _7_AZM_Angle_Stable  = false;
bool _6_AZM_Angle_Rot_Pos     = false;
bool _5_AZM_Angle_Rot_Neg     = false;
bool _4_INC_Angle_Stable  = false;
bool _3_INC_Angle_Rot_Pos     = false;
bool _2_INC_Angle_Rot_Neg     = false;
bool _1_IN_FOV	 	= false;
bool _0_ON_TARGET	= false;

bool IN_FOV_s[5] = { false };
bool ON_TARGET_s[5] = { false };

uint16_t BF_FSM_STATE_CHANGE_COUNT = 0;
float Angle, Angle_BF_INC_auto, Angle_BF_AZM_auto, Angle_MPA; 
float Angle_DUT1, Angle_DUT2, Angle_TEST;     // Alternatives for DUT assignment

float IN_FOV_Angle_LVL = IN_FOV_ANGLE_LVL;  
float ON_TARGET_Angle_LVL = ON_TARGET_ANGLE_LVL;
float Angle_Hysteresis = ANGLE_HYSTERESIS;   


double VRMS_BP;

BF_STATE BF_FSM_STATE[3], BF_FSM_STATE_s = S_BF_IDLE;
char bf_state_str_trim[30];
float DIR_Sens = STABLE_ANGLE_DEFAULT;

bool AGC_Enable = true;
uint16_t AGC_Adjust_Counter = 0;
uint16_t AGC_Adjust_Rate = AGC_ADJUST_RATE;

// 1st 4 items could be 4-bits each... but why bother
int8_t ID_Max;                                     // index of entry with largest dir_sum value
int8_t ID_First;                                   // First entry in linked list of entries on "bell curve" (may be at end of chart and wrap 15-0)
int8_t ID_Last;                                    // last entry in linked list of entries on bell curve
// int8_t N_linked;                                // # of linked, consecutive items (data quality measure) = ((ID_Last - ID_First + 1 ) + 16) % 16;
uint32_t AVG_dir_sum;                              // avg of dir_sum values in bell curve linked items = Sum(dir_sum[i])/N_linked;
uint32_t StdDev_dir_sum;                           // error of dir_sum values in bell curve linked list... use to detect when src is close to dead-center

CH_Data_s CH_Data[APU_DIR_CHANNEL_MAX];            // No need to put this in struct... barchart will display data for only the selected BF'r 

beamformer_s BF[N_BF] = { { .id = AZM, .name = "Azimuth"}, { .id = INC, .name = "Inclination"} };
BF_enum active_BF; 

float VRMS_MPA = 0.0;                             // RMS of MPA energy in signal as determined by weighted average of linked results in each scan... result is always <actual so not best
float VRMS_TEST = 0.0;                             // RMS of MPA energy in signal as determined by max level detected during each scan... should provide better result than other form
float DBFS = MINUS_INF;
float DBFS_MPA = MINUS_INF;                        // DB full scale of VRMS = 20*log10(VRMS_MPA/2^15)
float DBFS_DUT1 = MINUS_INF;                       // DUT option #1
float DBFS_DUT2 = MINUS_INF;                        // DUT option #1
float DBFS_TEST = MINUS_INF;                        // DUT option #2
float DBFS_CORR = MINUS_INF; 
float DBFS_BP = MINUS_INF;                         // DB full scale of VRMS_BP (FFT output in the range BP_Low_Freq-BP_High_Freq) = 20*log10(VRMS_MPA/2^15)
uint16_t APU_Gain_u;                               // gain value programmed into APU register
float APU_Gain;                                    // decimal equivalent of fixed point register

uint32_t dir_sum_max = 0;
static uint32_t dir_buffer_overflow_count = 0;     // Counter for individual DIR buffer result entries to detect when APU_DIR_BUFFER[][] == 0x00007FFF || 0xFFFF8000  
static uint32_t dir_buffer_near_overflow_count = 0;// something to indicate when we're within 10% of overflow in buffer values so we can proactively adjust APU_Gain
bool ClrScr_Done = false;
bool Enable_Host_Reporting = START_WITH_HOST_REPORTING;
bool Enable_Barchart = false;
bool Enable_Beamformer_Stats = false;
bool Enable_DIR_Buffer_Table = false;
bool Enable_DIR_Results_Table = false;
bool Enable_I2S_Data_Table = false;
bool Enable_I2S_Data_Capture = false; 
bool Enable_I2S_Hard_FFT = false;
bool Enable_I2S_Soft_FFT = false;
bool Enable_VOC_FFT = false;

bool Enable_I2S_Hard_FFT_Table = false;
bool Enable_i2s_receive_data_all = false;
bool Enable_i2s_rx_buff_padding = false;
bool Reset_i2s_rx_fifo_before_use = true;
bool Enable_I2S_Soft_FFT_Table = false;

//bool Wait_for_Letter_C = false;
bool Enable_BF_FSM_Debug = false;

int16_t Trial_ID, Trial_Num = -1;                                   // Counters to group data for related test samples.... preincremented so init to -1 so 1st trial starts at 0
int16_t Mic_Data[8][APU_DIR_CHANNEL_SIZE] = {0};                    // holds the Isolated mic data between APU runs
                                                                    
int16_t I2s_Data[8][APU_DIR_CHANNEL_SIZE] = {0};                    // hold the i2s data

int16_t I2s_Soft_FFT_Power_Data[8][APU_DIR_CHANNEL_SIZE / 2] = {0}; // hold the soft fft power of i2s data

int16_t I2s_Hard_FFT_Power_Data[8][APU_DIR_CHANNEL_SIZE / 2] = {0}; // hold the hard fft power of i2s data

int16_t VOC_FFT_Data[APU_DIR_CHANNEL_SIZE / 2] = {0};               // hold the FFT of VOC data

void * piData = NULL;

double complex AVG_Linked_DIR_SUM_Vect;                             // determines weighted average of linked DIR vectors

//// define a single record entry in Unit_DIR_Vectors array
//typedef struct Unit_DIR_Vector_
//{
//    uint8_t DIR : 4;
//    float angle;
//    double complex UDV;
//} Unit_DIR_Vector_s;
//Unit_DIR_Vector_s Unit_DIR_Vector_AZM_s[16] = { 0 }; // defined over range 0 to ±180°
//Unit_DIR_Vector_s Unit_DIR_Vector_INC_s[16] = { 0 }; // defined over range -90 to +90 deg where DIR==0 is leading side of array in plane of array (+90 deg) and DIR==8 is directly above array (0 deg) 
//
double complex Unit_DIR_Vectors_AZM[16] = { 
    // Unit vectors in complex plane for each DIRection 
   + 1.0000000000 + 0.0000000000 * I,    // DIR[0]  : +X axis of coordinate system (0°) 
   + 0.9238795325 + 0.3826834324 * I,   // DIR[1] 
   + 0.7071067812 + 0.7071067812 * I,   // DIR[2] 
   + 0.3826834324 + 0.9238795325 * I,   // DIR[3] 
   + 0.0000000000 + 1.0000000000 * I,   // DIR[4]  : +Y axis of coordinate system (90°) 
   - 0.3826834324 + 0.9238795325 * I,   // DIR[5] 
   - 0.7071067812 + 0.7071067812 * I,   // DIR[6] 
   - 0.9238795325 + 0.3826834324 * I,   // DIR[7] 
   - 1.0000000000 + 0.0000000000 * I,   // DIR[8]  : -X axis of coordinate system (±180°) 
   - 0.9238795325 - 0.3826834324 * I,   // DIR[9] 
   - 0.7071067812 - 0.7071067812 * I,   // DIR[10] 
   - 0.3826834324 - 0.9238795325 * I,   // DIR[11] 
   - 0.0000000000 - 1.0000000000 * I,   // DIR[12] : -Y axis of coordinate system (-90°) 
   + 0.3826834324 - 0.9238795325 * I,   // DIR[13] 
   + 0.7071067812 - 0.7071067812 * I,   // DIR[14] 
   + 0.9238795325 - 0.3826834324 * I,   // DIR[15] 
};                                      // DIR[16] : +X axis of coordinate system (0°)... for fractional DIR values <16 

double complex Unit_DIR_Vectors_INC[16] = { 
    // Unit vectors in complex plane for each DIRection 
   + 0.0000000000 + 1.0000000000 * I,    // DIR[0]  : device center side of array (+90 deg) 
   + 0.1950903220 + 0.9807852804 * I,    // DIR[1] 
   + 0.3826834324 + 0.9238795325 * I,    // DIR[2] 
   + 0.5555702330 + 0.8314696123 * I,    // DIR[3] 
   + 0.7071067812 + 0.7071067812 * I,    // DIR[4]  : (+45deg) 
   + 0.8314696123 + 0.5555702330 * I,    // DIR[5] 
   + 0.9238795325 + 0.3826834324 * I,    // DIR[6] 
   + 0.9807852804 + 0.1950903220 * I,    // DIR[7] 
   + 1.0000000000 + 0.0000000000 * I,    // DIR[8]  : directly above array (0 deg) 
   + 0.9807852804 - 0.1950903220 * I,    // DIR[9]   
   + 0.9238795325 - 0.3826834324 * I,    // DIR[10] 
   + 0.8314696123 - 0.5555702330 * I,    // DIR[11] 
   + 0.7071067812 - 0.7071067812 * I,    // DIR[12] : (-45deg) 
   + 0.5555702330 - 0.8314696123 * I,    // DIR[13] 
   + 0.3826834324 - 0.9238795325 * I,    // DIR[14] 
   + 0.1950903220 - 0.9807852804 * I,    // DIR[15]  
};                                       // DIR[16]  : device outer edge side of array (-90 deg)... for fractional DIR values <16 
 
double complex Unit_DIR_Vectors_AZM_Real_World[16];          // Uniquely calculated for each MCU
complex double Azimuth_Local_to_Real_World_Rotation_Vector();


uint8_t MPA_N = MPA_NMAX_INITIAL;  // Number of elements in ring buffer.
bool MPA_N_changed = true; // flag so we can flush MPA accumulators when MPA_N is changed by the host... not needed if we revert to original full loop method 

uint8_t Host_Update_Interval = HOST_UPDATE_INTERVAL;     // Update host after this many DIR tests are completed to allow more processing time for averaging/smoothing or MCU traffic balancing
uint8_t Host_Update_Counter = 0;                         // reverse counter... when == Host_Update_Interval send data to host

bool dir_buffer_near_overflow_detected = false;
    
bool avg_ssq_dir_sum_max_exceeds_uint32;
uint8_t AZM_INC_BF_Duty_Cycle = AZM_INC_BF_DUTY_CYCLE;
uint8_t AZM_INC_BF_Duty_Cycle_counter = 0;

_all_bf_data_s all_bf_data;
typedef struct _mcu_mic_context_
{
    mic_callback callback;
    _all_bf_data_s data;  // hoping this copies whole array
} _mcu_bf_context_s;

static uint64_t t_us_start, t_us_current;

static uint32_t loop_time = 0;

static _mcu_bf_context_s s_context = {};




extern uint8_t mcu1_bf_data_ready_flag;
extern uint8_t mcu2_bf_data_ready_flag;
extern uint64_t mcu1_bf_data_ready_count;
extern uint64_t mcu2_bf_data_ready_count;
static slave_mcu_data_s slave_mcu_data_tmp[2];          // temporary data struct holder for 2 slaves on the MCU0
static slave_mcu_data_s slave_mcu_data[2];              // data struct holder for 2 slaves on the MCU0

extern slave_mcu_data_s slave_data_tmp;                 // slave mcu data on slave MCUs

extern uint32_t slave1_data_addr;
extern uint32_t slave2_data_addr;
uint64_t mcu1_bf_data_read_error_count = 0;
uint64_t mcu2_bf_data_read_error_count = 0;
uint64_t mcu1_bf_data_read_count = 0;
uint64_t mcu2_bf_data_read_count = 0;
uint8_t enable_mcu1_bf_data_read = 1;
uint8_t enable_mcu2_bf_data_read = 1;

#define BF_DATA_READ_RATE    1
#define BF_DATA_READ_THRESHOLD   50                      // the first number of BF ready data that won't not read

#define MIN_LEVEL_THRESHHOLD 0x00000000                  // noise floor... if dir_max < this then zero display... may be adjusted
#define MIN_DETECT_COUNTER_THRESHHOLD 0

#define I2S_DMA 0
#define FRAME_LEN APU_DIR_CHANNEL_SIZE

#if (I2S_DMA)
uint32_t i2s_rx_buf[4][FRAME_LEN * 2];
#else
uint64_t rx_buf[4][APU_DIR_CHANNEL_SIZE + 128];          // max buffer depth
#endif

/////////
// https://solarianprogrammer.com/2019/04/08/c-programming-ansi-escape-codes-windows-macos-linux-terminals/

#ifdef DEBUG
// Move cursor to home position
void GoHome(void)
{
    moveTo(0, 0);
}

// Clear the terminal screen
void ClrScr(void)
{
  usleep(1000);
  usleep(1000);
  mcu_stdio_printf("\x1b[2J");
  moveTo(0, 0);
  usleep(1000);
  usleep(1000);
}

void moveTo(int row, int col)
{
    char str[16];
    sprintf(str, "\x1b[%d;%df", row, col);
    mcu_stdio_printf(str);
}


void Pause(char *str)
{
  mcu_stdio_printf("%s \n\rPress ENTER key to Continue... \n\r", str);
  mcu_stdio_getc();
};
#endif

char *ltrim(char *s)
{
    while (isspace(*s)) s++;
    return s;
}

char *rtrim(char *s)
{
    char* back = s + strlen(s);
    while (isspace(* --back)) ;
    *(back + 1) = '\0';
    return s;
}

char *trim(char *s)
{
    return rtrim(ltrim(s)); 
}

/////////
//int dir_logic(void) __attribute__((optimize(0)));

// Ex: https://stackoverflow.com/questions/6105513/need-help-using-qsort-with-an-array-of-structs
int dir_sum_compare(const void *a, const void *b)
{
  CH_Data_s *CH_DataA = (CH_Data_s *)a;
  CH_Data_s *CH_DataB = (CH_Data_s *)b;
  return (CH_DataB->dir_sum - CH_DataA->dir_sum);
}

int CH_ID_compare(const void *b, const void *a)
{
  CH_Data_s *CH_DataA = (CH_Data_s *)a;
  CH_Data_s *CH_DataB = (CH_Data_s *)b;
  return (CH_DataB->CH - CH_DataA->CH);
}

#ifdef DEBUG

void Display_Beamformer_header(void* Pself)
{
    beamformer_s *self = (beamformer_s*)(Pself); 
    char str[256] = "";

    if(!ClrScr_Done)
    {
        ClrScr();
        ClrScr_Done = true;
    }
    Cursor(0);  // need this just in case we reset terminal. Function is called during init and when exiting Terminal Mode

    sprintf(str, "MCU[%d] Beamformer Information [%s]", mcu_id, get_firmware_version_string());
    mcu_stdio_printf("%-64s\n\r", str);

    sprintf(str, "MODE: %s ", (char *)self->name);
    mcu_stdio_printf("%-64s\n\r", str);
     
    char DelayArrayStr[1024] = ""; 
    memset(DelayArrayStr, 0, sizeof(DelayArrayStr)); 
    apu_get_delay_str(BF[self->id].delays, DelayArrayStr); 
    printf("Delays: %s\n\n", DelayArrayStr); 
}

void Display_Barchart() 
{ 
    for (size_t ch = 0; ch < APU_DIR_CHANNEL_MAX; ch++)
    { // for each direction...
        if(Terminal_Mode == true) // user wants terminal back
            return;
        mcu_stdio_printf("%3d |", (int)ch);
      
        if (ch == ID_Max) // set color green for dominant channel
            mcu_stdio_printf(KGRN);
        else if (ID_First <= ID_Last)
        {
            if ((ch >= ID_First) && (ch <= ID_Last))
                mcu_stdio_printf(KYEL);
            else
                mcu_stdio_printf(KRED);
        }
        else if (ID_First > ID_Last)
        {
            if ((ch >= ID_First) || (ch <= ID_Last))
                mcu_stdio_printf(KYEL);
            else
                mcu_stdio_printf(KRED);
        };

        // print asterisks to indicate magnitude of cross-correlation result in that direction
        // This uses printf directly with args to print the correct # of '*" and ' ' characters to complete a line.
        uint8_t Bar_Len;
        if (dir_sum_max == 0)
            Bar_Len = 0;
        else
            Bar_Len = (float)CH_Data[ch].dir_sum / (float)dir_sum_max * 64.0;
        mcu_stdio_printf(
          "%.*s%.*s%s\r\n",
            Bar_Len,
            "****************************************************************",
            64 - Bar_Len,
            "                                                                ",
            KNRM);
    }
    

    mcu_stdio_printf("\n\r");
} 


void Display_Beamformer_Stats(void* Pself) 
{ 
    beamformer_s *self = (beamformer_s*)(Pself); 

    char str[256] = "";
    
    sprintf(str, "BF_FSM_STATE = %s%-10s%s, Test_State = %d", BF_STATE_col[BF_FSM_STATE[mcu_id]], BF_STATE_str[BF_FSM_STATE[mcu_id]], KNRM, TEST_State);
    mcu_stdio_printf("%-64s\n\r", str);
        
    //sprintf(str, "Min Sig Strength Detected = %s%s%s", _8_Min_Detect ? KGRN : KRED, _8_Min_Detect ? "True" : "False", KNRM);
    sprintf(str, "Min Sig Strength Detected = %s", TF_string(_8_Min_Detect));
    mcu_stdio_printf("%-64s\n\r", str);
    
    sprintf(str, "DIR Max strength = %d @ %d", dir_sum_max, ID_Max);
    mcu_stdio_printf("%-64s\n\r", str);
    
    sprintf(str, "Beamformer Angles  (local):  BF[INC].angle_local = %6.1f,  BF[AZM].angle_local = %6.1f", BF[INC].angle_local, BF[AZM].angle_local);
    mcu_stdio_printf("%-76s\n\r", str);

    sprintf(str, "Beamformer Inputs (static): %sAngle_BF_INC_static = %6.1f, %sAngle_BF_AZM_static = %6.1f", (Angle_BF_INC_auto_EN ? " " : "*"), Angle_BF_INC_static, (Angle_BF_AZM_auto_EN ? " " : "*"), Angle_BF_AZM_static);
    mcu_stdio_printf("%-76s\n\r", str);

    sprintf(str, "Beamformer Inputs   (auto):   %sAngle_BF_INC_auto = %6.1f, %sAngle_BF_AZM_auto   = %6.1f", (Angle_BF_INC_auto_EN ? "*" : " "), Angle_BF_INC_auto, (Angle_BF_AZM_auto_EN ? "*" : " "), Angle_BF_AZM_auto);
    mcu_stdio_printf("%-76s\n\r", str);
    
    sprintf(str, "                        [   MCU0    MCU1    MCU2 ]");
    mcu_stdio_printf("%-84s\n\r", str);

    sprintf(str, "Real World: Angle_AZM = [ %6.1f  %6.1f  %6.1f ]", all_bf_data.azm.angle[0], all_bf_data.azm.angle[1], all_bf_data.azm.angle[2]);
    mcu_stdio_printf("%-84s\n\r", str);

    sprintf(str, "Real World: Angle_INC = [ %6.1f  %6.1f  %6.1f ]", all_bf_data.inc.angle[0], all_bf_data.inc.angle[1], all_bf_data.inc.angle[2]);
    mcu_stdio_printf("%-64s\n\r", str);

    sprintf(str, "            Angle_DUT = [ %6.1f  %6.1f  %6.1f ]", all_bf_data.dut.angle[0], all_bf_data.dut.angle[1], all_bf_data.dut.angle[2]);
    mcu_stdio_printf("%-64s\n\r", str);

    double fAZM;
    double fINC;
    
    fAZM = cimag(BF[AZM].UVL.V);
    fINC = cimag(BF[INC].UVL.V);
    sprintf(str, "LNK [UVL.AZM:% 7.5f %s %7.5fi, UVL.INC:% 7.5f %s %7.5fi], [ROT.L.AZM:% 6.4f, L.INC:% 6.4f], [DISP.L.AZM:% 8.5f, L.INC:% 8.5f]", creal(BF[AZM].UVL.V), fAZM >= 0 ? "+" : "-", fabs(fAZM), creal(BF[INC].UVL.V), fINC >= 0 ? "+" : "-", fabs(fINC), BF[AZM].UVL.rotation, BF[INC].UVL.rotation, BF[AZM].UVL.dispersion, BF[INC].UVL.dispersion);
    mcu_stdio_printf("%-64s\n\r", str);

//    fAZM = cimag(BF[AZM].UVM.V);
//    fINC = cimag(BF[INC].UVM.V);
//    sprintf(str, "MAX [UVM.AZM:% 7.5f %s %7.5fi, UVM.INC:% 7.5f %s %7.5fi], [ROT.M.AZM:% 6.4f, M.INC:% 6.4f], [DISP.M.AZM:% 8.5f, M.INC:% 8.5f]", creal(BF[AZM].UVM.V), fAZM >= 0 ? "+" : "-", fabs(fAZM), creal(BF[INC].UVM.V), fINC >= 0 ? "+" : "-", fabs(fINC), BF[AZM].UVM.rotation, BF[INC].UVM.rotation, BF[AZM].UVM.dispersion, BF[INC].UVM.dispersion);
//    mcu_stdio_printf("%-64s\n\r", str);

    sprintf(str, "_1_IN_FOV    [%4.1f +- %4.1f] = %s", (IN_FOV_Angle_LVL / 2), (Angle_Hysteresis / 2), TF_string(_1_IN_FOV));
    mcu_stdio_printf("%-64s\n\r", str);

    sprintf(str, "_0_ON_TARGET [%4.1f +- %4.1f] = %s", (ON_TARGET_Angle_LVL / 2), (Angle_Hysteresis / 2), TF_string(_0_ON_TARGET));
    mcu_stdio_printf("%-64s\n\r", str);
    
    sprintf(str, "           [ Stable Rot_Pos Rot_Neg ]");
    mcu_stdio_printf("%-64s\n\r", str);
    
    sprintf(str, "AZM.Angle: [  %s  %s   %s  ]", TF_string(BF[AZM].is_Stable), TF_string(BF[AZM].is_Rot_Pos), TF_string(BF[AZM].is_Rot_Neg));
    mcu_stdio_printf("%-64s\n\r", str);
    
    sprintf(str, "INC.Angle: [  %s  %s   %s  ]", TF_string(BF[INC].is_Stable), TF_string(BF[INC].is_Rot_Pos), TF_string(BF[INC].is_Rot_Neg));
    mcu_stdio_printf("%-64s\n\r", str);

    if (Enable_I2S_Hard_FFT || Enable_I2S_Soft_FFT)
    {  
        sprintf(str, "VRMS_BP      = %.2f", VRMS_BP); 
        mcu_stdio_printf("%-64s\n\r", str);
    }
  
    sprintf(str, "MPA_N = %d, BF[self->id].MPA_N_Current = %d, self->MPA_Index = %d    ", MPA_N, BF[self->id].MPA_N_Current, self->MPA_Index);
    mcu_stdio_printf("%-64s\n\r", str);
        
    sprintf(str, "DBFS_MPA[%2d] = %s% 7.2f%s %s %.2f", MPA_N, DBFS_MPA > Noise_Floor ? KGRN : KRED, DBFS_MPA, KNRM, DBFS_MPA > Noise_Floor ? ">" : "<", Noise_Floor);
    mcu_stdio_printf("%-64s\n\r", str);

    sprintf(str, "DBFS_CORR    = %s% 7.2f%s %s %.2f", DBFS_CORR > Noise_Floor ? KGRN : KRED, DBFS_CORR, KNRM, DBFS_CORR > Noise_Floor ? ">" : "<", Noise_Floor);
    mcu_stdio_printf("%-64s\n\r", str);

    sprintf(str, "DBFS         = %s% 7.2f%s %s %.2f", DBFS > Noise_Floor ? KGRN : KRED, DBFS, KNRM, DBFS > Noise_Floor ? ">" : "<", Noise_Floor);
    mcu_stdio_printf("%-64s\n\r", str);

    if (Enable_I2S_Hard_FFT || Enable_I2S_Soft_FFT)
    {  
        sprintf(str, "DBFS_BP      = %s%7.2f%s", DBFS_BP > Noise_Floor ? KGRN : KRED, DBFS_BP, KNRM);
        mcu_stdio_printf("%-64s\n\r", str);
    }
    
    // Query the I2S0_BF_SAT_CFG register to see if we're saturated... this will likely never be an issue with our application

    // https://en.wikipedia.org/wiki/Q_(number_format)
    // https://people.ece.cornell.edu/land/courses/ece4760/PIC32/index_fixed_point.html
    sprintf(str, "I2S_FS: = %dHz, BP Low Freq [%d] = %d kHz, BP High Freq [%d] = %d kHz", I2S_FS_actual, BP_LOW_N, apu_fir_coefficients_options[i2s_fs_sel_current].bp_low, BP_HIGH_N, apu_fir_coefficients_options[i2s_fs_sel_current].bp_high); 
    mcu_stdio_printf("%-64s\n\r", str);
  
    sprintf(str, "Current gain =  %11.9f, APU VOC Saturation Count = %d, APU VOC Saturation Sample Count = %d", APU_Gain, apu_voc_get_saturation_count(), apu_voc_get_saturation_sample_count());
    mcu_stdio_printf("%-64s\n\r", str);
   
    sprintf(str, "Loop time = %d \xB5s = %.2f s", loop_time, (float)loop_time / 1000000);
    mcu_stdio_printf("%-64s\n\r", str);

    sprintf(str, "Loop speed = %.1f Hz", (1.0 / ((float)loop_time / 1000000)));
    mcu_stdio_printf("%-64s\n\r", str);

    sprintf(str, "APU_DIR_BUFFER range limit hit %d times.", dir_buffer_overflow_count);
    mcu_stdio_printf("%-64s\n\r", str);
    
    sprintf(str, "APU_DIR_BUFFER range limit nearly hit %d times.", dir_buffer_near_overflow_count);
    mcu_stdio_printf("%-64s\n\r", str);
 
}

void Display_Table_Header(char Heading[], uint8_t num_cols)
{
    mcu_stdio_printf("%s Data Table for MCU[%d]\n\r", Heading, mcu_id);
    mcu_stdio_printf("ID\t#\t");
    for (size_t ch = 0; ch < num_cols; ch++)
    {
        if (Terminal_Mode == true) // user wants terminal back
            return;
        mcu_stdio_printf("%s[%d]\t", Heading, (int)ch);
    }
    mcu_stdio_printf("\n\r");
}

void Display_DIR_Results_Table()
{
    if (Trial_Num == DIR_DATA_TABLE_SAMPLE_SIZE) // finished current series of tests so prep for next
    {
        ++Trial_ID;
        Trial_Num = 0;
        //Wait_for_Letter_C = true;
        TEST_State = TEST_WAIT;
        return;
    }
    else if (Trial_ID == -1) // 1st time in
    {
        ++Trial_ID;
        Display_Table_Header("DIR", APU_DIR_CHANNEL_MAX);
    }
    else
    {
        ++Trial_Num;
    }

    mcu_stdio_printf("%d\t%d", Trial_ID, Trial_Num);  // prepend line with common id for subset of tests

    for (size_t ch = 0; ch < APU_DIR_CHANNEL_MAX; ch++)
    {
        if (Terminal_Mode == true) // user wants terminal back
        {
            return;
        }
        #if(0)
        mcu_stdio_printf("\t%8x", CH_Data[ch].dir_sum);
        #else
        mcu_stdio_printf("\t%d", CH_Data[ch].dir_sum);
        #endif
    }
    mcu_stdio_printf("\n\r");
    if (Trial_Num == DIR_DATA_TABLE_SAMPLE_SIZE)
        mcu_stdio_printf("##TRIAL DONE##\n");
}

void Display_DIR_Buffer_Table()
    {
    if (Trial_ID == -1) // 1st time in
    {
        ++Trial_ID;
        Display_Table_Header("BUF", APU_DIR_CHANNEL_MAX);
    }
    else
    {
        ++Trial_ID;
    }

    for (size_t i = 0; i < APU_DIR_CHANNEL_SIZE; i++)
    {
        if (Terminal_Mode == true) // user wants terminal back
            return;
        mcu_stdio_printf("%d\t%d", Trial_ID, (int)i);  // prepend line with common id for subset of tests
        for (size_t ch = 0; ch < APU_DIR_CHANNEL_MAX; ch++)
        {
            if (Terminal_Mode == true) // user wants terminal back
            return;
        #if(0)
        mcu_stdio_printf("\t%8x", (int16_t)APU_DIR_BUFFER[ch][i]);
        #else
        mcu_stdio_printf("\t%d", (int16_t)APU_DIR_BUFFER[ch][i]);
        #endif        
        }
        mcu_stdio_printf("\n\r");
    }
    mcu_stdio_printf("##TRIAL DONE##\n");
    TEST_State = TEST_WAIT;
    //Wait_for_Letter_C = true;
    return; // return after call
}

void Display_I2S_Data_Table(void)
{
    //if (Trial_ID == 0) // 1st time in

    uint16_t num_pts_to_plot =  APU_DIR_CHANNEL_SIZE + (Enable_i2s_rx_buff_padding ? 32 : 0);
    ++Trial_ID;
    if (Enable_I2S_Data_Table)
    {
        Display_Table_Header("I2S", 8);
        for (size_t i = 0; i < num_pts_to_plot; i++) // if padding enabled then include extra 32 values... ensures valid delayed values for S/W DAS operations
        {
            if (Terminal_Mode == true) // user wants terminal back
                return;
            mcu_stdio_printf("%d\t%d", Trial_ID, (int)i);  // prepend line with common id for subset of tests
            for (size_t ch = 0; ch < 8; ch++)
            {
                if (Terminal_Mode == true) // user wants terminal back
                    return;
                mcu_stdio_printf("\t%d", (int16_t)I2s_Data[ch][i]);
            }
            mcu_stdio_printf("\n\r");
        }
    }

    if (Enable_I2S_Hard_FFT_Table)
    {
        Display_Table_Header("HFT", 8);
        for (size_t i = 0; i < APU_DIR_CHANNEL_SIZE / 2; i++)
        {
            if (Terminal_Mode == true) // user wants terminal back
                return;
            mcu_stdio_printf("%d\t%d", Trial_ID, (int)i);  // prepend line with common id for subset of tests
            for (size_t ch = 0; ch < 8; ch++)
            {
                if (Terminal_Mode == true) // user wants terminal back
                    return;
                mcu_stdio_printf("\t%d", (int16_t)I2s_Hard_FFT_Power_Data[ch][i]);
            }
            mcu_stdio_printf("\n\r");
        }
    }

    if (Enable_I2S_Soft_FFT_Table)
    {
        Display_Table_Header("SFT", 8);
        for (size_t i = 0; i < APU_DIR_CHANNEL_SIZE / 2; i++)
        {
            if (Terminal_Mode == true) // user wants terminal back
                return;
            mcu_stdio_printf("%d\t%d", Trial_ID, (int)i);  // prepend line with common id for subset of tests
            for (size_t ch = 0; ch < 8; ch++)
            {
                if (Terminal_Mode == true) // user wants terminal back
                    return;
                mcu_stdio_printf("\t%d", (int16_t)I2s_Soft_FFT_Power_Data[ch][i]);
            }
            mcu_stdio_printf("\n\r");
        }
    }
    mcu_stdio_printf("##TRIAL DONE##\n");
    TEST_State = TEST_WAIT;
    //Wait_for_Letter_C = true;
    //return; // return after call
}

#endif

/*************************************************************************************
 * get_and_summarize_bf_data() takes as an arg a pointer to a struct which holds 
 * the historical data for earlier BF scans for the current configuration
 * 
 * Present configurations scan for azimuth (AZM) and incident (INC) angles and signal 
 * strength. Other configurations will be possible with this approach. 
 * For example: A 3x4 resizable grid that is progressively focussed.
 **************************************************************************************/
int get_and_summarize_bf_data(void)
{
    // global vars : in struct.... used to scale barchart so must be rememverd with rest of data for this BF'r instance

    // global vars : NOT in struct
    dir_sum_max = 0;
    dir_buffer_near_overflow_detected = false;
    
    // local vars
    bool dir_buffer_overflow_detected = false;
    uint16_t dir_buffer_near_overflow_count_local = 0; // counter to indicate when we're within 10% of overflow in buffer values so we can proactively adjust APU_Gain
    uint32_t APU_DIR_BUFFER_ch_i_max = 0;              // may be (but not yet) used to retain absolute largest value to adjust APU_Gain... WIP 
    uint64_t ssq_dir_sum[16] = { 0 };                  
    uint64_t ssq_dir_sum_max = 0;                      // remember the largest ssq_dir_sum[] value so scaling can be more easily done with that 
    uint64_t avg_ssq_dir_sum_max;

    // This is the main loop that goes through all the elements of the 2D APU_DIR_BUFFER
    for (size_t ch = 0 ; ch < APU_DIR_CHANNEL_MAX ; ch++)
    {
        ssq_dir_sum[ch] = 0;

        if (Terminal_Mode == true) // user opted to break loop
          break;

        dir_buffer_near_overflow_count_local = 0;   // reset local counter

        for(size_t i = DISCARDED_DIR_BUFF_SAMPLES ; i < APU_DIR_CHANNEL_SIZE ; i++)
        {

            if (Terminal_Mode == true) // user opted to break loop
                break;

            ssq_dir_sum[ch] += (int32_t)APU_DIR_BUFFER[ch][i] * (int32_t)APU_DIR_BUFFER[ch][i];  // accumulate SSQ values and retain for each chanbnnel for later use
        
            /************************************************************************************************************************************
            *            Automatic Gain Control Data Point Checks
            * #1) Check individual data points in DIR Buffer for values for overflow = abs(32768) = abs(0x7fff) 
            * #2) Checks Delay and Sum accumulator (ssq_dir_sum) for exceeding uint32 register size when averaged before assigning to channel
            * #3) Check individual data points in DIR Buffer for values within 5% of full scale.
            *     
            * Only #1 is consider fatal for the current scan so exit the outer loop immediately. #2 & #3 are soft warnings
            * There are no concerns with #1. The current scan will end and the APU_Gain wil be adjusted so it'll work next time.
            * 
            * There is a boolean var called AGC_Adjust_Rate which enables this feature when it is >0 (on by default).
            * When disabled the gain can be accessed with SET & GET to the APU_Gain var.
            *
            *************************************************************************************************************************************/
            if(abs((int32_t) APU_DIR_BUFFER[ch][i]) == 0x00007FFF)
            {
                dir_buffer_overflow_count += 1;
                dir_buffer_overflow_detected = true;
                if (!(Enable_Beamformer_Stats || Enable_Barchart))
                    printf("WARNING: Overflow detected in DIR buffer[%d][%d]) = %8x\n", (int)ch, (int)i, (int32_t)APU_DIR_BUFFER[ch][i]);
                break; // quit inner loop... no recovery here
            }

            // Now check for near overflow within +/- 25% of full scale readings (APU_DIR_BUFFER[][] == 0x00007FFF || 0xFFFF8000  )
            if(abs((int32_t) APU_DIR_BUFFER[ch][i]) > (0x00007FFF * 0.75)) // hardcoded level... 
            {
                dir_buffer_near_overflow_count += 1;       // lifetime statistic 
                dir_buffer_near_overflow_count_local += 1; // stat for current scan only 
                dir_buffer_near_overflow_detected = true;  // remember this across all 16x512 loop operations
                if (abs((int32_t) APU_DIR_BUFFER[ch][i]) > APU_DIR_BUFFER_ch_i_max)
                    APU_DIR_BUFFER_ch_i_max = abs((int32_t) APU_DIR_BUFFER[ch][i]);  // retain absolute largest value and use to adjust APU_Gain
            }
        }
        
        // #1) Another break necessary to completely terminate current scan 
        if(dir_buffer_overflow_detected) 
            break;  // quit outer loop

        // #2) Print warning about being near overflow
//        if(dir_buffer_near_overflow_count_local > 0) 
//            printf("WARNING: Near overflow detected %d times in DIR buffer[%d] \n", dir_buffer_near_overflow_count_local, (int)ch);
        
        // #3) Save largest ssq_dir_sum[] value for later limit checks
        if(ssq_dir_sum[ch] > ssq_dir_sum_max)
            ssq_dir_sum_max = ssq_dir_sum[ch];
    }

    /************************************************************************************************************************************
    *            Automatic Gain Control Next Steps
    * #1) If we detected a hardware register overflow then reduce H/W gain register to 90% of current value and exit current scan.
    * #2) Check Delay and Sum accumulator (ssq_dir_sum) for exceeding uint32 register size when averaged before assigning to channel
    *     If found then reduces gain by 95% of ratio of uint32 value (0xFFFFFFFF) to current value 
    * #3) Checks individual data points in DIR Buffer for values within 10% of full scale.
    *     If found but overflow not detected then it reduces H/W gain register to 90% of current value and terminates the current scan.
    *     
    * At the end of these steps we are safely avoiding overflow conditions (with some uncertainty associated with individual I2S inputs)
    *
    *************************************************************************************************************************************/
      
    // AGC Step #1: If we detected a hardware register overflow then quit current scan     
    if(dir_buffer_overflow_detected && AGC_Enable && (AGC_Adjust_Rate > 0))
    {
        float APU_Gain_new = APU_Gain * 0.9;  // will be adjusted down to 90% of full scale... This should prevent the close calls
        set_apu_gain(APU_Gain_new);           // at this point H/W and S/W APU_Gain registers are set
        printf("Reducing current gain [%f] by 10%% FS\n", APU_Gain);
        //goto quit_loop;                       // jump to end to initiate new capture
        return 0;                               // scan interrupted by fault or user request
    }
    
    // AGC Step #2:  Now we need to check ssq_dir_sum_max to see if it exceeds uInt32 size and correct individual ssq_dir_sum_max[] entries and APU_gain as well.
    avg_ssq_dir_sum_max = (double)(ssq_dir_sum_max) / (APU_DIR_CHANNEL_SIZE - DISCARDED_DIR_BUFF_SAMPLES);
    avg_ssq_dir_sum_max_exceeds_uint32 = (avg_ssq_dir_sum_max >= 0xFFFFFFFF);

    //    if (avg_ssq_dir_sum_max_exceeds_uint32)
    //        printf("avg_ssq_dir_sum_max_exceeds_uint32 = true");

    // calculate dir_sum values for each channel, normalizing as required
    for(size_t ch = 0 ; ch < APU_DIR_CHANNEL_MAX ; ch++)
    {
        float Data_Scalar = 1.0;   // unity multiplier if no correction is required
        if(avg_ssq_dir_sum_max_exceeds_uint32 && AGC_Enable && (AGC_Adjust_Rate > 0)) // Adjustments if required so calculate scale factor to be applied to APU_Gain and existing data... do just once... was only checking ch==0!!!
        {
            Data_Scalar = ((double)0xFFFFFFFF) / avg_ssq_dir_sum_max * 0.95; 
            float APU_Gain_new = APU_Gain * Data_Scalar;                       // will be adjusted down to 95% of full scale... arbitrary hardcoded value
            set_apu_gain(APU_Gain_new);                                        // function sets H/W and S/W APU_Gain registers
        }
        
        CH_Data[ch].dir_sum = (double)(ssq_dir_sum[ch]) * Data_Scalar  / (APU_DIR_CHANNEL_SIZE - DISCARDED_DIR_BUFF_SAMPLES);
        CH_Data[ch].CH = ch;    // save index

        // Original max check method - may be deleted when sort routine is finished. SEH:20200816:Status?
        if(CH_Data[ch].dir_sum > dir_sum_max)
            dir_sum_max = CH_Data[ch].dir_sum;
    }
      
    // AGC Step #3: One or more of the values supplied by the APU DAS beamformer module is near overflow so adjust the APU_Gain to be safe... not a problem now though
    if(dir_buffer_near_overflow_detected > 0 && !avg_ssq_dir_sum_max_exceeds_uint32 && AGC_Enable && (AGC_Adjust_Rate > 0)) // don't do the APU_Gain adjustment if already done for AGC step #2
    {
#if (1)
        float APU_Gain_new = APU_Gain * 0.9; 
#else        
        float APU_Gain_max = (float)0x7ff / 0x400;
        uint32_t Value_max = (0x00007FFF * 0.9);
        float APU_Gain_new = APU_Gain * (float)Value_max / (float)APU_DIR_BUFFER_ch_i_max;
        //printf("avg_ssq_dir_sum_max_exceeds_uint32 = %s, Value_max = %x, APU_DIR_BUFFER_ch_i_max = %x, APU_Gain_max = %f, APU_Gain_new = %f\n", avg_ssq_dir_sum_max_exceeds_uint32 ? "True" : "False", Value_max, APU_DIR_BUFFER_ch_i_max, APU_Gain_max, APU_Gain_new);
#endif
        set_apu_gain(APU_Gain_new);       // at this point H/W and S/W APU_Gain registers are set

    }
    
    if (dir_buffer_overflow_detected || Terminal_Mode == true || dir_sum_max==0)
        return 0;        // scan interrupted by fault or user request (dir_sum_max==0 is an unexppected case)
    else
        return 1;        // successful scan... could return a useful
}

/****************************************************************************************
 * Correction to DBFS sent to host that more closely aligns value with UE System detector
 * Also used in the check against the _7_Min_Sig_Strength_Detected formula (BF FSM input)
 ****************************************************************************************/
double Corrected_dB(double dBin)
{
    double dBout = (((dBin + (DBFS_UE_ADJ_ADD - MIN_MIC_DB)) * DBFS_UE_ADJ_MULT) + MIN_MIC_DB);
    return dBout;
}

double cross(double complex A, double complex B)
{
    // c = a X b = a.x*b.y − a.y*b.x
    return creal(A)*cimag(B) - cimag(A)*creal(B);
    // (x1,y1)×(x2,y2):=x1y2−x2y1
}

int __dbgopt__ voc_logic(void)
{
    apu_voc_enable(0);
	// do something for VOC data
	if (active_BF == INC)
	{
	    calculate_fft(APU_VOC_BUFFER, VOC_FFT_Data, NULL);
	}
	
	
	return 0;
}

int __dbgopt__ dir_logic(void* Pself)
{
    beamformer_s *self = (beamformer_s*)(Pself);
    
    // local vars
    DIR_Result_s DIR_Result = { 0 }; // individual MPA ring buffer entry ... only lives during current BF scan but will be inserted into DIR_Result_BUFF for recall by this BF'r next time 

#ifdef DEBUG    
    if (Terminal_Mode == true) // user wants terminal back... prob doesn't matter having this right here
        return 1;

    if (Enable_Barchart || Enable_Beamformer_Stats || Enable_DIR_Buffer_Table || Enable_DIR_Results_Table || Enable_I2S_Data_Table || Enable_I2S_Hard_FFT_Table || Enable_I2S_Soft_FFT_Table) 
        usleep(7500); // quick refresh fairly smooth 
 
    if (Enable_Barchart || Enable_Beamformer_Stats) 
        moveTo(0, 0); 
#endif

    if (!get_and_summarize_bf_data())
    {
        goto quit_loop; // jump to end to initiate new capture
    }

    // Sort by dir_sum in descending order
    qsort(CH_Data, APU_DIR_CHANNEL_MAX, sizeof(CH_Data_s), dir_sum_compare);

    // Link Check Routine
    uint8_t ID_LHS = CH_Data[0].CH;
    uint8_t ID_RHS = CH_Data[0].CH;
    uint8_t ID_HEAD = CH_Data[0].CH;
    bool linked;

    for (uint8_t i = 1; i < APU_DIR_CHANNEL_MAX; i++) // start with 2nd element in array
    {
    linked = false;

    // check left side
    if (CH_Data[i].CH == ((ID_LHS - 1 + APU_DIR_CHANNEL_MAX) % APU_DIR_CHANNEL_MAX)) // mod function wraps so 15+1==0 and 0-1==15 ... see ./resources/MOD_16_Test.xlsx
    {
        linked = true;
        ID_LHS = CH_Data[i].CH;
    }

    // check right side
    else if (CH_Data[i].CH == ((ID_RHS + 1) % APU_DIR_CHANNEL_MAX))
    {
        linked = true;
        ID_RHS = CH_Data[i].CH;
    }

    // Do we still have a linked chain?
    if (linked == false)
        break;
    };

    // Now save the relevant info
    ID_Max = ID_HEAD;
    ID_First = ID_LHS;
    ID_Last = ID_RHS;
    uint8_t N_linked = (ID_RHS == ID_LHS ? 1 : (ID_RHS > ID_LHS ? ID_RHS - ID_LHS + 1 : (ID_RHS + 16) - ID_LHS + 1));

    // Restore original sort order to compute statistics
    qsort(CH_Data, APU_DIR_CHANNEL_MAX, sizeof(CH_Data_s), CH_ID_compare); // FIXME

    // Calculate average of linked DIR vectors
    // https://en.cppreference.com/w/c/numeric/complex
    AVG_Linked_DIR_SUM_Vect = 0 + I*0;
    for (int i = ID_First; i <= ID_Last + ((ID_Last < ID_First ? APU_DIR_CHANNEL_MAX : 0)); i++)
        AVG_Linked_DIR_SUM_Vect = AVG_Linked_DIR_SUM_Vect + CH_Data[i % APU_DIR_CHANNEL_MAX].dir_sum * (self->id == AZM  ? Unit_DIR_Vectors_AZM[i % APU_DIR_CHANNEL_MAX] : Unit_DIR_Vectors_INC[i % APU_DIR_CHANNEL_MAX]);
 
    AVG_Linked_DIR_SUM_Vect = AVG_Linked_DIR_SUM_Vect / N_linked; // and the average

    Angle = carg(AVG_Linked_DIR_SUM_Vect * Azimuth_Local_to_Real_World_Rotation_Vector()) * 180.0 / M_PI;  // corrected locally... not really of any use... may not need to retain this 

#if APU_VOC_ENABLE    
    if (active_BF == INC)
        apu_voc_set_direction((en_bf_dir_t)ID_Max);
#endif    
    // MPA for angle using vectors... load current values in record...
    DIR_Result.ID_Max = ID_Max;
    DIR_Result.DIR_SUM_Vect_MAX = CH_Data[ID_Max].dir_sum * (self->id == AZM  ? Unit_DIR_Vectors_AZM[ID_Max] : Unit_DIR_Vectors_INC[ID_Max]);
    DIR_Result.N_linked = N_linked;
    DIR_Result.Linked_DIR_SUM_Vect = AVG_Linked_DIR_SUM_Vect;
    if (AVG_Linked_DIR_SUM_Vect == 0 + 0*I)
        printf("AVG_Linked_DIR_SUM_Vect = 0+0*I\n");

    /*********************************************************************************
     *********************************************************************************
     **  This would be a good point to split the processing activity
     **  core1 (current) has received all the data for the current scan,
     **  reduced it to a minimal set of useful data:
     **     DIR_Result.N_linked
     **     DIR_Result.Linked_DIR_SUM_Vect
     **     and new stats fields 
     ** 
     ** We can send the summary fields this the other core 
     ** for further processing and aggregate processing in the case of MCU0.
     ** 
     ** To do this we need a new "aggregator" process running on core0
     ** We would involke this by raising an interrupt after first copying parts of the 
     ** DIR_Result struct into a s_context record for the other core to use.
     **
     *********************************************************************************
     **********************************************************************************/
    
    /*********************************************************
    * We are doing trend on the angle but this breaks at ±180°
    * Really need to do all stat accumulation with 
    * Linked_DIR_SUM_Vect and N_linked weighting
    * 
    * https://en.wikipedia.org/wiki/Complex_normal_distribution
    * https://dsp.stackexchange.com/questions/40306/what-would-be-the-variance-for-complex-number
    * https://en.wikipedia.org/wiki/Complex_random_variable   
    * http://web.mit.edu/6.02/www/currentsemester/handouts/L08_slides.pdf
    * https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.310.639&rep=rep1&type=pdf
    * 
    * https://en.wikipedia.org/wiki/Directional_statistics
    * 
    * Uniform and Concentric Circular Antenna Arrays Synthesis for Smart Antenna Systems Using Artificial Neural Network Algorithm
    * https://www.jpier.org/PIERB/pierb67/07.16031508.pdf
    *
    **********************************************************/

    if ((uint8_t)self->MPA_N_Current < MPA_N)
        self->MPA_N_Current += 1;
    else
        self->MPA_N_Current = MPA_N;

    // Now insert our new record into the ring buffer...
    self->DIR_Result_BUFF[self->MPA_Index] = DIR_Result;
    
    /**********************************************
     * Recalculate various MPA's and statistics
     **********************************************/
    // Clear accumulators first... 
    self->SUM_N_linked = 0; 
    self->SUM_Linked_DIR_SUM_Vect = 0 + 0*I;  
    self->SUM_DIR_SUM_Vect_MAX = 0 + 0*I;
    self->UVL.rotation = 0;
    self->UVM.rotation = 0;
    self->UVL.dispersion = 0;
    self->UVM.dispersion = 0;

    uint32_t SUM_rot_weight   = 0; // local var
    uint32_t SUM_disp_weight  = 0; // local var
    
    // Then fill them up... .... need to start and end at correct buffer index...
//    if ((Debug_AZM_BF && self->id == AZM) || (!Debug_AZM_BF && self->id == INC))
//        printf("Started...\n"); 
    for(int i = 0 ; i < self->MPA_N_Current ; i++) 
    { 
        uint8_t current_index = (self->MPA_Index + 1 + i) % self->MPA_N_Current; // start at oldest item in ring buffer (
        
        self->SUM_N_linked += self->DIR_Result_BUFF[current_index].N_linked; 
        self->SUM_Linked_DIR_SUM_Vect += self->DIR_Result_BUFF[current_index].Linked_DIR_SUM_Vect; 
        self->SUM_DIR_SUM_Vect_MAX += self->DIR_Result_BUFF[current_index].DIR_SUM_Vect_MAX;  
        

        // Dispersion measures to assess stability/stationality
        double complex C_temp;
        uint32_t disp_weight = pow(i + 1, 3);  // forces non-zero weight
        
        C_temp = self->DIR_Result_BUFF[current_index].Linked_DIR_SUM_Vect;
        self->UVL.dispersion += disp_weight * cabs((C_temp / cabs(C_temp)) - self->UVL.V);    // DIFF normlized current linked vector with MPA averaged UVL version

        C_temp = self->DIR_Result_BUFF[current_index].DIR_SUM_Vect_MAX;
        self->UVM.dispersion += disp_weight * cabs((C_temp / cabs(C_temp)) - self->UVM.V);    // DIFF normlized current max vector with MPA averaged UVM version
        
        SUM_disp_weight += disp_weight;
        // Determine current rotation (CW or CCW) based on cross-product of the previous vector with current one
        // Right Hand Rule (RHR) applies so +ve is CCW on receiver face with thumb outwards
        // Calculate the SUM of rotation test results using current and previous point for next pass
        // Scalar values that on average will indicate +ve or -ve rotation
        // Weighting function needed to favour latest data
        // https://www.euclideanspace.com/maths/algebra/vectors/angleBetween/
        // https://gamedev.stackexchange.com/questions/153180/how-to-determine-whether-the-direction-of-rotating-a-2d-vector-to-another-one-is
        // https://stackoverflow.com/questions/65324625/dot-product-and-cross-product-in-c-using-structures
        // https://en.wikipedia.org/wiki/Cross_product
        if(i > 0) // need to start comparing vector #1 to #0... if MPA_N==1 then rotation is always zero... impractical corner cose
        {
            uint8_t previous_index = (self->MPA_Index + i) % self->MPA_N_Current;
            uint32_t rot_weight = pow(i,2); // no larger than i<<5 or we could overflow with MPA_N==64
            // c = a X b = a.x*b.y − a.y*b.x
            double complex VA;
            double complex VB;
            double rotation_i;
            
            // rotation using linked vectors...
            VA = self->DIR_Result_BUFF[previous_index].Linked_DIR_SUM_Vect / cabs(self->DIR_Result_BUFF[previous_index].Linked_DIR_SUM_Vect); 
            VB = self->DIR_Result_BUFF[current_index].Linked_DIR_SUM_Vect  / cabs(self->DIR_Result_BUFF[current_index].Linked_DIR_SUM_Vect); 
            rotation_i = cross(VA, VB); 
            self->UVL.rotation += rot_weight*rotation_i;

// Rotation Debug
//            char temp[64]; 
//            sprintf(temp, "[%d,%d]=%6.4f%s", previous_index, current_index, self->UVL.rotation, i == (self->MPA_N_Current - 1) ? "]]\n" : ","); 
//            double prev_creal = creal(VA); 
//            double prev_cimag = cimag(VA); 
//            double curr_creal = creal(VB); 
//            double curr_cimag = cimag(VB); 
//            if ((Debug_AZM_BF && self->id == AZM) || (!Debug_AZM_BF && self->id == INC))
//                printf("%s: previous[%d] =\t%9.7f\t%s%9.7f\ti, current[%d] = \t%9.7f\t%s%9.7f\ti, rotation = \t%6.4f\tprev_angle =\t%6.2f\tcurr_tangle =\t%6.2f\n", self->name, previous_index, prev_creal, prev_cimag >= 0 ? "+" : "-", fabs(prev_cimag), current_index, curr_creal, curr_cimag >= 0 ? "+" : "-", fabs(curr_cimag), rotation_i, carg(VA)*180/M_PI, carg(VB)*180/M_PI); 
            
            // ... and with MAX only
            VA = self->DIR_Result_BUFF[previous_index].DIR_SUM_Vect_MAX / cabs(self->DIR_Result_BUFF[previous_index].DIR_SUM_Vect_MAX); 
            VB = self->DIR_Result_BUFF[current_index].DIR_SUM_Vect_MAX  / cabs(self->DIR_Result_BUFF[current_index].DIR_SUM_Vect_MAX); 
            rotation_i = cross(VA, VB); 
            self->UVM.rotation += rot_weight*rotation_i;

            SUM_rot_weight += rot_weight;
        }
    } 
//    if ((Debug_AZM_BF && self->id == AZM) || (!Debug_AZM_BF && self->id == INC))
//        printf("... Finished\n"); 
    
    // ... then the average
    self->MPA.N_linked            = self->SUM_N_linked            / self->MPA_N_Current;  // average # of linked vectors
    self->MPA.Linked_DIR_SUM_Vect = self->SUM_Linked_DIR_SUM_Vect / self->MPA_N_Current;
    self->MPA.DIR_SUM_Vect_MAX    = self->SUM_DIR_SUM_Vect_MAX    / self->MPA_N_Current;
    
    /********************************************************************
     * Signal Rotation & Dispersion (inputs for stability test)
     *********************************************************************/
    // Store current vector rotation metrics 
    self->UVL.rotation = self->MPA_N_Current < 1 ? 0 : self->UVL.rotation / SUM_rot_weight;  // age weighted average
    self->UVM.rotation = self->MPA_N_Current < 1 ? 0 : self->UVM.rotation / SUM_rot_weight;  // age weighted average
    
    // https://stats.stackexchange.com/questions/13272/2d-analog-of-standard-deviation
    // Use distance of each point from the mean as measure of stability for UVL & UVM vectors
    self->UVL.dispersion = self->UVL.dispersion / SUM_disp_weight;  // MPA_N_Current<1 corner case?
    self->UVM.dispersion = self->UVM.dispersion / SUM_disp_weight;
    
    self->UVL.V = self->MPA.Linked_DIR_SUM_Vect / cabs(self->MPA.Linked_DIR_SUM_Vect);
    self->UVM.V = self->MPA.DIR_SUM_Vect_MAX    / cabs(self->MPA.DIR_SUM_Vect_MAX);
            
    /*************************************************
     * local angle for use by other BF'r 
     *************************************************/
    self->angle_local = carg(self->MPA.Linked_DIR_SUM_Vect) * 180.0 / M_PI;      // local angle for use by other BF'r
    
    Angle_MPA  = carg(self->MPA.Linked_DIR_SUM_Vect * (self->id == AZM ? Azimuth_Local_to_Real_World_Rotation_Vector() : 1 + 0*I)) * (180 / M_PI); // OK to RW correct now
    Angle_TEST = carg(self->MPA.DIR_SUM_Vect_MAX    * (self->id == AZM ? Azimuth_Local_to_Real_World_Rotation_Vector() : 1 + 0*I)) * (180 / M_PI); // OK to RW correct now
    
    /****************************************************************************************************
     * Angle Stability Assessment
     * Use rotation & stability measures on both AZM & INC BF's to assess stability 
     * Seems like linked (LNK version of rotation and dispersion are better)... 
     * Observations: 
     *     1) When INC~0 AZM is highly variable (due to close proximity to clusters)
     *        In this case use INC.rot & disp
     *     2) When INC>>0 we can look at both
     *     3) At this point we're in either the AZM or INC BF so we shouldn't look at the other for input 
     *        This would be done at a higher level
     *****************************************************************************************************/
    // is_Stable inputs (with hysteresis calculation)
    self->rotation_LT_LIM_minus_hys   = fabs(self->UVL.rotation)   < (self->rotation_LIM   - self->rotation_HYS / 2);
    self->rotation_GT_LIM_plus_hys    = fabs(self->UVL.rotation)   > (self->rotation_LIM   + self->rotation_HYS / 2);
    self->dispersion_LT_LIM_minus_hys = fabs(self->UVL.dispersion) < (self->dispersion_LIM - self->dispersion_HYS / 2);
    self->dispersion_GT_LIM_plus_hys  = fabs(self->UVL.dispersion) > (self->dispersion_LIM + self->dispersion_HYS / 2);

    // Is stability changing?
    if (!self->is_Stable)
    {
        if (self->rotation_LT_LIM_minus_hys && self->dispersion_LT_LIM_minus_hys)
            self->is_Stable = true;
    }
    else if(self->rotation_GT_LIM_plus_hys && self->dispersion_GT_LIM_plus_hys)
    {
        self->is_Stable = false;
    }
    //self->is_Stable  = (fabs(self->UVL.rotation) < self->rotation_LIM && self->UVL.dispersion < self->dispersion_LIM) ? true : false;

    self->is_Rot_Pos = !self->is_Stable ? (self->UVL.rotation > 0 ? true : false) : false;
    self->is_Rot_Neg = !self->is_Stable ? (self->UVL.rotation < 0 ? true : false) : false;
    
    // increment ring buffer index and wrap when at max value (will point to next slot)
    self->MPA_Index = (self->MPA_Index + 1) % MPA_N;    
    
    // Calculate VRMS & DBFS
    VRMS_MPA  = sqrt(cabs(self->MPA.Linked_DIR_SUM_Vect) / (APU_DIR_CHANNEL_SIZE - DISCARDED_DIR_BUFF_SAMPLES)) / 8 / APU_Gain;  // SEH: Check use of discarded samples 
    VRMS_TEST = sqrt(cabs(self->MPA.DIR_SUM_Vect_MAX)    / (APU_DIR_CHANNEL_SIZE - DISCARDED_DIR_BUFF_SAMPLES)) / 8 / APU_Gain; 
    // correction for gain... div 8 because we add 8 mics together and need to average them
    // ... then div APU_Gain, which is real number equivalent of value programmed into APU gain register (fix floating point format).
    // This has to be investigated more using hardware and verified with independant instrumentation
    DBFS_MPA  = VRMS_MPA  < 1E-8 ? MINUS_INF : (double)20 * log10(VRMS_MPA  / (1 << 15));     // data is 16 bit signed so 15 bits for magnitude
    DBFS_TEST = VRMS_TEST < 1E-8 ? MINUS_INF : (double)20 * log10(VRMS_TEST / (1 << 15));      // data is 16 bit signed so 15 bits for magnitude
    
    DBFS_CORR = VRMS_MPA < 1E-8 ? MINUS_INF : Corrected_dB(DBFS_MPA);
    DBFS = DBFS_UE_Adj ? DBFS_CORR : DBFS_MPA;
    
    /*********************************************************************************************
     * Save angle and dbfs data in all_mcu aggregators
     * This is where we can assign values for perform A/B testing 
     *********************************************************************************************/
    // Default data used for actual beamformer... also considered the "A" data in A/B testing
    // These arrays should be moved into the beamformer_s struct
    self->angle = Angle_MPA;
    self->dbfs = DBFS;
    
    // DUT assignments
    Angle_DUT1 = Angle_MPA;
    DBFS_DUT1  = DBFS;

    Angle_DUT2 = Angle_TEST;
    DBFS_DUT2  = DBFS_TEST;
    
    // Select DUT test data for "B" part of A/B test ... 
    self->angle_dut = Select_Angle_DUT1 ? Angle_DUT1 : Angle_TEST; 
    self->dbfs_dut  = Select_DBFS_DUT1  ? DBFS_DUT1  : DBFS_TEST;  
    
    if ((Debug_AZM_BF && self->id==AZM) || (!Debug_AZM_BF && self->id==INC)) // pick which BF to debug
    {
        // Switch statement?
        if ((Host_Update_Counter >= Host_Update_Interval) || BF_FSM_STATE_CHANGE)
        {
            Host_Update_Counter = (Host_Update_Counter >= Host_Update_Interval) ? 0 : Host_Update_Counter + 1;
            #ifdef DEBUG
        
        if (Enable_Barchart || Enable_Beamformer_Stats)
        {                
            Display_Beamformer_header(self);
            if (Enable_Barchart) 
                Display_Barchart();
 
            if (Enable_Beamformer_Stats) 
                Display_Beamformer_Stats(self); 
        }

        else if (Enable_DIR_Results_Table && TEST_State==TEST_RUN /*!Wait_for_Letter_C*/)
        {
            Display_DIR_Results_Table();
        }
        else if (Enable_DIR_Buffer_Table && TEST_State==TEST_RUN /*!Wait_for_Letter_C*/)
        {
            Display_DIR_Buffer_Table();
        }
        else if ((Enable_I2S_Data_Table || Enable_I2S_Hard_FFT_Table || Enable_I2S_Soft_FFT_Table) && TEST_State==TEST_RUN  /*!Wait_for_Letter_C*/)
        {
            Display_I2S_Data_Table();
        }   
        #endif
        }
        else
        {
            Host_Update_Counter += 1;
        }
    }

    /************************************************************************************************************************************
    *            AGC step to maintain highest gain value.... if we didn't just drop the gain bump it up 5% every AGC_Adjust_Rate... 
    * 
    *   Progressive adjustments when the current gain is very low so we maximize it 
    * 
    *************************************************************************************************************************************/
    
    if (AGC_Enable) 
    {
        AGC_Adjust_Counter += 1;
        if ((AGC_Adjust_Rate>0) && (AGC_Adjust_Counter % AGC_Adjust_Rate == 0))
        {    
            AGC_Adjust_Counter = 0;
            if (!avg_ssq_dir_sum_max_exceeds_uint32 && !dir_buffer_near_overflow_detected && (AGC_Adjust_Rate > 0))
            {
                float APU_Gain_max = (float)0x7ff / 0x400;
                if (APU_Gain < APU_Gain_max)
                {
                    //float APU_Gain_new = APU_Gain + (APU_Gain_max*0.95 - APU_Gain)*0.5 ;   // close gap between our safe max (95% of full scale) and current value by 50%
                    float APU_Gain_new = APU_Gain * 1.10;
                    //printf("Increasing Gain: Old = %f, New = %f\n", APU_Gain, APU_Gain_new);
                    set_apu_gain(APU_Gain_new);             // set gain registers
                }
            }
        }
    }    
    quit_loop:;

    t_us_current = sysctl_get_time_us();
    loop_time = (uint32_t)(t_us_current - t_us_start);
    t_us_start = sysctl_get_time_us();
    return 0;
}

void Reset_BF_Accumulators(void* Pself) 
{ 
    beamformer_s *self = (beamformer_s*)(Pself); 
    
    // Not all of this is needed if we continue to rebuild all accumulators each scan
    
    // reset counters, pointers, etc. 
    self->MPA_N_Current = 0; // will be incremented to 1 before 1st use
    self->MPA_Index = 0;  
    memset(self->DIR_Result_BUFF, 0, MPA_NMAX * sizeof(DIR_Result_s));  // not really necessary 
     
    // reset accumulators 
    self->SUM_N_linked = 0; 
    self->SUM_Linked_DIR_SUM_Vect = 0 + 0*I;  
    self->SUM_DIR_SUM_Vect_MAX    = 0 + 0*I; 
    self->UVL.rotation = 0;
    self->UVM.rotation = 0;
    self->UVL.dispersion = 0;
    self->UVM.dispersion = 0;
} 

int verify_slave_data(uint8_t slave_id, slave_mcu_data_s* p_slave_data )
{
    uint8_t check_sum = 0;
    if (p_slave_data->slave_id != slave_id)
        return -1;
    else 
    {
        check_sum = p_slave_data->check_sum;
        if (check_sum != checksum((uint8_t*)((uint32_t)p_slave_data+2), SLAVE_DATA_LENGTH-2))
        {
            return -1;
        }
    }
    return 0;    
}

int core_ipi_callback_func(void * ctx)
{
    data_link_loop();
        
    return 0;
}

void data_link_loop(void)
{
    int ret = 0;
    if (mcu1_bf_data_ready_flag)
    {
        //printf("MCU 1 BF data ready\r\n");
        mcu1_bf_data_ready_count++;

        if ((enable_mcu1_bf_data_read > 0) && (mcu1_bf_data_ready_count > BF_DATA_READ_THRESHOLD) && (mcu1_bf_data_ready_count % BF_DATA_READ_RATE == 0))
        {
            // read data from slave MCU 1
            ret = spi_master_transfer(MCU1, (uint8_t*)&slave_mcu_data_tmp[0], slave1_data_addr, SLAVE_DATA_LENGTH, READ_DATA_BLOCK);
            if (ret == 0)
            {
                mcu1_bf_data_read_count++;
                swap_bytes_in_double_words((void *)&slave_mcu_data_tmp[0], SLAVE_DATA_LENGTH);
                // verify if the read data is correct or not
                if (verify_slave_data(MCU1, &slave_mcu_data_tmp[0]) != 0)
                {
                    mcu1_bf_data_read_error_count++;
                }
                else
                {
                    memcpy((void*)&slave_mcu_data[0], (void *)&slave_mcu_data_tmp[0], SLAVE_DATA_LENGTH);
                }
            }
            else
                mcu1_bf_data_read_error_count++;
        }

        mcu1_bf_data_ready_flag = 0;
    }
    
    if (mcu2_bf_data_ready_flag)
    {
        //printf("MCU 2 BF data ready\r\n");
        mcu2_bf_data_ready_count++;

        if ((enable_mcu2_bf_data_read > 0) && (mcu2_bf_data_ready_count > BF_DATA_READ_THRESHOLD) && (mcu2_bf_data_ready_count % BF_DATA_READ_RATE == 0))
        {
            // read data from slave MCU 2
            ret = spi_master_transfer(MCU2, (uint8_t*)&slave_mcu_data_tmp[1], slave2_data_addr, SLAVE_DATA_LENGTH, READ_DATA_BLOCK);
            if (0 == ret)
            {
                mcu2_bf_data_read_count++;
                swap_bytes_in_double_words((void *)&slave_mcu_data_tmp[1], SLAVE_DATA_LENGTH);
                // verify if the read data is correct or not
                if (verify_slave_data(MCU2, &slave_mcu_data_tmp[1]) != 0)
                {
                    mcu2_bf_data_read_error_count++;
                }
                else
                {
                    memcpy((void*)&slave_mcu_data[1], (void *)&slave_mcu_data_tmp[1], SLAVE_DATA_LENGTH);
                }
            }
            else
                mcu2_bf_data_read_error_count++;
        }

        mcu2_bf_data_ready_flag = 0;
    }

}

void event_loop(void)
{

    // Check if MPA_N was modified by the host and reset acumulators in each BF if so 
    if(MPA_N_changed) 
    { 
        Reset_BF_Accumulators(&BF[AZM]); 
        Reset_BF_Accumulators(&BF[INC]); 
        MPA_N_changed = false; 
    } 
 

    // Beamformer scan AZM & INC in configurable proportion of time
    if (dir_logic_count > 0)
    {
        if (AZM_INC_BF_Duty_Cycle_counter % AZM_INC_BF_Duty_Cycle == 0)
        {
            active_BF = INC; 
            if (dir_logic(&BF[INC]) == 0) 
            {
                // conditional assignment... use DUT from AZM or INC by switch?
                if(Assign_INC_to_DUT)
                {
                    all_bf_data.dut.angle[mcu_id] = BF[INC].angle_dut;
                    all_bf_data.dut.dbfs[mcu_id] = BF[INC].dbfs_dut;
                    memcpy(&(s_context.data.dut), &(all_bf_data.dut), sizeof(_all_mcu_bf_data_s));
                }
                // Populate 3x angle & dbfs arrays. Only MCU0 uses all 3 entries. The 1&2 slots are filled by JSON parser right now.
                all_bf_data.inc.angle[mcu_id] = BF[INC].angle;
                all_bf_data.inc.dbfs[mcu_id] = BF[INC].dbfs;

                // copy results from scan to record 
                memcpy(&(s_context.data.inc), &(all_bf_data.inc), sizeof(_all_mcu_bf_data_s));
                // We will use the angle result from here to set the FOV angle for the AZM beamformer
                // usleep(5000);  // little delay needed to prevent core crash
            }
            AZM_INC_BF_Duty_Cycle_counter = 0;

            Angle_BF_AZM_auto = 2*fabs(BF[INC].angle_local);
            // if angle_local==0 then FOV==0 and all delays==0... this is an invalid case for AZM BF... should be some min value... 
            Angle_BF_AZM_auto = Angle_BF_AZM_auto < 8 ? 8 : Angle_BF_AZM_auto;  
            apu_set_delays(I2S_FS_actual, UCA8_RADIUS_CM, UCA_N, Angle_BF_AZM_auto_EN ? Angle_BF_AZM_auto : Angle_BF_AZM_static);    // set delays back to azimuth configuration
            apu_get_delays(BF[AZM].delays); 
        }
        else
        {
            active_BF = AZM; 
            if (dir_logic(&BF[AZM]) == 0) 
            {
                // conditional assignment... use DUT from AZM or INC by switch
                if (!Assign_INC_to_DUT)
                {
                    all_bf_data.dut.angle[mcu_id] = BF[AZM].angle_dut;
                    all_bf_data.dut.dbfs[mcu_id] = BF[AZM].dbfs_dut;
                    memcpy(&(s_context.data.dut), &(all_bf_data.dut), sizeof(_all_mcu_bf_data_s));
                }

                // Populate 3x angle & dbfs arrays. Only MCU0 uses all 3 entries
                all_bf_data.azm.angle[mcu_id] = BF[AZM].angle;
                all_bf_data.azm.dbfs[mcu_id] = BF[AZM].dbfs;

                memcpy(&(s_context.data.azm), &(all_bf_data.azm), sizeof(_all_mcu_bf_data_s));
                               
                s_context.callback(&s_context.data); // only call when az is being updated?
                
                // fill the slave_bf_data structure using the latest BF results
                
                
                // generate a BF data ready interrupt from slave MCUs
                if (MCU1 == mcu_id)
                {
                    slave_data_tmp.BF_STATE  = all_bf_data.BF_STATE[mcu_id];
                    slave_data_tmp.padding_0 = 0x00;
	                slave_data_tmp.padding_1 = 0x00;
                    slave_data_tmp.azm_angle = all_bf_data.azm.angle[mcu_id];
                    slave_data_tmp.azm_dbfs  = all_bf_data.azm.dbfs[mcu_id];
                    slave_data_tmp.inc_angle = all_bf_data.inc.angle[mcu_id];
                    slave_data_tmp.inc_dbfs = all_bf_data.inc.dbfs[mcu_id];
                    slave_data_tmp.dut_angle = all_bf_data.dut.angle[mcu_id];
                    slave_data_tmp.dut_dbfs = all_bf_data.dut.dbfs[mcu_id];
                    slave_data_tmp.check_sum = checksum((uint8_t*)((uint32_t)&slave_data_tmp+2), SLAVE_DATA_LENGTH);              
                
                    gpiohs_set_pin(SPI_SLAVE_MCU1_READY_IO, GPIO_PV_LOW);
                    usleep(2);
                    gpiohs_set_pin(SPI_SLAVE_MCU1_READY_IO, GPIO_PV_HIGH);
                }
                else if (MCU2 == mcu_id)
                {
                    slave_data_tmp.BF_STATE  = all_bf_data.BF_STATE[mcu_id];
                    slave_data_tmp.padding_0 = 0x00;
	                slave_data_tmp.padding_1 = 0x00;
                    slave_data_tmp.azm_angle = all_bf_data.azm.angle[mcu_id];
                    slave_data_tmp.azm_dbfs  = all_bf_data.azm.dbfs[mcu_id];
                    slave_data_tmp.inc_angle = all_bf_data.inc.angle[mcu_id];
                    slave_data_tmp.inc_dbfs = all_bf_data.inc.dbfs[mcu_id];
                    slave_data_tmp.dut_angle = all_bf_data.dut.angle[mcu_id];
                    slave_data_tmp.dut_dbfs = all_bf_data.dut.dbfs[mcu_id];
                    slave_data_tmp.check_sum = checksum((uint8_t*)((uint32_t)&slave_data_tmp+2), SLAVE_DATA_LENGTH);              
                                
                    gpiohs_set_pin(SPI_SLAVE_MCU2_READY_IO, GPIO_PV_LOW);
                    usleep(2);
                    gpiohs_set_pin(SPI_SLAVE_MCU2_READY_IO, GPIO_PV_HIGH);
                }     
                // usleep(5000); // little delay needed to prevent core crash 
                // We will use the angle result from here to set the azimuthal direction for the INC beamformer
            }
        }
        AZM_INC_BF_Duty_Cycle_counter += 1;

        if (AZM_INC_BF_Duty_Cycle_counter % AZM_INC_BF_Duty_Cycle == 0) // set delays to incident configuration... correct to do outside of loop
        {
            Angle_BF_INC_auto = BF[AZM].angle_local;
            apu_set_delays_incident(I2S_FS_actual, UCA8_RADIUS_CM, UCA_N, Angle_BF_INC_auto_EN ? Angle_BF_INC_auto : Angle_BF_INC_static);
            apu_get_delays(BF[INC].delays); 
        }

/************************************************************************************
 ** Now we have all AZM & INC angle data available for each MCU but only the most 
 ** recent values. To do any further analysis re grouping or stability we need to 
 ** calculate running statistics. Best way would be to use original vectors but
 ** that will be a complicated process involving a new mcu_bf_aggregator module running 
 ** on core0, which is more lightly loaded, as well as SPI channels (also running on 
 ** core 0) between MCU0-MCU1 & MCU0-MCU2 for data sharing. 
 ** 
 ** Complication... Refer to note SEH#1...    
 **
 *************************************************************************************/

        if (Enable_I2S_Data_Capture)
        {
#if (I2S_DMA)
            i2s_data_t data_0 = (i2s_data_t){
                .rx_channel = DMAC_CHANNEL0,
                .rx_buf = &i2s_rx_buf[0][0],
                .rx_len = FRAME_LEN * 2,
                .transfer_mode = I2S_RECEIVE,
            };

            i2s_data_t data_1 = (i2s_data_t){
                .rx_channel = DMAC_CHANNEL1,
                .rx_buf = &i2s_rx_buf[1][0],
                .rx_len = FRAME_LEN * 2,
                .transfer_mode = I2S_RECEIVE,
            };

            i2s_data_t data_2 = (i2s_data_t){
                .rx_channel = DMAC_CHANNEL2,
                .rx_buf = &i2s_rx_buf[2][0],
                .rx_len = FRAME_LEN * 2,
                .transfer_mode = I2S_RECEIVE,
            };

            i2s_data_t data_3 = (i2s_data_t){
                .rx_channel = DMAC_CHANNEL3,
                .rx_buf = &i2s_rx_buf[3][0],
                .rx_len = FRAME_LEN * 2,
                .transfer_mode = I2S_RECEIVE,
            };

            // split the 32-bit data into two 16-bit
            while (i2s_get_dma_divide_16(I2S_DEVICE_0) == 0)
            {
                i2s_set_dma_divide_16(I2S_DEVICE_0, 1);
            }

            i2s_handle_data_dma(I2S_DEVICE_0, data_0, NULL);
            //i2s_set_dma_divide_16(I2S_DEVICE_0, 1);
            while (i2s_get_dma_divide_16(I2S_DEVICE_0) == 0)
            {
                i2s_set_dma_divide_16(I2S_DEVICE_0, 1);
            }
            i2s_handle_data_dma(I2S_DEVICE_0, data_1, NULL);
            //i2s_set_dma_divide_16(I2S_DEVICE_0, 1);
            while (i2s_get_dma_divide_16(I2S_DEVICE_0) == 0)
            {
                i2s_set_dma_divide_16(I2S_DEVICE_0, 1);
            }
            i2s_handle_data_dma(I2S_DEVICE_0, data_2, NULL);
            //i2s_set_dma_divide_16(I2S_DEVICE_0, 1);
            while (i2s_get_dma_divide_16(I2S_DEVICE_0) == 0)
            {
                i2s_set_dma_divide_16(I2S_DEVICE_0, 1);
            }
            i2s_handle_data_dma(I2S_DEVICE_0, data_3, NULL);

            for (size_t i = 0; i < APU_DIR_CHANNEL_SIZE; i++)
            {
                for (size_t j = 0; j < 4; j++)
                {
                    I2s_Data[2 * j][i] = (int16_t)i2s_rx_buf[j][2 * i];
                    I2s_Data[2 * j + 1][i] = (int16_t)i2s_rx_buf[j][2 * i + 1];
                }
            }
#else
            // We need a new receive function that cycles through each channel grabbing each one in turn...
            // need to use 2 paramaters to manage buffer... max depth is fixed on compile (APU_DIR_CHANNEL_SIZE + APU_DIR_CHANNEL_PADDING)...
            // then need to loop using variable depth and padding values

            size_t padding = (Enable_i2s_rx_buff_padding ? APU_DIR_CHANNEL_PADDING : 0);
            size_t full_buf_len = APU_DIR_CHANNEL_SIZE + APU_DIR_CHANNEL_PADDING;

            // Rx FIFO has stale data in it if left running while idle... flush here or perhaps always in the i2s_receive_data func's
            if (Reset_i2s_rx_fifo_before_use)
                i2s_reset_rx_fifo(I2S_DEVICE_0);

            if (Enable_i2s_receive_data_all)
                i2s_receive_data_all(I2S_DEVICE_0, &rx_buf[0][0], full_buf_len, I2S_Rx_Buff_Depth + padding);
            else
            {
                i2s_receive_data(I2S_DEVICE_0, I2S_CHANNEL_0, &rx_buf[0][0], I2S_Rx_Buff_Depth + padding);
                i2s_receive_data(I2S_DEVICE_0, I2S_CHANNEL_1, &rx_buf[1][0], I2S_Rx_Buff_Depth + padding);
                i2s_receive_data(I2S_DEVICE_0, I2S_CHANNEL_2, &rx_buf[2][0], I2S_Rx_Buff_Depth + padding);
                i2s_receive_data(I2S_DEVICE_0, I2S_CHANNEL_3, &rx_buf[3][0], I2S_Rx_Buff_Depth + padding);
            }
#endif
            for (size_t i = 0; i < APU_DIR_CHANNEL_SIZE; i++)
            {

                // if read directly from data buffer, the lower 32-bit data is for the left channel,
                // and the higher 32-bit data is for the right channel
                // Does not seem to be true based on the code
                //        buf[buf_len*(channel_num) + i] = readl(&i2s[device_num]->channel[channel_num].left_rxtx);
                //        buf[buf_len*(channel_num) + i] <<= 32;
                //        buf[buf_len*(channel_num) + i++] |= readl(&i2s[device_num]->channel[channel_num].right_rxtx);

                size_t discarded_samples = (Enable_i2s_rx_buff_padding ? APU_DIR_CHANNEL_PADDING - 32 : 0);
                for (size_t j = 0; j < 4; j++)
                {
                    // Optionally discard the first 96 data samples due to observed discontinuity at 64th data point... need to review this requirement
                    I2s_Data[2 * j + 1][i] = (int16_t)(rx_buf[j][i + discarded_samples]);
                    I2s_Data[2 * j][i] = (int16_t)(rx_buf[j][i + discarded_samples] >> 32);
                    //        I2s_Data[2 * j][i]     = (int16_t)(rx_buf[j][i + 0]);
                    //        I2s_Data[2 * j + 1][i] = (int16_t)(rx_buf[j][i + 0] >> 32);

                    /* HOW TO DETERMINE TRIGGER POSITION... 
                    
                    Refer to S/S ".\resources\I2S_Trigger_Position.xlsx"
                    
                    The S/S determines when the data sample is either at or has crossed the point when I2S[#]==0 (sign change)
                    If the sample is not zero then the crossing occurred from 0%-100% of the way from the previous point to this one so it calculates the %
                    This % can be used to delay the sample set for plotting so the new trace more closely aligns with the previous sample set.
                    
                    Column "Zero_X" in table "I2S_Data" has a formula which indicates that the current data point satisfies the zero crossing criteria...
                        =IF(ISNUMBER(OFFSET([@[I2S'[0']]],-1,0)),IF(OFFSET([@[I2S'[0']]],-1,0)<0,IF([@[I2S'[0']]]>=0,1,0),0),0) 
                        // the ISNUMBER function makes the formula work for the 1st row of data and is irrelevant in C code
                        // The OFFSET function returns the value in the cell that is a certain number of rows or cells away from the referenced cell.. in this case "-1,0" == one row up
                    
                    Column "Zero_X%" then uses the Zero_X flag to calculate the % of the sample time the actual zero crossing actually occurred...
                        =IF([@[Zero_X]],(0-OFFSET([@[I2S'[0']]],-1,0))/([@[I2S'[0']]]-OFFSET([@[I2S'[0']]],-1,0)),"") // formula for calculating y-intercept of a line segment
                    
                    These can be combined in code but it's easier to manage with separate calculations
                    
                    For now we are handling every bit of I2S data in this next inner loop (later only process as many points as we want to work with)
                    
                    As we move through the data we can do these intermediate calculations and determine the crossing points (Trigger_PT_current) 
                    and % values Trigger_PT_current_pct (there will be multiple crossings for normal signals).

                    We need to pick just one trigger point and the logical choice is the crossing that is closest to the midpoint in the data set.
                    For example: With N (even) samples we test as follows: 
                        if (abs(Trigger_PT_current-N/2)<abs(Trigger_PT-N/2)) { // test for lesser distance from current point to mid-point of sample set
                            Trigger_PT=Trigger_PT_current
                            Trigger_PT_pct=Trigger_PT_current_pct
                        }

                    We would also need the ability to specify any one mic as the trigger channel (Trigger_CH)... do with JSON command writing to var

                    The Trigger_CH, Trigger_PT (when Zero_X==true) and Trigger_PT_pct values would be included in a header record with each data block transfer of N data points.

                    We then plot starting at the 1st point but with an X-axis offset determined by Trigger_PT_pct.
                    
                    If you think how this might work in our real-time oscilloscope (RTO) then as we change the number of sample points (N) the chart 
                    will either expand or contract around the center of the plot area because there are either fewer or more points to display. 
                    
                    This would also happen with a change in source frequency so decreasing the tone frequency from 20kHz - 18kHz will
                    cause the traces to expand and increasing the frequency will cause contraction.
                    
               */
                }
            }
        }

        if (Enable_I2S_Hard_FFT || Enable_I2S_Soft_FFT)
        {
            for (size_t j = 0; j < 8; j++)
                calculate_fft(&I2s_Data[j][0], &I2s_Hard_FFT_Power_Data[j][0], &I2s_Soft_FFT_Power_Data[j][0]);

            // Calculate VRMS using FFT results (soft for now but we can speed it all up by only using hard FFT)
            VRMS_BP = 0; 

            for (size_t i = BP_LOW_N; i < BP_HIGH_N; i++) // summing over bandpass range only
            {
                for (size_t j = 0; j < 8; j++) // and over all 8 mics
                {
                    if (Enable_I2S_Hard_FFT) 
                        VRMS_BP = VRMS_BP + (I2s_Hard_FFT_Power_Data[j][i] * I2s_Hard_FFT_Power_Data[j][i]);   // computing sum of squares of H/W FFT data if available 
                    else 
                        VRMS_BP = VRMS_BP + (I2s_Soft_FFT_Power_Data[j][i] * I2s_Soft_FFT_Power_Data[j][i]);   // otherwise with S/W data 
                }
            }
            VRMS_BP = sqrt(2 * VRMS_BP / (8 * APU_DIR_CHANNEL_SIZE));   // only summing over subset of data but total number is the same... 2* is necessary for FFT use
            DBFS_BP = VRMS_BP == 0 ? MINUS_INF : (double)20 * log10(VRMS_BP / (1 << 15));      // data is 16 but signed so 15 bits for magnitude
        }
  
        // BF Detection State Mealy FSM
      
        // General inputs
        t_us_current = sysctl_get_time_us();
        Noise_Floor = MIN_MIC_DB + NF_Adjust;
        // looks like we should consider a min sig strength for 

        // FSM INPUTS (9)
        _8_Min_Detect  = DBFS >= Noise_Floor;
        
        // these are all mathy inputs
        _7_AZM_Angle_Stable  = BF[AZM].is_Stable;
        _6_AZM_Angle_Rot_Pos = BF[AZM].is_Rot_Pos;
        _5_AZM_Angle_Rot_Neg = BF[AZM].is_Rot_Neg;
        _4_INC_Angle_Stable  = BF[INC].is_Stable;
        _3_INC_Angle_Rot_Pos = BF[INC].is_Rot_Pos;
        _2_INC_Angle_Rot_Neg = BF[INC].is_Rot_Neg;
        
        // IN_FOV hysteresis calculation... glitching... copuld use a delay register to filter out spurious BF results but that creates lag... could also rely on _8_Min_Detect to get us in
        IN_FOV_s[2] = IN_FOV_s[1];
        IN_FOV_s[1] = IN_FOV_s[0];
        if(!_1_IN_FOV)
            IN_FOV_s[0] = BF[INC].angle < (IN_FOV_Angle_LVL / 2 - Angle_Hysteresis / 2) ? true : false;
        else
            IN_FOV_s[0] = BF[INC].angle > (IN_FOV_Angle_LVL / 2 + Angle_Hysteresis / 2) ? false : _1_IN_FOV;
        //_1_IN_FOV = IN_FOV_s[0] && IN_FOV_s[1] && IN_FOV_s[2];
        _1_IN_FOV = IN_FOV_s[0];
            
        
        // ON_TARGET hysteresis calculation
        ON_TARGET_s[2] = ON_TARGET_s[1];
        ON_TARGET_s[1] = ON_TARGET_s[0];
        if(!_0_ON_TARGET) 
            ON_TARGET_s[0] = BF[INC].angle < (ON_TARGET_Angle_LVL / 2 - Angle_Hysteresis / 2) ? true : false;
        else
            ON_TARGET_s[0] = BF[INC].angle > (ON_TARGET_Angle_LVL / 2 + Angle_Hysteresis / 2) ? false : _0_ON_TARGET;
        //_0_ON_TARGET = ON_TARGET_s[0] && ON_TARGET_s[1] && ON_TARGET_s[2];
        _0_ON_TARGET = ON_TARGET_s[0];
            
        
        // FSM OUTPUTS (1)
        
        switch(BF_FSM_STATE[mcu_id])
        {
            case S_BF_IDLE :
                if (Force_IDLE)
                    BF_FSM_STATE[mcu_id] = S_BF_IDLE;
                else if (_8_Min_Detect) //  
                {
                    BF_FSM_STATE[mcu_id] = S_BF_HEARD;
                }
                break;

            case S_BF_HEARD :
                if (Force_IDLE)
                    BF_FSM_STATE[mcu_id] = S_BF_IDLE;
                else if (_8_Min_Detect && (_7_AZM_Angle_Stable || _4_INC_Angle_Stable))
                {
                    BF_FSM_STATE[mcu_id] = S_BF_SEEN;
                }
                else if (!(_8_Min_Detect))
                {
                    BF_FSM_STATE[mcu_id] = S_BF_IDLE;
                }
                break;

            case S_BF_SEEN :
                if (Force_IDLE)
                    BF_FSM_STATE[mcu_id] = S_BF_IDLE;
                else if (_8_Min_Detect && (_7_AZM_Angle_Stable && _4_INC_Angle_Stable)) // need some way to allow angles to go unstable and still consider it stable... maybe rotation
                {
                    BF_FSM_STATE[mcu_id] = S_BF_STABLE;
                }
                else if (!(_8_Min_Detect && (_7_AZM_Angle_Stable || _4_INC_Angle_Stable)))
                {
                    BF_FSM_STATE[mcu_id] = S_BF_HEARD;
                }
                break;

            case S_BF_STABLE : // may need to hold here once we get in...
                if (Force_IDLE)
                    BF_FSM_STATE[mcu_id] = S_BF_IDLE;
                else if(_8_Min_Detect && _1_IN_FOV && _4_INC_Angle_Stable)
                {
                    BF_FSM_STATE[mcu_id] = S_BF_IN_VIEW;
                }
                else if (!(_8_Min_Detect && (_7_AZM_Angle_Stable && _4_INC_Angle_Stable)))
                {
                    BF_FSM_STATE[mcu_id] = S_BF_SEEN;
                }
                break;

            case S_BF_IN_VIEW :
                if (Force_IDLE)
                    BF_FSM_STATE[mcu_id] = S_BF_IDLE;
                else if (_8_Min_Detect && _1_IN_FOV && _0_ON_TARGET && _4_INC_Angle_Stable)
                {
                    BF_FSM_STATE[mcu_id] = S_BF_ON_TARGET;
                }
                else if (!(_8_Min_Detect && _1_IN_FOV))
                {
                    BF_FSM_STATE[mcu_id] = S_BF_STABLE;
                }
                break;

            case S_BF_ON_TARGET :
                if (Force_IDLE)
                    BF_FSM_STATE[mcu_id] = S_BF_IDLE;
                else if (!(_8_Min_Detect && _1_IN_FOV && _0_ON_TARGET))
                {
                    BF_FSM_STATE[mcu_id] = S_BF_IN_VIEW;
                }
                break;
            
            default:
                BF_FSM_STATE[mcu_id] = S_BF_IDLE; 
            }

        // BF FSM State change flag
        sprintf(bf_state_str_trim, "%s", trim(BF_STATE_str[BF_FSM_STATE[mcu_id]])); // FIXME
        if (BF_FSM_STATE[mcu_id] != BF_FSM_STATE_s)// && !BF_FSM_STATE_CHANGE) /// wait until previous state change has been acknowledged?... careful here!
        {
            BF_FSM_STATE_CHANGE = true;     // clear in mcu_device_management just like the flipped one also 
            BF_FSM_STATE_s = BF_FSM_STATE[mcu_id];   // realign versions
            BF_FSM_STATE_CHANGE_COUNT += 1;
        }


#ifdef DEBUG
//        if (Enable_BF_FSM_Debug && BF_FSM_STATE_CHANGE) // print once after entering state... may need additional statements inline
//        {
//            mcu_stdio_printf("MCU[%d], STATE[%d]: %s%-10s%s, DBFS_MPA[%d] = %.2f\n\r", 
//                mcu_id, BF_FSM_STATE_CHANGE_COUNT, BF_STATE_col[BF_FSM_STATE[mcu_id]], BF_STATE_str[BF_FSM_STATE[mcu_id]], KNRM, 
//                MPA_N, DBFS);
//        }
        if (Enable_BF_FSM_Debug)
        {
            if (!ClrScr_Done) 
            {
                ClrScr();
                ClrScr_Done = true;
                //msleep(500);
            }
            GoHome();
            mcu_stdio_printf("MCU[%d], STATE[%d]: %s%-10s%s, MPA_N = %d, AZM = % 6.1f, INC = % 5.1f, DBFS = % 6.2f\n\r", 
                mcu_id, BF_FSM_STATE_CHANGE_COUNT, BF_STATE_col[BF_FSM_STATE[mcu_id]], BF_STATE_str[BF_FSM_STATE[mcu_id]], KNRM, 
                MPA_N, BF[AZM].angle, BF[INC].angle, DBFS);
            
            printf("_8_Min_Detect               = %s\n", TF_string(_8_Min_Detect));
            printf("                                                             \n");
            printf("_7_AZM_Angle_Stable         = %s\n", TF_string(_7_AZM_Angle_Stable));
            printf("_6_AZM_Angle_Rot_Pos        = %s\n", TF_string(_6_AZM_Angle_Rot_Pos));
            printf("_5_AZM_Angle_Rot_Neg        = %s\n", TF_string(_5_AZM_Angle_Rot_Neg));
            printf("                                                             \n");
            printf("_4_INC_Angle_Stable         = %s\n", TF_string(_4_INC_Angle_Stable));
            printf("_3_INC_Angle_Rot_Pos        = %s\n", TF_string(_3_INC_Angle_Rot_Pos));
            printf("_2_INC_Angle_Rot_Neg        = %s\n", TF_string(_2_INC_Angle_Rot_Neg));
            printf("                                                       \n");
//            uint8_t in_fov_ang = (uint8_t)(IN_FOV_Angle_LVL / 2);
//            uint8_t on_target_ang = (uint8_t)(IN_FOV_Angle_LVL / 2);
//            uint8_t hys_ang= (uint8_t)(Angle_Hysteresis / 2);
//            printf("_0_ON_TARGET [%d +- %d] = %s\n", on_target_ang, hys_ang, TF_string(_0_ON_TARGET));
//            printf("_1_IN_FOV    [%d +- %d] = %s\n", in_fov_ang, hys_ang, TF_string(_1_IN_FOV));
            printf("_1_IN_FOV    [%4.1f +- %4.1f] = %s\n", (IN_FOV_Angle_LVL / 2)   , (Angle_Hysteresis / 2), TF_string(_1_IN_FOV));
            printf("_0_ON_TARGET [%4.1f +- %4.1f] = %s\n", (ON_TARGET_Angle_LVL / 2), (Angle_Hysteresis / 2), TF_string(_0_ON_TARGET));
            printf("                                                       \n");

            printf("BF[AZM].rotation_LT_LIM_minus_hys   = |%7.4f| < (%7.4f - %7.4f) = %d\n", BF[AZM].UVL.rotation, BF[AZM].rotation_LIM, BF[AZM].rotation_HYS, BF[AZM].rotation_LT_LIM_minus_hys);
            printf("BF[AZM].rotation_GT_LIM_plus_hys    = |%7.4f| > (%7.4f + %7.4f) = %d\n", BF[AZM].UVL.rotation, BF[AZM].rotation_LIM, BF[AZM].rotation_HYS, BF[AZM].rotation_GT_LIM_plus_hys);
            printf("BF[AZM].dispersion_LT_LIM_minus_hys = |%7.4f| < (%7.4f - %7.4f) = %d\n", BF[AZM].UVL.dispersion, BF[AZM].dispersion_LIM, BF[AZM].dispersion_HYS, BF[AZM].dispersion_LT_LIM_minus_hys);
            printf("BF[AZM].dispersion_GT_LIM_plus_hys  = |%7.4f| > (%7.4f + %7.4f) = %d\n", BF[AZM].UVL.dispersion, BF[AZM].dispersion_LIM, BF[AZM].dispersion_HYS, BF[AZM].dispersion_GT_LIM_plus_hys);
            printf("                                                                                       \n");
            printf("BF[INC].rotation_LT_LIM_minus_hys   = |%7.4f| < (%7.4f - %7.4f) = %d\n", BF[INC].UVL.rotation, BF[INC].rotation_LIM, BF[INC].rotation_HYS, BF[INC].rotation_LT_LIM_minus_hys);
            printf("BF[INC].rotation_GT_LIM_plus_hys    = |%7.4f| > (%7.4f + %7.4f) = %d\n", BF[INC].UVL.rotation, BF[INC].rotation_LIM, BF[INC].rotation_HYS, BF[INC].rotation_GT_LIM_plus_hys);
            printf("BF[INC].dispersion_LT_LIM_minus_hys = |%7.4f| < (%7.4f - %7.4f) = %d\n", BF[INC].UVL.dispersion, BF[INC].dispersion_LIM, BF[INC].dispersion_HYS, BF[INC].dispersion_LT_LIM_minus_hys);
            printf("BF[INC].dispersion_GT_LIM_plus_hys  = |%7.4f| > (%7.4f + %7.4f) = %d\n", BF[INC].UVL.dispersion, BF[INC].dispersion_LIM, BF[INC].dispersion_HYS, BF[INC].dispersion_GT_LIM_plus_hys);

            msleep(50);
        }         // °   ± ±
        
#endif
    
      
        // Rx FIFO has stale data in it if left running while idle... flush here or perhaps always in the i2s_receive_data func's
        if (Reset_i2s_rx_fifo_before_use)
        { // reset APU, wait briefly and flush Rx fifo's
            apu_dir_reset();
            usleep(1000);
            i2s_reset_rx_fifo(I2S_DEVICE_0); 
        }
        while (--dir_logic_count != 0)
        {
            mcu_stdio_printf("[warning]: %s, restart before prev callback has end\n\r", "dir_logic");
        }
        //apu_dir_enable(); // moved after while() loop to address random fault... might be best to have S/W reset in while loop.
        // apu_dir_reset();
        // usleep(1000);
        apu_voc_reset();
        // usleep(1000);
        // //apu_set_fft_shift_factor(1,0);
        // usleep(1000);
        apu_voc_enable(1);
        // better yet... run this as soon as all th buffers have been read!
    }

#if APU_VOC_ENABLE     
    if (voc_logic_count > 0)
    {
    
        (void)voc_logic();
        while (--voc_logic_count != 0)
        {
            //mcu_stdio_printf("[warning]: %s, restart before prev callback has end\n\r", "voc_logic");
        }        
        //apu_voc_enable(1);
        // apu_voc_reset();
        // usleep(1000);
        apu_dir_reset();
        // usleep(1000);
        // //apu_set_fft_shift_factor(0,0);
        // usleep(1000);
        apu_dir_enable();
    }        
#endif



}
  
  
 

/**********************************************************************************************************************
** Determine real-world direction vectors given orientation of each array in system
** https://blog.demofox.org/2014/12/27/using-imaginary-numbers-to-rotate-2d-vectors/
**
** Real-world direction vectors are determined by multiplying the local 
** vectors by the appropriate rotation vector (RV) as follows:
** 
** RV = cos(Ѳ) + sin(Ѳ)*I
**
** MCU = Offset angle = -90° (counter-clockwise)     : RV=cos(-90) + sin(-90)*I =      0 -   1*I
** MC1 = Offset angle = -90° + 120° =  30°(clockwise): RV=cos( 30) + sin( 30)*I =  0.866 + 0.5*I
** MC2 = Offset angle = -90° + 240° = 150°(clockwise): RV=cos(150) + sin(150)*I = -0.866 + 0.5*I
**                                                                                 
**********************************************************************************************************************/
complex double Azimuth_Local_to_Real_World_Rotation_Vector()
{
    float theta = 0.0;
    switch(mcu_uart_dev)
    {
        case MCU0:
            theta = -90.0;
        break;
        case MCU1:
            theta = 30.0;
            break;
        case MCU2:
            theta = 150.0;
        break;
        default:
            mcu_stdio_printf("Bad value for mcu_id = %d\n\r", mcu_uart_dev);
            break;
    }
    theta = theta * 2 * M_PI / 360.0;  // in radians
    complex double RV = cos(theta) + sin(theta)*I;
    return RV;
}

void build_Unit_DIR_Vectors(BF_enum which, Unit_DIR_Vectors_s UDV, float Angle_AZM, float Angle_INC) 
{ 
    switch (which) 
    {         
    case AZM: 
        for (int dir = 0; dir < 16; dir++) 
        { 
            float angle = (((float)((dir + 8) % 16)) / 16 * 360 - 180.0); 
            float rads = angle * (M_PI / 180.0); 
            UDV.V[dir] = cos(rads) + I * sin(rads); 
        } 
        break; 
    case INC: 
        for (int dir = 0; dir < 16; dir++) 
        { 
            float angle = 90 - (((float)(dir / 16)) * 180.0); 
            float rads = angle * (M_PI / 180.0); 
            UDV.V[dir] = cos(rads) + I * sin(rads); 
        } 
        break; 
    default: 
        mcu_stdio_printf("Bad value for beamformer identifier = %d\n\r", (uint8_t)which); 
        break; 
     
    } 
    for (int i = 1; i < 16; i++) 
    { 
        printf("%s: UDV[%d] = %f + I * %f\n", which == AZM ? "AZM" : "INC", i, creal(UDV.V[i]), cimag(UDV.V[i])); 
    } 
         
}    


uint8_t mic_array_init(mic_callback callback)
{
    clear_csr(mie, MIP_MEIP);
    init_all();
    set_csr(mie, MIP_MEIP);
    set_csr(mstatus, MSTATUS_MIE);
    s_context.callback = callback; // s_context includes data
    
    // configure BF sensitivity controls... magic numbers for now but should be a calibrattion step (automatic or manual)
    BF[AZM].rotation_LIM   = 0.1;
    BF[AZM].rotation_HYS   = 0.05;
    BF[AZM].dispersion_LIM = 0.6;
    BF[AZM].dispersion_HYS = 0.3;

    BF[INC].rotation_LIM   = 0.02;
    BF[INC].rotation_HYS   = 0.01;
    BF[INC].dispersion_LIM = 0.1;
    BF[INC].dispersion_HYS = 0.05;

    // apu_print_setting();
    // apu_dir_enable();
    //Azimuth_Local_to_Real_World_Rotation_Vector(); // configure for azimuth by default

    // Testing indicates that slaves should update at 50% of master (MCU0) rate to balance JSON streams.... can be modified as needed at any time
    if (mcu_uart_dev == MCU0)
        Host_Update_Interval = HOST_UPDATE_INTERVAL;
    else
        Host_Update_Interval = (uint8_t)(HOST_UPDATE_INTERVAL*1.75);
    
  return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void mic_array_loop(void)
{
  // mcu_stdio_printf("%s,%d\n\r", __func__,__LINE__);
    if(Terminal_Mode == true) // user wants terminal back
      return;
    
    event_loop();
}

//////////////////////////////////////////////////////////////////////////////////
// max_channel:     maximum number of channels
// channel_size:    maximum number of data in one channel
// data_size:       = 1 if data is 8-bit, 2 if data is 16-bit, 4 if data is 32-bit
// start_sample:    index of first data to be extracted in a channel
// end_sample:      index of the last data to be extracted from a channel
// channel_mask:    bit-map to indicate which channel's data to be extracted
//////////////////////////////////////////////////////////////////////////////////
void send_structure_block_data(uint8_t mcu, uint8_t format_type, void* piData, uint8_t max_channel, uint16_t channel_size,
                          uint8_t data_size, uint16_t start_sample, uint16_t end_sample, uint16_t channel_mask)
{
    uint16_t sample_length = end_sample - start_sample + 1;
    uint16_t total_bytes = 1;
    uint8_t active_channel_num = 0;
    uint16_t k = 0;
    _data_header_s head_data = {
        .head = 0x11,
        .version = 0x02,
        .type = format_type,
    };
    head_data.type = head_data.type << 8;

    for (k = 0; k < max_channel; k++)
    {
        if (((channel_mask >> k) & 0x01) != 0)
        {
            active_channel_num++;
        }
    }

    total_bytes = sample_length * active_channel_num * data_size;
    uint8_t *pTemp = malloc(total_bytes * sizeof(uint8_t));
    //int16_t *pData = (int16_t *)(piData);

    if (mcu < MCU3)
    {
        if (data_size == 1)  // for 8-bit data
        {
            for (uint16_t i = 0; i < sample_length; i++)
            {
                pTemp[i] = *((uint8_t *)piData + i + start_sample - 1) & 0xff;
            }
        }
        else if (data_size == 2)  // for 16-bit data
        {
            //printf("channel mask is %d\r\n", channel_mask);
            k = 0;
            for (uint16_t j = 0; j < max_channel; j++)
            {
                if (((channel_mask >> j) & 0x01) != 0)
                {
                    for (uint16_t i = 0; i < sample_length; i++)
                    {
                        pTemp[k * sample_length * 2 + 2 * i + 0] = (uint8_t)( *((int16_t *)piData + j * channel_size + i + start_sample - 1) & 0xFF);
                        pTemp[k * sample_length * 2 + 2 * i + 1] = (uint8_t)((*((int16_t *)piData + j * channel_size + i + start_sample - 1) >> 8) & 0xFF);
                    
                    }
                    k++;
                }
            }
        }
        else if (data_size == 4)  // for 32-bit data
        {
            k = 0;
            for (uint16_t j = 0; j < max_channel; j++)
            {
                if (((channel_mask >> j) & 0x01) != 0)
                {
                    for (uint16_t i = 0; i < sample_length; i++)
                    {
                        pTemp[k * sample_length * 4 + 4 * i + 0] = (uint8_t)( *((uint32_t*)piData + j * channel_size + i + start_sample - 1) & 0xFF);
                        pTemp[k * sample_length * 4 + 4 * i + 1] = (uint8_t)((*((uint32_t*)piData + j * channel_size + i + start_sample - 1) >> 8) & 0xFF);
                        pTemp[k * sample_length * 4 + 4 * i + 2] = (uint8_t)((*((uint32_t*)piData + j * channel_size + i + start_sample - 1) >> 16) & 0xFF);
                        pTemp[k * sample_length * 4 + 4 * i + 3] = (uint8_t)((*((uint32_t*)piData + j * channel_size + i + start_sample - 1) >> 24) & 0xFF);
                    }
                    k++;
                }
            }
        }

        send_raw_bytes(mcu, pTemp, total_bytes, head_data);
    }
    usleep(20000);
    free(pTemp);
}



void send_i2s_raw_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample, uint16_t channel_mask)
{
#if (!BLOCK_DATA_JSON_FORMAT)
    uint8_t max_channel = sizeof(I2s_Data) / sizeof(I2s_Data[0]);
    uint8_t data_size = sizeof(I2s_Data[0][0]);    
    uint16_t channel_size = sizeof(I2s_Data[0]) / sizeof(I2s_Data[0][0]);
    
    send_structure_block_data(mcu, COMM_MESSAGE_FORMAT_TYPE_L_I2S, 
                              (void*)&I2s_Data[0][0], max_channel, channel_size, 
                              data_size, start_sample, end_sample, channel_mask);
#else
    _data_header_s head_data = {
        .head = 0x11,
        .version = 0x02,
        .type = 0x0101,
    };
    uint16_t j = 0;
    uint16_t i = 0;
    uint8_t max_channel = sizeof(I2s_Data) / sizeof(I2s_Data[0]);
    uint8_t active_channel_num = 0;

    char str[22528];
    char temp[24];
    for (j = 0; j < max_channel; j++)
    {
        if (((channel_mask >> j) & 0x01) != 0)
        {
            active_channel_num++;
        }
    }
    //printf("channel mask is %x\r\n", channel_mask);

    sprintf(str, "{\"MCU\":%d, \"TYPE\":\"I2S_RAW\", \"DATA\":[", mcu);
    for (j = 0; j < max_channel; j++)
    {
        if (((channel_mask >> j) & 0x01) != 0)
        {
            for (i = (start_sample-1); i < end_sample; i++)
            {
                if (i == (start_sample-1))
                {
                    sprintf(temp, "[%d,", I2s_Data[j][i]);
                }
                else if (i < (end_sample - 1))
                {
                    sprintf(temp, "%d,", I2s_Data[j][i]);
                }
                else if (i == (end_sample - 1) && (j != (max_channel - 1)))
                {
                    sprintf(temp, "%d],", I2s_Data[j][i]);
                }
                else if (i == (end_sample - 1) && (j == (max_channel - 1)))
                {
                    sprintf(temp, "%d]]}", I2s_Data[j][i]);
                }
                strcat(str, temp);
            }
        }
        else
        {
            if (j != (max_channel - 1))
            {
                sprintf(temp, "[],");
            }
            else
            {
                sprintf(temp, "[]]}");
            }
            strcat(str, temp);
        }
    }
    
    //printf("%s\n", str);
    send_string(mcu, str, head_data);
 #endif
 
  
  
}


void send_i2s_fft_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample, uint16_t channel_mask)
{
    
#if (!BLOCK_DATA_JSON_FORMAT)
    uint8_t max_channel = sizeof(I2s_Hard_FFT_Power_Data) / sizeof(I2s_Hard_FFT_Power_Data[0]);
    uint8_t data_size = sizeof(I2s_Hard_FFT_Power_Data[0][0]);   
    uint16_t channel_size = sizeof(I2s_Hard_FFT_Power_Data[0]) / sizeof(I2s_Hard_FFT_Power_Data[0][0]);
    
    send_structure_block_data(mcu, COMM_MESSAGE_FORMAT_TYPE_L_I2S_FFT, 
                              (void*)&I2s_Hard_FFT_Power_Data[0][0], max_channel, 
                              channel_size, data_size, 
                              start_sample, end_sample, channel_mask);
#else
    _data_header_s head_data = {
        .head = 0x11,
        .version = 0x02,
        .type = 0x0101,
    };
    uint16_t j = 0;
    uint16_t i = 0;
    uint8_t max_channel = sizeof(I2s_Hard_FFT_Power_Data) / sizeof(I2s_Hard_FFT_Power_Data[0]);
    uint8_t active_channel_num = 0;

    char str[22528];
    char temp[24];
    for (j = 0; j < max_channel; j++)
    {
        if (((channel_mask >> j) & 0x01) != 0)
        {
            active_channel_num++;
        }
    }

    sprintf(str, "{\"MCU\":%d, \"TYPE\":\"I2S_FFT\", \"DATA\":[", mcu);
    for (j = 0; j < max_channel; j++)
    {
        if (((channel_mask >> j) & 0x01) != 0)
        {
            for (i = (start_sample-1); i < end_sample; i++)
            {
                if (i == (start_sample-1))
                {
                    sprintf(temp, "[%d,", I2s_Hard_FFT_Power_Data[j][i]);
                }
                else if (i < (end_sample - 1))
                {
                    sprintf(temp, "%d,", I2s_Hard_FFT_Power_Data[j][i]);
                }
                else if (i == (end_sample - 1) && (j != (max_channel - 1)))
                {
                    sprintf(temp, "%d],", I2s_Hard_FFT_Power_Data[j][i]);
                }
                else if (i == (end_sample - 1) && (j == (max_channel - 1)))
                {
                    sprintf(temp, "%d]]}", I2s_Hard_FFT_Power_Data[j][i]);
                }
                strcat(str, temp);
            }
        }
        else
        {
            if (j != (max_channel - 1))
            {
                sprintf(temp, "[],");
            }
            else
            {
                sprintf(temp, "[]]}");
            }
            strcat(str, temp);
        }
    }
    
    //printf("%s\n", str);
    send_string(mcu, str, head_data);


#endif
}


void send_i2s_raw_and_fft_data(uint8_t mcu)
{
    uint8_t strTemp[12288];
    int16_t i, j;
    _data_header_s head_data_raw_fft = {
        .head = 0x11,
        .version = 0x02,
        .type = 0x0800,
    };
    
    if (mcu < MCU3)
    {
        //strTemp[0] = channel;
        for (j = 0; j < 16; j++)
        {
            if (j < 8)
            {
                // raw data
                for (i = 0; i < 512; i++)
                {
                  strTemp[j*1024 + 2 * i ] = I2s_Data[j][i] & 0xff;
                  strTemp[j*1024 + 2 * i + 1] = (I2s_Data[j][i] >> 8) & 0xff;                  
                }
            }
            else
            {
                // fft data
                for (i = 0; i < 256; i++)
                {
                    if (Enable_I2S_Hard_FFT)
                    {
                      strTemp[8192 + (j-8) * 512 + 2 * i] = I2s_Hard_FFT_Power_Data[j-8][i] & 0xff;
                      strTemp[8192 + (j-8) * 512 + 2 * i + 1] = (I2s_Hard_FFT_Power_Data[j-8][i] >> 8) & 0xff;
                    }
                    else if (Enable_I2S_Soft_FFT)
                    {
                      strTemp[8192 + (j-8) * 512 + 2 * i] = I2s_Soft_FFT_Power_Data[j-8][i] & 0xff;
                      strTemp[8192 + (j-8) * 512 + 2 * i + 1] = (I2s_Soft_FFT_Power_Data[j-8][i] >> 8) & 0xff;
                    }
                }
            }
          
        }
        send_raw_bytes(mcu, strTemp, 12288, head_data_raw_fft);
    }

}



void send_dir_raw_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample, uint16_t channel_mask)
{
#if (!BLOCK_DATA_JSON_FORMAT)
    uint8_t max_channel = sizeof(APU_DIR_BUFFER) / sizeof(APU_DIR_BUFFER[0]);
    uint8_t data_size = sizeof(APU_DIR_BUFFER[0][0]);   
    uint16_t channel_size = sizeof(APU_DIR_BUFFER[0]) / sizeof(APU_DIR_BUFFER[0][0]);
    
    send_structure_block_data(mcu, COMM_MESSAGE_FORMAT_TYPE_L_DIR, 
                              (void*)&APU_DIR_BUFFER[0][0], max_channel, 
                              channel_size, data_size, 
                              start_sample, end_sample, channel_mask);
#else
    _data_header_s head_data = {
        .head = 0x11,
        .version = 0x02,
        .type = 0x0101,
    };
    uint16_t j = 0;
    uint16_t i = 0;
    uint8_t max_channel = sizeof(APU_DIR_BUFFER) / sizeof(APU_DIR_BUFFER[0]);
    uint8_t active_channel_num = 0;

    char str[22528];
    char temp[24];
    for (j = 0; j < max_channel; j++)
    {
        if (((channel_mask >> j) & 0x01) != 0)
        {
            active_channel_num++;
        }
    }

    sprintf(str, "{\"MCU\":%d, \"TYPE\":\"DIR_RAW\", \"DATA\":[", mcu);
    for (j = 0; j < max_channel; j++)
    {
        if (((channel_mask >> j) & 0x01) != 0)
        {
            for (i = (start_sample-1); i < end_sample; i++)
            {
                if (i == (start_sample-1))
                {
                    sprintf(temp, "[%d,", APU_DIR_BUFFER[j][i]);
                }
                else if (i < (end_sample - 1))
                {
                    sprintf(temp, "%d,", APU_DIR_BUFFER[j][i]);
                }
                else if (i == (end_sample - 1) && (j != (max_channel - 1)))
                {
                    sprintf(temp, "%d],", APU_DIR_BUFFER[j][i]);
                }
                else if (i == (end_sample - 1) && (j == (max_channel - 1)))
                {
                    sprintf(temp, "%d]]}", APU_DIR_BUFFER[j][i]);
                }
                strcat(str, temp);
            }
        }
        else
        {
            if (j != (max_channel - 1))
            {
                sprintf(temp, "[],");
            }
            else
            {
                sprintf(temp, "[]]}");
            }
            strcat(str, temp);
        }
    }
    //printf("%s\n", str);
    send_string(mcu, str, head_data);
#endif
    
}

void send_voc_raw_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample)
{
#if (!BLOCK_DATA_JSON_FORMAT)
    uint8_t max_channel = 1;
    uint8_t data_size = sizeof(APU_VOC_BUFFER[0]);   
    uint16_t channel_size = sizeof(APU_VOC_BUFFER) / sizeof(APU_VOC_BUFFER[0]);
    
    send_structure_block_data(mcu, COMM_MESSAGE_FORMAT_TYPE_L_VOC, 
                              (void*)&APU_VOC_BUFFER[0], max_channel, 
                              channel_size, data_size, 
                              start_sample, end_sample, 1);
#else
    _data_header_s head_data = {
        .head = 0x11,
        .version = 0x02,
        .type = 0x0101,
    };
    char str[22528];
    char temp[24];
    sprintf(str, "{\"MCU\":%d, \"TYPE\":\"VOC_RAW\", \"DATA\":[", mcu);
    for (uint32_t i = (start_sample - 1); i < end_sample; i++)
    {        
        if (i < (end_sample - 1))
        {
            sprintf(temp, "%d,", APU_VOC_BUFFER[i]);
        }
        else if (i == (end_sample - 1))
        {
            sprintf(temp, "%d]}", APU_VOC_BUFFER[i]);
        }
        strcat(str, temp);
    }
    send_string(mcu, str, head_data);
#endif

}

void send_voc_fft_data(uint8_t mcu, uint16_t start_sample, uint16_t end_sample)
{

#if (!BLOCK_DATA_JSON_FORMAT)
    // uint8_t max_channel = 1;
    // uint8_t data_size = sizeof(APU_VOC_FFT_BUFFER[0]);   
    // uint16_t channel_size = sizeof(APU_VOC_FFT_BUFFER) / sizeof(APU_VOC_FFT_BUFFER[0]);
    
    // send_structure_block_data(mcu, COMM_MESSAGE_FORMAT_TYPE_L_VOC_FFT, 
    //                           (void*)&APU_VOC_FFT_BUFFER[0], max_channel, 
    //                           channel_size, data_size, 
    //                           start_sample, end_sample, 1);

    uint8_t max_channel = 1;
    uint8_t data_size = sizeof(VOC_FFT_Data[0]);   
    uint16_t channel_size = sizeof(VOC_FFT_Data) / sizeof(VOC_FFT_Data[0]);
    
    send_structure_block_data(mcu, COMM_MESSAGE_FORMAT_TYPE_L_VOC_FFT, 
                              (void*)&VOC_FFT_Data[0], max_channel, 
                              channel_size, data_size, 
                              start_sample, end_sample, 1);
                              
#else
    _data_header_s head_data = {
        .head = 0x11,
        .version = 0x02,
        .type = 0x0101,
    };
    char str[10240];
    char temp[16];
    sprintf(str, "{\"MCU\":%d, \"TYPE\":\"VOC_FFT\", \"DATA\":[", mcu);
    for (uint32_t i = (start_sample - 1); i < end_sample; i++)
    {        
        if (i < (end_sample - 1))
        {
            sprintf(temp, "%d,", VOC_FFT_Data[i]);
        }
        else if (i == (end_sample - 1))
        {
            sprintf(temp, "%d]}", VOC_FFT_Data[i]);
        }
        strcat(str, temp);
    }
    send_string(mcu, str, head_data);
#endif


}

#endif