#ifndef __mcu_UTIL_QUEUE_H__
#define __mcu_UTIL_QUEUE_H__

typedef struct __mcu_util_queue_{
    size_t init_size;
    void *elems;
    size_t elem_size;
    volatile size_t count;
    size_t size;
    size_t pos_head;
    size_t pos_tail;
    void (*free_callback)(void *);
} _mcu_util_queue_s;

void mcu_util_queue_init(_mcu_util_queue_s *s, size_t init_size, size_t elem_size, void (*free_callback)(void *));
void mcu_util_queue_close(_mcu_util_queue_s *s);
void mcu_util_queue_enque(_mcu_util_queue_s *s, void *elem_addr);
void mcu_util_queue_deque(_mcu_util_queue_s *s, void *elem_addr);
void mcu_util_queue_peek(_mcu_util_queue_s *s, void *elem_addr);
void mcu_util_queue_remove(_mcu_util_queue_s *s, void *elem_addr);
uint8_t mcu_util_queue_is_empty(_mcu_util_queue_s *s);
uint8_t mcu_util_queue_is_full(_mcu_util_queue_s *s);
size_t mcu_util_queue_get_size(_mcu_util_queue_s *s);

#endif
