#ifndef __MCU_OTA_HANDLER_H__
#define __MCU_OTA_HANDLER_H__
typedef enum {
    OTA_EVENT_GOT_OTA_INFO,
    OTA_EVENT_GOT_FRAME_INFO,
    OTA_EVENT_FRAME_PAYLOAD,
    OTA_EVENT_FRAME_NEXT,
    OTA_EVENT_FRAME_FINISHED,
    OTA_EVENT_CONFIG_UPDATED,
    OTA_EVENT_ERROR,
} MCU_OTA_EVENT;

typedef struct _event_ota_ {
    uint8_t event_type;
    void *data;
} _event_ota_s;

typedef struct {
    uint32_t index;
    uint32_t size;
} mcu_ota_frame_info_s;

typedef struct {
    uint32_t magic_id;
    uint32_t app_address;
    uint32_t app_size;
    uint32_t app_crc;
    uint32_t cipher_flag;
    char     app_desc[16];
} bootloader_config_info_s;

typedef struct {
    uint32_t crc;
    uint32_t frame_count;
    uint32_t bin_size;
    uint8_t  target_mcu;
    mcu_ota_frame_info_s cur_frame_info;
    bootloader_config_info_s cur_config_info;
    
} mcu_ota_info_s;

typedef void (*write_t)(uint32_t address, uint8_t *data_buf, uint32_t length);
typedef void (*read_t)(uint32_t address, uint8_t *data_buf, uint32_t length);
typedef void (*reset_t)(void);

void mcu_init_ota_handler(write_t write, read_t read, reset_t reset);
void mcu_ota_event_handler(void *param);
mcu_ota_info_s* mcu_ota_info();
bool mcu_ota_get_start_flag();
void mcu_copy_factory_reset_config_to_config_sector();
void mcu_ota_eval_fsm_state();
void read_bootloader_version_string(uint8_t * version);
void mcu0_handle_ota_response_from_slave(char * command, uint8_t index);
#endif
