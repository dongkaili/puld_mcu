#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "bsp.h"
#include "mcu_util_queue.h"
#include "mcu_common_define.h"
#include "mcu_ipc_sync.h"

void mcu_util_queue_init(_mcu_util_queue_s *s, size_t init_size, size_t elem_size, void (*free_callback)(void *)) {
    assert(elem_size > 0);
    s->init_size = init_size;
    s->elem_size = elem_size;
    s->count = 0;
    s->size = init_size;
    s->elems = malloc(init_size * elem_size);
    s->pos_tail = 0;
    s->pos_head = s->pos_tail;
    s->free_callback = free_callback;
    assert(s->elems != NULL);
}

void mcu_util_queue_close(_mcu_util_queue_s *s) {
    if (s->free_callback != NULL) {
        uint32_t count = s->count;
        for (int i = 0; i < count; i++) {
            s->free_callback((char *)s->elems + i * s->elem_size);
        }
    }
    free(s->elems);
}

void mcu_util_queue_renew(_mcu_util_queue_s *s) {
    LOG1("%s, %d\n\r", __FUNCTION__, __LINE__);
    s->size += s->init_size;
    void* p_elems = realloc(s->elems, s->size * s->elem_size);
    s->elems = p_elems;

}

void mcu_util_queue_enque(_mcu_util_queue_s *s, void *elem_addr) {
    corelock_lock(&_init_lock);
    if (s->count == s->size) {
        mcu_util_queue_renew(s);
    }
    void *target = (void *)s->elems + s->pos_tail * s->elem_size;
    memcpy(target, elem_addr, s->elem_size);
    s->pos_tail++;
    s->count++;
    corelock_unlock(&_init_lock);
}

void mcu_util_queue_deque(_mcu_util_queue_s *s, void *elem_addr) {
    corelock_lock(&_init_lock);
    assert(s->count > 0);
    assert(s->pos_head <= s->pos_tail);
    if(s->count != 0) {
        s->count--;
        // if(s->count > 0)
        // printf("%s, s->count = %ld, queue size = %ld\n\r", __FUNCTION__, s->count, s->size);
        void *source = (void *)s->elems + s->pos_head * s->elem_size;
        memcpy(elem_addr, source, s->elem_size);
        s->pos_head++;
        if(s->count == 0) {
            s->pos_tail = 0;
            s->pos_head = s->pos_tail;
        }
    }
    corelock_unlock(&_init_lock);
}

void mcu_util_queue_peek(_mcu_util_queue_s *s, void *elem_addr) {
    assert(s->count > 0);
    void *source = (void *)s->elems + (s->pos_head) * s->elem_size;
    memcpy(elem_addr, source, s->elem_size);
}

uint8_t mcu_util_queue_is_empty(_mcu_util_queue_s *s) {
    // LOG("s->count = %ud\n\r", s->count);
    return (s->count == 0) ? 1 : 0;
}

uint8_t mcu_util_queue_is_full(_mcu_util_queue_s *s) {
    return (s->count == s->size) ? 1 : 0;
}

size_t mcu_util_queue_get_size(_mcu_util_queue_s *s) {
    return s->count;
}

