#ifndef __mcu_INTERNAL_FSM_H__
#define __mcu_INTERNAL_FSM_H__
#include <stdlib.h>
#include <stdio.h>

typedef uint8_t mcu_fsm_event_t;
typedef uint8_t mcu_fsm_state_t;

typedef uint8_t (*mcu_fsm_condition_func_t)(void *data);

typedef void (*mcu_fsm_handle_func_t)(uint8_t index, void *param);

typedef struct __mcu_fsm_table__ {
    void *data;
    uint8_t (*condition)(void *data);
    mcu_fsm_state_t cur_state;
    mcu_fsm_event_t event;
    mcu_fsm_state_t next_state;
    mcu_fsm_handle_func_t handle_func;
} mcu_fsm_table_s;

typedef struct __mcu_fsm__
{
    mcu_fsm_table_s *table;
    uint32_t size;
    mcu_fsm_state_t state;
} mcu_fsm_s;

void mcu_fsm_register(mcu_fsm_s* p_fsm, mcu_fsm_table_s* p_table, uint32_t count, mcu_fsm_state_t first_state);
void mcu_fsm_event_handler(uint8_t index, mcu_fsm_s* p_fsm, mcu_fsm_event_t event);

#endif