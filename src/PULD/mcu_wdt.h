#ifndef __mcu_WDT_H__
#define __mcu_WDT_H__
#include "wdt.h"
#include "sysctl.h"

typedef int (* wdt_callback) (void *parameter);

void mcu_wdt_init(wdt_device_number_t device_num, wdt_callback callback);

void mcu_wdt_loop(wdt_device_number_t device_num);

void mcu_wdt_enable(wdt_device_number_t id, uint8_t enable_flag);

#endif