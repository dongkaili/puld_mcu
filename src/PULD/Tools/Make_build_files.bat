cd ..
rmdir /s /q build
mkdir build
cd build

mkdir debug
cd debug
cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_FLAGS_DEBUG="-g -O3" -DPROJ=PULD ../../../..
rem --log-level=VERBOSE --log-context --trace-expand --debug-output --check-system-vars 

cd ..
REM Move debug build folder out of view for cmake so it won't try to reuse content for release build...
move debug ../..

mkdir release
cd release
cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Release -DPROJ=PULD ../../../..
rem cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_FLAGS_DEBUG="-g -O0" -DPROJ=PULD ../../../..
cd ..

REM ... and then restore
move ../../debug .

cd ../Tools


REM -D <var>:<type>=<value>