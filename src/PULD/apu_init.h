#pragma once
#include <stdint.h>
#include <plic.h>
#include <i2s.h>
#include <sysctl.h>
#include <dmac.h>
#include <fpioa.h>
#include "gpiohs.h"

#ifndef APU_DIR_ENABLE
#define APU_DIR_ENABLE 1
#endif

#ifndef APU_VOC_ENABLE
#define APU_VOC_ENABLE 1
#endif

#ifndef APU_DMA_ENABLE
#define APU_DMA_ENABLE 0
#endif

#ifndef APU_FFT_ENABLE
#define APU_FFT_ENABLE 1
#endif

#ifndef APU_SMPL_SHIFT
#define APU_SMPL_SHIFT 0x00
#endif

#ifndef APU_SATURATION_DEBUG
#define APU_SATURATION_DEBUG 0
#endif

#ifndef APU_SATURATION_VPOS_DEBUG
#define APU_SATURATION_VPOS_DEBUG 0x07ff
#endif

#ifndef APU_SATURATION_VNEG_DEBUG
#define APU_SATURATION_VNEG_DEBUG 0xf800
#endif

#ifndef APU_AUDIO_GAIN
#define APU_AUDIO_GAIN 0x7ff  // gain = 1.999
#endif

/* Need to test overflow or limit reg in H/W??? */

#define APU_DIR_DMA_CHANNEL DMAC_CHANNEL3
#define APU_VOC_DMA_CHANNEL DMAC_CHANNEL4

#define APU_DIR_CHANNEL_MAX 16
#define APU_DIR_CHANNEL_SIZE 512
#define APU_VOC_CHANNEL_SIZE 512
#define APU_DIR_CHANNEL_PADDING 128

#define UCA8_RADIUS_CM 0.437
#define UCA_N 8
typedef enum _i2s_fs_options
{
    I2S_FS_LPM,
    I2S_FS_253906,
    I2S_FS_285644,
    I2S_FS_317382_15,
    I2S_FS_317382_16,
    I2S_FS_317382_17,
    I2S_FS_317382_18,
    I2S_FS_317382_19,
    I2S_FS_317382_20,
    I2S_FS_317382_21,
    I2S_FS_317382_22,
    I2S_FS_317382_23,
    I2S_FS_317382_24,
    I2S_FS_317382_25,
    I2S_FS_317382_26,
    I2S_FS_317382_27,
    I2S_FS_317382_28,
    I2S_FS_317382_29,
    I2S_FS_317382_30,
    I2S_FS_317382_31,
    I2S_FS_317382_32,
    I2S_FS_317382_33,
    I2S_FS_317382_34,
    I2S_FS_317382_35,
    I2S_FS_349121,
    I2S_FS_380859,
    I2S_FS_399902,
    I2S_FS_406250,
    I2S_FS_MAX
} i2s_fs_options_t; 


#define BUILD_FIELD_NX(partA,partB) partA##partB
#define BUILD_FIELD(partA,partB) BUILD_FIELD_NX(partA,partB)

#define BP_LOW 18      // initial Low Pass filter cuttoff frequency

/////////////////////////////////////////////////////////////////////////////////
// These are the defined I2S_FS options based on enumerated i2s_fs_options_t list
/////////////////////////////////////////////////////////////////////////////////
//#define I2S_FS I2S_FS_LPM
//#define I2S_FS I2S_FS_253906
//#define I2S_FS I2S_FS_285644
  #define I2S_FS BUILD_FIELD(I2S_FS_317382_, BP_LOW)
//#define I2S_FS I2S_FS_349121
//#define I2S_FS I2S_FS_380859
//#define I2S_FS I2S_FS_399902
//#define I2S_FS I2S_FS_406250
////////////////////////////////////////////////////////////////////////

// Lower Power Mode (LPM) setting
//#define I2S_FS 8192 // System current is 15% of max with PLL clock 1/32 of default (800000000UL/32) and slave MCU's off

// Testing I2S_FS limits...
//#define I2S_FS 410000 // I2S clocks: OK[MCU0|MU1|MCU2]  FAIL
//#define I2S_FS 420000 // I2S clocks: OK[MCU0|MU1|MCU2]  FAIL
//#define I2S_FS 430000 // I2S clocks: OK[MCU1|MCU2]      FAIL[MCU0]
//#define I2S_FS 440000 // I2S clocks: OK[MCU2]           FAIL[MCU0|MCU1]
//#define I2S_FS 450000 // I2S clocks: OK[MCU2]           FAIL[MCU0|MCU1]
//#define I2S_FS 460000 // I2S clocks: OK                 FAIL[MCU0|MCU1|MCU2]
//#define I2S_FS 253906.25 // This value is fine-tuned so PLL hunting algorithm in sysctl_pll_set_freq determines this value exactly. Results in BCLK=520MHz. 

// FIR Filter select options...
typedef struct apu_fir_coefficients_
{
    uint32_t i2s_fs;   // associated frequency... discrete choices unless we establish real-time programming ability
    uint8_t  bp_low;   // Low end of bandpass filter in kHz
    uint8_t  bp_high;  // High end of bandpass filter in kHz
    uint16_t *apu_coefficients;
//} __attribute__((packed, aligned(4))) apu_fir_coefficients_t; 
} apu_fir_coefficients_t; 

extern apu_fir_coefficients_t apu_fir_coefficients_options[]; 

    

//#if APU_FFT_ENABLE
// extern uint32_t APU_DIR_FFT_BUFFER[APU_DIR_CHANNEL_MAX]
// 				       [APU_DIR_CHANNEL_SIZE]
// 	__attribute__((aligned(128)));
extern uint32_t APU_VOC_FFT_BUFFER[APU_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));
//extern int16_t APU_VOC_FFT_BUFFER[APU_VOC_CHANNEL_SIZE]
//	__attribute__((aligned(128)));
//#endif
//#if (1)
extern int16_t APU_DIR_BUFFER[APU_DIR_CHANNEL_MAX]
				  [APU_DIR_CHANNEL_SIZE]
	__attribute__((aligned(128)));
extern int16_t APU_VOC_BUFFER[APU_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));
//#endif

extern uint64_t dir_logic_count;
extern uint64_t voc_logic_count;
extern apu_fir_coefficients_t apu_fir_coefficients_options[];

void init_all(void);
int update_all_i2s_fs(i2s_fs_options_t i2s_fs_sel);
extern i2s_fs_options_t i2s_fs_sel_current, i2s_fs_sel_saved;

