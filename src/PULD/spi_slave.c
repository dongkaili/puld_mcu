/* Copyright 2018 Canaan Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "mcu_pins.h"
#include "mcu_common_define.h"
#include "spi_slave.h"
#include "fpioa.h"
#include "gpiohs.h"
#include "stdio.h"




// extern uint8_t slave_cfg[2][32];
int spi_slave_mcu1_receive_hook(void *data)
{
    // printf("slave mcu 1 cfg array: ");
    // for (int i = 0; i < 32; i++)
    // {
    //     printf("0x%x,", slave_cfg[0][i]);
    // }
    // printf("\r\n");
    // printf("cmd: %d\r\n", ((spi_slave_command_t *)data)->cmd);
    // printf("err: %d\r\n", ((spi_slave_command_t *)data)->err);
    // printf("len: %d\r\n", ((spi_slave_command_t *)data)->len);
    // printf("addr: 0x%x\r\n", ((spi_slave_command_t *)data)->addr);
    
    return 0;
}

int spi_slave_mcu2_receive_hook(void *data)
{
    // printf("slave mcu 2 cfg array: ");
    // for (int i = 0; i < 32; i++)
    // {
    //     printf("0x%x,", slave_cfg[1][i]);
    // }
    // printf("\r\n");
    // printf("cmd: %d\r\n", ((spi_slave_command_t *)data)->cmd);
    // printf("err: %d\r\n", ((spi_slave_command_t *)data)->err);
    // printf("len: %d\r\n", ((spi_slave_command_t *)data)->len);
    // printf("addr: 0x%x\r\n", ((spi_slave_command_t *)data)->addr);
    
    
    return 0;
}

int spi_slave_init(uint8_t *data, uint32_t len, uint8_t mcu_id)
{
    if (mcu_id == MCU1)
    {
        fpioa_set_function(SPI_SLAVE_MCU1_CS_PIN, FUNC_SPI_SLAVE_SS);
        fpioa_set_function(SPI_SLAVE_MCU1_CLK_PIN, FUNC_SPI_SLAVE_SCLK);
        fpioa_set_function(SPI_SLAVE_MCU1_MOSI_PIN, FUNC_SPI_SLAVE_D0);
        fpioa_set_function(SPI_SLAVE_MCU1_INT_PIN, FUNC_GPIOHS0 + SPI_SLAVE_MCU1_INT_IO);
        fpioa_set_function(SPI_SLAVE_MCU1_READY_PIN, FUNC_GPIOHS0 + SPI_SLAVE_MCU1_READY_IO);
        spi_slave_config(SPI_SLAVE_MCU1_INT_IO, SPI_SLAVE_MCU1_READY_IO, DMAC_CHANNEL5, 8, data, len, spi_slave_mcu1_receive_hook);
    }
    else if (mcu_id == MCU2)
    {
        fpioa_set_function(SPI_SLAVE_MCU2_CS_PIN, FUNC_SPI_SLAVE_SS);
        fpioa_set_function(SPI_SLAVE_MCU2_CLK_PIN, FUNC_SPI_SLAVE_SCLK);
        fpioa_set_function(SPI_SLAVE_MCU2_MOSI_PIN, FUNC_SPI_SLAVE_D0);
        fpioa_set_function(SPI_SLAVE_MCU2_INT_PIN, FUNC_GPIOHS0 + SPI_SLAVE_MCU2_INT_IO);
        fpioa_set_function(SPI_SLAVE_MCU2_READY_PIN, FUNC_GPIOHS0 + SPI_SLAVE_MCU2_READY_IO);
        spi_slave_config(SPI_SLAVE_MCU2_INT_IO, SPI_SLAVE_MCU2_READY_IO, DMAC_CHANNEL5, 8, data, len, spi_slave_mcu2_receive_hook);
    }

    return 0;
}

