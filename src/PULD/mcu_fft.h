#pragma once
#ifndef _MCU_FFT_
#define _MCU_FFT_

int calculate_fft(int16_t *p_value_in, int16_t * p_fft_power_hard, int16_t * p_fft_power_soft);
#endif

