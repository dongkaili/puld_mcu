#ifndef __MCU_MISC_FUNC__
#define __MCU_MISC_FUNC__

// https://flaviocopes.com/c-return-string/

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include "mcu_common_define.h"

const char * TF_string(bool flag)
{
    // https://stackoverflow.com/questions/1496313/returning-a-c-string-from-a-function
    static char* TF_states[] = { KRED "False" KNRM, KGRN "True " KNRM };
    return TF_states[flag];
}
char as_printable_char(char c)
{
    char result;
    result = (isprint(c) != 0 && isprint(c) != 128) ? c : '.';
    return result;
}


void swap_bytes_in_double_words(void* addr, uint32_t len)
{
    uint8_t temp;
    uint32_t length;
    if (len % 4 != 0)
    {
        length = (len / 4 + 1) * 4;
    }
    else
    {
        length = len;
    }

    for (uint32_t i = 0; i < length; i = i + 4)
    {
        temp = *(uint8_t *)((uint8_t *)addr + i);
        *(uint8_t *)((uint8_t *)addr + i) = *(uint8_t *)((uint8_t *)addr + i + 3);
        *(uint8_t *)((uint8_t *)addr + i + 3) = temp;
        temp = *(uint8_t *)((uint8_t *)addr + i + 1);
        *(uint8_t *)((uint8_t *)addr + i + 1) = *(uint8_t *)((uint8_t *)addr + i + 2);
        *(uint8_t *)((uint8_t *)addr + i + 2) = temp;
    }
}

#endif