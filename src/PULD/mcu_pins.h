// Pin definitions for all MCU IO in design
// Some are only applicable to certain MCU's. 
//   Indicated by [] wrapper in comment 
//   Use MCU_ID to conditionally enable funcitons in code

// JTAG [ALL] Not used programatically after boot

#ifndef _MCU_PINS_
#define _MCU_PINS_

// STDIO Debug Remap
#define PIN_MCU_JTAG_TCK    0
#define PIN_MCU_JTAG_TDI    1 // assigned as STDIO RX on boot
#define PIN_MCU_JTAG_TMS    2
#define PIN_MCU_JTAG_TDO    3// assigned as STDIO TX on boot

// ISP [ALL] Not used programatically after boot
#define PIN_MCU_ISP_RX      4
#define PIN_MCU_ISP_TX      5
#define PIN_MCU_BOOTn      16 

// SER IF [MCU0]
#define PIN_MCU_SER_RSTn    6
#define PIN_MCU_SER_RX      7
#define PIN_MCU_SER_TX      8
#define PIN_MCU_SER_TX01   18 // MCU0 -> MCU1
#define PIN_MCU_SER_RX01   19 // MCU0 <- MCU1
#define PIN_MCU_SER_TX02   20 // MCU0 -> MCU2
#define PIN_MCU_SER_RX02   21 // MCU0 <- MCU2

// SER IF [MCU1]
#define PIN_MCU_SER_TX10   18 // MCU1 -> MCU0
#define PIN_MCU_SER_RX10   19 // MCU1 <- MCU0

// SER IF [MCU2]
#define PIN_MCU_SER_TX20   18 // MCU2 -> MCU0
#define PIN_MCU_SER_RX20   19 // MCU2 <- MCU0

// LINK Busses
  // LINK0 [MCU0|MCU1]
#define PIN_MCU_LINK00     28
#define PIN_MCU_LINK01     29
  // LINK0 [MCU0|MCU2]
#define PIN_MCU_LINK02     30
#define PIN_MCU_LINK03     31
  // LINK1 [ALL]
#define PIN_MCU_LINK10      9
#define PIN_MCU_LINK11     10
#define PIN_MCU_LINK12     11
#define PIN_MCU_LINK13     12
  // LINK2 [MCU1|MCU2]
#define PIN_MCU_LINK20     24
#define PIN_MCU_LINK21     25
#define PIN_MCU_LINK22     26
#define PIN_MCU_LINK23     27

// Battery Functions [MCU0]
#define PIN_MCU_ADC_MISO   24
#define PIN_MCU_ADC_CLK    25
#define PIN_MCU_VOLTAGE_SS 26
#define PIN_MCU_CURRENT_SS 27
#define PIN_MCU_ACPRn      32
#define PIN_MCU_BAT_FAULTn 33
#define PIN_MCU_CHRGn      34
#define PIN_MCU_CHARGE_EN  35

// MCU Enables [MCU0]
#define PIN_MCU1_EN        13
#define PIN_MCU2_EN        14

// MCU ID [ALL]
#define PIN_MCU_ID0        15 
#define PIN_MCU_ID1        17 

// Triggers [ALL]
#define PIN_MCU_TRIG1      36
#define PIN_MCU_TRIG2      37

// I2S [ALL]
#define PIN_MCU_I2S0_SCLK  42 
#define PIN_MCU_I2S0_LRCLK 43
#define PIN_MCU_I2S0_IN_D3 44
#define PIN_MCU_I2S0_IN_D2 45
#define PIN_MCU_I2S0_IN_D1 46
#define PIN_MCU_I2S0_IN_D0 47

#endif