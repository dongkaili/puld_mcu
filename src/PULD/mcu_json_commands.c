#include <stdlib.h>
#include <stdio.h>
#include <sysctl.h>
#include <string.h>
#include "mcu_json_commands.h"
#include "mcu_common.h"
#include "mcu_device_management.h"
#include "apu.h"
#include "apu_init.h"
#include "gpio.h"
#include "mcu_adc.h"
#include "mcu_stdio.h"
#include "uart.h"
#include "uarths.h"
#include "timer.h"
#include "gpio_common.h"
#include "VCS_info.h"
//#include "mjson.h"
#include "mcu_common_define.h"
#include "mcu_ota_handler.h"
#include "mcu_flash.h"
#include "mcu_uart.h"
#include "mcu_mic_array.h"
#include "spi_master.h"


#define COMM_TIMEOUT_SEC         5

//#define __dbgopt__ __attribute__((optimize(0)))
#define __dbgopt__

extern void send_string(uart_device_number_t index, char* str, _data_header_s head_data);

typedef void (*mcu_cmd_handle_func_t)(void* param_1, void* param_2);

typedef void (*mcu_rsp_handle_func_t)(void* param_1, void* param_2);
typedef void(*mcu_streamp_handle_func_t)(void* param_1, void* param_2);

typedef void(*mcu_var_handle_func_t)(void* param_1, void* param_2, void* param_3, void* param_4, void* param_5, void* param_6);
typedef void(*mcu_set_modify_var_handle_func_t)(void* var_addr, void* var_val, void* mcu_idx);


char *config_str[] = { "DISABLE", "ENABLE" };
uint8_t onoff_value[] = { 0, 1 };
bool bool_choice[] = { false, true };
uint8_t mcu_number[] = { MCU0, MCU1, MCU2, MCU3 };

extern int mcu0_receive_rsp_count;
extern int mcu0_receive_multicast_cmd;
extern _all_bf_data_s all_bf_data;
extern beamformer_s BF[N_BF];
bool Slave_Data_Fwd = false;
static int timer0_ctx_table[UART_DEVICE_MAX] = {0, 1, 2};
static int timer1_ctx_table[UART_DEVICE_MAX] = {0, 1, 2};


static int uart_health_check_flag[UART_DEVICE_MAX] = {0,0,0};
static int uart_baudrate_restore_flag[UART_DEVICE_MAX] = {0,0,0};

extern int mcu0_receive_rsp_count;
extern int mcu0_receive_multicast_cmd;
static char json_str[32] = {0};
static uint32_t Uart_Check_Interval[UART_DEVICE_MAX] = {COMM_TIMEOUT_SEC, COMM_TIMEOUT_SEC, COMM_TIMEOUT_SEC};

static uint32_t timer1_fire_count[UART_DEVICE_MAX] = {0,0,0};

static double json_value = 0.0f;
#if !FACTORY_RESET
static double double_var_1 = 0.0f;    // internal variable to test "SET" and "GET" cmd
static double double_var_2 = 0.0f;    // internal variable to test "SET" and "GET" cmd
static uint32_t uint32_var_1 = 0;
static uint32_t uint32_var_2 = 0;
static uint32_t uint32_arr_1[UART_DEVICE_MAX] = { 0, 0, 0 };
static double double_arr_1[UART_DEVICE_MAX] = { 0.0, 0.0, 0.0 };
#endif

extern bool Restart_MCU0;
extern void set_last_reset_reason();

bool block_LPM = true; // flag that is cleared after min delay (5 sec) after to allow LPM command through. Needed to handle spurious LPM commands when app restarts

static _data_header_s head_data_JSON = {
    .head = 0x11,
    .version = 0x02,
    .type = 0x0101,
};

typedef struct {

    char    command_name[SIZE];
    uint8_t mcu;
    char    state[SIZE];
    void*   data;
    mcu_cmd_handle_func_t handle_func;
    void*   param_1;
    void*   param_2;  

} mcu_cmd_handle_table_s;

typedef struct {

    char    var_name[SIZE];
    void*   var_address;
    mcu_var_handle_func_t set_var_handle_func;
    mcu_var_handle_func_t get_var_handle_func;
} mcu_var_lookup_table_s;


typedef struct {

    char    command_name[SIZE];
    uint8_t mcu;
    char    var_name[SIZE];
    mcu_var_handle_func_t handle_func;
    void*   var_addr;
    void*   var_is_array;    // boolean flag to indicate the var is an arrray with 1 element for each MCU
    void*   var_val;
    void*   var_set_modify_handler;
} mcu_var_handle_table_s;

typedef struct {

    char    command_name[SIZE];
    uint8_t mcu;
    char    state[SIZE];
    void*   data;
    mcu_rsp_handle_func_t handle_func;
    void*   param_1;
    void*   param_2;  

} mcu_rsp_handle_table_s;


typedef struct {
    mcu_cmd_handle_table_s * table;
    uint32_t size;
} mcu_command_handle_s;


typedef struct {
    mcu_var_handle_table_s * table;
    uint32_t size;
} mcu_variable_handle_s;

typedef struct {
    mcu_rsp_handle_table_s * table;
    uint32_t size;
} mcu_response_handle_s;

// local variables 


static mcu_command_handle_s mcu0_cmd_handler;
static mcu_command_handle_s mcu1_cmd_handler;
static mcu_command_handle_s mcu2_cmd_handler;


static mcu_variable_handle_s mcu0_var_handler;
static mcu_variable_handle_s mcu1_var_handler;
static mcu_variable_handle_s mcu2_var_handler;

static mcu_response_handle_s mcu0_rsp_handler;

// local function declaration
static void mcu_init_variable_handler(void);
static void mcu_init_response_handler(void);
static void mcu_command_table_register(mcu_command_handle_s* p_cmd_hdl, mcu_cmd_handle_table_s* p_table, uint32_t count) ;
static void mcu_variable_table_register(mcu_variable_handle_s* p_var_hdl, mcu_var_handle_table_s* p_table, uint32_t count);
static void mcu_response_table_register(mcu_response_handle_s* p_rsp_hdl, mcu_rsp_handle_table_s* p_table, uint32_t count);
//static int assert_command(char * fld);
static void mcu_command_table_handler(const mcu_command_handle_s * p_cmd_handler, const struct mcu_command_object * cmd_obj, const char* str);
static void mcu_variable_table_handler(const mcu_variable_handle_s * p_var_handler, const struct mcu_command_object * cmd_obj, const char* str);
static void mcu_response_table_handler(const mcu_response_handle_s * p_rsp_handler, const struct mcu_response_object * rsp_obj, const char* str);

static void mcu_json_command_handler(uint8_t index, const struct mcu_command_object * cmd_obj, const char* str);
static void mcu_json_variable_handler(uint8_t index, const struct mcu_command_object * cmd_obj, const char* str);
static void mcu_json_response_handler(uint8_t index, const struct mcu_response_object * rsp_obj, const char* str);

static int mcu_json_response_object_read(const char *buf, mcu_response_object_t *myobject, int *structure_index);
static int mcu_json_command_object_read(const char *buf, struct mcu_command_object *myobject, int *structure_index);
static int mcu_json_stream_object(const char *buf, mcu_stream_object_t *myobject, int *structure_index);

static void get_battery_voltage_reading(void* para1, void* para2);
static void get_battery_current_reading(void* para1, void* para2);
static void get_charge_status(void* para_1, void* para_2);
static void enable_system_reset(void* mcu_idx, void* para_2);
static void configure_battery_charge(void* para_1, void* enable_flg);
static void configure_mcu_power(void* mcu_idx, void* enable_flg);
static void get_mcu_id(void* mcu_idx, void* para_2);
static void get_firmware_version(void* mcu_idx, void* para_2);
static void get_bootloader_version(void* mcu_idx, void* para_2);
static void mcu0_handle_ota_slave_response(void* mcu_idx, void* para_2);
static void get_actual_baud_rate_setpoint(void* mcu_idx, void* baudrate_set);
static void set_new_baud_rate(void* mcu_idx, void* baudrate_set);
static uint8_t mcu_uart_validate_new_baud_rate(uint32_t new_baud_rate);

static void mcu0_handle_set_baudrate_response(void* mcu_idx, void* para_2);
static void mcu0_handle_get_baudrate_response(void* mcu_idx, void* para_2);
static void mcu0_handle_ota_slave_response(void* mcu_idx, void* para_2);
static void mcu0_handle_slave_spi_ready_response(void* mcu_idx, void* para_2);

#if !FACTORY_RESET
static void configure_i2s_data_capture(void* mcu_idx, void* enable_flg);
static void configure_i2s_soft_fft(void* mcu_idx, void* enable_flg);
static void configure_i2s_hard_fft(void* mcu_idx, void* enable_flg);
#endif

#if !FACTORY_RESET
static void configure_mcu_host_report(void* mcu_index, void* enable_flag);

#endif

static void Activate_LPM(void* para_1, void* enable_flg);

// local function definition
int mcu_timer1_callback(void * ctx)
{
    int index = *(int *)ctx;
    timer1_fire_count[index]++;

    if (timer1_fire_count[index] >= Uart_Check_Interval[index] / COMM_TIMEOUT_SEC)
    {
        //printf("mcu:%d, uart:%d, flag:%d, period:%d seconds\r\n", mcu_id, index, uart_health_check_flag[index], timer1_fire_count[index] * COMM_TIMEOUT_SEC);
        timer1_fire_count[index] = 0;
        if (uart_health_check_flag[index] == 1)
        {
            if (index == UART_DEVICE_1 && mcu_id == MCU0)
            {
                uart_baudrate_restore_flag[index] = 1;
            }
            else if ((index == UART_DEVICE_2) && ((mcu_id == MCU0 || mcu_id == MCU1)))
            {

                uart_baudrate_restore_flag[index] = 1;
            }
            else if ((index == UART_DEVICE_3) && ((mcu_id == MCU0 || mcu_id == MCU2)))
            {

                uart_baudrate_restore_flag[index] = 1;
            }
        }
        // reset the uart_health_check flag
        uart_health_check_flag[index] = 1;
    }
    return 0;
}


void mcu_uart_restore_baud_rate(void)
{
    for (int i = 0; i < UART_DEVICE_MAX; i++)
    {
        if (uart_baudrate_restore_flag[i] == 1)
        {
            //printf("restore baud rate for uart %d\r\n", i);
            if (i == UART_DEVICE_1)
            {
                mcu_uart_set_nominal_baud_rate(i, HOST_MCU_BAUD_RATE);
                uart_init(i);
                mcu_uart_change_baud_rate(i, HOST_MCU_BAUD_RATE);
            }
            else
            {
                mcu_uart_set_nominal_baud_rate(i, MCU_MCU_BAUD_RATE);
                uart_init(i);
                mcu_uart_change_baud_rate(i, MCU_MCU_BAUD_RATE);
            }
            uart_baudrate_restore_flag[i] = 0;
        }
    }
}

int mcu_command_handler_timer_callback(void * ctx)
{
    int index = *(int *)ctx;
    //printf("mcu:%d, uart:%d, flag:%d\r\n", mcu_id, index, uart_health_check_flag[index]);
    if (uart_health_check_flag[index] == 1)
    {

        if (index == UART_DEVICE_1 && mcu_id == MCU0)
        {
            uart_baudrate_restore_flag[index] = 1;
            uart_health_check_flag[index] = 0;
        }
        else if ((index == UART_DEVICE_2) && ((mcu_id == MCU0 || mcu_id == MCU1)))
        {
            uart_baudrate_restore_flag[index] = 1;
            uart_health_check_flag[index] = 0;
        }
        else if ((index == UART_DEVICE_3) && ((mcu_id == MCU0 || mcu_id == MCU2)))
        {
            uart_baudrate_restore_flag[index] = 1;
            uart_health_check_flag[index] = 0;
        }
    }
    return 0;
}

static int mcu_json_response_object_read(const char *buf, mcu_response_object_t *myobject, int *structure_index)
{
    int ret_value;

    const struct json_attr_t json_attrs_0[] = {
        {"RSP",   t_string, .addr.string = myobject->response_command, 
                            .len = sizeof(myobject->response_command)},
        {"MCU",  t_integer, .addr.integer = &(myobject->mcu)},
        {"STATE", t_string, .addr.string = myobject->data.state,
                            .len = sizeof(myobject->data.state)},
        {NULL},
    };
    
    const struct json_attr_t json_attrs_1[] = {
        {"RSP",   t_string, .addr.string = myobject->response_command, 
                            .len = sizeof(myobject->response_command)},
        {"MCU",  t_integer, .addr.integer = &(myobject->mcu)},
        {"VALUE",   t_real, .addr.real = &(myobject->data.var_value)},
        {NULL},
    };
    
    // const struct json_attr_t json_value_attrs_2[] = {
    //     {"RSP",   t_string, .addr.string = myobject->response_command, 
    //                         .len = sizeof(myobject->response_command)},
    //     {"MCU",  t_integer, .addr.integer = &(myobject->mcu)},
    //     {"VALUE", t_string, .addr.string = myobject->data.str_value,
    //                         .len = sizeof(myobject->data.str_value)},
    //     {NULL},
    // };
    
    // const struct json_attr_t json_value_attrs_3[] = {
    //     {"RSP",   t_string, .addr.string = myobject->response_command, 
    //                         .len = sizeof(myobject->response_command)},
    //     {"MCU",  t_integer, .addr.integer = &(myobject->mcu)},
    //     {"VALUE",t_integer, .addr.integer = &(myobject->data.value)},
    //     {NULL},
    // };
    
    ret_value = json_read_object(buf, json_attrs_0, NULL);
    if (ret_value == 0)
    {
        *structure_index = 0;
    }
    else
    {
        ret_value = json_read_object(buf, json_attrs_1, NULL);
        if (ret_value == 0)
        {
            *structure_index = 1;
        }
        else
        {
            *structure_index = 255;            
        }       
    }
    return ret_value;
}

static int mcu_json_stream_object(const char *buf, mcu_stream_object_t *myobject, int *structure_index)
{
    int ret_value;

    // 1st struct option includes min required fields 
    const struct json_attr_t json_attrs_0[] = {
        {"MCU"      , t_integer, .addr.integer = &(myobject->mcu)},
        {"ANGLE"    , t_real,    .addr.real    = &(myobject->angle)},
        {"DBFS"     , t_real,    .addr.real    = &(myobject->dbfs)},
        {"NMPA"     , t_integer, .addr.integer = &(myobject->nmpa)},
        {"STATE"    , t_string,  .addr.string  = myobject->state,
                                 .len = sizeof(myobject->state)},
        { NULL },
    };

    // 2rd struct option includes optional DUT fields
    const struct json_attr_t json_attrs_1[] = {
        {"MCU"      , t_integer, .addr.integer = &(myobject->mcu)},
        {"ANGLE"    , t_real,    .addr.real    = &(myobject->angle)},
        {"ANGLE_DUT", t_real,    .addr.real    = &(myobject->angle_dut)},
        {"DBFS"     , t_real,    .addr.real    = &(myobject->dbfs)},
        {"DBFS_DUT" , t_real,    .addr.real    = &(myobject->dbfs_dut)},
        {"NMPA"     , t_integer, .addr.integer = &(myobject->nmpa)},
        {"STATE"    , t_string,  .addr.string  = myobject->state,
                                 .len = sizeof(myobject->state)},
        { NULL },
    };
    
    // 3nd struct option includes temporary AZM & INC fields (needed to build arrays until SPI method is completed)
    const struct json_attr_t json_attrs_2[] = {
        {"MCU"          , t_integer, .addr.integer = &(myobject->mcu)},
        {"ANGLE"        , t_real,    .addr.real    = &(myobject->angle)},
        {"ANGLE_TMP_AZM", t_real,    .addr.real    = &(myobject->angle_azm)},
        {"ANGLE_TMP_INC", t_real,    .addr.real    = &(myobject->angle_inc)},
        {"DBFS"         , t_real,    .addr.real    = &(myobject->dbfs)},
        {"DBFS_TMP_AZM" , t_real,    .addr.real    = &(myobject->dbfs_azm)},
        {"DBFS_TMP_INC" , t_real,    .addr.real    = &(myobject->dbfs_inc)},
        {"NMPA"         , t_integer, .addr.integer = &(myobject->nmpa)},
        {"STATE"        , t_string,  .addr.string  = myobject->state,
                                 .len          = sizeof(myobject->state)},
        { NULL },
    };

    // 4nd struct option includes all optional DUT, AZM & INC fields
    const struct json_attr_t json_attrs_3[] = {
        {"MCU"          , t_integer, .addr.integer = &(myobject->mcu)},
        {"ANGLE"        , t_real,    .addr.real    = &(myobject->angle)},
        {"ANGLE_DUT"    , t_real,    .addr.real    = &(myobject->angle_dut)},
        {"ANGLE_TMP_AZM", t_real,    .addr.real    = &(myobject->angle_azm)},
        {"ANGLE_TMP_INC", t_real,    .addr.real    = &(myobject->angle_inc)},
        {"DBFS"         , t_real,    .addr.real    = &(myobject->dbfs)},
        {"DBFS_DUT"     , t_real,    .addr.real    = &(myobject->dbfs_dut)},
        {"DBFS_TMP_AZM" , t_real,    .addr.real    = &(myobject->dbfs_azm)},
        {"DBFS_TMP_INC" , t_real,    .addr.real    = &(myobject->dbfs_inc)},
        {"NMPA"         , t_integer, .addr.integer = &(myobject->nmpa)},
        {"STATE"        , t_string,  .addr.string  = myobject->state,
                                 .len          = sizeof(myobject->state)},
        { NULL },
    };

#if 1
    ret_value = json_read_object(buf, json_attrs_0, NULL);
    
    if (ret_value == 0)
    {
        *structure_index = 0;
    }
    else
    {
        ret_value = json_read_object(buf, json_attrs_1, NULL);
        if (ret_value == 0)
        {
            *structure_index = 1;
        }
        else
        {
            ret_value = json_read_object(buf, json_attrs_2, NULL);
            if (ret_value == 0)
            {
                *structure_index = 2;
            }
            else
            {
                ret_value = json_read_object(buf, json_attrs_3, NULL);
                if (ret_value == 0)
                {
                    *structure_index = 3;
                }
                else
                {
                    *structure_index = 255;
                }            
            }
        }        
    }    
    return ret_value;
#else    
    struct json_attr_t json_attrs[4] = { *json_attrs_0, *json_attrs_1, *json_attrs_2 , *json_attrs_3 };
    
    ret_value = json_read_object(buf, json_attrs_0, NULL);
    
    JSON_debug_print1("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
    if (ret_value == 0)
    {
        *structure_index = 0;
        JSON_debug_print1("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
    }
    else
    {
        *structure_index = 255;
        for (int i = 1; i < 4; i++)
        {
            JSON_debug_print1("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
            ret_value = json_read_object(buf, &json_attrs[i], NULL);
            JSON_debug_print1("%s,%s,%d >> i=%d, ret_value = %d <<\n\r", __FILE__, __FUNCTION__, __LINE__, i, ret_value);
            if(ret_value == 0)
            {
                *structure_index = i;
                JSON_debug_print1("%s,%s,%d   FOUND i = %d\n\r", __FILE__, __FUNCTION__, __LINE__, i);
                break;
            }
        }
    }
    return ret_value;
#endif    
}

static __dbgopt__ int mcu_json_command_object_read(const char *buf, struct mcu_command_object *myobject, int *structure_index)
{
    JSON_debug_print1("%s,%s,%d\n\r", __FILE__, __FUNCTION__, __LINE__);
    int ret_value;
    const struct json_attr_t json_attrs_0[] = {
        {"CMD",   t_string, .addr.string = myobject->command, 
                            .len = sizeof(myobject->command)},
        {"MCU", t_integer,  .addr.integer = &(myobject->mcu), 
                            .dflt.integer = MCU3},
        {"STATE", t_string,  .addr.string = myobject->data.state,
                             .len = sizeof(myobject->data.state)},
        {NULL},
    };
    
    const struct json_attr_t json_attrs_1[] = {
        {"CMD",   t_string,  .addr.string = myobject->command,
                             .len = sizeof(myobject->command)},
        {"MCU",   t_integer, .addr.integer = &(myobject->mcu),
                             .dflt.integer = MCU3},
        {"VAR",   t_string,  .addr.string = myobject->data.var_real.var_name,
                             .len = sizeof(myobject->data.var_real.var_name)},
        {"VAL",   t_real,    .addr.real = &(myobject->data.var_real.var_value)},
        {NULL},
    };
    
    const struct json_attr_t json_attrs_2[] = {
        {"CMD",   t_string,  .addr.string = myobject->command,
                             .len = sizeof(myobject->command)},
        {"MCU",   t_integer, .addr.integer = &(myobject->mcu),
                             .dflt.integer = MCU3},        
        {"VALUE", t_real,    .addr.real = &(myobject->data.var_value)},
        {NULL},
    };

    ret_value = json_read_object(buf, json_attrs_0, NULL);
    
    if (ret_value == 0)
    {
        *structure_index = 0;
    }
    else
    {
        ret_value = json_read_object(buf, json_attrs_1, NULL);
        if (ret_value == 0)
        {
            *structure_index = 1;
        }
        else
        {
            ret_value = json_read_object(buf, json_attrs_2, NULL);
            if (ret_value == 0)
            {
                *structure_index = 2;
            }
            else
            {
                *structure_index = 255;
            }            
        }        
    }    
    return ret_value;
}


static void mcu_json_command_handler(uint8_t index, const struct mcu_command_object * cmd_obj, const char* str)
{
    if ((cmd_obj != NULL) && (index == UART_DEVICE_1) && (mcu_id == MCU0))
    {        
        mcu_command_table_handler(&mcu0_cmd_handler, cmd_obj, str);                
    }    
    else if ((cmd_obj != NULL) && (index == UART_DEVICE_2) && (mcu_id == MCU1))
    {
        mcu_command_table_handler(&mcu1_cmd_handler, cmd_obj, str);               
    }    
    else if ((cmd_obj != NULL) && (index == UART_DEVICE_3) && (mcu_id == MCU2))
    {    
        mcu_command_table_handler(&mcu2_cmd_handler, cmd_obj, str);              
    }
}

static void mcu_json_response_handler(uint8_t index, const struct mcu_response_object * rsp_obj, const char* str)
{
    if (rsp_obj != NULL)
    {        
         mcu_response_table_handler(&mcu0_rsp_handler, rsp_obj, str);                
    }
}

static void mcu_json_variable_handler(uint8_t index, const struct mcu_command_object * cmd_obj, const char* str)
{
        
    if ((cmd_obj != NULL) && (index == UART_DEVICE_1) && (mcu_id == MCU0))
    {        
        mcu_variable_table_handler(&mcu0_var_handler, cmd_obj, str);                
    }    
    else if ((cmd_obj != NULL) && (index == UART_DEVICE_2) && (mcu_id == MCU1))
    {
        mcu_variable_table_handler(&mcu1_var_handler, cmd_obj, str);               
    }    
    else if ((cmd_obj != NULL) && (index == UART_DEVICE_3) && (mcu_id == MCU2))
    {    
        mcu_variable_table_handler(&mcu2_var_handler, cmd_obj, str);              
    }
}

void __dbgopt__ mcu_json_command_proc(uint8_t index, mcu_comm_data_s* data) 
{

    char str_2[FRAME_DATA_SIZE] = {'\0'};
    strncpy(str_2, data->p_data, data->size);
    JSON_debug_print("received data value: %s\n\r", str_2);
    
    
    if( ((index == UART_DEVICE_1) && (mcu_id == MCU0)) || 
        ((index == UART_DEVICE_2) && (mcu_id == MCU1)) || 
        ((index == UART_DEVICE_3) && (mcu_id == MCU2)) )
    {
        // MCU 0 handle data received from the host via UART 0 RX 
        struct mcu_command_object * cmd_obj = malloc(sizeof(struct mcu_command_object));
        int struct_index = 255;
        int status = mcu_json_command_object_read((const char *)str_2, cmd_obj, &struct_index);
        if (status == 0) 
        {    
            if (struct_index == 0)
            {
                JSON_debug_print("CMD: %s\r\n", cmd_obj->command);
    	        JSON_debug_print("MCU: %d\r\n", cmd_obj->mcu);
                JSON_debug_print("STATE: %s\r\n", cmd_obj->data.state);
                mcu_json_command_handler(index, cmd_obj, (const char *)str_2);
            }    
            
            if (struct_index == 1)
            {
                JSON_debug_print("CMD: %s\r\n", cmd_obj->command);
    	        JSON_debug_print("MCU: %d\r\n", cmd_obj->mcu);
                JSON_debug_print("VAR: %s\r\n", cmd_obj->data.var_real.var_name);
                JSON_debug_print("VAL: %f\r\n", cmd_obj->data.var_real.var_value);  
                mcu_json_variable_handler(index, cmd_obj, (const char *)str_2);
            }
    
            if (struct_index == 2)
            {
                JSON_debug_print("CMD: %s\r\n", cmd_obj->command);
                JSON_debug_print("MCU: %d\r\n", cmd_obj->mcu);
                JSON_debug_print("VALUE: %d\r\n", cmd_obj->data.var_value);
                mcu_json_command_handler(index, cmd_obj, (const char *)str_2);
            } 
        }
        else 
        {
            //puts(json_error_string(status));
            JSON_debug_print(json_error_string(status));
        }
    
        free(cmd_obj);    
    
    }
    else if (((index == UART_DEVICE_2) || (index == UART_DEVICE_3)) && (mcu_id == MCU0))
    {      
        // directly forward received data from MCU1 or MCU2 to host (if feature enabled)
        if (strstr((char *)str_2, "RSP") != NULL)
        {
            send_string(UART_DEVICE_1, (char *)str_2, head_data_JSON);
            usleep(10000);
        }
        else if(Slave_Data_Fwd) // combine with previous
        {
            send_string(UART_DEVICE_1, (char *)str_2, head_data_JSON);
            usleep(10000);
        }
        
        int struct_index;
        int status;
            
        // handle stream object first as this will occur frequently
        struct mcu_stream_object * stream_obj = malloc(sizeof(struct mcu_stream_object));
        struct_index = 255;
        status = mcu_json_stream_object((const char *)str_2, stream_obj, &struct_index);
        if (status == 0) // found a stream object
        {
#if !FACTORY_RESET
            if (struct_index < 4) // common fields first... not interested in data unless they are azm/inc values going into arrays
            {
                JSON_debug_print1("MCU: %d\r\n", stream_obj->mcu);
                JSON_debug_print1("ANGLE: %.1f\r\n", stream_obj->angle);
                JSON_debug_print1("DBFS: %.1f\r\n", stream_obj->dbfs);
                JSON_debug_print1("NMPA: %d\r\n", stream_obj->nmpa);
                JSON_debug_print1("STATE: %s\r\n", stream_obj->state);

                if (struct_index == 1) // adds DUT field but MCU0 does not need
                {
                    JSON_debug_print1("ANGLE_DUT: %.1f\r\n", stream_obj->angle_dut);
                    JSON_debug_print1("DBFS_DUT: %.1f\r\n", stream_obj->dbfs_dut);
                }
                else if (struct_index == 2) // adds AZM/INC... MCU0 needs these
                {
                    JSON_debug_print1("ANGLE_AZM: %.1f\r\n", stream_obj->angle_azm);
                    JSON_debug_print1("ANGLE_INC: %.1f\r\n", stream_obj->angle_inc);
                    JSON_debug_print1("DBFS_AZM: %.1f\r\n", stream_obj->dbfs_azm);
                    JSON_debug_print1("DBFS_INC: %.1f\r\n", stream_obj->dbfs_inc);
                    all_bf_data.azm.angle[stream_obj->mcu] = stream_obj->angle_azm;
                    all_bf_data.azm.dbfs[stream_obj->mcu] = stream_obj->dbfs_azm;
                    all_bf_data.inc.angle[stream_obj->mcu] = stream_obj->angle_inc;
                    all_bf_data.inc.dbfs[stream_obj->mcu] = stream_obj->dbfs_inc;
                }
                else if (struct_index == 3) // adds DUT
                {
                    JSON_debug_print1("ANGLE_DUT: %.1f\r\n", stream_obj->angle_dut);
                    JSON_debug_print1("DBFS_DUT: %.1f\r\n", stream_obj->dbfs_dut);
                    JSON_debug_print1("ANGLE_AZM: %.1f\r\n", stream_obj->angle_azm);
                    JSON_debug_print1("ANGLE_INC: %.1f\r\n", stream_obj->angle_inc);
                    JSON_debug_print1("DBFS_AZM: %.1f\r\n", stream_obj->dbfs_azm);
                    JSON_debug_print1("DBFS_INC: %.1f\r\n", stream_obj->dbfs_inc);
                }
            }
            else 
            {
                //puts(json_error_string(status));
                JSON_debug_print1(json_error_string(status));
            }
            free(stream_obj);
#endif
        }
        else
        {
            struct mcu_response_object * rsp_obj = malloc(sizeof(struct mcu_response_object));
            struct_index = 255;
            status = mcu_json_response_object_read((const char *)str_2, rsp_obj, &struct_index);
            if (status == 0) 
            {
                if (struct_index == 0)
                {
                    JSON_debug_print("CMD: %s\r\n", rsp_obj->response_command);
    	            JSON_debug_print("MCU: %d\r\n", rsp_obj->mcu);
                    JSON_debug_print("STATE: %s\r\n", rsp_obj->data.state);
                    mcu_json_response_handler(index, rsp_obj, (const char *)str_2);
                }
                else if (struct_index == 1)
                {
                    JSON_debug_print("CMD: %s\r\n", rsp_obj->response_command);
    	            JSON_debug_print("MCU: %d\r\n", rsp_obj->mcu);
                    JSON_debug_print("VALUE: %f\r\n", rsp_obj->data.var_value);
                    mcu_json_response_handler(index, rsp_obj, (const char *)str_2);
                }
            }
            else 
            {
                //puts(json_error_string(status));
                JSON_debug_print(json_error_string(status));
            }
            free(rsp_obj);
        }
    }       
}



/**************************************************************************************************************************************************
*
* Macro to generate SET & GET functions for any type of JSON variable 
*
**************************************************************************************************************************************************/
#define BUILD_MCU_VARIABLE_HANDLE_FUNCTIONS(var_type, var_fmt)                                                                                                      \
static void __attribute__((optimize(0))) set_modify_##var_type##_var_val(void* var_addr, void* var_val, void* var_array_idx)                                        \
{   /**********************************************************************************************************************/                                        \
    /* NOTE: This function MUST be declared with "__attribute__((optimize(0)))" as it will be optimzed out during compile */                                        \
    /**********************************************************************************************************************/                                        \
    var_type var_val_i = (var_type)*((double *)var_val);                            /* temp holder - seems to break assignbment */                                  \
    uint8_t var_array_idx_i = *((uint8_t *)var_array_idx);                                                                                                          \
    if(var_array_idx)                                                                                                                                               \
        ((var_type *)var_addr)[var_array_idx_i] = var_val_i; /* var as array is already identified by pointer*/                                                     \
    else                                                                                                                                                            \
        *((var_type *)var_addr) = var_val_i; /* may be modified more easily in this form */                                                                         \
    if(false)                                                                                                                                                       \
    {                                                                                                                                                               \
        char temp_str[4] = { '\0' };                                                                                                                                \
        if(var_array_idx)                                                                                                                                           \
            sprintf(temp_str, "[%1d]", var_array_idx_i);                                                                                                            \
        mcu_stdio_printf("\n\r\n\r" # var_type "_var_val%s<set> = %" # var_fmt "\n\r", temp_str, var_val_i); /* prints input val passed */                          \
        mcu_stdio_printf(# var_type "_var_val%s<get> = %" # var_fmt "\n\r", temp_str, *((var_type*)var_addr)); /* prints new val from updated target location */    \
    }                                                                                                                                                               \
}                                                                                                                                                                   \
                                                                                                                                                                    \
static void __dbgopt__ set_mcu_##var_type##_variable(void* mcu_idx, void* var_name, void* var_is_array, void* var_addr, void* var_val, void* set_modify_handler)    \
{                                                                                                                                                                   \
    uint8_t mcu_index = *(uint8_t *)mcu_idx;                                                                                                                        \
    char variable_name[32] = { '\0' };                                                                                                                              \
    strcpy(variable_name, (char *)var_name);                                                                                                                        \
    bool var_is_array_i = *(bool *)var_is_array;                                                                                                                    \
    var_type var_val_i = (var_type)*((double *)var_val);                                                                                                            \
    var_type var_val_fetch;                                                                                                                                         \
    mcu_set_modify_var_handle_func_t set_modify_handler_ = (void*)(set_modify_handler);                                                                             \
                                                                                                                                                                    \
    char strTemp[128];                                                                                                                                              \
                                                                                                                                                                    \
    if (mcu_id == mcu_index)                                                                                                                                        \
    {                                                                                                                                                               \
        if (set_modify_handler)                                                                                                                                     \
            set_modify_handler_(var_addr, var_val, var_is_array_i ? mcu_idx : NULL); /* If NULL passed then is not array... otherwise must... */                    \
        else                                                                                                                                                        \
            set_modify_ ##var_type## _var_val(var_addr, var_val, var_is_array_i ? mcu_idx : NULL); /* ... be and this is the pointer to index  */                   \
                                                                                                                                                                    \
        var_val_fetch = var_is_array_i ? ((var_type *)var_addr)[mcu_index] : *((var_type *)var_addr);                                                               \
        sprintf(strTemp, "{\"RSP\":\"SET\", \"MCU\":%d, \"VAR\":\"%s\", \"VAL\":%" #var_fmt "}", mcu_index, variable_name, var_val_fetch);                          \
        send_string(mcu_id, strTemp, head_data_JSON);                                                                                                               \
    }                                                                                                                                                               \
    else if (mcu_id == MCU0)                                                                                                                                        \
    {                                                                                                                                                               \
        if (mcu_index != MCU3)                                                                                                                                      \
        {                                                                                                                                                           \
            /* pass cmd to other mcu */                                                                                                                             \
            sprintf(strTemp, "{\"CMD\":\"SET\", \"MCU\":%d, \"VAR\":\"%s\", \"VAL\":%" #var_fmt "}", mcu_index, variable_name, var_val_i);                          \
            send_string(mcu_index, strTemp, head_data_JSON);                                                                                                        \
        }                                                                                                                                                           \
        else /* multicast... MCU0 needs to set parameters for self (including any that it uses to manage its part of interaction with slaves */                     \
        { /* Need to call set_modify handler in this case as well so matching array on MCU0 can be updated with slaves... slaves OFF issue */                       \
            for(int mcu = 0 ; mcu < UART_DEVICE_MAX ; mcu++)                                                                                                        \
            {                                                                                                                                                       \
                if (set_modify_handler)                                                                                                                             \
                    set_modify_handler_(var_addr, var_val, var_is_array_i ? &mcu_number[mcu] : NULL);                                                               \
                else                                                                                                                                                \
                    set_modify_##var_type##_var_val(var_addr, var_val, var_is_array_i ? &mcu_number[mcu] : NULL);                                                   \
            }                                                                                                                                                       \
                                                                                                                                                                    \
            var_val_fetch = var_is_array_i ? ((var_type *)var_addr)[MCU0] : *((var_type *)var_addr);       /* Not verifying array values for otehr MCU's on MCU0 */ \
            sprintf(strTemp, "{\"RSP\":\"SET\", \"MCU\":0, \"VAR\":\"%s\", \"VAL\":%" #var_fmt "}", variable_name, var_val_fetch);                                  \
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);                                                                                       \
                                                                                                                                                                    \
            sprintf(strTemp, "{\"CMD\":\"SET\", \"MCU\":1, \"VAR\":\"%s\", \"VAL\":%" #var_fmt "}", variable_name, var_val_i);                                      \
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);                                                                                       \
            sprintf(strTemp, "{\"CMD\":\"SET\", \"MCU\":2, \"VAR\":\"%s\", \"VAL\":%" #var_fmt "}", variable_name, var_val_i);                                      \
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);                                                                                       \
        }                                                                                                                                                           \
    }                                                                                                                                                               \
}                                                                                                                                                                   \
                                                                                                                                                                    \
/* para_1: address of internal double variable, para_2: address of external variable */                                                                             \
static void __dbgopt__ get_mcu_##var_type##_variable(void* mcu_idx, void* var_name, void* var_is_array, void* var_addr, void* param_5, void* param_6)               \
{                                                                                                                                                                   \
    uint8_t mcu_index = *(uint8_t *)mcu_idx;                                                                                                                        \
    char variable_name[32] = { '\0' };                                                                                                                              \
    strcpy(variable_name, (char *)var_name);                                                                                                                        \
    bool var_is_array_i = *(bool *)var_is_array;                                                                                                                    \
    var_type var_val_fetch;                                                                                                                                         \
                                                                                                                                                                    \
    char strTemp[128];                                                                                                                                              \
    if (mcu_id == mcu_index)                                                                                                                                        \
    {                                                                                                                                                               \
        var_val_fetch = var_is_array_i ? ((var_type *)var_addr)[mcu_index] : *((var_type *)var_addr);                                                               \
        sprintf(strTemp, "{\"RSP\":\"GET\", \"MCU\":%d, \"VAR\":\"%s\", \"VAL\":%" #var_fmt "}", mcu_index, variable_name, var_val_fetch);                          \
        send_string(mcu_id, strTemp, head_data_JSON);                                                                                                               \
    }                                                                                                                                                               \
    else if (mcu_id == MCU0)                                                                                                                                        \
    {                                                                                                                                                               \
        if (mcu_index != MCU3)                                                                                                                                      \
        {                                                                                                                                                           \
            /* forward cmd to other mcu */                                                                                                                          \
            sprintf(strTemp, "{\"CMD\":\"GET\", \"MCU\":%d, \"VAR\":\"%s\"}", mcu_index, variable_name);                                                            \
            send_string(mcu_index, strTemp, head_data_JSON);                                                                                                        \
        }                                                                                                                                                           \
        else                                                                                                                                                        \
        {                                                                                                                                                           \
            var_val_fetch = var_is_array_i ? ((var_type *)var_addr)[MCU0] : *((var_type *)var_addr);                                                                \
            sprintf(strTemp, "{\"RSP\":\"GET\", \"MCU\":0, \"VAR\":\"%s\", \"VAL\":%" #var_fmt "}", variable_name, var_val_fetch);                                  \
            if (false) printf("%s\n", strTemp);                                                                                                                     \
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);                                                                                       \
                                                                                                                                                                    \
            sprintf(strTemp, "{\"CMD\":\"GET\", \"MCU\":1, \"VAR\":\"%s\"}", variable_name);                                                                        \
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);                                                                                       \
            sprintf(strTemp, "{\"CMD\":\"GET\", \"MCU\":2, \"VAR\":\"%s\"}", variable_name);                                                                        \
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);                                                                                       \
        }                                                                                                                                                           \
    }                                                                                                                                                               \
}                                                                                                                                                                   \


/**********************************************************
 *
 * Var SET/GET function builders
 * Builds type appropriate functons from macro template 
 * 1st arg is the data type, 2nd is the format string 
 *
 **********************************************************/
BUILD_MCU_VARIABLE_HANDLE_FUNCTIONS(uint32_t, d)

#if !FACTORY_RESET
BUILD_MCU_VARIABLE_HANDLE_FUNCTIONS(uint16_t, d)
BUILD_MCU_VARIABLE_HANDLE_FUNCTIONS(uint8_t, d)
BUILD_MCU_VARIABLE_HANDLE_FUNCTIONS(float, .7g) // .7g ensures that rounding error created by double-float conversion is ignored
BUILD_MCU_VARIABLE_HANDLE_FUNCTIONS(double, g)
BUILD_MCU_VARIABLE_HANDLE_FUNCTIONS(bool, d)
    
/*********************************************************************************
 * CUSTOM DATA PRE-PROCESSORS
 *********************************************************************************
 * To create a custom variable update function expand the appropriate function macro
 * just above this section and copy the expanded set_modify<<type>>_var_val function
 * to this section and rename as required. This provides a shell of the update function 
 * that will write the variable with whatever is passed to the function by default.
 * 
 * We can do whatever we want in here. If we are updating a global variable then 
 * that can be done directly. If we need to call another function which does 
 * advanced handlng then that can be done as well.
 * Just add the instructions to the new function.
 * 
 *********************************************************************************/

/**************************************************************************************************************************/
/**************************************************************************************************************************/
/*                                                                                                                        */
/* NOTE: These functions MUST be declared with "__attribute__((optimize(0)))" or they will be optimzed out during compile */
/*                                                                                                                        */
/**************************************************************************************************************************/
/**************************************************************************************************************************/   
    
static void __attribute__((optimize(0))) set_modify_uint32_t_val_square(void* var_addr, void* var_val, void* var_array_idx)
{
    uint32_t var_val_i = (uint32_t)*((double *)var_val);
    *((uint32_t*)var_addr) = var_val_i*var_val_i;
    if (0)
    {
        mcu_stdio_printf("\n\r\n\ruint32_t_var_val<set> = %d\n\r", var_val_i);
        mcu_stdio_printf("uint32_t_var_val<get> = %d\n\r", *((uint32_t*)var_addr));
    }
}

static void __attribute__((optimize(0))) set_modify_uint32_t_val_cube(void* var_addr, void* var_val, void* var_array_idx)
{
    double var_val_i = (double)*((double *)var_val);
    *((double*)var_addr) = var_val_i*var_val_i*var_val_i;
    if (0)
    {
        mcu_stdio_printf("\n\r\n\rdouble_var_val<set> = %g\n\r", var_val_i);
        mcu_stdio_printf("double_var_val<get> = %g\n\r", *((double*)var_addr));
    }
}


static void __attribute__((optimize(0))) set_modify_BP_Low_val(void* var_addr, void* var_val, void* var_array_idx)
{
    uint32_t var_val_i = (uint32_t)*((double *)var_val);
    set_apu_bp_low_freq(var_val_i);  // self-contained function call that updated var as well as other dependent vars 
    if (0)
    {
        mcu_stdio_printf("\n\r\n\rBP_Low<set> = %d\n\r", var_val_i);
        mcu_stdio_printf("BP_Low<get> = %d\n\r", *((uint32_t*)var_addr));
    }
}


static void __attribute__((optimize(0))) set_modify_BP_High_val(void* var_addr, void* var_val, void* var_array_idx)
{
    uint32_t var_val_i = (uint32_t)*((double *)var_val);
    set_apu_bp_low_freq(var_val_i);  // self-contained function call that updated var as well as other dependent vars 
    if (0)
    {
        mcu_stdio_printf("\n\r\n\rBP_High<set> = %d\n\r", var_val_i);
        mcu_stdio_printf("BP_High<get> = %d\n\r", *((uint32_t*)var_addr));
    }
}


static void set_modify_APU_Gain_val(void* var_addr, void* var_val, void* var_array_idx)
{
    float var_val_i = (float)*((double *)var_val);
    set_apu_gain(var_val_i);
    if (0)
    {
        mcu_stdio_printf("\n\r\n\rAPU_Gain<set> = %7g\n\r", var_val_i);
        mcu_stdio_printf("APU_Gain<get> = %7g\n\r", *((float *)var_addr));
    }
}


static void set_modify_IN_FOV_Angle_LVL_val(void* var_addr, void* var_val, void* var_array_idx)
{
    float var_val_i = (float)*((double *)var_val);
    if (var_val_i < 0)
        *((float *)var_addr) = 0;
    else if(var_val_i > ON_TARGET_Angle_LVL)
        *((float *)var_addr) = ON_TARGET_Angle_LVL;
    else if (var_val_i > 180)
        *((float *)var_addr) = 0;
    else
        *((float *)var_addr) = var_val_i;
}

static void set_modify_ON_TARGET_Angle_LVL_val(void* var_addr, void* var_val, void* var_array_idx)
{
    float var_val_i = (float)*((double *)var_val);
    if (var_val_i < 0)
        *((float *)var_addr) = 0;
    else if(var_val_i < IN_FOV_Angle_LVL)
        *((float *)var_addr) = IN_FOV_Angle_LVL;
    else if (var_val_i > 180)
        *((float *)var_addr) = 0;
    else
        *((float *)var_addr) = var_val_i;
}

static void set_modify_Angle_Hysteresis_val(void* var_addr, void* var_val, void* var_array_idx)
{
    float var_val_i = (float)*((double *)var_val);
    if (var_val_i < 0)
        *((float *)var_addr) = 0;
    else if (var_val_i > (IN_FOV_Angle_LVL-ON_TARGET_Angle_LVL))
        *((float *)var_addr) = (IN_FOV_Angle_LVL-ON_TARGET_Angle_LVL);
    else if (var_val_i > 20)
        *((float *)var_addr) = 20;
    else
        *((float *)var_addr) = var_val_i;
}

static void set_modify_Angle_BF_AZM_static_val(void* var_addr, void* var_val, void* var_array_idx)
{
    float var_val_i = (float)*((double *)var_val);
    set_FOV_Angle(var_val_i);
    if (0)
    {
        mcu_stdio_printf("\n\r\n\rFOV_Angle<set> = %7g\n\r", var_val_i);
        mcu_stdio_printf("FOV_Angle<get> = %7g\n\r", *((float *)var_addr));
    }
}

static void set_modify_Angle_BF_INC_static_val(void* var_addr, void* var_val, void* var_array_idx)
{
    float var_val_i = (float)*((double *)var_val);
    set_AZM_Angle(var_val_i);
    if (0)
    {
        mcu_stdio_printf("\n\r\n\rAZM_Angle<set> = %7g\n\r", var_val_i);
        mcu_stdio_printf("AZM_Angle<get> = %7g\n\r", *((float *)var_addr));
    }
}

static void __attribute__((optimize(0))) set_modify_Host_Update_Interval_val(void* var_addr, void* var_val, void* var_array_idx)
{
    uint8_t var_val_i = (uint8_t)*((double *)var_val);
    if (var_val_i < 1)
        Host_Update_Interval = 1;
    else if (var_val_i > 64)
        Host_Update_Interval = 64;
    else 
        Host_Update_Interval = var_val_i;
}

static void __attribute__((optimize(0))) set_modify_MPA_N_val(void* var_addr, void* var_val, void* var_array_idx)
{
    uint8_t var_val_i = (uint8_t)*((double *)var_val);

    if (var_val_i < 1)
        var_val_i = 1;
    else if (var_val_i > MPA_NMAX)
        var_val_i = MPA_NMAX;
    
    if (var_val_i != MPA_N)
    {
        MPA_N_changed = true;
        MPA_N = var_val_i;
    }
}

static void __attribute__((optimize(0))) set_modify_AZM_INC_BF_Duty_Cycle_val(void* var_addr, void* var_val, void* var_array_idx)
{
    uint8_t var_val_i = (uint8_t)*((double *)var_val);
    if (var_val_i < 1)
        AZM_INC_BF_Duty_Cycle = 1;
    else if (var_val_i > 128)
        AZM_INC_BF_Duty_Cycle = 128;
    else 
        AZM_INC_BF_Duty_Cycle = var_val_i;
}

#endif

/************************************************************************************************/
/*  Special handling for Vars that are supported in the Factory Reset build go here            */
/************************************************************************************************/

static void __attribute__((optimize(0))) set_modify_set_Uart_Check_Interval(void* var_addr, void* var_val, void* var_array_idx)
{
    uint32_t var_val_i = (uint32_t)*((double *)var_val);
    uint8_t var_array_idx_i = *((uint8_t *)var_array_idx);
    if (var_array_idx)
        ((uint32_t*)var_addr)[(uint32_t)var_array_idx_i] = var_val_i;
    else
        *((uint32_t*)var_addr) = var_val_i;

    Uart_Check_Interval[var_array_idx_i] = var_val_i;
    if (var_val_i != 0)
    {
        timer_set_enable(TIMER_DEVICE_1, var_array_idx_i, 1);
        uart_health_check_flag[var_array_idx_i] = 1;
    }
    else
    {
        timer_set_enable(TIMER_DEVICE_1, var_array_idx_i, 0);
        uart_health_check_flag[var_array_idx_i] = 0;
        timer1_fire_count[var_array_idx_i] = 0;
    }
    
    if (0)
    {
        mcu_stdio_printf("\n\r\n\rUart_Check_Interval<set> = %d\n\r", var_val_i);
        mcu_stdio_printf("Uart_Check_Interval<get> = %d\n\r", ((uint32_t*)var_addr)[(uint32_t)var_array_idx_i]);
    }
}


/************************************************************************************************/
/************************************************************************************************/

static void get_battery_voltage_reading(void* para1, void* para2)
{
    char strTemp[32];

    // 50% voltage divider at input to U21 (volts ADC)
    float fValue = get_battery_voltage(3.3 * 2);
    // repack in JSON mssg
    sprintf(strTemp, "{\"RSP\":\"RBV\", \"VALUE\":%3.2f}", fValue);    
    send_string(UART_DEVICE_1, strTemp, head_data_JSON);
}

static void get_battery_current_reading(void* para1, void* para2)
{
    char strTemp[32];

    // 50:1 current sense amplifier, 100 mohm current sense resistor, A->mA
    float fValue = get_battery_current(3.3 / 50 / 0.1 * 1000);
    // repack in JSON mssg
    sprintf(strTemp, "{\"RSP\":\"RBC\", \"VALUE\":%4.1f}", fValue);    
    send_string(UART_DEVICE_1, strTemp, head_data_JSON);
}

#if !FACTORY_RESET
static void configure_mcu_host_report(void* mcu_idx, void* enable_flg)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    uint8_t enable_flag = *(uint8_t*)enable_flg;
    char strTemp[128];

    if (mcu_id == mcu_index)
    {        
        Enable_Host_Reporting = enable_flag;
        sprintf(strTemp, "{\"RSP\":\"HEN\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"HEN\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            Enable_Host_Reporting = enable_flag;
            sprintf(strTemp, "{\"RSP\":\"HEN\", \"MCU\":0, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"HEN\", \"MCU\":1, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"HEN\", \"MCU\":2, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
}
#endif

static void get_charge_status(void* para_1, void* para_2)
{
    // read charge status
    char strTemp[64];
    int temp = gpio_get_pin(GPIO_4_ACPRn);
    int chargeStatus = temp;
    temp = gpio_get_pin(GPIO_5_BAT_FAULTn);
    chargeStatus += (temp << 1);
    temp = gpio_get_pin(GPIO_6_CHRGn);
    chargeStatus += (temp << 2);
    sprintf(strTemp, "{\"RSP\":\"RCS\", \"VALUE\":%d}", chargeStatus);    
    send_string(UART_DEVICE_1, strTemp, head_data_JSON);
}

static void set_factory_reset_firmware(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t *)mcu_idx;
    char strTemp[128];
    uint8_t version[16];

    if (mcu_id == mcu_index)
    {
        read_bootloader_version_string(version);
        if (0 == strcmp((const char *)version, "v1.0.1"))
        {
            sprintf(strTemp, "{\"RSP\":\"RFRF\", \"MCU\":%d, \"STATE\":\"Not Supported\"}", mcu_index);
            send_string(mcu_id, strTemp, head_data_JSON);
        }
        else
        {
            mcu_wdt_enable(WDT_DEVICE_0, false);
            mcu_wdt_enable(WDT_DEVICE_1, false);
            mcu_copy_factory_reset_config_to_config_sector();
            mcu_flash_erase_app_area(LAYOUT_APP_A_OFFSET, LAYOUT_APP_A_SIZE);
            mcu_flash_erase_app_area(LAYOUT_APP_B_OFFSET, LAYOUT_APP_B_SIZE);
            sprintf(strTemp, "{\"RSP\":\"RFRF\", \"MCU\":%d}", mcu_index);
            send_string(mcu_id, strTemp, head_data_JSON);
            usleep(10000);
            sysctl_reset(SYSCTL_RESET_SOC);
        }
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"RFRF\", \"MCU\":%d}", mcu_index);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            // forward command to MCU1 and MCU2
            sprintf(strTemp, "{\"CMD\":\"RFRF\", \"MCU\":1}");
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"RFRF\", \"MCU\":2}");
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);

            // MCU0 response
            read_bootloader_version_string(version);
            if (0 == strcmp((const char *)version, "v1.0.1"))
            {
                sprintf(strTemp, "{\"RSP\":\"RFRF\", \"MCU\":0, \"STATE\":\"Not Supported\"}");
                send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);
            }
            else
            {
                mcu_wdt_enable(WDT_DEVICE_0, false);
                mcu_wdt_enable(WDT_DEVICE_1, false);
                mcu_copy_factory_reset_config_to_config_sector();
                mcu_flash_erase_app_area(LAYOUT_APP_A_OFFSET, LAYOUT_APP_A_SIZE);
                mcu_flash_erase_app_area(LAYOUT_APP_B_OFFSET, LAYOUT_APP_B_SIZE);
                sprintf(strTemp, "{\"RSP\":\"RFRF\", \"MCU\":0}");
                send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);
                usleep(500000); // 250ms isn;t quite enough time so make it 500ms
                Restart_MCU0 = true;
            }
        }
    }
}

static void enable_system_reset(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    char strTemp[128];
    
    if (mcu_id == mcu_index)
    {
        sprintf(strTemp, "{\"RSP\":\"RST\", \"MCU\":%d}", mcu_index);
        send_string(mcu_id, strTemp, head_data_JSON);
        usleep(1000000);
        mcu_reboot();
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"RST\", \"MCU\":%d}", mcu_index);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            sprintf(strTemp, "{\"CMD\":\"RST\", \"MCU\":1}");
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"RST\", \"MCU\":2}");
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"RSP\":\"RST\", \"MCU\":0}");
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);
            usleep(1000000);
            mcu_reboot();
        }
    }
}

static void get_status(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    char strTemp[128], strTemp2[64];
    char temp[64]; 
    memset(temp, '\0', 64);
    
    if (mcu_id == mcu_index)
    {
        sprintf(strTemp2, "{\"Last_Restart\":\"%s\"}", get_last_reset_reason()); 
        sprintf(strTemp, "{\"RSP\":\"STAT\", \"MCU\":%d, \"VALUE\":%s}", mcu_index, strTemp2); 
        send_string(mcu_index, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"STAT\", \"MCU\":%d}", mcu_index);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            sprintf(strTemp, "{\"CMD\":\"STAT\", \"MCU\":1}");
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"STAT\", \"MCU\":2}");
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);

            sprintf(strTemp2, "{\"Last_Restart\":\"%s\"}", get_last_reset_reason()); 
            sprintf(strTemp, "{\"RSP\":\"STAT\", \"MCU\":0, \"VALUE\":%s}", strTemp2); 
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);
        }
    }
}

static void enable_watchdog(void* mcu_idx, void* enable_flg)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    uint8_t enable_flag = *(uint8_t*)enable_flg;
    char strTemp[128];
    
    if (mcu_id == mcu_index)
    {
        WDT0_Enable = enable_flg;
        WDT1_Enable = enable_flg;
        mcu_wdt_enable(WDT_DEVICE_0, WDT0_Enable);
        mcu_wdt_enable(WDT_DEVICE_1, WDT1_Enable);
        sprintf(strTemp, "{\"RSP\":\"WDT\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"WDT\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            WDT0_Enable = enable_flg;
            WDT1_Enable = enable_flg;
            mcu_wdt_enable(WDT_DEVICE_0, WDT0_Enable);
            mcu_wdt_enable(WDT_DEVICE_1, WDT1_Enable);
            sprintf(strTemp, "{\"RSP\":\"WDT\", \"MCU\":0, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"WDT\", \"MCU\":1, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"WDT\", \"MCU\":2, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
    
    
}

    
static void configure_battery_charge(void* para_1, void* enable_flg)
{
    uint8_t enable_flag = *(uint8_t*)enable_flg;
    char strTemp[64];
    Enable_Charger = enable_flag;
    gpio_set_pin(GPIO_7_CHARGE_EN, (gpio_pin_value_t)Enable_Charger);
    sprintf(strTemp, "{\"RSP\":\"CHEN\", \"STATE\":\"%s\"}", config_str[enable_flag]);        
    send_string(UART_DEVICE_1, strTemp, head_data_JSON);
}


static void configure_mcu_power(void* mcu_idx, void* enable_flg)
{
    uint8_t mcu_index = *(uint8_t *)mcu_idx;
    uint8_t enable_flag = *(uint8_t *)enable_flg;

    char strTemp[128];
    if (mcu_index & MCU1)
    {
        Enable_MCU1 = enable_flag;
        gpio_set_pin(GPIO_2_MCU1_EN, (gpio_pin_value_t)Enable_MCU1);
        sprintf(strTemp, "{\"RSP\":\"PWR\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index & 0x01, config_str[enable_flag]);
        send_string(mcu_id, strTemp, head_data_JSON);
    }

    if (mcu_index & MCU2)
    {
        Enable_MCU2 = enable_flag;
        gpio_set_pin(GPIO_3_MCU2_EN, (gpio_pin_value_t)Enable_MCU2);
        sprintf(strTemp, "{\"RSP\":\"PWR\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index & 0x02, config_str[enable_flag]);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
}

#if (!FACTORY_RESET)
static void configure_i2s_data_capture(void* mcu_idx, void* enable_flg)
{

    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    bool enable_flag = *(bool*)enable_flg;
    char strTemp[128];

    if (mcu_id == mcu_index)
    {        
        Enable_I2S_Data_Capture = enable_flag;
        Terminal_Mode = false;
        sprintf(strTemp, "{\"RSP\":\"IDC\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"IDC\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            Enable_I2S_Data_Capture = enable_flag;
            Terminal_Mode = false;
            sprintf(strTemp, "{\"RSP\":\"IDC\", \"MCU\":0, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"IDC\", \"MCU\":1, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"IDC\", \"MCU\":2, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
}


static void configure_i2s_soft_fft(void* mcu_idx, void* enable_flg)
{

    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    bool enable_flag = *(bool*)enable_flg;
    char strTemp[128];

    if (mcu_id == mcu_index)
    {        
        Enable_I2S_Data_Capture = enable_flag;
        Enable_I2S_Soft_FFT = enable_flag;
        Terminal_Mode = false;
        sprintf(strTemp, "{\"RSP\":\"ISF\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"ISF\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            Enable_I2S_Data_Capture = enable_flag;
            Enable_I2S_Soft_FFT = enable_flag;
            Terminal_Mode = false;
            sprintf(strTemp, "{\"RSP\":\"ISF\", \"MCU\":0, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"ISF\", \"MCU\":1, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"ISF\", \"MCU\":2, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
}


static void configure_i2s_hard_fft(void* mcu_idx, void* enable_flg)
{

    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    bool enable_flag = *(bool*)enable_flg;
    char strTemp[128];

    if (mcu_id == mcu_index)
    {        
        Enable_I2S_Data_Capture = enable_flag;
        Enable_I2S_Hard_FFT = enable_flag;
        Terminal_Mode = false;
        sprintf(strTemp, "{\"RSP\":\"IHF\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"IHF\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            Enable_I2S_Data_Capture = enable_flag;
            Enable_I2S_Hard_FFT = enable_flag;
            Terminal_Mode = false;
            sprintf(strTemp, "{\"RSP\":\"IHF\", \"MCU\":0, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"IHF\", \"MCU\":1, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"IHF\", \"MCU\":2, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
}
#endif

static void get_mcu_id(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    char strTemp[128];
    if (mcu_id == mcu_index)
    {
        sprintf(strTemp, "{\"RSP\":\"RID\", \"MCU\":%d, \"VALUE\":%d}", mcu_index, mcu_id);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"RID\", \"MCU\":%d}", mcu_index);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            sprintf(strTemp, "{\"RSP\":\"RID\", \"MCU\":0, \"VALUE\":%d}", mcu_id);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"RID\", \"MCU\":1}");
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"RID\", \"MCU\":2}");
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
    
}

#ifdef DEBUG
    #define CFG_STR "DEBUG"
#elif defined NDEBUG
    #define CFG_STR "RELEASE"
#else
    #define CFG_STR ""
#endif    
#if FACTORY_RESET
    #ifdef VCS_TAG
        #undef VCS_TAG
    #endif
    #define VCS_TAG FRF_FWV // override current version string when building stripped down factory reset version
#endif


#ifdef DEBUG
#define  SIZEOF_FW_VERSION (sizeof VCS_TAG - 1) + 10 + (sizeof VCS_SHORT_HASH - 1) + 3 + (sizeof VCS_EXTRA-1) + (sizeof VCS_EXTRA > 1 ? 1 : 0) + 6  // Add VCS_EXTRA[WIP or other] + DEBUG to end + NULL.
static char fw_version[SIZEOF_FW_VERSION];
#else
static char fw_version[sizeof VCS_TAG + (sizeof VCS_EXTRA - 1) + (sizeof VCS_EXTRA > 1 ? 1 : 0)];    // Only Major:Minor:Patch from CVS tag + EXTRA string if presentin version # string
#endif
void set_firmware_version_string()
{
#ifdef DEBUG
    const char *vcs_date_str = VCS_DATE;  // includes time... typically "2020-11-26t17:35:10z"
    char date_str[11];
    
    memset(date_str, '\0', sizeof date_str);
    memcpy(date_str, vcs_date_str, 10);
            
    memset(fw_version, '\0', SIZEOF_FW_VERSION);
    sprintf(fw_version, "%s_%s_%s%s%s_%s", VCS_TAG, date_str, VCS_SHORT_HASH, (sizeof VCS_EXTRA > 1 ? "_" : ""), VCS_EXTRA, CFG_STR);
#else
    sprintf(fw_version, "%s%s%s", VCS_TAG, (sizeof VCS_EXTRA > 1 ? "_" : ""), VCS_EXTRA);
#endif    
}    

char* get_firmware_version_string()
{
    return fw_version;
}

static void get_firmware_version(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    char strTemp[128];
    
    if (mcu_id == mcu_index)
    {
        sprintf(strTemp, "{\"RSP\":\"RFV\", \"MCU\":%d, \"VALUE\":\"%s\"}", mcu_index, get_firmware_version_string());
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"RFV\", \"MCU\":%d}", mcu_index);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            sprintf(strTemp, "{\"RSP\":\"RFV\", \"MCU\":0, \"VALUE\":\"%s\"}", get_firmware_version_string());
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"RFV\", \"MCU\":1}");
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"RFV\", \"MCU\":2}");
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
    
}

static void get_bootloader_version(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    char strTemp[128];
    uint8_t version[16];
	
    if (mcu_id == mcu_index)
    {
	    read_bootloader_version_string(version);
        sprintf(strTemp, "{\"RSP\":\"RBLV\", \"MCU\":%d, \"VALUE\":\"%s\"}", mcu_index, version);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"RBLV\", \"MCU\":%d}", mcu_index);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
	        read_bootloader_version_string(version);
            sprintf(strTemp, "{\"RSP\":\"RBLV\", \"MCU\":0, \"VALUE\":\"%s\"}", version);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"RBLV\", \"MCU\":1}");
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"RBLV\", \"MCU\":2}");
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
}

static void get_actual_baud_rate_setpoint(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    char strTemp[128];
    uint32_t baud_rate;
    //uint32_t baud_rate_r = *(uint32_t*)baudrate_set;
    
    if (mcu_id == mcu_index)
    {
        // clear flag on its UART0 of MCU0, or UART1 of MCU1, or UART2 of MCU2 
        uart_health_check_flag[mcu_index] = 0;
        //each MCU response with the actual baud rate it uses
        baud_rate = mcu_uart_get_actual_set_baud_rate(mcu_index);
        sprintf(strTemp, "{\"RSP\":\"RBR\", \"MCU\":%d, \"VALUE\":%d}", mcu_index, baud_rate);
        send_string(mcu_index, strTemp, head_data_JSON); 
        
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // MCU0 forward command to MCU1 or MCU2
            sprintf(strTemp, "{\"CMD\":\"RBR\", \"MCU\":%d}", mcu_index);
            send_string(mcu_index, strTemp, head_data_JSON);    
            
        }
        else
        {
            // MCU0 first response to host and clear the flag 
	        baud_rate = mcu_uart_get_actual_set_baud_rate(MCU0);
            sprintf(strTemp, "{\"RSP\":\"RBR\", \"MCU\":0, \"VALUE\":%d}", baud_rate);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);
            uart_health_check_flag[MCU0] = 0;
            
            // UART1 on MCU0 will forward command first and then change itself baud rate
            sprintf(strTemp, "{\"CMD\":\"RBR\", \"MCU\":1}");
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            usleep(10000);
            
            // UART 2 on MCU0 also forward command and then change itself baud rate
            sprintf(strTemp, "{\"CMD\":\"RBR\", \"MCU\":2}");
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
}


static uint8_t mcu_uart_validate_new_baud_rate(uint32_t new_baud_rate)
{
    uint8_t isValid = 1;
    if (new_baud_rate < 100 || new_baud_rate > 50000000)
    {
        isValid = 0;
    }
    return isValid;
}


static void set_new_baud_rate(void* mcu_idx, void* baudrate_set)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    char strTemp[128];
    uint32_t baud_rate;
    uint32_t baud_rate_r = (uint32_t)*(double*)baudrate_set;
	
    if (mcu_id == mcu_index)
    {
        // MCU0/1/2 response BAUD command
        if (mcu_uart_validate_new_baud_rate(baud_rate_r))
        {
            // save new baud rate first and then response using old baud rate
            mcu_uart_set_nominal_baud_rate(mcu_index, baud_rate_r);
            baud_rate = mcu_uart_get_actual_set_baud_rate(mcu_index);
            //MCU0, MCU1 or MCU2 response command 
            sprintf(strTemp, "{\"RSP\":\"BAUD\", \"MCU\":%d, \"VALUE\":%d}", mcu_index, baud_rate);
            send_string(mcu_index, strTemp, head_data_JSON);
            usleep(10000);  // need a bit time to wait Tx finish
            // change baud rate on UART0 of MCU0, UART1 of MCU1 or UART2 of MCU2
            mcu_uart_change_baud_rate(mcu_index, baud_rate_r);   
            uart_health_check_flag[mcu_index] = 1;
            //start timer to wait for RBR command
            timer_set_enable(TIMER_DEVICE_0, mcu_index, 1);
            //printf("mcu:%d starts timer for uart %d\r\n", mcu_id, mcu_index);
        }
        else
        {   
            baud_rate = mcu_uart_get_actual_set_baud_rate(mcu_index);
            sprintf(strTemp, "{\"RSP\":\"BAUD\", \"MCU\":%d, \"VALUE\":%d}", mcu_index, baud_rate);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        
    }
    else if (mcu_id == MCU0)
    {
        mcu0_receive_rsp_count = 0;
        if (mcu_index != MCU3)
        {
            if (mcu_uart_validate_new_baud_rate(baud_rate_r))
            {   
                // save new baud rate for UART1 or UART2 on MCU0
                mcu_uart_set_nominal_baud_rate(mcu_index, baud_rate_r);
                // MCU0 forward cmd to MCU1 or MCU2
                sprintf(strTemp, "{\"CMD\":\"BAUD\", \"MCU\":%d, \"VALUE\":%d}", mcu_index, baud_rate_r);
                send_string(mcu_index, strTemp, head_data_JSON);
            }
            else
            {
                //sprintf(strTemp, "{\"RSP\":\"BAUD\", \"MCU\":%d, \"VALUE\":\"INVALID\"}", mcu_index);
                //send_string(MCU0, strTemp, head_data_JSON);
                // MCU0 forward new baud rate to MCU1 or MCU2, but not save on MCU0
                sprintf(strTemp, "{\"CMD\":\"BAUD\", \"MCU\":%d, \"VALUE\":%d}", mcu_index, baud_rate_r);
                send_string(mcu_index, strTemp, head_data_JSON);
            }
        }
        else
        {
            // MCU:3 case
            if (mcu_uart_validate_new_baud_rate(baud_rate_r))
            {
                // MCU0 first save all new nominal value for each UART 
    	        mcu_uart_set_nominal_baud_rate(MCU0, baud_rate_r);
    	        mcu_uart_set_nominal_baud_rate(MCU1, baud_rate_r);
    	        mcu_uart_set_nominal_baud_rate(MCU2, baud_rate_r);
    	                        
                // UART1 on MCU0 will forward command first and then change itself baud rate
                sprintf(strTemp, "{\"CMD\":\"BAUD\", \"MCU\":1, \"VALUE\":%d}", baud_rate_r);
                send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
                
                // UART 2 on MCU0 also forward command and then change itself baud rate
                sprintf(strTemp, "{\"CMD\":\"BAUD\", \"MCU\":2, \"VALUE\":%d}", baud_rate_r);
                send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
                
                mcu0_receive_multicast_cmd = 1;
            }
            else
            {
                baud_rate = mcu_uart_get_actual_set_baud_rate(MCU0);
                sprintf(strTemp, "{\"RSP\":\"BAUD\", \"MCU\":%d, \"VALUE\":%d}", MCU0, baud_rate);
                send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);
                
                sprintf(strTemp, "{\"CMD\":\"BAUD\", \"MCU\":1, \"VALUE\":%d}", baud_rate_r);
                send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
                
                sprintf(strTemp, "{\"CMD\":\"BAUD\", \"MCU\":2, \"VALUE\":%d}", baud_rate_r);
                send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
            }
        }
    }
}




// MCU0 handle RSP:BAUD from MCU1/2
static void mcu0_handle_set_baudrate_response(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    if (mcu0_receive_multicast_cmd)
    {
        mcu0_receive_rsp_count++;
        //printf("mcu0_receive_rsp_count:%d from mcu:%d\r\n", mcu0_receive_rsp_count, mcu_index);
        usleep(10000);
    }
    
    // MCU0 change its UART1 and/or UART2 baud rate
    uint32_t baud_rate_r = mcu_uart_get_nominal_baud_rate(mcu_index);
    //printf("MCU0 Got BAUD RSP from MCU %d\r\n", mcu_index);
    mcu_uart_change_baud_rate(mcu_index, baud_rate_r); 
    // set the baud rate change flag
    uart_health_check_flag[mcu_index] = 1;
    // start the timer for UART1 or UART2 on MCU0
    timer_set_enable(TIMER_DEVICE_0, mcu_index, 1);  
    //printf("mcu0 starts timer for uart %d\r\n", mcu_index);
    
    // MCU0 receive both response from MCU1 and MCU2
    if (mcu0_receive_rsp_count == 2)
    {
        // MCU0 needs to response BAUD also and then change UART0 on MCU0 to new baud rate
        uint32_t baud_rate = mcu_uart_get_actual_set_baud_rate(MCU0);
        char strTemp[128];
        sprintf(strTemp, "{\"RSP\":\"BAUD\", \"MCU\":%d, \"VALUE\":%d}", MCU0, baud_rate);
        send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);    
        usleep(20000);
        baud_rate_r = mcu_uart_get_nominal_baud_rate(MCU0);
        mcu_uart_change_baud_rate(MCU0, baud_rate_r); 
        uart_health_check_flag[MCU0] = 1;
        timer_set_enable(TIMER_DEVICE_0, (uart_device_number_t)MCU0, 1);
        mcu0_receive_rsp_count = 0;
        mcu0_receive_multicast_cmd = 0;
    }
}

// MCU0 handle RSP:RBR from MCU1/2 
static void mcu0_handle_get_baudrate_response(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    // MCU0 clear flag for UART1 or UART2 when recv RBR from MCU1/2
    uart_health_check_flag[mcu_index] = 0;
    //("mcu0 get_baudrate_response from %d\r\n", mcu_index);
}



static void mcu0_handle_ota_slave_response(void* mcu_idx, void* para_2)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    char state[32] = {'\0'};
    strcpy(state, (char*)para_2);
    if (mcu0_receive_multicast_cmd)
    {
	    if ((strcmp(state, "COMPLETE") == 0) && (mcu_index == MCU1 || mcu_index == MCU2))
	    {		    
		    mcu0_receive_rsp_count++;
		    printf("Got COMPLETE from %d\r\n", mcu_index);
	    }
	    
        //printf("mcu0_receive_rsp_count:%d\r\n", mcu0_receive_rsp_count);
    }
    
    // MCU0 receive both response from MCU1 and MCU2
    if (mcu0_receive_rsp_count == 2)
    {
        mcu0_receive_rsp_count = 0;
        mcu0_receive_multicast_cmd = 0;
        _event_ota_s data;
        data.event_type = OTA_EVENT_FRAME_FINISHED;
        data.data = NULL;
        mcu_ota_event_handler(&data);
        
    }
}

static void mcu0_handle_slave_spi_ready_response(void* mcu_idx, void* para_2)
{
#if !FACTORY_RESET      
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    uint32_t slave_addr = 0;
  
    if (mcu_index == MCU1)
    {
        spi_master_init_slave_mcu1();
        slave_addr = spi_master_get_data_address_slave_mcu1();
        printf("master reads mcu %d slave address 0x%x\r\n", mcu_index, slave_addr);
    }

    if (mcu_index == MCU2)
    {
        spi_master_init_slave_mcu2();
        slave_addr = spi_master_get_data_address_slave_mcu2();
        printf("master reads mcu %d slave address 0x%x\r\n", mcu_index, slave_addr);
    }
#endif
}
void Set_LPM(bool status)
{ 
#if !FACTORY_RESET
    Enable_Host_Reporting = false;
    Enable_Barchart = false;
    Enable_Beamformer_Stats = false;
#endif
    if (status)
    {
        // switch slaves OFF
        Enable_MCU1 = false;
        Enable_MCU2 = false;
        gpio_set_pin(GPIO_2_MCU1_EN, (gpio_pin_value_t)Enable_MCU1);
        gpio_set_pin(GPIO_3_MCU2_EN, (gpio_pin_value_t)Enable_MCU2);

        // Disable Charger
        Enable_Charger = false;
        gpio_set_pin(GPIO_7_CHARGE_EN, (gpio_pin_value_t)Enable_Charger);

        // disable all peripheral clocks other than UART1
        for(int clk = SYSCTL_CLOCK_AI ; clk <= SYSCTL_CLOCK_RTC ; clk++)
        {
#ifdef DEBUG
            if (clk != SYSCTL_CLOCK_UART1 && clk != SYSCTL_CLOCK_GPIO && clk != SYSCTL_CLOCK_SPI1 && clk != SYSCTL_CLOCK_FPIOA)
#else
            if (clk != SYSCTL_CLOCK_UART1)
#endif            
                sysctl_clock_disable(clk);
        }

        System_Clock_Freq_Target = (SYSTEM_CLOCK_FREQ_MIN_MHZ * 1e6);
#if !FACTORY_RESET
        //update_all_i2s_fs(I2S_FS_LPM); 
#endif
    }
    else
    {
        System_Clock_Freq_Target = (SYSTEM_CLOCK_FREQ_INIT_MHZ + (SYSTEM_CLOCK_FREQ_OFFSET_MHZ * (int8_t)(mcu_id - 1))) * 1e6;
#if !FACTORY_RESET
        update_all_i2s_fs(i2s_fs_sel_saved);
#endif
    }

    System_Clock_Freq_Actual = sysctl_pll_set_freq(SYSCTL_PLL0, System_Clock_Freq_Target);
    uarths_debug_init();
}                        
                         
                         
static void Activate_LPM(void* para_1, void* enable_flg)
{                    
    if (!block_LPM)
    {
        printf("LPM NOT Blocked!\n");
        char strTemp[128];
        sprintf(strTemp, "{\"RSP\":\"LPM\"}");
        send_string(mcu_id, strTemp, head_data_JSON);
        usleep(100000); 
        Enable_LPM = true;
        Set_LPM(Enable_LPM);
    }
    else
    {
        printf("****************\n");
        printf("* LPM Blocked! *\n");
        printf("****************\n");
    }
}

#if !FACTORY_RESET

#if INCLUDE_STDIO_REMAP
void remap_stdio_debug(void* mcu_idx, void* enable_flg)
{
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    uint8_t enable_flag = *(uint8_t*)enable_flg;
    char strTemp[128];

    if (mcu_id == mcu_index)
    {
        Enable_Remap_STDIO = (bool)enable_flag;
        sprintf(strTemp, "{\"RSP\":\"DBG\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"DBG\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            Enable_Remap_STDIO = (bool)enable_flag;
            sprintf(strTemp, "{\"RSP\":\"DBG\", \"MCU\":0, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"DBG\", \"MCU\":1, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"DBG\", \"MCU\":2, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
    
    
}
#endif


void configure_console_menu(void* mcu_idx, void* enable_flg)
{
    // When STDIO is mapped to UART0 we have problem with non-printable characters in JSON header turning on the menu.
    // Could disable CTRL-T interception in this mode and have this command toggle the menu ON/OFF
    // Would need to inhibit all other output stream when menu is up
    uint8_t mcu_index = *(uint8_t*)mcu_idx;
    uint8_t enable_flag = *(uint8_t*)enable_flg;
    char strTemp[128];
    
    if (mcu_id == mcu_index)
    {
        Enable_Stdio = (bool)enable_flag;
        sprintf(strTemp, "{\"RSP\":\"MEN\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
        send_string(mcu_id, strTemp, head_data_JSON);
    }
    else if (mcu_id == MCU0)
    {
        if (mcu_index != MCU3)
        {
            // forward cmd to other mcu
            sprintf(strTemp, "{\"CMD\":\"MEN\", \"MCU\":%d, \"STATE\":\"%s\"}", mcu_index, config_str[enable_flag]);
            send_string(mcu_index, strTemp, head_data_JSON);
        }
        else
        {
            Enable_Stdio = (bool)enable_flag;
            sprintf(strTemp, "{\"RSP\":\"MEN\", \"MCU\":0, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU0, strTemp, head_data_JSON);

            sprintf(strTemp, "{\"CMD\":\"MEN\", \"MCU\":1, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU1, strTemp, head_data_JSON);
            sprintf(strTemp, "{\"CMD\":\"MEN\", \"MCU\":2, \"STATE\":\"%s\"}", config_str[enable_flag]);
            send_string((uart_device_number_t)MCU2, strTemp, head_data_JSON);
        }
    }
}
#endif


#define SET_MCU_VAR_HANDLE_TABLE_ENTRY(mcu, var_name, var_is_array, set_handler, get_handler, set_modify_handler)\
    {"SET", mcu, #var_name, (void*)set_handler, (void*)&var_name, (void*)&bool_choice[var_is_array], (void*)&json_value, (void*)set_modify_handler},\
    {"GET", mcu, #var_name, (void*)get_handler, (void*)&var_name, (void*)&bool_choice[var_is_array], NULL, NULL},\
    
#define SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(var_name, var_is_array, set_handler, get_handler, set_modify_handler)\
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU0, var_name, var_is_array, set_handler, get_handler, set_modify_handler)\
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, var_name, var_is_array, set_handler, get_handler, set_modify_handler)\
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, var_name, var_is_array, set_handler, get_handler, set_modify_handler)\
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU3, var_name, var_is_array, set_handler, get_handler, set_modify_handler)\

mcu_var_handle_table_s mcu0_variable_handle_table[] = { 
#if !FACTORY_RESET    
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(double_var_1          , false, set_mcu_double_variable  , get_mcu_double_variable  , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(double_var_2          , false, set_mcu_double_variable  , get_mcu_double_variable  , set_modify_uint32_t_val_cube)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(uint32_var_1          , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(uint32_var_2          , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_uint32_t_val_square)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(uint32_arr_1          , true , set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(double_arr_1          , true , set_mcu_double_variable  , get_mcu_double_variable  , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BP_Low                , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_BP_Low_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BP_High               , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_BP_High_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(NF_Adjust             , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(IN_FOV_Angle_LVL      , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_IN_FOV_Angle_LVL_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(ON_TARGET_Angle_LVL   , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_ON_TARGET_Angle_LVL_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Angle_Hysteresis      , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_Angle_Hysteresis_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(DIR_Sens              , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(APU_Gain              , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_APU_Gain_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Angle_BF_AZM_static   , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_Angle_BF_AZM_static_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Angle_BF_INC_static   , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_Angle_BF_INC_static_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Angle_BF_AZM_auto_EN  , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Angle_BF_INC_auto_EN  , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Enable_AZM_INC_Data   , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Assign_INC_to_DUT     , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Select_Angle_DUT1     , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Select_DBFS_DUT1      , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Debug_AZM_BF          , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(AZM_INC_BF_Duty_Cycle , false, set_mcu_uint8_t_variable , get_mcu_uint8_t_variable , set_modify_AZM_INC_BF_Duty_Cycle_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(DBFS_UE_Adj           , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(MPA_N                 , false, set_mcu_uint8_t_variable , get_mcu_uint8_t_variable , set_modify_MPA_N_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Host_Update_Interval  , false, set_mcu_uint8_t_variable , get_mcu_uint8_t_variable , set_modify_Host_Update_Interval_val)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Enable_DUT_Test       , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(AGC_Adjust_Rate       , false, set_mcu_uint16_t_variable, get_mcu_uint16_t_variable, NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Enable_All_ANGLE      , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Enable_All_DBFS       , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Slave_Data_Fwd        , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Force_IDLE            , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Enable_VOC_FFT        , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    
//    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BF[AZM].rotation_LIM  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BF[AZM].rotation_HYS  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BF[AZM].dispersion_LIM, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BF[AZM].dispersion_HYS, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BF[INC].rotation_LIM  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BF[INC].rotation_HYS  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BF[INC].dispersion_LIM, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(BF[INC].dispersion_HYS, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
#endif
    SET_ALL_MCU_VAR_HANDLE_TABLE_ENTRIES(Uart_Check_Interval   , true , set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_set_Uart_Check_Interval)
};


mcu_var_handle_table_s mcu1_variable_handle_table[] = { 
#if !FACTORY_RESET    
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, double_var_1          , false, set_mcu_double_variable  , get_mcu_double_variable  , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, double_var_2          , false, set_mcu_double_variable  , get_mcu_double_variable  , set_modify_uint32_t_val_cube)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, uint32_var_1          , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, uint32_var_2          , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_uint32_t_val_square)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, uint32_arr_1          , true , set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, double_arr_1          , true , set_mcu_double_variable  , get_mcu_double_variable  , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BP_Low                , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_BP_Low_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BP_High               , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_BP_High_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, NF_Adjust             , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, IN_FOV_Angle_LVL      , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_IN_FOV_Angle_LVL_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, ON_TARGET_Angle_LVL   , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_ON_TARGET_Angle_LVL_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Angle_Hysteresis      , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_Angle_Hysteresis_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, DIR_Sens              , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, APU_Gain              , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_APU_Gain_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Angle_BF_AZM_static   , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_Angle_BF_AZM_static_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Angle_BF_INC_static   , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_Angle_BF_INC_static_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Angle_BF_AZM_auto_EN  , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Angle_BF_INC_auto_EN  , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Enable_AZM_INC_Data   , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Assign_INC_to_DUT     , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Select_Angle_DUT1     , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Select_DBFS_DUT1      , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Debug_AZM_BF          , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, AZM_INC_BF_Duty_Cycle , false, set_mcu_uint8_t_variable , get_mcu_uint8_t_variable , set_modify_AZM_INC_BF_Duty_Cycle_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, DBFS_UE_Adj           , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, MPA_N                 , false, set_mcu_uint8_t_variable , get_mcu_uint8_t_variable , set_modify_MPA_N_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Host_Update_Interval  , false, set_mcu_uint8_t_variable , get_mcu_uint8_t_variable , set_modify_Host_Update_Interval_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Enable_DUT_Test       , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, AGC_Adjust_Rate       , false, set_mcu_uint16_t_variable, get_mcu_uint16_t_variable, NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Enable_VOC_FFT        , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BF[AZM].rotation_LIM  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BF[AZM].rotation_HYS  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BF[AZM].dispersion_LIM, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BF[AZM].dispersion_HYS, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BF[INC].rotation_LIM  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BF[INC].rotation_HYS  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BF[INC].dispersion_LIM, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, BF[INC].dispersion_HYS, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
#endif
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU1, Uart_Check_Interval   , true , set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_set_Uart_Check_Interval)
};

mcu_var_handle_table_s mcu2_variable_handle_table[] = { 
#if !FACTORY_RESET    
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, double_var_1          , false, set_mcu_double_variable  , get_mcu_double_variable  , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, double_var_2          , false, set_mcu_double_variable  , get_mcu_double_variable  , set_modify_uint32_t_val_cube)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, uint32_var_1          , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, uint32_var_2          , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_uint32_t_val_square)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, uint32_arr_1          , true , set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, double_arr_1          , true , set_mcu_double_variable  , get_mcu_double_variable  , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BP_Low                , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_BP_Low_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BP_High               , false, set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_BP_High_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, NF_Adjust             , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, IN_FOV_Angle_LVL      , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_IN_FOV_Angle_LVL_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, ON_TARGET_Angle_LVL   , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_ON_TARGET_Angle_LVL_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Angle_Hysteresis      , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_Angle_Hysteresis_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, DIR_Sens              , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, APU_Gain              , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_APU_Gain_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Angle_BF_AZM_static   , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_Angle_BF_AZM_static_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Angle_BF_INC_static   , false, set_mcu_float_variable   , get_mcu_float_variable   , set_modify_Angle_BF_INC_static_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Angle_BF_AZM_auto_EN  , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Angle_BF_INC_auto_EN  , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Enable_AZM_INC_Data   , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Assign_INC_to_DUT     , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Select_Angle_DUT1     , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Select_DBFS_DUT1      , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, AZM_INC_BF_Duty_Cycle , false, set_mcu_uint8_t_variable , get_mcu_uint8_t_variable , set_modify_AZM_INC_BF_Duty_Cycle_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Debug_AZM_BF          , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, DBFS_UE_Adj           , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, MPA_N                 , false, set_mcu_uint8_t_variable , get_mcu_uint8_t_variable , set_modify_MPA_N_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Host_Update_Interval  , false, set_mcu_uint8_t_variable , get_mcu_uint8_t_variable , set_modify_Host_Update_Interval_val)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Enable_DUT_Test       , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, AGC_Adjust_Rate       , false, set_mcu_uint16_t_variable, get_mcu_uint16_t_variable, NULL)
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Enable_VOC_FFT        , false, set_mcu_bool_variable    , get_mcu_bool_variable    , NULL)

//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BF[AZM].rotation_LIM  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BF[AZM].rotation_HYS  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BF[AZM].dispersion_LIM, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BF[AZM].dispersion_HYS, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BF[INC].rotation_LIM  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BF[INC].rotation_HYS  , false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BF[INC].dispersion_LIM, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
//    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, BF[INC].dispersion_HYS, false, set_mcu_float_variable   , get_mcu_float_variable   , NULL)
#endif
    SET_MCU_VAR_HANDLE_TABLE_ENTRY(MCU2, Uart_Check_Interval   , true , set_mcu_uint32_t_variable, get_mcu_uint32_t_variable, set_modify_set_Uart_Check_Interval)
};




/********************************************************
 *
 * Prelimnary work to simplify cmd_handler functions
 * Maro definitions to simplify tables
 *
 *********************************************************/

// ON/OFF type commands 
#define SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(cmd, mcu, cmd_handler)\
    {#cmd, MCU##mcu, "ENABLE" , NULL, cmd_handler, (void*)&mcu_number[MCU##mcu], (void*)&onoff_value[1]},\
    {#cmd, MCU##mcu, "DISABLE", NULL, cmd_handler, (void*)&mcu_number[MCU##mcu], (void*)&onoff_value[0]},\

#define SET_ALL_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRIES(cmd, cmd_handler)\
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(cmd, 0, cmd_handler)\
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(cmd, 1, cmd_handler)\
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(cmd, 2, cmd_handler)\
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(cmd, 3, cmd_handler)\


// Commands which set an internal variable (TBD: move to var handler table to simplify and further standardize here)
#define SET_MCU_DATA_CMD_HANDLE_TABLE_ENTRY(cmd, mcu, cmd_handler)\
    {#cmd, MCU##mcu, "", (void*)&json_value, cmd_handler, (void*)&mcu_number[MCU##mcu], (void*)&json_value},\

#define SET_ALL_MCU_DATA_CMD_HANDLE_TABLE_ENTRIES(cmd, cmd_handler)\
    SET_MCU_DATA_CMD_HANDLE_TABLE_ENTRY(cmd, 0, cmd_handler)\
    SET_MCU_DATA_CMD_HANDLE_TABLE_ENTRY(cmd, 1, cmd_handler)\
    SET_MCU_DATA_CMD_HANDLE_TABLE_ENTRY(cmd, 2, cmd_handler)\
    SET_MCU_DATA_CMD_HANDLE_TABLE_ENTRY(cmd, 3, cmd_handler)\

// Commands which trigger an immediate action 
#define SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(cmd, mcu, cmd_handler)\
    {#cmd, MCU##mcu, "", NULL , cmd_handler, (void*)&mcu_number[MCU##mcu], NULL},\
    
#define SET_ALL_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRIES(cmd, cmd_handler)\
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(cmd, 0, cmd_handler)\
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(cmd, 1, cmd_handler)\
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(cmd, 2, cmd_handler)\
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(cmd, 3, cmd_handler)\

// Command Handler Tables    
mcu_cmd_handle_table_s mcu0_command_handle_table[] = { 
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY      (CHEN, 3, configure_battery_charge)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY      (RCS , 3, get_charge_status)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY      (RBV , 3, get_battery_voltage_reading)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY      (RBC , 3, get_battery_current_reading)
    SET_ALL_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRIES(PWR , configure_mcu_power)
    SET_ALL_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRIES(RID , get_mcu_id)
    SET_ALL_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRIES(RFV , get_firmware_version)
    SET_ALL_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRIES(RST , enable_system_reset)
    SET_ALL_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRIES(STAT, get_status)
    SET_ALL_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRIES(RFRF, set_factory_reset_firmware)
    SET_ALL_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRIES(RBLV, get_bootloader_version)
    SET_ALL_MCU_DATA_CMD_HANDLE_TABLE_ENTRIES   (BAUD, set_new_baud_rate)
    SET_ALL_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRIES(RBR , get_actual_baud_rate_setpoint)
    {"WDT", MCU0, "ENABLE_ALL" , NULL, enable_watchdog, (void*)&mcu_number[MCU3], (void*)&onoff_value[1]},
    {"WDT", MCU0, "DISABLE_ALL", NULL, enable_watchdog, (void*)&mcu_number[MCU3], (void*)&onoff_value[0]},
    SET_ALL_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRIES(WDT, enable_watchdog)
    {"LPM", MCU3, "", NULL, Activate_LPM, NULL, NULL},
    
    
#if !FACTORY_RESET
	SET_ALL_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRIES(IDC, configure_i2s_data_capture)
    SET_ALL_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRIES(ISF, configure_i2s_soft_fft)
    SET_ALL_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRIES(IHF, configure_i2s_hard_fft)
    SET_ALL_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRIES(HEN, configure_mcu_host_report)
    SET_ALL_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRIES(MEN, configure_console_menu)
#if INCLUDE_STDIO_REMAP
	{"DBG", MCU0, "ENABLE_ALL" , NULL, remap_stdio_debug, (void*)&mcu_number[MCU3], (void*)&onoff_value[1]},
    {"DBG", MCU0, "DISABLE_ALL", NULL, remap_stdio_debug, (void*)&mcu_number[MCU3], (void*)&onoff_value[0]},
    SET_ALL_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRIES(DBG, remap_stdio_debug)
#endif
#endif    
};

mcu_cmd_handle_table_s mcu1_command_handle_table[] = { 
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RID , 1, get_mcu_id)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RFV , 1, get_firmware_version)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RST , 1, enable_system_reset)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(STAT, 1, get_status)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RFRF, 1, set_factory_reset_firmware)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RBLV, 1, get_bootloader_version)
    SET_MCU_DATA_CMD_HANDLE_TABLE_ENTRY   (BAUD, 1, set_new_baud_rate)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RBR,  1, get_actual_baud_rate_setpoint)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(WDT,  1, enable_watchdog)
    
    
#if !FACTORY_RESET
	SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(IDC,  1, configure_i2s_data_capture)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(ISF,  1, configure_i2s_soft_fft)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(IHF,  1, configure_i2s_hard_fft)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(HEN,  1, configure_mcu_host_report)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(MEN,  1, configure_console_menu)
#if INCLUDE_STDIO_REMAP
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(DBG,  1, remap_stdio_debug)
#endif
#endif
};

mcu_cmd_handle_table_s mcu2_command_handle_table[] = { 
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RID , 2, get_mcu_id)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RFV , 2, get_firmware_version)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RST , 2, enable_system_reset)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(STAT, 2, get_status)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RFRF, 2, set_factory_reset_firmware)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RBLV, 2, get_bootloader_version)
    SET_MCU_DATA_CMD_HANDLE_TABLE_ENTRY   (BAUD, 2, set_new_baud_rate)
    SET_MCU_INSTANT_CMD_HANDLE_TABLE_ENTRY(RBR,  2, get_actual_baud_rate_setpoint)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(WDT,  2, enable_watchdog)
    
#if !FACTORY_RESET
	SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(IDC,  2, configure_i2s_data_capture)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(ISF,  2, configure_i2s_soft_fft)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(IHF,  2, configure_i2s_hard_fft)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(HEN,  2, configure_mcu_host_report)
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(MEN,  2, configure_console_menu)
#if INCLUDE_STDIO_REMAP
    SET_MCU_BOOLEAN_CMD_HANDLE_TABLE_ENTRY(DBG,  2, remap_stdio_debug)
#endif
#endif
};


mcu_rsp_handle_table_s mcu0_response_handle_table[] = { 

	{ "OTA", MCU1, "", (void*)json_str, mcu0_handle_ota_slave_response, (void*)&mcu_number[MCU1], (void*)json_str },
	{ "OTA", MCU2, "", (void*)json_str, mcu0_handle_ota_slave_response, (void*)&mcu_number[MCU2], (void*)json_str },

	{ "BAUD", MCU1, "", NULL, mcu0_handle_set_baudrate_response, (void*)&mcu_number[MCU1], NULL },
	{ "BAUD", MCU2, "", NULL, mcu0_handle_set_baudrate_response, (void*)&mcu_number[MCU2], NULL },
	
	{ "RBR" , MCU1, "", NULL, mcu0_handle_get_baudrate_response, (void*)&mcu_number[MCU1], NULL },
	{ "RBR" , MCU2, "", NULL, mcu0_handle_get_baudrate_response, (void*)&mcu_number[MCU2], NULL },
	
	{ "SSR" , MCU1, "", NULL, mcu0_handle_slave_spi_ready_response, (void*)&mcu_number[MCU1], NULL },
	{ "SSR" , MCU2, "", NULL, mcu0_handle_slave_spi_ready_response, (void*)&mcu_number[MCU2], NULL },
	
	
};

static void mcu_command_table_register(mcu_command_handle_s* p_cmd_hdl, mcu_cmd_handle_table_s* p_table, uint32_t count) 
{
	p_cmd_hdl->table = p_table;
	p_cmd_hdl->size = count;
}

static void mcu_variable_table_register(mcu_variable_handle_s* p_var_hdl, mcu_var_handle_table_s* p_table, uint32_t count) 
{
	p_var_hdl->table = p_table;
	p_var_hdl->size = count;
}

static void mcu_response_table_register(mcu_response_handle_s* p_rsp_hdl, mcu_rsp_handle_table_s* p_table, uint32_t count) 
{
	p_rsp_hdl->table = p_table;
	p_rsp_hdl->size = count;
}

void mcu_init_command_handler() 
{
    uint32_t count = 0u;  
	size_t uart_check_interval_ns = 0;
    count = sizeof(mcu0_command_handle_table)/sizeof(mcu_cmd_handle_table_s);
    mcu_command_table_register(&mcu0_cmd_handler, mcu0_command_handle_table, count);    
    
    count = sizeof(mcu1_command_handle_table)/sizeof(mcu_cmd_handle_table_s);
    mcu_command_table_register(&mcu1_cmd_handler, mcu1_command_handle_table, count);    
    
    count = sizeof(mcu2_command_handle_table)/sizeof(mcu_cmd_handle_table_s);
    mcu_command_table_register(&mcu2_cmd_handler, mcu2_command_handle_table, count);    
    
    mcu_init_variable_handler();
    mcu_init_response_handler();
    
    // Init 3 channels of timer0 for each UART
    timer_init(TIMER_DEVICE_0);
    for(size_t i = 0; i < UART_DEVICE_MAX; i++)
    {
        // one channel of timer0 is used for a UART channel 
        // set one-shot timer interval to 5 sec
        timer_set_interval(TIMER_DEVICE_0, i, COMM_TIMEOUT_SEC*1e9);
        timer_irq_register(TIMER_DEVICE_0, i, 1, 1, mcu_command_handler_timer_callback, &timer0_ctx_table[i]);
    }
    
    // Initialize 3 channels of timer1 for each UART, this is for periodic checking of UART comm
    timer_init(TIMER_DEVICE_1);
    for(size_t i = 0; i < UART_DEVICE_MAX; i++)
    {
	    // one channel of timer0 is used for a UART channel 
	    // set periodic timer interval to 5 sec
		uart_check_interval_ns = Uart_Check_Interval[i] * 1e9;
        timer_set_interval(TIMER_DEVICE_1, i, uart_check_interval_ns);
        timer_irq_register(TIMER_DEVICE_1, i, 0, 1, mcu_timer1_callback, &timer1_ctx_table[i]);
    }
    
}

static void mcu_init_variable_handler() 
{
    int count = 0;    
    count = sizeof(mcu0_variable_handle_table)/sizeof(mcu_var_handle_table_s);
    mcu_variable_table_register(&mcu0_var_handler, mcu0_variable_handle_table, count);    
    
    count = sizeof(mcu1_variable_handle_table)/sizeof(mcu_var_handle_table_s);
    mcu_variable_table_register(&mcu1_var_handler, mcu1_variable_handle_table, count);    
    
    count = sizeof(mcu2_variable_handle_table)/sizeof(mcu_var_handle_table_s);
    mcu_variable_table_register(&mcu2_var_handler, mcu2_variable_handle_table, count);    
    
}

static void mcu_init_response_handler()
{
    int count = 0;    
    count = sizeof(mcu0_response_handle_table)/sizeof(mcu_rsp_handle_table_s);
    mcu_response_table_register(&mcu0_rsp_handler, mcu0_response_handle_table, count);  
}



static void mcu_command_table_handler(const mcu_command_handle_s * p_cmd_handler, const struct mcu_command_object * cmd_obj, const char* str)
{
    if (cmd_obj == NULL)
        return;     
	
    for (uint16_t i = 0; i < p_cmd_handler->size; ++i)
    {
        if (strcmp(cmd_obj->command, p_cmd_handler->table[i].command_name) == 0)
        {
            if (cmd_obj->mcu == p_cmd_handler->table[i].mcu)
            {
                //printf("cmd_obj->data.state: %s", cmd_obj->data.state);
                if (strcmp(cmd_obj->data.state, p_cmd_handler->table[i].state) == 0)  
                {                    
                    if (p_cmd_handler->table[i].data)
                    {
                        // store incoming value into a local variable and use variable as input to handle function
                        *((double*)p_cmd_handler->table[i].data) = cmd_obj->data.var_value;
                    }
                    // call the handler function
                    p_cmd_handler->table[i].handle_func(p_cmd_handler->table[i].param_1, p_cmd_handler->table[i].param_2);
                    
                }
                
            }            
        }    
    }    
}

static void __dbgopt__ mcu_variable_table_handler(const mcu_variable_handle_s * p_var_handler, const struct mcu_command_object * cmd_obj, const char* str)
{
    if (cmd_obj == NULL)
        return;     
    for (uint16_t i = 0; i < p_var_handler->size; ++i)
    {
        if (strcmp(cmd_obj->command, p_var_handler->table[i].command_name) == 0)
        {
            if (cmd_obj->mcu == p_var_handler->table[i].mcu)
            {
                if ((strcmp(cmd_obj->data.var_real.var_name, p_var_handler->table[i].var_name) == 0))  
                {
                    if (p_var_handler->table[i].var_val)
                    {
                        // save the value from json string to interval variable
                        *((double*)p_var_handler->table[i].var_val) = cmd_obj->data.var_real.var_value;
                    }
                    // call the handler function
                    p_var_handler->table[i].handle_func((void*)&(p_var_handler->table[i].mcu),
                                                        (void*)p_var_handler->table[i].var_name,
                                                        p_var_handler->table[i].var_is_array, 
                                                        p_var_handler->table[i].var_addr, 
                                                        p_var_handler->table[i].var_val, 
                                                        p_var_handler->table[i].var_set_modify_handler);
                    break;
                }
            }            
        }    
    }    
}

static void mcu_response_table_handler(const mcu_response_handle_s * p_rsp_handler, const struct mcu_response_object * rsp_obj, const char* str)
{
	if (rsp_obj == NULL || p_rsp_handler == NULL)
        return;     
	
	for (uint16_t i = 0; i < p_rsp_handler->size; ++i)
    {
	    if (strcmp(rsp_obj->response_command, p_rsp_handler->table[i].command_name) == 0)
        {
	        if (rsp_obj->mcu == p_rsp_handler->table[i].mcu)
            {
                if (rsp_obj->data.state)
                {
                    // store incoming value into a local variable and use variable as input to handle function
                    strcpy((char *)p_rsp_handler->table[i].data, rsp_obj->data.state);
                    
                }
                else if (rsp_obj->data.var_value)
                {
                
                    *((double*)p_rsp_handler->table[i].data) = rsp_obj->data.var_value;
                }
                // call the handler function
                p_rsp_handler->table[i].handle_func(p_rsp_handler->table[i].param_1, p_rsp_handler->table[i].param_2);
                
            }            
        }    
    }    
}

// decide if the fld is in the command list
/*
static int assert_command(char * fld)
{
    int ret_value = 0;
    int num_of_command = sizeof(command_list) / sizeof(command_list[0]);
    for (int i = 0; i < num_of_command; ++i)
    {
        if (strcmp(fld, command_list[i]) == 0)
        {
            ret_value = 1;
            break;
        }
    }
    return ret_value;
}*/

void report_firmware_version()
{
    get_firmware_version((void*)&mcu_id, NULL);
}    

void report_mcu_id()
{
    get_mcu_id((void*)&mcu_id, NULL);
}    

void report_mcu_status()
{
    get_status((void*)&mcu_id, NULL);
}    

void slave_mcu_spi_ready_notification(uint8_t mcu_index)
{
    char strTemp[64];
    
    sprintf(strTemp, "{\"RSP\":\"SSR\", \"MCU\":%d, \"VALUE\":1}", mcu_index);
    send_string((uart_device_number_t)mcu_index, strTemp, head_data_JSON);

}