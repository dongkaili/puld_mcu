#include "mcu_common_define.h" 
#if !FACTORY_RESET 
#include <stdio.h>
#include <unistd.h>
#include "fpioa.h"
#include "gpio.h"
#include "mcu_fpga.h"

/* GPIO Functions

  - gpio_init
  - gpio_set_drive_mode
  - gpio_set_pin
  - gpio_get_pin

Function prototypes... 

Int gpio_init( void ) // returns 0=success
Void gpio_set_drive_mode(uint8_t pin , gpio_drive_mode_t mode) // returns nothing
Void gpio_set_pin(uint8_t pin , gpio_pin_value_t value) // returns nothing
Gpio_pin_value_t gpio_get_pin(uint8_t pin) // returns pin value

Typedef enum _gpio_drive_mode
{
GPIO_DM_INPUT ,
GPIO_DM_INPUT_PULL_DOWN ,
GPIO_DM_INPUT_PULL_UP ,
GPIO_DM_OUTPUT ,
} gpio_drive_mode_t;

Typedef enum _gpio_pin_value
{
GPIO_PV_LOW ,
GPIO_PV_HIGH
} gpio_pin_value_t;

*/

//typedef struct _mcu_fpga_context_ {
//    uint8_t enter_shell;
//} _mcu_fpga_context_s;
//
//static _mcu_fpga_context_s s_content = {
//    .enter_shell = 0,
//};
//fpga bypass

void mcu_fpga_init(void* p) {

    gpio_init ();
    
//    // iCE_CRST_B 
//    fpioa_set_function(iCE_CRST_B, FUNC_GPIO0);
//    gpio_set_drive_mode(0, GPIO_DM_INPUT_PULL_UP); // Leave line tristated but with weak pullup
//
//    // SPI_SS_B_CFG 
//    fpioa_set_function(SPI_SS_B_CFG, FUNC_GPIO1); // Leave line tristated but with weak pullup
//    gpio_set_drive_mode(1, GPIO_DM_INPUT_PULL_UP);

}

void mcu_fpga_loop(void) {
//    gpio_set_drive_mode(0, GPIO_DM_OUTPUT);
//    gpio_set_pin (0, GPIO_PV_HIGH );
}
#endif
