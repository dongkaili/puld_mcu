#ifndef __mcu_COMM_PROTOCOL_H__
#define __mcu_COMM_PROTOCOL_H__

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "mcu_internal_fsm.h"
#include "mcu_common_define.h"
/**

Prosaris TLV Data Format

HH|VV|TH|TL
L4|L3|L2|L1
CS|R3|R2|R1
< data >

HH - Header Byte (Unused atm)
VV - Version Byte (Unused atm)
TH - Type (High) -- Sets the data format.  0x00: Raw, 0x01: JSON
TL - Type (Low) -- Sets the message type: 0x00 (Configuration), 0x01 (Debug), 0x02 (Heat Map), 0x03 (Vector), 0x04 (FFT)
L - Length (LSB first) -- Message length.  Ex. 15 byte message: 0x00 0x00 0x00 0xFF
CS - Checksum (based off checksum alg)
R - Reserved for future use
< data > - Data bytes

-------

Configuration Message Data Format (Type: 0x0100)
Supports: JSON

heat_map: boolean -- Controls whether heatmap information should be sent to device
fft: boolean -- Controls whether fft information should be sent to device
vector: boolean -- Controls whether polar vector should be sent to device

defaults: all false

------

Debug Message Data Format (Type: 0x0001 or 0x0101)
Supports: JSON, Raw

JSON Keys:
msg: string -- The Debug message

------

Heat Map Message Data Format (Type: 0x0002)
Supports: Raw

Message format:
256 bytes representing 16x16 heat map of sound

------

Vector Message Data Format (Type: 0x0103)
Supports: JSON

JSON keys:
theta: Angle from X-Axis (Center of mapping as origin)
radius: distance from center of mapping

------

FFT Message Data Format (Type: 0x0104)
Supports: JSON

JSON Keys:
freq_per_bin: double -- frequency per bin
sample_rate: double -- sampling rate
data: [[double]] -- fft output (pairs of real / complex components)
... anything else I need to compute power density spectrum

**/

#define COMM_MESSAGE_FORMAT_HEAD            0x11
#define COMM_MESSAGE_FORMAT_VERSION         0x01
#define COMM_MESSAGE_FORMAT_TYPE_H_RAW      0x00
#define COMM_MESSAGE_FORMAT_TYPE_H_JSON     0x01
#define COMM_MESSAGE_FORMAT_TYPE_H_STRING   0x02
#define COMM_MESSAGE_FORMAT_TYPE_L_CONFIG   0x00
#define COMM_MESSAGE_FORMAT_TYPE_L_DEBUG    0x01
#define COMM_MESSAGE_FORMAT_TYPE_L_HEATMAP  0x02
#define COMM_MESSAGE_FORMAT_TYPE_L_VECTOR   0x03
#define COMM_MESSAGE_FORMAT_TYPE_L_I2S_FFT      0x04
#define COMM_MESSAGE_FORMAT_TYPE_L_COMMAND  0x05
#define COMM_MESSAGE_FORMAT_TYPE_L_OTA_BIN  0x06
#define COMM_MESSAGE_FORMAT_TYPE_L_I2S      0x07
#define COMM_MESSAGE_FORMAT_TYPE_L_I2S_RAW_FFT 0x08
#define COMM_MESSAGE_FORMAT_TYPE_L_DIR      0x09
#define COMM_MESSAGE_FORMAT_TYPE_L_VOC      0x10
#define COMM_MESSAGE_FORMAT_TYPE_L_VOC_FFT  0x12



typedef enum {
    E_COMM_CHECK_HEAD,
    E_COMM_GET_VERSION,
    E_COMM_GET_FORMAT,
    E_COMM_GET_TYPE,
    E_COMM_GET_LENGTH,
    E_COMM_GET_CHECKSUM,
    E_COMM_GET_SQEID,
    E_COMM_GET_PAYLOAD,
    E_COMM_DATA_VALIDATED,
    E_COMM_ERROR,
    E_COMM_GET_FORMATOR
} COMM_PARSE_EVENT_E;

typedef enum {
    S_COMM_GOT_ERROR = -1,
    S_COMM_INIT = 0,
    S_COMM_IDLE,
    S_COMM_GOT_HEAD,
    S_COMM_GOT_VER,
    S_COMM_GOT_TYPE_H,
    S_COMM_GOT_TYPE_L,
    S_COMM_GOT_LENGTH1,
    S_COMM_GOT_LENGTH2,
    S_COMM_GOT_LENGTH3,
    S_COMM_GOT_LENGTH4,
    S_COMM_GOT_CHECKSUM,
    S_COMM_GOT_RESERVED,
    S_COMM_GOT_SEQID_H,
    S_COMM_GOT_SEQID_L,
    S_COMM_GOT_PAYLOAD
} COMM_PARSE_STATE_E;

typedef struct _event_comm_ {
    uint8_t event_type;
    void *data;
} _event_comm_s;

typedef struct {
    uint8_t format;
    uint8_t msg_type;
    char *p_data;
    uint32_t size;
} mcu_comm_data_s;

// typedef struct _data_header_ {
//     uint8_t head;
//     uint8_t version;
//     uint16_t type;
//     uint32_t length;
//     uint8_t checksum;
//     uint8_t reserved[3];

//     // uint8_t data[0];
// } _data_header_s;
// //}__attribute__ ((packed)) _data_header_s;

typedef struct _data_header_ {
    uint8_t head;
    uint8_t version;
    uint16_t type;
    uint32_t length;
    uint8_t checksum;
    uint8_t reserved[3];
    // uint8_t* pdata;
} _data_header_s;

typedef void (*mcu_comm_parse_data_t)(uint8_t index, void *data);
typedef void (*mcu_comm_event_t)(void *event);

// typedef struct __data_context__ {
//     uint8_t value;
//     uint8_t checksum;
// }data_context_s;

uint8_t checksum(uint8_t *buf, uint32_t len);
void init_parser(uint8_t index, mcu_comm_parse_data_t data_callback);//, mcu_comm_event_t event_callback);
uint8_t mcu_comm_parse_data(uint8_t index, const uint8_t *buf, uint32_t len);
uint32_t mcu_comm_get_length(uint8_t index);
mcu_fsm_state_t mcu_comm_get_state(uint8_t index);
uint8_t mcu_comm_get_format(uint8_t index);
uint8_t mcu_comm_get_type(uint8_t index);
void mcu_comm_set_fsm_state(uint8_t index, mcu_fsm_state_t state);
mcu_fsm_state_t mcu_comm_get_fsm_state(uint8_t index);

void dump_hexinfo(const uint8_t *buf, int32_t len);

#endif