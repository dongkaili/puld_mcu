#ifndef __MCU_FPGA_H__
#define __MCU_FPGA_H__

#define iCE_CRST_B 10
#define ICE_CDONE 11
#define JTAG_TCK__SPI_CLK 43
#define TJAG_TDO__SPI_SO 42
#define JTAG_TDI__SPI_SI 44
#define SPI_SS_B 45
#define SPI_SS_B_CFG 39
#define RST 37 // soft reset signal from MCU_A  to FPGA - not used by other MCU's

void mcu_fpga_init(void*);
void mcu_fpga_loop();


#endif
