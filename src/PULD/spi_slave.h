/* Copyright 2018 Canaan Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __SPI_SLAVE__
#define __SPI_SLAVE__

#include <stdint.h>
#include "spi.h"

#define SPI_SLAVE_MCU1_INT_PIN       PIN_MCU_LINK10
#define SPI_SLAVE_MCU1_INT_IO        GPIOHS_8_MCU1_INT
#define SPI_SLAVE_MCU1_READY_PIN     PIN_MCU_LINK01
#define SPI_SLAVE_MCU1_READY_IO      GPIOHS_3_MCU1_RDY
#define SPI_SLAVE_MCU1_CS_PIN        PIN_MCU_LINK00
#define SPI_SLAVE_MCU1_CLK_PIN       PIN_MCU_LINK11
#define SPI_SLAVE_MCU1_MOSI_PIN      PIN_MCU_LINK12
#define SPI_SLAVE_MCU1_MISO_PIN      PIN_MCU_LINK12

#define SPI_SLAVE_MCU2_INT_PIN       PIN_MCU_LINK13
#define SPI_SLAVE_MCU2_INT_IO        GPIOHS_9_MCU2_INT
#define SPI_SLAVE_MCU2_READY_PIN     PIN_MCU_LINK03
#define SPI_SLAVE_MCU2_READY_IO      GPIOHS_4_MCU2_RDY
#define SPI_SLAVE_MCU2_CS_PIN        PIN_MCU_LINK02
#define SPI_SLAVE_MCU2_CLK_PIN       PIN_MCU_LINK11
#define SPI_SLAVE_MCU2_MOSI_PIN      PIN_MCU_LINK12
#define SPI_SLAVE_MCU2_MISO_PIN      PIN_MCU_LINK12

int spi_slave_init(uint8_t *data, uint32_t len, uint8_t mcu_id);

#endif
