#include "mcu_comm_protocol.h"
#include "mcu_data_handler.h"
#include "mcu_device_management.h"
#include "mcu_common_define.h"
#include "mcu_adc.h"
#include "gpio.h"
#include "gpiohs.h"
#include "uart.h"
#include "uarths.h"
#include "spi.h"
#include "apu_init.h"
#include "mcu_common.h"
#include "mcu_json_commands.h"
    
//#define SIZE     16
typedef enum {
    S_DATA_HANDLE_IDLE,
    S_DATA_HANDLE_STARTED,
    S_DATA_HANDLE_PROCESSING,
    S_DATA_HANDLE_FINISH,
    S_DATA_HANDLE_ERROR,
} DATA_HANDLE_STATE;

typedef struct {
    mcu_fsm_s fsm;
    uint8_t event_data;
    mcu_fsm_state_t state;
    mcu_comm_data_s cur_data;
    uint32_t data_count;
    _mcu_util_queue_s data_queue_set;
} mcu_data_handle_context_s;

// typedef struct mcu_response_state {
//     char response_command[SIZE];
//     int  slave;
//     char  state[SIZE];    
// } mcu_response_state_t;


// typedef struct mcu_response_value {
//     char response_command[SIZE];
//     int  slave;
//     double f_value;    
// } mcu_response_value_t;

// typedef struct mcu_stream_object {

//     double angle;
//     double vrms;
//     double dbfs;
//     int    nmpa;
//     int    linked;    

// } mcu_stream_object_t;


static mcu_data_handle_context_s sc[] = {
    {
        .state = S_DATA_HANDLE_IDLE,
        .data_count = 0,
    },
    {
        .state = S_DATA_HANDLE_IDLE,
        .data_count = 0,
    },
    {
        .state = S_DATA_HANDLE_IDLE,
        .data_count = 0,
    } 
};

//static int enable_charge = 0;

// local function declaration
static void mcu_data_handle_started(uint8_t index, void *p);
static void mcu_data_handle_payload(uint8_t index, void *p);
static void mcu_data_handle_validated(uint8_t index, void *p);
static void mcu_data_handle_error(uint8_t index, void *p);
static void mcu_release_comm_data_buffer(void *elem);
static void mcu_data_handle_got_length(uint8_t index, void *p);


static void mcu_release_comm_data_buffer(void *elem)
{
    uint8_t* p = (*(uint8_t **)elem);
    LOG1("free address: 0x%08lx  ]\n\r", (uint64_t)p);
	free(p);
}



static void mcu_data_handle_started(uint8_t index, void *p) {
    // mcu_fsm_s* p_fsm = (mcu_fsm_s *)p;
    sc[index].data_count = 0;
}

static void mcu_data_handle_got_length(uint8_t index, void *p) {
    //malloc buffer
    mcu_comm_data_s tdata = {
        .format = mcu_comm_get_format(index),
        .msg_type = mcu_comm_get_type(index),
        .size = mcu_comm_get_length(index),
        .p_data = malloc(mcu_comm_get_length(index)*sizeof(uint8_t))
    };
    //LOG("malloc address: 0x%08lx  ]\n\r", (uint64_t)tdata.p_data);
    memcpy(&(sc[index].cur_data), &tdata, sizeof(mcu_comm_data_s));
}

static void mcu_data_handle_payload(uint8_t index, void *p) {
    // mcu_fsm_s* p_fsm = (mcu_fsm_s *)p;
    uint8_t *target = (uint8_t *)sc[index].cur_data.p_data + sc[index].data_count;
    memcpy(target, &(sc[index].event_data), 1);
    sc[index].data_count ++;
#if (0) 
    uint8_t str[1024] = {0};
    strncpy(str, sc.cur_data.p_data, sc.data_count);
    printf("mcu_data_handle_payload is called\n\r");
    printf("payload value: %s\n\r", str);
    //LOG("payload value: %s\n\r", str);
#endif
}

static void mcu_data_handle_validated(uint8_t index, void *p) 
{   
    //enqueue the data then free it
    mcu_util_queue_enque(&(sc[index].data_queue_set), &(sc[index].cur_data));
    sc[index].cur_data.p_data = NULL;
    sc[index].cur_data.size = 0;
    //LOG("%s,queue size: %ld\n\r",__FUNCTION__, mcu_util_queue_get_size(&(sc[index].data_queue_set)));
    //JSON_debug_print("mcu_data_handle_validated() is called\n\r"); // enabling this results in interference elsewhere
#if (0)
     mcu_comm_data_s tdata;
     mcu_util_queue_deque(&(sc.data_queue_set), &tdata);
     LOG("format:0x%02x\n\r", tdata.format);
     LOG("msg_type:0x%02x\n\r", tdata.msg_type);
     uint8_t str1[1024] = {0};
     strncpy(str1, tdata.p_data, tdata.size);
     LOG("queue value: %s\n\r", str1);
     printf("queue value: %s\n\r", str1);
     free(tdata.p_data);
     // mcu_util_queue_close(&(sc.data_queue_set));
#endif
    

}

static void mcu_data_handle_error(uint8_t index, void *p) {
    // mcu_fsm_s* p_fsm = (mcu_fsm_s *)p;
    //free the pennding data
    if(sc[index].cur_data.size > 0) 
    {
        sc[index].cur_data.size = 0;
        sc[index].data_count = 0;
        mcu_release_comm_data_buffer(&(sc[index].cur_data.p_data));
        sc[index].cur_data.p_data = NULL;
        sc[index].cur_data.size = 0;
    }
}

static mcu_fsm_table_s mcu_uart_handle_table[] = {
    /*data  condition   cur_state               event                      next_state                   handler_func */
    {NULL,  NULL,  S_DATA_HANDLE_PROCESSING,    E_COMM_GET_PAYLOAD,      S_DATA_HANDLE_PROCESSING,   mcu_data_handle_payload},
    {NULL,  NULL,  S_DATA_HANDLE_PROCESSING,    E_COMM_DATA_VALIDATED,   S_DATA_HANDLE_IDLE,         mcu_data_handle_validated},
    {NULL,  NULL,  S_DATA_HANDLE_PROCESSING,    E_COMM_ERROR,            S_DATA_HANDLE_IDLE,         mcu_data_handle_error},

    {NULL,  NULL,  S_DATA_HANDLE_IDLE,          E_COMM_ERROR,            S_DATA_HANDLE_IDLE,         mcu_data_handle_error},
    {NULL,  NULL,  S_DATA_HANDLE_IDLE,          E_COMM_CHECK_HEAD,       S_DATA_HANDLE_STARTED,      mcu_data_handle_started},

    {NULL,  NULL,  S_DATA_HANDLE_STARTED,       E_COMM_GET_LENGTH,       S_DATA_HANDLE_PROCESSING,   mcu_data_handle_got_length},
    {NULL,  NULL,  S_DATA_HANDLE_STARTED,       E_COMM_ERROR,            S_DATA_HANDLE_IDLE,         mcu_data_handle_error},
};

void mcu_init_data_handler(uint8_t index) 
{
    int count = 0;
    
    count = sizeof(mcu_uart_handle_table)/sizeof(mcu_fsm_table_s);
    mcu_fsm_register(&(sc[index].fsm), mcu_uart_handle_table, count, S_DATA_HANDLE_IDLE);
    mcu_util_queue_init(&(sc[index].data_queue_set), 10, sizeof(mcu_comm_data_s), mcu_release_comm_data_buffer);
    
}

void mcu_comm_event_handler(uint8_t index, void *param) 
{
    uint8_t type = (uint8_t)((_event_comm_s*)param)->event_type;
    sc[index].event_data = (uint8_t)((_event_comm_s*)param)->data;
    
    //sprintf(str, "event:%d\n\r", type);
    
    // printf("data: 0x%02x\n\r", data);
    // printf("%s,%s,%d\n\r", __FILE__,__FUNCTION__,__LINE__);
    mcu_fsm_event_handler(index, &(sc[index].fsm), type);
}

_mcu_util_queue_s* mcu_data_handle_get_data_set(uint8_t index) {
    return &(sc[index].data_queue_set);
}


