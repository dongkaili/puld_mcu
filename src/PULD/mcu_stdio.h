#ifndef __mcu_STDIO_H__
#define __mcu_STDIO_H__

void mcu_stdio_init(void*);
void mcu_stdio_loop(void);
void mcu_stdio_putc(int8_t c);
void mcu_stdio_puts(char *str);
uint8_t mcu_stdio_getc(void);


#define BANNER_TEXT \
"\n\r\
 _____    __    __   _____       ______     \n\r\
(  __ \\   ) )  ( (  (_   _)     (_  __ \\  \n\r\
 ) )_) ) ( (    ) )   | |         ) ) \\ \\ \n\r\
(  ___/   ) )  ( (    | |        ( (   ) )  \n\r\
 ) )     ( (    ) )   | |   __    ) )  ) )  \n\r\
( (       ) \\__/ (  __| |___) )  / /__/ /  \n\r\
/__\\      \\______/  \\________/  (______/ \n\r\
\n\r\
Welcome to PULD 1.0\n\rCtrl+T enter command mode\n\rCtrl+V show help\n\r"


#endif
