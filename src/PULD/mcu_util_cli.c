#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "bsp.h"
#include "mcu_util_cli.h"

static void current_core_info() {
    //uint64_t core = current_coreid();
    // printf("%s, called from Core %ld\n\r", __FUNCTION__, core);
}

uint8_t mcu_util_cli_invoker(char* command_string, mcu_command_s* table, uint32_t table_size, _mcu_util_queue_s* commands_stack) {
    current_core_info();
    if(NULL == command_string ||
        NULL == commands_stack ||
        strlen(command_string) >= MAX_COMMAND_STRING_LEN) {
        printf("invalid command address: 0x%08lx\n\r", (uint64_t)command_string);
    } else {
        // int count = sizeof(table)/sizeof(mcu_command_s);
        for (uint16_t i = 0; i < table_size; i++) {
            char* p = strstr(command_string, table[i].command_name);
            if(p == command_string) {
                mcu_command_data_s command_data;
                memset(&command_data, 0, sizeof(mcu_command_data_s));
                command_data.callback = table[i].command_callback;
                strcpy(command_data.command, command_string);
                mcu_util_queue_enque(commands_stack, &command_data);
                return 1;
            }
        }
        printf("unmatched command: %s\n\r", command_string);
    }
    return 0;
}

uint8_t mcu_util_cli_receiver(_mcu_util_queue_s* commands_queue) {
    if(NULL == commands_queue)
        return 0;
    while(0 == mcu_util_queue_is_empty(commands_queue)) {
        current_core_info();
        mcu_command_data_s tdata;
        mcu_util_queue_deque(commands_queue, &tdata);
        // free(tdata.p_data);
        //printf("latest command from queue: %s\n\r", tdata.command);
        mcu_command_entry_func_t command_entry_func = tdata.callback;
        if(NULL != command_entry_func) {
            command_entry_func(tdata.command);
        }
    }
    return 1;

}

