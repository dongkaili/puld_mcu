#include <stdio.h>
#include <stdarg.h>
#include "uarths.h"
#include "mcu_stdio.h"
#include "fpioa.h"
#include "stdlib.h"
#include "ctype.h"
#include "sleep.h"
#include "sysctl.h"
#include "gpio.h"

#define TAKEOVER_STD_SHOW 0
#include "apu.h"
#include "apu_init.h"
#include "uart.h"
#include "mcu_common_define.h"
#include "mcu_device_management.h"
#include "mcu_data_handler.h"
#include "mcu_pins.h"
#include "mcu_mic_array.h"
#include "mcu_wdt.h"
#include "mcu_adc.h"
#include "mcu_uart.h"
#include "mcu_json_commands.h"
#include "spi_master.h"

typedef struct _mcu_stdio_context_ {
    uint8_t enter_shell;
} _mcu_stdio_context_s;

#ifdef DEBUG
#if !FACTORY_RESET
static _mcu_stdio_context_s s_context = {
    .enter_shell = 0,
};
#endif
#endif

//stdio bypass
bool Terminal_Mode = false;
bool Enable_LPM = false;
bool Force_WDT0_Trip = false;  // flag to trigger wdt0 test
bool Force_WDT1_Trip = false;  // flag to trigger wdt1 test
bool WDT0_Enable = false;      // WDT's disabled by default so they don't interfere when debugging with J-Link device
bool WDT1_Enable = false; 
bool Enable_MCU1 = ENABLE_MCU_PWR;
bool Enable_MCU2 = ENABLE_MCU_PWR;
bool Enable_Charger = ENABLE_CHARGER;
bool Core_Loop_Time_Test = false;
uint64_t core0_us_start = 0;
uint32_t core0_us_elapsed = 0;
uint64_t core1_us_start = 0;
uint32_t core1_us_elapsed = 0;

extern beamformer_s BF[N_BF];
extern bool Slave_Data_Fwd;


uint8_t DelayArray[16][8];
char DelayArrayStr[1024] = "";

// test data array for SPI
//static uint8_t test_data[1024];
extern uint64_t mcu1_bf_data_ready_count;
extern uint64_t mcu2_bf_data_ready_count;
//extern uint64_t mcu1_bf_ready_irq_count;
//extern uint64_t mcu2_bf_ready_irq_count;

extern uint32_t slave1_data_addr;
extern uint32_t slave2_data_addr;

extern uint64_t mcu1_bf_data_read_error_count;
extern uint64_t mcu2_bf_data_read_error_count;

extern uint64_t mcu1_bf_data_read_count;
extern uint64_t mcu2_bf_data_read_count;

extern uint8_t enable_mcu1_bf_data_read;
extern uint8_t enable_mcu2_bf_data_read;



extern void swap_bytes_in_double_words(void* addr, uint32_t len);


#if INCLUDE_STDIO_REMAP
bool Enable_Remap_STDIO = false;     // RX default for all MCU0's is IO04, TX is on IO05 for all MCU's but MCU1 & MCU2 are disconnected sp can only receive characters on USB console
bool Remap_STDIO_lockout = false; // set a flag as soon as JTAG connection is attempted and lock in state change to prevent reverting JTAG pins for STDIO use

void remap_jtag_pins_as_stdio(bool enable_flag)
{
    fpioa_set_function(Enable_Remap_STDIO ? PIN_MCU_JTAG_TDI : PIN_MCU_ISP_RX, FUNC_UARTHS_RX);
    fpioa_set_function(Enable_Remap_STDIO ? PIN_MCU_JTAG_TDO : PIN_MCU_ISP_TX, FUNC_UARTHS_TX);
}
#endif

void mcu_stdio_init(void* p) {
#if INCLUDE_STDIO_REMAP
    remap_jtag_pins_as_stdio(Enable_Remap_STDIO);
#endif
    uarths_init();
    uarths_config(DEBUG_BAUD_RATE, UARTHS_STOP_1);
}    


void mcu_stdio_putc(int8_t c) {
    uarths_putchar(c);
}
void mcu_stdio_puts(char *str) {
	uarths_puts(str);
}


// https://stackoverflow.com/questions/14766040/function-arguments-like-printf-in-c
int mcu_stdio_printf(const char *fmt, ...)
{
    char buffer[4096];
    va_list args;
    va_start(args, fmt);
    (void)vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);
    uarths_puts(buffer);
    return 0;
}


uint8_t mcu_stdio_getc(void) {
    return uarths_getchar(); // the STDIO function getchar works more like scanf... only acts after CR is received
}

void mcu_stdio_show_banner(void) {
#if 1
    const char banner[] = BANNER_TEXT;
#else
    const char banner[] = "";
#endif
    mcu_stdio_printf(banner);
}

#if !FACTORY_RESET
#ifdef DEBUG

void Display_Data_Table_Mssg(bool Enable_Flag, char Table_Mssg[])
{
  if (Enable_Flag)
  {
    mcu_stdio_printf("\n\rReady to begin new series of %s data tests.\n\r", Table_Mssg);
	mcu_stdio_printf("Press Ctrl-T to exit terminal mode and enable data logging.\n\r");
	mcu_stdio_printf("Setup new trial conditions and then press the letter \"C\" to continue.\n\r");
	mcu_stdio_printf("Repeat trial setup and press the letter \"C\" again after each trial completes.\n\r");
  }
}

#endif
#endif

// https://www.ccsinfo.com/forum/viewtopic.php?t=36549
// TURN THE CURSOR ON/OFF
void Cursor(int IO)
{
  if (IO)
  {
      mcu_stdio_printf("\x1B[?25h");  
  }
  else
  {
      mcu_stdio_printf("\x1B[?25l");
  }
}

#if !FACTORY_RESET
#ifdef DEBUG

void display_menu_item_with_enable_flag(bool Enable_Flag, char menu_item[]) 
{
  mcu_stdio_printf("%s %s\n\r", (Enable_Flag ? "*" : " "), menu_item);
}

void display_menu()
{
    usleep(5000);
    //char menu_mssg[128] = "";
    ClrScr();
    mcu_stdio_printf("MCU[%d] command letter options [%s]\n\r", mcu_id, get_firmware_version_string());
    mcu_stdio_printf("UART BAUD Rates: UART[%d] = %d", mcu_id, mcu_uart_get_actual_set_baud_rate(mcu_id));
    if (mcu_id == MCU0)
    {
        mcu_stdio_printf(", UART[%d] = %d, UART[%d] = %d", MCU1, mcu_uart_get_actual_set_baud_rate(MCU1), MCU2, mcu_uart_get_actual_set_baud_rate(MCU2));
    }
    mcu_stdio_printf("\n\r");
    
#if INCLUDE_JSON_LOG
    display_menu_item_with_enable_flag(Activate_JSON_Log, "J: Enable JSON message logging. [F8]");
#endif
#if 0
    mcu_stdio_printf("  A: Display APU register settings.\n\r");
#else
    mcu_stdio_printf("  A: Display 16x8 delay arrays for multiple angles.\n\r");
#endif
    
    display_menu_item_with_enable_flag(Core_Loop_Time_Test, "C: Enabled core loop time comparitor.");
    if (mcu_id == 0)
    {        
        mcu_stdio_printf("p|P: Display battery power data (voltage and current).[F4==V/C]\n\r");
        display_menu_item_with_enable_flag(Enable_Charger, "E: Enable battery charger. [F5]");
    }
    display_menu_item_with_enable_flag(Slave_Data_Fwd, "V: Forward slave streams to host.");

    display_menu_item_with_enable_flag(Enable_Host_Reporting, "H: Enable reporting of beamformer data to host. [F1==1|F2==2|F3==0]");
    
    display_menu_item_with_enable_flag(Debug_AZM_BF, "Z: Debug menu options apply to AZiMuth version of beamformer (otherwise INCident).");

    display_menu_item_with_enable_flag(Angle_BF_INC_auto_EN, "z: INClination beamformer uses local angle generated by AZiMuth beamformer.");
    display_menu_item_with_enable_flag(Angle_BF_AZM_auto_EN, "e: AZiMuth beamformer uses FOV generated by INClination beamformer (2*abs(local angle)");

    display_menu_item_with_enable_flag(Enable_Beamformer_Stats, "b: Enable display of beamformer statistics.");
    display_menu_item_with_enable_flag(Enable_Barchart, "B: Enable display of beamformer barchart.");
    display_menu_item_with_enable_flag(Enable_BF_FSM_Debug, "m: Enable debug messages for beamformer FSM.");
    
    display_menu_item_with_enable_flag(Enable_I2S_Data_Capture, "i: Toggle capture and analysis of I2S data.");
    display_menu_item_with_enable_flag(Enable_I2S_Data_Table, "I: Toggle display of I2S data table.");

    display_menu_item_with_enable_flag(Enable_I2S_Hard_FFT, "f: Toggle generation of I2S Hard FFT.");
    display_menu_item_with_enable_flag(Enable_I2S_Hard_FFT_Table, "F: Toggle display of I2S Hard FFT power table.");
  
    display_menu_item_with_enable_flag(Enable_I2S_Soft_FFT, "s: Toggle generation of I2S Soft FFT.");
    display_menu_item_with_enable_flag(Enable_I2S_Soft_FFT_Table, "S: Toggle display of I2S Soft FFT power table.");

    display_menu_item_with_enable_flag(Enable_DIR_Results_Table, "D: Toggle display of DIR results table.");
    display_menu_item_with_enable_flag(Enable_DIR_Buffer_Table, "d: Toggle display of DIR buffer table.");
    
    mcu_stdio_printf("     TEST_State = %d\n\r", TEST_State);

#if INCLUDE_WDT
    display_menu_item_with_enable_flag(WDT0_Enable, "w: Enable core 0 Watchdog Timer #0 (WDT0)");
    display_menu_item_with_enable_flag(WDT1_Enable, "W: Enable core 1 Watchdog Timer #1 (WDT1)");
    display_menu_item_with_enable_flag(Force_WDT0_Trip, "r: Perform reset on core 0 with Watchdog Timer #0 (WDT0).");
    display_menu_item_with_enable_flag(Force_WDT1_Trip, "R: Perform reset on core 1 with Watchdog Timer #1 (WDT1).");
#endif
  
    mcu_stdio_printf(" ^R: Perform software reset. [F12]\n\r");

    if (mcu_id == 0)
    {        
        display_menu_item_with_enable_flag(Enable_MCU1, "1: Enable Slave #1 Power [F6].");
        display_menu_item_with_enable_flag(Enable_MCU2, "2: Enable Slave #2 Power [F7]");
    }

    mcu_stdio_printf("g|G: Halve|Double APU audio gain (max = 1.000000, min = 0.000977) [%.6f : 0x%x].\n\r", APU_Gain, APU_Gain_u);  // Why is there a "+0.000125"?
    display_menu_item_with_enable_flag(AGC_Enable, "k: Automatic Gain Control (AGC)");

    display_menu_item_with_enable_flag(Enable_DUT_Test, "X: Enable streaming of DUT data.");
    display_menu_item_with_enable_flag(Enable_AZM_INC_Data, "x: Enable streaming of individual Azimuth and Inclination data.");

    display_menu_item_with_enable_flag(Enable_All_ANGLE, "!: Enable streaming of all array angle data.");
    display_menu_item_with_enable_flag(Enable_All_DBFS,  "~: Enable streaming of all array dbfs data.");

    mcu_stdio_printf("n|N: Decrease|Increase number of points used in MPA calculations [%d].\n\r", MPA_N);
    
    mcu_stdio_printf("/|\\: Prev/Next I2S_FS & BP Low|High Option [I2S FS: %dHz, BPL: %dkHz, BPH: %dkHz].\n\r", I2S_FS_actual, apu_fir_coefficients_options[i2s_fs_sel_current].bp_low, apu_fir_coefficients_options[i2s_fs_sel_current].bp_high);
    mcu_stdio_printf("{|}: Decrease|Increase BP Low Freq  (n1 = %d) [%dkHz].\n\r", BP_LOW_N, (uint8_t)(BP_Low / 1000));
    mcu_stdio_printf("<|>: Decrease|Increase BP High Freq (n2 = %d) [%dkHz].\n\r", BP_HIGH_N, (uint8_t)(BP_High / 1000));
    
    mcu_stdio_printf("y|Y: Decrease|Increase System Clock Frequency[Target: %.1fMHz][Actual: %.1fMHz].\n\r", System_Clock_Freq_Target / 1e6, System_Clock_Freq_Actual / 1e6);
    mcu_stdio_printf("u|U: Decrease|Increase host update interval [%d].\n\r", Host_Update_Interval);
    if (mcu_id == 0)
    {        
        display_menu_item_with_enable_flag(Enable_LPM, "L: Set device into Low Power Mode(slaves OFF and low core clock frequency) [F7].");
    }
    mcu_stdio_printf("t: Write config to slave MCU 1 via SPI\r\n");
    mcu_stdio_printf("T: Read config from slave MCU 1 via SPI\r\n");
    mcu_stdio_printf("^S: Test SPI communication between Master MCU0 and slave MCU 1\r\n");
    mcu_stdio_printf("^A: Test SPI communication between Master MCU0 and slave MCU 2\r\n");
    display_menu_item_with_enable_flag(Enable_VOC_FFT, "^B: Toggle FFT via VOC.");
    display_menu_item_with_enable_flag(Force_IDLE, "l: Force MCU0 BF FSM into IDLE state.");
    mcu_stdio_printf("send_done_count = %d\r\n", send_done_count);
}

void Process_Command(bool *Enable_Flag, char Enable_Mssg[])
{
  // toggle flag
  *Enable_Flag = (*Enable_Flag) ? false : true;
  //char str[128] = "";
  //  sprintf(str, "%s %sabled.\n\r", Enable_Mssg, (*Enable_Flag) ? "en" : "dis");
  //  mcu_stdio_puts(str);
};

static uint64_t t_us_start;
static bool menu_displayed_once = false;
#endif
#endif
void mcu_stdio_loop(void) {
#if !FACTORY_RESET
#ifdef DEBUG
#if INCLUDE_STDIO_REMAP
    // check to see if TCK is high as this signals start of JTAG takeover (TRST does as well but this is unused)
    // Then release STDIO TX/RX from TDI/TDO pins and reassign JTAG pins to proper functions
    // Test for the state of the debug enable flag and if it is already deasserted then I don't do any checks on the TCK pin
  	// Setup TCK as a GPIO input right from the start and test it in here. When it goes high it's time to flip it back to normal TCK function and leave it that way permanently
    
//    uint8_t TCK_TEST_VAL = gpiohs_get_pin(GPIOHS_0_TCK_TEST);
//    printf("TCK_TEST_VAL = %d, Remap_STDIO_lockout  = %d\n\r", TCK_TEST_VAL, Remap_STDIO_lockout);
    if (!Remap_STDIO_lockout && (1 == gpiohs_get_pin(GPIOHS_0_TCK_TEST)))
    {
        mcu_stdio_printf("Resetting JTAG\n\r");
        Enable_Remap_STDIO = false;
        Remap_STDIO_lockout = true;
        remap_jtag_pins_as_stdio(Enable_Remap_STDIO); // put STDIO back on ISPRX/TX
        // .. and restore original JTAG pin functions
        fpioa_set_function(PIN_MCU_JTAG_TCK, FUNC_JTAG_TCLK); 
        fpioa_set_function(PIN_MCU_JTAG_TDI, FUNC_JTAG_TDI); 
        fpioa_set_function(PIN_MCU_JTAG_TDO, FUNC_JTAG_TDO); 
    };
#endif    
    //char mcu_id_str[10] = "";

// Display menu with slight delay to allow UART to complete
    if(!menu_displayed_once && Terminal_Mode && (sysctl_get_time_us() - t_us_start) > 100000) // wait 100ms
    {
        display_menu();
        menu_displayed_once = true;
    }

    uint8_t c = mcu_stdio_getc();
    //char str[64] = "";
    if(c == 0 || c == 0xff)
        return;

//  mcu_stdio_printf("c:%02x\n\r", c);
  if (c == 0x14) 
  { // CTRL-T
		s_context.enter_shell = s_context.enter_shell ? 0 : 1;
		if (s_context.enter_shell) 
		{
			Terminal_Mode = true;
  		t_us_start = sysctl_get_time_us();
  		menu_displayed_once = false;
    }
		else 
		{
			mcu_stdio_printf("Exiting Terminal Mode\n\r");
			Terminal_Mode = false;
  		    ClrScr_Done = false;
  		    Cursor(0);  // disable screen cursor
		}
	} 
  else if((c == 0x16) && (Terminal_Mode == true)) 
  {
  	// CTRL-V
    display_menu();
  } 
  else if(s_context.enter_shell) 
  {
    if (Terminal_Mode == true)
    {
      switch (c)
      {
#if INCLUDE_JSON_LOG
        case 'j': 
        case 'J': 
            Process_Command(&Activate_JSON_Log, "");
            display_menu();
            break;
#endif
          
        case 'A': 
            memset(DelayArrayStr, 0, sizeof(DelayArrayStr));
            apu_get_delays(DelayArray);
            apu_get_delay_str(DelayArray, DelayArrayStr);
            printf("Current Delays: %s\n\n", DelayArrayStr);

            memset(DelayArrayStr, 0, sizeof(DelayArrayStr));
            apu_get_delay_str(BF[AZM].delays, DelayArrayStr);
            printf("BF[AZM]: %s\n\n", DelayArrayStr);

            memset(DelayArrayStr, 0, sizeof(DelayArrayStr));
            apu_get_delay_str(BF[INC].delays, DelayArrayStr);
            printf("BF[INC]: %s\n\n", DelayArrayStr);
#if 1
            apu_print_setting();    
#else

#if 0
            for (int ang = 0; ang <= 180; ang += 45)
            //while ('q' != mcu_stdio_getc())
            {
                //int ang = rand() % 180;
                apu_calc_delays_azimuth(I2S_FS_actual, UCA8_RADIUS_CM, UCA_N, (float)ang, DelayArray);     // calculates array of offsets for given FOV_Angle
                printf("FOV"); 
#else
            for(int ang = -45; ang <= 45; ang += 5) // vary azm angle 
            //while ('q' != mcu_stdio_getc())
            {
                //int ang = 180 - rand() % 360;
                apu_calc_delays_incident(I2S_FS_actual, UCA8_RADIUS_CM, UCA_N, (float)ang, DelayArray);     // calculates array of offsets for given FOV_Angle
                printf("AZM"); 
#endif                
                memset(DelayArrayStr, 0, sizeof(DelayArrayStr));
                apu_get_delay_str(DelayArray, DelayArrayStr);
                printf("=%3d Delays: %s\n\n", ang, DelayArrayStr);
            }

#endif
            break;
                
                
        case 'Z' : 
            Process_Command(&Debug_AZM_BF, "");
            display_menu();
            break;
          
        case 'z' : 
            Process_Command(&Angle_BF_INC_auto_EN, "");
            display_menu();
            break;

        case 'e' : 
            Process_Command(&Angle_BF_AZM_auto_EN, "");
            display_menu();
            break;
          
        case 'C' : 
            Process_Command(&Core_Loop_Time_Test, "");
            display_menu();
            break;
          
        case 'm':
        case 'M':
            Process_Command(&Enable_BF_FSM_Debug, "");
            if (Enable_BF_FSM_Debug)
            {
                Enable_Barchart = false;
                Enable_Beamformer_Stats = false;
                Enable_DIR_Results_Table = false;
                Enable_DIR_Buffer_Table = false;
                Enable_I2S_Data_Table = false;
                Enable_I2S_Hard_FFT_Table = false;
                Enable_I2S_Soft_FFT_Table = false;
            }
            display_menu();
            break;

        case 'p': 
            if (mcu_id == 0)
            {        
                mcu_stdio_printf("Battery V|I: %3.2fV, %4.1fmA\r\n", get_battery_voltage(3.3 * 2), get_battery_current(3.3 / 50 / 0.1 * 1000));    
                // 50% voltage divider at input to U21 (volts ADC)
                // 50:1 current sense amplifier, 100 mohm current sense resistor, A->mA
            }
            break;

        case 'P': 
            if (mcu_id == 0)
            {        
                mcu_stdio_printf("Battery discharge test ('q' to quit)... Enable_MCU1[%s], Enable_MCU2[%s]\n\r", Enable_MCU1 ? "true" : "false", Enable_MCU2 ? "true" : "false");
                // Disable WDT
                mcu_wdt_enable(WDT_DEVICE_0, false);
                mcu_wdt_enable(WDT_DEVICE_1, false);
        
                uint64_t t_us_current, t_us_start;
                t_us_start = sysctl_get_time_us();
                while ('q' != mcu_stdio_getc())
                {
                    t_us_current = sysctl_get_time_us();
                    mcu_stdio_printf("Time (sec)[%8d] Battery V|I: %3.2fV, %4.1fmA\r\n", (int)((t_us_current - t_us_start) / 1e6), get_battery_voltage(3.3 * 2), get_battery_current(3.3 / 50 / 0.1 * 1000));    
                    usleep(1e6);
                }
                // Restore WDT status
                mcu_wdt_enable(WDT_DEVICE_0, WDT0_Enable);
                mcu_wdt_enable(WDT_DEVICE_1, WDT1_Enable);
                display_menu();
            }
            break;

        case 'E': 
            if (mcu_id == 0)
            {        
                Process_Command(&Enable_Charger, "Battery Charger");  
                gpio_set_pin(GPIO_7_CHARGE_EN, (gpio_pin_value_t)Enable_Charger);
                display_menu();
            }
            break;            

        case 'V':
        case 'v' : 
            Process_Command(&Slave_Data_Fwd, "");
            display_menu();
            break;
                
        case 'H': 
        case 'h': 
            Process_Command(&Enable_Host_Reporting, "Host reporting");  
            display_menu();
            break;
       
        case 'b': 
            Process_Command(&Enable_Beamformer_Stats, "Beamformer stats");  
            if (Enable_Beamformer_Stats)
            {
                Enable_BF_FSM_Debug = false;
                Enable_DIR_Results_Table = false;
                Enable_DIR_Buffer_Table = false;
                Enable_I2S_Data_Table = false;
                Enable_I2S_Hard_FFT_Table = false;
                Enable_I2S_Soft_FFT_Table = false;
            }
            display_menu();
            break;
            
        case 'B': 
            Process_Command(&Enable_Barchart, "Barchart display");      
            if (Enable_Barchart)
            {
                Enable_BF_FSM_Debug = false;
                Enable_DIR_Results_Table = false;
                Enable_DIR_Buffer_Table = false;
                Enable_I2S_Data_Table = false;
                Enable_I2S_Hard_FFT_Table = false;
                Enable_I2S_Soft_FFT_Table = false;
            }
            display_menu();
            break;

        case 'i':
            Process_Command(&Enable_I2S_Data_Capture, "capture of I2S data"); 
            if (!Enable_I2S_Data_Capture)
            {
                Enable_I2S_Data_Table = false;
                Enable_I2S_Hard_FFT_Table = false;
                Enable_I2S_Soft_FFT_Table = false;
                Enable_I2S_Hard_FFT   = false;
                Enable_I2S_Soft_FFT   = false;
            }
            display_menu();
            break;
        
        case 'I':
            Process_Command(&Enable_I2S_Data_Table, "I2S Buffer Table display"); 
            if (Enable_I2S_Data_Table)
            {
                Enable_Barchart = false;
                Enable_Beamformer_Stats = false;
                Enable_DIR_Results_Table = false;
                Enable_DIR_Buffer_Table = false;
                Enable_I2S_Data_Capture = true;
                Enable_BF_FSM_Debug = false;
                TEST_State = TEST_WAIT;
            }
            else
            {    
                TEST_State = Enable_I2S_Data_Table || Enable_I2S_Hard_FFT_Table || Enable_I2S_Soft_FFT_Table ? TEST_State : TEST_DISABLE;  // retain state if other table dumps are enabled
            }
            display_menu();
            Display_Data_Table_Mssg(Enable_I2S_Data_Table, "I2S Buffer");
            Trial_ID = -1;
            Trial_Num = 0;
            //Wait_for_Letter_C = true;
            break;

        case 'f':
            Process_Command(&Enable_I2S_Hard_FFT, "generation of I2S Hard FFT"); 
            if (Enable_I2S_Hard_FFT)
            {
                Enable_I2S_Data_Capture = true;
            }
            else
            {
                Enable_I2S_Hard_FFT_Table = false;
            }
            display_menu();
            break;
        
        case 'F':
            Process_Command(&Enable_I2S_Hard_FFT_Table, "I2S Hard FFT Power Table display"); 
            if (Enable_I2S_Hard_FFT_Table)
            {
                Enable_Barchart = false;
                Enable_Beamformer_Stats = false;
                Enable_DIR_Results_Table = false;
                Enable_DIR_Buffer_Table = false;
                Enable_I2S_Data_Capture = true;
                Enable_I2S_Hard_FFT = true;
                Enable_BF_FSM_Debug = false;
                TEST_State = TEST_WAIT;
            }
            else
            {    
                TEST_State = Enable_I2S_Data_Table || Enable_I2S_Hard_FFT_Table || Enable_I2S_Soft_FFT_Table ? TEST_State : TEST_DISABLE;   // retain state if other table dumps are enabled
            }
            display_menu();
            Display_Data_Table_Mssg(Enable_I2S_Hard_FFT_Table, "I2S Hard FFT");
            Trial_ID = -1;
            Trial_Num = 0;
            //Wait_for_Letter_C = true;
            break;

        case 's':
            Process_Command(&Enable_I2S_Soft_FFT, "generation of I2S Soft FFT"); 
            if (Enable_I2S_Soft_FFT)
            {
                Enable_I2S_Data_Capture = true;
            }
            else
            {
                Enable_I2S_Soft_FFT_Table = false;
            }
            display_menu();
            break;
      
        case 'S':
            Process_Command(&Enable_I2S_Soft_FFT_Table, "I2S Soft FFT Power Table display"); 
            if (Enable_I2S_Soft_FFT_Table)
            {
                Enable_Barchart = false;
                Enable_Beamformer_Stats = false;
                Enable_DIR_Results_Table = false;
                Enable_DIR_Buffer_Table = false;
                Enable_I2S_Data_Capture = true;
                Enable_I2S_Soft_FFT = true;
                Enable_BF_FSM_Debug = false;
                TEST_State = TEST_WAIT;
            }
            else
            {    
                TEST_State = Enable_I2S_Data_Table || Enable_I2S_Hard_FFT_Table || Enable_I2S_Soft_FFT_Table ? TEST_State : TEST_DISABLE;   // retain state if other table dumps are enabled
            }
            display_menu();
            Display_Data_Table_Mssg(Enable_I2S_Soft_FFT_Table, "I2S Soft FFT");
            Trial_ID = -1;
            Trial_Num = 0;
            //Wait_for_Letter_C = true;
            break;

        case 'D': 
            Process_Command(&Enable_DIR_Results_Table, "DIR Results Table display"); 
            if (Enable_DIR_Results_Table)
            {
                Enable_Barchart = false;
                Enable_Beamformer_Stats = false;
                Enable_DIR_Buffer_Table = false;
                Enable_I2S_Data_Table = false;
                Enable_I2S_Hard_FFT_Table = false;
                Enable_I2S_Soft_FFT_Table = false;
                Enable_BF_FSM_Debug = false;
                TEST_State = TEST_WAIT;
            }
            else 
            {
                TEST_State = TEST_DISABLE;
            }
            display_menu();
            Display_Data_Table_Mssg(Enable_DIR_Results_Table, "DIR Results");
            Trial_ID = -1;
            Trial_Num = 0;
            //Wait_for_Letter_C = true;
            break;

        case 'd': 
            Process_Command(&Enable_DIR_Buffer_Table, "DIR Buffer Table display"); 
            if (Enable_DIR_Buffer_Table)
            {
                Enable_Barchart = false;
                Enable_Beamformer_Stats = false;
                Enable_DIR_Results_Table = false;
                Enable_I2S_Data_Table = false;
                Enable_I2S_Hard_FFT_Table = false;
                Enable_I2S_Soft_FFT_Table = false;
                Enable_BF_FSM_Debug = false;
                TEST_State = TEST_WAIT;
            }
            else 
            {
                TEST_State = TEST_DISABLE;
            }
            display_menu();
            Display_Data_Table_Mssg(Enable_DIR_Buffer_Table, "DIR Buffer");
            Trial_ID = -1;
            Trial_Num = 0;
            //Wait_for_Letter_C = true;
            break;

        case '1' :  // Enable slave #1 (MCU1)
            if(mcu_id == 0)
            {        
                Process_Command(&Enable_MCU1, "Slave #1"); 
                gpio_set_pin(GPIO_2_MCU1_EN, (gpio_pin_value_t)Enable_MCU1);
                display_menu();
            }
            break;

        case '2' :  // Enable slave #2 (MCU2)
            if(mcu_id == 0)
            {        
                Process_Command(&Enable_MCU2, "Slave #2"); 
                gpio_set_pin(GPIO_3_MCU2_EN, (gpio_pin_value_t)Enable_MCU2);
                display_menu();
            }
            break;
              
        case 'g': // halve audio gain (min (1 << 3) = 1.0)
            if(APU_Gain > ((float)0x001 / 0x400))
            { 
                APU_Gain = APU_Gain / 2;
                set_apu_gain(APU_Gain);
            }
            display_menu();
            break;

        case 'G': // double audio gain (max (0x3ff = 1.999)
            if(APU_Gain < ((float )0x3ff/0x400))
            {
                APU_Gain = APU_Gain * 2;
                set_apu_gain(APU_Gain);
            }
            display_menu();
            break;

        case 'k': 
            Process_Command(&AGC_Enable, "Test AGC");  
            display_menu();
            break;
          
        case '-': 
            if (NF_Adjust > -5.0) 
                NF_Adjust = NF_Adjust -0.1;
            display_menu();
            break;
       
        case '+': 
            if (NF_Adjust < 15.0) 
                NF_Adjust = NF_Adjust + 1;
            display_menu();
          break;
 
                
        case 'X': 
            Process_Command(&Enable_DUT_Test, "Enable DUT data streaming");  
            display_menu();
            break;

        case 'x': 
            Process_Command(&Enable_AZM_INC_Data, "Enable AZM & INC data streaming");  
            display_menu();
            break;

        case '!': 
            Process_Command(&Enable_All_ANGLE, "Enable angle array data streaming");  
            display_menu();
            break;

        case '~': 
            Process_Command(&Enable_All_DBFS, "Enable dbfs array data streaming");  
            display_menu();
            break;
                
        case 'n': 
            if (MPA_N >1) 
                MPA_N = MPA_N - 1;
            display_menu();
            break;

        case 'N': 
            if (MPA_N < MPA_NMAX) 
                MPA_N = MPA_N + 1;
            display_menu();
            break;

        case '/': 
            if (i2s_fs_sel_current > I2S_FS_LPM)
            {
                i2s_fs_sel_saved = i2s_fs_sel_current--;   // OK to drop one step
                update_all_i2s_fs(i2s_fs_sel_current);
                display_menu();
            }
            break;
        
        case '\\': 
            if (i2s_fs_sel_current < I2S_FS_MAX-1)
            {
                i2s_fs_sel_saved = i2s_fs_sel_current++;   // OK to increase one step
                update_all_i2s_fs(i2s_fs_sel_current);
                display_menu();
            }
            break;
        case '{' : 
            if(BP_Low > 15000)
            {
                set_apu_bp_low_freq(BP_Low - 1000);
                display_menu();
            }
            break;
        
        case '}': 
            if (BP_Low < 35000)
            {
                set_apu_bp_low_freq(BP_Low + 1000);
                display_menu();
            }
            break;                      
          
        case '<' : 
            if(BP_High > 42000)
            {
                set_apu_bp_high_freq(BP_High - 1000);
                display_menu();
            }
            break;
        
        case '>': 
            if(BP_High < 42000)
            {
                set_apu_bp_high_freq(BP_High + 1000);
                display_menu();
            }
            break;
          
        case 'y': 
            if (System_Clock_Freq_Target > (1 * 1e6))
            {
#if(0)
                System_Clock_Freq_Target = (System_Clock_Freq_Actual >> 1) < SYSTEM_CLOCK_FREQ_MIN ? SYSTEM_CLOCK_FREQ_MIN : System_Clock_Freq_Target >> 1; // halve
#else
                System_Clock_Freq_Target = System_Clock_Freq_Target - 1000000UL;   // USe this to find lowest frequency for JSON ack (708MHz)
#endif
                System_Clock_Freq_Actual = sysctl_pll_set_freq(SYSCTL_PLL0, System_Clock_Freq_Target);
                uarths_debug_init();
                display_menu();
            }
            break;
        
        case 'Y': 
            if (System_Clock_Freq_Target < (SYSTEM_CLOCK_FREQ_MAX_MHZ * 1e6)) 
            {
#if(0)
              System_Clock_Freq_Target = (System_Clock_Freq_Actual + 10e6) > (SYSTEM_CLOCK_FREQ_MAX_MHZ * 1e6) ? (SYSTEM_CLOCK_FREQ_MAX_MHZ * 1e6) : System_Clock_Freq_Target + 10e6;  // bump up
#else
              System_Clock_Freq_Target = (System_Clock_Freq_Actual + 1e6) > (SYSTEM_CLOCK_FREQ_MAX_MHZ * 1e6) ? (SYSTEM_CLOCK_FREQ_MAX_MHZ * 1e6) : System_Clock_Freq_Target + 1e6;   // bump up
#endif
                System_Clock_Freq_Actual = sysctl_pll_set_freq(SYSCTL_PLL0, System_Clock_Freq_Target);
                uarths_debug_init();
                display_menu();
            }
            break;

        case 'l': 
            Process_Command(&Force_IDLE, "Force_IDLE");
            display_menu();
            break;
        case 'L': 
            if (mcu_id == 0)
            {        
                Process_Command(&Enable_LPM, "Low Power Mode");
                i2s_fs_sel_saved = i2s_fs_sel_current;
                Set_LPM(Enable_LPM);
                display_menu();
            }
            break;
          
        case 'u': 
            if (Host_Update_Interval > 1) 
            {
                Host_Update_Interval = Host_Update_Interval - 1;
                display_menu();
            }
            break;

        case 'U': 
            if (Host_Update_Interval < 64) // would have to be enough!
            {
                Host_Update_Interval = Host_Update_Interval + 1;
                display_menu();
            }
            break;
        
        case 'w':
            Process_Command(&WDT0_Enable, "");
            mcu_wdt_enable(WDT_DEVICE_0, WDT0_Enable);
            display_menu();
            break;

        case 'W':
            Process_Command(&WDT1_Enable, "");
            mcu_wdt_enable(WDT_DEVICE_1, WDT1_Enable);
            display_menu();
            break;

        case 'r':
            // WDT #0 installed on core0 so we need the while(1) loop to execute there
            Process_Command(&Force_WDT0_Trip, "");
            display_menu();
            break;

        case 'R':
            // WDT #1 installed on core1 so we need the while(1) loop to execute there
            Process_Command(&Force_WDT1_Trip, "");
            display_menu();
            break;
        
        case 't' :
            // for (uint32_t i = 0; i < 1024; i++)
            //     test_data[i] = i % 256;
            // spi_master_transfer(MCU1, test_data, 0, 8, WRITE_CONFIG);
            // printf("MCU1 BF data ready count: %ld, bf rdy irq count: %ld\r\n", mcu1_bf_data_ready_count, mcu1_bf_ready_irq_count);
            
            // read the address of the data on slave mcu 1 
            // count = 0;
            // do
            // {
            //     slave1_data_addr = 0;
            //     slave1_addr_bak = 0;
            //     spi_master_transfer(MCU1, (uint8_t *)&slave1_data_addr, 8, 4, READ_CONFIG);
            //     spi_master_transfer(MCU1, (uint8_t *)&slave1_addr_bak, 8, 4, READ_CONFIG);
            //     count++;
            // } while (slave1_data_addr != slave1_addr_bak);
            // printf("count: %d, slave1_data_addr: 0x%x\r\n", count, slave1_data_addr);
            
            // // read the address of the data on slave mcu 2
            // count = 0;
            // do
            // {
            //     slave2_data_addr = 0;
            //     slave2_addr_bak = 0;
            //     spi_master_transfer(MCU2, (uint8_t *)&slave2_data_addr, 8, 4, READ_CONFIG);
            //     spi_master_transfer(MCU2, (uint8_t *)&slave2_addr_bak, 8, 4, READ_CONFIG);
            //     count++;
            // } while (slave2_data_addr != slave2_addr_bak);
            // printf("count: %d, slave2_data_addr: 0x%x\r\n", count, slave2_data_addr);
            
            
            printf("slave 1 data address: 0x%x, slave 2 data address: 0x%x\r\n", spi_master_get_data_address_slave_mcu1(), spi_master_get_data_address_slave_mcu2());

            break;
        
        case 'T' :
            // for (uint32_t i = 0; i < 1024; i++)
            //     test_data[i] = 0;
            // spi_master_transfer(MCU1, test_data, 0, 8, READ_CONFIG);
            // for (uint32_t i = 0; i < 8; i++)
            // {
            //     //if (test_data[i] != (uint8_t)i)
            //     printf("%d: 0x%02x,", i, test_data[i]);
            // }
            // printf("\r\n");
            // printf("MCU1 BF data ready count: %ld, bf rdy irq count: %ld\r\n", mcu1_bf_data_ready_count, mcu1_bf_ready_irq_count);
            printf("MCU1 bf data ready count: %ld, mcu2 BF data ready count: %ld\r\n", mcu1_bf_data_ready_count, mcu2_bf_data_ready_count);
            printf("mcu1 BF data read count: %ld, mcu2 BF data read count: %ld\r\n", mcu1_bf_data_read_count, mcu2_bf_data_read_count);
            printf("mcu1 BF data read err count: %ld, mcu2 BF data read error count: %ld\r\n", mcu1_bf_data_read_error_count, mcu2_bf_data_read_error_count);

            break;
        
        case 0x01 :  // Ctrl+A
            // for (uint32_t i = 0; i < 8; i++)
            //     test_data[i] = 7 - i;
            // spi_master_transfer(MCU1, test_data, 0, 8, WRITE_CONFIG);
            // for (uint32_t i = 0; i < 8; i++)
            //     test_data[i] = 0;
            // spi_master_transfer(MCU1, test_data, 0, 8, READ_CONFIG);
            // printf("Read 8 bytes of Config after writing:\r\n");
            // for (uint32_t i = 0; i < 8; i++)
            // {
            //     //if (test_data[i] != (uint8_t)i)
            //         printf("%d: 0x%02x,", i, test_data[i]);
            // }
            // printf("\r\n");
            
            // for (uint32_t i = 0; i < 8; i++)
            //     test_data[i] = i;
            // spi_master_transfer(MCU2, test_data, 0, 8, WRITE_CONFIG);
            // for (uint32_t i = 0; i < 8; i++)
            //     test_data[i] = 0;
            // spi_master_transfer(MCU2, test_data, 0, 8, READ_CONFIG);
            // printf("Read 8 bytes of Config after writing:\r\n");
            // for (uint32_t i = 0; i < 8; i++)
            // {
            //     //if (test_data[i] != (uint8_t)i)
            //         printf("%d: 0x%02x,", i, test_data[i]);
            // }
            printf("\r\n");

            
            

            // for (uint32_t i = 0; i < 8; i++)
            //     test_data[i] = i;
            // spi_master_transfer(MCU1, test_data, slave1_data_addr, 8, WRITE_DATA_BYTE);
            // for (int j = 0; j < 10; j++)
            // {
            //     for (uint32_t i = 0; i < 8; i++)
            //         test_data[i] = 0;
            //     spi_master_transfer(MCU2, test_data, slave1_data_addr+j*8, 8, READ_DATA_BYTE);
            //     printf("Read %dth 8 data bytes from slave1_data_addr 0x%x: \r\n", j, slave1_data_addr+j*8);
            //     for (uint32_t i = 0; i < 8; i++)
            //     {
            //         //if (test_data[i] != (uint8_t)i)
            //         printf("%d: 0x%02x,", i, test_data[i]);
            //     }
            //     printf("\r\n");
            // }
         
            
            // printf("Writing Block data: \r\n");
            // uint64_t blk_write_start_time_us = sysctl_get_time_us();
            // for (uint32_t i = 0; i < 1024; i++)
            //     test_data[i] = (i+1);
            // spi_master_transfer(MCU2, test_data, slave1_data_addr, 1024, WRITE_DATA_BLOCK);
            
            // uint64_t blk_write_elapse_time = sysctl_get_time_us() - blk_write_start_time_us;
            // printf("Block write via SPI elapsed time: %ld\r\n", blk_write_elapse_time);
            
            // printf("Read data block from MCU 2 address 0x%x: \r\n", slave2_data_addr);
            // uint64_t blk_read_start_time_us = sysctl_get_time_us();
            
            // for (uint32_t i = 0; i < 1024; i++)
            //     test_data[i] = 0;
            // spi_master_transfer(MCU2, test_data, slave2_data_addr, 1024, READ_DATA_BLOCK);
            
            // swap_bytes_in_double_words((void*)test_data, sizeof(test_data)/sizeof(test_data[0]));
            
            // uint64_t blk_read_elapse_time = sysctl_get_time_us() - blk_read_start_time_us;
            // printf("Block read via SPI elapsed time: %ld us\r\n", blk_read_elapse_time);
            // for (uint32_t i = 0; i < 1024; i++)
            // {
            //     //if (test_data[i] != (uint8_t)i)
            //         printf("0x%02x,", test_data[i]);
            // }
            // printf("\r\n");

            // printf("SPI on MCU 2 test finish\n");
            // printf("MCU2 BF data ready count: %ld, rdy irq counts: %ld\r\n", mcu2_bf_data_ready_count, mcu2_bf_ready_irq_count);
            enable_mcu1_bf_data_read = 0;
            enable_mcu2_bf_data_read = 0;
            break;
        
        
        case 0x13 :  // Ctrl+S

            // for (uint32_t i = 0; i < 8; i++)
            //     test_data[i] = i;
            // spi_master_transfer(MCU1, test_data, 0, 8, WRITE_CONFIG);
            // for (uint32_t i = 0; i < 8; i++)
            //     test_data[i] = 0;
            // spi_master_transfer(MCU1, test_data, 0, 8, READ_CONFIG);
            // printf("Read 8 bytes of Config after writing:\r\n");
            // for (uint32_t i = 0; i < 8; i++)
            // {
            //     //if (test_data[i] != (uint8_t)i)
            //         printf("%d: 0x%02x,", i, test_data[i]);
            // }
            // printf("\r\n");

            

            // for (uint32_t i = 0; i < 8; i++)
            //     test_data[i] = i;
            // spi_master_transfer(MCU1, test_data, slave1_data_addr, 8, WRITE_DATA_BYTE);
            // for (int j = 0; j < 10; j++)
            // {
            //     for (uint32_t i = 0; i < 8; i++)
            //         test_data[i] = 0;
            //     spi_master_transfer(MCU1, test_data, slave1_data_addr+j*8, 8, READ_DATA_BYTE);
            //     printf("Read %dth 8 data bytes from slave1_data_addr 0x%x: \r\n", j, slave1_data_addr+j*8);
            //     for (uint32_t i = 0; i < 8; i++)
            //     {
            //         //if (test_data[i] != (uint8_t)i)
            //         printf("%d: 0x%02x,", i, test_data[i]);
            //     }
            //     printf("\r\n");
            // }

            
            
            
            // printf("Writing Block data: \r\n");
            // uint64_t blk_write_start_time_us = sysctl_get_time_us();
            // for (uint32_t i = 0; i < 1024; i++)
            //     test_data[i] = (i+1);
            // spi_master_transfer(MCU1, test_data, slave1_data_addr, 1024, WRITE_DATA_BLOCK);
            
            // uint64_t blk_write_elapse_time = sysctl_get_time_us() - blk_write_start_time_us;
            // printf("Block write via SPI elapsed time: %ld\r\n", blk_write_elapse_time);
            
            // printf("Read data block from MCU 1 address 0x%x: \r\n", slave1_data_addr);
            // blk_read_start_time_us = sysctl_get_time_us();
    
            
            // for (uint32_t i = 0; i < 1024; i++)
            //     test_data[i] = 0;
            // spi_master_transfer(MCU1, test_data, slave1_data_addr, 1024, READ_DATA_BLOCK);
                        
            // swap_bytes_in_double_words((void*)test_data, sizeof(test_data)/sizeof(test_data[0]));
            
            // blk_read_elapse_time = sysctl_get_time_us() - blk_read_start_time_us;
            // printf("Block read via SPI elapsed time: %ld us\r\n", blk_read_elapse_time);
            // for (uint32_t i = 0; i < 1024; i++)
            // {
            //     //if (test_data[i] != (uint8_t)i)
            //         printf("0x%02x,", test_data[i]);
            // }
            // printf("\r\n");

            // printf("SPI on MCU 1 test finish\n");
            // printf("MCU1 BF data ready count: %ld, bf rdy irq count: %ld\r\n", mcu1_bf_data_ready_count, mcu1_bf_ready_irq_count);
            enable_mcu1_bf_data_read = 1;
            enable_mcu2_bf_data_read = 1;
            
            
            break;        
        
        
        case 0x02: // Ctrl+B
            Process_Command(&Enable_VOC_FFT, "Enable FFT via VOC");  
            //Enable_VOC_FFT = !Enable_VOC_FFT;
            // if (Enable_VOC_FFT)
            // {
            //     printf("Enable FFT via VOC\r\n");
            //     //apu_voc_enable(0);
            //     apu_voc_reset();
            //     usleep(1000);
            //     apu_set_fft_shift_factor(1, 0xb0);
            //     //apu_voc_reset();
            //     //apu_voc_enable(1);
            // }
            // else
            // {
            //     printf("Disable FFT via VOC\r\n");
            //     //apu_voc_enable(0);
            //     apu_dir_reset();
            //     usleep(1000);
            //     apu_set_fft_shift_factor(0, 0);
            //     //apu_voc_reset();
            //     //apu_voc_enable(1);
            // }
                
            display_menu();
            break;
        
        case 0x12: // [Ctrl] R 	DEC=18 	HEX=12 	 ABBR=DC2 	NAME=Device Control 2
            mcu_reboot();   // immediate action... be very careful!
            break;
          
        default: 
        ;
        //sprintf(str, "Invalid command! [%1x]\n\r", c);
        //mcu_stdio_puts(str); 
      } // end switch
    }
    else
    {
	    // mcu_stdio_putc(c); // no need to echo
    }
  }
  else if ((Enable_DIR_Buffer_Table || 
            Enable_DIR_Results_Table || 
            Enable_I2S_Data_Table || 
            Enable_I2S_Hard_FFT_Table || 
            Enable_I2S_Soft_FFT_Table) && TEST_State==TEST_WAIT /*Wait_for_Letter_C*/)
  {
      switch (toupper(c))
      {
          case 'C':
            //Wait_for_Letter_C = false;
            TEST_State = TEST_RUN;
            mcu_stdio_printf("Starting test...\n\r");
            break;
          default:
          mcu_stdio_printf("INVALID RESPONSE: Setup new trial conditions and press the letter \"C\" to continue...\n\r");
      }
  }
#endif
#endif
} 
  