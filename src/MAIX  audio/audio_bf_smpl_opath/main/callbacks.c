#include "config.h"
#include "callbacks.h"
#include <stdio.h>


int16_t VOICE_BF_DIR_BUFFER[VOICE_BF_DIR_CHANNEL_MAX][VOICE_BF_DIR_CHANNEL_SIZE]
	__attribute__((aligned(128)));
int16_t VOICE_BF_VOC_BUFFER[VOICE_BF_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));

extern int dir_logic(void);
extern int dir_dma_logic(void);

int bf_rdy(void *ctx)
{
	printf("[warning]: you should not see this if you are using dma in audio_bf.\n");

	struct audio_bf_int_stat_t rdy_reg = audio_bf->bf_int_stat_reg;

	if (rdy_reg.dir_search_data_rdy) {
		for (size_t ch = 0; ch < VOICE_BF_DIR_CHANNEL_MAX; ch++) {
			for (size_t i = 0; i < 256; i++) { //
				uint32_t data = audio_bf->sobuf_dma_rdata;

				VOICE_BF_DIR_BUFFER[ch][i * 2 + 0] =
					data & 0xffff;
				VOICE_BF_DIR_BUFFER[ch][i * 2 + 1] =
					(data >> 16) & 0xffff;
			}
		}
		dir_logic();
		audio_bf_dir_clear_int_state();
		audio_bf_dir_enable();
	} else if (rdy_reg.voc_buf_data_rdy) {
		for (size_t i = 0; i < 256; i++) { //
			uint32_t data = audio_bf->vobuf_dma_rdata;

			VOICE_BF_VOC_BUFFER[i * 2 + 0] = data & 0xffff;
			VOICE_BF_VOC_BUFFER[i * 2 + 1] = (data >> 16) & 0xffff;
		}
		audio_bf_voc_clear_int_state();
	} else { //
		printf("[waring]: unknown %s interrupt cause,\nrdy_reg: %x, i2s_ch0.isr: %x, i2s_ch1.isr: %x, i2s_ch2.isr: %x, i2s_ch3.isr: %x\n",
		       __func__, *(uint32_t *)&rdy_reg, i2s[0]->channel[0].isr,
		       i2s[0]->channel[1].isr, i2s[0]->channel[2].isr,
		       i2s[0]->channel[3].isr);
	}
	return 0;
}

int bf_dir_dma_fin(void *ctx)
{
	//  printf("dma_dir.\n");
	uint64_t chx_intstatus = dmac->channel[VOICE_BF_DIR_DMA_CH].intstatus;

	if (chx_intstatus & 0x02) {
		dmac_chanel_interrupt_clear(VOICE_BF_DIR_DMA_CH);

		dmac->channel[VOICE_BF_DIR_DMA_CH].dar =
			(uint64_t)VOICE_BF_DIR_BUFFER;
		dmac->chen = 0x0101 << VOICE_BF_DIR_DMA_CH;

		audio_bf_dir_enable();
		dir_dma_logic();
	} else {
		LOGW(__FILE__, "unknown dma interrupt. %lx %lx\n",
		     dmac->intstatus, dmac->com_intstatus);
		LOGW(__FILE__, "dir intstatus: %lx\n", chx_intstatus);

		dmac_chanel_interrupt_clear(VOICE_BF_DIR_DMA_CH);
	}
	return 0;
}

int bf_voc_dma_fin(void *ctx)
{
	// printf("dma_voc.\n");
	uint64_t chx_intstatus = dmac->channel[VOICE_BF_VOC_DMA_CH].intstatus;

	if (chx_intstatus & 0x02) {
		dmac_chanel_interrupt_clear(VOICE_BF_VOC_DMA_CH);

		dmac->channel[VOICE_BF_VOC_DMA_CH].dar =
			(uint64_t)VOICE_BF_VOC_BUFFER;
		dmac->chen = 0x0101 << VOICE_BF_VOC_DMA_CH;
	} else {
		LOGW(__FILE__, "unknown dma interrupt. %lx %lx\n",
		     dmac->intstatus, dmac->com_intstatus);
		LOGW(__FILE__, "voc intstatus: %lx\n",
		     dmac->channel[VOICE_BF_VOC_DMA_CH].intstatus);

		dmac_chanel_interrupt_clear(VOICE_BF_VOC_DMA_CH);
	}
	return 0;
}
