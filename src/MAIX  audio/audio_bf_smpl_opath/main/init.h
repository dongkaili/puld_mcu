#pragma once
#include <stdint.h>
#include <plic.h>
#include <i2s.h>
#include <sysctl.h>
#include <dmac.h>
#include <fpioa.h>
#include "ws2812b.h"
#include "gpiohs.h"
#include <audio_bf.h>
#include <stdio.h>


void init_all(plic_irq_callback_t bf_rdy, uint16_t *voice_bf_dir_buffer,
	      int dir_ch, plic_irq_callback_t dma_dir_cb,
	      uint16_t *voice_bf_voc_buffer, int voc_ch,
	      plic_irq_callback_t dma_voc_cb);
