#include "init.h"
#include <env/encoding.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "callbacks.h"

int count;
int assert_state;

int dir_logic(void)
{
	if (count < 5) {
		for (size_t ch = 0; ch < VOICE_BF_DIR_CHANNEL_MAX; ch++) {  //
			for (size_t i = 0; i < 512; i++) {		    //
				if (VOICE_BF_DIR_BUFFER[ch][i] != ch * i) { //
					printf("at %lx %lx, expect %lx got %x\n",
					       ch, i, ch * i,
					       VOICE_BF_DIR_BUFFER[ch][i]);
					assert_state = -1;
				}
			}
		}
		count++;
	}
	return 0;
}

int dma_dir_logic(void)
{
	return 0;
}


int main(void)
{
	clear_csr(mie, MIP_MEIP);
	printf("init start.\n");

	init_all(&bf_rdy, (uint16_t *)VOICE_BF_DIR_BUFFER, VOICE_BF_DIR_DMA_CH,
		 NULL, (uint16_t *)VOICE_BF_VOC_BUFFER, VOICE_BF_VOC_DMA_CH,
		 NULL);
	printf("init done.\n");
	set_csr(mie, MIP_MEIP);
	set_csr(mstatus, MSTATUS_MIE);

	while (count < 5) { //
		asm volatile("nop");
		// dir_led();
	}
	if (assert_state < 0) { //
		printf("__TEST_FAIL__\n");
	} else {
		printf("__TEST_OK__\n");
	}
	printf("__TEST_FINISH__\n");
}
