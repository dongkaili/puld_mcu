#pragma once
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <encoding.h>

#include "init.h"

#define VOICE_BF_DIR_DMA_CH DMAC_CHANNEL4
#define VOICE_BF_VOC_DMA_CH DMAC_CHANNEL3
#define VOICE_BF_DIR_CHANNEL_MAX 16
#define VOICE_BF_DIR_CHANNEL_SIZE 512
#define VOICE_BF_VOC_CHANNEL_SIZE 512

extern int16_t VOICE_BF_DIR_BUFFER[VOICE_BF_DIR_CHANNEL_MAX]
				  [VOICE_BF_DIR_CHANNEL_SIZE]
	__attribute__((aligned(128)));
extern int16_t VOICE_BF_VOC_BUFFER[VOICE_BF_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));


int bf_rdy(void *ctx);

int bf_dir_dma_fin(void *ctx);

int bf_voc_dma_fin(void *ctx);
