#include "init.h"
#include <stddef.h>

void init_fpioa(void)
{
	printf("init fpioa.\n\r");
	fpioa_init();
// 	fpioa_set_function(19, FUNC_GPIOHS4); // what was this used for? 
	fpioa_set_function(23, FUNC_I2S0_IN_D0);
	fpioa_set_function(22, FUNC_I2S0_IN_D1);
	fpioa_set_function(21, FUNC_I2S0_IN_D2);
	fpioa_set_function(20, FUNC_I2S0_IN_D3);
	fpioa_set_function(19, FUNC_I2S0_WS);
	fpioa_set_function(18, FUNC_I2S0_SCLK);
}

static void init_pll_aux(uint32_t pll_idx, uint32_t freq)
{
/* Enable and reset all PLL */
//	sysctl_pll_enable(SYSCTL_PLL0 + pll_idx);
//	sysctl_pll_enable(pll_idx);
/* Set all PLL to 150MHz */
//	sysctl_pll_set_freq(SYSCTL_PLL0 + pll_idx, SYSCTL_SOURCE_IN0, freq);
	sysctl_pll_set_freq(pll_idx, freq);
	printf("here 4\n\r");
	sysctl_pll_set_freq(pll_idx, freq);
	printf("here 5\n\r");
	/* Clear PLL lock status */
//	sysctl_pll_clear_slip(SYSCTL_PLL0 + pll_idx);
	sysctl_pll_clear_slip(pll_idx);
	/* Waiting PLL lock */
//	while (!sysctl_pll_is_lock(SYSCTL_PLL0 + pll_idx))
//		sysctl_pll_clear_slip(SYSCTL_PLL0 + pll_idx);
	while (!sysctl_pll_is_lock(pll_idx))
		sysctl_pll_clear_slip(pll_idx);
	/* Enable PLL clock output */
//	sysctl_clock_enable(SYSCTL_CLOCK_PLL0 + pll_idx);
	sysctl_clock_enable(pll_idx);
}

void init_pll(void)
{
	printf("init pll.\n\r");
	init_pll_aux(0, 300000000);
	/* Set ACLK to PLL0 */
	sysctl_clock_set_clock_select(SYSCTL_CLOCK_SELECT_ACLK,
				      SYSCTL_SOURCE_PLL0);
	init_pll_aux(2, 45000000);

	sysctl_clock_set_threshold(SYSCTL_THRESHOLD_I2S0, 0xf);
	sysctl_clock_set_threshold(SYSCTL_THRESHOLD_APB0, 0);

	// [note]: enable after clock init done.
	sysctl_clock_enable(SYSCTL_CLOCK_I2S0);

	sysctl_clock_enable(SYSCTL_CLOCK_DMA);
}

void init_i2s(void)
{
	printf("init i2s.\n\r");

//	i2s_device_enable(I2S_DEVICE_0);
	i2s_set_enable(I2S_DEVICE_0, 1);

	union _iter_u u_iter;

	u_iter.reg_data = readl(&i2s[0]->iter);
	u_iter.iter.txen = 0;
	writel(u_iter.reg_data, &i2s[0]->iter);


	union _ter_u u_ter;

	u_ter.reg_data = readl(&i2s[0]->channel[0].ter);
	u_ter.ter.txchenx = 0;
	writel(u_ter.reg_data, &i2s[0]->channel[0].ter);

	i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_0, RESOLUTION_16_BIT,
				 SCLK_CYCLES_16, TRIGGER_LEVEL_1,
			  STANDARD_MODE);
	i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_1, RESOLUTION_16_BIT,
				 SCLK_CYCLES_16, TRIGGER_LEVEL_1,
			  STANDARD_MODE);
	i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_2, RESOLUTION_16_BIT,
				 SCLK_CYCLES_16, TRIGGER_LEVEL_1,
			  STANDARD_MODE);
	i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_3, RESOLUTION_16_BIT,
				 SCLK_CYCLES_16, TRIGGER_LEVEL_1,
			  STANDARD_MODE);

	i2s_receive_enable(I2S_DEVICE_0, I2S_CHANNEL_0);
	i2s_receive_enable(I2S_DEVICE_0, I2S_CHANNEL_1);
	i2s_receive_enable(I2S_DEVICE_0, I2S_CHANNEL_2);
	i2s_receive_enable(I2S_DEVICE_0, I2S_CHANNEL_3);

	i2s_set_mask_interrupt(I2S_DEVICE_0, I2S_CHANNEL_0, 1, 1, 1, 1);
	i2s_set_mask_interrupt(I2S_DEVICE_0, I2S_CHANNEL_1, 1, 1, 1, 1);
	i2s_set_mask_interrupt(I2S_DEVICE_0, I2S_CHANNEL_2, 1, 1, 1, 1);
	i2s_set_mask_interrupt(I2S_DEVICE_0, I2S_CHANNEL_3, 1, 1, 1, 1);

	printf("i2s enable: %d %d %d %d\n\r", i2s[0]->ier, i2s[0]->irer,
	       i2s[0]->channel[0].rer, i2s[0]->channel[0].imr);
}

void init_bf(int dir_int_enabled, int voc_int_enabled)
{
	printf("init bf.\n\r");

	uint16_t fir0_prev[] = {
		0x020b, 0x0401, 0xff60, 0xfae2, 0xf860, 0x0022,
		0x10e6, 0x22f1, 0x2a98, 0x22f1, 0x10e6, 0x0022,
		0xf860, 0xfae2, 0xff60, 0x0401, 0x020b,
	};
	uint16_t fir0_post[] = {
		0xf649, 0xe59e, 0xd156, 0xc615, 0xd12c, 0xf732,
		0x2daf, 0x5e03, 0x7151, 0x5e03, 0x2daf, 0xf732,
		0xd12c, 0xc615, 0xd156, 0xe59e, 0xf649,
	};

	uint16_t fir_common[] = {0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3,
				 0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3,
				 0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3};
	apu_dir_set_prev_fir(fir0_prev);
	apu_dir_set_post_fir(fir0_post);
	apu_voc_set_post_fir(fir0_post);
	apu_voc_set_post_fir(fir0_post);

	uint8_t offsets[16][8] = {
		{22, 17, 6, 0, 6, 17, 11},  {21, 19, 9, 0, 1, 12, 10},
		{19, 22, 14, 3, 0, 8, 11},  {15, 22, 18, 7, 0, 4, 11},
		{10, 19, 19, 10, 0, 0, 10}, {7, 18, 22, 15, 4, 0, 11},
		{3, 14, 22, 19, 8, 0, 11},  {0, 9, 19, 21, 12, 1, 10},
		{0, 6, 17, 22, 17, 6, 11},  {0, 1, 12, 21, 19, 9, 10},
		{3, 0, 8, 19, 22, 14, 11},  {7, 0, 4, 15, 22, 18, 11},
		{10, 0, 0, 10, 19, 19, 10}, {15, 4, 0, 7, 18, 22, 11},
		{19, 8, 0, 3, 14, 22, 11},  {21, 12, 1, 0, 9, 19, 10} //
	};
	for (int i = 0; i < 16; i++) {//
		apu_set_direction_delay(i, offsets[i]);
	}
	apu_voc_set_direction(3);
	apu_set_channel_enabled(0x7f);
	apu_set_audio_gain((1<<10)/8);

	apu_set_interrupt_mask(!dir_int_enabled, !voc_int_enabled);
	apu_set_down_size(0, 0);

	apu_dir_enable();
	apu_voc_enable(1);
}

void init_dma(int dir_ch, int voc_ch, int int_en)
{
	printf("init dma.\n\r");
	// dmac enable dmac and interrupt
//	union dmac_cfg_u dmac_cfg;
	dmac_cfg_u_t dmac_cfg;

	dmac_cfg.data = readq(&dmac->cfg);
	dmac_cfg.cfg.dmac_en = 1;
	dmac_cfg.cfg.int_en = int_en; // 0 to disable dma int
	writeq(dmac_cfg.data, &dmac->cfg);

	sysctl_dma_select(SYSCTL_DMA_CHANNEL_0 + dir_ch,
			  SYSCTL_DMA_SELECT_I2S0_BF_DIR_REQ);
	sysctl_dma_select(SYSCTL_DMA_CHANNEL_0 + voc_ch,
			  SYSCTL_DMA_SELECT_I2S0_BF_VOICE_REQ);
}

void init_dma_ch(int ch, volatile uint32_t *src_reg,
		 uint16_t *voice_bf_dir_buffer, size_t size_of_byte)
{
	printf("init dma ch %d.\n\r", ch);
	dmac->channel[ch].sar = (uint64_t)src_reg;
	dmac->channel[ch].dar = (uint64_t)voice_bf_dir_buffer;
	dmac->channel[ch].block_ts = (size_of_byte / 4) - 1;
	dmac->channel[ch].ctl =
		(((uint64_t)1 << 47) | ((uint64_t)15 << 48)
		 | ((uint64_t)1 << 38) | ((uint64_t)15 << 39)
		 | ((uint64_t)3 << 18) | ((uint64_t)3 << 14)
		 | ((uint64_t)2 << 11) | ((uint64_t)2 << 8) | ((uint64_t)0 << 6)
		 | ((uint64_t)1 << 4) | ((uint64_t)1 << 2) | ((uint64_t)1));
	/*
	 * dmac->channel[ch].ctl = ((  wburst_len_en  ) |
	 *                        (    wburst_len   ) |
	 *                        (  rburst_len_en  ) |
	 *                        (    rburst_len   ) |
	 *                        (one transaction:d) |
	 *                        (one transaction:s) |
	 *                        (    dst width    ) |
	 *                        (    src width   ) |
	 *                        (    dinc,0 inc  )|
	 *                        (  sinc:1,no inc ));
	 */

	dmac->channel[ch].cfg = (((uint64_t)1 << 49) | ((uint64_t)4 << 44)
				 | ((uint64_t)4 << 39) | ((uint64_t)2 << 32));
	/*
	 * dmac->channel[ch].cfg = ((     prior       ) |
	 *                         (      dst_per    ) |
	 *                         (     src_per     )  |
	 *           (    peri to mem  ));
	 *  01: Reload
	 */

	dmac->channel[ch].intstatus_en = 0x2; // 0xFFFFFFFF;
	dmac->channel[ch].intclear = 0xFFFFFFFF;

	dmac->chen = 0x0101 << ch;
}


void init_interrupt(plic_irq_callback_t bf_rdy, int dir_ch,
		    plic_irq_callback_t dma_dir_cb, int voc_ch,
		    plic_irq_callback_t dma_voc_cb)
{
	printf("init interrupt. bf:%p dir:%p voc:%p\n\r", bf_rdy, dma_dir_cb,
	       dma_voc_cb);

	plic_init();
	// bf
	plic_set_priority(IRQN_I2S0_INTERRUPT, 4);
	plic_irq_enable(IRQN_I2S0_INTERRUPT);
	plic_irq_register(IRQN_I2S0_INTERRUPT, bf_rdy, NULL);

	if (dma_dir_cb) {
		// dma
		plic_set_priority(IRQN_DMA0_INTERRUPT + dir_ch, 4);
		plic_irq_register(IRQN_DMA0_INTERRUPT + dir_ch, dma_dir_cb,
				  NULL);
		plic_irq_enable(IRQN_DMA0_INTERRUPT + dir_ch);
	}
	if (dma_voc_cb) {
		// dma
		plic_set_priority(IRQN_DMA0_INTERRUPT + voc_ch, 4);
		plic_irq_register(IRQN_DMA0_INTERRUPT + voc_ch, dma_voc_cb,
				  NULL);
		plic_irq_enable(IRQN_DMA0_INTERRUPT + voc_ch);
	}
}

void init_ws2812b(void)
{
	gpiohs->output_en.bits.b4 = 1;
	gpiohs->output_val.bits.b4 = 0;
}

void init_all(plic_irq_callback_t bf_rdy, uint16_t *voice_bf_dir_buffer,
	      int dir_ch, plic_irq_callback_t dma_dir_cb,
	      uint16_t *voice_bf_voc_buffer, int voc_ch,
	      plic_irq_callback_t dma_voc_cb)
{
	init_fpioa();
//	init_pll();
	init_interrupt(bf_rdy, dir_ch, dma_dir_cb, voc_ch, dma_voc_cb);
	init_i2s();
printf("here 1\n\r");
	init_bf(dma_dir_cb == NULL && bf_rdy != NULL,
		dma_voc_cb == NULL && bf_rdy != NULL);
	if (dma_dir_cb || dma_voc_cb) {
		init_dma(dir_ch, voc_ch, dma_dir_cb || dma_voc_cb);
		if (dma_dir_cb) {
			init_dma_ch(dir_ch, &apu->sobuf_dma_rdata,
				    voice_bf_dir_buffer, 512 * 16 * 2);
		}
		if (dma_voc_cb) {
			init_dma_ch(voc_ch, &apu->vobuf_dma_rdata,
				    voice_bf_voc_buffer, 512 * 2);
		}
	}
printf("here 2\n\r");
	init_ws2812b();
printf("here 3\n\r");
	init_pll();
}
