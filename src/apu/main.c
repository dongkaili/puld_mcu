#include "init.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <printf.h>
#include "apu.h"
#include "fpioa.h"
#include "gpio.h"

int count;
int assert_state;

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#define DEBUG_APU_BARCHART
#define PAUSE_APU_CHART
#define DEBUG_APU
//#define DEBUG_APU_DUMP_BUFFERS
//#define DEBUG_APU_DUMP_BUFFERS_SAMPLE_SIZE 250
//#define DEBUG_APU_VOC

typedef struct CH_Data_
{
  int8_t CH;    // retain original channel number after sort
  uint32_t dir_sum;
} CH_Data_s;

typedef struct Mics_
{
  // 1st 4 items could be 4-bits each... but why bother
  int8_t ID_Max;      // index of entry with largest dir_sum value
  int8_t ID_First;     // First entry in linked list of entries on bell curve (may be at end of chart and wrap 15-0)
  int8_t ID_Last;     // last entry in linked list of entries on bell curve
  int8_t N_linked;    // # of linked, consecutive items (data quality measure) = ((ID_Last - ID_First + 1 ) + 16) % 16;
  uint32_t SUM_x;
  uint32_t SUM_xx, SS_x;
  uint32_t AVG_dir_sum;    // avg of dir_sum values in bell curve linked items = Sum(dir_sum[i])/N_linked;
  uint32_t StdDev_dir_sum;     // error of dir_sum values in bell curve linked list... use to detect when src is close to dead-center
  float ID_Max_r;    // Better approxmation (weighted avg ) to index of max level
  float Angle;       // Angle to source = Sum(Angle[i]*dir_sum[i])/Sum(dir_sum[i]);
  // include general summation statitics like SUM(XY), SUM(X^2), SUM(Y^2)?
    CH_Data_s CH_Data[APU_DIR_CHANNEL_MAX];     // = { 0 };  
  
} Mics_S;

#define Angle_Moving_Average_Points 10
float Angle[Angle_Moving_Average_Points] = { 0.0 } ;

Mics_S Mics = { 0 }; // defined at top so global visibility
static uint32_t overflow_count = 0;
static uint32_t near_overflow_count = 0; // something to indicate when we're within 10% of overflow in buffer values?
char get_temp_str[10];
uint32_t dir_max = 0;
uint8_t no_sig_found = 0;                   // return value for function
uint8_t invalid_voc;
gpio_pin_value_t value = GPIO_PV_LOW;

#ifdef PDM4_to_I2S4
//#define MIN_LEVEL_THRESHHOLD 0x00000800 // noise floor... if dir_max < this then zero display... may be adjusted 
//  #define MIN_LEVEL_THRESHHOLD 0x00000100 // noise floor... if dir_max < this then zero display... may be adjusted 
  #define MIN_LEVEL_THRESHHOLD 0x00000080 // noise floor... if dir_max < this then zero display... may be adjusted 
  #define MIN_DETECT_COUNTER_THRESHHOLD 0
#else
//  #define MIN_LEVEL_THRESHHOLD 0x00001000 // noise floor... higher threshold for audible levels 
  #define MIN_LEVEL_THRESHHOLD 0x00000001 // noise floor... higher threshold for audible levels 
  #define MIN_DETECT_COUNTER_THRESHHOLD 0 // throw away more before 
#endif

#ifdef DEBUG_APU_DUMP_BUFFERS_SAMPLE_SIZE
    uint16_t Trial_ID, Trial_Num = 0; // Counters to group data for related test samples
#endif
// Clear the terminal screen
void ClrScr(void)
{
   printf("\x1B[2J");
} 

// https://solarianprogrammer.com/2019/04/08/c-programming-ansi-escape-codes-windows-macos-linux-terminals/
void moveTo(int row, int col) {
  printf("\x1b[%d;%df", row, col);
}

// https://www.ccsinfo.com/forum/viewtopic.php?t=36549
// TURN THE CURSOR ON/OFF
void Cursor(int IO)
{
   if(IO)
   {
      printf("\x1B[?25h");
   }
   else
   {
      printf("\x1B[?25l");
   }
} 

void saveCursorPosition(void)
{
  printf("\x1b%d", 7);    // Save cursor
};

void restoreCursorPosition(void)
{
  printf("\x1b%d", 8);    // Restore saved cursor
};

void  Pause(char *str)
{
  printf("\n\r");
  printf("%s \n\rPress ENTER key to Continue... \n\r", str);
  getchar();
};


int Ask(char *str)
{
  char c, d;
  printf("%s", str);
//  scanf(" %c %c",&c,&d);
  scanf(" %c",&c);
  d = 'y';
  if (c==d)
    return 1;
  else
    return 0;
};


/////////
//int dir_logic(void) __attribute__((optimize(0)));

// Ex: https://stackoverflow.com/questions/6105513/need-help-using-qsort-with-an-array-of-structs 
int dir_sum_compare(const void * a, const void * b)
{
  CH_Data_s *CH_DataA = (CH_Data_s *)a;
  CH_Data_s *CH_DataB = (CH_Data_s *)b;
  return (CH_DataB->dir_sum - CH_DataA->dir_sum);
}    

int CH_ID_compare(const void * b, const void * a)
{
  CH_Data_s *CH_DataA = (CH_Data_s *)a;
  CH_Data_s *CH_DataB = (CH_Data_s *)b;
  return (CH_DataB->CH - CH_DataA->CH);
}    

void Display_Barchart()
{
#ifdef DEBUG_APU_BARCHART
  // Bar chart...
  #ifdef PAUSE_APU_CHART
    ClrScr();
  #endif
  for(size_t ch = 0 ; ch < APU_DIR_CHANNEL_MAX ; ch++) {
     // for each direction... 
          printf("%3d |", (int)ch);
    if (ch == Mics.ID_Max) 
      // set color green for dominant channel
      printf("%s", KGRN);
    else if (Mics.ID_First <= Mics.ID_Last)
    {
      if ((ch >= Mics.ID_First) && (ch <= Mics.ID_Last))
        printf("%s", KYEL);
      else
        printf("%s", KRED);
    }
    else if (Mics.ID_First > Mics.ID_Last)
    {
      if ((ch >= Mics.ID_First) || (ch <= Mics.ID_Last))
        printf("%s", KYEL);
      else
        printf("%s", KRED);
    }
    ;
    
		for (size_t i = 0; i < 64; i++) {                            
      // print asterisks to indicate magnitude of cross-correlation result in that direction
      // could use BREAK statement to quit when condition is false as that will be true for remainder of time in lop
      if ((((int32_t) Mics.CH_Data[ch].dir_sum * 64 / dir_max) > i) && (no_sig_found == 0))
        printf("*");
      else
        printf(" ");
    } 
    printf("%s", KNRM);
    printf("\n\r");        
  }       
  printf("\n\r");        

  printf("Angle = %5.1f \n\rID_Max = %2d \n\rID_Max_r = %4.1f \n\rID_First = %2d \n\rID_Last = %2d \n\rN_linked = %d \n\r\n\r",
    Mics.Angle,
    Mics.ID_Max,
    Mics.ID_Max_r,
    Mics.ID_First,
    Mics.ID_Last,
    Mics.N_linked);    

    printf("APU_DIR_BUFFER range limit hit %d times.\n\r", overflow_count);
    printf("APU_DIR_BUFFER range limit nearly hit %d times.\n\r", near_overflow_count);

    apu_ch_cfg_t bf_ch_cfg_reg = apu->bf_ch_cfg_reg;
    printf("  Current gain =  %d\n\r\n\r", bf_ch_cfg_reg.audio_gain);

#endif

#if defined (DEBUG_APU_BARCHART) || defined (DEBUG_APU_DUMP_BUFFERS)
/* 
  Dump 16 channels of DIR data in compact format so it can be captured in a file from a terminal.
  16 CH x 4 bytes/CH (32 bits) = 64 bytes
  
  Try to figure out how to add in the VOC stuff as well... Do over there instead?
  
*/
#if defined(DEBUG_APU_DUMP_BUFFERS) && defined (DEBUG_APU_DUMP_BUFFERS_SAMPLE_SIZE)
  if (Trial_ID == 0) // 1st time in
  {
    ++Trial_ID;
    printf("\n\rReady to begin new series of tests.\n\r");
    Pause("Position equipment in starting position and enable data logging.\n\r");

    int16_t New_Trial_ID = atoi(get_temp_str);
    printf("Trial_ID = %d\n\r", Trial_ID);
  }
  if (Trial_Num == DEBUG_APU_DUMP_BUFFERS_SAMPLE_SIZE) // finished current series of tests so prep for next
  {
    ++Trial_ID;
    Trial_Num=0;
    printf("\n\rReady to begin trial # %d.\n\r", Trial_ID);

    Pause("Reposition for next test.");
  }
  else
  {
    ++Trial_Num;
  }  
    printf("%d\t", Trial_ID); // prepend line with common id for subset of tests
//    printf("%d\t", (Trial_ID-19)*5); // prepend line with array angle of current test (-90 to +90 deg in steps of 5 = 37 tests)

#endif

  for (size_t ch = 0; ch < APU_DIR_CHANNEL_MAX; ch++) {
  #ifdef DEBUG_APU_BARCHART // OK to apply color formatting if only displaying one line of data with bar chart
    if (ch == Mics.ID_Max) 
      // set color green for dominant channel
      printf("%s", KGRN);
    else if (Mics.ID_First <= Mics.ID_Last)
    {
      if ((ch >= Mics.ID_First) && (ch <= Mics.ID_Last))
        printf("%s", KYEL);
      else
        printf("%s", KRED);
    }
    else if (Mics.ID_First > Mics.ID_Last)
    {
      if ((ch >= Mics.ID_First) || (ch <= Mics.ID_Last))
        printf("%s", KYEL);
      else
        printf("%s", KRED);
    };
  #endif    

    printf("%8d", Mics.CH_Data[ch].dir_sum);
  #ifndef DEBUG_APU_BARCHART
    printf("\t");
  #else
    printf("%s ", KNRM);    
  #endif     
  };
  printf("\n"); 
#endif

#ifdef PAUSE_APU_CHART
  Pause("");
#endif

#ifdef DEBUG_APU // Dump APU_DIR_BUFFER table to console
  uint16_t result;
  result = (Ask("\rGenerate DIR buffer data table (y/n)?\n") == 1);
  if (result) {
    printf("i");
    for (size_t ch = 0; ch < APU_DIR_CHANNEL_MAX; ch++) 
      printf("\tBUFF[%2d]", (uint16_t) ch);
    printf("\n");
    for (size_t i = 0; i < APU_DIR_CHANNEL_SIZE; i++) {
      printf("%3d", (uint16_t) i);
      for (size_t ch = 0; ch < APU_DIR_CHANNEL_MAX; ch++) 
//        printf("\t%3d:%12d", (uint32_t) ch, (int32_t) APU_DIR_BUFFER[ch][i]);
        printf("\t%12d", (int32_t) APU_DIR_BUFFER[ch][i]);
      printf("\n");
    };
    Pause("");
  };
#endif
};

int __attribute__((optimize(0))) dir_logic(void)
{
  dir_max = 0;  
  uint16_t context = 0;
  static uint16_t min_level_threshold = MIN_LEVEL_THRESHHOLD;
  static uint16_t min_detect_counter_threshold = MIN_DETECT_COUNTER_THRESHHOLD;
  bool overflow_detected;
  overflow_count = 0;
  static uint16_t prev_sig_found_counter = 0;  // Counter to allow detection of spurious data at low end of threshold
                                               // Increment when data detected but only return/display data if counter exceeds threhsold
                                               // Cleared when data not found
  no_sig_found = 0;
 

#if defined (DEBUG_APU_BARCHART) || defined (DEBUG_APU)
  moveTo(0, 0);
  Cursor(0);
#endif

  overflow_detected = false;
  // This is the main loop that goes through all the elements of the 2D APU_DIR_BUFFER
  for(size_t ch = 0 ; ch < APU_DIR_CHANNEL_MAX ; ch++) {
    for(size_t i = 0 ; i < APU_DIR_CHANNEL_SIZE ; i++) {

      // higher energy in ultrasonic? Getting overflow!!! Need gain adjustment!!!!!!!
      Mics.CH_Data[ch].dir_sum += (int32_t)APU_DIR_BUFFER[ch][i] * (int32_t)APU_DIR_BUFFER[ch][i];

      // Check for +/- full scale readings indicating overflow
      if (((int32_t) APU_DIR_BUFFER[ch][i] == 0x00007FFF) || ((int32_t) APU_DIR_BUFFER[ch][i] == 0xFFFF8000))
      {
        overflow_count += 1;
        overflow_detected  = true; 
        break; // quit inner loop
      }
        // Check for near (within 10%) +/- full scale readings indicating near overflow
      if (!abs( (int32_t) APU_DIR_BUFFER[ch][i] < (0x00007FFF*0.9))) // SEH: Set threshhold with parameter
        near_overflow_count += 1;

      // May need to check dir_sum... Squaring values that are already close to max int16          
        
//      #ifdef DEBUG_APU
//            printf("ch = %3d, i = %3d, ", (uint32_t) ch, (uint32_t) i); 
//            printf("APU_DIR_BUFFER[%2d][%3d] = %8d (0x%08x), dir_sum[%2d] = %8d (0x%08x)\n\r", (int32_t) ch, (int32_t) i, (int16_t) APU_DIR_BUFFER[ch][i], (int16_t) APU_DIR_BUFFER[ch][i], (int32_t) ch, (int32_t) Mics.CH_Data[ch].dir_sum, (int32_t) Mics.CH_Data[ch].dir_sum); 
//      #endif
       
    }

    if (overflow_detected)
    {
      break; // quit outer loop 
    }
      
      
    // Calculate value for current channel
    Mics.CH_Data[ch].dir_sum = Mics.CH_Data[ch].dir_sum / APU_DIR_CHANNEL_SIZE;
    Mics.CH_Data[ch].CH = ch;  // save index
      
    // Original max check method - may be deleteed when sort routine is finished
    if(Mics.CH_Data[ch].dir_sum > dir_max) {
      dir_max = Mics.CH_Data[ch].dir_sum;
      context = ch;
    }  
  }

  if (overflow_detected)
    {
      goto quit_loop; // jump to end to initiate new capture
    }

  
#ifdef DEBUG_APU_LINKED_LIST
  printf("\n\r\n\rBefore sort...\n\r");
  for (int i = 0; i < 16; i++)  
  {
    printf("Mics.CH_Data[%2d]: CH = %2d, dir_sum = %8d\n\r",
      (uint16_t) i,
      (uint16_t) Mics.CH_Data[i].CH,
      (uint32_t) Mics.CH_Data[i].dir_sum);
  }
#endif
  
  // Sort by dir_sum in descending order
  qsort(Mics.CH_Data, APU_DIR_CHANNEL_MAX, sizeof(CH_Data_s), dir_sum_compare);
    
#ifdef DEBUG_APU_LINKED_LIST
  printf("\n\r\n\rAfter sort...\n\r");
  for (int i = 0; i < APU_DIR_CHANNEL_MAX; i++)  
  {
    printf("Mics.CH_Data[%2d]: CH = %2d, dir_sum = %8d\n\r",
      (uint16_t) i,
      (uint16_t) Mics.CH_Data[i].CH,
      (uint32_t) Mics.CH_Data[i].dir_sum);
  }
  printf("\n\r");
#endif
 
  /* 

  Mechanism to filter out low quality data...

  Good data looks like this when plotted...
   
      0 |*****
      1 |******************************
      2 |*****************************************************
      3 |****************************************************************
      4 |*************************************************
      5 |**************************
      6 |*******
      7 |*
      8 |
      9 |*******
     10 |****************
     11 |*******************************
     12 |******************************
     13 |****************
     14 |*******
     15 |

    There is a maximum at one channel index from 0-15 and the values on either side drop off gradually until 
    the values get into the noise. The smoother the rolloff on either side of the peak the better. 

    We also commonly see a 2nd peak group which may be interpretted as a reflection of the 1st but this is not the case.
    The nature of the UCA8 beamformer is such that we only consider the sensors on the incident side of the "virtual
    mapping line, which is the reference line for delay computations" as described in "Beamforming Using Uniform Circular 
    Arrays for Distant Speech Recognition in Reverberant Environment and Double-Talk Scenarios" 
    https://www2.spsc.tugraz.at/www-archive/downloads/thesis_reduced_file_size.pdf

    From this we should only work with ~8 data points for the analysis. The dominant channel and the +/- 4 channels 
    on either side of the dominant channel. So right away we cut the list of 16 values down to 9 (or perhaps 7). 
    We can then try different methods to assess the quality of the data (N_linked algorithm already developed and then 
    calculate the angle to source using a weighted average or a circle fitting algorithm. Circle fitting may have the 
    advantage of providing a distance indication as well as angle. Rather than a circle an oval might be better.
    
    Prior to building this chart we would sort the data into a 9 (or 7) element array from max_VAL to min_VAL with the 
    value at each position being the channel index. For the above example the array would look like:
    DIR_ID[0..15] = {3, 2, 4, 11, 1, 12, 5, 13, 10, 14, 9, 1, 6, 1, 7, 8}
	
    To assess the quality of the data set we would scan the list starting at pos #0 (DIR_ID[1] = 2) and check to see if 
    that value is either 1 less than the previous left-most value or 1 more than the previous right-most value. If so then we have a so 
    far unbroken chain.

    For this example we start with DIR_ID_LHS=DIR_ID_RHS=DIR_ID[0]=3
    Then for DIR_ID[1]=2 we have 2 = 3-1 so they're linked and we can continue  (DIR_ID_LHS=2; DIR_ID_RHS=3)
    For DIR_ID[2]=4 we have 4 <> 2-1  but 4 = 3+1 so we still have a linked chain and we can continue (DIR_ID_LHS=2; DIR_ID_RHS=4)
    
    Next time we test DIR_ID[3]=11 and we have 11 <> 2-1 and 11 <> 4+1. Neither test condition is true so the chain is broken and quality=i=3

    This example does not consider the array geometry and that we should only use the sensors on teh oncident side for the analysis. 
    The algorithm can be improved.

    Pseudo Algorithm...

    DIR_ID_LHS = DIR_ID[0]
    DIR_ID_RHS = DIR_ID[0]

    For (i=1, i<16, i=i+1) // start with 2nd element in DIR_ID array 
    begin
     	linked=false
	
     	// check left side
     	if ((DIR_ID[mod((i+16)-1,16)]=DIR_ID_LHS-1) then // mod function wraps so 15+1==0 and 0-1==15 ... see ./resources/MOD_16_Test.xlsx
     	begin
     		linked=true
     		DIR_ID_LHS = DIR_ID[mod((i+16)-1,16)]
   end

   // check right side
   if (DIR_ID[mod((i+16)+1,16)]=DIR_ID_RHS+1) then
   begin
   	linked=true
   	DIR_ID_RHS = DIR_ID[mod((i+16)+1,16)]
  end
	
  // Do we still have a linked chain?
  if (linked==false)
  begin
  	quality=i
  	break
  end
end
// Return direction to source as DIR_ID[0]*360/16 (or perhaps a weighted average or geometric mean of the 1st few values...)

In this example, direction = 11*360/16 = 247.5 deg clockwise from the direction of the 1st sensor.

*/

  // Link Check Routine

  int8_t ID_LHS = Mics.CH_Data[0].CH;
  int8_t ID_RHS = Mics.CH_Data[0].CH;
  int8_t ID_HEAD = Mics.CH_Data[0].CH;
  int8_t ID_TAIL = Mics.CH_Data[0].CH;
  bool linked;
    
  for (int8_t i = 1; i < APU_DIR_CHANNEL_MAX; i++) // start with 2nd element in array  
    {
      linked = false;
	
      // check left side
      if(Mics.CH_Data[i].CH == ((ID_LHS - 1 + APU_DIR_CHANNEL_MAX) % APU_DIR_CHANNEL_MAX)) // mod function wraps so 15+1==0 and 0-1==15 ... see ./resources/MOD_16_Test.xlsx
      {
        linked = true;
        ID_LHS = Mics.CH_Data[i].CH;
      }

      // check right side
      else if(Mics.CH_Data[i].CH == ((ID_RHS + 1) % APU_DIR_CHANNEL_MAX))
      {
        linked = true;
        ID_RHS = Mics.CH_Data[i].CH;
      }
	
      // Do we still have a linked chain?
      if(linked == true)
        ID_TAIL = Mics.CH_Data[i].CH;
      else
        break;
    }
  ;  

  // Now store relevant info in fields of Mics variable
  Mics.ID_Max   = ID_HEAD;
  Mics.ID_First = ID_LHS;
  Mics.ID_Last  = ID_RHS;
  Mics.N_linked = (ID_RHS == ID_LHS ? 1 : (ID_RHS > ID_LHS ? ID_RHS - ID_LHS + 1 : (ID_RHS + 16) - ID_LHS + 1));

  // Restore original sort order to compute statistics
  qsort(Mics.CH_Data, APU_DIR_CHANNEL_MAX, sizeof(CH_Data_s), CH_ID_compare);
  
  // Calculate statistics https://stattrek.com/statistics/formulas.aspx
  Mics.SUM_x = 0;
  Mics.SUM_xx = 0;
  //Mics.Angle = 0;
  int Angle_i, Angle_temp = 0;
  int SUM_i = 0;
  for (int i = Mics.ID_First; i <= Mics.ID_Last + ((Mics.ID_Last < Mics.ID_First ? APU_DIR_CHANNEL_MAX : 0)); i++) 
  {
    Mics.SUM_x = Mics.SUM_x + Mics.CH_Data[i % APU_DIR_CHANNEL_MAX].dir_sum;
    Mics.SUM_xx = Mics.SUM_xx + (Mics.SUM_x * Mics.SUM_x);
    Angle_i = Angle_i + (Mics.CH_Data[i % 16].dir_sum * i);  //Mics.CH_Data[i%16].CH); // working on weighted avg
    SUM_i = SUM_i + i;
  }
  Mics.AVG_dir_sum = Mics.SUM_x / Mics.N_linked;
  Mics.SS_x = Mics.SUM_xx - (Mics.SUM_x * Mics.SUM_x) / Mics.N_linked;
  Mics.ID_Max_r = (float) Angle_i / (float) Mics.SUM_x;
  Mics.ID_Max_r = (Mics.ID_Max_r > (APU_DIR_CHANNEL_MAX - 1) ? Mics.ID_Max_r - (APU_DIR_CHANNEL_MAX - 1) : Mics.ID_Max_r);
  Mics.Angle = (360.0 / APU_DIR_CHANNEL_MAX) * Mics.ID_Max_r;     // weighted average 

  // Determine if value detected is less than min threshold (indicates noise)
  if (dir_max < min_level_threshold) {
    no_sig_found = 1;
    prev_sig_found_counter = 0;
  }
  else {
    if (prev_sig_found_counter > min_detect_counter_threshold) {
      no_sig_found = 0;
      prev_sig_found_counter = prev_sig_found_counter; // hold value
    }
    else {
      no_sig_found = 1;
      prev_sig_found_counter += 1; // increment counter until threshold is exceeded and then hold to prevent eventual overflow
    }        
  }

  Display_Barchart();
   
quit_loop:;
  
// If DIR quality is high enough then enable the VOC function (if VOC is defined to run)
#if APU_VOC_ENABLE == 1
  if((Mics.N_linked > 4))
  {
    apu_voc_set_direction(Mics.ID_Max);
    apu_voc_enable(1); // Maybe this should be always running so data is right there when needed... Might not be optimal if done that way.
  }
  else
  {
    apu_dir_enable(); // re-fire the DIR search function
  }
#else
  apu_dir_enable(); // No VOC so just re-fire the DIR search function
#endif  
  return no_sig_found;
}

int voc_logic(void)
{
  apu_voc_enable(0); // Enabled by dir_logic function if and when we want

  u_int8_t voc_data_eq_count = 0;
  for (uint32_t i = 1; i <= 10; i++) 
    if ((APU_VOC_BUFFER[510 - i * 2] == APU_VOC_BUFFER[510]) && (APU_VOC_BUFFER[510 - i * 2+1] == APU_VOC_BUFFER[511]))
      voc_data_eq_count += 1;
  if (voc_data_eq_count == 10) // repeating patterns so faulty data
    invalid_voc = 1;
  else
    invalid_voc = 0;
  
//  if (!invalid_voc)
//  {
    // For an unknown reason enabling the Ask function and test results in a core exit.
    //  uint16_t result;n
    //  result = (Ask("\rGenerate VOC data table (y/n)?\n") == 1);
    //  if (result) {
#ifdef DEBUG_APU_VOC
      printf("i\tAPU_VOC_BUFFER\tAPU_DIR_BUFFER[%2d]\n", Mics.ID_Max);
#else
//     printf("\n"); // FAILS - results in core exit... Need to printf min of 2 characters... Why?????
//     printf(".."); // works
//       printf(".\n"); // works
       printf("\n\r"); // works
//       printf("."); // FAILS
//  printf("nop"); // works
//      printf("[%2d]", Mics.ID_Max); 
//  		asm volatile("nop");
#endif  		
    for (uint32_t i = 0; i < 512; i++) {
      //
#ifdef DEBUG_APU_VOC
       printf("%3d\t%8d\t%8d\n", i, APU_VOC_BUFFER[i], APU_DIR_BUFFER[Mics.ID_Max][i]) // results in disjointed data streams
#endif  		
      ;
    };
  //  };
  // Every other dump is showing mostly just 2 alternating values... TBD: needs to be investigated and resolved 
//  };
    
  apu_dir_enable();  // Final step... restart DIR search
  return invalid_voc;
}

int event_loop(void)
{
	while (1) {
		if (dir_logic_count > 0) {
			if (dir_logic() == 1)
#ifdef DEBUG_APU
  			printf("\n\rNo signal found.\n\r")
#endif
  		  ;
  		else
#ifdef DEBUG_APU
			  printf("\n\r                \n\r") // clears "No signal found" message if present
#endif
        ;        
			while (--dir_logic_count != 0) {
				printk("[warning]: %s, restart before prev callback has end\n\r",
				       "dir_logic");
			}
		}
		if (voc_logic_count > 0) {
			if (voc_logic() == 1)
#ifdef DEBUG_APU
  			printf("\n\rInvalid VOC data.\n\r")
#endif
  		  ;
  		else
#ifdef DEBUG_APU
			  printf("\n\r                \n\r") // clears "Invalid VOC data" message if present
#endif
        ;        
			while (--voc_logic_count != 0) {
				printk("[warning]: %s, restart before prev callback has end\n\r",
				       "voc_logic");
			}
		}
	}
	return 0;
}

int main(void)
{
#ifdef PDM4_to_I2S4
//	sysctl_pll_set_freq(SYSCTL_PLL2, 153600000UL); // May not be optimal value
//	sysctl_pll_set_freq(SYSCTL_PLL2, 196608000UL); // 64x BCLK
//	sysctl_pll_set_freq(SYSCTL_PLL2, 245760000UL); // 64x BCLK

//  #if (I2S_FS>=256000) && (I2S_FS<=384000) // OSR=8
//    sysctl_pll_set_freq(SYSCTL_PLL2, I2S_FS*32*16); // 16x BCLK... easy number to divide down
//  #else	  
    sysctl_pll_set_freq(SYSCTL_PLL2, I2S_FS*32*64); // 64x BCLK... easy number to divide down
//  #endif
#else
	sysctl_pll_set_freq(SYSCTL_PLL2, 45158400UL); // = 45.1 MHz
#endif
	printk("git id: %u\n\r", sysctl->git_id.git_id);
	printk("init start.\n\r");
	clear_csr(mie, MIP_MEIP);
	init_all();
	printk("init done.\n\r");
  
  set_csr(mie, MIP_MEIP);
	set_csr(mstatus, MSTATUS_MIE);

//  apu_print_setting();
//  Pause("");
//  printf("\e[1;1H\e[2J"); // clear screen

// LED proof of life
  fpioa_set_function(13, FUNC_GPIO3);

  gpio_init();
  gpio_set_drive_mode(3, GPIO_DM_OUTPUT);
  gpio_set_pin(3, value);
  
  event_loop();

}
