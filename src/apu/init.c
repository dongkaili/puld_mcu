#include "init.h"
#include <stddef.h>
#include <stdio.h>
#include <printf.h>
#include "apu.h"

uint64_t dir_logic_count;
uint64_t voc_logic_count;

#if APU_FFT_ENABLE // FFT only applies to VOC output so this must be wrong!
uint32_t APU_DIR_FFT_BUFFER[APU_DIR_CHANNEL_MAX]
				[APU_DIR_CHANNEL_SIZE]
	__attribute__((aligned(128)));
uint32_t APU_VOC_FFT_BUFFER[APU_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));
#else
int16_t APU_DIR_BUFFER[APU_DIR_CHANNEL_MAX][APU_DIR_CHANNEL_SIZE]
	__attribute__((aligned(128)));
int16_t APU_VOC_BUFFER[APU_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));
#endif


int int_apu(void *ctx)
{
	apu_int_stat_t rdy_reg = apu->bf_int_stat_reg;

	if (rdy_reg.dir_search_data_rdy) {
		apu_dir_clear_int_state();

#if APU_FFT_ENABLE
		static int ch;

		ch = (ch + 1) % 16;
		for (uint32_t i = 0; i < 512; i++) { //
			uint32_t data = apu->sobuf_dma_rdata;

			APU_DIR_FFT_BUFFER[ch][i] = data;
		}
		if (ch == 0) { //
			dir_logic_count++;
		}
#else
		for (uint32_t ch = 0; ch < APU_DIR_CHANNEL_MAX; ch++) {
			for (uint32_t i = 0; i < 256; i++) { //
				uint32_t data = apu->sobuf_dma_rdata;

				APU_DIR_BUFFER[ch][i * 2 + 0] =
					data & 0xffff;
				APU_DIR_BUFFER[ch][i * 2 + 1] =
					(data >> 16) & 0xffff;
			}
		}
		dir_logic_count++;
#endif

	} else if (rdy_reg.voc_buf_data_rdy) {
		apu_voc_clear_int_state();

#if APU_FFT_ENABLE
		for (uint32_t i = 0; i < 512; i++) { //
			uint32_t data = apu->vobuf_dma_rdata;

			APU_VOC_FFT_BUFFER[i] = data;
		}
#else
		for (uint32_t i = 0; i < 256; i++) { //
			uint32_t data = apu->vobuf_dma_rdata;

			APU_VOC_BUFFER[i * 2 + 0] = data & 0xffff;
			APU_VOC_BUFFER[i * 2 + 1] = (data >> 16) & 0xffff;
		}
#endif

		voc_logic_count++;
	} else { //
		printk("[warning]: unknown %s interrupt cause.\n\r", __func__);
	}
	return 0;
}

#if APU_DMA_ENABLE
int int_apu_dir_dma(void *ctx)
{
	uint64_t chx_intstatus =
		dmac->channel[APU_DIR_DMA_CHANNEL].intstatus;
	if (chx_intstatus & 0x02) {
		dmac_chanel_interrupt_clear(APU_DIR_DMA_CHANNEL);

#if APU_FFT_ENABLE
		static int ch;

		ch = (ch + 1) % 16;
		dmac->channel[APU_DIR_DMA_CHANNEL].dar =
			(uint64_t)APU_DIR_FFT_BUFFER[ch];
#else
		dmac->channel[APU_DIR_DMA_CHANNEL].dar =
			(uint64_t)APU_DIR_BUFFER;
#endif

		dmac->chen = 0x0101 << APU_DIR_DMA_CHANNEL;

#if APU_FFT_ENABLE
		if (ch == 0) { //
			dir_logic_count++;
		}
#else
		dir_logic_count++;
#endif

	} else {
		printk("[warning] unknown dma interrupt. %lx %lx\n\r",
		       dmac->intstatus, dmac->com_intstatus);
		printk("dir intstatus: %lx\n\r", chx_intstatus);

		dmac_chanel_interrupt_clear(APU_DIR_DMA_CHANNEL);
	}
	return 0;
}


int int_apu_voc_dma(void *ctx)
{
	uint64_t chx_intstatus =
		dmac->channel[APU_VOC_DMA_CHANNEL].intstatus;

	if (chx_intstatus & 0x02) {
		dmac_chanel_interrupt_clear(APU_VOC_DMA_CHANNEL);

#if APU_FFT_ENABLE
		dmac->channel[APU_VOC_DMA_CHANNEL].dar =
			(uint64_t)APU_VOC_FFT_BUFFER;
#else
		dmac->channel[APU_VOC_DMA_CHANNEL].dar =
			(uint64_t)APU_VOC_BUFFER;
#endif

		dmac->chen = 0x0101 << APU_VOC_DMA_CHANNEL;


		voc_logic_count++;

	} else {
		printk("[warning] unknown dma interrupt. %lx %lx\n\r",
		       dmac->intstatus, dmac->com_intstatus);
		printk("voc intstatus: %lx\n\r", chx_intstatus);

		dmac_chanel_interrupt_clear(APU_VOC_DMA_CHANNEL);
	}
	return 0;
}
#endif

void init_fpioa(void)
{
	printk("init fpioa.\n\r");
	fpioa_init();
// 6+1 board with GO
//	fpioa_set_function(47, FUNC_GPIOHS4);
//	fpioa_set_function(42, FUNC_I2S0_IN_D0);
//	fpioa_set_function(43, FUNC_I2S0_IN_D1);
//	fpioa_set_function(44, FUNC_I2S0_IN_D2);
//	fpioa_set_function(45, FUNC_I2S0_IN_D3);
//	fpioa_set_function(46, FUNC_I2S0_WS);
//	fpioa_set_function(39, FUNC_I2S0_SCLK);

  // Pinout for Mic_PDM4_to_I2S4_Adaptor
  fpioa_set_function(PIN_MCU_I2S0_IN_D0, FUNC_I2S0_IN_D0);
  fpioa_set_function(PIN_MCU_I2S0_IN_D1, FUNC_I2S0_IN_D1);
  fpioa_set_function(PIN_MCU_I2S0_IN_D2, FUNC_I2S0_IN_D2);
  fpioa_set_function(PIN_MCU_I2S0_IN_D3, FUNC_I2S0_IN_D3);
  fpioa_set_function(PIN_MCU_I2S0_WS   , FUNC_I2S0_WS);
  fpioa_set_function(PIN_MCU_I2S0_SCLK , FUNC_I2S0_SCLK);

// LED's on 6+1 board
#ifndef PDM4_to_I2S4
  fpioa_set_function(24, FUNC_GPIOHS0 + SK9822_DAT_GPIONUM);
  fpioa_set_function(25, FUNC_GPIOHS0 + SK9822_CLK_GPIONUM);
#endif
  
#ifdef PDM4_to_I2S4
  // PDM_to_I2S Config
 	fpioa_set_function(PIN_MCU_WL_LSB, FUNC_GPIOHS10);
  fpioa_set_function(PIN_MCU_WL_MSB, FUNC_GPIOHS11);
  fpioa_set_function(PIN_MCU_OS_MODE1, FUNC_GPIOHS12);
  fpioa_set_function(PIN_MCU_OS_MODE2, FUNC_GPIOHS13);
  fpioa_set_function(PIN_MCU_OS_MODE3, FUNC_GPIOHS14);
  fpioa_set_function(PIN_MCU_2CH_TDM1, FUNC_GPIOHS15);
  fpioa_set_function(PIN_MCU_2CH_TDM2, FUNC_GPIOHS16);
  fpioa_set_function(PIN_MCU_SCLK_POL, FUNC_GPIOHS17);

  gpiohs_set_drive_mode(MCU_WL_LSB, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(MCU_WL_MSB, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(MCU_OS_MODE1, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(MCU_OS_MODE2, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(MCU_OS_MODE3, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(MCU_2CH_TDM1, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(MCU_2CH_TDM2, GPIO_DM_OUTPUT);
  gpiohs_set_drive_mode(MCU_SCLK_POL, GPIO_DM_OUTPUT);

  // Set config pins
  // APU demo works with 6+1 array board... MSM261S4030H0 I2S digital output MEMS microphone 32-bit words, 24 bit data 

#if (I2S_FS>=256000) && (I2S_FS<=384000) // OSR=8
  // 24-bit
  gpiohs_set_pin(MCU_WL_LSB, GPIO_PV_HIGH);
  gpiohs_set_pin(MCU_WL_MSB, GPIO_PV_LOW);
#else
  // 16-bits  : Based on results with 6+1 array it probably should be set to 16-bit PCM words
  gpiohs_set_pin(MCU_WL_LSB, GPIO_PV_HIGH);
  gpiohs_set_pin(MCU_WL_MSB, GPIO_PV_HIGH);
#endif
  

  // 32-bits  
//  gpiohs_set_pin(MCU_WL_LSB, GPIO_PV_LOW);
//  gpiohs_set_pin(MCU_WL_MSB, GPIO_PV_LOW);
  
//  gpiohs_set_pin(MCU_OS_MODE1, GPIO_PV_HIGH); // LF version with FS<48kHz
  // OS_MODE settings from Table #3 of TSDP18xx DS
  //  https://temposemi.com/wp-content/uploads/2019/09/TSDP18xx_DS.pdf
	#if (I2S_FS>=32000) && (I2S_FS<48000) // OSR=64... Needs SCLK=32 in i2s_rx_channel_config calls (init.c)
		gpiohs_set_pin(MCU_OS_MODE1, GPIO_PV_HIGH);
		gpiohs_set_pin(MCU_OS_MODE2, GPIO_PV_LOW);
		gpiohs_set_pin(MCU_OS_MODE3, GPIO_PV_HIGH);
	#endif
	#if (I2S_FS>=48000) && (I2S_FS<64000) // OSR=48... Valid SCLK/LRCLK = 48,96... Needs SCLK=24 in i2s_rx_channel_config calls (init.c)
		gpiohs_set_pin(MCU_OS_MODE1, GPIO_PV_HIGH);
		gpiohs_set_pin(MCU_OS_MODE2, GPIO_PV_LOW);
		gpiohs_set_pin(MCU_OS_MODE3, GPIO_PV_HIGH);
	#endif
	#if (I2S_FS>=64000) && (I2S_FS<=96000) // OSR=32
		gpiohs_set_pin(MCU_OS_MODE1, GPIO_PV_LOW);
		gpiohs_set_pin(MCU_OS_MODE2, GPIO_PV_LOW);
		gpiohs_set_pin(MCU_OS_MODE3, GPIO_PV_HIGH);
	#endif
	#if (I2S_FS>96000) && (I2S_FS<128000) // Undefined Fs range... same as 64<Fs<96 so OS_MODE[3:1]={100} & OSR=32
		gpiohs_set_pin(MCU_OS_MODE1, GPIO_PV_LOW);
		gpiohs_set_pin(MCU_OS_MODE2, GPIO_PV_LOW);
		gpiohs_set_pin(MCU_OS_MODE3, GPIO_PV_HIGH);
	#endif
	#if (I2S_FS>=128000) && (I2S_FS<=192000) // OSR=16
		gpiohs_set_pin(MCU_OS_MODE1, GPIO_PV_HIGH);
		gpiohs_set_pin(MCU_OS_MODE2, GPIO_PV_HIGH);
		gpiohs_set_pin(MCU_OS_MODE3, GPIO_PV_LOW);
	#endif
	#if (I2S_FS>192000) && (I2S_FS<256000) // undefined range in TSDP18 DS... TBD by testing
		gpiohs_set_pin(MCU_OS_MODE1, GPIO_PV_HIGH);
		gpiohs_set_pin(MCU_OS_MODE2, GPIO_PV_HIGH);
		gpiohs_set_pin(MCU_OS_MODE3, GPIO_PV_LOW);
	#endif
	#if (I2S_FS>=256000) && (I2S_FS<=384000) // OSR=8 ... seems to be invalid for this MCU unde any conditions
		gpiohs_set_pin(MCU_OS_MODE1, GPIO_PV_LOW);
		gpiohs_set_pin(MCU_OS_MODE2, GPIO_PV_HIGH);
		gpiohs_set_pin(MCU_OS_MODE3, GPIO_PV_LOW);
	#endif

  gpiohs_set_pin(MCU_2CH_TDM1, GPIO_PV_LOW);
  gpiohs_set_pin(MCU_2CH_TDM2, GPIO_PV_LOW); 
  gpiohs_set_pin(MCU_SCLK_POL, GPIO_PV_LOW);
#endif

}


void init_i2s(void)
{
	printk("init i2s.\n\r");

	/* I2s init */
    i2s_init(I2S_DEVICE_0, I2S_RECEIVER, 0x3);

/*
  Specs for 6+1 array PCB mic ...
  MSM261S4030H0 I2S digital output MEMS microphone with Multi-modes

  https://img.filipeflop.com/files/download/Datasheet-Microfone-Sipeed-MSM261S4030H0.pdf

	I2S DATA INTERFACE
	The serial data is in slave mode I2S format, which has 24 bit depth in a 32 bit word.
	In a stereo frame there are 64 SCK cycles, or 32 SCK cycles per data word.
	When L/R=0, the output data in the left channel, while L/R=Vdd, data in the right channel.
	The output data pin (SD) is tri-stated after the LSB is output so that another microphone
	can drive the common data line. 

	Data Word Length 
	The output data word length is 24 bits per channel. The Mic must always have 64 clock cycles
	for every stereo data word (fSCK = 64 x fWS). 

	Data Word Format
	The default data format is I2S, SB first. In this format, the MSB of each word is delayed by
	one SCK cycle from the start of each half frame.

*/  

#ifdef PDM4_to_I2S4
	#if (I2S_FS>=32000) && (I2S_FS<48000) // OSR=64... Needs SCLK=32 in i2s_rx_channel_config calls (init.c)
		i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_0,
            RESOLUTION_16_BIT, SCLK_CYCLES_32,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_1,
            RESOLUTION_16_BIT, SCLK_CYCLES_32,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_2,
            RESOLUTION_16_BIT, SCLK_CYCLES_32,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_3,
            RESOLUTION_16_BIT, SCLK_CYCLES_32,
            TRIGGER_LEVEL_4, STANDARD_MODE);
  #elseif (I2S_FS>=48000) && (I2S_FS<64000) // OSR=48... Valid SCLK/LRCLK = 48,96... Needs SCLK=24 in i2s_rx_channel_config calls (init.c)
		i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_0,
            RESOLUTION_16_BIT, SCLK_CYCLES_24,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_1,
            RESOLUTION_16_BIT, SCLK_CYCLES_24,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_2,
            RESOLUTION_16_BIT, SCLK_CYCLES_24,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_3,
            RESOLUTION_16_BIT, SCLK_CYCLES_24,
            TRIGGER_LEVEL_4, STANDARD_MODE);
	#elseif ((I2S_FS>=256000) && (I2S_FS<=384000)) // OSR=8... Valid SCLK/LRCLK = 32, 48, 64, 96, 128
		i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_0,
            RESOLUTION_16_BIT, SCLK_CYCLES_24,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_1,
            RESOLUTION_16_BIT, SCLK_CYCLES_24,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_2,
            RESOLUTION_16_BIT, SCLK_CYCLES_24,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_3,
            RESOLUTION_16_BIT, SCLK_CYCLES_24,
            TRIGGER_LEVEL_4, STANDARD_MODE);
	#else
		i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_0,
            RESOLUTION_16_BIT, SCLK_CYCLES_16,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_1,
            RESOLUTION_16_BIT, SCLK_CYCLES_16,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_2,
            RESOLUTION_16_BIT, SCLK_CYCLES_16,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_3,
            RESOLUTION_16_BIT, SCLK_CYCLES_16,
            TRIGGER_LEVEL_4, STANDARD_MODE);
	#endif
#else // Default for APU with 6+1 mic array
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_0,
             RESOLUTION_16_BIT, SCLK_CYCLES_32,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_1,
            RESOLUTION_16_BIT, SCLK_CYCLES_32,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_2,
            RESOLUTION_16_BIT, SCLK_CYCLES_32,
            TRIGGER_LEVEL_4, STANDARD_MODE);
    i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_3,
            RESOLUTION_16_BIT, SCLK_CYCLES_32,
            TRIGGER_LEVEL_4, STANDARD_MODE);
#endif

    i2s_set_sample_rate(I2S_DEVICE_0, I2S_FS); // increase sample rate to optimize BP 26kHz-42kHz FIR filter performance 
    // APU uses a define to set the sample rate "#define I2S_FS 44100" in apu.h by default. Changed to 54000,
}

void init_bf(void)
{
	printk("init bf.\n\r");

  uint16_t fir_prev_t[] = {
		0x020b, 0x0401, 0xff60, 0xfae2, 0xf860, 0x0022,
		0x10e6, 0x22f1, 0x2a98, 0x22f1, 0x10e6, 0x0022,
		0xf860, 0xfae2, 0xff60, 0x0401, 0x020b,
	};
	uint16_t fir_post_t[] = {
		0xf649, 0xe59e, 0xd156, 0xc615, 0xd12c, 0xf732,
		0x2daf, 0x5e03, 0x7151, 0x5e03, 0x2daf, 0xf732,
		0xd12c, 0xc615, 0xd156, 0xe59e, 0xf649,
	};

  uint16_t fir_LP05_t[] = {
		0x020b, 0x0401, 0xff60, 0xfae2, 0xf860, 0x0022,
		0x10e6, 0x22f1, 0x2a98, 0x22f1, 0x10e6, 0x0022,
		0xf860, 0xfae2, 0xff60, 0x0401, 0x020b,
	}; // Lowpass 5kHz - ORIG - FS=48kHz

	uint16_t fir_LP10_t[] = {
    0x00c1, 0xfd1c, 0xfe53, 0x0531, 0x0223, 0xf45b, 
    0xfd69, 0x2810, 0x42c1, 0x2810, 0xfd69, 0xf45b, 
    0x0223, 0x0531, 0xfe53, 0xfd1c, 0x00c1
	}; // Lowpass 10kHz - FS=48kHz

	uint16_t fir_LP15_t[] = {
    0xfe8a, 0xfef1, 0x039a, 0xfae8, 0x024d, 0x0662, 
    0xed04, 0x1e56, 0x5d18, 0x1e56, 0xed04, 0x0662, 
    0x024d, 0xfae8, 0x039a, 0xfef1, 0xfe8a
	}; // Lowpass 15kHz - FS=48kHz

	uint16_t fir_LP20_t[] = {
    0xfe75, 0x027b, 0xfbe5, 0x0609, 0xf7e8, 0x0a0f, 
    0xf450, 0x0cc4, 0x72db, 0x0cc4, 0xf450, 0x0a0f, 
    0xf7e8, 0x0609, 0xfbe5, 0x027b, 0xfe75
	}; // Lowpass 20kHz - FS=50kHz

	uint16_t fir_LP22_t[] = {
	  0xfe1f, 0x02b9, 0xfbae, 0x062a, 0xf7ea, 0x09e5, 
	  0xf4a1, 0x0c58, 0x7352, 0x0c58, 0xf4a1, 0x09e5, 
	  0xf7ea, 0x062a, 0xfbae, 0x02b9, 0xfe1f
	}; // Lowpass 22kHz - FS=54kHz

	uint16_t fir_BP7_20_t[] = {
    0x0428, 0xf75b, 0xfb94, 0x0b06, 0x036e, 0x221d, 
    0xc288, 0xdf8c, 0x749c, 0xdf8c, 0xc288, 0x221d, 
    0x036e, 0x0b06, 0xfb94, 0xf75b, 0x0428
	}; // Bandpass 7kHz-20kHz

  uint16_t fir_BP16_32_96_t[] = {
		0xfaa3, 0x0000, 0x17ba, 0x0000, 0x0ade, 0x0000, 
    0xb19c, 0x0000, 0x74fd, 0x0000, 0xb19c, 0x0000, 
    0x0ade, 0x0000, 0x17ba, 0x0000, 0xfaa3     
}; // Bandpass 16kHz-32kHz; Matlab: Fs=96kHz, FS1=10, FP1=16,FP2=32,FS2=38; BP,equiripple

  uint16_t fir_BP16_32_128_t[] = {
		0x1174, 0x0149, 0x00e4, 0x0eb2, 0xff13, 0xcb4b, 
    0xca8c, 0x206b, 0x5aa3, 0x206b, 0xca8c, 0xcb4b, 
    0xff13, 0x0eb2, 0x00e4, 0x0149, 0x1174
}; // Bandpass 16kHz-32kHz; Matlab: Fs=128kHz, FS1=10, FP1=16,FP2=32,FS2=38; BP,equiripple
  
    uint16_t fir_BP16_32_144_t[] = {
		0x0b56, 0xf53b, 0x0a59, 0x0a0a, 0xe7c3, 0xc53a, 
    0xdb90, 0x247f, 0x4dfc, 0x247f, 0xdb90, 0xc53a, 
    0xe7c3, 0x0a0a, 0x0a59, 0xf53b, 0x0b56
}; // Bandpass 16kHz-32kHz; Matlab: Fs=144kHz, FS1=10, FP1=16,FP2=32,FS2=38; BP,equiripple

    uint16_t fir_BP16_32_192_t[] = {
		0x0d95, 0xf9d1, 0x1a2f, 0xc3e8, 0x9d89, 0xbb34, 
    0x01a3, 0x4f32, 0x7220, 0x4f32, 0x01a3, 0xbb34, 
    0x9d89, 0xc3e8, 0x1a2f, 0xf9d1, 0x0d95  
}; // Bandpass 16kHz-32kHz; Matlab: Fs=192kHz, FS1=10, FP1=16,FP2=32,FS2=38; BP,equiripple

  uint16_t fir_BP16_32_256_t[] = {
		0xfb44, 0x05b9, 0xc967, 0xc294, 0xcd6d, 0xf16d, 
    0x27f4, 0x5973, 0x6d5b, 0x5973, 0x27f4, 0xf16d, 
    0xcd6d, 0xc294, 0xc967, 0x05b9, 0xfb44
}; // Bandpass 16kHz-32kHz; Matlab: Fs=256kHz, FS1=5, FP1=16,FP2=32,FS2=43; BP,equiripple

  uint16_t fir_BP18_42_96_t[] = {
		0xf77f, 0x01ce, 0xfd1c, 0x0d4d, 0xff30, 0x055b, 
    0xe4bc, 0xe79a, 0x4ef1, 0xe79a, 0xe4bc, 0x055b, 
    0xff30, 0x0d4d, 0xfd1c, 0x01ce, 0xf77f
}; // Bandpass 18kHz-42kHz; Matlab: Fs=128kHz, FS1=12, FP1=18,FP2=42,FS2=48; BP,equiripple
  
  uint16_t fir_BP18_42_128_t[] = {
	  0x00a0, 0x0197, 0x0805, 0xfa13, 0xfc69, 0xfb52, 
    0xd944, 0x0535, 0x435d, 0x0535, 0xd944, 0xfb52, 
    0xfc69, 0xfa13, 0x0805, 0x0197, 0x00a0
}; // Bandpass 18kHz-42kHz; Matlab: Fs=128kHz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple

  uint16_t fir_BP18_42_144_t[] = {
		0x077a, 0x1c1b, 0xfcc4, 0xfc3a, 0x0985, 0xdda8, 
    0xbced, 0x19be, 0x6aa0, 0x19be, 0xbced, 0xdda8, 
    0x0985, 0xfc3a, 0xfcc4, 0x1c1b, 0x077a
}; // Bandpass 18kHz-42kHz; Matlab: Fs=144kHz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple
  
  uint16_t fir_BP18_42_190_t[] = {
		0x0852, 0xf2d0, 0xfed1, 0x0211, 0xe829, 0xc95a, 
    0xe1d9, 0x3077, 0x5db5, 0x3077, 0xe1d9, 0xc95a, 
    0xe829, 0x0211, 0xfed1, 0xf2d0, 0x0852
}; // Bandpass 18kHz-42kHz; Matlab: Fs=190430Hz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple
    
  uint16_t fir_BP18_42_192_t[] = {
		0x07af, 0xf25e, 0xffb7, 0x01d3, 0xe6ff, 0xc96b, 
    0xe319, 0x30c2, 0x5d04, 0x30c2, 0xe319, 0xc96b, 
    0xe6ff, 0x01d3, 0xffb7, 0xf25e, 0x07af
}; // Bandpass 18kHz-42kHz; Matlab: Fs=192kHz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple
  
  uint16_t fir_BP18_42_256_t[] = {
		0xf3a1, 0x0c15, 0xfa8f, 0xe507, 0xd706, 0xe177, 
    0x06f8, 0x329e, 0x4602, 0x329e, 0x06f8, 0xe177, 
    0xd706, 0xe507, 0xfa8f, 0x0c15, 0xf3a1  
}; // Bandpass 18kHz-42kHz; Matlab: Fs=256kHz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple
  
  uint16_t fir_BP18_42_384_t[] = {
		0xe82c, 0xef37, 0xac0d, 0xcbe3, 0xf298, 0x0d14, 
    0x2f3a, 0x5173, 0x5f96, 0x5173, 0x2f3a, 0x0d14, 
    0xf298, 0xcbe3, 0xac0d, 0xef37, 0xe82c  
}; // Bandpass 18kHz-42kHz; Matlab: Fs=384kHz, FS1=8, FP1=18,FP2=42,FS2=52; BP,equiripple

  uint16_t fir_BP26_42_96_t[] = {
    0xfb19, 0x0e88, 0xed04, 0xfc1d, 0xf4e9, 0x2965, 
    0xe9cc, 0xc1ce, 0x725b, 0xc1ce, 0xe9cc, 0x2965, 
    0xf4e9, 0xfc1d, 0xed04, 0x0e88, 0xfb19
}; // Bandpass 26kHz-42kHz; Matlab: Fs=96kHz, FS1=20, FP1=26,FP2=42,FS2=48; BP,equiripple

  uint16_t fir_BP33_37_192_t[] = {
		0x8ab9, 0x179a, 0x586e, 0x5b37, 0x0a67, 0xbe00, 
    0xd46b, 0x36f5, 0x6ea0, 0x36f5, 0xd46b, 0xbe00, 
    0x0a67, 0x5b37, 0x586e, 0x179a, 0x8ab9  
}; // Bandpass 33kHz-37kHz; Matlab: Fs=192kHz, FS1=27, FP1=33,FP2=37,FS2=43; BP,equiripple

  uint16_t fir_BP30_40_192_t[] = {
		0xd203, 0x079c, 0x341e, 0x3ab6, 0xff00, 0xbd8f, 
    0xcd99, 0x266b, 0x5a8c, 0x266b, 0xcd99, 0xbd8f, 
    0xff00, 0x3ab6, 0x341e, 0x079c, 0xd203
}; // Bandpass 30kHz-40kHz; Matlab: Fs=192kHz, FS1=24, FP1=30,FP2=40,FS2=46; BP,equiripple
  
  uint16_t fir_BP30_40_144_t[] = {
		0xfb45, 0xff78, 0xcbc9, 0x17ed, 0x5645, 0xfb64, 
    0x9fb0, 0x0b78, 0x7808, 0x0b78, 0x9fb0, 0xfb64, 
    0x5645, 0x17ed, 0xcbc9, 0xff78, 0xfb45
}; // Bandpass 30kHz-40kHz; Matlab: Fs=144kHz, FS1=24, FP1=30,FP2=40,FS2=46; BP,equiripple
 
	uint16_t fir_neg_one[] = {
		0x8000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	};

	uint16_t fir_common[] = {
		0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3,
		0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3,
		0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3,
	}; // Kendryte staff user manageryzy indicates this is just a test value and is not used. Test to confirm.


//#ifdef PDM4_to_I2S4
// Exhibits sanity when using Android device tone generator (up to 22kHz) 
// Fixed frequency, 40kHz emitter (EM282-T) does not not work with this due to bursted nature of emitted signal

	#if (I2S_FS>=32000) && (I2S_FS<48000) // OSR=64... Needs SCLK=32 in i2s_rx_channel_config calls (init.c)
		apu_dir_set_prev_fir(fir_LP05_t); // must be 6+1 array
		apu_dir_set_post_fir(fir_LP05_t);  
	#endif
	#if (I2S_FS>=48000) && (I2S_FS<64000) // OSR=48... Valid SCLK/LRCLK = 48,96... Needs SCLK=24 in i2s_rx_channel_config calls (init.c)
		apu_dir_set_prev_fir(fir_LP05_t); // must be 6+1 array
		apu_dir_set_post_fir(fir_LP05_t);  
	#endif
	#if (I2S_FS>=64000) && (I2S_FS<=96000) // OSR=32
	  apu_dir_set_prev_fir(fir_BP18_42_96_t);
		apu_dir_set_post_fir(fir_BP18_42_96_t);
	#endif
	#if (I2S_FS>96000) && (I2S_FS<128000) // Undefined Fs range... same as 64<Fs<96 so OS_MODE[3:1]={100} & OSR=32
	  apu_dir_set_prev_fir(fir_BP18_42_128_t);
		apu_dir_set_post_fir(fir_BP18_42_128_t);
	#endif
	#if (I2S_FS>=128000) && (I2S_FS<=192000) // OSR=16
	  apu_dir_set_prev_fir(fir_BP18_42_192_t);
		apu_dir_set_post_fir(fir_BP18_42_192_t);
	#endif
	#if (I2S_FS>192000) && (I2S_FS<256000) // undefined range in TSDP18 DS... TBD by testing
	  apu_dir_set_prev_fir(fir_BP18_42_256_t);
		apu_dir_set_post_fir(fir_BP18_42_256_t);
	#endif
//#endif

    apu_voc_set_prev_fir(fir_BP18_42_256_t);
    apu_voc_set_post_fir(fir_BP18_42_256_t);
  
#ifdef PDM4_to_I2S4
	apu_set_delay(I2S_FS, 0.437, 8, 0);   // 4.37mm radius on UCA8 array, no center mic
#else	
  apu_set_delay(I2S_FS, 4, 6, 0);  // 4cm radius on 6+1 mic array PCB with center mic disabled
#endif
	apu_set_smpl_shift(APU_SMPL_SHIFT);
	apu_voc_set_saturation_limit(APU_SATURATION_VPOS_DEBUG, 
					  APU_SATURATION_VNEG_DEBUG); /// might need to be larger values... I already check for "overflow"
	apu_set_audio_gain(APU_AUDIO_GAIN_TEST);
	apu_voc_set_direction(0);

#ifdef PDM4_to_I2S4
  apu_set_channel_enabled(0xff);
#else
  apu_set_channel_enabled(0x3f);
#endif
 
  apu_set_down_size(0, 0); // no downsampling

#if APU_FFT_ENABLE
	apu_set_fft_shift_factor(1, 0xaa);
#else
	apu_set_fft_shift_factor(0, 0);
#endif

	apu_set_interrupt_mask(APU_DMA_ENABLE, APU_DMA_ENABLE);
#if APU_DIR_ENABLE
	apu_dir_enable();
#endif

apu_voc_enable(0); // disable by default .... enable as required elsewhere
//#if APU_VOC_ENABLE
//  apu_voc_enable(1);
//#endif
}

#if APU_DMA_ENABLE
void init_dma(void)
{
	printk("%s\n\r", __func__);
	// dmac enable dmac and interrupt
	union dmac_cfg_u dmac_cfg;

	dmac_cfg.data = readq(&dmac->cfg);
	dmac_cfg.cfg.dmac_en = 1;
	dmac_cfg.cfg.int_en = 1;
	writeq(dmac_cfg.data, &dmac->cfg);

	sysctl_dma_select(SYSCTL_DMA_CHANNEL_0 + APU_DIR_DMA_CHANNEL,
			  SYSCTL_DMA_SELECT_I2S0_BF_DIR_REQ);
	sysctl_dma_select(SYSCTL_DMA_CHANNEL_0 + APU_VOC_DMA_CHANNEL,
			  SYSCTL_DMA_SELECT_I2S0_BF_VOICE_REQ);
}
#endif

void init_dma_ch(int ch, volatile uint32_t *src_reg, void *buffer,
		 size_t size_of_byte)
{
	printk("%s %d\n\r", __func__, ch);

	dmac->channel[ch].sar = (uint64_t)src_reg;
	dmac->channel[ch].dar = (uint64_t)buffer;
	dmac->channel[ch].block_ts = (size_of_byte / 4) - 1;
	dmac->channel[ch].ctl =
		(((uint64_t)1 << 47) | ((uint64_t)15 << 48)
		 | ((uint64_t)1 << 38) | ((uint64_t)15 << 39)
		 | ((uint64_t)3 << 18) | ((uint64_t)3 << 14)
		 | ((uint64_t)2 << 11) | ((uint64_t)2 << 8) | ((uint64_t)0 << 6)
		 | ((uint64_t)1 << 4) | ((uint64_t)1 << 2) | ((uint64_t)1));
	/*
	 * dmac->channel[ch].ctl = ((  wburst_len_en  ) |
	 *                        (    wburst_len   ) |
	 *                        (  rburst_len_en  ) |
	 *                        (    rburst_len   ) |
	 *                        (one transaction:d) |
	 *                        (one transaction:s) |
	 *                        (    dst width    ) |
	 *                        (    src width   ) |
	 *                        (    dinc,0 inc  )|
	 *                        (  sinc:1,no inc ));
	 */

	dmac->channel[ch].cfg = (((uint64_t)1 << 49) | ((uint64_t)ch << 44)
				 | ((uint64_t)ch << 39) | ((uint64_t)2 << 32));
	/*
	 * dmac->channel[ch].cfg = ((     prior       ) |
	 *                         (      dst_per    ) |
	 *                         (     src_per     )  |
	 *           (    peri to mem  ));
	 *  01: Reload
	 */

	dmac->channel[ch].intstatus_en = 0x2; // 0xFFFFFFFF;
	dmac->channel[ch].intclear = 0xFFFFFFFF;

	dmac->chen = 0x0101 << ch;
}


void init_interrupt(void)
{
	plic_init();
	// bf
	plic_set_priority(IRQN_I2S0_INTERRUPT, 4);
	plic_irq_enable(IRQN_I2S0_INTERRUPT);
	plic_irq_register(IRQN_I2S0_INTERRUPT, int_apu, NULL);

#if APU_DMA_ENABLE
	// dma
	plic_set_priority(IRQN_DMA0_INTERRUPT + APU_DIR_DMA_CHANNEL, 4);
	plic_irq_register(IRQN_DMA0_INTERRUPT + APU_DIR_DMA_CHANNEL,
			  int_apu_dir_dma, NULL);
	plic_irq_enable(IRQN_DMA0_INTERRUPT + APU_DIR_DMA_CHANNEL);
	// dma
	plic_set_priority(IRQN_DMA0_INTERRUPT + APU_VOC_DMA_CHANNEL, 4);
	plic_irq_register(IRQN_DMA0_INTERRUPT + APU_VOC_DMA_CHANNEL,
			  int_apu_voc_dma, NULL);
	plic_irq_enable(IRQN_DMA0_INTERRUPT + APU_VOC_DMA_CHANNEL);
#endif
}

void init_ws2812b(void)
{
	gpiohs->output_en.bits.b4 = 1;
	gpiohs->output_val.bits.b4 = 0;
}

void init_all(void)
{
	init_fpioa();
#ifndef PDM4_to_I2S4
  init_mic_array_led();
#endif
	init_interrupt();
	init_i2s();
	init_bf();

	if (APU_DMA_ENABLE) {
		#if APU_DMA_ENABLE
		init_dma();
		#endif
#if APU_FFT_ENABLE
		init_dma_ch(APU_DIR_DMA_CHANNEL,
			    &apu->sobuf_dma_rdata,
			    APU_DIR_FFT_BUFFER[0], 512 * 4);
		init_dma_ch(APU_VOC_DMA_CHANNEL,
			    &apu->vobuf_dma_rdata, APU_VOC_FFT_BUFFER,
			    512 * 4);
#else
		init_dma_ch(APU_DIR_DMA_CHANNEL,
			    &apu->sobuf_dma_rdata, APU_DIR_BUFFER,
			    512 * 16 * 2);
		init_dma_ch(APU_VOC_DMA_CHANNEL,
			    &apu->vobuf_dma_rdata, APU_VOC_BUFFER,
			    512 * 2);
#endif
	}
	init_ws2812b();
	// apu_print_setting();
}
