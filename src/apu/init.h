#pragma once
#include <stdint.h>
#include <plic.h>
#include <i2s.h>
#include <sysctl.h>
#include <dmac.h>
#include <fpioa.h>
#include  <sk9822.h>
#include "gpiohs.h"

#define USONIC_SOURCE_IS_PHONE
//#define USONIC_SOURCE_IS_AIR
//#define USONIC_SOURCE_IS_EMMITER

#define PDM4_to_I2S4 // enable when testing ultrasonic hardware
#define EXPANDED_BP_RANGE


#ifndef APU_DIR_ENABLE
#define APU_DIR_ENABLE 1
#endif

#ifndef APU_VOC_ENABLE
#define APU_VOC_ENABLE 1
#endif

#ifndef APU_DMA_ENABLE
#define APU_DMA_ENABLE 0
#endif

#ifndef APU_FFT_ENABLE
#define APU_FFT_ENABLE 0
#endif

#ifndef APU_DATA_DEBUG
#define APU_DATA_DEBUG 0
#endif

#ifndef APU_GAIN_DEBUG
#define APU_GAIN_DEBUG 0
#endif

#ifndef APU_SETDIR_DEBUG
#define APU_SETDIR_DEBUG 0
#endif

#ifndef APU_SMPL_SHIFT
#define APU_SMPL_SHIFT 0x00
#endif

#ifndef APU_SATURATION_DEBUG
#define APU_SATURATION_DEBUG 0
#endif

#ifndef APU_SATURATION_VPOS_DEBUG
#define APU_SATURATION_VPOS_DEBUG 0x07ff
#endif

#ifndef APU_SATURATION_VNEG_DEBUG
#define APU_SATURATION_VNEG_DEBUG 0xf800
#endif

#ifndef APU_INPUT_CONST_DEBUG
#define APU_INPUT_CONST_DEBUG 0x0
#endif

#ifndef APU_SMPL_SHIFT_DEBUG
#define APU_SMPL_SHIFT_DEBUG 0
#endif

#ifndef I2S_RESOLUTION_TEST
#define I2S_RESOLUTION_TEST RESOLUTION_12_BIT
#endif

#ifndef I2S_SCLK_CYCLES_TEST
#define I2S_SCLK_CYCLES_TEST SCLK_CYCLES_16
#endif

#ifndef SYSCTL_THRESHOLD_I2S0_TEST
#define SYSCTL_THRESHOLD_I2S0_TEST 0xf
#endif

#ifndef APU_AUDIO_GAIN_TEST
	#ifdef USONIC_SOURCE_IS_PHONE
//		#define APU_AUDIO_GAIN_TEST (1 << 0)
		#define APU_AUDIO_GAIN_TEST (1 << 6) // 6 is good setting for phone with SQ DIR_SUM but too high for emitter... needs to be adaptive
//		#define APU_AUDIO_GAIN_TEST (1 << 10) // 
//		#define APU_AUDIO_GAIN_TEST (0x7FFF) // 
//		#define APU_AUDIO_GAIN_TEST (float)(1.125) // 
	#elif defined USONIC_SOURCE_IS_AIR
		#define APU_AUDIO_GAIN_TEST (1 << 10) 
	#elif USONIC_SOURCE_IS_EMMITER
		#define APU_AUDIO_GAIN_TEST (1 << 3) 
	#else
		#define APU_AUDIO_GAIN_TEST 300 // need to determine generic setting that works initially before going adaptive 
//		#define APU_AUDIO_GAIN_TEST (1 << 2) // need to determine generic setting that works initially before going adaptive 
	#endif
// audio_gain 	Audio data gain factor. 
//							Used to adjust the energy of the audio signal output by the channel mixing and mixing adder. 
//							11-bit fixed point number, low 10-bit is the fractional part.
//							Inspect this variable after setting to understand use

    /**
     * This is the audio sample gain factor. Using this gain factor to
     * enhance or reduce the stength of the sum of at most 8 source
     * sound channel outputs. This is a unsigned 11-bit fix-point number,
     * bit 10 is integer part and bit 9~0 are the fractional part.
     */
//    uint32_t audio_gain : 11;

// https://embeddedartistry.com/blog/2018/07/12/simple-fixed-point-conversion-in-c/
// https://en.wikipedia.org/wiki/Fixed-point_arithmetic
// Max UQ1.10 === 1+(2^10-1)/2^10 = 1.9990234375 = 0x7FF

/* 
 * Gain hunting algorithm...
 * If current = overflow then GAIN_NEW = GAIN_OLD * 0.5
 * If current is near overflow then GAIN_NEW = GAIN_OLD * 0.95
 * If current is NOT near overflow then GAIN_NEW = GAIN_OLD * 1.05
 * This should result in running from 85%-90% of full scale and when surges come along we clear to 59% and hunt up again
 * 
 * NOTE: Gain for use by MCU should be 1/8 audio_gain because values are added together or is it unity for VOC output?
 * Hard to say what value measured by steered antenna is compard to single channel...
 **/

#endif

#ifndef APU_PRESETN_DEBUG
#define APU_PRESETN_DEBUG 1
#endif

#ifndef APU_DEBUG_NO_EXIT
#define APU_DEBUG_NO_EXIT 1
#endif


#define APU_DIR_DMA_CHANNEL DMAC_CHANNEL3
#define APU_VOC_DMA_CHANNEL DMAC_CHANNEL4

#define APU_DIR_CHANNEL_MAX 16
#define APU_DIR_CHANNEL_SIZE 512
#define APU_VOC_CHANNEL_SIZE 512

#ifdef PDM4_to_I2S4

//256-384 {010} // OSR=8
//  #define I2S_FS 384000 // V(BCLK)=1.052 P-P, PDM_CLK = 3.096MHz... Doesn't work... no surprise given BCLK
//  #define I2S_FS 256000 // V(BCLK)=1.256 P-P, PDM_CLK=2.05MHz ... BF'r seems to work as well as Fs=255.9kHz but BCLK is reduced amplitude...
													// Must be hitting K210 limits? Only rated up to 192

// 192-256 : Undefined Fs range... same as 128<Fs<=192 so OS_MODE[3:1]={011} & OSR=16
#define I2S_FS 253906 // This value is fine-tuned so PLL hunting algorithm in sysctl_pll_set_freq determines this value exactly. Results in BCLK=520MHz.
                      // Use I2S_FS=253906.25 for FFT & elsewhere as that is actual frequency 
//  #define I2S_FS 224000
//  #define I2S_FS 192000 // Actually uses a lower PDM_CLK rate (2.05MHz) than 127999Hz but works fairly well with 22kHz sig

// 128-192 {011} OSR=16 
//  #define I2S_FS 192000 // V(BCLK)=1.698V P-P... works well at 22kHz with fir_BP18_42_192_t
//  #define I2S_FS 190430 // determined by PLL configuration as 390MHz / 32 (2 16bit words) / 64 (main multiplier)... matching BP filter
//  #define I2S_FS 128000 // 

// 96-128 : Undefined Fs range... same as 64<Fs<96 so OS_MODE[3:1]={100} & OSR=32
//  #define I2S_FS 127999
//	#define I2S_FS 96001 

// 64-96 {100} OSR=32
//	#define I2S_FS 96000 // Targetted ultrasonic rate
//  #define I2S_FS 78000 // 
//  #define I2S_FS 64001 // 

// 48-64 {101} OSR=48, SCLK_CYCLES=24 in i2s_rx_channel_config calls (init.c)
//	#define I2S_FS 64000
//	#define I2S_FS 54000
//  #define I2S_FS 48000
//  #define I2S_FS 44100

// 32-48 {101} OSR=64
  //#define I2S_FS 47999 // Needs SCLK=32... Shuts down TSDP18... MCU isn't generating SCLK=32... only 16!
//  Might be related to choce of PLL clock rate... works for original APU demo so...
// Changing SCLK_CLYCLES to 24 didn't make a diff
//  #define I2S_FS 47000 // Shuts down TSDP18
//	#define I2S_FS 40000 // Shuts down TSDP18
//	#define I2S_FS 32000 // Shuts down TSDP18

// This is odd... Increase I2S_FS by 1Hz and it wildly affects clocks actually generated. Scaled 96/1.081... error in SDK???
//#define I2S_FS 100595 // Results in BCLK=5.6MHz & LRCLK=88kHz
//#define I2S_FS 100596 // Results in BCLK=7.5MHz & LRCLK=117.3kHz
#else
  #define I2S_FS 44100 // Default rate for use with 6+1 board
#endif

#if APU_FFT_ENABLE
extern uint32_t APU_DIR_FFT_BUFFER[APU_DIR_CHANNEL_MAX]
				       [APU_DIR_CHANNEL_SIZE]
	__attribute__((aligned(128)));
extern uint32_t APU_VOC_FFT_BUFFER[APU_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));
#else
extern int16_t APU_DIR_BUFFER[APU_DIR_CHANNEL_MAX]
				  [APU_DIR_CHANNEL_SIZE]
	__attribute__((aligned(128)));
extern int16_t APU_VOC_BUFFER[APU_VOC_CHANNEL_SIZE]
	__attribute__((aligned(128)));
#endif


// TSDP18 Configuration
#ifdef PDM4_to_I2S4
	#define PIN_MCU_WL_LSB		41 
	#define PIN_MCU_WL_MSB		40
	#define PIN_MCU_OS_MODE1	39
	#define PIN_MCU_OS_MODE2	38
	#define PIN_MCU_OS_MODE3	37
	#define PIN_MCU_2CH_TDM1	36
	#define PIN_MCU_2CH_TDM2	17
	#define PIN_MCU_SCLK_POL	15

	#define MCU_WL_LSB		10 
	#define MCU_WL_MSB		11
	#define MCU_OS_MODE1	12
	#define MCU_OS_MODE2	13
	#define MCU_OS_MODE3	14
	#define MCU_2CH_TDM1	15
	#define MCU_2CH_TDM2	16
	#define MCU_SCLK_POL	17
#endif

// I2S IO Configuration
#ifdef PDM4_to_I2S4
	#define PIN_MCU_I2S0_IN_D0 47
	#define PIN_MCU_I2S0_IN_D1 46
	#define PIN_MCU_I2S0_IN_D2 45
	#define PIN_MCU_I2S0_IN_D3 44
	#define PIN_MCU_I2S0_WS    43   
	#define PIN_MCU_I2S0_SCLK  42 
#else
	#define PIN_MCU_I2S0_IN_D0 23
	#define PIN_MCU_I2S0_IN_D1 22
	#define PIN_MCU_I2S0_IN_D2 21
	#define PIN_MCU_I2S0_IN_D3 20
	#define PIN_MCU_I2S0_WS    19   
	#define PIN_MCU_I2S0_SCLK  18 
#endif
											  

extern uint64_t dir_logic_count;
extern uint64_t voc_logic_count;

void init_all(void);
