#include <stdlib.h>

#include <stdint.h>
#include <stdio.h>
#include "i2s.h"
#include "sysctl.h"
#include "plic.h"
#include "fpioa.h"
#include "dmac.h"

#include "gpiohs.h"

#include "fft.h"
#include <math.h>

#include "sleep.h"
#include "apu.h"
#include "voice.h"

#define FFT_N 512


uint64_t fft_out_data[FFT_N / 2];

extern volatile struct fft_t *const fft;

int16_t i2s_buffer[FFT_N * 16];
int16_t i2s_buffer2[FFT_N];

uint8_t  voc_data_rdy_flag;
uint8_t  dir_data_rdy_flag;

uint8_t channel2_block_done;
uint8_t channel3_block_done;
uint8_t channel2_dma_done;
uint8_t channel3_dma_done;

void dmac_ch2_irs(void *ctx);
void dmac_ch3_irs(void *ctx);

void i2s_config(void)
{
	sysctl_clock_set_threshold(SYSCTL_THRESHOLD_I2S0, 7);
	//sysctl_clock_set_threshold(SYSCTL_THRESHOLD_I2S0, 35);
	//96k:5,44k:12,24k:23,22k:25 16k:35 sampling
	/*sample rate*32bit*2 =75MHz/((N+1)*2) */

//	i2s_device_enable(I2S_DEVICE_0);
  i2s_set_enable(I2S_DEVICE_0,1);
	//enable i2s before configure
	i2s_disable_block(I2S_DEVICE_0, I2S_RECEIVER);

	i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_0,
		RESOLUTION_16_BIT, SCLK_CYCLES_32,
		TRIGGER_LEVEL_4, STANDARD_MODE);
	i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_1,
		RESOLUTION_16_BIT, SCLK_CYCLES_32,
		TRIGGER_LEVEL_4, STANDARD_MODE);
	i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_2,
		RESOLUTION_16_BIT, SCLK_CYCLES_32,
		TRIGGER_LEVEL_4, STANDARD_MODE);
	i2s_rx_channel_config(I2S_DEVICE_0, I2S_CHANNEL_3,
		RESOLUTION_16_BIT, SCLK_CYCLES_32,
		TRIGGER_LEVEL_4, STANDARD_MODE);

	i2s_set_mask_interrupt(I2S_DEVICE_0, I2S_CHANNEL_0, 1, 1, 1, 1);
	i2s_set_mask_interrupt(I2S_DEVICE_0, I2S_CHANNEL_1, 1, 1, 1, 1);
	i2s_set_mask_interrupt(I2S_DEVICE_0, I2S_CHANNEL_2, 1, 1, 1, 1);
	i2s_set_mask_interrupt(I2S_DEVICE_0, I2S_CHANNEL_3, 1, 1, 1, 1);

	i2s_receive_enable(I2S_DEVICE_0, I2S_CHANNEL_0);
	i2s_receive_enable(I2S_DEVICE_0, I2S_CHANNEL_1);
	i2s_receive_enable(I2S_DEVICE_0, I2S_CHANNEL_2);
	i2s_receive_enable(I2S_DEVICE_0, I2S_CHANNEL_3);


}
void i2s_swap(int x, int y)
{
	i2s_buffer[x] ^= i2s_buffer[y];
	i2s_buffer[y] ^= i2s_buffer[x];
	i2s_buffer[x] ^= i2s_buffer[y];
}
int i2s_irs(void *ctx)
{
//	struct apu_int_stat_t int_stat;
	apu_int_stat_t int_stat;
	int *l_ctx = ctx;

	if (*l_ctx < 10) {
		*l_ctx += 1;
		printf("rx times %d\n", *l_ctx);
	}
	int_stat = apu->bf_int_stat_reg;
	if (int_stat.dir_search_data_rdy) {
		int_stat.dir_search_data_rdy = 1; //clear int flag
		dir_data_rdy_flag = 1;
		if (*l_ctx < 10)
			printf("dir search\n");
	}
	if (int_stat.voc_buf_data_rdy) {
		int_stat.voc_buf_data_rdy = 1; //clear int flag
		voc_data_rdy_flag = 1;
		if (*l_ctx < 10)
			printf("voc_buf\n");
	}

	apu->bf_int_stat_reg = int_stat;

	return 0;
}

void dir_dma_init(void)
{
		//sysctl->misc.dma_sel3 = SYSCTL_DMA_SELECT_I2S1_RX_REQ;
	sysctl_dma_select(DMAC_CHANNEL2, SYSCTL_DMA_SELECT_I2S0_BF_DIR_REQ);
	dmac->channel[2].ctl = (((uint64_t)1 << 47) | ((uint64_t)15 << 48) | ((uint64_t)1 << 38) | ((uint64_t)15 << 39) | ((uint64_t)3 << 18) |
	//dmac->channel[2].ctl = (( wburst_len_en ) | (    wburst_len    ) | (  rburst_len_en  ) | (    rburst_len    ) | (one transaction:d) |
				((uint64_t)3 << 14) | ((uint64_t)2 << 11) | ((uint64_t)2 << 8) | ((uint64_t)0 << 6) | ((uint64_t)1 << 4) | ((uint64_t)1 << 2) | (uint64_t)1);
	//            (one transaction:s) | (    dst width    ) | (    src width   ) | (    dinc,0 inc  ) | (  sinc:1,no inc ));
	dmac->channel[2].cfg = (((uint64_t)1 << 49) | ((uint64_t)3 << 44) | ((uint64_t)3 << 39)  | ((uint64_t)2 << 32));
	//dmac->channel[2].cfg = ((     prior     ) | (      dst_per    ) | (     src_per     )  | (   peri to peri  ));
	//dmac->channel[2].intstatus_en = 0xFFFFFFE2;
	dmac->channel[2].intstatus_en = 0xFFFFFFFF;

	dmac->channel[2].sar = (uint64_t)&apu->sobuf_dma_rdata;
	dmac->channel[2].dar = (uint64_t)i2s_buffer;
	dmac->channel[2].block_ts = 512 * 16 / 2;
	dmac->channel[2].intclear = 0xFFFFFFFF;

	dmac->chen = 0x0404;
}
void voc_dma_init(void)
{
		//sysctl->misc.dma_sel3 = SYSCTL_DMA_SELECT_I2S1_RX_REQ;
	sysctl_dma_select(DMAC_CHANNEL3, SYSCTL_DMA_SELECT_I2S0_BF_VOICE_REQ);
	dmac->channel[3].ctl = (((uint64_t)1 << 47) | ((uint64_t)15 << 48) | ((uint64_t)1 << 38) | ((uint64_t)15 << 39) | ((uint64_t)3 << 18) |
	//dmac->channel[3].ctl = (( wburst_len_en ) | (    wburst_len    ) | (  rburst_len_en  ) | (    rburst_len    ) | (one transaction:d) |
				((uint64_t)3 << 14) | ((uint64_t)2 << 11) | ((uint64_t)2 << 8) | ((uint64_t)0 << 6) | ((uint64_t)1 << 4));
	//            (one transaction:s) | (    dst width    ) | (    src width   ) | (    dinc,0 inc  ) | (  sinc:1,no inc ));
	dmac->channel[3].cfg = (((uint64_t)1 << 49) | ((uint64_t)3 << 44) | ((uint64_t)3 << 39)  | ((uint64_t)2 << 32));
	//dmac->channel[3].cfg = ((     prior     ) | (      dst_per    ) | (     src_per     )  | (   peri to peri  ));
	//dmac->channel[3].intstatus_en = 0xFFFFFFE2;
	dmac->channel[3].intstatus_en = 0xFFFFFFFF;

	dmac->channel[3].sar = (uint64_t)&apu->vobuf_dma_rdata;
	dmac->channel[3].dar = (uint64_t)i2s_buffer2;
	dmac->channel[3].block_ts = 512 / 2;
	dmac->channel[3].intclear = 0xFFFFFFFF;

	dmac->chen = 0x0808;
}

void interrupt_init(void)
{
	int ctx1 = 0;
	int ctx2 = 0;

	plic_init();

	clear_csr(mie, MIP_MEIP);
	plic_set_priority(IRQN_DMA2_INTERRUPT, 5);
	plic_irq_enable(IRQN_DMA2_INTERRUPT);
	plic_irq_register(IRQN_DMA2_INTERRUPT, (plic_irq_callback_t)dmac_ch2_irs, &ctx1);

	plic_set_priority(IRQN_DMA3_INTERRUPT, 6);
	plic_irq_enable(IRQN_DMA3_INTERRUPT);
	plic_irq_register(IRQN_DMA3_INTERRUPT, (plic_irq_callback_t)dmac_ch3_irs, &ctx2);
}

void dmac_interrupt_enable(void)
{
	uint64_t reg_data = 0;

	reg_data = readq(&dmac->cfg);
	reg_data |= 1 << 1;
	writeq(reg_data, &dmac->cfg);
}
void interrupt_enable(void)
{
	set_csr(mie, MIP_MEIP);
	/* Enable interrupts in general. */
	set_csr(mstatus, MSTATUS_MIE);
}
void dmac_channel_int_enable(uint8_t channel_num)
{
	uint64_t reg_data = 0;

	reg_data = readq(&dmac->channel[channel_num].intstatus_en);
	reg_data |= 3;
	writeq(reg_data, &dmac->channel[channel_num].intstatus_en);
}
void dmac_ch2_irs(void *ctx)
{
	uint64_t reg_data;

	printf("dma channel2 int\n");

	reg_data = readq(&dmac->channel[DMAC_CHANNEL2].intstatus);

	dmac_chanel_interrupt_clear(DMAC_CHANNEL2);
	if ((reg_data & 0x2) == 0x2) {
		channel2_dma_done = 1;
	//	dmac->channel[2].sar = (uint64_t)&audio_bf->sobuf_dma_rdata;
	//	dmac->channel[2].dar = (uint64_t)i2s_buffer;
	//	dmac->channel[2].block_ts = 512 * 16 / 2 - 1;
	//	dmac->chen = 0x0404;
	} else
		printf("channel2 int status %lx\n", reg_data);
}
void dmac_ch3_irs(void *ctx)
{
	uint64_t reg_data;

	printf("dma channel3 int\n");

	reg_data = readq(&dmac->channel[DMAC_CHANNEL3].intstatus);

	dmac_chanel_interrupt_clear(DMAC_CHANNEL3);
	if ((reg_data & 0x2) == 0x2) {
		channel3_dma_done = 1;
		dmac->channel[3].sar = (uint64_t)&apu->vobuf_dma_rdata;
		dmac->channel[3].dar = (uint64_t)i2s_buffer2;
		dmac->channel[3].block_ts = 512 / 2 - 1;
		dmac->chen = 0x0808;
	} else
		printf("channel3 int status %lx\n", reg_data);
}
void init_fpioa(void)
{
	printf("init fpioa.\n");
	fpioa_init();
	fpioa_set_function(19, FUNC_GPIOHS4);
	fpioa_set_function(27, FUNC_I2S0_IN_D0);
	fpioa_set_function(20, FUNC_I2S0_IN_D1);
	fpioa_set_function(18, FUNC_I2S0_IN_D2);
	fpioa_set_function(21, FUNC_I2S0_IN_D3);
	fpioa_set_function(26, FUNC_I2S0_WS);
	fpioa_set_function(25, FUNC_I2S0_SCLK);
}
static void init_pll_aux(uint32_t pll_idx, uint32_t freq)
{
	/* Enable and reset all PLL */
	sysctl_pll_enable(SYSCTL_PLL0 + pll_idx);
	/* Set all PLL to 150MHz */
//	sysctl_pll_set_freq(SYSCTL_PLL0 + pll_idx, SYSCTL_SOURCE_IN0, freq);
	sysctl_pll_set_freq(SYSCTL_PLL0 + pll_idx, freq);
	/* Clear PLL lock status */
	sysctl_pll_clear_slip(SYSCTL_PLL0 + pll_idx);
	/* Waiting PLL lock */
	while (!sysctl_pll_is_lock(SYSCTL_PLL0 + pll_idx))
		sysctl_pll_clear_slip(SYSCTL_PLL0 + pll_idx);
	/* Enable PLL clock output */
	sysctl_clock_enable(SYSCTL_CLOCK_PLL0 + pll_idx);
}

void init_pll(void)
{
	printf("init pll.\n");
	init_pll_aux(0, 300000000);
	/* Set ACLK to PLL0 */
	sysctl_clock_set_clock_select(SYSCTL_CLOCK_SELECT_ACLK,
				      SYSCTL_SOURCE_PLL0);
	init_pll_aux(2, 45000000);
}
int main(void)
{
	int i;
	int ctx = 0;
	uint32_t data;

	printf("[INFO] RTL Git version : %08x\n", sysctl_get_git_id());
	printf("Init start\n");
	//init_pll();
	init_fpioa();
	//sysctl_reset(SYSCTL_RESET_FFT);
	sysctl_clock_enable(SYSCTL_CLOCK_APB0);
	sysctl_clock_enable(SYSCTL_CLOCK_I2S0);
	sysctl_reset(SYSCTL_RESET_I2S0);
	i2s_config();
	bf_module_cfg();


#if 1
	sysctl_clock_enable(SYSCTL_CLOCK_DMA);
	dmac_enable();
	dir_dma_init();
	voc_dma_init();

	interrupt_init();
	dmac_interrupt_enable();
	dmac_channel_int_enable(DMAC_CHANNEL2);
	dmac_channel_int_enable(DMAC_CHANNEL3);

#endif
	plic_set_priority(IRQN_I2S0_INTERRUPT, 3);
	plic_irq_enable(IRQN_I2S0_INTERRUPT);
	//if use poll receive ,must disable I2S0 interrupt
	plic_irq_register(IRQN_I2S0_INTERRUPT, i2s_irs, &ctx);

	interrupt_enable();

	apu_dir_enable();//just for one time enable , after finish then to be disable
	apu_voc_enable(1); //0: disable
	printf("Init finish\n");
	printf("sobuf_dma addr %p\n", &apu->sobuf_dma_rdata);
	printf("vobuf_dma addr %p\n", &apu->vobuf_dma_rdata);
//	for (size_t i = 0; i <= 0x128; i += 4) {
//		printf("1 %04x %08x\n", (int)i + 0x200,
//		*(uint32_t *)(((void *)audio_bf) + i));
//	}
	while (1) {
#if 0
		if (dir_data_rdy_flag == 1) {
			dir_data_rdy_flag = 0;
			printf("direction data:\n");
			for (i = 0; i < FFT_N * 16 / 2; i++) {
				data = apu->sobuf_dma_rdata;//.sobuf_dma_rdata;
				i2s_buffer[2 * i] = data & 0xffff;
				i2s_buffer[2 * i + 1] = (data >> 16) & 0xffff;

				if (i % 10 == 0)
					printf("\n");
				printf("%d %d ", i2s_buffer[2 * i], i2s_buffer[2 * i + 1]);
			}
			//as the result to choose the direction
			direction_select(APU_DIR0);
			bf_stream_enable(1);
		}

		if (voc_data_rdy_flag == 1) {
			voc_data_rdy_flag = 0;
			printf("voc data:\n");
			for (i = 0; i < FFT_N / 2; i++) {
				data = apu->vobuf_dma_rdata;//.sobuf_dma_rdata;
				i2s_buffer2[2 * i] = data & 0xffff;
				i2s_buffer2[2 * i + 1] = (data >> 16) & 0xffff;

				if (i % 10 == 0)
					printf("\n");
				printf("%d %d ", i2s_buffer2[2 * i], i2s_buffer2[2 * i + 1]);
			}
		}
#else
		if (channel2_dma_done == 1) {
			channel2_dma_done = 0;
			printf("direction data:\n");
		//	for (i = 0; i < FFT_N * 16 / 2; i++) {
		//		if (i % 10 == 0)
		//			printf("\n");

		//		printf("%d %d ", (int16_t)(i2s_buffer[i] & 0xffff), (int16_t)((i2s_buffer[i] >> 16) & 0xffff));

		//	}
			//as the result to choose the direction
			apu_voc_set_direction(APU_DIR0);
			apu_voc_enable(1);
		}
		if (channel3_dma_done == 1) {
			channel3_dma_done = 0;
			printf("voc data:\n");
		//	for (i = 0; i < FFT_N / 2; i++) {
		//		if (i % 10 == 0)
		//			printf("\n");
		//		printf("%d %d ", (int16_t)(i2s_buffer2[i] & 0xffff), (int16_t)((i2s_buffer2[i] >> 16) & 0xffff));
		//	}
		}
#endif
	}
	return 0;
}
