#include <stddef.h>
#include <stdint.h>
#include "encoding.h"
#include "syscalls.h"
#include "sysctl.h"
#include "voice.h"
#include "apu.h"


void bf_module_cfg(void)
{
	int i;
	uint8_t offsets[16][8] = {
		{0, 6, 17, 22, 17, 6, 11}, {0, 0, 10, 19, 19, 10, 10},
		{6, 0, 6, 17, 22, 17, 11}, {10, 0, 0, 10, 19, 19, 10},
		{17, 6, 0, 6, 17, 22, 11}, {19, 10, 0, 0, 10, 19, 10},
		{22, 17, 6, 0, 6, 17, 11}, {19, 19, 10, 0, 0, 10, 10},
		{17, 22, 17, 6, 0, 6, 11}, {10, 19, 19, 10, 0, 0, 10},
		{6, 17, 22, 17, 6, 0, 11}, {0, 10, 19, 19, 10, 0, 10},
		{0, 0, 0, 0, 0, 0, 0},     {0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0},     {0, 0, 0, 0, 0, 0, 0}, //
	};

	uint16_t fir_prev_t[] = {
		0x020b, 0x0401, 0xff60, 0xfae2, 0xf860, 0x0022,
		0x10e6, 0x22f1, 0x2a98, 0x22f1, 0x10e6, 0x0022,
		0xf860, 0xfae2, 0xff60, 0x0401, 0x020b,
	};
	uint16_t fir_post_t[] = {
		0xf649, 0xe59e, 0xd156, 0xc615, 0xd12c, 0xf732,
		0x2daf, 0x5e03, 0x7151, 0x5e03, 0x2daf, 0xf732,
		0xd12c, 0xc615, 0xd156, 0xe59e, 0xf649,
	};
	uint16_t fir_common[] = {
		0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3,
		0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3,
		0x03c3, 0x03c3, 0x03c3, 0x03c3, 0x03c3};

	apu_dir_set_prev_fir(fir_prev_t);
	apu_dir_set_post_fir(fir_post_t);
	apu_voc_set_prev_fir(fir_common);
	apu_voc_set_post_fir(fir_common);

	for (i = 0; i < 16; i++)
		apu_set_direction_delay(i, &offsets[i][0]);

	apu_set_audio_gain((1<<10)/8);
	apu_voc_set_direction(3);
	apu_set_channel_enabled(0x7F);
	apu_set_down_size(0, 0);
	apu_set_fft_shift_factor(0, 0);

	apu_set_interrupt_mask(1, 1);
//	audio_bf_print_setting();
}
